UPDATE Folders SET isPrivate=1 WHERE name='Personal';

UPDATE Files fi,Folders fo 
SET fi.isPrivate=1 
WHERE fi.folderID=fo.ID AND fo.isPrivate=1;