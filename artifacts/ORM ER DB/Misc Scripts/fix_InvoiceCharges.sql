UPDATE invoicecharges ic, invoices i
SET ic.created = i.endDate 
WHERE ic.optionID IN (1,2,3,5,7) AND ic.invoiceID IS NOT NULL AND ic.created NOT BETWEEN i.startDate AND i.endDate AND ic.invoiceID = i.ID;

UPDATE invoicecharges ic, invoices i, cases c
SET ic.created = c.created
WHERE ic.optionID IN (4) AND ic.invoiceID IS NOT NULL AND ic.created NOT BETWEEN i.startDate AND i.endDate AND ic.caseID = c.ID AND ic.invoiceID = i.ID;

UPDATE invoicecharges ic, invoices i, depositions d 
SET ic.created = d.created
WHERE ic.optionID IN (6,8) AND ic.invoiceID IS NOT NULL AND ic.created NOT BETWEEN i.startDate AND i.endDate AND ic.depositionID = d.ID AND ic.invoiceID = i.ID;

UPDATE invoicecharges ic, invoices i, depositions d 
SET ic.created = d.started
WHERE ic.optionID IN (9,10,11,12,13,14,15) AND ic.invoiceID IS NOT NULL AND ic.created NOT BETWEEN i.startDate AND i.endDate AND ic.depositionID = d.ID AND ic.invoiceID = i.ID;