## Attendees of deposition

select da.ID as AttendeeID,da.userID,da.name,da.email,u.email,u.clientID,das.userID as isAssistant,cm.userID as isManager,(u.ID=d.createdBy) AS isOwner from DepositionAttendees da
left join Users u on u.ID=da.userID
left join DepositionAssistants das on da.userID=das.userID AND das.depositionID=da.depositionID
inner join Depositions d on d.ID=da.depositionID
left join CaseManagers cm on cm.userID=da.userID AND cm.caseID=d.caseID
where da.depositionID=376