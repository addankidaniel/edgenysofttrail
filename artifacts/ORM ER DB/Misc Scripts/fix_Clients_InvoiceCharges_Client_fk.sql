ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `Clients_InvoiceCharges_Child`;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Clients_InvoiceCharges_Child` 
    FOREIGN KEY (`childClientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;