# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          edepo.dez                                       #
# Project name:                                                          #
# Author:                                                                #
# Script type:           Database drop script                            #
# Created on:            2013-02-27 13:39                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `Cases` DROP FOREIGN KEY `Users_Cases`;

ALTER TABLE `Cases` DROP FOREIGN KEY `Clients_Cases`;

ALTER TABLE `Cases` DROP FOREIGN KEY `LookupCaseTypes_Cases`;

ALTER TABLE `Users` DROP FOREIGN KEY `Clients_Users`;

ALTER TABLE `Users` DROP FOREIGN KEY `LookupUserTypes_Users`;

ALTER TABLE `Depositions` DROP FOREIGN KEY `Cases_Depositions`;

ALTER TABLE `Depositions` DROP FOREIGN KEY `Users_Depositions`;

ALTER TABLE `Depositions` DROP FOREIGN KEY `LookupDepositionStatuses_Depositions`;

ALTER TABLE `Depositions` DROP FOREIGN KEY `Depositions_Depositions_Linked`;

ALTER TABLE `Depositions` DROP FOREIGN KEY `Users_Depositions_Owner`;

ALTER TABLE `Depositions` DROP FOREIGN KEY `Users_Depositions_Speaker`;

ALTER TABLE `Folders` DROP FOREIGN KEY `Depositions_Folders`;

ALTER TABLE `Folders` DROP FOREIGN KEY `Users_Folders`;

ALTER TABLE `Files` DROP FOREIGN KEY `Folders_Files`;

ALTER TABLE `Files` DROP FOREIGN KEY `Users_Files`;

ALTER TABLE `Files` DROP FOREIGN KEY `Users_Files_Source`;

ALTER TABLE `DepositionAttendees` DROP FOREIGN KEY `Users_DepositionAttendees`;

ALTER TABLE `DepositionAttendees` DROP FOREIGN KEY `Depositions_DepositionAttendees`;

ALTER TABLE `Clients` DROP FOREIGN KEY `LookupClientTypes_Clients`;

ALTER TABLE `Clients` DROP FOREIGN KEY `Clients_Clients`;

ALTER TABLE `CaseManagers` DROP FOREIGN KEY `Cases_CaseManagers`;

ALTER TABLE `CaseManagers` DROP FOREIGN KEY `Users_CaseManagers`;

ALTER TABLE `DepositionAssistants` DROP FOREIGN KEY `Depositions_DepositionAssistants`;

ALTER TABLE `DepositionAssistants` DROP FOREIGN KEY `Users_DepositionAssistants`;

ALTER TABLE `RolePermissions` DROP FOREIGN KEY `Roles_RolePermissions`;

ALTER TABLE `RolePermissions` DROP FOREIGN KEY `Permissions_RolePermissions`;

ALTER TABLE `UserRoles` DROP FOREIGN KEY `Roles_UserRoles`;

ALTER TABLE `UserRoles` DROP FOREIGN KEY `Users_UserRoles`;

ALTER TABLE `PricingModelOptions` DROP FOREIGN KEY `LookupPricingOptions_PricingModelOptions`;

ALTER TABLE `PricingModelOptions` DROP FOREIGN KEY `Clients_PricingModelOptions`;

ALTER TABLE `PricingModelOptions` DROP FOREIGN KEY `LookupPricingTerms_PricingModelOptions`;

ALTER TABLE `PasswordResetRequests` DROP FOREIGN KEY `Users_PasswordResetRequests`;

ALTER TABLE `APISessions` DROP FOREIGN KEY `Users_APISessions`;

ALTER TABLE `APISessions` DROP FOREIGN KEY `DepositionAttendees_APISessions`;

ALTER TABLE `FileShares` DROP FOREIGN KEY `Files_FileShares`;

ALTER TABLE `FileShares` DROP FOREIGN KEY `Users_FileShares`;

ALTER TABLE `FileShares` DROP FOREIGN KEY `DepositionAttendees_FileShares`;

ALTER TABLE `FileShares` DROP FOREIGN KEY `Depositions_FileShares`;

ALTER TABLE `FileShares` DROP FOREIGN KEY `Users_FileShares_Creator`;

ALTER TABLE `FileShares` DROP FOREIGN KEY `Files_FileShares_Copy`;

ALTER TABLE `AuditLog` DROP FOREIGN KEY `LookupAuditActions_AuditLog`;

ALTER TABLE `AuditLog` DROP FOREIGN KEY `Users_AuditLog`;

ALTER TABLE `AuditLog` DROP FOREIGN KEY `LookupEntityTypes_AuditLog`;

ALTER TABLE `Invoices` DROP FOREIGN KEY `Clients_Invoices`;

ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `LookupPricingOptions_InvoiceCharges`;

ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `Invoices_InvoiceCharges`;

ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `Clients_InvoiceCharges`;

ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `Cases_InvoiceCharges`;

ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `Depositions_InvoiceCharges`;

ALTER TABLE `InvoiceCharges` DROP FOREIGN KEY `Clients_InvoiceCharges_Child`;

# ---------------------------------------------------------------------- #
# Drop table "InvoiceCharges"                                            #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `InvoiceCharges` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `InvoiceCharges` ALTER COLUMN `quantity` DROP DEFAULT;

ALTER TABLE `InvoiceCharges` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `InvoiceCharges`;

# ---------------------------------------------------------------------- #
# Drop table "Invoices"                                                  #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Invoices` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Invoices` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Invoices`;

# ---------------------------------------------------------------------- #
# Drop table "AuditLog"                                                  #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `AuditLog` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `AuditLog` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `AuditLog` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `AuditLog`;

# ---------------------------------------------------------------------- #
# Drop table "FileShares"                                                #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `FileShares` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `FileShares` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `FileShares` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `FileShares`;

# ---------------------------------------------------------------------- #
# Drop table "APISessions"                                               #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `APISessions` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `APISessions` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `APISessions` DROP PRIMARY KEY;

DROP INDEX `TUC_APISessions_1` ON `APISessions`;

# Drop table #

DROP TABLE `APISessions`;

# ---------------------------------------------------------------------- #
# Drop table "PasswordResetRequests"                                     #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `PasswordResetRequests` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `PasswordResetRequests` ALTER COLUMN `finished` DROP DEFAULT;

ALTER TABLE `PasswordResetRequests` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `PasswordResetRequests`;

# ---------------------------------------------------------------------- #
# Drop table "PricingModelOptions"                                       #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `PricingModelOptions` ALTER COLUMN `price` DROP DEFAULT;

ALTER TABLE `PricingModelOptions` ALTER COLUMN `typeID` DROP DEFAULT;

ALTER TABLE `PricingModelOptions` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `PricingModelOptions`;

# ---------------------------------------------------------------------- #
# Drop table "UserRoles"                                                 #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `UserRoles` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `UserRoles` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `UserRoles` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `UserRoles`;

# ---------------------------------------------------------------------- #
# Drop table "DepositionAssistants"                                      #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `DepositionAssistants` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `DepositionAssistants`;

# ---------------------------------------------------------------------- #
# Drop table "CaseManagers"                                              #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `CaseManagers` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `CaseManagers` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `CaseManagers`;

# ---------------------------------------------------------------------- #
# Drop table "Clients"                                                   #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Clients` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Clients` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Clients` ALTER COLUMN `deactivated` DROP DEFAULT;

ALTER TABLE `Clients` ALTER COLUMN `deleted` DROP DEFAULT;

ALTER TABLE `Clients` ALTER COLUMN `includedAttendees` DROP DEFAULT;

ALTER TABLE `Clients` ALTER COLUMN `attendeeBundleSize` DROP DEFAULT;

ALTER TABLE `Clients` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Clients`;

# ---------------------------------------------------------------------- #
# Drop table "DepositionAttendees"                                       #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `DepositionAttendees` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `DepositionAttendees` DROP PRIMARY KEY;

DROP INDEX `TUC_DepositionAttendees_1` ON `DepositionAttendees`;

DROP INDEX `TUC_DepositionAttendees_2` ON `DepositionAttendees`;

# Drop table #

DROP TABLE `DepositionAttendees`;

# ---------------------------------------------------------------------- #
# Drop table "Files"                                                     #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Files` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Files` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Files` ALTER COLUMN `isExhibit` DROP DEFAULT;

ALTER TABLE `Files` ALTER COLUMN `isPrivate` DROP DEFAULT;

ALTER TABLE `Files` ALTER COLUMN `isTranscript` DROP DEFAULT;

ALTER TABLE `Files` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Files`;

# ---------------------------------------------------------------------- #
# Drop table "Folders"                                                   #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Folders` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Folders` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Folders` ALTER COLUMN `isExhibit` DROP DEFAULT;

ALTER TABLE `Folders` ALTER COLUMN `isPrivate` DROP DEFAULT;

ALTER TABLE `Folders` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Folders`;

# ---------------------------------------------------------------------- #
# Drop table "Depositions"                                               #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Depositions` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Depositions` ALTER COLUMN `statusID` DROP DEFAULT;

ALTER TABLE `Depositions` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Depositions` ALTER COLUMN `deleted` DROP DEFAULT;

ALTER TABLE `Depositions` ALTER COLUMN `started` DROP DEFAULT;

ALTER TABLE `Depositions` ALTER COLUMN `finished` DROP DEFAULT;

ALTER TABLE `Depositions` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Depositions`;

# ---------------------------------------------------------------------- #
# Drop table "Users"                                                     #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Users` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Users` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Users` ALTER COLUMN `deactivated` DROP DEFAULT;

ALTER TABLE `Users` ALTER COLUMN `deleted` DROP DEFAULT;

ALTER TABLE `Users` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Users`;

# ---------------------------------------------------------------------- #
# Drop table "Cases"                                                     #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Cases` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Cases` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Cases` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Cases`;

# ---------------------------------------------------------------------- #
# Drop table "RolePermissions"                                           #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `RolePermissions` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `RolePermissions` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `RolePermissions` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `RolePermissions`;

# ---------------------------------------------------------------------- #
# Drop table "LookupEntityTypes"                                         #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupEntityTypes` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupEntityTypes`;

# ---------------------------------------------------------------------- #
# Drop table "LookupAuditActions"                                        #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupAuditActions` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupAuditActions`;

# ---------------------------------------------------------------------- #
# Drop table "LookupDepositionStatuses"                                  #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupDepositionStatuses` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupDepositionStatuses`;

# ---------------------------------------------------------------------- #
# Drop table "_db_version"                                               #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `_db_version` ALTER COLUMN `version` DROP DEFAULT;

ALTER TABLE `_db_version` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `_db_version`;

# ---------------------------------------------------------------------- #
# Drop table "LookupUserTypes"                                           #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupUserTypes` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupUserTypes`;

# ---------------------------------------------------------------------- #
# Drop table "LookupPricingTerms"                                        #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupPricingTerms` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupPricingTerms`;

# ---------------------------------------------------------------------- #
# Drop table "LookupPricingOptions"                                      #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `LookupPricingOptions` MODIFY `ID` INTEGER UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `LookupPricingOptions` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupPricingOptions`;

# ---------------------------------------------------------------------- #
# Drop table "Permissions"                                               #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Permissions` MODIFY `ID` BIGINT UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Permissions` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Permissions` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Permissions`;

# ---------------------------------------------------------------------- #
# Drop table "Roles"                                                     #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `Roles` MODIFY `ID` INTEGER UNSIGNED NOT NULL;

# Drop constraints #

ALTER TABLE `Roles` ALTER COLUMN `created` DROP DEFAULT;

ALTER TABLE `Roles` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `Roles`;

# ---------------------------------------------------------------------- #
# Drop table "LookupCaseTypes"                                           #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupCaseTypes` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupCaseTypes`;

# ---------------------------------------------------------------------- #
# Drop table "LookupClientTypes"                                         #
# ---------------------------------------------------------------------- #

# Drop constraints #

ALTER TABLE `LookupClientTypes` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `LookupClientTypes`;
