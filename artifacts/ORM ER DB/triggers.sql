# Purpose of this triggers is to automatically fill AuditLog table
delimiter |

DROP TRIGGER IF EXISTS Users_After_Update|
CREATE TRIGGER Users_After_Update AFTER UPDATE ON Users
  FOR EACH ROW BEGIN
	IF (OLD.deleted IS NOT NULL AND NEW.deleted IS NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='RST', entityTypeID='U';
	ELSEIF (OLD.deleted IS NULL AND NEW.deleted IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='DL', entityTypeID='U';
	END IF;
	IF (OLD.deactivated IS NOT NULL AND NEW.deactivated IS NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='RA', entityTypeID='U';
	ELSEIF (OLD.deactivated IS NULL AND NEW.deactivated IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='DA', entityTypeID='U';
    END IF;
  END
|

DROP TRIGGER IF EXISTS Clients_After_Update|
CREATE TRIGGER Clients_After_Update AFTER UPDATE ON Clients
  FOR EACH ROW BEGIN
	IF (OLD.deleted IS NOT NULL AND NEW.deleted IS NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='RST', entityTypeID='CL';
	ELSEIF (OLD.deleted IS NULL AND NEW.deleted IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='DL', entityTypeID='CL';
	END IF;
	IF (OLD.deactivated IS NOT NULL AND NEW.deactivated IS NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='RA', entityTypeID='CL';
	ELSEIF (OLD.deactivated IS NULL AND NEW.deactivated IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='DA', entityTypeID='CL';
    END IF;
  END 
|

DROP TRIGGER IF EXISTS Cases_After_Update|
CREATE TRIGGER Cases_After_Update AFTER UPDATE ON Cases
  FOR EACH ROW BEGIN
	IF (OLD.deleted IS NOT NULL AND NEW.deleted IS NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='RST', entityTypeID='CA';
	ELSEIF (OLD.deleted IS NULL AND NEW.deleted IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='DL', entityTypeID='CA';
	END IF;
  END
|

DROP TRIGGER IF EXISTS Depositions_After_Update|
CREATE TRIGGER Depositions_After_Update AFTER UPDATE ON Depositions
  FOR EACH ROW BEGIN
	IF (OLD.deleted IS NOT NULL AND NEW.deleted IS NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='RST', entityTypeID='D';
	ELSEIF (OLD.deleted IS NULL AND NEW.deleted IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='DL', entityTypeID='D';
	END IF;
	IF (OLD.started IS NULL AND NEW.started IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='ST', entityTypeID='D';
	END IF;
	IF (OLD.finished IS NULL AND NEW.finished IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=OLD.ID, actionID='FI', entityTypeID='D';
	END IF;
  END
|

DROP TRIGGER IF EXISTS Depositions_After_Insert|
CREATE TRIGGER Depositions_After_Insert AFTER INSERT ON Depositions
  FOR EACH ROW BEGIN
	IF (NEW.started IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=NEW.ID, actionID='ST', entityTypeID='D';
	END IF;
	IF (NEW.finished IS NOT NULL) THEN
		INSERT INTO AuditLog SET relatedTo=NEW.ID, actionID='FI', entityTypeID='D';
	END IF;
  END
|


delimiter ;