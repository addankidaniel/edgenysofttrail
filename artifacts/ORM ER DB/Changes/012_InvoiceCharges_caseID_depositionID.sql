CALL _assert_db_version(11);

ALTER TABLE `InvoiceCharges`
	ADD `caseID` BIGINT UNSIGNED AFTER `invoiceID`,
	ADD `depositionID` BIGINT UNSIGNED AFTER `caseID`;
	
ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Cases_InvoiceCharges` 
    FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Depositions_InvoiceCharges` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

UPDATE _db_version SET version = 12;