CALL _assert_db_version(36);


/* ED-2489; Reseller Levels */
ALTER TABLE `Clients` CHANGE COLUMN `resellerLevel` `resellerLevel` ENUM('1:Platinum','2:Gold','3:Silver','4:Bronze','5:None','9:Demo') NULL DEFAULT NULL;
UPDATE `Clients` SET `resellerLevel`='5:None' WHERE `resellerLevel`='4:Bronze';


UPDATE _db_version SET version = 37;
