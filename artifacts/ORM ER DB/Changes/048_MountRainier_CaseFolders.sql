
CALL _assert_db_version(47);

/* ED-2728; Case Folders */
ALTER TABLE `Folders` DROP FOREIGN KEY `Depositions_Folders`;
ALTER TABLE `Folders` CHANGE COLUMN `depositionID` `depositionID` BIGINT(20) UNSIGNED NULL,
  ADD COLUMN `caseID` BIGINT(20) UNSIGNED NULL AFTER `ID`,
  ADD INDEX `idx_Folders_caseID_Cases_ID` (`caseID` ASC);
ALTER TABLE `Folders`
  ADD CONSTRAINT `fk_Folders_depositionID_Depositions_ID` FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`)  ON DELETE CASCADE,
  ADD CONSTRAINT `fk_Folders_caseID_Cases_ID` FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `SessionCaseFolders` (
  `folderID` BIGINT(20) UNSIGNED NOT NULL,
  `sessionID` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`folderID`, `sessionID`),
  INDEX `idx_SessionCaseFolders_sessionID_Depositions_ID` (`sessionID` ASC),
  CONSTRAINT `fk_SessionCaseFolders_folderID_Folders_ID` FOREIGN KEY (`folderID`) REFERENCES `Folders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_SessionCaseFolders_sessionID_Depositions_ID` FOREIGN KEY (`sessionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO Folders (ID,caseID,depositionID,createdBy,name,created,lastModified,class)
  SELECT NULL,c.ID,NULL,u.ID,"Case Exhibits",NOW(),NOW(),"Exhibit" FROM Cases c
  LEFT JOIN Folders f ON (f.caseID=c.ID AND f.class="Exhibit")
  INNER JOIN Clients cl ON (cl.ID=c.clientID)
  INNER JOIN Users u ON (u.clientID=cl.ID AND u.typeID="C")
  WHERE c.deleted IS NULL AND cl.deactivated IS NULL AND cl.deleted IS NULL AND f.ID IS NULL
  GROUP BY c.ID;

INSERT INTO Folders (ID,caseID,depositionID,createdBy,name,created,lastModified,class)
  SELECT NULL,c.ID,NULL,u.ID,"Case Transcripts",NOW(),NOW(),"Transcript" FROM Cases c
  LEFT JOIN Folders f ON (f.caseID=c.ID AND f.class="Transcript")
  INNER JOIN Clients cl ON (cl.ID=c.clientID)
  INNER JOIN Users u ON (u.clientID=cl.ID AND u.typeID="C")
  WHERE c.deleted IS NULL AND cl.deactivated IS NULL AND cl.deleted IS NULL AND f.ID IS NULL
  GROUP BY c.ID;


UPDATE _db_version SET version = 48;
