
CALL _assert_db_version(26);

ALTER TABLE `Folders` ADD COLUMN `isCourtesyCopy` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0  AFTER `isPrivate`;
ALTER TABLE `Files` ADD COLUMN `sourceID` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `isTranscript`, ADD CONSTRAINT `Files_Files_SourceID` FOREIGN KEY (`sourceID`) REFERENCES `Files` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE, ADD INDEX `Files_Files_SourceID_idx` (`sourceID` ASC);

UPDATE _db_version SET version = 27;
