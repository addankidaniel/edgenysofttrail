CALL _assert_db_version(54);

/* Depositions.exhibitDate */
ALTER TABLE `Depositions` ADD COLUMN `exhibitDate` VARCHAR(16) NOT NULL DEFAULT '' AFTER `exhibitSubTitle`;


UPDATE _db_version SET version = 55;
