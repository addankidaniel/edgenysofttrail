CALL _assert_db_version(20);

INSERT INTO `LookupPricingOptions` (`ID`,`name`) VALUES ('10','Attendee - Per Deposition Fee (0-15)'),('11','Attendee - Per Deposition Fee (16-30)'),('12','Attendee - Per Deposition Fee (31+)'),('13','Deposition Leader - Per Deposition Fee (0-15)'),('14','Deposition Leader - Per Deposition Fee (16-100)'),('15','Deposition Leader - Per Deposition Fee (101+)');
ALTER TABLE InvoiceCharges ADD `userID` bigint(20) unsigned AFTER `depositionID`;
ALTER TABLE InvoiceCharges ADD CONSTRAINT `Users_InvoiceCharges` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

UPDATE _db_version SET version = 21;
