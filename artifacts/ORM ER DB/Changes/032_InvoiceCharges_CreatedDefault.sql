
CALL _assert_db_version(31);

ALTER TABLE `InvoiceCharges` CHANGE COLUMN `created` `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE _db_version SET version = 32;