START TRANSACTION;
CALL _assert_db_version(3);

ALTER TABLE `DepositionAttendees` 
  ADD `email` VARCHAR(255) AFTER `name`,
  CHANGE `name` `name` VARCHAR(255);
  
ALTER TABLE `DepositionAttendees` ADD CONSTRAINT `TUC_DepositionAttendees_1` 
    UNIQUE (`depositionID`, `userID`);
ALTER TABLE `DepositionAttendees` ADD CONSTRAINT `TUC_DepositionAttendees_2` 
    UNIQUE (`depositionID`, `email`);

UPDATE _db_version SET version = 4;
COMMIT;