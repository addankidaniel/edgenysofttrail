CALL _assert_db_version(44);

/* ED-2717; Trial Sessions */
ALTER TABLE `Depositions` CHANGE COLUMN `class` `class` ENUM('Deposition', 'Demo', 'WitnessPrep', 'WPDemo', 'Trial', 'Demo Trial') NOT NULL DEFAULT 'Deposition' COMMENT '';


UPDATE _db_version SET version = 45;
