CALL _assert_db_version(21);

ALTER TABLE Folders ADD `lastModified` timestamp NULL DEFAULT NULL AFTER `created`;

UPDATE _db_version SET version = 22;
