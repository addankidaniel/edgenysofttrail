CALL _assert_db_version(35);

INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (21, 'Attendee - Per Deposition Fee (0-15)', 0);
INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (22, 'Attendee - Per Deposition Fee (16-30)', 0);
INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (23, 'Attendee - Per Deposition Fee (31+)', 0);
INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (24, 'Leader - Per Deposition Fee (0-15)', 0);
INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (25, 'Leader - Per Deposition Fee (16-100)', 0);
INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (26, 'Leader - Per Deposition Fee (101+)', 0);

UPDATE `PricingEnterpriseOptions` SET `optionID`=13 WHERE `optionID`=16;
UPDATE `PricingEnterpriseOptions` SET `optionID`=14 WHERE `optionID`=17;
UPDATE `PricingEnterpriseOptions` SET `optionID`=15 WHERE `optionID`=18;

/* copy enterprise options to pricing model options if they are for sponsoring reseller */
INSERT INTO PricingModelOptions (
	SELECT peo.clientID, peo.optionID, peo.price, peo.typeID FROM PricingEnterpriseOptions peo
	INNER JOIN Clients c ON (c.ID=peo.clientID AND c.resellerID=peo.resellerID)
);

/* removing enterprise options that where copied to pricing model options */
DELETE peo FROM PricingEnterpriseOptions peo
INNER JOIN Clients c ON (c.ID=peo.clientID AND c.resellerID=peo.resellerID)
WHERE c.ID=peo.clientID AND c.resellerID=peo.resellerID;

/* removing enterprise agreements with sponsor */
DELETE ea FROM EnterpriseAgreements ea
INNER JOIN Clients ec ON (ec.ID=ea.enterpriseClientID)
WHERE ea.enterpriseResellerID=ec.resellerID;


/* Live Transcripts Email */
ALTER TABLE `Clients` ADD COLUMN `liveTranscriptsEmail` VARCHAR(255) NULL DEFAULT NULL AFTER `courtReporterEmail`;


/* Remove Enterprise Reseller */
UPDATE `Clients` SET `typeID`='R' WHERE `typeID`='ER';


/* ED-2489; Reseller Levels */
ALTER TABLE `Clients` ADD COLUMN `resellerLevel` ENUM('1:Platinum','2:Gold','3:Silver','4:Bronze') NULL DEFAULT NULL AFTER `casesDemoDefault`;
UPDATE `Clients` SET `resellerLevel`='4:Bronze' WHERE `typeID`='R';

/* WitnessPrep Demo */
ALTER TABLE `Depositions` CHANGE COLUMN `class` `class` ENUM('Deposition','Demo','WitnessPrep','WPDemo') NOT NULL DEFAULT 'Deposition';


UPDATE _db_version SET version = 36;
