CALL _assert_db_version(33);

/* SQL changes go here */
ALTER TABLE `Cases` DROP FOREIGN KEY `LookupCaseTypes_Cases`;
ALTER TABLE `Cases` DROP COLUMN `typeID`;
ALTER TABLE `Cases` ADD COLUMN `class` enum('Case','Demo') NOT NULL DEFAULT 'Case';
ALTER TABLE `Depositions` ADD COLUMN `class` enum('Deposition','Demo') NOT NULL DEFAULT 'Deposition';

/* ED 1420 - Add per-client tracking of demo deposition checkbox state for every new case */
ALTER TABLE `Clients` ADD COLUMN `casesDemoDefault` tinyint(1) unsigned NOT NULL DEFAULT '1';

UPDATE _db_version SET version = 34;
