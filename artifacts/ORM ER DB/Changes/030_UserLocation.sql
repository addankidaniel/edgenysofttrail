CALL _assert_db_version(29);

ALTER TABLE `Clients` ADD COLUMN `location` VARCHAR(30) DEFAULT NULL;

UPDATE _db_version SET version = 30 WHERE version = 29;
