CALL _assert_db_version(19);

ALTER TABLE `Files` 
    ADD `isTranscript` TINYINT(1) NOT NULL DEFAULT 0 AFTER `isPrivate`;

UPDATE _db_version SET version = 20;