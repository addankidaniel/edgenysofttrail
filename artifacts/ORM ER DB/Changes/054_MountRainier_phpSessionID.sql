CALL _assert_db_version(53);

/* ED-2919; Allow only one session per user */
ALTER TABLE `Users` ADD COLUMN `phpSessionID` CHAR(26) NULL DEFAULT NULL AFTER `lastLogin`;

UPDATE _db_version SET version = 54;
