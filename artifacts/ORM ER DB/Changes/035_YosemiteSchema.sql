CALL _assert_db_version(34);

/* ED-1595 - fixes for Folder permissions */
ALTER TABLE `Folders`
DROP COLUMN `isCourtesyCopy`,
DROP COLUMN `isPrivate`,
DROP COLUMN `isExhibit`,
CHANGE COLUMN `class` `class` ENUM('Folder','Exhibit','Personal','CourtesyCopy','Transcript','WitnessAnnotations','Trusted') NOT NULL DEFAULT 'Folder';

UPDATE Folders f
INNER JOIN Depositions d ON (d.ID=f.depositionID AND d.createdBy=f.createdBy)
SET f.class='Trusted'
WHERE f.class='Folder';

/* Custom Sort */
CREATE TABLE `UserSortPreferences` (
  `userID` BIGINT(20) UNSIGNED NOT NULL,
  `sortObject` ENUM('Cases','Depositions','Folders','Files') NOT NULL,
  `sortBy` ENUM('Case Number','Custom','Date','Name','Type','Witness') NOT NULL,
  `sortOrder` ENUM('Asc','Desc') NOT NULL DEFAULT 'Asc',
  PRIMARY KEY (`userID`, `sortObject`),
  CONSTRAINT `fk_UserSortPreferences_userID_Users_ID`
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);
/* ALTER TABLE UserSortPreferences CHANGE `sortBy` `sortBy` enum('Case Number','Custom','Date','Name','Type','Witness') NOT NULL; */

CREATE TABLE `UserCustomSortCases` (
  `userID` BIGINT(20) UNSIGNED NOT NULL,
  `caseID` BIGINT(20) UNSIGNED NOT NULL,
  `sortPos` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`userID`,`caseID`),
  INDEX `fk_UserCustomSortCases_caseID_Cases_ID_idx` (`caseID` ASC),
  CONSTRAINT `fk_UserCustomSortCases_userID_Users_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_UserCustomSortCases_caseID_Cases_ID` FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE `UserCustomSortDepositions` (
  `userID` BIGINT(20) UNSIGNED NOT NULL,
  `depositionID` BIGINT(20) UNSIGNED NOT NULL,
  `sortPos` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`userID`,`depositionID`),
  INDEX `fk_UserCustomSortDepositions_depositionID_Depositions_ID_idx` (`depositionID` ASC),
  CONSTRAINT `fk_UserCustomSortDepositions_userID_Users_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_UserCustomSortDepositions_depositionID_Depositions_ID` FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE `UserCustomSortFolders` (
  `userID` BIGINT(20) UNSIGNED NOT NULL,
  `folderID` BIGINT(20) UNSIGNED NOT NULL,
  `sortPos` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`userID`,`folderID`),
  INDEX `fk_UserCustomSortFolders_folderID_Folders_ID_idx` (`folderID` ASC),
  CONSTRAINT `fk_UserCustomSortFolders_userID_Users_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_UserCustomSortFolders_folderID_Folders_ID` FOREIGN KEY (`folderID`) REFERENCES `Folders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE);

CREATE TABLE `UserCustomSortFiles` (
  `userID` BIGINT(20) UNSIGNED NOT NULL,
  `fileID` BIGINT(20) UNSIGNED NOT NULL,
  `sortPos` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`userID`,`fileID`),
  INDEX `fk_UserCustomSortFiles_fileID_Files_ID_idx` (`fileID` ASC),
  CONSTRAINT `fk_UserCustomSortFiles_userID_Users_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_UserCustomSortFiles_fileID_Files_ID` FOREIGN KEY (`fileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE);


/* ED 1287 - Add WitnessPrep to Deposition classes */
ALTER TABLE `Depositions` CHANGE COLUMN `class` `class` ENUM('Deposition','Demo','WitnessPrep') NOT NULL DEFAULT 'Deposition';

INSERT INTO `LookupPricingOptions` (`ID`, `name`, `isEnterprise`) VALUES (19, 'Witness Prep - One-Time Fee', 0), (20, 'Witness Prep - Attendee Fee', 0);

/* API Sessions */
ALTER TABLE `APISessions` CHANGE COLUMN `sKey` `sKey` CHAR(128) NOT NULL;

/* Witness Login -- add temporary witness and witness member roles */
ALTER TABLE `DepositionAttendees` CHANGE COLUMN `role` `role` ENUM('M','G','W','TW','WM') NOT NULL DEFAULT 'G';

/* User Preferences */
CREATE TABLE `UserPreferences` (
  `userID` BIGINT UNSIGNED NOT NULL,
  `prefKey` ENUM('DepositionsFilter') CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
  `value` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
  PRIMARY KEY (`userID`, `prefKey`),
  CONSTRAINT `fk_UserPreferences_User_ID` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE);

ALTER TABLE `Depositions` ADD COLUMN `liveTranscript` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `class`;

UPDATE _db_version SET version = 35;
