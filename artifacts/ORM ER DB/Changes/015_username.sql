CALL _assert_db_version(14);

ALTER TABLE `Users` 
  ADD `username` VARCHAR(255) NOT NULL AFTER `email`;

UPDATE Users SET username=email;

ALTER TABLE `Users` DROP INDEX `IDX_Users_Auth`;

CREATE UNIQUE INDEX `IDX_Users_Auth` ON `Users` (`username`);

UPDATE _db_version SET version = 15;