START TRANSACTION;
CALL _assert_db_version(0);

CREATE TABLE `PasswordResetRequests` (
    `hash` VARCHAR(32) NOT NULL,
    `userID` BIGINT UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `finished` TIMESTAMP DEFAULT NULL COMMENT 'when password was actually changed by this request' NULL,
    CONSTRAINT `PK_PasswordResetRequests` PRIMARY KEY (`hash`)
);
ALTER TABLE `PasswordResetRequests` ADD CONSTRAINT `Users_PasswordResetRequests` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`);

UPDATE _db_version SET version = 1;
COMMIT;