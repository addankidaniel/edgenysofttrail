CALL _assert_db_version(24);
ALTER TABLE `DepositionAttendees` ADD COLUMN `role` ENUM('M','G','W') NULL AFTER `email`;
ALTER TABLE `DepositionAttendees` DROP COLUMN `isWitness`;
UPDATE _db_version SET version = 25;
