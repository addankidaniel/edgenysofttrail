
CALL _assert_db_version(23);

ALTER TABLE Depositions ADD `exhibitTitle` CHAR(128) NOT NULL DEFAULT '' AFTER `parentID`, ADD `exhibitSubTitle` CHAR(128) NOT NULL DEFAULT '' AFTER `exhibitTitle`, ADD `exhibitXOrigin` DECIMAL(9,8) UNSIGNED NOT NULL DEFAULT '0' AFTER `exhibitSubTitle`, ADD `exhibitYOrigin` DECIMAL(9,8) UNSIGNED NOT NULL DEFAULT '0' AFTER `exhibitXOrigin`;

UPDATE _db_version SET version = 24;
