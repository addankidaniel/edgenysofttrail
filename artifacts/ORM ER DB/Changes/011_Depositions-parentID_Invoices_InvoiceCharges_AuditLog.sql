CALL _assert_db_version(10);

# ---------------------------------------------------------------------- #
# Add table "LookupAuditActions"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupAuditActions` (
    `actionID` VARCHAR(3) NOT NULL,
    `action` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupAuditActions` PRIMARY KEY (`actionID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `LookupAuditActions` (`actionID`,`action`) VALUES
('CR','Created'),
('DL','Deleted'),
('RST','Restored'),
('DA','Deactivated'),
('RA','Reactivated'),
('ST','Started'),
('FI','Finished'),
('FUP','File uploaded'),
('ATI','Attendee limit increased'),
('OTH','Other action');

# ---------------------------------------------------------------------- #
# Add table "LookupEntityTypes"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupEntityTypes` (
    `typeID` VARCHAR(3) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupEntityTypes` PRIMARY KEY (`typeID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `LookupEntityTypes` (`typeID`,`type`) VALUES
('U','User'),
('D','Deposition'),
('CA','Case'),
('CL','Client');

ALTER TABLE `Depositions`
	ADD `parentID` BIGINT UNSIGNED AFTER `courtReporterPassword`,
	ADD `attendeeLimit` INTEGER NOT NULL AFTER `parentID`;
	
	
# ---------------------------------------------------------------------- #
# Add table "AuditLog"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `AuditLog` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(255),
    `actionID` VARCHAR(3) NOT NULL,
    `relatedTo` BIGINT UNSIGNED NOT NULL,
    `entityTypeID` VARCHAR(3) NOT NULL,
    `createdBy` BIGINT UNSIGNED,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_AuditLog` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "Invoice"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `Invoices` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL,
    `clientID` BIGINT UNSIGNED NOT NULL,
    `startDate` DATE NOT NULL,
    `endDate` DATE NOT NULL,
    CONSTRAINT `PK_Invoice` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "InvoiceCharge"                                              #
# ---------------------------------------------------------------------- #

CREATE TABLE `InvoiceCharges` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `clientID` BIGINT UNSIGNED NOT NULL,
    `invoiceID` BIGINT UNSIGNED,
    `price` DECIMAL(10,2) NOT NULL,
    `optionID` INTEGER UNSIGNED NOT NULL,
    `quantity` INTEGER NOT NULL DEFAULT 1,
    `created` TIMESTAMP NOT NULL,
    CONSTRAINT `PK_InvoiceCharges` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
ALTER TABLE `Depositions` ADD CONSTRAINT `Depositions_Depositions_Linked` 
    FOREIGN KEY (`parentID`) REFERENCES `Depositions` (`ID`) ON DELETE SET NULL;

ALTER TABLE `AuditLog` ADD CONSTRAINT `LookupAuditActions_AuditLog` 
    FOREIGN KEY (`actionID`) REFERENCES `LookupAuditActions` (`actionID`);

ALTER TABLE `AuditLog` ADD CONSTRAINT `Users_AuditLog` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `AuditLog` ADD CONSTRAINT `LookupEntityTypes_AuditLog` 
    FOREIGN KEY (`entityTypeID`) REFERENCES `LookupEntityTypes` (`typeID`);

ALTER TABLE `Invoices` ADD CONSTRAINT `Clients_Invoices` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `LookupPricingOptions_InvoiceCharges` 
    FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`);

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Invoices_InvoiceCharges` 
    FOREIGN KEY (`invoiceID`) REFERENCES `Invoices` (`ID`) ON DELETE CASCADE;
	
ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Clients_InvoiceCharges` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;
	
INSERT INTO LookupPricingOptions (`ID`,`name`) VALUES 
	(9,'Additional Attendee Bundle');
	
UPDATE Depositions d,Cases c,PricingModel pm SET d.attendeeLimit=pm.includedAttendees WHERE c.ID=d.caseID AND pm.clientID=c.clientID;

UPDATE _db_version SET version = 11;
