CALL _assert_db_version(43);

/* ED-2718; Court Client */
INSERT INTO `LookupClientTypes` (`typeID`, `type`) VALUES ('CRT', 'Court Client');
INSERT INTO `LookupUserTypes` (`typeID`, `type`) VALUES ('JDG', 'Judge'), ('RPT', 'Reporter'), ('OFF', 'Officer');

ALTER TABLE `Users` DROP COLUMN `password`, ADD COLUMN `clientAdmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `termsOfService`;
UPDATE `Users` SET `clientAdmin`=1 WHERE typeID='C';


UPDATE _db_version SET version = 44;
