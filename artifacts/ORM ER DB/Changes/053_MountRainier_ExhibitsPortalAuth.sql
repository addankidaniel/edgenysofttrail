CALL _assert_db_version(52);

/* ED-2944; Exhibits Portal Auth */
CREATE TABLE `ExhibitsPortalAuth` (
  `sessionID` BIGINT(20) UNSIGNED NOT NULL,
  `email` CHAR(128) NOT NULL,
  `word` CHAR(128) NOT NULL,
  `spice` CHAR(64) NOT NULL,
  PRIMARY KEY (`sessionID`, `email`),
  CONSTRAINT `fk_ExhibitsPortalAuth_sessionID_Depositions_ID` FOREIGN KEY (`sessionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);

UPDATE _db_version SET version = 53;
