CALL _assert_db_version(55);

/* Cases.clientAddress2 */
ALTER TABLE `Cases` ADD COLUMN `clientAddress2` VARCHAR(255) NULL AFTER `clientAddress`;

UPDATE _db_version SET version = 56;
