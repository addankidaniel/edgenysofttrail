CALL _assert_db_version(57);

ALTER TABLE `Cases` ADD COLUMN `clientCountryCode` CHAR(2) NOT NULL DEFAULT 'US' AFTER `clientZIP`;
ALTER TABLE `Cases` ADD COLUMN `clientRegion` VARCHAR(64) AFTER `clientState`;

ALTER TABLE `Clients` ADD COLUMN `countryCode` CHAR(2) NOT NULL DEFAULT 'US' AFTER `ZIP`;
ALTER TABLE `Clients` ADD COLUMN `region` VARCHAR(64) AFTER `state`;

ALTER TABLE `Users` ADD COLUMN `countryCode` CHAR(2) NOT NULL DEFAULT 'US' AFTER `ZIP`;
ALTER TABLE `Users` ADD COLUMN `region` VARCHAR(64) AFTER `state`;

UPDATE _db_version SET version = 58;
