CALL _assert_db_version(9);

ALTER TABLE `DepositionAssistants` DROP FOREIGN KEY `Depositions_DepositionAssistants`;
ALTER TABLE `DepositionAssistants` ADD CONSTRAINT `Depositions_DepositionAssistants` 
FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

UPDATE _db_version SET version = 10;
