CALL _assert_db_version(13);

ALTER TABLE `Depositions` 
  ADD `ownerID` BIGINT UNSIGNED NOT NULL AFTER `createdBy`;

UPDATE `Depositions` SET `ownerID`=`createdBy` WHERE 1;

ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions_Owner` 
    FOREIGN KEY (`ownerID`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

UPDATE _db_version SET version = 14;