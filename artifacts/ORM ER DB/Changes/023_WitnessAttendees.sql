
CALL _assert_db_version(22);

ALTER TABLE DepositionAttendees ADD `isWitness` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER `email`;

UPDATE _db_version SET version = 23;