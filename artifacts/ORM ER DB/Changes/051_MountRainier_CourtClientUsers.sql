CALL _assert_db_version(50);

UPDATE `Users` SET `typeID`='CU' WHERE `typeID`='OFF';
UPDATE `LookupUserTypes` SET `type`='Reporter / Official' WHERE `typeID`='RPT';
DELETE FROM `LookupUserTypes` WHERE `typeID`='OFF';

UPDATE _db_version SET version = 51;
