START TRANSACTION;
CALL _assert_db_version(2);

CREATE TABLE `LookupDepositionStatuses` (
    `statusID` CHAR(3) NOT NULL,
    `status` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupDepositionStatuses` PRIMARY KEY (`statusID`)
);
INSERT INTO `LookupDepositionStatuses` (`statusID`,`status`) VALUES 
('N', 'New'),
('I', 'In Process'),
('F', 'Finished');

ALTER TABLE `Depositions`
	ADD `statusID` CHAR(3) NOT NULL DEFAULT 'N' AFTER `createdBy`;
	
ALTER TABLE `Depositions` ADD CONSTRAINT `LookupDepositionStatuses_Depositions` 
    FOREIGN KEY (`statusID`) REFERENCES `LookupDepositionStatuses` (`statusID`);
	
ALTER TABLE `Folders` 
	ADD `isExhibit` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `created`;

UPDATE _db_version SET version = 3;
COMMIT;