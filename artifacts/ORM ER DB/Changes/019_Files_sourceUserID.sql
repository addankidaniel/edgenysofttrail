CALL _assert_db_version(18);

ALTER TABLE `Files` 
  ADD `sourceUserID` BIGINT UNSIGNED AFTER `createdBy`;
  
ALTER TABLE `Files` ADD CONSTRAINT `Users_Files_Source` 
    FOREIGN KEY (`sourceUserID`) REFERENCES `Users` (`ID`) ON DELETE SET NULL;

UPDATE _db_version SET version = 19;