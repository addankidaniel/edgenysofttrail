CALL _assert_db_version(59);

/*  Activated per User */
ALTER TABLE `Users` ADD COLUMN `activated` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `affiliateCode`;
UPDATE `Users` SET `activated`=1;

/* ED-3577 -- Free Trial */
ALTER TABLE `Clients` ADD COLUMN `freeTrial` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `resellerClass`;

UPDATE _db_version SET version = 60;
