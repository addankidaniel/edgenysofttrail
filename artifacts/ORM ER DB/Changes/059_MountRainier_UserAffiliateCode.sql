CALL _assert_db_version(58);

/*  Affiliate Code per User */
ALTER TABLE `Users` ADD COLUMN `affiliateCode` VARCHAR(32) NULL DEFAULT NULL AFTER `phpSessionID`;

UPDATE _db_version SET version = 59;
