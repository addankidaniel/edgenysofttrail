CALL _assert_db_version(17);

ALTER TABLE `Depositions` 
  ADD `speakerID` BIGINT UNSIGNED AFTER `ownerID`;
  
ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions_Speaker` 
    FOREIGN KEY (`speakerID`) REFERENCES `Users` (`ID`) ON DELETE SET NULL;

UPDATE _db_version SET version = 18;