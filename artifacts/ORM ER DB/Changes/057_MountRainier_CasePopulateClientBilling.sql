CALL _assert_db_version(56);

/* Cases.jurisdiction, clientName, clientAccount, clientCompanyName will now be DROPPED */
ALTER TABLE `Cases` 
  DROP COLUMN `jurisdiction`,
  DROP COLUMN `clientName`,
  DROP COLUMN `clientAccount`,
  DROP COLUMN `clientCompanyName`;

/* Populate the case billing info from the client address data */
UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientAddress = cl.address1
WHERE c.clientAddress IS NULL OR c.clientAddress = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientAddress2 = cl.address2
WHERE c.clientAddress2 IS NULL OR c.clientAddress2 = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientCity = cl.city
WHERE c.clientCity IS NULL OR c.clientCity = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientState = cl.state
WHERE c.clientState IS NULL OR c.clientState = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientZIP = cl.ZIP
WHERE c.clientZIP IS NULL OR c.clientZIP = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientEmail = cl.contactEmail
WHERE c.clientEmail IS NULL OR c.clientEmail = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientPhone = cl.contactPhone
WHERE c.clientPhone IS NULL OR c.clientPhone = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientFirstName = TRIM(REVERSE(SUBSTRING(REVERSE(TRIM(cl.contactName)),LOCATE(' ',REVERSE(TRIM(cl.contactName))))))
WHERE c.clientFirstName IS NULL OR c.clientFirstName = '';

UPDATE Cases c
  JOIN Cases c2 ON c.ID = c2.ID
  JOIN Clients cl ON c2.clientID = cl.ID
SET c.clientLastName = TRIM(SUBSTRING_INDEX(TRIM(cl.contactName), ' ', -1))
WHERE c.clientLastName IS NULL OR c.clientLastName = '';

/*
Since if there is no space in the contactName
then we only end up with a last name, move the
last name to the first name.
*/
UPDATE Cases c
SET c.clientFirstName = c.clientLastName,
  c.clientLastName = ''
WHERE (c.clientFirstName IS NULL OR c.clientFirstName = '') AND c.clientLastName <> '' AND c.clientLastName IS NOT NULL;

UPDATE _db_version SET version = 57;
