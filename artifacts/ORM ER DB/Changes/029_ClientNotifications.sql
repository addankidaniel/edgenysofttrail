CALL _assert_db_version(28);

CREATE TABLE `ClientMessages` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `messageBody` longtext,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE `ClientNotifications` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned DEFAULT NULL,
  `clientMessageID` bigint(20) unsigned DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sentOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_CLIENTMESSAGEID_idx` (`clientMessageID`),
  KEY `FK_USERID_idx_idx` (`userID`),
  CONSTRAINT `FK_USERID_idx` FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CLIENTMESSAGEID` FOREIGN KEY (`clientMessageID`) REFERENCES `ClientMessages` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

UPDATE _db_version SET version = 29;
