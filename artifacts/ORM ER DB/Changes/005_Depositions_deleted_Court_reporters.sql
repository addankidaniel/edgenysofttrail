START TRANSACTION;
CALL _assert_db_version(4);

ALTER TABLE `Depositions` 
  ADD `deleted` TIMESTAMP DEFAULT NULL NULL AFTER `created`,
  ADD `courtReporterID` BIGINT UNSIGNED AFTER `finished`;
  
ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions_CourtReporter` 
    FOREIGN KEY (`courtReporterID`) REFERENCES `Users` (`ID`) ON DELETE SET NULL;

UPDATE _db_version SET version = 5;
COMMIT;