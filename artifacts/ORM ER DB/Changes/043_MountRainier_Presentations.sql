CALL _assert_db_version(42);


/* Relate File ID to Deposition for Presentation(s) */
CREATE TABLE `Presentations` (
  `depositionID` BIGINT(20) UNSIGNED NOT NULL,
  `fileID` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`depositionID`),
  INDEX `idx_Presentations_fileID_Files_ID` (`fileID` ASC),
  CONSTRAINT `fk_Presentations_depositionID_Depositions_ID` FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Presentations_fileID_Files_ID` FOREIGN KEY (`fileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);


UPDATE _db_version SET version = 43;
