START TRANSACTION;
CALL _assert_db_version(8);

UPDATE `Depositions` SET uKey = ID;

UPDATE _db_version SET version = 9;
COMMIT;