CALL _assert_db_version(12);

SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `Clients` 
	ADD `includedAttendees` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `ZIP`,
	ADD `attendeeBundleSize` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `includedAttendees`;
	
UPDATE `Clients` c INNER JOIN `PricingModel` pm ON pm.clientID=c.ID SET c.includedAttendees=pm.includedAttendees, c.attendeeBundleSize=pm.bundleSize;
REPLACE INTO `PricingModelOptions` (clientID, optionID, price, typeID) (SELECT clientID, 9, pricePerAdditionalBundle, 'O' FROM PricingModel);

ALTER TABLE `PricingModel` DROP FOREIGN KEY `Clients_PricingModel`;
DROP TABLE `PricingModel`;

ALTER TABLE `InvoiceCharges`
	ADD `childClientID` BIGINT UNSIGNED AFTER `clientID`;
	
ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Clients_InvoiceCharges_Child` 
    FOREIGN KEY (`childClientID`) REFERENCES `Clients` (`ID`);
	
UPDATE PricingModelOptions SET typeID='M' WHERE optionID IN (5,7);

SET FOREIGN_KEY_CHECKS=1;

UPDATE _db_version SET version = 13;