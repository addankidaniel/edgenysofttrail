# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          edepo.dez                                       #
# Project name:                                                          #
# Author:                                                                #
# Script type:           Database creation script                        #
# Created on:            2013-02-27 13:39                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Tables                                                                 #
# ---------------------------------------------------------------------- #

# ---------------------------------------------------------------------- #
# Add table "LookupClientTypes"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupClientTypes` (
    `typeID` CHAR(3) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupClientTypes` PRIMARY KEY (`typeID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO LookupClientTypes(`typeID`,`type`) VALUES ('O','Owner'), ('R','Reseller'), ('C','Client');

# ---------------------------------------------------------------------- #
# Add table "LookupCaseTypes"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupCaseTypes` (
    `typeID` INTEGER UNSIGNED NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupCaseTypes` PRIMARY KEY (`typeID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "Roles"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `Roles` (
    `ID` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(40) NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_Roles` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'check this video https://saritasa.webex.com/saritasa/ldr.php?AT=pb&SP=MC&rID=28306447&rKey=e8e6140d68eb5c2d';

# ---------------------------------------------------------------------- #
# Add table "Permissions"                                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `Permissions` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `node` VARCHAR(255) NOT NULL,
    `permission` VARCHAR(40) NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `comment` TEXT,
    CONSTRAINT `PK_Permissions` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'check this video https://saritasa.webex.com/saritasa/ldr.php?AT=pb&SP=MC&rID=28306447&rKey=e8e6140d68eb5c2d';

CREATE UNIQUE INDEX `IDX_Permissions_NN` ON `Permissions` (`node`,`permission`);

# ---------------------------------------------------------------------- #
# Add table "LookupPricingOptions"                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupPricingOptions` (
    `ID` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupPricingOptions` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO LookupPricingOptions (`ID`,`name`) VALUES 
(1,'Base Licensing Fee'), 
(2,'Setup Fee'), 
(3,'Subscription Fee (per user)'), 
(4,'Case One-Time Setup'), 
(5,'Case Monthly Hosting'), 
(6,'Deposition One-Time Setup'), 
(7,'Deposition Monthly Hosting'), 
(8,'Upload Fee Per Document'), 
(9,'Additional Attendee Bundle');

# ---------------------------------------------------------------------- #
# Add table "LookupPricingTerms"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupPricingTerms` (
    `typeID` CHAR(3) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupPricingTerms` PRIMARY KEY (`typeID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO LookupPricingTerms (`typeID`, `type`) VALUES ('O', 'One Time'), ('M', 'Monthly');

# ---------------------------------------------------------------------- #
# Add table "LookupUserTypes"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupUserTypes` (
    `typeID` CHAR(3) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupUserTypes` PRIMARY KEY (`typeID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO LookupUserTypes(`typeID`,`type`) VALUES 
('SA','Super Admin'), ('A','Admin'), ('R','Reseller Admin'), ('C', 'Client Admin'), ('CU', 'Client User');

# ---------------------------------------------------------------------- #
# Add table "_db_version"                                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `_db_version` (
    `version` INTEGER(11) NOT NULL DEFAULT 0,
    CONSTRAINT `PK__db_version` PRIMARY KEY (`version`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `_db_version` (`version`) VALUES (20);

DELIMITER $$

DROP PROCEDURE IF EXISTS `_assert_db_version`$$

CREATE DEFINER= CURRENT_USER PROCEDURE `_assert_db_version`(IN db_version INT(6) UNSIGNED)
BEGIN
	IF NOT EXISTS (SELECT 1 FROM _db_version WHERE _db_version.version = db_version) THEN
		SELECT CONCAT('Invalid previous version of ', db_version, ', please fix it') AS Error;
		CALL nonexistent_error;
	END IF;
    END$$

DELIMITER ;

# ---------------------------------------------------------------------- #
# Add table "LookupDepositionStatuses"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupDepositionStatuses` (
    `statusID` CHAR(3) NOT NULL,
    `status` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupDepositionStatuses` PRIMARY KEY (`statusID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `LookupDepositionStatuses` (`statusID`,`status`) VALUES 
('N', 'New'),
('I', 'In Process'),
('F', 'Finished');

# ---------------------------------------------------------------------- #
# Add table "LookupAuditActions"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupAuditActions` (
    `actionID` VARCHAR(3) NOT NULL,
    `action` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupAuditActions` PRIMARY KEY (`actionID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `LookupAuditActions` (`actionID`,`action`) VALUES
('CR','Created'),
('DL','Deleted'),
('RST','Restored'),
('DA','Deactivated'),
('RA','Reactivated'),
('ST','Started'),
('FI','Finished'),
('FUP','File uploaded'),
('ATI','Attendee limit increased'),
('OTH','Other action');

# ---------------------------------------------------------------------- #
# Add table "LookupEntityTypes"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `LookupEntityTypes` (
    `typeID` VARCHAR(3) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    CONSTRAINT `PK_LookupEntityTypes` PRIMARY KEY (`typeID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `LookupEntityTypes` (`typeID`,`type`) VALUES
('U','User'),
('D','Deposition'),
('CA','Case'),
('CL','Client');

# ---------------------------------------------------------------------- #
# Add table "RolePermissions"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `RolePermissions` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `roleID` INTEGER UNSIGNED NOT NULL,
    `permissionID` BIGINT UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_RolePermissions` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'check this video https://saritasa.webex.com/saritasa/ldr.php?AT=pb&SP=MC&rID=28306447&rKey=e8e6140d68eb5c2d';

# ---------------------------------------------------------------------- #
# Add table "Cases"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `Cases` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `clientID` BIGINT UNSIGNED NOT NULL,
    `typeID` INTEGER UNSIGNED,
    `createdBy` BIGINT UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL COMMENT 'comment goes here',
    `number` VARCHAR(60) NOT NULL,
    `jurisdiction` VARCHAR(255) NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted` TIMESTAMP NULL,
    `clientName` VARCHAR(255) NOT NULL,
    `clientAccount` VARCHAR(40),
    `clientCompanyName` VARCHAR(255),
    `clientFirstName` VARCHAR(255),
    `clientLastName` VARCHAR(255),
    `clientAddress` VARCHAR(255),
    `clientCity` VARCHAR(255),
    `clientState` CHAR(2),
    `clientZIP` VARCHAR(10),
    `clientEmail` VARCHAR(255),
    `clientPhone` VARCHAR(40),
    CONSTRAINT `PK_Cases` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'Cases desc goes here';

# ---------------------------------------------------------------------- #
# Add table "Users"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `Users` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `typeID` CHAR(3) NOT NULL,
    `clientID` BIGINT UNSIGNED,
    `firstName` VARCHAR(255) NOT NULL,
    `lastName` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `phone` VARCHAR(40),
    `address1` VARCHAR(255),
    `address2` VARCHAR(255),
    `city` VARCHAR(255),
    `state` CHAR(2),
    `ZIP` VARCHAR(10),
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deactivated` TIMESTAMP DEFAULT NULL NULL,
    `deleted` TIMESTAMP DEFAULT NULL NULL,
    CONSTRAINT `PK_Users` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `Users` (`ID`,`typeID`,`firstName`,`lastName`,`email`,`username`,`password`) VALUES
(1,'SA','Super','Admin','admin@edepo.com','admin','123');

CREATE UNIQUE INDEX `IDX_Users_Auth` ON `Users` (`username`);

# ---------------------------------------------------------------------- #
# Add table "Depositions"                                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `Depositions` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `caseID` BIGINT UNSIGNED NOT NULL,
    `createdBy` BIGINT UNSIGNED NOT NULL,
    `ownerID` BIGINT UNSIGNED NOT NULL,
    `speakerID` BIGINT UNSIGNED,
    `statusID` CHAR(3) NOT NULL DEFAULT 'N',
    `uKey` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255),
    `openDateTime` DATETIME NOT NULL,
    `depositionOf` VARCHAR(255) NOT NULL,
    `volume` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `location` VARCHAR(255),
    `oppositionNotes` VARCHAR(255),
    `notes` TEXT,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deleted` TIMESTAMP DEFAULT NULL NULL,
    `started` TIMESTAMP DEFAULT NULL NULL,
    `finished` TIMESTAMP DEFAULT NULL NULL,
    `courtReporterEmail` VARCHAR(255),
    `courtReporterPassword` VARCHAR(255),
    `parentID` BIGINT UNSIGNED,
    `attendeeLimit` INTEGER NOT NULL,
    CONSTRAINT `PK_Depositions` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE UNIQUE INDEX `IDX_Depositions_ukey` ON `Depositions` (`uKey`);

# ---------------------------------------------------------------------- #
# Add table "Folders"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `Folders` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `depositionID` BIGINT UNSIGNED NOT NULL,
    `createdBy` BIGINT UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `isExhibit` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `isPrivate` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    CONSTRAINT `PK_Folders` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "Files"                                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `Files` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `folderID` BIGINT UNSIGNED NOT NULL,
    `createdBy` BIGINT UNSIGNED NOT NULL,
    `sourceUserID` BIGINT UNSIGNED,
    `name` VARCHAR(500) NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `isExhibit` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `isPrivate` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `isTranscript` TINYINT(1) NOT NULL DEFAULT 0,
    CONSTRAINT `PK_Files` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `IDX_Files_1` ON `Files` (`isExhibit`);

CREATE INDEX `IDX_Files_2` ON `Files` (`isPrivate`);

# ---------------------------------------------------------------------- #
# Add table "DepositionAttendees"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `DepositionAttendees` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `depositionID` BIGINT UNSIGNED NOT NULL,
    `userID` BIGINT UNSIGNED,
    `name` VARCHAR(255),
    `email` VARCHAR(255),
    `banned` TIMESTAMP NULL,
    CONSTRAINT `PK_DepositionAttendees` PRIMARY KEY (`ID`),
    CONSTRAINT `TUC_DepositionAttendees_1` UNIQUE (`depositionID`, `userID`),
    CONSTRAINT `TUC_DepositionAttendees_2` UNIQUE (`depositionID`, `email`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "Clients"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `Clients` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `typeID` CHAR(3) NOT NULL,
    `resellerID` BIGINT UNSIGNED,
    `name` VARCHAR(255) NOT NULL,
    `startDate` DATE NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deactivated` TIMESTAMP DEFAULT NULL NULL,
    `deleted` TIMESTAMP DEFAULT NULL NULL,
    `contactName` VARCHAR(255) NOT NULL,
    `contactPhone` VARCHAR(40) NOT NULL,
    `contactEmail` VARCHAR(255) NOT NULL,
    `description` TEXT,
    `logo` VARCHAR(255),
    `bannerColor` VARCHAR(40),
    `URL` VARCHAR(255) COMMENT 'this is unique part of URL for reseller, ex. if value is "reseller2" than following URL will became available: edepo.com/reseller2',
    `address1` VARCHAR(255) NOT NULL,
    `address2` VARCHAR(255),
    `city` VARCHAR(255) NOT NULL,
    `state` CHAR(2) NOT NULL,
    `ZIP` VARCHAR(10) NOT NULL,
    `includedAttendees` INTEGER UNSIGNED NOT NULL DEFAULT 0,
    `attendeeBundleSize` INTEGER UNSIGNED NOT NULL DEFAULT 0,
    CONSTRAINT `PK_Clients` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE UNIQUE INDEX `IDX_Clients_URL` ON `Clients` (`URL`);

# ---------------------------------------------------------------------- #
# Add table "CaseManagers"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `CaseManagers` (
    `caseID` BIGINT UNSIGNED NOT NULL,
    `userID` BIGINT UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_CaseManagers` PRIMARY KEY (`caseID`, `userID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "DepositionAssistants"                                       #
# ---------------------------------------------------------------------- #

CREATE TABLE `DepositionAssistants` (
    `depositionID` BIGINT UNSIGNED NOT NULL,
    `userID` BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (`depositionID`, `userID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "UserRoles"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `UserRoles` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `userID` BIGINT UNSIGNED NOT NULL,
    `roleID` INTEGER UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_UserRoles` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'check this video https://saritasa.webex.com/saritasa/ldr.php?AT=pb&SP=MC&rID=28306447&rKey=e8e6140d68eb5c2d';

# ---------------------------------------------------------------------- #
# Add table "PricingModelOptions"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `PricingModelOptions` (
    `clientID` BIGINT UNSIGNED NOT NULL,
    `optionID` INTEGER UNSIGNED NOT NULL,
    `price` DECIMAL(10,2) NOT NULL DEFAULT 0,
    `typeID` CHAR(3) NOT NULL DEFAULT 'O',
    CONSTRAINT `PK_PricingModelOptions` PRIMARY KEY (`clientID`, `optionID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "PasswordResetRequests"                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `PasswordResetRequests` (
    `hash` VARCHAR(32) NOT NULL,
    `userID` BIGINT UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `finished` TIMESTAMP DEFAULT NULL COMMENT 'when password was actually changed by this request' NULL,
    CONSTRAINT `PK_PasswordResetRequests` PRIMARY KEY (`hash`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "APISessions"                                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `APISessions` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `sKey` VARCHAR(255) NOT NULL,
    `userID` BIGINT UNSIGNED,
    `attendeeID` BIGINT UNSIGNED,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_APISessions` PRIMARY KEY (`ID`),
    CONSTRAINT `TUC_APISessions_1` UNIQUE (`sKey`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `IDX_APISessions_1` ON `APISessions` (`created` ASC);

# ---------------------------------------------------------------------- #
# Add table "FileShares"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `FileShares` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `depositionID` BIGINT UNSIGNED NOT NULL,
    `createdBy` BIGINT UNSIGNED NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fileID` BIGINT UNSIGNED NOT NULL,
    `copyFileID` BIGINT UNSIGNED,
    `userID` BIGINT UNSIGNED,
    `attendeeID` BIGINT UNSIGNED,
    CONSTRAINT `PK_FileShares` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "AuditLog"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `AuditLog` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(255),
    `actionID` VARCHAR(3) NOT NULL,
    `relatedTo` BIGINT UNSIGNED NOT NULL,
    `entityTypeID` VARCHAR(3) NOT NULL,
    `createdBy` BIGINT UNSIGNED,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `PK_AuditLog` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "Invoices"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `Invoices` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` TIMESTAMP NOT NULL,
    `clientID` BIGINT UNSIGNED NOT NULL,
    `startDate` DATE NOT NULL,
    `endDate` DATE NOT NULL,
    CONSTRAINT `PK_Invoices` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Add table "InvoiceCharges"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `InvoiceCharges` (
    `ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `clientID` BIGINT UNSIGNED NOT NULL,
    `childClientID` BIGINT UNSIGNED,
    `invoiceID` BIGINT UNSIGNED,
    `caseID` BIGINT UNSIGNED,
    `depositionID` BIGINT UNSIGNED,
    `price` DECIMAL(10,2) NOT NULL,
    `optionID` INTEGER UNSIGNED NOT NULL,
    `quantity` INTEGER NOT NULL DEFAULT 1,
    `created` TIMESTAMP NOT NULL,
    CONSTRAINT `PK_InvoiceCharges` PRIMARY KEY (`ID`)
)
 ENGINE=InnoDB DEFAULT CHARSET=utf8;

# ---------------------------------------------------------------------- #
# Foreign key constraints                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `Cases` ADD CONSTRAINT `Users_Cases` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Cases` ADD CONSTRAINT `Clients_Cases` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`);

ALTER TABLE `Cases` ADD CONSTRAINT `LookupCaseTypes_Cases` 
    FOREIGN KEY (`typeID`) REFERENCES `LookupCaseTypes` (`typeID`);

ALTER TABLE `Users` ADD CONSTRAINT `Clients_Users` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`);

ALTER TABLE `Users` ADD CONSTRAINT `LookupUserTypes_Users` 
    FOREIGN KEY (`typeID`) REFERENCES `LookupUserTypes` (`typeID`);

ALTER TABLE `Depositions` ADD CONSTRAINT `Cases_Depositions` 
    FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE;

ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Depositions` ADD CONSTRAINT `LookupDepositionStatuses_Depositions` 
    FOREIGN KEY (`statusID`) REFERENCES `LookupDepositionStatuses` (`statusID`);

ALTER TABLE `Depositions` ADD CONSTRAINT `Depositions_Depositions_Linked` 
    FOREIGN KEY (`parentID`) REFERENCES `Depositions` (`ID`) ON DELETE SET NULL;

ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions_Owner` 
    FOREIGN KEY (`ownerID`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Depositions` ADD CONSTRAINT `Users_Depositions_Speaker` 
    FOREIGN KEY (`speakerID`) REFERENCES `Users` (`ID`) ON DELETE SET NULL;

ALTER TABLE `Folders` ADD CONSTRAINT `Depositions_Folders` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

ALTER TABLE `Folders` ADD CONSTRAINT `Users_Folders` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Files` ADD CONSTRAINT `Folders_Files` 
    FOREIGN KEY (`folderID`) REFERENCES `Folders` (`ID`) ON DELETE CASCADE;

ALTER TABLE `Files` ADD CONSTRAINT `Users_Files` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE RESTRICT;

ALTER TABLE `Files` ADD CONSTRAINT `Users_Files_Source` 
    FOREIGN KEY (`sourceUserID`) REFERENCES `Users` (`ID`) ON DELETE SET NULL;

ALTER TABLE `DepositionAttendees` ADD CONSTRAINT `Users_DepositionAttendees` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `DepositionAttendees` ADD CONSTRAINT `Depositions_DepositionAttendees` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

ALTER TABLE `Clients` ADD CONSTRAINT `LookupClientTypes_Clients` 
    FOREIGN KEY (`typeID`) REFERENCES `LookupClientTypes` (`typeID`);

ALTER TABLE `Clients` ADD CONSTRAINT `Clients_Clients` 
    FOREIGN KEY (`resellerID`) REFERENCES `Clients` (`ID`);

ALTER TABLE `CaseManagers` ADD CONSTRAINT `Cases_CaseManagers` 
    FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE `CaseManagers` ADD CONSTRAINT `Users_CaseManagers` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `DepositionAssistants` ADD CONSTRAINT `Depositions_DepositionAssistants` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

ALTER TABLE `DepositionAssistants` ADD CONSTRAINT `Users_DepositionAssistants` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `RolePermissions` ADD CONSTRAINT `Roles_RolePermissions` 
    FOREIGN KEY (`roleID`) REFERENCES `Roles` (`ID`);

ALTER TABLE `RolePermissions` ADD CONSTRAINT `Permissions_RolePermissions` 
    FOREIGN KEY (`permissionID`) REFERENCES `Permissions` (`ID`);

ALTER TABLE `UserRoles` ADD CONSTRAINT `Roles_UserRoles` 
    FOREIGN KEY (`roleID`) REFERENCES `Roles` (`ID`);

ALTER TABLE `UserRoles` ADD CONSTRAINT `Users_UserRoles` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `PricingModelOptions` ADD CONSTRAINT `LookupPricingOptions_PricingModelOptions` 
    FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`);

ALTER TABLE `PricingModelOptions` ADD CONSTRAINT `Clients_PricingModelOptions` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;

ALTER TABLE `PricingModelOptions` ADD CONSTRAINT `LookupPricingTerms_PricingModelOptions` 
    FOREIGN KEY (`typeID`) REFERENCES `LookupPricingTerms` (`typeID`);

ALTER TABLE `PasswordResetRequests` ADD CONSTRAINT `Users_PasswordResetRequests` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `APISessions` ADD CONSTRAINT `Users_APISessions` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `APISessions` ADD CONSTRAINT `DepositionAttendees_APISessions` 
    FOREIGN KEY (`attendeeID`) REFERENCES `DepositionAttendees` (`ID`) ON DELETE CASCADE;

ALTER TABLE `FileShares` ADD CONSTRAINT `Files_FileShares` 
    FOREIGN KEY (`fileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE;

ALTER TABLE `FileShares` ADD CONSTRAINT `Users_FileShares` 
    FOREIGN KEY (`userID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `FileShares` ADD CONSTRAINT `DepositionAttendees_FileShares` 
    FOREIGN KEY (`attendeeID`) REFERENCES `DepositionAttendees` (`ID`) ON DELETE CASCADE;

ALTER TABLE `FileShares` ADD CONSTRAINT `Depositions_FileShares` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

ALTER TABLE `FileShares` ADD CONSTRAINT `Users_FileShares_Creator` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `FileShares` ADD CONSTRAINT `Files_FileShares_Copy` 
    FOREIGN KEY (`copyFileID`) REFERENCES `Files` (`ID`) ON DELETE CASCADE;

ALTER TABLE `AuditLog` ADD CONSTRAINT `LookupAuditActions_AuditLog` 
    FOREIGN KEY (`actionID`) REFERENCES `LookupAuditActions` (`actionID`);

ALTER TABLE `AuditLog` ADD CONSTRAINT `Users_AuditLog` 
    FOREIGN KEY (`createdBy`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;

ALTER TABLE `AuditLog` ADD CONSTRAINT `LookupEntityTypes_AuditLog` 
    FOREIGN KEY (`entityTypeID`) REFERENCES `LookupEntityTypes` (`typeID`);

ALTER TABLE `Invoices` ADD CONSTRAINT `Clients_Invoices` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `LookupPricingOptions_InvoiceCharges` 
    FOREIGN KEY (`optionID`) REFERENCES `LookupPricingOptions` (`ID`);

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Invoices_InvoiceCharges` 
    FOREIGN KEY (`invoiceID`) REFERENCES `Invoices` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Clients_InvoiceCharges` 
    FOREIGN KEY (`clientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Cases_InvoiceCharges` 
    FOREIGN KEY (`caseID`) REFERENCES `Cases` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Depositions_InvoiceCharges` 
    FOREIGN KEY (`depositionID`) REFERENCES `Depositions` (`ID`) ON DELETE CASCADE;

ALTER TABLE `InvoiceCharges` ADD CONSTRAINT `Clients_InvoiceCharges_Child` 
    FOREIGN KEY (`childClientID`) REFERENCES `Clients` (`ID`) ON DELETE CASCADE;
