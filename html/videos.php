<?php

require( './Application/_engine/classes/edvideos.php' );

if( basename( $_SERVER['REQUEST_URI'] ) ) {
	$media = './videos/' . basename( strtolower( $_SERVER['REQUEST_URI'] ) );
	if( is_file( $media ) ) {
		header( 'Content-Type: video/mp4' );
		if( isset( $_SERVER['HTTP_RANGE'] ) ) {
			\ClickBlocks\EDVideos::rangeDownload( $media );
		} else {
			\ClickBlocks\EDVideos::readfileChunked( $media );
		}
	}
}
