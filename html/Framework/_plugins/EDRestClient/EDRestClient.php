<?php

class EDRestClient {
	protected $URL;
	protected $curlHandle;

	public $info;
	public $error;
	public $errno;
	public $lastResponse;
	public $verbose;


	public function __construct( $URL=NULL, $userPwd=NULL, $jsonQuery=NULL )
	{
		$this->verbose = FALSE;
		$parsedURL = parse_url( $URL );
		if( $parsedURL && is_array( $parsedURL ) ) {
			$this->URL = \ClickBlocks\Utils::buildURL( $parsedURL );
			$this->curlHandle = curl_init( $this->URL );
		} else {
			$this->curlHandle = curl_init();
		}
		curl_setopt( $this->curlHandle, CURLOPT_USERAGENT, "EDRestClient/1.0 ({$_SERVER['SERVER_NAME']}) PHP/" . PHP_VERSION . " {$_SERVER['SERVER_SOFTWARE']}" );
		curl_setopt( $this->curlHandle, CURLOPT_TIMEOUT, 60 );
		curl_setopt( $this->curlHandle, CURLOPT_FAILONERROR, TRUE );
		curl_setopt( $this->curlHandle, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt( $this->curlHandle, CURLINFO_HEADER_OUT, TRUE );
		curl_setopt( $this->curlHandle, CURLOPT_FOLLOWLOCATION, TRUE );
		curl_setopt( $this->curlHandle, CURLOPT_POST, FALSE );
		curl_setopt( $this->curlHandle, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $this->curlHandle, CURLOPT_SSL_VERIFYHOST, FALSE );
	}

	public function setOption( $opt, $value )
	{
		if( $opt === CURLOPT_URL ) {
			$this->URL = $value;
		}
		curl_setopt( $this->curlHandle, $opt, $value );
	}

	public function setOptions( Array $opts )
	{
		if( isset( $opts[CURLOPT_URL] ) ) {
			$this->URL = $opts[CURLOPT_URL];
		}
		curl_setopt_array( $this->curlHandle, $opts );
	}

	public function exec()
	{
		$this->error = $this->errno = NULL;
		if( !$this->curlHandle || !$this->URL ) {
			return FALSE;
		}

		if( $this->verbose ) {
			\ClickBlocks\Debug::ErrorLog( "[EDRestClient] URL: {$this->URL}" );
		}

		$this->lastResponse = curl_exec( $this->curlHandle );
		$this->info = curl_getinfo( $this->curlHandle );
		$this->error = curl_error( $this->curlHandle );
		$this->errno = curl_errno( $this->curlHandle );

		if( $this->verbose ) {
			\ClickBlocks\Debug::ErrorLog( '[EDRestClient] Info: ' . print_r( $this->info, TRUE ) );
			\ClickBlocks\Debug::ErrorLog( "[EDRestClient] ErrNo: {$this->errno}" );
			\ClickBlocks\Debug::ErrorLog( "[EDRestClient] Error: {$this->error}" );
		}

		$this->close();

		return $this->lastResponse;
	}

	public function close()
	{
		curl_close( $this->curlHandle );
		unset( $this->curlHandle );
	}
}