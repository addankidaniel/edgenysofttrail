<?php

class EDRelativityClient extends EDRestClient
{
	public $datasourceBaseURL;

	public function __construct( $URL=NULL, $userPwd=NULL, $jsonQuery=NULL )
	{
		parent::__construct( $URL, $userPwd, $jsonQuery );
		$options = [
			CURLOPT_HTTPHEADER => [
				"X-CSRF-Header: \r\nX-Frivolous-Header: 1",
				'Content-Type: application/json; charset=utf-8'
			]
		];
		if( is_string( $userPwd ) && strlen( $userPwd ) ) {
			$options[CURLOPT_USERPWD] = $userPwd;
		}
		if( is_string( $jsonQuery ) && strlen( $jsonQuery ) ) {
			$options[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen( $jsonQuery );
			$options[CURLOPT_CUSTOMREQUEST] = 'POST';
			$options[CURLOPT_POSTFIELDS] = $jsonQuery;
			\ClickBlocks\Debug::ErrorLog( "[EDRelativityClient] JSON Query: {$jsonQuery}" );
		}
		$this->setOptions( $options );
	}

	// maintain components of the datasource URI
	public function fixLocation( $baseURL )
	{
		if( stripos( $this->URL, $baseURL ) !== FALSE ) {
			return FALSE;
		}
		$urlComponents = parse_url( $this->URL );
		$baseComponents = parse_url( $baseURL );
		$urlComponents['scheme'] = $baseComponents['scheme'];
		$urlComponents['host'] = $baseComponents['host'];
		$URL = \ClickBlocks\Utils::buildURL( $urlComponents );
		$this->setOption( CURLOPT_URL, $URL );
	}
}
