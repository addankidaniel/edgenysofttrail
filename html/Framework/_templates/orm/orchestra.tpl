namespace <?php echo $namespace; ?>;

use ClickBlocks\Core,
    ClickBlocks\Cache<?php if ($namespace != 'ClickBlocks\DB') echo ',' . PHP_EOL . '    ClickBlocks\DB'; echo ';'; ?>


class <?php echo $class; ?> extends \ClickBlocks\DB\OrchestraEdepo
{
   public function __construct()
   {
      parent::__construct('<?php echo $className; ?>');
   }
   
   public static function getBLLClassName()
   {
      return '<?php echo $className; ?>';
   }

   <?php if (substr($class,10,6)=='Lookup'): ?>
   public static function getLookup()
   {
      return self::getDB()->couples('SELECT * FROM `<?php echo substr($class,10); ?>`');
   }
   <?php endif; ?>
   
}