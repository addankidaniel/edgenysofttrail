<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Exceptions;

class BLLCollection implements \Iterator, \ArrayAccess, \SeekableIterator, \Countable, \Serializable
{
   protected $config = null;
   protected $rows = array();
   protected $removed = array();
   protected $objects = array();
   protected $position = 0;
   protected $lastPosition = 0;
   protected $bllClassName= null;
   protected $field = null;
   protected $info = null;

   public function __construct(array $rows, $bllClassName)
   {
      $this->bllClassName = $bllClassName;
      $this->rows = $rows;
   }

   public function getRows()
   {
      return $this->rows;
   }

   public function count()
   {
      return count($this->rows);
   }

   public function seek($position)
   {
      $this->position = $position;
      if (!$this->valid()) throw new \Exception(err_msg('ERR_GENERAL_2', array($this->position)));
   }

   public function rewind()
   {
      $this->position = 0;
      return true;
   }

   public function current()
   {
      return $this->offsetGet($this->key(), true);
   }

   public function key()
   {
      $tmp = array_keys($this->rows);
      return $tmp[$this->position];
   }

   public function next()
   {
      $this->position++;
      return true;
   }

   public function valid()
   {
      return isset($this->rows[$this->key()]);
   }

   public function offsetSet($offset, $value)
   {
      /*if (!$this->info['insertable'] && !$alwaysRead) throw new \Exception(err_msg('ERR_NAV_2', array($this->field)));
      if (is_array($value))
      {
         if (is_null($offset)) $this->rows[] = $value;
         else $this->rows[$offset] = $value;
      }
      else if (is_a($value, $this->info['to']['bll']))
      {
         if (is_null($offset)) $this->rows[] = $value->getValues(true);
         else $this->rows[$offset] = $value->getValues(true);
      }
      else throw new \Exception(err_msg('ERR_DAL_3', array($field)));
      if (!is_null($offset)) $this->lastPosition = $offset;
      {
         end($this->rows);
         $this->lastPosition = key($this->rows);
      }
      if (isset($this->removed[$offset])) unset($this->removed[$offset]);
      if (isset($this->objects[$offset])) unset($this->objects[$offset]);*/
   }

   public function offsetExists($offset)
   {
      return isset($this->rows[$offset]);
   }

   public function offsetUnset($offset)
   {
      unset($this->rows[$offset]);
      unset($this->objects[$offset]);
   }

   public function offsetGet($offset)
   {
      $this->lastPosition = $offset;
      if ($this->info['output'] == 'raw') return $this->rows[$offset];
      if (isset($this->objects[$offset])) return $this->objects[$offset];
      $bll = new $this->bllClassName();
      $bll->assign((array)$this->rows[$offset]);
      $this->objects[$offset] = $bll;
      return $bll;
   }

   public function __set($param, $value)
   {
      $obj = $this[$this->lastPosition];
      if (is_object($obj)) $obj->{$param} = $value;
      else $obj[$param] = $value;
   }

   public function __get($param)
   {
      $obj = $this[$this->lastPosition];
      if (is_object($obj)) return $obj->{$param};
      return $obj[$param];
   }

   public function __call($method, $params)
   {
      $obj = $this[$this->lastPosition];
      if (is_object($obj)) return call_user_func_array(array($obj, $method), $params);
      throw new \BadMethodCallException(err_msg('ERR_NAV_5', array($method, $this->field)));
   }

   public function serialize()
   {
      $data = get_object_vars($this);
      unset($data['objects']);
      unset($data['info']);
      unset($data['config']);
      unset($data['bll']);
      return serialize($data);
   }

   public function unserialize($data)
   {
      $data = unserialize($data);
      foreach ($data as $k => $v) $this->{$k} = $v;
   }

   /*public function limit($page = 0, $pagesize = 0)
   {
      if ($pagesize == 0) $rows = $this->rows;
      else $rows = array_slice(array_values($this->rows), $page * $pagesize, $pagesize);
      if ($this->info['output'] == 'object') foreach ($rows as $k => $row) $rows[$k] = $this[$k];
      return $rows;
   }*/

   public function save()
   {
      //foreach ($this->removed as $k => $item) $this->doAction($k, $item, 'delete');
      //foreach ($this->rows as $k => $row) $this->doAction($k, $row, 'save');
   }

   /*protected function execute()
   {
      
   }*/

   /*protected function doAction($key, array $row, $action)
   {
      $bll = new $this->info['to']['bll']();
      $bll->setValues($row, true);
      $flag = $bll->getDAL()->isKeyFilled();
      $fields = array_values($this->info['to']['fields']);
      foreach (array_values($this->info['from']['fields']) as $k => $field)
      {
         $bll->{$fields[$k]} = $this->bll->{$field};
      }
      $service = new $this->info['to']['service']();
      switch ($action)
      {
         case 'delete':
           $service->delete($bll);
           unset($this->removed[$key]);
           return;
         case 'save':
           if ($flag)
           {
              if ($this->info['updateable']) $service->update($bll);
           }
           else
           {
              if ($this->info['insertable']) $service->insert($bll);
           }
           break;
      }
      $this->rows[$key] = $bll->getValues(true);
      if ($this->info['output'] != 'raw') $this->objects[$key] = $bll;
   }*/
}

?>
