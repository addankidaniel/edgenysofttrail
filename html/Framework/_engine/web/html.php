<?php

namespace ClickBlocks\Web;

use ClickBlocks\Core;

/**
 * The class is designed for advanced work with html code and the subsequent generation of the page
 */
class HTML
{
   private static $instance = null;

   /**
    * An array that stores the elements of html code
    *
    * @var array
    */
   protected $html = array();

   /**
    * The class constructor
    * Defines the two elements, which will contain the code is placed in the top of the page and in the end.
    */
   private function __construct()
   {
      $this->html['top'] = array();
      $this->html['bottom'] = array();
   }

   /**
    * Returns the class object or if it does not exist, create it
    * 
    * @return \ClickBlocks\Web\HTML
    * @access public
    * @static
    */
   public static function getInstance()
   {
      if (self::$instance === null) self::$instance = new HTML();
      return self::$instance;
   }

   /**
    * Add html code of the specified type.
    * 
    * @param string $html
    * @param string $type  'top' or 'bottom'
    * @return \ClickBlocks\Web\HTML
    * @throws \Exception if this type does not exist
    * @access public
    */
   public function add($html, $type = 'top')
   {
      if (!isset($this->html[$type])) throw new \Exception(err_msg('ERR_HTML_1', array($type)));
      $this->html[$type][md5($html)] = $html;
      return $this;
   }

   /**
    * Set html code of the specified type. if $id is null then use the method add.
    *
    * @param string $html
    * @param string $id
    * @param string $type
    * @return \ClickBlocks\Web\HTML
    * @throws \Exception if this type does not exist
    * @access public
    */
   public function set($html, $id = null, $type = 'top')
   {
      if (!isset($this->html[$type])) throw new \Exception(err_msg('ERR_HTML_1', array($type)));
      if ($id === null) $this->add($html, $type);
      else $this->html[$type][$id] = $html;
      return $this;
   }

   /**
    * Get html code for a given element $id
    *
    * @param string $id
    * @param string $type
    * @return type
    * @throws \Exception if this type does not exist. Type only "top" or "bottom"
    * @access public
    */
   public function get($id, $type = 'top')
   {
	  if (!isset($this->html[$type])) throw new \Exception(err_msg('ERR_HTML_1', array($type)));
      return $this->html[$type][$id];
   }

   /**
    * Delete given element $id
    *
    * @param string $id
    * @param string $type
    * @return \ClickBlocks\Web\HTML
    * @throws \Exception if this type does not exist
    * @access public
    */
   public function delete($id, $type = 'top')
   {
	  if (!isset($this->html[$type])) throw new \Exception(err_msg('ERR_HTML_1', array($type)));
      unset($this->html[$type][$id]);
      return $this;
   }

   /**
    * Render and return html code of the specified type (top or bottom)
    *
    * @param string $type
    * @return string
    * @throws \Exception if this type does not exist
    * @access public
    */
   public function render($type = 'top')
   {
      if (!isset($this->html[$type])) throw new \Exception(err_msg('ERR_HTML_1', array($type)));
      return implode('', $this->html[$type]);
   }
   
   /**
    * Parses the file into the template
    *
    * @param string $file
    * @param \ClickBlocks\Core\Template $template
    * @return mixed 
    * @access public
    * @static
    */
   public static function parse($file, &$template)
   {
	  return foo(new XHTMLParser())->parse($file, $template);
   }
}

?>
