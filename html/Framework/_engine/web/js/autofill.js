var AutoFill = function()
{
   var cur_hovered = -1; // Track which result, if any, is highlighted
   var sub_list = false; // List of returned results to track.
   this.initialize = function(id, timeout)
   {
      var tt, ajax = new Ajax();
      if (!timeout) timeout = 300;
      ajax.options.isShowLoader = false;
      var target = controls.$(id);
      this._addEvent(id, 'keyup', function(e)
      {
         e = e || event;
         // Check for down arrow/up arrow/Enter and highlight or select the appropriate item.
         if (e.keyCode == 13 || e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 9) {
            // Check if the list is visible
            if( controls.$('list_' + id).style.display == 'none' ){
               return false;
            }
            else{
               if(!sub_list){
                  sub_list = controls.$('list_' + id).getElementsByTagName("li");
                  cur_hovered = -1;
               }
               switch(e.keyCode)
               {
               case 38: // Up key pushed 
                  if(cur_hovered >= 0) {
                     $(sub_list[cur_hovered]).removeClass('hover');
                     cur_hovered--;
                     if(cur_hovered >= 0) {
                        $(sub_list[cur_hovered]).addClass('hover');
                     }
                  }
               break;
               case 40: // Down key pressed
                  if(cur_hovered < 0) {
                     cur_hovered++;
                     $(sub_list[cur_hovered]).addClass('hover');
                  }
                  else if(cur_hovered < sub_list.length-1) {
                     $(sub_list[cur_hovered]).removeClass('hover');
                     cur_hovered++;
                     $(sub_list[cur_hovered]).addClass('hover');
                  }
               break;
               case 13: // Enter key pressed
                  if(cur_hovered >= 0 && cur_hovered <= sub_list.length-1) {
                     sub_list[cur_hovered].onclick();
                     cur_hovered = -1;
                  }
               break;
               case 9: // Tab key pressed
                  return false;
               break;
               default:
                  return false;
               }
               return false;
            }
         }
         // Make sure we are at the first item
         cur_hovered = -1;

         if (tt) clearTimeout(tt);
         tt = setTimeout(function(){
            if (!target.value.length || (typeof target._Focus == 'undefined') || !target._Focus) return;
            var callback = 'ClickBlocks\\Web\\UI\\POM\\AutoFill@' + id + '->search';
            ajax.abort(callback);
            target._XHR = ajax.max(1).doit(callback, target.value, 1);
         }, timeout);
      })._addEvent(document.body, 'click', function()
      {
         autofill.hideList(id);
         sub_list = false;
      })._addEvent(id, 'focus', function(){
        target._Focus = true;
      })._addEvent(id, 'blur', function(){
         target._Focus = false;
         if (typeof target._XHR != 'undefined') ajax.abort(target._XHR);
         target._XHR = undefined;
      })
   };

   this.showList = function(id, xx, yy)
   {
      if (xx == undefined) xx = 0;
      if (yy == undefined) yy = 0;
      var el = controls.$('list_' + id), size = controls.getSize(id), pos = controls.getPosition(id);
      var inp = controls.$(id);
      if (typeof inp._Focus == 'undefined' || !inp._Focus) return false;
      el.style.position = 'absolute';
      el.style.display = 'block';
      controls.setPosition(el, {x: pos.x + xx, y: pos.y + size.y + yy});
   };

   this.hideList = function(id)
   {
      var list = controls.$('list_' + id);
      if (list) list.style.display = 'none';
   };

   this._addEvent = function(id, type, fn)
   {
      var el = (typeof id == 'string') ? document.getElementById(id) : id;
      if (el == null) return this;
      if (el.addEventListener) el.addEventListener(type, fn, false);
      else el.attachEvent('on' + type, fn);
      return this;
   }
};

var autofill = new AutoFill();
