var InputButton = function()
{
	this.c = new Array();

	this.initialize = function( id, x, y )
	{
		var upload = document.getElementById( id );
		if( !upload ) {
		  return;
		}
		var frame = document.getElementById( 'frame_' + id );
		if( !frame ) {
			frame = document.createElement( 'iframe' );
			frame.id = frame.name = 'frame_' + id;
			frame.style.display = 'none';
			frame.setAttribute( 'onload', 'ajax.hideLoader();' );
			document.body.appendChild( frame );
		}
		this.c[id] = {'x': x || 0, 'y': y || 0};
	};
};

var inputbutton = new InputButton();