<?php

namespace ClickBlocks\Web\UI\POM;

use	ClickBlocks\Core,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers;

class AutoFillDropDownBox extends DropDownBox {

	public function __construct( $id, $value=NULL ) {
		parent::__construct( $id );
		$this->properties['tag'] = 'div';
		$this->properties['rounded'] = false;
		$this->properties['key'] = '';
	}

	public function JS() {
		if( !$this->properties['disabled'] ) {
			if( $this->ajax->isSubmit() ) {
				$p = 'parent.';
			}
			$script = "{$p}afddb.initialize('{$this->attributes['uniqueID']}');";
			if( Web\Ajax::isAction() ) {
				$this->ajax->script( $script, $this->updated, true );
			} else {
				$this->js->add( new Helpers\Script( 'afddb', null, Core\IO::url( 'common-js' ) . '/afddb.js' ), 'link' );
				$this->js->add( new Helpers\Script( 'afddb_' . $this->attributes['uniqueID'], $script ), 'foot' );
			}
		}
		return $this;
	}

	public function render() {
		if( !$this->properties['visible'] ) {
			return $this->invisible();
		}
		$this->addClass( 'autofilldropdown' );
		if( $this->properties['disabled'] ) {
			$this->addClass( 'disabled' );
		} else {
			$this->removeClass( 'disabled' );
		}
		if( !$this->hasStyle( 'width' ) ) {
			$this->addStyle( 'width', '100px' );
		}
		$maxH = '100px';
		if( $this->hasStyle( 'max-height' ) ) {
			$maxH = $this->getStyle( 'max-height' );
		}
		$uID = $this->attributes['uniqueID'];
		if( $this->properties['defaultValue'] && $this->properties['value'] == '') {
			$value = $this->properties['defaultValue'];
		} else {
			$value = $this->properties['value'] != '' ? $this->properties['value'] : current( (array)$this->properties['options'] );
		}
		$options = (is_array( $this->properties['options'] ) ) ? $this->properties['options'] : [];
		if( $this->properties['defaultValue'] != '' ) {
			$options[''] = $this->properties['defaultValue'];
		}
		preg_match( '/(\-?\d+)(.*)/', $this->getStyle( 'width' ), $match );
		$width = (int)$match[1];
		$ulWidth = ($width - 2);
		$inputWidth = ($width - 12);
		$disabled = ($this->properties['disabled'] ? 'disabled="disabled"' : '');
		$inputParams = $this->getParams();
		$htmlscValue = htmlspecialchars( $value );
		$htmlscClass = htmlspecialchars( $this->attributes['class'] );
		$htmlscStyle = htmlspecialchars( $this->attributes['style'] );
		$html = <<<EOD
		<{$this->properties['tag']} id="container_{$uID}" class="{$htmlscClass}" style="{$htmlscStyle}">
			<span id="select_{$uID}" class="arrow button" onclick="afddb.toggle('{$uID}',event);"></span>
			<input type="hidden" id="key_{$uID}" name="key_{$uID}" value="{$this->properties['key']}"/>
			<input type="text" id="val_{$uID}"  class="field" style="width:{$inputWidth}px;height:16px;" {$disabled} {$inputParams}
				onkeyup="afddb.filter('{$uID}',event);"
				onblur="afddb.instances['{$uID}'].blurTimeout = setTimeout(function(){ afddb.close('{$uID}'); }, 100);"
				value="{$htmlscValue}"/>
			<ul id="list_{$uID}" style="display:none;z-index:10;width:{$ulWidth}px;max-height:{$maxH}" class="_afddb_list" onclick="afddb.refocus('{$uID}');">
EOD;
		foreach( $options as $key => $option ) {
			if( is_array( $option ) ) {
				continue;
			}
			$htmlscOption = htmlspecialchars( $option );
			$html .= <<<EOD
				<li onclick="afddb.select('{$uID}', this.innerHTML, '{$key}');{$this->attributes['onchange']}" data-id="{$key}">{$htmlscOption}</li>
EOD;
		}
		$html .= <<<EOD
			</ul>
		</{$this->properties['tag']}>
EOD;
		return trim( $html );
	}

	protected function repaint() {
		parent::repaint();
		if( !$this->properties['visible'] ) {
			return;
		}
		$this->JS();
	}

	protected function getRepaintID() {
		return 'container_' . $this->attributes['uniqueID'];
	}

}
