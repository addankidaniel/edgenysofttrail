<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

/**
 * Class to work with the control "TEXTBOX" 
 */
class TextBox extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties.
    * 
    * @param string $id
    * @param string $value 
    * @access public
    */
   public function __construct($id, $value = null)
   {
      parent::__construct($id);
      $this->attributes['name'] = $id;
      $this->attributes['value'] = $value;
      $this->attributes['type'] = 'text';
      $this->attributes['title'] = null;
      $this->attributes['size'] = null;
      $this->attributes['maxlength'] = null;
      $this->attributes['onselect'] = null;
      $this->attributes['onchange'] = null;
      $this->attributes['autocorrect'] = null;
      $this->attributes['autocapitalize'] = null;
      $this->attributes['autocomplete'] = null;
      $this->attributes['placeholder'] = null;
      $this->properties['readonly'] = false;
      $this->properties['defaultValue'] = null;
   }

   /**
    * The method clears the attribute "value"
    *
    * @return \ClickBlocks\Web\UI\POM\TextBox 
    * @access public
    */
   public function clean()
   {
      $this->attributes['value'] = null;
      return $this;
   }

   /**
    * The method used to assign the attribute "value"
    *
    * @param string $value
    * @return \ClickBlocks\Web\UI\POM\TextBox 
    * @access public
    */
   public function assign($value)
   {
      $this->attributes['value'] = $value;
      return $this;
   }

   /**
    * Validation of control. 
    * The method passes the "value" attribute to the callback function $check.
    *
    * @param string $type
    * @param Core\IDelegate $check
    * @return mixed 
    * @access public
    */
   public function validate($type, Core\IDelegate $check)
   {
      return $check($this->attributes['value']);
   }

   /**
    * The function generates the html code control, based on established properties and attributes
    *
    * @return string 
    * @access public
    */
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      if ($this->properties['defaultValue'])
      {
         $value = addslashes(htmlspecialchars($this->properties['defaultValue']));
         $this->attributes['onblur'] .= ";if (!this.value) this.value = '" . $value . "';";
         $this->attributes['onfocus'] .= ";if (this.value == '" . $value . "') this.value = '';";
         if (!$this->attributes['value']) $this->attributes['value'] = $this->properties['defaultValue'];
      }
      $html = '<input type="'.$this->attributes['type'].'"';
      if ($this->properties['title']) $html .= ' title="'.$this->properties['title'].'"';
      if ($this->properties['disabled']) $html .= ' disabled="disabled"';
      if ($this->properties['readonly']) $html .= ' readonly="readonly"';
      $html .= $this->getParams() . ' />';
      return $html;
   }
}

?>
