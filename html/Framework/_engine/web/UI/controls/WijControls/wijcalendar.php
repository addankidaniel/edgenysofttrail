<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijCalendar extends WebControl
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['culture'] = array('', 'string');
    $this->options['monthCols'] = array(1, 'integer');
    $this->options['monthRows'] = array(1, 'integer');
    $this->options['titleFormat'] = array('MMMM yyyy', 'string');
    $this->options['showTitle'] = array(true, 'string');
    $this->options['displayDate'] = array(null, 'js');
    $this->options['dayRows'] = array(6, 'integer');
    $this->options['dayCols'] = array(7, 'integer');
    $this->options['weekDayFormat'] = array('short', 'string');
    $this->options['showWeekDays'] = array(true, 'boolean');
    $this->options['showWeekNumbers'] = array(false, 'boolean');
    $this->options['calendarWeekRule'] = array('firstDay', 'string');
    $this->options['minDate'] = array(null, 'js');
    $this->options['maxDate'] = array(null, 'js');
    $this->options['showOtherMonthDays'] = array(true, 'boolean');
    $this->options['showDayPadding'] = array(false, 'boolean');
    $this->options['selectionMode'] = array(null, 'js');
    $this->options['allowPreview'] = array(false, 'boolean');
    $this->options['allowQuickPick'] = array(true, 'boolean');
    $this->options['toolTipFormat'] = array('dddd, MMMM dd, yyyy', 'string');
    $this->options['prevTooltip'] = array('Previous', 'string');
    $this->options['nextTooltip'] = array('Next', 'string');
    $this->options['quickPrevTooltip'] = array('Quick Next', 'string');
    $this->options['prevPreviewTooltip'] = array('', 'string');
    $this->options['nextPreviewTooltip'] = array('', 'string');
    $this->options['navButtons'] = array('default', 'string');
    $this->options['quickNavStep'] = array(12, 'integer');
    $this->options['direction'] = array('horizontal', 'string');
    $this->options['easing'] = array('easeInQuad', 'string');
    $this->options['popupMode'] = array(false, 'boolean');
    $this->options['autoHide'] = array(true, 'boolean');
    $this->options['customizeDate'] = array(null, 'function');
    $this->options['title'] = array(null, 'function');
    $this->options['beforeSlide'] = array(null, 'function');
    $this->options['afterSlide'] = array(null, 'function');
    $this->options['beforeSelect'] = array(null, 'function');
    $this->options['afterSelect'] = array(null, 'function');
    $this->options['selectedDatesChanged'] = array(null, 'function');
    $this->properties['value'] = null;
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function CSS()
  {
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  public function render()
  {
    if (!$this->properties['visible']) return $this->invisible();
    $uniqueID = $this->attributes['uniqueID'];
    $this->attributes['uniqueID'] = null;
    $html = '<div id="container_' . $uniqueID . '"' . $this->getParams() . '>';
    $html .= '<input type="text" id="' . $uniqueID . '" runat="server" style="display:none;" /></div>';
    $this->attributes['uniqueID'] = $uniqueID;
    return $html;
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#container_' . $this->attributes['uniqueID'] . '\').wijcalendar(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#container_' . $this->attributes['uniqueID'] . '\').wijcalendar(\'destroy\');';
  }
}

?>