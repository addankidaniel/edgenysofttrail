<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijSuperPanel extends Panel
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['allowResize'] = array(false, 'boolean');
    $this->options['autoRefresh'] = array(false, 'boolean');
    $this->options['animationOptions'] = array(array('queue' => false, 'disabled' => false, 'duration' => 250, 'easing' => ''), 'array');
    $this->options['hScrollerActivating'] = array(null, 'function');
    $this->options['hScroller'] = array(null, 'js');
    $this->options['keyboardSupport'] = array(false, 'boolean');
    $this->options['keyDownInterval'] = array(100, 'float');
    $this->options['mouseWheelSupport'] = array(true, 'boolean');
    $this->options['bubbleScrollingEvent'] = array(true, 'boolean');
    $this->options['resized'] = array(null, 'function');
    $this->options['dragStop'] = array(null, 'function');
    $this->options['painted'] = array(null, 'function');
    $this->options['scrolling'] = array(null, 'function');
    $this->options['scroll'] = array(null, 'function');
    $this->options['scrolled'] = array(null, 'function');
    $this->options['showRounder'] = array(true, 'boolean');
    $this->options['vScrollerActivating'] = array(null, 'function');
    $this->options['vScroller'] = array(null, 'js');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function &__get($param)
  {
    if ($param == 'animationOptions') return $this->properties['animationOptions'];
    return parent::__get($param);
  }
  
  public function CSS()
  {
    parent::CSS();
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    parent::JS();
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  public function scrollTo($x, $y)
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijsuperpanel(\'scrollTo\', \'' . (int)$x . '\', \'' . (int)$y . '\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  public function hScrollTo($x)
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijsuperpanel(\'hScrollTo\', \'' . (int)$x . '\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  public function vScrollTo($y)
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijsuperpanel(\'vScrollTo\', \'' . (int)$y . '\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijsuperpanel(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijsuperpanel(\'destroy\');';
  }
}

?>