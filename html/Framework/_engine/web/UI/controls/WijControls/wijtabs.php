<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijTabs extends Panel
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['alignment'] = array('top', 'string');
    $this->options['sortable'] = array(false, 'boolean');
    $this->options['scrollable'] = array(false, 'boolean');
    $this->options['ajaxOptions'] = array(null, 'js');
    $this->options['cache'] = array(false, 'boolean');
    $this->options['cookie'] = array(null, 'js');
    $this->options['collapsible'] = array(false, 'boolean');
    $this->options['hideOption'] = array(null, 'js');
    $this->options['showOption'] = array(null, 'js');
    $this->options['disabledIndexes'] = array(array(), 'array');
    $this->options['event'] = array('click', 'string');
    $this->options['idPrefix'] = array('ui-tabs-', 'string');
    $this->options['panelTemplate'] = array('', 'string');
    $this->options['spinner'] = array('', 'string');
    $this->options['tabTemplate'] = array('', 'string');
    $this->options['add'] = array(null, 'function');
    $this->options['remove'] = array(null, 'function');
    $this->options['select'] = array(null, 'function');
    $this->options['beforeShow'] = array(null, 'function');
    $this->options['show'] = array(null, 'function');
    $this->options['load'] = array(null, 'function');
    $this->options['disable'] = array(null, 'function');
    $this->options['enable'] = array(null, 'function');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function CSS()
  {
    parent::CSS();
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    parent::JS();
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }

  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijtabs(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijtabs(\'destroy\');';
  }
}

?>