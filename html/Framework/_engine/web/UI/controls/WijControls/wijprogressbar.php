<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijProgressBar extends WebControl
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['value'] = array(null, 'float');
    $this->options['labelAlign'] = array('center', 'string');
    $this->options['fillDirection'] = array('east', 'string');
    $this->options['maxValue'] = array(100, 'float');
    $this->options['minValue'] = array(0, 'float');
    $this->options['labelFormatString'] = array('{1}%', 'string');
    $this->options['toolTipFormatString'] = array('{1}%', 'string');
    $this->options['indicatorIncrement'] = array(1, 'float');
    $this->options['indicatorImage'] = array('', 'string');
    $this->options['animationDelay'] = array(0, 'float');
    $this->options['animationOptions'] = array(array('disabled' => false, 'easing' => null, 'duration' => 500), 'array');
    $this->options['progressChanging'] = array(null, 'function');
    $this->options['beforeProgressChanging'] = array(null, 'function');
    $this->options['progressChanged'] = array(null, 'function');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function &__get($param)
  {
    if ($param == 'animationOptions') return $this->properties['animationOptions'];
    return parent::__get($param);
  }
  
  public function change($param, $value)
  {
    if ($param == 'value') {
      $this->options['value'] = $value;
      $this->ajax->script('$("#' . $this->attributes['uniqueID'] . '").wijprogressbar("option", "value", ' . $value . ')');
    }
  }
  
  public function CSS()
  {
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }

  public function render()
  {
    if (!$this->properties['visible']) return $this->invisible();
    return '<div' . $this->getParams() . '></div>';
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijprogressbar(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijprogressbar(\'destroy\');';
  }
}

?>