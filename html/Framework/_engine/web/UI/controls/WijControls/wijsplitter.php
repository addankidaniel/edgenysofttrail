<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijSplitter extends Panel
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['orientation'] = array('vertical', 'string');
    $this->options['sizing'] = array(null, 'function');
    $this->options['sized'] = array(null, 'function');
    $this->options['expand'] = array(null, 'function');
    $this->options['collapse'] = array(null, 'function');
    $this->options['expanded'] = array(null, 'function');
    $this->options['collapsed'] = array(null, 'function');
    $this->options['barZIndex'] = array(-1, 'integer');
    $this->options['showExpander'] = array(true, 'boolean');
    $this->options['splitterDistance'] = array(100, 'integer');
    $this->options['fullSplit'] = array(false, 'boolean');
    $this->options['resizeSettings'] = array(null, 'js');
    $this->options['panel1'] = array(null, 'js');
    $this->options['panel2'] = array(null, 'js');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function CSS()
  {
    parent::CSS();
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    parent::JS();
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  public function refresh()
  {
    $this->action('refresh');
  }
  
  protected function action($act)
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijsplitter(\'' . $act . '\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijsplitter(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijsplitter(\'destroy\');';
  }
}

?>