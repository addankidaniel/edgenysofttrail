<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijVideo extends WebControl
{
  public function __construct($id)
  {
    parent::__construct($id);
    $this->attributes['width'] = null;
    $this->attributes['height'] = null;
    $this->properties['theme'] = 'rocket';
    $this->properties['fullScreenButtonVisible'] = true;
    $this->properties['showControlsOnHover'] = true;
    $this->properties['source'] = null;
    $this->properties['type'] = null;
    $this->properties['codecs'] = null;
  }
  
  public function pause()
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijvideo(\'pause\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  public function play()
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijvideo(\'play\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  public function CSS()
  {
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }

  public function JS()
  {
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijvideo_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }

  public function render()
  {
    if (!$this->properties['visible']) return $this->invisible();
    $html = '<video';
    $html .= $this->getParams() . '>';
    if ($this->properties['source'])
    {
      $codecs = is_array($this->properties['codecs']) ? implode(',', $this->properties['codecs']) : $this->properties['codecs'];
      $html .= '<source src="' . htmlspecialchars($this->properties['source']) . '" type=\'' . addslashes(htmlspecialchars($this->properties['type'])) . ';codecs="' . addslashes(htmlspecialchars($codecs)) . '"\' />';
    }
    $html .= '</video>';
    return $html;
  }
  
  protected function repaint()
  {
    $this->ajax->script($this->getDestructor(), 0, true);
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }
  
  protected function getConstructor()
  {
    $options = array();
    if (!$this->properties['fullScreenButtonVisible']) $options[] = 'fullScreenButtonVisible: false';
    if (!$this->properties['showControlsOnHover']) $options[] = 'showControlsOnHover: false';
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijvideo(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijvideo(\'destroy\');';
  }
}

?>