<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijPopup extends Panel
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['ensureOutermost'] = array(false, 'boolean');
    $this->options['showEffect'] = array('show', 'string');
    $this->options['showOptions'] = array(array(), 'array');
    $this->options['showDuration'] = array(300, 'integer');
    $this->options['hideEffect'] = array('hide', 'string');
    $this->options['hideOptions'] = array(array(), 'array');
    $this->options['hideDuration'] = array(100, 'integer');
    $this->options['autoHide'] = array(false, 'boolean');
    $this->options['position'] = array('{}', 'js');
    $this->options['showing'] = array(null, 'function');
    $this->options['shown'] = array(null, 'function');
    $this->options['hiding'] = array(null, 'function');
    $this->options['hidden'] = array(null, 'function');
    $this->options['posChanged'] = array(null, 'function');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function &__get($param)
  {
    if ($param == 'position') return $this->properties['position'];
    return parent::__get($param);
  }
  
  public function CSS()
  {
    parent::CSS();
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    parent::JS();
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  public function show($position = null, $isJS = false)
  {
    if ($position)
    {
      if (!$isJS && is_array($position)) $position = json_encode($position);
    }
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijpopup(\'show\'' . ($position ? ', ' . $position : '') . ');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  public function showAt($x, $y)
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijpopup(\'show\', \'' . (int)$x . '\', \'' . (int)$y . '\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }   
  
  public function hide()
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijpopup(\'hide\')';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijpopup(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijpopup(\'destroy\');';
  }
}

?>