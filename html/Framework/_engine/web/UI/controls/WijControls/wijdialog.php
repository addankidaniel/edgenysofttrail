<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijDialog extends Panel
{
  protected $options = array();

  public function __construct($id)
  {
    parent::__construct($id);
    $this->options['captionButtons'] = array(null, 'js');
    $this->options['collapsingAnimation'] = array(null, 'js');
    $this->options['expandingAnimation'] = array(null, 'js');
    $this->options['contentUrl'] = array('', 'string');
    $this->options['minimizeZoneElementId'] = array('', 'string');
    $this->options['buttonCreating'] = array(null, 'function');
    $this->options['stateChanged'] = array(null, 'function');
    $this->options['blur'] = array(null, 'function');
    $this->options['create'] = array(null, 'function');
    $this->options['close'] = array(null, 'function');
    $this->options['open'] = array(null, 'function');
    $this->options['focus'] = array(null, 'function');
    $this->options['resizeStart'] = array(null, 'function');
    $this->options['resizeStop'] = array(null, 'function');
    $this->options['resize'] = array(null, 'function');
    $this->options['dragStart'] = array(null, 'function');
    $this->options['dragStop'] = array(null, 'function');
    $this->options['drag'] = array(null, 'function');
    $this->options['buttons'] = array(null, 'js');
    $this->options['autoOpen'] = array(true, 'boolean');
    $this->options['modal'] = array(false, 'boolean');
    $this->options['show'] = array('', 'string');
    $this->options['hide'] = array('', 'string');
    $this->options['width'] = array(null, 'integer');
    $this->options['height'] = array(null, 'integer');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
    $this->properties['theme'] = 'rocket';
  }
  
  public function CSS()
  {
    parent::CSS();
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }
  
  public function JS()
  {
    parent::JS();
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijctrl_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  public function disable()
  {
    $this->action('disable');
  }
  
  public function enable()
  {
    $this->action('enable');
  }
  
  public function pin()
  {
    $this->action('pin');
  }
  
  public function refresh()
  {
    $this->action('refresh');
  }
  
  public function toggle()
  {
    $this->action('toggle');
  }
  
  public function minimize()
  {
    $this->action('minimize');
  }
  
  public function maximize()
  {
    $this->action('mzximize');
  }
  
  public function restore()
  {
    $this->action('restore');
  }
  
  public function reset()
  {
    $this->action('reset');
  }
  
  public function open()
  {
    $this->action('open');
  }
  
  public function close()
  {
    $this->action('close');
  }
  
  protected function action($act)
  {
    $script = '$(\'#' . $this->attributes['uniqueID'] . '\').wijdialog(\'' . $act . '\');';
    if (Web\Ajax::isAction()) $this->ajax->script($script);
    else $this->js->add(new Helpers\Script(null, $script), 'foot');
  }
  
  protected function repaint()
  {
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }

  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijdialog(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#' . $this->attributes['uniqueID'] . '\').wijdialog(\'destroy\');';
  }
}

?>