<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WijSlider extends WebControl
{
  protected $options = null;

  public function __construct($id, $value = null)
  {
    parent::__construct($id);
    $this->properties['theme'] = 'rocket';
    $this->options['buttonMouseOver'] = array(null, 'function');
    $this->options['buttonMouseOut'] = array(null, 'function');
    $this->options['buttonMouseDown'] = array(null, 'function');
    $this->options['buttonMouseUp'] = array(null, 'function');
    $this->options['buttonClick'] = array(null, 'function');
    $this->options['change'] = array(null, 'function');
    $this->options['dragFill'] = array(true, 'boolean');
    $this->options['minRange'] = array(0, 'integer');
    $this->options['orientation'] = array('horizontal', 'string');
    $this->options['range'] = array(false, 'boolean');
    $this->options['min'] = array(0, 'float');
    $this->options['max'] = array(100, 'float');
    $this->options['step'] = array(1, 'float');
    $this->options['value'] = array($value, 'array');
    foreach ($this->options as $option => $value) $this->properties[$option] = $value[0];
  }
  
  public function &__get($param)
  {
    if ($param == 'value') return $this->properties['value'];
    return parent::__get($param);
  }

  public function CSS()
  {
    $this->css->add(new Helpers\Style('wijstyles', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/jquery.wijmo-open.css'), 'link');
    $this->css->add(new Helpers\Style('wijtheme', null, Core\IO::url('framework') . '/web/js/jquery/wijmo/' . $this->properties['theme'] . '/jquery-wijmo.css'), 'link');
    return $this;
  }

  public function JS()
  {
    $this->js->addTool('wijmo');
    if (!$this->properties['visible']) return $this;
    if (Web\Ajax::isAction()) $this->ajax->script($this->getConstructor());
    else $this->js->add(new Helpers\Script('wijmemo_' . $this->attributes['uniqueID'], $this->getConstructor()), 'foot');
    return $this;
  }
  
  public function clean()
  {
    $this->properties['value'] = null;
    return $this;
  }

  public function assign($value)
  {
    if ($value != '' && $value[0] == '[' || $value[strlen($value) - 1] == ']')
    {
      if (($r = json_decode($value, true)) !== null) $value = $r;
    }
    if (is_array($value))
    {
      if (count($value) == 0) $value = null;
      else if (count($value) == 1) $value = reset($value);
    }
    $this->properties['value'] = $value;
    return $this;
  }

  public function validate($type, Core\IDelegate $check)
  {
    switch ($type)
    {
       case 'required':
         return $check($this->properties['value']);
    }
    return true;
  }
  
  public function render()
  {
    if (!$this->properties['visible']) return $this->invisible();
    $uniqueID = $this->attributes['uniqueID'];
    $this->attributes['uniqueID'] = null;
    $html = '<div id="container_' . $uniqueID . '"' . $this->getParams() . '>';
    $html .= '<input type="text" id="' . $uniqueID . '" runat="server" style="display:none;" /></div>';
    $this->attributes['uniqueID'] = $uniqueID;
    return $html;
  }
  
  protected function repaint()
  {
    $this->ajax->script($this->getDestructor(), 0, true);
    parent::repaint();
    $this->ajax->script($this->getConstructor());
  }
  
  protected function getRepaintID()
  {
    return 'container_' . $this->attributes['uniqueID'];
  }
  
  protected function remove($time = 0)
  {
    $this->ajax->script($this->getDestructor(), $time, true);
    return parent::remove($time);
  }
  
  protected function getConstructor()
  {
    $options = array();
    foreach ($this->options as $option => $value)
    {
      $val = $this->properties[$option];
      if ($option == 'change') $val .= 'function(event, ui){' . $this->properties['change'] . ';$(\'#' . $this->attributes['uniqueID'] . '\').val(JSON.stringify(ui.value' . ($this->properties['range'] ? 's' : '') . '));}';
      if ($value[0] != $val)
      {
        switch ($value[1])
        {
          default:
          case 'string';
            $options[] = $option . ': \'' . addslashes($val) . '\'';
            break;
          case 'boolean':
            $options[] = $option . ': ' . ((bool)$val ? 'true' : 'false');
            break;
          case 'integer':
          case 'float':
            $options[] = $option . ': ' . $val;
            break;
          case 'array':
            if ($option == 'value') 
            {
              if (is_array($val)) $options[] = $option . 's : ' . json_encode($val);
              else $options[] = $option . ': ' . $val;
            }
            else $options[] = $option . ': ' . json_encode($val);
            break;
          case 'function':
          case 'js':
            $options[] = $option . ': ' . ($val === null ? 'null' : $val);
            break;
          case 'list':
            $tmp = array();
            foreach ($val as $k => $v)
            {
              $tmp[] = '{label: \'' . addslashes($v) . '\', value: \'' . addslashes($k) . '\'}';
            }
            if (count($tmp)) $options[] = 'listItems: [' . implode(',', $tmp) . ']';
            break;
        }
      }
    }
    $options = $options ? '{' . implode(', ', $options) . '}' : '';
    return '$(\'#container_' . $this->attributes['uniqueID'] . '\').wijslider(' . $options . ');';
  }
  
  protected function getDestructor()
  {
    return '$(\'#container_' . $this->attributes['uniqueID'] . '\').wijslider(\'destroy\');';
  }
}

?>