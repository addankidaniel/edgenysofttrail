<?php

namespace ClickBlocks\Web\UI\POM;

/**
 * Class to work with the control "TEXTBUTTON". default: <input type="button"...
 */
class TextButton extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties.
    * $type - type of <input />
    *
    * @param string $id
    * @param string $value
    * @param string $type 
    */
   public function __construct($id, $value = null, $type = 'button')
   {
      parent::__construct($id);
      $this->attributes['name'] = $id;
      $this->attributes['value'] = $value;
      $this->attributes['type'] = $type;
      $this->attributes['src'] = null;
   }
   
   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string 
    */
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      $html = '<input';
      if ($this->properties['disabled']) $html .= ' disabled="disabled"';
      $html .= $this->getParams() . ' />';
      return $html;
   }
}

?>
