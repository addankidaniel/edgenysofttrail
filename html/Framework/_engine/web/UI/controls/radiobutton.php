<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core;

/**
 * Class to work with the control "RADIOBUTTON"
 */
class RadioButton extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties
    *
    * @param string $id
    * @param string $value
    * @param string $caption 
    */
   public function __construct($id, $value = null, $caption = null)
   {
      parent::__construct($id);
      $this->attributes['name'] = $id;
      $this->attributes['value'] = $value;
      $this->attributes['onchange'] = null;
      $this->properties['align'] = 'right';
      $this->properties['checked'] = false;
      $this->properties['caption'] = $caption;
      $this->properties['readonly'] = false;
      $this->properties['tagContainer'] = 'span';
      $this->properties['classContainer'] = null;
      $this->properties['styleContainer'] = null;
	  $this->properties['classCaption'] = null;
      $this->properties['styleCaption'] = null;
   }

   /**
    * Set the property "checked" is false
    *
    * @return \ClickBlocks\Web\UI\POM\RadioButton 
    * @access public
    */
   public function clean()
   {
      $this->properties['checked'] = false;
      return $this;
   }

   /**
    * Sets the value for the radiobutton control.
    * Options for the values ​​of $value.
    * String, boolean - set the property "checked"
    * Array - set the attribute "value" and the property "checked"
    *
    *
    * @param mixed $value
    * @return \ClickBlocks\Web\UI\POM\RadioButton 
    * @access public
    */
   public function assign($value)
   {
      if (is_array($value))
      {
         $this->attributes['value'] = $value['value'];
         $this->properties['checked'] = (bool)$value['state'];
      }
      else if (is_bool($value)) $this->properties['checked'] = $value;
      else $this->properties['checked'] = ($this->attributes['value'] == $value);
      return $this;
   }

   /**
    * Validation of control. 
    * If the type is specified as "required", attributes "value" is passed to the callback function $check
    *
    * @param string $type
    * @param Core\IDelegate $check
    * @return mixed 
    * @access public
    */
   public function validate($type, Core\IDelegate $check)
   {
      switch ($type)
      {
         case 'required':
           return $check($this->attributes['value']);
      }
      return true;
   }

   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string 
    * @access public
    */
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      if ($this->properties['caption'] != '') $label = '<label for="' . $this->attributes['uniqueID'] . '" style="' . htmlspecialchars($this->properties['styleCaption']) . '" class="' . htmlspecialchars($this->properties['classCaption']) . '">' . $this->properties['caption'] . '</label>';
      $html = '<' . $this->properties['tagContainer'] . ' id="container_' . $this->attributes['uniqueID'] . '" style="' . htmlspecialchars($this->properties['styleContainer']) . '" class="' . htmlspecialchars($this->properties['classContainer']) . '">';
      if ($this->properties['align'] == 'left') $html .= $label;
      $html .= '<input type="radio"';
      if ($this->properties['checked']) $html .= ' checked="checked"';
      if ($this->properties['disabled']) $html .= ' disabled="disabled"';
      if ($this->properties['readonly']) $html .= ' readonly="readonly"';
      $html .= $this->getParams() . ' />';
      if ($this->properties['align'] == 'right') $html .= $label;
      $html .= '</' . $this->properties['tagContainer'] . '>';
      return $html;
   }

   /**
    * Returns the ID of the control that you want to repaint
    *
    * @return string 
    * @access protected
    */
   protected function getRepaintID()
   {
      return 'container_' . $this->attributes['uniqueID'];
   }
}

?>
