<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class DateTimePicker extends TextBox
{
	public function __construct( $id, $value=NULL )
	{
		parent::__construct( $id, $value );
		$this->properties['theme'] = NULL;
		$this->properties['classButton'] = NULL;
		$this->properties['styleButton'] = NULL;
		$this->properties['classContainer'] = NULL;
		$this->properties['styleContainer'] = NULL;
		$this->properties['options'] = [];
		$this->properties['tagContainer'] = 'span';
		$this->attributes['maxlength'] = '22';
		$this->properties['dateFormat'] = NULL;
	}

	public function &__get($param)
	{
		if ($param == 'options') return $this->properties['options'];
		return parent::__get($param);
	}

	public function CSS()
	{
		if (!$this->properties['visible']) return;
		if( !Web\Ajax::isAction() ) {
			$this->css->add( new Helpers\Style( 'general', null, '/Application/_includes/backend/css/jquery-ui-1.11.0.min.css' ), 'link' );
		}
		return $this;
	}

	public function JS()
	{
		if( $this->properties['visible'] ) {
			if( Web\Ajax::isAction() ) {
				$this->ajax->script( $this->getConstructor(), $this->updated, TRUE );
			} else {
				$this->js->addTool( 'ui' );
				$this->js->add( new Helpers\Script( 'datetimepickerinit_' . $this->attributes['uniqueID'], $this->getConstructor() ), 'foot' );
			}
		}
		return $this;
	}

	public function render()
	{
		if (!$this->properties['visible']) return $this->invisible();
		$html = '<' . $this->properties['tagContainer'] . ' id="container_' . $this->attributes['uniqueID'] . '" style="' . htmlspecialchars($this->properties['styleContainer']) . '" class="' . htmlspecialchars($this->properties['classContainer']) . '">';
		$html .= '<input type="text"';
		if ($this->properties['disabled']) $html .= ' disabled="disabled"';
		if ($this->properties['readonly']) $html .= ' readonly="readonly"';
		$html .= $this->getParams() . ' />';
		$html .= '</' . $this->properties['tagContainer'] . '>';
		return $html;
	}

	protected function repaint()
	{
		parent::repaint();
		$this->CSS();
		$this->JS();
	}

	protected function getRepaintID()
	{
		return 'container_' . $this->attributes['uniqueID'];
	}

	private function getConstructor()
	{
		$options = [];
		if( $this->properties['dateFormat'] ) {
			$options['dateFormat'] = $this->properties['dateFormat'];
		}
		$optionPairs = explode( ',', $this->properties['options'] );
		foreach( $optionPairs as $optionPair ) {
			list( $key, $value ) = explode( ':', $optionPair );
			$options[$key] = $value;
		}
		$jsonOptions = json_encode( $options, JSON_UNESCAPED_SLASHES );
		return "$(function() { $( '#{$this->attributes['uniqueID']}' ).datepicker( {$jsonOptions} ); });";
	}
}
