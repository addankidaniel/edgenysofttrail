<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

/**
 * All Items in Repeater have 2 types of indexes:
 * - real item ID
 * - order index
 * By default, they are the same and start from 0, 1, 2, .. and so on
 * But if you change order of items by overwriting order property, or if you delete any item except last one, it will change
 * order index ($n in methods like put(), deleteItem()) is always 0-based order index
 * real item ID will never change. It is used to get item by short ID, 
 * for example $page->get('questions.question_2') will return item with real index 2
 * if you delete item before item 2 in order, question_2 will remain at same ID, but it's index will decrement by 1 
 */
class Repeater extends Panel
{
   private $section = null;

   /**
    *
    * @param type $id 
    * @access public
    */
   public function __construct($id)
   {
      parent::__construct($id);
      $this->properties['sections'] = new Utils\ArraySortOrder();
      $this->properties['count'] = 0;
      $this->properties['order'] = array();
      $this->properties['source'] = array();
      $this->properties['autoincrement'] = 0;
   }

   /**
    *
    * @param string $uniqueID
    * @return \ClickBlocks\Web\UI\POM\Repeater|boolean 
    * @access public
    */
   public function offsetGet($uniqueID)
   {
      if ($this->section === null)
      {
         $ctrl = $this->getByUniqueID($uniqueID);
         if ($ctrl !== false) return $ctrl;
         if ((string)$uniqueID == (string)(int)$uniqueID)
         {
            $this->section = $uniqueID;
            return $this;
         }
         return false;
      }
      $ctrl = $this->get($uniqueID . '_' . $this->section);
      $this->section = null;
      return $ctrl;
   }

   /**
    *
    * @param string $param
    * @param string $value
    * @return type 
    * @access public
    */
   public function __set($param, $value)
   {
      if ($param == 'source' || $param == 'sections') return;
      if ($param == 'count') $this->setCount($value);
      else if ($param == 'order')
      {
         $this->properties['sections']->setOrder($value);
         $this->properties['order'] = $this->properties['sections']->getOrder();
      }
      else parent::__set($param, $value);
   }

   /**
    *
    * @param string $param
    * @return mixed 
    * @access public
    */
   public function __get($param)
   {
      if ($param == 'order') $this->properties['order'] = $this->properties['sections']->getOrder();
      return parent::__get($param);
   }

   /**
    * 
    * 
    * @access public
    */
   public function init()
   {
      $count = $this->properties['count'];
      $this->properties['count'] = 0;
      for ($i = 0; $i < $count; $i++) $this->putItem($i);
   }

   /**
    *
    * @param IWebControl $ctrl
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function add(IWebControl $ctrl)
   {
      if (Web\XHTMLParser::isParsing())
      {
         $this->properties['source'][] = $ctrl->uniqueID;
         parent::add($ctrl);
         $this->controls[$ctrl->uniqueID] = false;
         if ($ctrl instanceof IValidator) $this->page->lockValidator($ctrl->uniqueID, false);
         return $this;
      }
      return parent::add($ctrl);
   }

   /**
    * Put the last in repeater
    *
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function putLast()
   {
      $this->putItem($this->properties['count']);
      return $this;
   }

   /**
    * Put the first in repeater
    *
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function putFirst()
   {
      $this->putItem(0);
      return $this;
   }

   /**
    * Placed before the element with index $ n
    *
    * @param int $n
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function putBefore($n)
   {
      $this->putItem($n - 1);
      return $this;
   }

   /**
    * Placed after the element with index $n
    *
    * @param int $n
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function putAfter($n)
   {
      $this->putItem($n);
      return $this;
   }

   /**
    * The method puts the item with the index $n in the repeater.
    *
    * @param int $n
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function putItem($n)
   {
      if ($n > count($this->properties['sections'])) $n = count($this->properties['sections']);
      if ($n < 0) $n = 0;
      $ids = array();
      $realIndex = $this->properties['autoincrement']++;
      foreach ($this->properties['source'] as $uniqueID)
      {
         $ctrl = $this->getByUniqueID($uniqueID)->copy();
         if ($ctrl instanceof Repeater) $ctrl->init();
         $ctrl->id .= '_' . $realIndex;
         if (isset($ctrl->name)) $ctrl->name .= '_' . $realIndex;
         $ids[$uniqueID] = $ctrl->uniqueID;
         parent::add($ctrl);
      }
      foreach ($ids as $uniqueID)
      {
         $ctrl = $this->getByUniqueID($uniqueID);
         if ($ctrl instanceof IValidator)
         {
            $controls = $ctrl->controls;
            $parentFullID = $this->getFullID();
            foreach ($controls as $cID)
            {
               $fullID = $this->page->getByUniqueID($cID)->getFullID();
               $fullID = substr($fullID, strpos($fullID, $parentFullID) + strlen($parentFullID) + 1);
               $obj = $this->get($fullID);
               if ($obj !== false)
               {
                  $controls[] = $ids[$obj->uniqueID];
                  $key = array_search($obj->uniqueID, $this->properties['source']);
                  unset($controls[array_search($this->properties['source'][$key], $controls)]);
               }
            }
            $ctrl->controls = $controls;
         }
      }
      $this->properties['sections']->inject($n, $ids, $realIndex);
      $this->properties['count']++;
      $this->properties['order'] = $this->properties['sections']->getOrder();
      return $this;
   }

   /**
    * Remove the first item of the repeater
    *
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function deleteFirst()
   {
      $this->deleteItem(0);
      return $this;
   }

   /**
    * Remove the last item of the repeater
    *
    * @return \ClickBlocks\Web\UI\POM\Repeater 
    * @access public
    */
   public function deleteLast()
   {
      $this->deleteItem($this->properties['count'] - 1);
      return $this;
   }

   /**
    * Remove the element with index $n
    *
    * @param int $n
    * @param bool $isRealIndex if true, $n will specify real section ID instead of order index
    * @return \ClickBlocks\Web\UI\POM\Repeater
    * @access public 
    */
   public function deleteItem($n, $isRealIndex = false)
   {
      $realIndex = $isRealIndex ? $n : $this->properties['order'][$n];
      foreach ((array)$this->properties['sections'][$realIndex] as $uniqueID)
      {
         $this->getByUniqueID($uniqueID)->delete();
      }
      unset($this->properties['sections'][$realIndex]);
      $this->properties['order'] = $this->properties['sections']->getOrder();
      $this->properties['count']--;
      return $this;
   }

   /**
    * 
    *
    * @return string
    * @access public
    */
   public function getInnerHTML()
   {
      $template = (string)$this->tpl;
      foreach ($this->properties['order'] as $realIndex)
      {
         $ids = (array)$this->properties['sections'][$realIndex];
         $this->tpl = strtr($template, $ids);
         foreach ($ids as $uniqueID) {
            $ctrl = $this->getByUniqueID($uniqueID);
            $this->tpl->{$uniqueID} = $ctrl->render();
            $ctrl->setRerendered();
         }
         $this->tpl->count = $realIndex;
         $this->tpl->uniqueID = $this->attributes['uniqueID'];
         $html .= $this->tpl->render();
      }
      $this->tpl = $template;
      return $html;
   }

   /**
    *
    * @return string
    * @access public
    */
   public function getXHTML()
   {
      $tag = strtolower(Utils\PHPParser::getClassName(get_class($this)));
      $xml = '<' . $tag . $this->getXHTMLParams() . '>';
      $temp = (string)$this->tpl;
      foreach ($this->controls as $uniqueID => $flag) if (!$flag) $temp = str_replace('<?=$' . $uniqueID . ';?>', $this->getByUniqueID($uniqueID)->getXHTML(), $temp);
      $xml .= $temp;
      return $xml .= '</' . $tag . '>';
   }

   /**
    *
    * @param int $count
    * @access protected
    */
   public function setCount($count)
   {
      if (!Web\XHTMLParser::isParsing())
      {
         if ($count < 0) $count = 0;
         if ($count < $this->properties['count']) for ($i = $this->properties['count']; $i >= $count; $i--) $this->deleteItem($i);
         else if ($count > $this->properties['count']) for ($i = $this->properties['count'] + 1; $i <= $count; $i++) $this->putItem($i);
      }
      $this->properties['count'] = $count;
      return $this;
   }

   /**
    *
    * @return string
    * @access protected 
    */
   protected function getXHTMLParams()
   {
      $source = $this->properties['source'];
      $sections = $this->properties['sections'];
      $this->properties['source'] = '';
      $this->properties['sections'] = '';
      $params = parent::getXHTMLParams();
      $this->properties['source'] = $source;
      $this->properties['sections'] = $sections;
      return $params;
   }
   
   /**
    * Returns real index of last added item
    * @return int real index of item or boolean FALSE if there were no items
    */
   public function getLastID()
   {
      if ($this->properties['autoincrement'] == 0) return FALSE;
      return (int)$this->properties['autoincrement'] - 1;
   }
}

?>