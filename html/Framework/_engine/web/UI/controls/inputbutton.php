<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
	ClickBlocks\Web\UI\Helpers;

class InputButton extends WebControl
{
	/**
	 * Class constructor
	 * Sets the initial attributes and properties
	 *
	 * <inputbutton id="btnUploadOverwrite" type="file" class="upload_btn" />
	 * 
	 * @param string $id
	 * @param string $value
	 * @access public
	 */
	public function __construct( $id, $value=NULL )
	{
		parent::__construct( $id );
		$this->attributes['name'] = $id;
		$this->attributes['value'] = $value;
		$this->attributes['type'] = 'button';
		$this->attributes['onchange'] = NULL;
	}

	/**
	 * The method clears the attribute "value"
	 *
	 * @return \ClickBlocks\Web\UI\POM\InputButton
	 * @access public
	 */
	public function clean()
	{
		$this->attributes['value'] = null;
		return $this;
	}

	/**
	 * The method used to assign the attribute "value"
	 *
	 * @param string $value
	 * @return \ClickBlocks\Web\UI\POM\InputButton
	 * @access public
	 */
	public function assign($value)
	{
		$this->attributes['value'] = $value;
		return $this;
	}

	/**
	 * Validation of control.
	 * The method passes the "value" attribute to the callback function $check.
	 *
	 * @param string $type
	 * @param Core\IDelegate $check
	 * @return mixed
	 * @access public
	 */
	public function validate($type, Core\IDelegate $check)
	{
		return $check($this->attributes['value']);
	}

	/**
	 * Creates the html code of the control on the basis of established properties and attributes
	 *
	 * @return string
	 * @access public
	 */
	public function render()
	{
		$this->attributes['name'] = $this->attributes['uniqueID'];
		if( !$this->properties['visible'] ) return $this->invisible();
		$html = '<input ' . $this->getParams() . ' />';
		if( strtolower( $this->attributes['type'] ) == 'file' ) {
			$html = '<form>' . $html . '</form>';
		}
		return $html;
	}

	public function JS()
	{
		if( !$this->properties['visible'] ) return;
		if( strtolower( $this->attributes['type'] ) == 'file' ) {
			$this->js->addTool( 'inputbutton' );
			$this->js->add( new Helpers\Script( 'inputbutton_' . $this->attributes['uniqueID'], 'setTimeout(function(){inputbutton.initialize(\'' . $this->attributes['uniqueID'] . '\');},100)' ), 'foot' );
		}
	}
}
