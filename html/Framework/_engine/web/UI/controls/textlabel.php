<?php

namespace ClickBlocks\Web\UI\POM;

/**
 * Class to work with the control "TEXTLABEL"
 */
class TextLabel extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties.
    *
    * @param string $id
    * @param string $text
    * @param string $for 
    */
   public function __construct($id, $text = null, $for = null)
   {
      parent::__construct($id);
      $this->attributes['for'] = $for;
      $this->properties['text'] = $text;
      $this->properties['tag'] = 'label';
   }
   
   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string 
    */
   public function render()
   {
	  $for = $this->attributes['for'];
	  if ($this->attributes['for'] != '')
	  {
		 $ctrl = $this->page->get($this->attributes['for']);
		 if ($ctrl !== false) $this->attributes['for'] = $ctrl->uniqueID;
	  }
      if (!$this->properties['visible']) return $this->invisible();
      $html = '<' . $this->properties['tag'] . $this->getParams() . '>' . $this->properties['text'] . '</' . $this->properties['tag'] . '>';
      $this->attributes['for'] = $for;
      return $html;
   }
}

?>
