<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core;

/**
 * Class to work with the control "MEMO"
 */
class Memo extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties
    *
    * @param string $id
    * @param string $text
    * @param int $cols
    * @param int $rows 
    * @access public
    */
   public function __construct($id, $text = null, $cols = null, $rows = null)
   {
      parent::__construct($id);
      $this->attributes['name'] = $id;
      $this->attributes['cols'] = $cols;
      $this->attributes['rows'] = $rows;
      $this->attributes['wrap'] = null;
      $this->attributes['onselect'] = null;
      $this->attributes['onchange'] = null;
      $this->attributes['autocorrect'] = null;
      $this->attributes['autocapitalize'] = null;
      $this->properties['readonly'] = false;
      $this->properties['text'] = $text;
      $this->attributes['placeholder'] = '';
   }

   /**
    * The method clears the properties "text"
    *
    * @return \ClickBlocks\Web\UI\POM\Memo 
    * @access public
    */
   public function clean()
   {
      $this->properties['text'] = null;
      return $this;
   }

   /**
    * The method used to assign the properties "text"
    *
    * @param string $value
    * @return \ClickBlocks\Web\UI\POM\Memo
    * @access public 
    */
   public function assign($value)
   {
      $this->properties['text'] = $value;
      return $this;
   }

   /**
    * Validation of control. 
    * The method passes the "text" properties to the callback function $check.
    *
    * @param string $type
    * @param Core\IDelegate $check
    * @return mixed 
    * @access public
    */
   public function validate($type, Core\IDelegate $check)
   {
      return $check($this->properties['text']);
   }

   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string 
    * @access public
    */
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      $html = '<textarea';
      if ($this->properties['disabled']) $html .= ' disabled="disabled"';
      if ($this->properties['readonly']) $html .= ' readonly="readonly"';
      $html .= $this->getParams() . '>' . htmlspecialchars($this->properties['text']) . '</textarea>';
      return $html;
   }
}

?>
