<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers;

class MenuButton extends WebControl
{
	public function __construct( $id )
	{
		parent::__construct( $id );
		$this->attributes['class'] = 'blue';
		$this->properties['label'] = '';
		$this->properties['options'] = [];
		//$this->attributes['options'] = [['label'=>'Edit','href'=>'/edit/'],['label'=>'Download','href'=>'/download/'],['label'=>'Exhibit Log','href'=>'/exhibitlogs/']];
	}

	public function render()
	{
		if( !$this->properties['visible'] ) return $this->invisible();
		$html .= '<div ';
		if( $this->properties['disabled'] ) {
			$this->attributes['class'] = str_replace('blue', 'gray', $this->attributes['class'] );
			$html .= ' disabled="disabled"';
		}
		$html .= $this->getParams() . ' />';
		$html .= '<ul onclick="menubutton.hide();">';
		foreach( $this->properties['options'] as $label => $option ) {
			$href = (isset( $option['href'] ) && $option['href'] ) ? $option['href'] : 'javascript:void(0);';
			$onclick = (isset( $option['onclick'] ) && $option['onclick'] ) ? " onclick=\"{$option['onclick']}\"" : '';
			$html .= '<li><a href="'.$href.'"'.$onclick.'><div>'.$label.'</div></a></li>';
		}
		$html .= '</ul>';
		$html .= '<a href="javascript:void(0);" onclick="menubutton.toggle(this);"><div><span>'.$this->properties['label'] . '<img width="14" height="14" src="/Application/_includes/backend/img/2downarrow.png" border="0"/></span></div></a>';
		$html .= '</div>';
		return $html;
	}

	public function JS() {
		if( !$this->properties['disabled'] ) {
			$script = "$('#{$this->attributes['uniqueID']}').click( function(event){ event.stopPropagation(); });";
			if( Web\Ajax::isAction() ) {
				$this->ajax->script( $script, $this->updated, TRUE );
			} else {
				$this->js->add( new Helpers\Script( 'menubutton', NULL, Core\IO::url( 'common-js' ) . '/menubutton.js' ), 'link' );
				$this->js->add( new Helpers\Script( "menubtn_{$this->attributes['uniqueID']}", $script ), 'foot' );
			}
		}
		return $this;
	}
}
