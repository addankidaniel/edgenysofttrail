<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core;

/**
 * Class to work with the control "MEMO"
 */
class Hidden extends WebControl
{
   /**
    * Class constructor
    * Sets the initial attributes and properties
    *
    * @param string $id
    * @param string $value 
    * @access public
    */
   public function __construct($id, $value = null)
   {
      parent::__construct($id);
      $this->attributes['name'] = $id;
      $this->attributes['value'] = $value;
   }
   
   /**
    * The method clears the attribute "value"
    *
    * @return \ClickBlocks\Web\UI\POM\Hidden 
    * @access public
    */
   public function clean()
   {
      $this->attributes['value'] = null;
      return $this;
   }
   
   /**
    * The method used to assign the attribute "value"
    *
    * @param string $value
    * @return \ClickBlocks\Web\UI\POM\Hidden 
    * @access public
    */
   public function assign($value)
   {
      $this->attributes['value'] = $value;  
      return $this;
   }
   
   /**
    * Validation of control. 
    * The method passes the "value" attribute to the callback function $check.
    *
    * @param string $type
    * @param Core\IDelegate $check
    * @return mixed 
    * @access public
    */
   public function validate($type, Core\IDelegate $check)
   {
      return $check($this->attributes['value']);
   }
   
   /**
    * Creates the html code of the control on the basis of established properties and attributes
    *
    * @return string 
    * @access public
    */
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      return '<input type="hidden"' . $this->getParams() . ' />';
   }
}

?>
