<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to generate <script>...</script> with the set parameters and properties
 * <br>Класс расширяет Control и предназначен для генерации <script>...</script> с установленными параметрами и свойствами
 */
class Script extends Control
{
   /**
    *  The class constructor
    * <br>Конструктор класса
    * 
    * @param string $id
    * @param string $script
    * @param string $src 
    * @access public
    */
   public function __construct($id = null, $script = null, $src = null)
   {
      parent::__construct($id);
      $this->attributes['type'] = 'text/javascript';
      $this->attributes['charset'] = null;
      $this->attributes['src'] = $src;
      $this->properties['text'] = $script; 
   }
   
   /**
    * Generates the html code to insert a script on the page
    * <br>Генерирует html код для вставки скрипта на страницу
    * 
    * @return string 
    * @access public
    */
   public function render()
   {
      return '<script' . $this->getParams() . '>' . $this->properties['text'] . '</script>'; 
   }
}

?>
