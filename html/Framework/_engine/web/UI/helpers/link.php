<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to generate the html code tag <a>...</a>
 * <br>Класс расширяет Control и предназначен для генерации html кода тега <a>...</a> 
 */
class Link extends Control
{
   /**
    * The class constructor
    * <br>Конструктор класса
    * 
    * @param string $id
    * @param string $href
    * @param string $text 
    */
   public function __construct($id = null, $href = null, $text = null)
   {
      parent::__construct($id);
      $this->attributes['type'] = null;
      $this->attributes['href'] = $href;
      $this->attributes['target'] = null;
      $this->attributes['hreflang'] = null;
      $this->properties['text'] = $text;
   }

   /**
    * Generates the html code to link taking into account all the attributes set
    * <br>Генерирует html код для ссылок учитывая все установленные атрибуты
    * 
    * @return string
    */
   public function render()
   {
      return '<a' . $this->getParams() . '>' . $this->properties['text'] . '</a>';
   }
}

?>
