<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Controll and is designed to generate all the html page 
 * using the specified attributes and properties
 * <br>Класс расширяет Controll и предназначен для генерации всей html страницы 
 * используя установленные атрибуты и свойства
 */
class XHTML extends Control
{
   /**
    * The contents of the tag head
    * Содержимое тега head
    * 
    * @var string 
    * @access public
    */
   public $head = null;
   
   /**
    * The contents of the body tag
    * Содержимое тега body
    * 
    * @var string
    * @access public 
    */
   public $body = null;

   /**
    * The class constructor
    * <br>Конструктор класса
    * 
    * @param string $id 
    * @access public
    */
   public function __construct($id = null)
   {
      parent::__construct($id);
      $this->attributes['xmlns'] = 'http://www.w3.org/1999/xhtml';
      $this->properties['doctype'] = 'transitional'; // may be also "strict", "frameset" nad "html".
   }
   
   /**
    * Generates full html code page
    * <br>Генерирует полный html код страницы
    * 
    * @return string 
    */
   public function render()
   {
      switch ($this->properties['doctype'])
      {
         default;
         case 'strict':
           $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
           break;
         case 'transitional':
           $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
           break;
         case 'frameset':
           $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
           break;
         case 'html':
           $html = '<!DOCTYPE html>';
           break;
      }  
      $body = $this->body->render();
      return $html . '<html' . $this->getParams() . ">\n" . $this->head->render() . $body . "\n</html>";
   }
}

?>
