<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to display static text 
 * in the tag set with the set parameters (the default span)
 * <br> Класс расширяет Control и предназначен для вывода статичного текста 
 * в установленном теге с установленными параметрами (по умолчанию span)
 */
class StaticText extends Control
{
   /**
    * The class constructor
    * <br>Конструктор класса
    * 
    * @param string $id
    * @param string $text 
    */
   public function __construct($id = null, $text = null)
   {
      parent::__construct($id);      
      $this->properties['text'] = $text;
      $this->properties['tag'] = 'span';
   }

   /**
    * Generates the html code to display the text in the tag set with the set parameters
    * <br>Генерирует html код для вывода текста в установленном теге с установленными параметрами
    * 
    * @return string
    * @access public 
    */
   public function render()
   {
      return '<' . $this->properties['tag'] . $this->getParams() . '>' . $this->properties['text'] . '</' . $this->properties['tag'] . '>';
   }
}

?>
