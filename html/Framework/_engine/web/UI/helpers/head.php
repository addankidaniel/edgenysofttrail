<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to generate content tags <head>...</head>
 * <br>Класс расширяет Control и предназначен для генерации содержимого тегов <head>...</head>
 */
class Head extends Control
{
   protected $meta = array();
   protected $link = [];
   protected $js = null;
   protected $css = null;

   /**
    * The class constructor
    * <br>Конструктор класса
    *
    * @param string $name site title
    */
   public function __construct($name = null)
   {
      parent::__construct();
      $this->js = Web\JS::getInstance();
      $this->css = Web\CSS::getInstance();
      $this->attributes['profile'] = null;
      $this->properties['name'] = $name;
      $this->properties['icon'] = null;
   }

   /**
    * Adds metadata to header
    * <br>Добавляет метаданные в шапку сайта
    *
    * @param Meta $obj
    * @return \ClickBlocks\Web\UI\Helpers\Head
    */
   public function addMeta(Meta $obj)
   {
      $this->meta[$obj->id] = $obj;
      return $this;
   }

   /**
    * Sets the metadata for the $id
    * <br>Устанавливает метаданные для $id
    *
    * @param string $id
    * @param Meta $obj
    * @return \ClickBlocks\Web\UI\Helpers\Head
    */
   public function setMeta($id, Meta $obj)
   {
      $this->meta[$id] = $obj;
      return $this;
   }

   /**
    * get the metadata for $id
    * <br>Получает метаданные по $id
    *
    * @param string $id
    * @return string
    */
   public function getMeta($id)
   {
      return $this->meta[$id];
   }

   /**
    * Remove the metadata element $id
    * <br>Удаляет метаданные элемента $id
    *
    * @param string $id
    * @return \ClickBlocks\Web\UI\Helpers\Head
    */
   public function deleteMeta($id)
   {
      unset($this->meta[$id]);
      return $this;
   }

   /**
    * Adds metadata to header
    *
    * @param Link $obj
    * @return \ClickBlocks\Web\UI\Helpers\Head
    */
   public function addLink( MetaLink $obj)
   {
      $this->link[$obj->id] = $obj;
      return $this;
   }

   /**
    * Sets the metadata for the $id
    *
    * @param string $id
    * @param Link $obj
    * @return \ClickBlocks\Web\UI\Helpers\Head
    */
   public function setLink($id, MetaLink $obj)
   {
      $this->link[$id] = $obj;
      return $this;
   }

   /**
    * get the metadata for $id
    *
    * @param string $id
    * @return string
    */
   public function getLink($id)
   {
      return $this->link[$id];
   }

   /**
    * Remove the metadata element $id
    *
    * @param string $id
    * @return \ClickBlocks\Web\UI\Helpers\Head
    */
   public function deleteLink($id)
   {
      unset($this->link[$id]);
      return $this;
   }

   /**
    * Generate html code element <head>...</head>
    * <br>Генерирует html код элемента <head>...</head>
    *
    * @return string
    */
   public function render()
   {
      if ($this->properties['icon'] != '')
      {
         $icon = new Style('headicon');
         $icon->rel = 'shortcut icon';
         $icon->type = 'image/x-icon';
         $icon->href = $this->icon;
         $this->css->add($icon, 'link');
      }
      else $this->css->delete('headicon', 'link');
      $dom = $this->js->render('domready');
      $html = '<head' . $this->getParams() . '>';
      foreach ($this->meta as $obj) $html .= $obj->render();
	  foreach ($this->link as $obj) $html .= $obj->render();
      $html .= '<title>' . htmlspecialchars($this->properties['name']) . '</title>';
      $html .= $this->css->render('link');
      $html .= $this->js->render('link');
      $html .= $this->css->render('style');
      $html .= $this->js->render('head');
      $html .= $dom;
      $html .= '</head>';
      return $html;
   }
}

?>
