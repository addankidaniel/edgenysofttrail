<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core;

interface IControl
{
   public function getParameters();
   public function setParameters(array $parameters);

   public function addClass($class);
   public function removeClass($class);
   public function replaceClass($class1, $class2);
   public function toggleClass($class1, $class2 = null);
   public function hasClass($class);

   public function addStyle($style, $value);
   public function setStyle($style, $value);
   public function getStyle($style);
   public function removeStyle($style);
   public function toggleStyle($style, $value);
   public function hasStyle($style);
}

/**
 * A general class, which extends all the controls. Provides basic functionality.
 */
abstract class Control implements IControl
{
   protected $attributes = array('id' => null,
                                 'style' => null,
                                 'class' => null,
                                 'title' => null,
                                 'lang' => null,
                                 'dir' => null,
                                 'onfocus' => null,
                                 'onblur' => null,
                                 'onclick' => null,
                                 'ondblclick' => null,
                                 'onmousedown' => null,
                                 'onmouseup' => null,
                                 'onmouseover' => null,
                                 'onmousemove' => null,
                                 'onmouseout' => null,
                                 'onkeypress' => null,
                                 'onkeydown' => null,
                                 'onkeyup' => null,
	                             'autofocus' => null);
   protected $properties = array('showID' => false);


   /**
    * The class constructor. Sets the attributes of a unique id value
    * <br>Конструктор класса. Устанавливает в атрибутах уникальное значение id
    *
    * @param string $id
    * @access public
    */
   public function __construct($id = null)
   {
      $this->attributes['id'] = ($id === null) ? uniqid('c') : $id;
   }

   /**
    * Checks whether a property or atrubut $param. If you have - will set the value to $value, otherwise it will throw an exception.
    * <br>Проверяет, есть ли такое свойство или атрубут $param. Если есть - установит ему значение $value,иначе будет вызвано исключение.
    *
    * @param string $param
    * @param string $values
    * @throws \Exception If there is no such property or attribute $param
    * @access public
    */
   public function __set($param, $value)
   {
      if (array_key_exists($param, $this->attributes)) $this->attributes[$param] = $value;
      else if (array_key_exists($param, $this->properties)) $this->properties[$param] = $value;
      else throw new \Exception(err_msg('ERR_GENERAL_3', array($param, get_class($this))));
   }

   /**
    * If an existing property or attribute of $param - it will return its value, otherwise it will throw an exception.
    * <br>Если существуе такое свойство или атрибут $param - то вернет его значение, иначе будет вызвано исключение.
    *
    * @param string $param
    * @return string
    * @throws \Exception If there is no such property or attribute $param
    * @access public
    */
   public function __get($param)
   {
      if (array_key_exists($param, $this->attributes)) return $this->attributes[$param];
      else if (array_key_exists($param, $this->properties)) return $this->properties[$param];
      else throw new \Exception(err_msg('ERR_GENERAL_3', array($param, get_class($this))));
   }

   /**
    * The function returns an array consisting of two arrays, which are the attributes and properties of param=>value
    * <br>Функция вернет массив состоящий из двух массивов, в которых расположены атрибуты и свойства param=>value
    *
    * @return array
    * @access public
    */
   public function getParameters()
   {
      return array($this->attributes, $this->properties);
   }

   /**
    * Sets attributes and properties.
    * The array $parameters shall consist of two arrays with the structure param=>value
    * <br>Устанавливает атрибуты и свойства.
    * Массив $parameters должен состоять из 2 массивов со структурой param=>value
    *
    * @param array $parameters
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function setParameters(array $parameters)
   {
      list ($this->attributes, $this->properties) = $parameters;
      return $this;
   }

   /**
    * Check whether there is such a property or attribute
    * <br>Проверка, существует ли такое свойство или атрибут
    *
    * @param string $param
    * @return bool
    * @access public
    */
   public function __isset($param)
   {
      return (array_key_exists($param, $this->attributes) || array_key_exists($param, $this->properties));
   }

   /**
    * Removes the property and attribute $param
    * <br>Удаляет свойство и атрибут $param
    *
    * @param string $param
    * @access public
    */
   public function __unset($param)
   {
      unset($this->attributes[$param]);
      unset($this->properties[$param]);
   }

   /**
    * When accessing the object as a string to try to generate html code. Otherwise, throw an exception
    * <br>При обращении к объекту как к строке попробовать сгенерировать html код. Иначе выдать исключение
    *
    * @return type
    * @throws \Exception  If can not convert to string
    * @access public
    */
   public function __toString()
   {
      try
      {
         return $this->render();
      }
      catch (\Exception $e)
      {
         Core\Debugger::exceptionHandler($e);
      }
   }

   /**
    * The abstract function. It implemented the conversion of all the parameters in the html code
    * <br>Абстрактная функция. В ней реализуется преобразование всех параметров в html код
    *
    * @access public
    * @abstract
    */
   abstract public function render();

   /**
    * Adds a class if it's not.
    * <br>Добавляет класс если его еще нет.
    *
    * @param string $class
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function addClass($class)
   {
      if (!$this->hasClass($class)) $this->class = trim($this->class . ' ' . $class);
      return $this;
   }

   /**
    * removes the class
    * <br>Удаляет класс
    *
    * @param string $class
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function removeClass($class)
   {
      $this->class = str_replace($class, '', $this->class);
      $this->class = trim(str_replace(array('  ', '   '), ' ', $this->class));
      return $this;
   }

   /**
    * Replaces $class1 on $class2
    * <br>Заменяет $class1 на $class2
    *
    * @param string $class1
    * @param string $class2
    * @access public
    */
   public function replaceClass($class1, $class2)
   {
      $this->removeClass($class1)->addClass($class2);
   }

   /**
    * Replaces one class to another
    * <br>Производит замену одного класса на другой
    *
    * @param string $class1
    * @param string $class2
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function toggleClass($class1, $class2 = null)
   {
      if (!$this->hasClass($class1)) $this->replaceClass($class2, $class1);
      else $this->replaceClass($class1, $class2);
      return $this;
   }

   /**
    * Is the such class or not.
    * <br>Установлен ли такой класс или нет.
    *
    * @param string $class
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function hasClass($class)
   {
      return (strpos($this->class, $class) !== false);
   }

   /**
    * Sets a style property with a value of $style $value. If such a property existed - it overwritten.
    * <br>Устанавливает свойство стиля $style со значением $value. Если такое свойство существовало - оно перезапишется.
    *
    * @param string $style
    * @param string $value
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function addStyle($style, $value)
   {
      if (!$this->hasStyle($style)) $this->style = trim($this->style . $style . ':' . $value . ';');
      else $this->setStyle($style, $value);
      return $this;
   }

   /**
    * Changes the value of the property at $style $value
    * <br>Изменяет значение свойства $style на $value
    *
    * @param string $style
    * @param string $value
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function setStyle($style, $value)
   {
      $this->style = preg_replace('/' . $style . ' *:[^;]*;*/', $style . ':' . $value . ';', $this->style);
      return $this;
   }

   /**
    * Returns the value of the $style style
    * <br>Вернет значение свойства стиля $style
    *
    * @param string $style
    * @return string
    */
   public function getStyle($style)
   {
      preg_match('/' . $style . ' *:([^;]*);*/', $this->style, $arr);
      return $arr[1];
   }

   /**
    * Removes a property $style
    * <br>Удаляет свойство $style
    *
    * @param string $style
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function removeStyle($style)
   {
      $this->style = preg_replace('/' . $style . ' *:[^;]*;*/', '', $this->style);
      $this->style = trim(str_replace(array('  ', '   '), ' ', $this->style));
      return $this;
   }

   /**
    * If $style is not already set - set it, or remove this property.
    * <br>Если $style еще не установлен - устанавливает его, иначе удаляет это свойство.
    *
    * @param string $style
    * @param string $value
    * @return \ClickBlocks\Web\UI\Helpers\Control
    * @access public
    */
   public function toggleStyle($style, $value)
   {
      if (!$this->hasStyle($style)) $this->addStyle($style, $value);
      else $this->removeStyle($style);
      return $this;
   }

   /**
    * Established whether such a style property or not.
    * <br>Установлено ли такое свойство стиля или нет.
    *
    * @param string $style
    * @return bool
    * @access public
    */
   public function hasStyle($style)
   {
      return (strpos($this->style, $style) !== false);
   }

   /**
    * Generates a string with the defined attributes element
    * <br>Генерирует строку с установленными атребутами элемента
    *
    * @return string
    * @access protected
    */
   protected function getParams()
   {
      $tmp = array(); $attr = $this->attributes;
      if (!$this->showID) unset($attr['id']);
      foreach ($attr as $k => $v) if ($v != '') $tmp[] = $k . '="' . htmlspecialchars($v) . '"';
      return (count($tmp)) ? ' ' . implode(' ', $tmp) : '';
   }
}

?>
