<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and enables you to generate html code css styles
 * <br>Класс расширяет Control и позволяет генерировать html код css стилей
 */
class Style extends Control
{
   /**
    * class constructor
    * <br>конструктор класса
    * 
    * @param string $id
    * @param string $style
    * @param string $href
    * @access public
    */
   public function __construct($id = null, $style = null, $href = null)
   {
      parent::__construct($id);
      $this->attributes['type'] = 'text/css';
      $this->attributes['charset'] = null;
      $this->attributes['href'] = $href;
      $this->attributes['hreflang'] = null;
      $this->attributes['rel'] = null;
      $this->attributes['rev'] = null;
      $this->attributes['target'] = null;
      $this->attributes['media'] = null;
      $this->properties['text'] = $style;
      $this->properties['conditions'] = null;
   }

   /**
    * Generates the html code to insert css style or connection file with style.
    * <br>Генерирует html код для вставки css стиля либо подключения файла со стилем.
    * 
    * @return string 
    */
   public function render()
   {
      if (strlen($this->attributes['href']))
      {
         if (!$this->attributes['rel']) $this->attributes['rel'] = 'stylesheet';
         if (!$this->properties['conditions']) return '<link' . $this->getParams() . ' />';
         return '<!--[' . $this->properties['conditions'] . ']><link ' . $this->getParams() . ' /><![endif]-->';
      }
      return '<style' . $this->getParams() . '>' . $this->properties['text'] . '</style>';
   }
}

?>
