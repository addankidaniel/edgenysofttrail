<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to generate html code to insert an iframe on the page using the specified properties
 * <br>Класс расширяет Control и предназначен для генерации html кода для вставки iframe на страницу используя установленные свойства
 */
class IFrame extends Control
{
   public function __construct($id = null)
   {
      parent::__construct($id);
      $this->attributes['name'] = $id;
      $this->attributes['src'] = null;
      $this->attributes['onload'] = null;
      $this->attributes['width'] = null;
      $this->attributes['height'] = null;
      $this->attributes['scrolling'] = null;
      $this->attributes['align'] = null;
      $this->attributes['marginwidth'] = null;
      $this->attributes['marginheight'] = null;
      $this->attributes['frameborder'] = null;
      $this->attributes['allowTransparency'] = null;
   }

   /**
    * Generates the html code <iframe></iframe> with all parameters set
    * <br>Генерирует html код <iframe></iframe> со всеми установленными параметрами
    * 
    * @return string 
    */
   public function render()
   {
      return '<iframe' . $this->getParams() . '></iframe>';
   }
}

?>
