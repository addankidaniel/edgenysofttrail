<?php

namespace ClickBlocks\Web\UI\Helpers;

use ClickBlocks\Core,
    ClickBlocks\Web;

/**
 * The class extends Control and is designed to generate metadata
 * <br>Класс расширяет Control и предназначен для генерации метаданных
 */
class Meta extends Control
{
   /**
    * The class constructor
    * <br>Конструктор класса
    * 
    * @param string $id
    * @param string $name
    * @param string $content
    * @param string $httpequiv 
    */
   public function __construct($id = null, $name = null, $content = null, $httpequiv = null)
   {
      parent::__construct($id);
      $this->attributes['name'] = $name;
      $this->attributes['content'] = $content;
      $this->attributes['scheme'] = null;
      $this->attributes['http-equiv'] = $httpequiv;      
   }
   
   /**
    * Generates the html code tag <meta/> with the established attributes
    * <br>Генерирует html код тега <meta/> с установленными параметрами
    * 
    * @return string 
    */
   public function render()
   {
      return '<meta' . $this->getParams() . ' />';
   }
}

?>
