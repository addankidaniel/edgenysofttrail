<?php

namespace ClickBlocks\Web\UI\POM;

class ValidatorEmail extends ValidatorRegularExpression {
   const EMAIL_REG_EXP = '/^[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+(\.[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)+(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|law|pro|tel|travel|online|lawyer|attorney|solutions|[a-z][a-z])$/i';

   public function __construct( $id, $message=NULL ) {
      parent::__construct( $id, $message );
      $this->type = 'email';
      $this->properties['expression'] = self::EMAIL_REG_EXP;
   }
}
