<?php

namespace ClickBlocks\Web;

use ClickBlocks\Core,
    ClickBlocks\Web\UI\Helpers;

/**
 * The class allows you to set specific styles for the elements and generate the html code of these styles
 * <br>Класс позволяет устанавливать определенные стили для элементов и генерировать html код этих стилей
 */
class CSS
{
    private static $instance = null;
    
    protected $css = array();

   /**
    * class constructor
    * <br>конструктор класса
    * 
    * @access private
    */
   private function __construct()
   {
      $this->css['link'] = array();
      $this->css['style'] = array();
   }

   /**
    * Returns the class object or if it does not exist, create it
    * <br>Возвращает объект класса или, если его нет, создает его
    *
    * @return \ClickBlocks\Web\CSS 
    * @access public
    * @static
    */
   public static function getInstance()
   {
      if (self::$instance === null) self::$instance = new CSS();
      return self::$instance;
   }

   /**
    * Assigns an element $obj->id $obj style settings
    * <br>Присваивает для элемента $obj->id настройки стиля $obj
    *
    * @param Helpers\Style $obj
    * @param string $type
    * @return \ClickBlocks\Web\CSS
    * @throws \Exception  If $type is not valid
    * @access public
    */
   public function add(Helpers\Style $obj, $type = 'link')
   {
      $config = Core\Register::getInstance()->config;
      if ($config['staticFilesExpired'] && $type == 'link') $obj->href .= (strrpos($obj->href, '?') === false ? '?' : '&') . 'id=' . $config['staticFilesExpired'];
      if (!isset($this->css[$type])) throw new \Exception(err_msg('ERR_CSS_1', array($type)));
      $this->css[$type][$obj->id] = $obj;
      return $this;
   }

   /**
    * Sets the element $id style settings $obj
    * <br>Устанавливает для выбранного элемента $id настройки стиля $obj
    * 
    * @param string $id
    * @param Helpers\Style $obj
    * @param string $type
    * @return \ClickBlocks\Web\CSS
    * @throws \Exception If $type is not valid
    * @access public
    */
   public function set($id, Helpers\Style $obj, $type = 'link')
   {
      if (!isset($this->css[$type])) throw new \Exception(err_msg('ERR_CSS_1', array($type)));
      $this->css[$type][$id] = $obj;
      return $this;
   }

   /**
    * Gets all the style settings for a given item id
    * <br>Получает все настройки стиля для заданного элемента ID
    * 
    * @param string $id
    * @param string $type type of css (link or style)
    * @return object
    * @throws \Exception If $type is not valid
    * @access public
    */
   public function get($id, $type = 'link')
   {
      if (!isset($this->css[$type])) throw new \Exception(err_msg('ERR_CSS_1', array($type)));
      return $this->css[$type][$id];
   }

   /**
    * Removes all style settings for a given element id 
    * <br>Удаляет все настройки стиля для заданного id элемента
    *
    * @param string $id
    * @param string $type type of css (link or style)
    * @return \ClickBlocks\Web\CSS
    * @throws \Exception  If $type is not valid
    * @access public
    */
   public function delete($id, $type = 'link')
   {
      if (!isset($this->css[$type])) throw new \Exception(err_msg('ERR_CSS_1', array($type)));
      unset($this->css[$type][$id]);
      return $this;
   }

   /**
    * creates html code to insert css style or css file
    * <br>создает html код для вставки css стиля либо файла css
    *
    * @param string $type type of css (link or style)
    * @return string
    * @throws \Exception If $type is not valid
    * @access public
    */
   public function render($type = 'link')
   {
      if (!isset($this->css[$type])) throw new \Exception(err_msg('ERR_CSS_1', array($type)));
      foreach ($this->css[$type] as $obj) $html .= $obj->render();
      return $html;
   }

   /**
    * The function returns the generated style
    * <br>Функция возвращает сгенерированниый стиль
    *
    * @param string $style
    * @return string 
    * @access public
    * @static
    */
   public static function style($style)
   {
      return foo(new Helpers\Style(null, $style))->render();
   }

   /**
    * This function creates the correct link to the CSS file
    * <br>Функция создает правильную ссылку на CSS файл
    *
    * @param string $src relative or absolute path
    * @param string $charset
    * @return string
    * @access public
    * @static
    */
   public static function link($src, $charset = null)
   {
      if ($src[0] == '/') $src = Core\IO::url('css') . $src;
      $obj = new Helpers\Style();
      $obj->href = $src;
      $obj->charset = $charset;
      return $obj->render();
   }
}

?>
