<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: infoclass.php
 *
 * @category   MVC
 * @package    MVC
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */
namespace ClickBlocks\Utils;

use ClickBlocks\Core;

/**
 * The class is designed to work with the source code of classes, receive and retrieve information about the class
 *
 * Класс предназначен для работы с исходным кодом классов, получения и извлечения информации о классе
 *
 * @category   Helper
 * @package    Core
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @version    Release: 1.0.0
 */
class InfoClass
{
   /**
    * Array of information about the class
    *
    * Массив информации о классе
    *
    * @var array $info
    * @access private
    */
   private $info = array();

   /**
    * Array of strings, and file information on them
    *
    * Массив строк файла и информации по ним
    *
    * @var array $lines
    * @access private
    */
   private $lines = array();

   /**
    * The name of the current class
    *
    * Имя текущего класса
    *
    * @var string $current
    * @access private
    */
   private $current = null;

   /**
    * Constructor of this class.
    *
    * Конструктор класса.
    *
    * @access public
    */
   public function __construct($name = null)
   {
      if (is_file($name)) $this->parse($name);
      else if ($name)
      {
         if (!is_object($name) || !get_class($name)) $this->parseCode($name);
         else $this->extraction($name);
      }
   }

   /**
    * Parser PHP Files
    *
    * Парсер PHP Файлов
    *
    * @param string $file
    * @return object
    * @access public
    */
   public function parse($file)
   {
      if (!is_file($file)) throw new \RuntimeException('File "' . $file . '" is not found.');
      require_once($file);
      $this->extractAll($this->extractFileContent($file));
      return $this;
   }

   /**
    * Parser PHP code
    *
    * Парсер PHP кода
    *
    * @param string $code
    * @param string $key
    * @return object
    * @access public
    */
   public function parseCode($code, $key = null)
   {
      Core\Loader::getInstance()->load($code);
      $this->extractContentFromCode($code, $key);
      $this->extractAll($code, $key);
      return $this;
   }

   /**
    * Retrieves the property $info
    *
    * Извлекает данные в свойство $info
    *
    * @param mixed $class
    * @param string $key
    * @access public
    */
   public function extraction($class, $key = null)
   {
      if (is_object($class)) $class = get_class($class);
      $info = array();
      $class = new \ReflectionClass($class);
      $parent = $class->getParentClass();
      $info['file'] = $class->getFileName();
      if (is_file($info['file']))
      {
         $key = $info['file'];
         $this->extractFileContent($info['file']);
      }
      list ($info['name'], $info['shortName'], $info['namespace']) = $this->getClassNames($class);
      $info['comment'] = $class->getDocComment();
      $info['startLine'] = $class->getStartLine();
      $info['endLine'] = $class->getEndLine();
      $info['isAbstract'] = $class->isAbstract();
      $info['isFinal'] = $class->isFinal();
      $info['isInterface'] = $class->isInterface();
      $info['isInternal'] = $class->isInternal();
      $info['isUserDefined'] = $class->isUserDefined();
      $info['isInstantiable'] = $class->isInstantiable();
      $info['isIterateable'] = $class->isIterateable();
      $info['inNamespace'] = $class->inNamespace();
      list ($info['parentName'], $info['parentShortName'], $info['parentNamespace']) = (is_object($parent)) ? $this->getClassNames($parent) : array('', '', '');
      $info['extension'] = $class->getExtensionName();
      $info['interfaces'] = array();
      foreach ($class->getInterfaces() as $name => $interface)
      {
         if ($parent instanceof \ReflectionClass && $parent->implementsInterface($name)) continue;
         list ($info['interfaces'][$name]['name'], $info['interfaces'][$name]['shortName'], $info['interfaces'][$name]['namespace']) = $this->getClassNames($interface);
      }
      $info['constants'] = array();
      foreach ($class->getConstants() as $name => $constant)
      {
         if ($parent instanceof \ReflectionClass && $parent->hasConstant($name)) continue;
         $info['constants'][$name] = self::getPHPType($constant);
      }
      $info['properties'] = array();
      $defValues = $class->getDefaultProperties();
      foreach ($class->getProperties() as $property)
      {
         $name = $property->getName();
         if ($parent instanceof \ReflectionClass && $parent->hasProperty($name)) continue;
         $info['properties'][$name]['isStatic'] = $property->isStatic();
         $info['properties'][$name]['isPublic'] = $property->isPublic();
         $info['properties'][$name]['isProtected'] = $property->isProtected();
         $info['properties'][$name]['isPrivate'] = $property->isPrivate();
         $info['properties'][$name]['isDefault'] = $property->isDefault();
         $info['properties'][$name]['value'] = $this->getPHPType($defValues[$name]);
         $info['properties'][$name]['comment'] = $property->getDocComment();
      }
      $info['methods'] = array();
      foreach ($class->getMethods() as $method)
      {
         $name = $method->getName();
         if ($parent instanceof \ReflectionClass && $parent->hasMethod($name) && $method->getDeclaringClass()->getName() != $info['name']) continue;
         $info['methods'][$name]['isConstructor'] = $method->isConstructor();
         $info['methods'][$name]['isDestructor'] = $method->isDestructor();
         $info['methods'][$name]['isStatic'] = $method->isStatic();
         $info['methods'][$name]['isPublic'] = $method->isPublic();
         $info['methods'][$name]['isProtected'] = $method->isProtected();
         $info['methods'][$name]['isPrivate'] = $method->isPrivate();
         $info['methods'][$name]['isAbstract'] = $method->isAbstract();
         $info['methods'][$name]['isFinal'] = $method->isFinal();
         $info['methods'][$name]['isInternal'] = $method->isInternal();
         $info['methods'][$name]['isUserDefined'] = $method->isUserDefined();
         $info['methods'][$name]['isClosure'] = $method->isClosure();
         $info['methods'][$name]['isDeprecated'] = $method->isDeprecated();
         $info['methods'][$name]['returnsReference'] = $method->returnsReference();
         $info['methods'][$name]['comment'] = $method->getDocComment();
         $info['methods'][$name]['startLine'] = $method->getStartLine();
         $info['methods'][$name]['endLine'] = $method->getEndLine();
         $info['methods'][$name]['numberOfParameters'] = $method->getNumberOfParameters();
         $info['methods'][$name]['numberOfRequiredParameters'] = $method->getNumberOfRequiredParameters();
         $info['methods'][$name]['arguments'] = array();
         foreach ($method->getParameters() as $parameter)
         {
            $class = $parameter->getClass();
			if ($class)
			{
			   $cls = array();
			   list ($cls['name'], $cls['shortName'], $cls['namespace']) = $this->getClassNames($class);
			   $class = $cls;
			}
            $info['methods'][$name]['arguments'][$parameter->getPosition()] = array('name' => $parameter->getName(),
                                                                                    'isDefaultValueAvailable' => $parameter->isDefaultValueAvailable(),
                                                                                    'isArray' => $parameter->isArray(),
                                                                                    'isOptional' => $parameter->isOptional(),
                                                                                    'isPassedByReference' => $parameter->isPassedByReference(),
                                                                                    'allowsNull' => $parameter->allowsNull(),
                                                                                    'class' => $class ?: '',
                                                                                    'value' => ($parameter->isOptional()) ? $this->getPHPType($parameter->getDefaultValue()) : '');
         }
         $code = array();
         for ($n = $method->getStartLine() - 1; $n < $method->getEndLine(); $n++) $code[] = $this->lines[$key]['rows'][$n];
         $code = implode(PHP_EOL, $code);
         $info['methods'][$name]['code'] = self::getMethodBody($code);
      }
      $this->lines[$key]['classes'][] = $info['name'];
      $this->current = $info['name'];
      if (strlen($info['comment']))
      {
         for ($i = $info['startLine'] - 2; $i > 0; $i--)
         {
            if (trim($this->lines[$key]['rows'][$i]) != '') break;
         }
         $info['startLine'] = $i - count(explode(PHP_EOL, $info['comment'])) + 2;
      }
      $this->info[$info['name']] = new Simplex($info);
   }

   /**
    * Returns information from the property $info, at the option
    *
    * Возвращает информацию из свойства $info по указанному параметру
    *
    * @param string $param
    * @return mixed
    * @access public
    */
   public function __get($param)
   {
      if (isset($this->info[$param]))
      {
         $this->current = $param;
         return $this;
      }
      else return $this->info[$this->current]->{$param};
   }

   /**
    * Sets the value for the properties $info
    *
    * Устанавливаетт значение для элемента свойства $info
    *
    * @param string $param
    * @param mixed $value
    * @access public
    */
   public function __set($param, $value)
   {
      if (!isset($this->info[$this->current]->{$param})) throw new Exceptions\NotExistingPropertyException('Property "' . $param . '" does not exist.');
      $this->info[$this->current]->{$param} = $value;
   }

   /**
    * Verify the existence of a parameter in the property info
    *
    * Проверка существования параметра в свойстве info
    *
    * @param string $param
    * @return boolean
    * @access public
    */
   public function __isset($param)
   {
      return isset($this->info[$param]);
   }

   /**
    * Returns the source code for this class
    *
    * Возвращает исходный код указанного класса
    *
    * @param string $class
    * @return string
    * @access public
    */
   public function getCodeByClassName($class)
   {
      return $this->getCodeFile($this->info[$class]->file);
   }

   /**
    * Returns the source code for this file
    *
    * Возвращает исходный код указанного файла
    *
    * @param string $file
    * @return string
    * @access public
    */
   public function getCodeFile($file)
   {
      if (is_file($file)) $file = Core\IO::normalizeFileName($file);
      $code = array();
      $n = $k = 0; $max = count($this->lines[$file]['rows']);
      while ($n < $max)
      {
         if (!$class) $class = $this->info[$this->lines[$file]['classes'][$k]];
         if ($n == $class->startLine - 1)
         {
            $code[] = $this->getCodeClass($class->name);
            $n = $class->endLine;
            $class = '';
            $k++;
         }
         else $code[] = $this->lines[$file]['rows'][$n];
         $n++;
      }
      return implode(PHP_EOL, $code);
   }


   /**
    * Returns the source code for this class
    *
    * Возвращает исходный код указанного класса
    *
    * @param string $class
    * @return string
    * @access public
    */
   public function getCodeClass($class)
   {
      $obj = $this->info[$class];
      $code = $cls = $interfaces = $constants = $properties = $methods = array();

      if ($obj->comment) $code[] = $obj->comment;
      if ($obj->isFinal) $cls[] = 'final';
      if ($obj->isAbstract) $cls[] = 'abstract';
      if ($obj->isInterface) $cls[] = 'interface';
      else $cls[] = 'class';
      if ($obj->inNamespace) $cls[] = $obj->shortName;
      else $cls[] = $obj->name;
      if ($obj->parentName)
      {
         if ($obj->parentNamespace == $obj->namespace) $cls[] = 'extends ' . $obj->parentShortName;
         else $cls[] = 'extends \\' . $obj->parentName;
      }

      if (count($obj->interfaces)) foreach ($obj->interfaces as $interface)
      {
         if ($interface->namespace == $obj->namespace) $interfaces[] = $interface->shortName;
         else $interfaces[] = $interface->name;
      }
      if (count($interfaces))  $cls[] = 'implements ' . implode(', ', $interfaces);

      $code[] = implode(' ', $cls);
      $code[] = '{';

      if (count($obj->constants)) foreach ($obj->constants as $constant => $value) $constants[] = '   ' . $this->getCodeConstant($class, $constant);
      if (count($constants)) $code[] = implode(PHP_EOL, $constants) . PHP_EOL;

      if (count($obj->properties)) foreach ($obj->properties as $property => $value) $properties[] = $this->getCodeProperty($class, $property);
      if (count($properties)) $code[] = implode(PHP_EOL, $properties) . PHP_EOL;

      if (count($obj->methods)) foreach ($obj->methods as $method => $value) $methods[] = $this->getCodeMethod($class, $method);
      if (count($methods)) $code[] = implode(PHP_EOL . PHP_EOL, $methods);

      $code[] = '}';
      return implode(PHP_EOL, $code) . PHP_EOL;
   }

   /**
    * Returns the code definition of the constant
    *
    * Возвращает код определения константы
    *
    * @param string $class
    * @param string $constant
    * @return string
    * @access public
    */
   public function getCodeConstant($class, $constant)
   {
      if (!isset($this->info[$class]->constants->{$constant})) return '';
      return 'const ' . $constant . ' = ' . $this->info[$class]->constants->{$constant} . ';';
   }

   /**
    * Returns the code definition of the property
    *
    * Возвращает код определения свойства
    *
    * @param string $class
    * @param string $property
    * @return string
    * @access public
    */
   public function getCodeProperty($class, $property)
   {
      if (!isset($this->info[$class]->properties->{$property})) return '';
      $code = array();
      $obj = $this->info[$class]->properties->{$property};
      if ($obj->isPublic) $code[] = 'public';
      if ($obj->isProtected) $code[] = 'protected';
      if ($obj->isPrivate) $code[] = 'private';
      if ($obj->isStatic) $code[] = 'static';
      $code[] = '$' . $property;
      if ($obj->isDefault) $code[] = '= ' . $obj->value;
      return (($obj->comment) ? '   ' . $obj->comment . PHP_EOL : '') .  '   ' . implode(' ', $code) . ';';
   }


   /**
    * Returns the code of this method
    *
    * Возвращает код указанного метода
    *
    * @param string $class
    * @param string $method
    * @return string
    * @access public
    */
   public function getCodeMethod($class, $method)
   {
      if (!isset($this->info[$class]->methods->{$method})) return '';
      $code = $parameters = array();
      $obj = $this->info[$class]->methods->{$method};
      foreach ($obj->arguments as $parameter)
      {
         $param = '';
         if (!$parameter->allowsNull && isset($parameter->class->name))
         {
            if ($this->info[$class]->namespace == $parameter->class->namespace) $param .= $parameter->class->shortName . ' ';
            else $param .= $parameter->class->name . ' ';
         }
         if ($parameter->isArray) $param .= 'array ';
         $param .= (($parameter->isPassedByReference) ? '&' : '') . '$' . $parameter->name;
         if ($parameter->isDefaultValueAvailable) $param .= ' = ' . $parameter->value;
         $parameters[] = $param;
      }
      if ($obj->isFinal) $code[] = 'final';
      if ($obj->isAbstract) $code[] = 'abstract';
      if ($obj->isPublic) $code[] = 'public';
      if ($obj->isProtected) $code[] = 'protected';
      if ($obj->isPrivate) $code[] = 'private';
      if ($obj->isStatic) $code[] = 'static';
      $code[] = 'function ' . $method . '(' . implode(', ', $parameters) . ')';
      $code = '  ' . implode(' ', $code);
      if ($obj->isAbstract) $code .= ';';
      else $code .= PHP_EOL . '  {' . $obj->code . '}';
      return (($obj->comment) ? '  ' . $obj->comment . PHP_EOL : '') . $code;
   }


   /**
    * Returns an array of properties $info
    *
    * Возвращает массив свойства $info
    *
    * @return array
    * @access public
    */
   public function getInfo()
   {
      return $this->info;
   }

   /**
    * Retrieves all classes of $content
    *
    * Извлекает данные всех классов из $content
    *
    * @param string $content
    * @param string $key
    * @return array
    * @access private
    */
   private function extractAll($content, $key = null)
   {
      foreach (PHPParser::getFullClassNames($content) as $class) $this->extraction($class, $key);
   }

   /**
    * Returns an array of class names and namespaces
    *
    * Возвращает массив имен класса и пространства имен
    *
    * @param string $class
    * @return array
    * @access private
    */
   private function getClassNames(\ReflectionClass $class)
   {
      $name = $class->getShortName();
      $namespace = $class->getNamespaceName();
      if (!$namespace) $namespace = '\\';
      return array($namespace . (($namespace != '\\') ? '\\' : '') . $name, $name, $namespace);
   }

   /**
    * Retrieves an array of lines of code
    *
    * Извлекает в массив строки кода
    *
    * @param string $class
    * @param string $code
    * @access private
    */
   private function extractContentFromCode($code, $key)
   {
      if (!isset($this->lines[$key]))
      {
         $this->lines[$key]['rows'] = explode("\n", str_replace("\r", '', $code));
      }
   }

   /**
    * Retrieves an array of lines of code from a file and returns the source code file
    *
    * Извлекает в массив строки кода из файла и возвращает исходный код файла
    *
    * @param string $file
    * @return array
    * @access private
    */
   private function extractFileContent($file)
   {
      $file = Core\IO::normalizeFileName($file);
      if (!isset($this->lines[$file]))
      {
         $content = file_get_contents($file);
         $this->lines[$file]['rows'] = explode("\n", str_replace("\r", '', $content));
         return $content;
      }
   }

   /**
    * Returns the value in the format of the type to which it relates
    *
    * Возвращает значение в формате того типа к которому оно относится
    *
    * @param mixed $value
    * @return mixed
    * @access private
    */
   private static function getPHPType($value)
   {
      if (is_string($value)) return "'" . $value . "'";
      if (is_bool($value)) return ($value) ? 'true' : 'false';
      if (is_null($value)) return 'null';
      if (is_array($value))
      {
         $tmp = array();
         foreach ($value as $k => $v) $tmp[] = self::getPHPType($k) . ' => ' . self::getPHPType($v);
         return 'array(' . implode(', ', $tmp) . ')';
      }
      return $value;
   }

   /**
    * The method returns the PHP code in the method body
    *
    * Метод возвращает PHP код тела метода
    *
    * @param string $code
    * @return string
    * @access private
    */
   private static function getMethodBody($code)
   {
      $tokens = PHPParser::getTokens($code);
      $max = count($tokens) - 3;
      for ($i = 1; $i < $max; $i++)
      {
         if ($tokens[$i] == '{')
         {
            $min = $i + 1;
            break;
         }
      }
      $code = '';
      for ($i = $min; $i < $max; $i++)
      {
         $value = $tokens[$i];
         $code .= is_array($value) ? $value[1] : $value;
      }
      return $code;
   }
}

?>
