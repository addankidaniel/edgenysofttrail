<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core;

class Mailer extends \PHPMailer
{
    public function __construct()
    {
        parent::__construct();
        $config = Core\Register::getInstance()->config->email;
		$this->CharSet = $config['charset'] ?: 'utf8';
        if ($config['isSMTP']) {
            $this->IsSMTP();
            $this->SMTPDebug = 1;                        // enables SMTP debug information (for testing)
                                                         // 1 = errors and messages
                                                         // 2 = messages only
            $this->SMTPAuth     = true;                  // enable SMTP authentication
            //$this->SMTPSecure   = $config['smtp_secure'];
            $this->Host         = $config['smtp_server'];// sets the SMTP server
            $this->Port         = $config['smtp_port'];  // set the SMTP port for the GMAIL server
            $this->Username     = $config['smtp_user'];  // SMTP account username
            $this->Password     = $config['smtp_pass'];  // SMTP account password
            
        }
        
        $this->SetFrom($config['fromEmail'], $config['fromName']);
        if (isset($config['emailCC'])) {
            $emailCC = explode(",", $config['emailcc']);
            foreach ($emailCC as $cc) {
                if ($cc) $this->AddCC($cc);
            }
        }
        if (isset($config['emailBCC'])) {
            $emailBCC = explode(",", $config['emailbcc']);
            foreach ($emailBCC as $bcc) {
                if ($bcc) $this->AddBCC($bcc);
            }
        }
        
        if ($config['isHTML']) {
            $this->IsHTML();
        }
    }

}