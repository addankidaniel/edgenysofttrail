<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core;

class Picture
{
   const PIC_AUTOWIDTH = 0;
   const PIC_AUTOHEIGHT = 1;
   const PIC_MANUAL = 2;
   const PIC_AUTO = 3;

   private $img = null;
   private $config = null;
   protected $src = null;
   protected $info = null;
   protected $width = null;
   protected $height = null;

   public function __construct($source = null)
   {
      $this->config = Core\Register::getInstance()->config;
      $this->setSource($source);
   }

   public function close()
   {
      if (class_exists('Imagick', false)) return;
      if (!function_exists('NewMagickWand')) imageDestroy($this->img);
   }

   public function setSource($source)
   {
      $this->src = Core\IO::dir($source);
      $this->info = pathinfo($this->src);
      $this->img = $this->createImage();
      if (!$this->img) throw new Core\Exception(__CLASS__.' failed creating image!');
   }

   public function getSource()
   {
      return $this->src;
   }

   public function getSize()
   {
      if (class_exists('Imagick', false)) {
         return array('width'=>$this->img->getImageWidth(), 'height'=>$this->img->getImageHeight());
      }
      if (function_exists('NewMagickWand'))
      {
         MagickReadImage($this->img, $this->src);
         return array('width' => MagickGetImageWidth($this->img), 'height' => MagickGetImageHeight($this->img));
      }
      return array('width' => imagesx($this->img), 'height' => imagesy($this->img));
   }

   public function rotate($dest, $angle, $bgcolor = '#ffffff', $isTransparent=false)
   {
      $dest = Core\IO::dir($dest);
      if ($bgcolor[0] != '#') $bgcolor = '#' . $bgcolor;
      if (class_exists('Imagick', false))
      {
         foreach ($this->img as $frame) 
         {
            $frame->rotateImage($isTransparent ? 'none' : $bgcolor, $angle);
         }
         return $this->saveImage($this->img, $dest);
      }
      if (function_exists('NewMagickWand'))
      {
         MagickReadImage($this->img, $this->src);
         //$alpha = MagickGetImagePixels($this->img, 0, 0, 1, 1, 'A', MW_CharPixel);
         if ($isTransparent) // transparent image
         {
            $pw = NewPixelWand();
            PixelSetColor($pw, 'none');
         }
         else $pw = NewPixelWand($bgcolor);
         MagickRotateImage($this->img, $pw, (float)$angle);
         return $this->saveImage($this->img, $dest);
      }
      //$alpha = (imagecolorat($this->img,$x,$y) & 0x7F000000) >> 24;
      $bg = ($isTransparent) ? imagecolortransparent($this->img) : hexdec(substr($bgcolor,1));
      $rotated=imagerotate($this->img,(float)-$angle, $bg);
      if($rotated!==false)$this->img=$rotated;
#     imagealphablending($this->img, true);
      imagesavealpha($this->img, true);
      $this->saveImage($this->img, $dest);
   }

   public function crop($dest, $left, $top, $width, $height, $bgcolor = '#ffffff', $isSmartCrop = false, $isTransparent=false)
   {
      $dest = Core\IO::dir($dest);
      if ($bgcolor[0] != '#') $bgcolor = '#' . $bgcolor;
      if (class_exists('Imagick', false))
      {
         if ($isSmartCrop)
         {
            $oldimg = clone $this->img;
			$this->img = new \Imagick();
            foreach ($oldimg as $overlay)
            {
               $overlay->cropImage($width, $height, $left, $top);
               $frame = new \Imagick();
               $frame->newImage($width, $height, $isTransparent ? 'none' : $bgcolor);
               $frame->compositeImage($overlay, \IMagick::COMPOSITE_OVER, ($left < 0) ? -$left : 0, ($top < 0) ? -$top : 0);
               $this->img->addImage($frame);
            }
         } else {
            foreach ($this->img as $frame) $frame->cropImage($width, $height, $left, $top);
         }
         $this->saveImage($this->img, $dest);
      }
      elseif (function_exists('NewMagickWand'))
      {
         MagickReadImage($this->img, $this->src);
         //$alpha = MagickGetImagePixels($this->img, 0, 0, 1, 1, 'A', MW_CharPixel);
         if ($isTransparent) {
             $bgcolor = NewPixelWand();
             PixelSetColor($bgcolor, 'none');
         }
         else $bgcolor = NewPixelWand($bgcolor);
         MagickCropImage($this->img, $width, $height, $left, $top);
         $magickVersion = MagickGetVersion();
         if ($magickVersion[1] >= 1589)
         {
            // magick wrote: We added MagickResetImagePage() to replicate the functionality of the -repage option.
            // It will be available in ImageMagick 6.3.3 Beta and MagickWandForPHP 1.0.2 Beta tommorrow.
            MagickSetImageBackgroundColor($this->img, $bgcolor);
            MagickResetImagePage($this->img, $width . 'x' . $height . '-' . $left . '-' . $top);
         }
         if ($isSmartCrop)
         {
            $mw = NewMagickWand();
            MagickNewImage($mw, $width, $height, $bgcolor);
            MagickSetImageFormat($mw, 'png');
            MagickCompositeImage($mw, $this->img, MW_SrcOverCompositeOp , ($left < 0) ? -$left : 0, ($top < 0) ? -$top : 0);
            MagickWriteImage($mw, $dest);
         }
         else $this->saveImage($this->img, $dest);
      }
      else
      {
         $size = $this->getSize();
         $srcDimensions = array('top' => 0, 'right' => 0, 'bottom' => 0, 'left' => 0, 'width' => 0, 'height' => 0);
         $destDimensions = array('top' => 0, 'left' => 0, 'width' => $width, 'height' => $height);
         $keys = array('top' => 'height', 'right' => 'width', 'bottom' => 'height', 'left' => 'width');
         $srcDimensions['top'] = $top;
         $srcDimensions['left'] = $left;
         $srcDimensions['right'] = $left + $width;
         $srcDimensions['bottom'] = $top + $height;
         foreach ($srcDimensions as $key => $value)
         {
            if (array_key_exists($key, $keys))
            {
               if ($srcDimensions[$key] < 0) $srcDimensions[$key] = 0;
               if ($srcDimensions[$key] > $size[$keys[$key]]) $srcDimensions[$key] = $size[$keys[$key]];
            }
         }
         $srcDimensions['width'] = $srcDimensions['right'] - $srcDimensions['left'];
         $srcDimensions['height'] = $srcDimensions['bottom'] - $srcDimensions['top'];
         $img = imageCreateTrueColor($destDimensions['width'], $destDimensions['height']);
         imagesavealpha($img, true);
         //    $transparent = imagecolorallocatealpha($img, 0, 0, 0, 127);
         //$alpha = (imagecolorat($this->img,0,0) & 0x7F000000) >> 24;
         $bg = ($isTransparent) ? imagecolortransparent($this->img) : hexdec(substr($bgcolor,1));
         if ($isSmartCrop)
         {
            imagefill($img, 0, 0, $bg);
            imageCopy($img, $this->img, ($left < 0) ? -$left : 0, ($top < 0) ? -$top : 0, $srcDimensions['left'], $srcDimensions['top'], $size['width'] - $srcDimensions['left'], $size['height'] - $srcDimensions['top']);
         } else {
            imagefill($img, 0, 0, $bg);
            imageCopy($img, $this->img, $destDimensions['left'], $destDimensions['top'], $srcDimensions['left'], $srcDimensions['top'], $srcDimensions['width'], $srcDimensions['height']);
         }
         $this->saveImage($img, $dest);
      }
   }

   public function resize($dest, $width, $height, $mode = self::PIC_MANUAL, $maxwidth = null, $maxheight = null)
   {
      $dest = Core\IO::dir($dest);
      if (class_exists('Imagick', false))
      {
         $size = $this->getSize();
         $this->setSize($width, $height, $mode, $size['width'], $size['height'], $maxwidth, $maxheight);
         foreach ($this->img as $frame)
            $frame->thumbnailImage($this->width, $this->height, false);
         return $this->saveImage($this->img, $dest);
      }
      if (function_exists('NewMagickWand'))
      {
         MagickReadImage($this->img, $this->src);
         $size = $this->getSize();
         $this->setSize($width, $height, $mode, $size['width'], $size['height'], $maxwidth, $maxheight);
         MagickScaleImage($this->img, $this->width, $this->height);
         return $this->saveImage($this->img, $dest);
      }
      $size = $this->getSize();
      $this->width = $size['width']; $ow = $this->width;
      $this->height = $size['height']; $oh = $this->height;
      $this->setSize($width, $height, $mode, $this->width, $this->height, $maxwidth, $maxheight);
      $img = ImageCreateTrueColor($this->width, $this->height);
      $bg = imagecolorallocatealpha($img, 255, 255, 255, 127);
      //$color = imagecolorsforindex($this->img, imagecolorat($this->img,0,0));
      if (in_array(strtolower($this->info['extension']), array('png','tga','gif')))
      {
        $bg = imagecolortransparent($this->img);
        imagesavealpha($img, true);
        $transparent = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagefill($img, 0, 0, $transparent);      
        imagecopyresized($img, $this->img, 0, 0, 0, 0, $this->width, $this->height, $ow, $oh);
        imagealphablending($this->img, true);
        imagesavealpha($this->img, true);
      }
      else
      {
        imagefill($img, 0, 0, 0x000000);      
        imagecopyresampled($img, $this->img, 0, 0, 0, 0, $this->width, $this->height, $ow, $oh);
      }
      $this->saveImage($img, $dest);
   }
   
   public function resizeFit($dest, $width, $height)
   {
      $size = $this->getSize();
      $aspect = $width/$height;
      $left = $top = 0;
      $crwidth = round((int)$size['height']*$aspect);
      $crheight = round((int)$size['width']/$aspect);
      if ($size['width'] > $size['height']) {
         $left = round(((int)$size['width'] - $crwidth)/2);
      } elseif ($size['width'] < $size['height']) {
         $top = round(((int)$size['height'] - $crheight)/2);
      }
      $this->crop($dest, $left, $top, $crwidth, $crheight);
      foo(new Picture($dest))->resize($dest, $width, $height, self::PIC_MANUAL);
      //$left = 
      //foo(new Picture($dest))->crop($dest, $left, $top, $width, $height);
   }

   // $imgSrc - GD image handle of source image
   // $angle - angle of rotation. Needs to be positive integer
   // angle shall be 0,90,180,270, but if you give other it
   // will be rouned to nearest right angle (i.e. 52->90 degs, 96->90 degs)
   // returns GD image handle of rotated image.
   protected function ImageRotateRightAngle($imgSrc, $angle, $bgcolor = '#ffffff')
   {
      $angle = min(((int)(($angle + 45) / 90) * 90), 270);
      if ($angle == 0) return $imgSrc;
      $srcX = imagesx($imgSrc);
      $srcY = imagesy($imgSrc);
      switch ($angle)
      {
         case 90:
           $imgDest = imagecreatetruecolor($srcY, $srcX);
           for ($x = 0; $x < $srcX; $x++)
           {
              for ($y = 0; $y < $srcY; $y++)
              {
                 imagecopy($imgDest, $imgSrc, $srcY - $y - 1, $x, $x, $y, 1, 1);
              }
           }
           break;
         case 270:
           $imgDest = imagecreatetruecolor($srcY, $srcX);
           for ($x = 0; $x < $srcX; $x++)
           {
              for ($y = 0; $y < $srcY; $y++)
              {
                 imagecopy($imgDest, $imgSrc, $y, $srcX - $x - 1, $x, $y, 1, 1);
              }
           }
           break;
         default:
           return $imgSrc;
      }
      return $imgDest;
   }

   protected function setSize($width, $height, $mode, $w, $h, $maxwidth, $maxheight)
   {
      switch ($mode)
      {
         case self::PIC_AUTOWIDTH:
           $nh = $height;
           if ($maxheight > 0 && $nh > $maxheight)
           {
              $nh = $maxheight;
              $height = $maxheight;
           }
           $nw = $height / $h * $w;
           if ($maxwidth > 0 && $nw > $maxwidth) $nw = $maxwidth;
           break;
         case self::PIC_AUTOHEIGHT:
           $nw = $width;
           if ($maxwidth > 0 && $nw > $maxwidth)
           {
              $nw = $maxwidth;
              $width = $maxwidth;
           }
           $nh = $width / $w * $h;
           if ($maxheight > 0 && $nh > $maxheight) $nh = $maxheight;
           break;
         case self::PIC_MANUAL;
           $nw = $width;
           $nh = $height;
           if ($maxwidth > 0 && $nw > $maxwidth) $nw = $maxwidth;
           if ($maxheight > 0 && $nh > $maxheight) $nh = $maxheight;
           break;
         case self::PIC_AUTO:
         default:
           $nw = $w;
           $nh = $h;
           if ($maxwidth > 0 && $nw > $maxwidth)
           {
              $nw = $maxwidth;
              $nh = $nw / $w * $h;
           }
           if ($maxheight > 0 && $nh > $maxheight)
           {
              $nh = $maxheight;
              $nw = $nh / $h * $w;
           }
           break;
      }
      $this->height = $nh; $this->width = $nw;
   }

   private function saveImage($img, $dest)
   {
      if (class_exists('Imagick', false)) return $img->writeImages($dest, true);
      elseif (function_exists('NewMagickWand')) return MagickWriteImage($img, $dest);
      switch (strtolower(pathinfo($dest, PATHINFO_EXTENSION)))
      {
         case 'png':
           imagePNG($img, $dest);
           break;
         case 'gif':
           imageGIF($img, $dest);
           break;
         case 'jpeg':
         case 'jpg':
           imageJPEG($img, $dest);
           break;
      }
      return imageDestroy($img);
   }

   public function createImage()
   {
      if (class_exists('Imagick', false)) {
         $src = $this->src;
         $ext = pathinfo($src, PATHINFO_EXTENSION);
         if (strpos($ext, '[')===FALSE) $src.='[0]'; // for multipage files, load only first page - saves a lot of time
         $img = new \Imagick($src);
         $info = $img->identifyImage();
         if (stripos($info['format'], 'PDF') !== FALSE) {
            foreach ($img as $k=>$page) if ($k > 0) $page->removeImage();
         }
         return $img;
      }
      elseif (function_exists('NewMagickWand')) return NewMagickWand();
      try
      {
         switch (strtolower($this->info['extension']))
         {
            case 'png':
              return imagecreatefrompng($this->src);
              break;
            case 'gif':
              return imagecreatefromgif($this->src);
              break;
            case 'jpeg':
            case 'jpg':
              return imagecreatefromjpeg($this->src);
              break;
            default: // if extension empty
              return $this->createImageTry();
         }
      }
      catch(Exception $e)
      {
         return $this->createImageTry();
      }
   }

   private function createImageTry()
   {
      try
      {
         return imagecreatefromgif($this->src);
      }
      catch(\Exception $e)
      {
         try
         {
            return imagecreatefromjpeg($this->src);
         }
         catch(\Exception $e)
         {
            try
            {
               return imagecreatefrompng($this->src);
            }
            catch(\Exception $e)
            {
               return false;
            }
         }
      }
   }
}

?>