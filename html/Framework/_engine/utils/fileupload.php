<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core,
	ClickBlocks\FFProbe;

class FileUpload {

	public $extensions;
	public $maxsize;
	public $minsize;
	public $types;
	public $error;
	public $validate;
	public $name;
	public $id;
	public $unique;
	public $destination;
	public $mimetype;

	/**
	 * Constructs a new UploadFile.
	 *
	 * @access public
	 */
	public function __construct()
	{
		$this->extensions = array();
		$this->types = array();
		$this->error = 0;
		$this->validate = true;
		$this->unique = true;
		$this->destination = Core\IO::dir( 'temp' );
		$this->maxsize = Core\Register::getInstance()->config->maxFilesize;
	}

	/**
	 * Moves an uploaded file to the requested directory.
	 *
	 * @param array $data
	 * @return boolean|array returns FALSE if a error is occured and the array of parameters of a moved file otherwise.
	 * @access public
	 */
	public function upload( array $data=NULL )
	{
		if( $data === NULL ) {
			$data = $this->handleDirectUpload();
		}
		$data['type'] = $this->mimetype = mime_content_type( $data['tmp_name'] );
		if( $this->validate && !$this->isValid( $data ) ) {
			return FALSE;
		}
		$data['type'] = $this->mimetype;
		$name = $this->getName( $data );
		$path = $this->normalizePath( ($this->destination ?: Core\IO::dir( 'temp' )) );
		Core\IO::createDirectories( $path );
		if( !$this->moveUploadedFile( $data, $path . $name ) ) {
			$this->error = 14;
			return FALSE;
		}
		$url = $this->normalizeURL( str_replace( Core\Register::getInstance()->config->root, '', $path ) );
		return [
			'originalName' => $data['name'],
			'type' => $data['type'],
			'size' => $data['size'],
			'name' => $name,
			'path' => $path,
			'fullname' => $path . $name,
			'url' => $url . $name,
		];
	}

	public function handleDirectUpload()
	{
		if ($_SERVER['HTTP_X_FILE_NAME']) {
			return array(
				'_isDirect' => true,
				'name' => urldecode($_SERVER['HTTP_X_FILE_NAME']),
				'size' => (int)$_SERVER['HTTP_X_FILE_SIZE'],
				'type' => (string)$_SERVER['HTTP_X_FILE_TYPE'],
				'error' => UPLOAD_ERR_OK,
			);
		}
		return null;
	}

	/**
	 * Verify whether the download file satisfies required conditions.
	 *
	 * @param array $data
	 * @return boolean
	 * @access public
	 */
	public function isValid(array $data)
	{
		try {
			$this->error = 0;
			if ($data['error'] > 0)
			{
				throw new \Exception( $data['error'] );
			}
			if (is_array($this->extensions) && count($this->extensions) > 0)
			{
				$pp = pathinfo( $data['name'] );
				if (!in_array( strtolower( $pp['extension'] ), $this->extensions ))
				{
					throw new \Exception( UPLOAD_ERR_EXTENSION );
				}
			}
			if (is_array($this->types) && count($this->types) > 0)
			{
				if (!in_array($data['type'], $this->types))
				{
					$ffprobe = new FFProbe( $data['tmp_name'] );
					if($ffprobe->audioIsMP3())
					{
						$this->mimetype = 'audio/mp3';
						return true;
					}
					\ClickBlocks\Debug::ErrorLog( "uploaded mimetype: {$data['type']} is unsupported!" );
					throw new \Exception( 13 );
				}
			}
			if ($this->maxsize && $data['size'] > $this->maxsize)
			{
				throw new \Exception( UPLOAD_ERR_INI_SIZE );
			}
			if ($this->minsize && $data['size'] < $this->minsize)
			{
				throw new \Exception( 12 );
			}
		} catch (\Exception $e) {
			$this->error = (int)$e->getMessage();
		}
		return ($this->error === 0);
	}

	/**
	 * Returns new name for a moving file.
	 *
	 * @param array $data
	 * @return string
	 * @access protected
	 */
	protected function getName(array $data)
	{
		if ($this->unique) {
			$name = md5(microtime());
		} else if ($this->name) {
			$name = $this->name;
		} else {
		   return $data['name'];
		}
		$pp = pathinfo( $data['name'] );
		return $pp['extension'] ? $name . '.' . $pp['extension'] : $name;
	}

	protected function moveUploadedFile( array $data, $dest )
	{
	   return ($data['_isDirect'] ? copy( 'php://input', $dest ) : copy( $data['tmp_name'], $dest ));
	}

	/**
	 * Adds slash to the end of path.
	 *
	 * @param string $path
	 * @return string
	 * @access private
	 */
	private function normalizePath($path)
	{
		return !$path ? '' : ($path . (($path[strlen($path) - 1] == '/') ? '' : '/'));
	}

	/**
	 * Adds slash to the start of url.
	 *
	 * @param string $url
	 * @return string
	 * @access private
	 */
	private function normalizeURL($url)
	{
		return $this->normalizePath( (($url[0] == '/') ? '' : '/') . $url );
	}
}
