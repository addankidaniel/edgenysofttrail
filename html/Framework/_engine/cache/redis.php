<?php
/**
 * redis.php v. 1.7.0
 *
 * Copyright (C) 2013  eDepoze LLC
 * http://www.edepoze.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: redis.php
 *
 * @category   Cache
 * @package    Core
 * @copyright  2013 eDepoze LLC <info@edepoze.com>
 * @link       http://www.edepoze.com
 * @since      File available since Release 1.7.0
 */

namespace ClickBlocks\Cache;

/**
 * The class designed for caching data in redis.
 *
 * @category  Cache
 * @package   Core
 * @copyright 2013 eDepoze LLC <info@edepoze.com>
 * @version   Release: 1.0.0
 */
class CacheRedis implements ICache {

	/**
	 * The instance of \Redis class.
	 *
	 * @var object $redis
	 * @access private
	 */
	private $redis = NULL;

	private $logging = FALSE;

	/**
	 * Constructor of this site.
	 *
	 * @param string $host	- host for a redis connection.
	 * @param integer $port	- port for a redis connection.
	 * @access public
	 */
	public function __construct( $host='127.0.0.1', $port='6379', $timeout=2 ) {
		$this->redis = new \Redis();
		if( strpos( $host, '.sock' ) !== FALSE ) {
			$this->redis->connect( $host );
		} else {
			$this->redis->connect( $host, $port, $timeout, NULL, 100 );	//100ms retry delay
		}
	}

	/**
	 * Puts a content into a cache.
	 *
	 * @param string $key     - unique identifier of a cache.
	 * @param mixed $content  - arbitrary value.
	 * @param integer $expire - expiration time of a cache.
	 * @access public
	 */
	public function set( $key, $content, $expire=0 ) {
		$key = $this->namespaceKey( $key );
		$content = serialize( $content );
		$expire = (int)$expire;

		if( $this->logging ) {
			$bytes = number_format( strlen( $content ) );
			$this->errorLog( "[Redis::set] {$key} expire: {$expire} bytes: {$bytes}" );
		}

		$this->redis->set( $key, $content, $expire );
	}

	/**
	 * Gets a value from a cache.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @return mixed
	 * @access public
	 */
	public function get( $key ) {
		$key = $this->namespaceKey( $key );
		if( $this->logging ) {
			$this->errorLog( "[Redis::get] {$key}" );
		}

		return unserialize( $this->redis->get( $key ) );
	}

	/**
	 * Gets a JSON object from a cache.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @return mixed
	 * @access public
	 */
	public function getJSON( $key ) {
		$key = $this->namespaceKey( $key );
		if( $this->logging ) {
			$this->errorLog( "[Redis::get] {$key}" );
		}

		return json_decode( $this->redis->get( $key ) );
	}

	/**
	 * Gets a JSON object from a cache.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @return mixed
	 * @access public
	 */
	public function setJSON( $key, $content, $expire=0 ) {
		$key = $this->namespaceKey( $key );
		$content = json_encode( $content, JSON_UNESCAPED_SLASHES );
		$expire = (int)$expire;

		if( $this->logging ) {
			$bytes = number_format( strlen( $content ) );
			$this->errorLog( "[Redis::set] {$key} expire: {$expire} bytes: {$bytes}" );
		}

		$this->redis->set( $key, $content, $expire );
	}

	/**
	 * Checks whether or not a cache is expired.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @return boolean    - the method returns TRUE if a cache is expired and FALSE otherwise.
	 */
	public function isExpired( $key ) {
		$key = $this->namespaceKey( $key );
		return ($this->redis->exists( $key ) === FALSE);
	}

	/**
	 * Checks whether or not the given type of a cache memory is available for usage.
	 *
	 * @return boolean    - the method returns TRUE if the given type of a cache memory is available and FALSE otherwise.
	 * @access public
	 * @static
	 */
	public static function isAvailable() {
		return extension_loaded( 'redis' );
	}

	/**
	 * Deletes a cache associated with certain unique identifier.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @access public
	 */
	public function delete( $key ) {
		$key = $this->namespaceKey( $key );
		if( $this->logging ) {
			$this->errorLog( "[Redis::delete] {$key}" );
		}
		$this->redis->delete( $key );
	}

	/**
	 * Closes the current connection with redis.
	 *
	 * @access public
	 */
	public function __destruct() {
		$this->redis->close();
	}

	/**
	 * Cleans entire cache memory of the given type.
	 *
	 * @access public
	 */
	public function clean() {
		if( $this->logging ) {
			$this->errorLog( "[Redis::clean]" );
		}
		$this->redis->flushDB();
	}

	private function namespaceKey( $key ) {
		$key = str_replace( 'page_tpl_', 'page_tpl:', $key );
		return str_replace( 'page_vs_', 'page_vs:', $key );
	}

	private function errorLog( $message ) {
		\ClickBlocks\Debug::ErrorLog( $message );
	}
}
