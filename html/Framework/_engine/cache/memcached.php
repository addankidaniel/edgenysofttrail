<?php
/**
 * memcached.php v. 1.6.0
 *
 * Copyright (C) 2013  eDepoze LLC
 * http://www.edepoze.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: memcached.php
 *
 * @category   Cache
 * @package    Core
 * @copyright  2013 eDepoze LLC <info@edepoze.com>
 * @link       http://www.edepoze.com
 * @since      File available since Release 1.6.0
 */

namespace ClickBlocks\Cache;

/**
 * The class designed for caching data in memory.
 *
 * @category  Cache
 * @package   Core
 * @copyright 2013 eDepoze LLC <info@edepoze.com>
 * @version   Release: 1.0.0
 */
class CacheMemcached implements ICache
{

	/**
	 * The instance of \Memcached class.
	 *
	 * @var object $memobj
	 * @access private
	 */
	private $memcached = null;

	/**
	 * Constructor of this site.
	 *
	 * @param string $host      - host for a memcache connection.
	 * @param integer $port     - port for a memcache connection.
	 * @access public
	 */
	public function __construct( $host='127.0.0.1', $port='11211' )
	{
		$this->memcached = new \Memcached( 'WebMgrPool' );
		//$this->memcached->setOption( \Memcached::OPT_BINARY_PROTOCOL, TRUE );
		if( !count( $this->memcached->getServerList() ) ) $this->memcached->addServer( $host, $port );
	}

	/**
	 * Puts a content into a cache.
	 *
	 * @param string $key     - unique identifier of a cache.
	 * @param mixed $content  - arbitrary value.
	 * @param integer $expire - expiration time of a cache.
	 * @access public
	 */
	public function set( $key, $content, $expire=0 )
	{
		//$key = md5( $key );
		$content = serialize( $content );
		$expire = (int)$expire;

//		$mt = substr( str_pad( microtime( TRUE ), 15, '0' ), 11 );
//		$now = date( 'Y-m-d H:i:s' );
//		$bytes = number_format( strlen( $content ) );
//		error_log( "{$now}.{$mt}: [memcached::set] {$key} expire: {$expire} bytes: {$bytes}\n", 3, "{$_SERVER['DOCUMENT_ROOT']}/../logs/php-error.log" );

		$this->memcached->set( $key, $content, $expire );
	}

	/**
	 * Gets a value from a cache.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @return mixed
	 * @access public
	 */
	public function get( $key )
	{
		//$key = md5( $key );

//		$mt = substr( str_pad( microtime( TRUE ), 15, '0' ), 11 );
//		$now = date( 'Y-m-d H:i:s' );
//		error_log( "{$now}.{$mt}: [memcached::get] {$key}\n", 3, "{$_SERVER['DOCUMENT_ROOT']}/../logs/php-error.log" );

		return unserialize( $this->memcached->get( $key ) );
	}

	/**
	 * Checks whether or not a cache is expired.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @return boolean    - the method returns TRUE if a cache is expired and FALSE otherwise.
	 */
	public function isExpired( $key )
	{
		return ($this->memcached->get( $key ) === FALSE && $this->memcached->getResultCode() == \Memcached::RES_NOTFOUND);
	}

	/**
	 * Checks whether or not the given type of a cache memory is available for usage.
	 *
	 * @return boolean    - the method returns TRUE if the given type of a cache memory is available and FALSE otherwise.
	 * @access public
	 * @static
	 */
	public static function isAvailable()
	{
		return extension_loaded('memcached');
	}

	/**
	 * Deletes a cache associated with certain unique identifier.
	 *
	 * @param string $key - unique identifier of a cache.
	 * @access public
	 */
	public function delete( $key )
	{
		$this->memcached->delete( $key );
	}

	/**
	 * Closes the current connection with memcache.
	 *
	 * @access public
	 */
	public function __destruct()
	{
		$this->memcached->quit();
	}

	/**
	 * Cleans entire cache memory of the given type.
	 *
	 * @access public
	 */
	public function clean()
	{
		$this->memcached->flush();
	}
}
