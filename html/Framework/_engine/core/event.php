<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: event.php
 *
 * @category   Core
 * @package    Core
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */

namespace ClickBlocks\Core;

interface IEvent
{
   public function __construct(IDelegate $delegate);
   public function __invoke();
   public function add(IDelegate $delegate);
   public function delete(IDelegate $delegate);
   public function call(array $params);
}

/**
 * The class allows you to name a few delegates from the same set of input parameters
 * Класс позволяет вызвать несколько делегатов с одинаковым набором входных параметров
 *
 * @category  Core
 * @package   Core
 * @copyright 2007-2010 SARITASA KZ <info@saritasa.kz>
 * @version   Release: 1.0.0
 */
class Event implements IEvent
{
   /**
    * Array of delegates
    * Массив делегатов
    *
    * @var array
    */
   protected $delegates = array();

   /**
    * The class constructor
    * add a delegate to the array $ delegates
    * Конструктор класса
    * добавляет делегата в массив $delegates
    *
    * @param Idelegate string $delegate
    */
   public function __construct(IDelegate $delegate)
   {
      $this->add($delegate);
   }

   /**
    * add a delegate to the array $ delegates
    * добавляет делегата в массив $delegates
    *
    * @param Idelegate string $delegate
    */
   public function add(IDelegate $delegate)
   {
      $this->delegates[(string)$delegate] = $delegate;
   }

   /**
    * deletes a delegate from the array $ delegates
    * удаляет делегата из массива $delegates
    *
    * @param Idelegate string $delegate
    */
   public function delete(IDelegate $delegate)
   {
      unset($this->delegates[(string)$delegate]);
   }

   /**
    * when referring to the object as a function - calls this->call() with the current settings
    * при обращении к объекту как к функции - вызывает метод this->call() с текущими параметрами
    *
    * @return mixed
    */
   public function __invoke()
   {
      return $this->call(func_get_args());
   }

   /**
    * function calls all the delegates from the array $ delegates with the current parameters $ params
    * функция вызывает всех делегатов из массива $delegates с текущими параметрами $params
    *
    * @param array $params
    * @return mixed
    */
   public function call(array $params)
   {
      foreach ($this->delegates as $delegate) $res = $delegate->call($params);
      return $res;
   }
}

?>
