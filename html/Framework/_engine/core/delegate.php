<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: delegate.php
 *
 * @category   Core
 * @package    Core
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */

namespace ClickBlocks\Core;

/**
 * Interface for delegates.
 *
 * Интерфейс для делегатов.
 *
 * @category  Core
 * @package   Core
 * @copyright 2007-2010 SARITASA LLC <info@saritasa.com>
 * @version   Release: 1.0.0
 */
interface IDelegate
{
   public function __construct($callback);
   public function __invoke();
   public function call(array $params);
}

/**
 * This class is intended to serve as a callback method
 *
 * Этот класс предназначен для выполнения функции callback метода
 *
 * @category  Core
 * @package   Core
 * @copyright 2007-2010 SARITASA LLC <info@saritasa.com>
 * @version   Release: 1.0.0
 */
class Delegate implements IDelegate
{
   /**
    * As this property is written as a string class method call
    *
    * В данное свойство записан в виде строки вызов метода класса
    *
    * @var string
    * @access private
    */
   private $callback = null;

   /**
    * The property value contains a namespace called a class.
    *
    * Значение свойства содержит пространство имен, вызываемого класса.
    *
    * @var string
    * @access protected
    */
   protected $namespace = null;

   /**
    * The value property contains the name of the called class.
    *
    * Значение свойства содержит имя вызываемого класса.
    *
    * @var string
    * @access protected
    */
   protected $class = null;

   /**
    * The value property contains the name of the method being called.
    *
    * Значение свойства содержит имя вызываемого метода.
    *
    * @var string
    * @access protected
    */
   protected $method = null;

   /**
    * Property determines whether a static method called
    *
    * Свойство определяет  является ли вызываемый метод статическим
    *
    * @var bool
    * @access protected
    */
   protected $isStatic = false;

   /**
    * This property defines a static method is invoked, the function, a method of another class or control.
    *
    * Данное свойство определяет, вызывается статический метод, функция, метод другого класса или контрол.
    *
    * @var string
    * @access protected
    */
   protected $type = 'function';

   /**
    * Contains the id of the control
    *
    * Содержит в себе id контрола
    *
    * @var string
    * @access protected
    */
   protected $cid = null;


   /**
    * The class constructor. Extracts from the $callback class and method. Defines the properties of the class
    *
    * Конструктор класса. Выделяет из $callback класс и метод. Определяет свойства класса.
    *
    * @param string $callback
    * @access public
    */
   public function __construct($callback)
   {
      $this->callback = htmlspecialchars_decode($callback);
      $func = explode('::', $this->callback);
      if (count($func) > 1)
      {
         $this->class = $func[0];
         $this->method = $func[1];
         $this->type = 'static';
         $this->isStatic = true;
      }
      else
      {
         $func = explode('->', $this->callback);
         if (count($func) > 1)
         {
            $this->class = $func[0];
            $this->method = $func[1];
            $this->type = 'method';
         }
         else $this->method = $this->callback;
      }
      if ($this->type == 'function')
      {
         $k = strrpos($this->method, '\\');
         if ($k !== false) $this->namespace = substr($this->method, 0, $k + 1);
         if ($this->namespace[0] != '\\') $this->namespace = '\\' . $this->namespace;
         return;
      }
      if (!strlen($this->class))
      {
         $this->class = get_class(Register::getInstance()->page);
         $this->namespace = '\ClickBlocks\MVC\\';
      }
      else
      {
         $class = explode('@', $this->class);
         if (count($class) > 1)
         {
            $this->class = $class[0];
            $this->cid = $class[1];
            $this->type = 'control';
         }
         $k = strrpos($this->class, '\\');
         if ($k !== false) $this->namespace = substr($this->class, 0, $k + 1);
         if ($this->namespace[0] != '\\') $this->namespace = '\\' . $this->namespace;
      }
   }
   
   /**
    * Returns array of detail information of a callback.
    * Output array has the format array('class' => ... [string] ..., 
    *                                   'method' => ... [string] ..., 
    *                                   'isStatic' => ... [boolean] ..., 
    *                                   'type' => ... [string] ...)
    *
    * @return array
    * @access public
    */
   public function getInfo()
   {
     return array('class' => $this->namespace . $this->class, 
                  'method' => $this->method,
                  'isStatic' => $this->isStatic,
                  'type' => $this->type);
   } 

   /**
    * The method determines whether the name of equal class and method names in the space of string $callback data specified in the properties
    *
    * Метод определяет равные ли названия класса и метода, пространства имен в строке $callback данным определенным в свойствах.
    *
    * @param string $callback
    * @return bool
    * @access public
    */
   public function in($callback)
   {
      if ($this->type == 'function')
      {
         if ($callback == $this->method) return true;
         $k = strrpos($callback, '\\');
         if ($k !== false) $namespace = substr($callback, 0, $k + 1);
         if ($this->namespace == $namespace && $namespace == $callback) return true;
      }
      else
      {
         $data = explode($this->isStatic ? '::' : '->', $callback);
         if ($this->class == $data[0] && $this->method == $data[1]) return true;
         $k = strrpos($data[0], '\\');
         if ($k !== false) $namespace = substr($data[0], 0, $k + 1);
         if ($this->namespace == $namespace && $namespace == $callback || $data[1] == '' && $data[0] == $this->class) return true;
      }
      return false;
   }


   /**
    * The method is called when the script tries to execute as a function of the object. Return the method $this->call().
    *
    * Метод вызывается когда скрипт пытается выполнить объект как функцию. Возвращаем метод $this->call().
    *
    * @return mixed
    * @access public
    */
   public function __invoke()
   {
      return $this->call(func_get_args());
   }

   /**
    * The method calls the specified method is defined in the property $ method with a parameter array.
    *
    * Метод вызывает указанный метод определенный в свойстве $method с массивом параметров.
    *
    * @param array $params
    * @return mixed
    * @access public
    */
   public function call(array $params)
   {
      switch ($this->type)
      {
         case 'function':
           return call_user_func_array($this->method, $params);
         case 'static':
           return call_user_func_array(array($this->class, $this->method), $params);
         case 'method':
           if (get_class(Register::getInstance()->page) == $this->class) $class = Register::getInstance()->page;
           else
           {
              $class = new \ReflectionClass($this->class);
              if (is_array($params['construct']))
              {
                 $class = $class->newInstanceArgs($params['construct']);
                 unset($params['construct']);
                 $params = array_pop($params);
              }
              else $class = $class->newInstance();
           }
           return call_user_func_array(array($class, $this->method), $params);
         case 'control':
           $ctrl = Register::getInstance()->page->getByUniqueID($this->cid);
           if (!$ctrl) $ctrl = Register::getInstance()->page->get($this->cid);
           if ($ctrl === false) throw new \Exception(err_msg('ERR_EVN_1', array($this->cid)));
           if ($this->isStatic) return call_user_func_array(array($this->class, $this->method), $params);
           return call_user_func_array(array($ctrl, $this->method), $params);
      }
   }

   /**
    * The method returns a value of $callback, when referring to the object as a string
    *
    * Метод возвращает значение свойства $callback, при обращении к объекту как к строке
    *
    * @return string
    * @access public
    */
   public function __toString()
   {
      return $this->callback;
   }
}

?>
