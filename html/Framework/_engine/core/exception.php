<?php
/**
 * ClickBlocks.PHP v. 1.0
 * 
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com   
 * 
 * This framework is free software. You can redistribute it and/or modify 
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.   
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.    
 * 
 * Responsibility of this file: logexception.php 
 * 
 * @category   Core
 * @package    Core
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0        
 */

namespace ClickBlocks\Core;

/**
 * This class is designed for all Framework-related exceptions
 * 
 * Этот класс предназначен для генерации любых исключений связанных с фреймворком
 *
 * @category  Core
 * @package   Core
 * @copyright 2007-2010 SARITASA LLC <info@saritasa.com>
 * @version   Release: 1.0.0
 */
class Exception extends \Exception
{
   /**
    * Constructor of this class.
    * 
    * Конструктор класса.
    * 
    * @param string $token   - message or token
    * @param array 
    * @access public
    */
   public function __construct($message = '', $code = 0, $previous = null)
   {
      if (is_array($message))
      {
         
      }
      parent::__construct($message, $code, $previous);
   }

   /**
    * Returns token of exception
    * 
    * Возвращает токен исключения
    * 
    * @return string
    * @access public
    * @final
    */
   final public function getToken()
   {
      preg_match('/^\s*token\:\s*(\w+)/i', $this->getMessage(), $m);
      return $m[1];
   }
}

?>
