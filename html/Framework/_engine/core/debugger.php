<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: debugger.php
 *
 * @category   Core
 * @package    Core
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */

namespace ClickBlocks\Core;

/**
 * This class is designed for handling bugs and showing error messages.
 *
 * Этот класс предназначен для перехвата ошибок и показа сообщений об ошибках.
 *
 * @category  Core
 * @package   Core
 * @copyright 2007-2010 SARITASA LLC <info@saritasa.com>
 * @version   Release: 1.0.0
 */
class Debugger
{
  const TEMPLATE_DEBUG = '<!doctype html><html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><title>Bug Report</title><body bgcolor="gold">The following error <pre>$message</pre> has been catched in file <b>$file</b> on line $line<br /><br /><b style="font-size: 14px;">Stack Trace:</b><pre>$traceAsString</pre><b>Execution Time:</b><pre>$executionTime sec</pre><b>Memory Usage:</b><pre>$memoryUsage Mb</pre></pre></body></html>';
  const TEMPLATE_BUG = 'Sorry, server is not available at the moment. Please wait. This site will be working very soon!';

  /**
   * The instance of this class.
   *
   * Экземпляр класса.
   *
  * @var object $instance
   * @access private
   */
  private static $instance = null;

  /**
   * The property containing of the output result of debugger
   *
   * Свойство содержит в себе выходную информацию о результате работы отладчика
   *
   * @var string
   * @access public
   */
  public static $output = null;

  /**
   * The array contains the code for the function eval (), and the stack of the program
   *
   * Массив содержит в себе код для выполнения функции eval(), а также стэк выполнения программы
   *
   * @var array
   * @access private
   */
  private static $eval = array();

  /**
   * The property defines an error
   *
   * Свойство определяет наличие ошибки
   *
   * @var bool
   * @access private
   */
  private static $isParseError = false;

  /**
   * Clones an object of this class. The private method '__clone' doesn't allow to clone an instance of the class.
   *
   * Клонирует объект данного класса. При этом скрытый метод __clone не позволяет клонировать объект.
   *
   * @access private
   */
  private function __clone(){}

  /**
   * Constructor of this class.
   *
   * Конструктор класса.
   *
   * @access private
   */
  private function __construct()
  {
    ini_set('display_errors', 1);
    ini_set('html_errors', 0);
    ob_start(array('\ClickBlocks\Core\Debugger', 'errorFatal'));
    set_exception_handler(array('\ClickBlocks\Core\Debugger', 'exceptionHandler'));
    self::setErrorReporting(E_ALL & ~E_NOTICE);
  }

  /**
   * Set params the PHP error
   *
   * Устанавливает параметры вывода ошибок PHP
   *
   * @param int $errorLevel
   * @access public
   * @static
   */
  public static function setErrorReporting($errorLevel)
  {
    error_reporting($errorLevel);
    restore_error_handler();
    set_error_handler(array('\ClickBlocks\Core\Debugger', 'errorHandler'), $errorLevel);
  }

  /**
   * Returns an instance of this class.
   *
   * Возвращает экземпляр этого класса.
   *
   * @return object
   * @access public
   * @static
   */
  public static function getInstance()
  {
    if (self::$instance === null) self::$instance = new self();
    return self::$instance;
  }


  /**
   * Method determines the value of $eval
   *
   * Метод определяет значение свойства $eval
   *
   * @param string $code
   * @param bool $isParseError
   * @access public
   * @static
   */
  public static function setEvalCode($code)
  {
    self::$eval[md5($code)] = $code;
    return $code;
  }
   
  /**
   * Executes PHP code that inserted into HTML.
   *
   * @param string $code - the PHP inline code.
   * @param array $vars - variables to extract to the PHP code.
   * @return string
   * @access public
   * @static
   */
  public static function exe($code, array $vars = null)
  {
    ${'(_._)'} = $code; unset($code);
    if ($vars) extract($vars);
    ob_start();
    eval(self::setEvalCode(' ?>' . ${'(_._)'} . '<?php '));
    $res = ob_get_clean();
    if (strpos($res, 'eval()\'d') !== false) exit($res);  
    return $res;
  }

  /**
   * Non-fatal error handler.
   *
   * Обработчик простых ошибок.
   *
   * @param integer $errno   - number of an error.
   * @param string $errstr   - error message.
   * @param string $errfile  - file where an error occurred.
   * @param integer $errline - line in the file where an error occurred.
   * @access public
   * @static
   */
  public static function errorHandler($errno, $errstr, $errfile, $errline)
  {
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
  }

  /**
   * Fatal error handler.
   *
   * Обработчик фатальных ошибок.
   *
   * @param string $html - entire output data sent to a browser.
   * @access public
   * @static
   */
  public static function errorFatal($html)
  {
    if (!Register::getInstance()->config['isDebug'] || !preg_match('/(Fatal|Parse) error:(.*) in (.*) on line (\d+)/', $html, $res)) return self::$output ?: $html;
    self::exceptionHandler(new \ErrorException($res[2], 0, 1, $res[3], $res[4]));
    return self::$output;
  }

  /**
   * Exception handler.
   *
   * Обработчик исключений.
   *
   * @param object $e         - instance of \Exception class.
   * @param integer $category - category of an exception.
   * @access public
   * @static
   */
  public static function exceptionHandler(\Exception $e, $category = Logger::LOG_CATEGORY_EXCEPTION)
  {
    restore_error_handler();
    restore_exception_handler();
    $info = self::analyzeException($e);
    new LogException($info['message'], $category, $e->getCode(), $e->getFile(), $e->getLine());
    $config = (array)Register::getInstance()->config;
    $isDebug = (bool)$config['isDebug'];
    foreach (array('debugPage', 'bugPage') as $var) $$var = isset($config[$var]) ? IO::dir($config[$var]) : null;
    if ($isDebug && $config->customDebugMethod && class_exists('\ClickBlocks\Core\Delegate', false))
    {
      $debug = new Delegate($config->customDebugMethod);
      if (!$debug($e, $info)) return;
    }
    if (PHP_SAPI == 'cli' || empty($_SERVER['REMOTE_ADDR']))
    {
      if ($isDebug)
      {
        $output = PHP_EOL . PHP_EOL . 'BUG REPORT' . PHP_EOL . PHP_EOL;
        $output .= 'The following error [[ ' . $info['message'] . ' ]] has been catched in file ' . $info['file'] . ' on line ' . $info['line'] . PHP_EOL . PHP_EOL;
        $output .= 'Stack Trace:' . PHP_EOL . $info['traceAsString'] . PHP_EOL . PHP_EOL;
        $output .= 'Execution Time: ' . $info['executionTime'] . ' sec' . PHP_EOL . 'Memory Usage: ' . $info['memoryUsage'] . ' Mb' . PHP_EOL . PHP_EOL;
        self::$output = $output;
      }
      else
      {
        self::$output = self::TEMPLATE_BUG . PHP_EOL;
      }
      return;
    }
    if ($isDebug)
    {
      $render = function($tpl, $info)
      {
        ${'(_._)'} = $tpl; unset($tpl);
        if (is_file(${'(_._)'})) 
        {
          extract($info);
          return require(${'(_._)'});
        }
        $info['traceAsString'] = htmlspecialchars($info['traceAsString']);
        extract($info);
        eval('$res = "' . str_replace('"', '\"', ${'(_._)'}) . '";');
        return $res;
      };
      if (!is_file($debugPage) || !is_readable($debugPage)) $debugPage = self::TEMPLATE_DEBUG;
      $debugPage = $render($debugPage, $info);
      if (isset($_SESSION))
      { 
        $hash = md5(microtime() . uniqid('', true));
        $_SESSION['__DEBUG_INFORMATION__'][$hash] = $debugPage;
        $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
        $url .= ((strpos($url, '?') !== false) ? '&' : '?') . '__DEBUG_INFORMATION__=' . $hash;
        self::redirect($url, true);
      }
      else 
      {
        self::$output = $debugPage;
      }
    }
    else
    {
      self::$output = (is_file($bugPage) && is_readable($bugPage)) ? file_get_contents($bugPage) : self::TEMPLATE_BUG;
    }
  }

  /**
   * Analyzes an exception.
   *
   * @param \Exception $e
   * @return array - exception information.
   * @access public
   * @static  
   */
  public static function analyzeException(\Exception $e)
  {
    $reduceObject = function($obj) use(&$reduceObject)
    {
      if ($obj === null) return 'null';
      if (is_bool($obj)) return $obj ? 'true' : 'false';
      if (is_object($obj)) return '${\'' . get_class($obj) . '\'}';
      if (is_array($obj))
      {
        if (count($obj) == 0) return '[]';
        $tmp = array(); 
        foreach ($obj as $k => $v) 
        {
          if ($k == '__DEBUG_INFORMATION__') continue;
          if ($k == 'GLOBALS') $tmp[] = 'GLOBALS => *RECURSION*';
          else $tmp[] = $k . ' => ' . $reduceObject($v);
        }
        return '[ ' . implode(', ', $tmp) . ' ]';
      }
      if (is_string($obj)) 
      {
        if (strlen($obj) > 1024) $obj = substr($obj, 0, 512) . ' ... [fragment missing] ... ' . substr($obj, -512);
        return "'" . str_replace("'", '\\\'', $obj) . "'";
      }
      return $obj;
    };
    $reducePath = function($file)
    {
      if (strpos($file, $_SERVER['DOCUMENT_ROOT']) === 0) $file = substr($file, strlen($_SERVER['DOCUMENT_ROOT']) + 1);
      return str_replace((DIRECTORY_SEPARATOR == '\\') ? '/' : '\\', DIRECTORY_SEPARATOR, $file);
    };
    $request = function()
    {
      if (function_exists('apache_request_headers')) return apache_request_headers();
      $headers = array();
      foreach ($_SERVER as $key => $value) 
      {
        if (strpos($key, 'HTTP_') === 0) 
        {
          $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))))] = $value;
        }
      }
      return $headers;
    };
    $response = function()
    {
      if (function_exists('apache_response_headers')) return apache_response_headers();
      $headers = array();
      foreach (headers_list() as $header) 
      {
        $header = explode(':', $header);
        $headers[array_shift($header)] = trim(implode(':', $header));
      }
      return $headers;
    };
    $fragment = function($file, $line, &$index, &$command = null, $half = 10)
    {
      $lines = explode("\n", str_replace("\r\n", "\n", (is_file($file) && is_readable($file)) ? file_get_contents($file) : $file));
      $count = count($lines); $line--;
      if ($line + $half > $count)
      {
        $min = max(0, $line - $half);
        $max = $count;
      } 
      else
      {
        $min = max(0, $line - $half);
        $max = min($line + $half, $count);
      }
      $lines = array_splice($lines, $min, $max - $min + 1);
      $index = $line - $min;
      $command = empty($lines[$index]) ? '' : $lines[$index];
      return implode("\n", $lines);
    };
    $findFunc = function($func, $line, $code)
    {
      $line--;
      foreach (array_reverse($code) as $part)
      {
        $row = explode("\n", $part);
        if (empty($row[$line])) continue;
        $row = $row[$line];
        $tokens = token_get_all('<?php ' . $row . '?>');
        $k = 0; $n = count($tokens);
        while ($k < $n) 
        {
          $token = $tokens[$k++];
          if (is_array($token) && $token[0] == T_STRING && $token[1] == $func) return $part;
        }
      }
      return false;
    };
    $flag = false; $trace = $e->getTrace();
    $info = array();
    $info['isFatalError'] = count($trace) == 1 && empty($trace[0]['file']) && $trace[0]['function'] == '{closure}';
    $message = $e->getMessage();
    $file = $e->getFile();
    $line = $e->getLine();
    if (self::$eval && (strpos($file, 'eval()\'d') !== false || strpos($message, 'eval()\'d') !== false))
    {
      if (preg_match('/, called in ([^ ]+) on line (\d+)/', $message, $matches))
      {
        $line = $matches[2];
        $message = substr($message, 0, strpos($message, ', called in'));
      }
      else if (preg_match('/, called in ([^\(]+)\((\d+)\) : eval\(\)\'d code on line (\d+)/', $message, $matches))
      {
        $line = $matches[3];
        $message = substr($message, 0, strpos($message, ', called in'));
      }
      $file = 'eval()\'s code';
    }
    else if (preg_match('/, called in ([^ ]+) on line (\d+)/', $message, $matches))
    {
      $file = $matches[1];
      $line = $matches[2];
      $message = substr($message, 0, strpos($message, ', called in'));
    }
    foreach ($trace as $k => &$item)
    {
      $item['command'] = isset($item['class']) ? $item['class'] . $item['type'] : '';
      $item['command'] .= $item['function'] . '( ';
      if (isset($item['args']))
      {
        $tmp = array();
        foreach ($item['args'] as $arg) $tmp[] = $reduceObject($arg);
        $item['command'] .= implode(', ', $tmp);
      }
      $item['command'] .= ' )';
      if (isset($item['file']))
      {
        if (self::$eval && strpos($item['file'], 'eval()\'d') !== false)
        {
          $item['file'] = 'eval()\'s code';
          if ($item['function'] == '{closure}' && isset($item['args'][0]) && $item['args'][0] == 4096)
          {
            $item['code'] = $fragment($findFunc($trace[$k + 1]['function'], $item['line'], self::$eval), $item['line'], $index);
          }
          else
          {
            $item['code'] = $fragment($findFunc($item['function'], $item['line'], self::$eval), $item['line'], $index);
          }
        }
        else
        {
          $item['code'] = $fragment($item['file'], $item['line'], $index);
          $item['file'] = $reducePath($item['file']);
        }
      }
      else
      {
        $index = 0; $item['code'] = '';
        if ($file != 'eval()\'s code') 
        {
          if (is_file($file)) $item['code'] = $fragment($file, $line, $index);
        }
        else if (self::$eval) $item['code'] = $fragment(array_pop(self::$eval), $line, $index);
        $item['file'] = '[Internal PHP]';
      }
      $item['index'] = $index;
    }
    $info['host'] = $_SERVER['HTTP_HOST'];
    $info['root'] = $_SERVER['DOCUMENT_ROOT']; 
    $info['memoryUsage'] = number_format(Logger::getMemoryUsage() / 1048576, 4);
    $info['executionTime'] = Logger::getExecutionTime();
    $info['message'] = ltrim($message);
    $info['file'] = $reducePath($file);
    $info['line'] = $line;
    $info['trace'] = $trace;
    if (method_exists($e, 'getClass')) $info['class'] = $e->getClass();
    if (method_exists($e, 'getToken')) $info['token'] = $e->getToken();
    $info['severity'] = method_exists($e, 'getSeverity') ? $e->getSeverity() : '';
    $info['traceAsString'] = $e->getTraceAsString();
    $info['request'] = $request();
    $info['response'] = $response();
    $info['GET'] = isset($_GET) ? $_GET : array();
    $info['POST'] = isset($_POST) ? $_POST : array();
    $info['COOKIE'] = isset($_COOKIE) ? $_COOKIE : array();
    $info['FILES'] = isset($_FILES) ? $_FILES : array();
    $info['SERVER'] = isset($_SERVER) ? $_SERVER : array();
    $info['SESSION'] = isset($_SESSION) ? $_SESSION : array();
    unset($info['SESSION']['__DEBUG_INFORMATION__']);
    return $info;
  }

  /**
   * Returns js-script for redirect to given URL.
   *
   * Возвращает js для перенаправления по заданному URL.
   *
   * @param string $url          - given URL.
   * @param boolean $inNewWindow - determines whether or not to open a new window.
   * @return string
   * @access private
   * @static
   */
  private static function redirect($url, $inNewWindow = false)
  {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
    {
      if ($inNewWindow) self::$output = 'window.open(\'' . addslashes($url) . '\');';
      else self::$output = 'window.location.assign(\'' . addslashes($url) . '\');';
    }
    else
    {
      if ($inNewWindow) self::$output = '<script type="text/javascript">window.open(\'' . addslashes($url) . '\');</script>';
      else self::$output = '<script type="text/javascript">window.location.assign(\'' . addslashes($url) . '\');</script>';
    }
  }
}

?>