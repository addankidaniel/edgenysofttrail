<?php
/**
 * ClickBlocks.PHP v. 1.0
 *
 * Copyright (C) 2010  SARITASA LLC
 * http://www.saritasa.com
 *
 * This framework is free software. You can redistribute it and/or modify
 * it under the terms of either the current ClickBlocks.PHP License
 * viewable at theclickblocks.com) or the License that was distributed with
 * this file.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the ClickBlocks.PHP License
 * along with this program.
 *
 * Responsibility of this file: tracebreaker.php
 *
 * @category   Core
 * @package    Core
 * @copyright  2007-2010 SARITASA LLC <info@saritasa.com>
 * @link       http://www.saritasa.com
 * @since      File available since Release 1.0.0
 */

namespace ClickBlocks\Core;

/**
 * Friendly methods
 *
 * С помощью этого класса можно эмитировать дружественные методы как в языке С++
 *
 * @category  Core
 * @package   Core
 * @copyright 2007-2010 SARITASA LLC <info@saritasa.com>
 * @version   Release: 1.0.0
 */
class TraceBreaker implements \Serializable
{
    /**
    * Instance
    *
    * Экземпляр объекта
    *
    * @var object
    * @access private
    */
   private $obj = null;


   /**
    * The class constructor. Identifies the property $obj
    *
    * Конструктор класса. Определяет свойство $obj
    *
    * @param object $obj
    * @access public
    */
   public function __construct($obj)
   {
      $this->obj = $obj;
   }

   /**
    * Sets the value of the specified property of the object $obj
    *
    * Задаем значение указанному свойству объекта $obj
    *
    * @param string $param
    * @param mixed $value
    * @access public
    */
   public function __set($param, $value)
   {
      $this->obj->{$param} = $value;
   }

   /**
    * Get the value of the specified object's properties $obj
    *
    * Получаем значение указанного свойства объекта $obj
    *
    * @param string $param
    * @return mixed
    * @access public
    */
   public function __get($param)
   {
      return $this->obj->{$param};
   }

   /**
    * Checking the existence of the specified property from the object $obj
    *
    * Проверка существования указанного свойства у объекта $obj
    *
    * @param string $param
    * @return bool
    * @access public
    */
   public function __isset($param)
   {
      return isset($this->obj->{$param});
   }

   /**
    * Remove the specified property of the object $obj
    *
    * Удаляем указанное свойство объекта $obj
    *
    * @param string $param
    * @access public
    */
   public function __unset($param)
   {
      unset($this->obj->{$param});
   }

   /**
    * Calling this method with the object $obj set of parameters.
    *
    * Вызов указанного метода объекта $obj с набором параметров.
    *
    * @param string $method
    * @param array $params
    * @return mixed
    * @access public
    */
   public function __call($method, $params)
   {
      return call_user_func_array(array($this->obj, $method), $params);
   }

   /**
    * Calling this method with the object $obj set of parameters.
    *
    * Вызов указанного статического метода объекта $obj с набором параметров.
    *
    * @param string $method
    * @param array $params
    * @return mixed
    * @access public
    */
   public static function __callStatic($method, $params)
   {
      return call_user_func_array(array($this->obj, $method), $params);
   }

   /**
    * The method is called when the script tries to execute as a function of the object. Call the magic method __ invoke() object $obj.
    *
    * Метод вызывается когда скрипт пытается выполнить объект как функцию. Вызываем магический метод __invoke() объекта $obj.
    *
    * @return mixed
    * @access public
    */
   public function __invoke()
   {
      return call_user_func_array(array($this->obj, '__invoke'), func_get_args());
   }

   /**
    * The method is called when a script accesses an object as a string.
    *
    * Метод вызывается когда скрипт обращается к объекту как к строке.
    *
    * @return string
    * @access public
    */
   public function __toString()
   {
      return (string)$this->obj;
   }

   /**
    * Returns a string with a byte-stream representation of the object $obj
    *
    * Возвращает строку с байтово-поточным представлением объекта $obj
    *
    * @return string
    * @access public
    */
   public function serialize()
   {
      return serialize($this->obj);
   }

   /**
    * The method recovers the object $obj from a serialized string
    *
    * Метод восстанавливает объект $obj из сериализованной строки
    *
    * @param string $data
    * @access public
    */
   public function unserialize($data)
   {
      $this->obj = unserialize($data);
   }


   /**
    * The method clones the object
    *
    * Метод клонирует объект
    *
    * @access public
    */
   public function __clone()
   {
      $this->obj = clone $this->obj;
   }

   /**
    * The method destroys the object
    *
    * Метод разрушает объект
    *
    * @access public
    */
   public function __destruct()
   {
      $this->obj = null;
   }

   /**
    * The method returns TRUE, if a method from which it caused was summoned from the class $class and method $method, if defined
    *
    * Метод возвращает TRUE, если метод из которого она вызывается был вызван из класса $class и метода $method, если он задан
    *
    * @param string $class
    * @param string $method
    * @param int $level
    * @return bool
    * @access public
    */
   public static function isCalledFrom($class, $method = null, $level = 2)
   {
      $trace = debug_backtrace();
      return (strcasecmp($trace[$level]['class'], $class) == 0 && (!$method || strcasecmp($method, $trace[$level]['function']) == 0));
   }
}

?>