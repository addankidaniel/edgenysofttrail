#!/usr/bin/php
<?php

use ClickBlocks\PDFDaemon,
	ClickBlocks\Debug;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

date_default_timezone_set('UTC');

session_write_close();
ob_end_clean();

if( count( $argv ) < 2 || !isset( $argv[1] ) ) {
	exit( "Usage: {$argv[0]} '<json command string>'" );
}

//$cmdMsg = '{"cmd":"cache","params":{"inPath":"/var/www/sites/jlarson.edepoze/media/clients/2/cases/48/981/EN000102.pdf","hash":"3f58d1cc47a8817fb7d06b794145a0f4","page":0,"scale":"zoom4","skipSeed":true}}';
Debug::ErrorLog( "pdfdaemon-cli; msg: {$argv[1]}" );
$result = PDFDaemon::request( $argv[1] );
if( !$result->success && $result->error ) {
	Debug::ErrorLog( "pdfdaemon-cli; error: {$result->error}" );
}
