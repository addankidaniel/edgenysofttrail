<?php

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB;

require_once(__DIR__ . '/../connect.php');

/**
 * HOURLY cron script
 * this script should be called once in one hour
 *
 * this script should do the following functions:
 * - finish active depositions that were started more than 24 hours ago
*/

exit();	// ED-2961; deprecate finishing sessions automatically

date_default_timezone_set('UTC');

if( isset($_SERVER['REQUEST_METHOD']) ) {
	header('Content-Type: text/plain');
}

function wlog($txt) {
	echo print_r($txt,1).PHP_EOL;
}

$db = DB\ORM::getInstance()->getDB('db0');

// FINISH DEPOSITIONS after 24 hours

$rows = $db->rows('SELECT * FROM Depositions WHERE parentID IS NULL AND statusID="I" AND started IS NOT NULL AND (class != "WitnessPrep" AND class != "WPDemo" AND class != "Trial" AND class != "Demo Trial") AND NOW()>DATE_ADD(started, INTERVAL 24 HOUR)');
$depo = new DB\Depositions;
foreach( $rows as $row ) {
	$depo->assign($row);

	$depo->finish();
}

if( count($rows) ) {
	echo 'Finished '.count($rows).' depositions.'.PHP_EOL;
}

?>
