#!/usr/bin/php
<?php

use ClickBlocks\DB,
	ClickBlocks\Utils,
	ClickBlocks\Debug;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

date_default_timezone_set('UTC');

session_write_close();
ob_end_clean();

$verbose = FALSE;
$resellerID = 0;
$folderID = 0;

if( count( $argv ) > 1 ) {
	foreach( $argv as $arg ) {
		if( $arg === '-v' ) {
			$verbose = TRUE;
			continue;
		}
		if( strpos( $arg, '-f=' ) !== FALSE ) {
			list( $f, $fv ) = explode( '-f=', $arg );
			$folderID = (int)$fv;
			continue;
		}
		$resellerID = (int)$arg;
	}
}

if( $folderID ) {
	$verbose = FALSE;
}

if( !$resellerID ) {
	exit( "Usage: {$argv[ 0 ]} [-v] <Reseller ID> [-f=<Folder ID>]\n" );
}

Debug::ErrorLog( implode( ' ', $argv ) );
define( 'VERBOSE', $verbose );

try {
	$rdu = new ResellerDataUsage( $resellerID, $folderID );
} catch( \Exception $e ) {
	var_dump( $e );
}

class ResellerDataUsage {

	private $ID;
	/**
	 * @var \ClickBlocks\DB\Clients
	 */
	private $reseller;
	/**
	 * @var \ClickBlocks\DB\DB
	 */
	private $db;
	private $clients = [];
	private $totalSize = 0;
	private $totalDocs = 0;
	private $totalPages = 0;
	private $clientSize = 0;
	private $clientDocs = 0;
	private $clientPages = 0;
	private $report = [];
	private $cli;

	public function __construct( $resellerID, $folderID=NULL ) {
		$this->cli = $_SERVER[ 'PHP_SELF' ];
		$this->ID = (int)$resellerID;
		$this->reseller = new DB\Clients( $this->ID );
		if( !$this->reseller || !$this->reseller->ID || $this->reseller->ID != $this->ID ) {
			echo "Error: Reseller not found by ID: {$resellerID}\n";
			exit;
		}
		$this->db = DB\ORM::getInstance()->getDB( 'db0' );
		if( !$folderID ) {
			$this->log( "Reseller: {$this->reseller->name}" );
			$this->calcUsage();
			$hrTotalSize = Utils::readableFilesize( $this->totalSize, 1 );
			$result = "{$this->reseller->name}\t{$hrTotalSize}\t{$this->totalDocs}\t{$this->totalPages}";
			$this->log( $result, TRUE );
			Debug::ErrorLog( $result );
			echo 'Memory Usage: ', \ClickBlocks\Utils::readableFilesize( memory_get_usage() ), "\n";
		} else {
			$folder = new DB\Folders( $folderID );
			echo $this->folderPages( $folder );
		}
	}

	private function log( $msg, $verbose=FALSE ) {
		if( VERBOSE || $verbose ) {
			echo "{$msg}\n";
		}
	}

	private function getClients() {
		$orcClients = new DB\OrchestraClients();
		foreach( $orcClients->getResellerClients( $this->reseller->ID ) as $client ) {
			if( $client->deleted ) {
				//$this->log( "ResellerDataUsage::getClients; Info: Skipping Deleted Client -- {$client->ID}, {$client->name}" );
				continue;
			}
			//$this->log( "ResellerDataUsage::getClients; Info: Client -- {$client->ID}, {$client->name}" );
			$this->clients[] = $client;
		}
	}

	private function calcUsage() {
		$this->getClients();
		foreach( $this->clients as $client ) {
			$this->log( "Client: {$client->ID}, {$client->name}" );
			$this->caseUsage( $client->ID );
			$report = new stdClass();
			$report->ID = $client->ID;
			$report->name = $client->name;
			$report->size = $this->clientSize;
			$report->docs = $this->clientDocs;
			$this->report[ $client->ID ] = $report;
			$this->totalSize += $this->clientSize;
			$this->totalDocs += $this->clientDocs;
			$this->totalPages += $this->clientPages;
		}
	}

	private function getCases( $clientID ) {
		return $this->db->cols( 'SELECT ca.ID FROM Cases ca WHERE ca.clientID=:clientID AND ca.deleted IS NULL AND ca.class="Case"', [ 'clientID' => (int)$clientID ] );
	}

	private function caseUsage( $clientID ) {
		$this->clientSize = 0;
		$this->clientDocs = 0;
		$this->clientPages = 0;
		$caseIDs = $this->getCases( $clientID );
		//print_r( $caseIDs );
		foreach( $caseIDs as $caseID ) {
			$this->caseFolderUsage( $caseID );
			$this->sessionFolderUsage( $caseID );
		}
		$this->log( "clientSize: {$this->clientSize}, totalDocs: {$this->clientDocs}, totalPages: {$this->clientPages}" );
	}

	private function caseFolderUsage( $caseID ) {
		$caseFolderIDs = $this->db->cols( 'SELECT fl.ID FROM Folders fl WHERE fl.caseID=:caseID', [ 'caseID' => (int)$caseID ] );
		foreach( $caseFolderIDs as $folderID ) {
			$caseFolder = new DB\Folders( $folderID );
			$folderPath = $caseFolder->getFullPath();
			//$this->log( "{$folderPath}", TRUE );
			$folderSize = $this->folderSize( $folderPath );
			$folderDocs = $this->folderDocs( $folderID );
			$folderPages = $this->folderPages( $caseFolder );
			$this->log( "{$folderPath} -- folderSize: {$folderSize}, folderDocs: {$folderDocs}" );
			$this->clientSize += $folderSize;
			$this->clientDocs += $folderDocs;
			$this->clientPages += $folderPages;
		}
	}

	private function getSessions( $caseID ) {
		return $this->db->cols( 'SELECT d.ID FROM Depositions d WHERE d.caseID=:caseID AND d.deleted IS NULL AND d.class != "Demo" AND d.class != "WPDemo" AND d.class != "Demo Trial"', [ 'caseID' => (int)$caseID ] );
	}

	private function sessionFolderUsage( $caseID ) {
		foreach( $this->getSessions( $caseID ) as $sessionID ) {
			$folderIDs = $this->db->cols( 'SELECT fl.ID FROM Folders fl WHERE fl.depositionID=:sessionID', [ 'sessionID' => (int)$sessionID ] );
			foreach( $folderIDs as $folderID ) {
				$folder = new DB\Folders( $folderID );
				if( $folder->class === ClickBlocks\MVC\Edepo::FOLDERS_COURTESYCOPY ) {
					continue;
				}
				$folderPath = $folder->getFullPath();
				//$this->log( "{$folderPath}", TRUE );
				$folderSize = $this->folderSize( $folderPath );
				$folderDocs = $this->folderDocs( $folderID );
				//$folderPages = $this->folderPages( $folder );
				$cmd = "php {$this->cli} {$this->ID} -f={$folderID}";
				$folderPages = (int)shell_exec( $cmd );
				$this->log( "{$folderPath} -- folderSize: {$folderSize}, folderDocs: {$folderDocs}, folderPages: {$folderPages}" );
				$this->clientSize += $folderSize;
				$this->clientDocs += $folderDocs;
				$this->clientPages += $folderPages;
			}
		}
	}

	private function folderSize( $folderPath ) {
		if( !is_dir( $folderPath ) ) {
			$this->log( "ResellerDataUsage::getClients; Error: invalid folder path: {$folderPath}", TRUE );
			return 0;
		}
		$escFolderPath = escapeshellarg( $folderPath );
		$io = popen( "/usr/bin/du -s -b {$escFolderPath}", 'r' );
		$output = fgets( $io, 4096 );
		$size = (int)substr( $output, 0, strpos ( $output, "\t" ) );
		if( $size === 6 ) {
			//empty dir is 0
			$size = 0;
		}
		pclose ( $io );
		return $size;
	}

	private function folderDocs( $folderID ) {
		return (int)$this->db->col( 'SELECT COUNT(f.ID) as numFiles FROM Files f WHERE f.folderID=:folderID', [ 'folderID' => (int)$folderID ] );
	}

	private function folderPages( \ClickBlocks\DB\Folders $folder ) {
		$folderPages = 0;
		foreach( $folder->files as $file ) {
			if( $file->folderID != $folder->ID ) {
				continue;
			}
			$filePath = realpath( $file->getFullName() );
			$mimeType = mime_content_type( $filePath );
			if( $mimeType !== 'application/pdf' ) {
				continue;
			}
			if( !is_readable( $filePath ) ) {
				$this->log( "ResellerDataUsage::folderPages; Error: invalid file path: {$filePath}", TRUE );
				continue;
			}
			$escFilePath = escapeshellarg( $filePath );
			$output = shell_exec( "pdfinfo {$escFilePath} 2>&1" );
			$pages = explode( 'Pages:', $output );
			$numPages = (int)trim( $pages[ 1 ] );
			//$this->log( "ResellerDataUsage::folderPages; Info: {$filePath} -- {$numPages} pages" );
			$folderPages += $numPages;
		}
		return $folderPages;
	}

}
