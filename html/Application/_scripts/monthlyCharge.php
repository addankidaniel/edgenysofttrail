<?php

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB,
	ClickBlocks\MVC\Edepo;

require_once(__DIR__ . '/../connect.php');

date_default_timezone_get('UTC');

if (isset($_SERVER['REQUEST_METHOD'])) header('Content-Type: text/plain');

// Determine date range, it's always whole previous month (with 1 day buffer)
$month = foo(new \DateTime)->add(new \DateInterval('P1D'))->sub(new \DateInterval('P1M'));
$startDate = $month->format('Y-m-01');
$startDateTime = $startDate.' 00:00:00';
$endDate = $month->format('Y-m-t');
$endDateTime = $endDate.' 23:59:59';

function wlog($txt) {
   echo print_r($txt,1).PHP_EOL;
}

wlog($startDateTime.' - '.$endDateTime);

$db = DB\ORM::getInstance()->getDB('db0');

//auto-finish previous month depositions
$rows = $db->rows("SELECT ID FROM Depositions WHERE parentID IS NULL AND statusID='I' AND started IS NOT NULL AND (class != 'WitnessPrep' AND class != 'WPDemo') AND started BETWEEN '{$startDateTime}' AND '{$endDateTime}'");
foreach( $rows as $row ) {
	$depo = new DB\Depositions( $row['ID'] );

//	if( $depo->courtReporterEmail && $depo->courtReporterPassword !== $depo->courtReporterEmail && !$depo->isDemo() ) {
//		// send notification if email is set
//		Utils\EmailGenerator::sendCourtReporterInvite( $depo->ID );
//	}
	$depo->finish();
}

if( count($rows) ) {
	echo 'Finished '.count($rows).' depositions.'.PHP_EOL;
}

$pricing = $invoiceID = array();

// This function will charge Client and Reseller for some monthly used objects, if needed by Pricing model
function charge($info, $optionID, $quantity, $caseID = null, $depoID = null, $userID = null)
{
	global $db, $pricing, $invoiceID, $endDate;

	$client = (new DB\ServiceClients())->getByID( $info['clientID'] );

	$n = 0;

	// Charge Client
	if ($pricing['client'][$optionID])
	{
		$db->insert( 'InvoiceCharges', array(
			'invoiceID' => $invoiceID['client'],
			'clientID' => $client->ID,
			'optionID' => $optionID,
			'price'=> $quantity*(float)$pricing['client'][$optionID],
			'quantity' => $quantity,
			'caseID' => $caseID,
			'depositionID' => $depoID,
			'userID' => $userID,
			'created' => $endDate
		) );
		$n++;
	}

	// Check to see if a Reseller to charge
	if ($client->typeID !== Edepo::CLIENT_TYPE_ENT_CLIENT)
	{
		$resellerID = $info['resellerID'];
	} else {
		$resellerID = null;

		$optionIsEnterprise = $db->col( 'SELECT isEnterprise FROM LookupPricingOptions WHERE ID=:optionID', [':optionID' => $optionID] );
		if ($optionIsEnterprise == 1 && $depoID)
		{
			$resellerID = $db->col( 'SELECT enterpriseResellerID FROM Depositions WHERE ID=:depoID', [':depoID' => $depoID] );
		}
	}

	if ($resellerID)
	{
		// Charge Reseller
		if ($pricing['reseller'][$optionID])
		{
			$db->insert( 'InvoiceCharges', array(
				'invoiceID' => $invoiceID['reseller'],
				'clientID' => $resellerID,
				'childClientID' => $client->ID,
				'optionID' => $optionID,
				'price' => $quantity*(float)$pricing['reseller'][$optionID],
				'quantity' => $quantity,
				'caseID' => $caseID,
				'depositionID' => $depoID,
				'userID' => $userID,
				'created' => $endDate
			) );
			$n++;
		}
	}

	return $n;
}

$resellerMap = array();

$invC = $chaC = $cliC = 0;

// Query only those clients, that don't have Invoices for this month
$clients = $db->rows( "SELECT c.ID AS clientID, c.resellerID FROM Clients c
   INNER JOIN Clients rs ON (rs.ID=c.resellerID)
   LEFT JOIN Invoices inv ON (inv.clientID=c.ID AND inv.startDate=:startDate AND inv.endDate=:endDate)
   WHERE (c.typeID='C' OR c.typeID='EC') AND inv.ID IS NULL", ['startDate'=>$startDate,'endDate'=>$endDate] );

foreach ($clients as $info)
{
   /*
    * 1. User Subscription fee with per-month (option 3)
    * 2. Case monthly hosting (option 5)
    * 3. Depo Monthly hosting (option 7)
    */
   // Get Client pricing model && create new Invoices
   foreach (array('client','reseller') as $type) {
      // Allow new invoice for each reseller only once
      if ($type != 'reseller' || !isset($resellerMap[$info['resellerID']])) {
         $invoiceID[$type] = $db->insert('Invoices', array('clientID'=>$info[$type.'ID'], 'startDate'=>$startDate, 'endDate'=>$endDate));
         if ($type == 'reseller') $resellerMap[$info['resellerID']] = true;
         $invC ++;
      }
      $pricing[$type] = $db->couples('SELECT pmo.optionID,pmo.price FROM PricingModelOptions pmo WHERE pmo.clientID=? AND pmo.typeID="M" AND pmo.price>0', array($info[$type.'ID']));
   }
   //wlog($invoiceID);wlog($pricing);
   $numUsers = 0;
   if( isset( $pricing['client'][Edepo::PRICING_OPTION_SUBSCRIPTION] ) || isset( $pricing['reseller'][Edepo::PRICING_OPTION_SUBSCRIPTION] ) ) {
      // Query number of used Users
      $numUsers = (int)$db->col('SELECT COUNT(u.ID) FROM Users u
         WHERE u.clientID=:clientID AND (u.created BETWEEN :from AND :to OR
         ((u.deleted IS NULL OR u.deleted BETWEEN :from AND NOW()) AND
         (u.deactivated IS NULL OR u.deactivated BETWEEN :from AND NOW())))',
          array('clientID'=>$info['clientID'],'from'=>$startDateTime,'to'=>$endDateTime));
      if ($numUsers) {
        if ($pricing['client'][Edepo::PRICING_OPTION_SUBSCRIPTION] || $pricing['reseller'][Edepo::PRICING_OPTION_SUBSCRIPTION]) {
		  $chaC += charge($info, Edepo::PRICING_OPTION_SUBSCRIPTION, $numUsers);
		}
	  }
   }
   $params = ['resellerID'=>$info['resellerID'], 'optionID'=>Edepo::PRICING_OPTION_SUBSCRIPTION];
   $term = $db->col( 'SELECT pmo.typeID FROM PricingModelOptions pmo WHERE pmo.clientID=:resellerID AND pmo.optionID=:optionID', $params );
   if( $term == Edepo::PRICING_TYPE_MONTHLY ) {
     DB\ServicePlatformLog::addLog( $info['resellerID'], Edepo::PRICING_OPTION_SUBSCRIPTION, $numUsers, $info['clientID'], NULL, NULL, NULL, $endDate );
   }

   if( isset( $pricing['client'][Edepo::PRICING_OPTION_CASE_MONTHLY] ) || isset( $pricing['reseller'][Edepo::PRICING_OPTION_CASE_MONTHLY] ) ) {
      // Query used Cases
      $cases = $db->cols('SELECT c.ID FROM Cases c
         WHERE c.clientID=:clientID AND (c.created BETWEEN :from AND :to OR
         (c.deleted IS NULL OR c.deleted BETWEEN :from AND NOW()))',
          array('clientID'=>$info['clientID'],'from'=>$startDateTime,'to'=>$endDateTime));
      foreach ($cases as $caseID) {
		 if( $pricing['client'][Edepo::PRICING_OPTION_CASE_MONTHLY] || $pricing['reseller'][Edepo::PRICING_OPTION_CASE_MONTHLY] ) {
           $chaC += charge($info, Edepo::PRICING_OPTION_CASE_MONTHLY, 1, $caseID);
		 }
		 DB\ServicePlatformLog::addLog( $info['resellerID'], Edepo::PRICING_OPTION_CASE_MONTHLY, 1, $info['clientID'], NULL, $caseID, NULL, $endDate );
      }
   }
   if( isset( $pricing['client'][Edepo::PRICING_OPTION_DEPOSITION_MONTHLY] ) || isset( $pricing['reseller'][Edepo::PRICING_OPTION_DEPOSITION_MONTHLY] ) ) {
      // Query used Depositions (in condition that Case was enabled too
      $depos = $db->rows('SELECT d.ID,d.caseID FROM Depositions d
         INNER JOIN Cases c ON (d.caseID=c.ID)
         WHERE c.clientID=:clientID AND (c.created BETWEEN :from AND :to OR (c.deleted IS NULL OR c.deleted BETWEEN :from AND NOW())) AND
         (d.created BETWEEN :from AND :to OR (d.deleted IS NULL OR d.deleted BETWEEN :from AND NOW()))',
          array('clientID'=>$info['clientID'],'from'=>$startDateTime,'to'=>$endDateTime));
      foreach ($depos as $depo) {
		 if( $pricing['client'][Edepo::PRICING_OPTION_DEPOSITION_MONTHLY] || $pricing['reseller'][Edepo::PRICING_OPTION_DEPOSITION_MONTHLY] ) {
           $chaC += charge($info, Edepo::PRICING_OPTION_DEPOSITION_MONTHLY, 1, $depo['caseID'], $depo['ID']);
		 }
		 if( $depo['class'] == Edepo::DEPOSITION_CLASS_WITNESSPREP ) {
			 continue;
		 }
		 DB\ServicePlatformLog::addLog( $info['resellerID'], Edepo::PRICING_OPTION_DEPOSITION_MONTHLY, 1, $info['clientID'], NULL, $depo['caseID'], $depo['ID'], $endDate );
      }
   }

   foreach (array('client','reseller') as $type) {
      $db->execute('UPDATE InvoiceCharges SET invoiceID=:invoiceID WHERE clientID=:clientID AND created BETWEEN :from AND :to',
          array('invoiceID'=>$invoiceID[$type], 'clientID'=>$info[$type.'ID'], 'from'=>$startDateTime, 'to'=>$endDateTime));
   }
   $cliC++;
   // Query number of used Users
   //DB\ServiceInvoiceCharges::charge($clientID, $optionID, $quantity, $caseID, $depoID);

	$uploadsSQL = "SELECT COUNT(fi.ID) as uploads,c.ID as caseID,fo.depositionID,IFNULL(fi.sourceUserID,fi.createdBy) as userID FROM Files fi
		INNER JOIN Folders fo ON (fo.ID=fi.folderID)
		INNER JOIN Depositions d ON (d.ID=fo.depositionID)
		INNER JOIN Cases c ON (c.ID=d.caseID)
		WHERE fi.isUpload=1 AND c.clientID=:clientID AND d.class='Deposition' AND fi.created BETWEEN :startDate AND :endDate
		GROUP BY fo.depositionID,IFNULL(fi.sourceUserID,fi.createdBy)";
	$userUploads = $db->rows( $uploadsSQL, ['clientID'=>$info['clientID'], 'startDate'=>$startDateTime, 'endDate'=>$endDateTime] );
	foreach( $userUploads as $userUpload ) {
		$qty = (int)$userUpload['uploads'];
		$caseID = (int)$userUpload['caseID'];
		$depoID = (int)$userUpload['depositionID'];
		DB\ServicePlatformLog::addLog( $info['resellerID'], Edepo::PRICING_OPTION_UPLOAD_PER_DOC, $qty, $info['clientID'], $userID, $caseID, $depoID, $endDate );
	}
}

echo "Created $invC Invoices for $cliC Clients and ".count($resellerMap)." Resellers with $chaC charges total.\n";
