#!/usr/bin/php
<?php

use ClickBlocks\DB,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Debug;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

date_default_timezone_set('UTC');

session_write_close();
ob_end_clean();

if( count( $argv ) < 3 || !isset( $argv[ 1 ] ) || !isset( $argv[ 2 ] ) ) {
	exit( "Usage: {$argv[ 0 ]} [--drop|--rename] <PrimaryKey> [<New Exhibit Name>]\n" );
}

Debug::ErrorLog( implode( ' ', $argv ) );
//$db = DB\ORM::getInstance()->getDB( 'db0' );

switch( strtolower( $argv[ 1 ] ) ) {
	case '--drop':
		DropExhibit( $argv[ 2 ] );
		break;
	case '--rename':
		if( count( $argv ) < 4 || !isset( $argv[ 3 ] ) ) {
			exit( "Usage: {$argv[ 0 ]} --rename <PrimaryKey> <New Exhibit Name>\n" );
		}
		RenameExhibit( $argv[ 2 ], $argv[ 3 ] );
		break;
}

function DropExhibit( $_fileID ) {
	$fileID = (int)$_fileID;
	Debug::ErrorLog( "DropExhibit; fileID: {$fileID}" );
	if( !$fileID || !($fileID > 0) ) {
		Debug::ErrorLog( "DropExhibit; invalid primary key: {$_fileID}" );
		exit( "Error: invalid primary key: {$_fileID}\n" );
	}
	$exhibitData = exhibitRelatedData( $fileID );
	if( !$exhibitData ) {
		exit( "Error: invalid exhibit ID: {$fileID}\n" );
	}
	echo "Removing Exhibit; ID: {$exhibitData->file->ID}, Name: '{$exhibitData->file->name}'\n";
	$queue = [];
	$queue[$exhibitData->session->ID] = $exhibitData->file;
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	foreach( $exhibitData->childSessions as $session ) {
		$sql = 'SELECT f.ID FROM Files f
			INNER JOIN Folders fl ON (fl.ID=f.folderID)
			WHERE f.folderID=fl.ID AND fl.depositionID=:depoID AND fl.class=:exhibit AND f.isExhibit=1 AND f.name=:name';
		$exhibitID = (int)$db->col( $sql, ['depoID'=>$session->ID, 'exhibit'=>Edepo::FOLDERS_EXHIBIT, 'name'=>$exhibitData->file->name] );
		$file = new DB\Files( $exhibitID );
		if( $file && $file->ID && $file->ID == $exhibitID ) {
			$filename = $file->getFullName();
			Debug::ErrorLog( "DropExhibit; child session exhibit file: {$file->ID}, '{$filename}'" );
			$queue[$session->ID] = $file;
		}
	}
	if( property_exists( $exhibitData, 'courtesyCopy' ) ) {
		$ccFilename = $exhibitData->courtesyCopy->getFullName();
		Debug::ErrorLog( "DropExhibit; deleting Courtesy Copy: {$exhibitData->courtesyCopy->ID}, '{$ccFilename}'" );
		$exhibitData->courtesyCopy->delete();
	}
	foreach( $queue as $sessionID => $file ) {
		$filename = $file->getFullName();
		$exhibitHistory = $db->cols( "SELECT ID FROM ExhibitHistory WHERE depositionID=:depoID AND exhibitFileID=:fileID", ['depoID'=>$sessionID, 'fileID'=>$file->ID] );
		if( is_array( $exhibitHistory ) && $exhibitHistory ) {
			$IDs = implode( ',', $exhibitHistory );
			Debug::ErrorLog( "DropExhibit; deleting ExhibitHistory: {$IDs} for sessionID: {$sessionID} exhibitID: {$file->ID}" );
			$limit = count( $exhibitHistory );
			$rowsAffected = $db->execute( "DELETE FROM ExhibitHistory WHERE ID IN ({$IDs}) LIMIT {$limit}" );
			Debug::ErrorLog( "DropExhibit; deleting ExhibitHistory: {$rowsAffected} rows affected" );
		}
		Debug::ErrorLog( "DropExhibit; deleting Exhibit: {$file->ID}, '{$filename}'" );
		$file->delete();
	}
}

function RenameExhibit( $_fileID, $renameTo ) {
	$fileID = (int)$_fileID;
	Debug::ErrorLog( "RenameExhibit; fileID: {$fileID}" );
	if( !$fileID || !($fileID > 0) ) {
		Debug::ErrorLog( "RenameExhibit; invalid primary key: {$_fileID}" );
		exit( "Error: invalid primary key: {$_fileID}\n" );
	}
	if( !$renameTo || !(mb_strlen( $renameTo ) > 4) ) {
		Debug::ErrorLog( "RenameExhibit; invalid filename: {$renameTo}" );
		exit( "Error: invalid filename: {$renameTo}\n" );
	}
	$exhibitData = exhibitRelatedData( $fileID );
	if( !$exhibitData ) {
		exit( "Error: invalid exhibit ID: {$fileID}\n" );
	}
	echo "Renaming Exhibit; ID: {$exhibitData->file->ID}, Name: '{$exhibitData->file->name}' To: '{$renameTo}'\n";
	if( property_exists( $exhibitData, 'courtesyCopy' ) ) {
		$exPathinfo = pathinfo( $renameTo );
		$ccName = "{$exPathinfo['filename']}_Courtesy.{$exPathinfo['extension']}";
		$ccFilename = $exhibitData->courtesyCopy->getFullName();
		Debug::ErrorLog( "RenameExhibit; renaming Courtesy Copy: {$exhibitData->courtesyCopy->ID}, '{$ccFilename}' To: {$ccName}" );
		$exhibitData->courtesyCopy->name = $ccName;
		$exhibitData->courtesyCopy->update();
	}
	$queue = [];
	$queue[$exhibitData->session->ID] = $exhibitData->file;
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	foreach( $exhibitData->childSessions as $session ) {
		$sql = 'SELECT f.ID FROM Files f
			INNER JOIN Folders fl ON (fl.ID=f.folderID)
			WHERE f.folderID=fl.ID AND fl.depositionID=:depoID AND fl.class=:exhibit AND f.isExhibit=1 AND f.name=:name';
		$exhibitID = (int)$db->col( $sql, ['depoID'=>$session->ID, 'exhibit'=>Edepo::FOLDERS_EXHIBIT, 'name'=>$exhibitData->file->name] );
		$file = new DB\Files( $exhibitID );
		if( $file && $file->ID && $file->ID == $exhibitID ) {
			$filename = $file->getFullName();
			Debug::ErrorLog( "RenameExhibit; child session exhibit file: {$file->ID}, '{$filename}'" );
			$queue[$session->ID] = $file;
		}
	}
	foreach( $queue as $sessionID => $file ) {
		$exhibitHistoryIDs = $db->cols( "SELECT ID FROM ExhibitHistory WHERE depositionID=:depoID AND exhibitFileID=:fileID", ['depoID'=>$sessionID, 'fileID'=>$file->ID] );
		if( is_array( $exhibitHistoryIDs ) && $exhibitHistoryIDs ) {
			foreach( $exhibitHistoryIDs as $ehID ) {
				$exhibitHistory = new DB\ExhibitHistory( $ehID );
				if( !$exhibitHistory || !$exhibitHistory->ID || $exhibitHistory->ID != $ehID ) {
					Debug::ErrorLog( "RenameExhibit; unable to determine ExhibitHistory by ID: {$ehID}" );
					continue;
				}
				$exhibitHistory->exhibitFilename = $renameTo;
				$exhibitHistory->update();
				Debug::ErrorLog( "RenameExhibit; renamed ExhibitHistory: {$ehID} for sessionID: {$sessionID} exhibitID: {$file->ID}" );
			}
		}
		$filename = $file->getFullName();
		Debug::ErrorLog( "RenameExhibit; renaming: {$file->ID}, '{$filename}', To: '{$renameTo}'" );
		$file->name = $renameTo;
		$file->update();
	}
}

function exhibitRelatedData( $fileID ) {
	$exhibit = new DB\Files( $fileID );
	if( !$exhibit || !$exhibit->ID || $exhibit->ID != $fileID || !$exhibit->isExhibit ) {
		Debug::ErrorLog( "exhibitRelatedData; invalid fileID: {$fileID}" );
		return FALSE;
	}
	$folder = new DB\Folders( $exhibit->folderID );
	if( !$folder || !$folder->ID || $folder->ID != $exhibit->folderID ) {
		Debug::ErrorLog( "exhibitRelatedData; unable to determine folder by ID: {$exhibit->folderID}" );
		return FALSE;
	}
	if( $folder->isCaseFolder() ) {
		Debug::ErrorLog( "exhibitRelatedData; file is a member of a Case Folder and is unsupported." );
		return FALSE;
	}
	if( $folder->class != Edepo::FOLDERS_EXHIBIT ) {
		Debug::ErrorLog( "exhibitRelatedData; file is not a member of an Exhibit folder." );
		return FALSE;
	}
	$session =  new DB\Depositions( $folder->depositionID );
	if( !$session || !$session->ID || $session->ID != $folder->depositionID ) {
		Debug::ErrorLog( "exhibitRelatedData; unable to determine session by ID: {$folder->depositionID}" );
		return FALSE;
	}
	$parentSession = ($session->parentID ? new DB\Depositions( $session->parentID ) : $session);
	if( !$parentSession || !$parentSession->ID || ($parentSession->ID != $session->ID && $parentSession->ID != $session->parentID) ) {
		Debug::ErrorLog( "exhibitRelatedData; unable to determine parent session by ID: {$session->parentID}" );
		return FALSE;
	}
	$exhibitData = new \stdClass();
	$exhibitData->file = $exhibit;
	$exhibitData->folder = $folder;
	if( !in_array( $parentSession->class, Edepo::trialClasses() ) ) {
		$db = DB\ORM::getInstance()->getDB( 'db0' );
		$ccFolderID = (int)$db->col( 'SELECT ID FROM Folders WHERE depositionID=:depoID AND class=:cc', ['depoID'=>(int)$parentSession->ID, 'cc'=>Edepo::FOLDERS_COURTESYCOPY] );
		if( !$ccFolderID ) {
			Debug::ErrorLog( 'exhibitRelatedData; unable to determine Courtesy Copy folder' );
			return FALSE;
		}
		$exPathinfo = pathinfo( $exhibit->name );
		$ccName = "{$exPathinfo['filename']}_Courtesy.{$exPathinfo['extension']}";
		$ccFileID = (int)$db->col( 'SELECT ID FROM Files WHERE folderID=:ccFolder AND name=:ccName', ['ccFolder'=>$ccFolderID, 'ccName'=>$ccName] );
		if( !$ccFileID ) {
			Debug::ErrorLog( "exhibitRelatedData; unable to determine Courtesy Copy by name: {$ccName}" );
			return FALSE;
		}
		$ccFile = new DB\Files( $ccFileID );
		if( !$ccFile || !$ccFile->ID || $ccFile->ID != $ccFileID ) {
			Debug::ErrorLog( "exhibitRelatedData; unable to determine Courtesy Copy by ID: {$ccFileID}" );
			return FALSE;
		}
		$exhibitData->courtesyCopy = $ccFile;
	}
	$exhibitData->session = $parentSession;
	$exhibitData->childSessions = [];
	foreach( $parentSession->childrenDepositions1 as $child ) {
		$exhibitData->childSessions[] = $child;
	}
	$case = new DB\Cases( $parentSession->caseID );
	if( !$case || !$case->ID || $case->ID != $parentSession->caseID ) {
		Debug::ErrorLog( "exhibitRelatedData; unable to determine case by ID: {$parentSession->caseID}" );
		return FALSE;
	}
	$exhibitData->case = $case;
	$client = new DB\Clients( $case->clientID );
	if( !$client || !$client->ID || $client->ID != $case->clientID ) {
		Debug::ErrorLog( "exhibitRelatedData; unable to determine client by ID: {$case->clientID}" );
		return FALSE;
	}
	$exhibitData->client = $client;
	$reseller = new DB\Clients( $client->resellerID );
	if( !$reseller || !$reseller->ID || $reseller->ID != $client->resellerID ) {
		Debug::ErrorLog( "exhibitRelatedData; unable to determine reseller by ID: {$client->resellerID}" );
		return FALSE;
	}
	$exhibitData->reseller = $reseller;
	echo "Reseller: {$reseller->name}\n";
	echo "Client: {$client->name}\n";
	echo "Case ID: {$case->ID} Name: '{$case->name}', Matter/Number: {$case->number}\n";
	echo "Session ID: {$parentSession->ID}, Session: '{$parentSession->depositionOf}'\n";
	//echo "Exhibit ID: {$exhibit->ID} Name: '{$exhibit->name}'\n";
	return $exhibitData;
}
