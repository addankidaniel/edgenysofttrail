#!/usr/bin/php
<?php

use ClickBlocks\DB,
	ClickBlocks\Elasticsearch,
	ClickBlocks\Debug;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../../connect.php' );

date_default_timezone_set( 'UTC' );
session_destroy();
ob_end_clean();
ob_implicit_flush( TRUE );

$clientID = null;
$folderID = null;

if( count( $argv ) > 1 ) {
	foreach( $argv as $arg ) {
		if( strpos( $arg, '-c=' ) !== FALSE ) {
			list( $c, $cv ) = explode( '-c=', $arg );
			$clientID = (int)$cv;
		}
		if( strpos( $arg, '-f=' ) !== FALSE ) {
			list( $f, $fv ) = explode( '-f=', $arg );
			$folderID = (int)$fv;
		}
	}
}

if( !$clientID && !$folderID ) {
	getClients(); //main
} elseif( $clientID && $folderID ) {
	$indexClient = new IndexClient( $clientID ); //fork
	$indexClient->indexFolder( $folderID );
} else {
	$indexClient = new IndexClient( $clientID ); //fork
	$indexClient->indexCases();
}

function getClients() {
	$cli = $_SERVER['PHP_SELF'];
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$clientIDs = $db->cols( 'SELECT c.ID FROM Clients c WHERE (c.typeID="C" OR c.typeID="EC" OR c.typeID="CRT") AND c.deleted IS NULL ORDER BY c.resellerID,c.ID' );
	if( !$clientIDs || !is_array( $clientIDs ) ) {
		exit( 'No clients found!' );
	}
	foreach( $clientIDs as $i => $clientID ) {
		$totalClients = count( $clientIDs );
		++$i;
		$client = new DB\Clients( $clientID );
		echo "[{$i}/{$totalClients}] ClientID: {$client->ID}, Type: {$client->typeID}, Name: {$client->name}\n";
		$cmd = "php {$cli} -c={$clientID}";
		echo shell_exec( $cmd );
	}
}

class IndexClient {

	protected $db;
	/**
	 * @var \ClickBlocks\DB\Clients
	 */
	protected $client;

	public function __construct( $clientID ) {
		$this->client = new DB\Clients( $clientID );
		$this->db = DB\ORM::getInstance()->getDB( 'db0' );
	}

	public function indexCases() {
		//echo "ClientID: {$this->client->ID}, Type: {$this->client->typeID}, Name: {$this->client->name}\n";
		$caseIDs = $this->db->cols( 'SELECT ID FROM Cases WHERE clientID=:clientID AND deleted IS NULL', ['clientID' => (int)$this->client->ID] );
		foreach( $caseIDs as $caseID ) {
			$this->indexCase( $caseID );
		}
		echo 'Memory Usage: ', \ClickBlocks\Utils::readableFilesize( memory_get_usage() ), "\n";
	}

	protected function indexCase( $caseID ) {
		$caseFolderIDs = $this->db->cols( 'SELECT ID FROM Folders WHERE caseID=:caseID', ['caseID' => (int)$caseID] );
		foreach( $caseFolderIDs as $folderID ) {
			// $this->indexFolder( $folderID );
			$cli = $_SERVER['PHP_SELF'];
			$cmd = "php {$cli} -c={$this->client->ID} -f={$folderID}";
			echo shell_exec( $cmd );
		}
		$sessionIDs = $this->db->cols( 'SELECT ID FROM Depositions WHERE caseID=:caseID AND deleted IS NULL', ['caseID' => (int)$caseID] );
		foreach( $sessionIDs as $sessionID ) {
			$this->indexSession( $sessionID );
		}
		// $memUsage = memory_get_usage();
		// echo 'Memory Usage -- indexCase: ', \ClickBlocks\Utils::readableFilesize( $memUsage ), "\n";
	}

	protected function indexSession( $sessionID ) {
		$folderIDs = $this->db->cols( 'SELECT ID FROM Folders WHERE depositionID=:sessionID', ['sessionID' => (int)$sessionID] );
		foreach( $folderIDs as $folderID ) {
			// $this->indexFolder( $folderID );
			$cli = $_SERVER['PHP_SELF'];
			$cmd = "php {$cli} -c={$this->client->ID} -f={$folderID}";
			echo shell_exec( $cmd );
		}
	}

	public function indexFolder( $folderID ) {
		$fileIDs = $this->db->cols( 'SELECT ID FROM Files WHERE folderID=:folderID', ['folderID' => (int)$folderID] );
		foreach( $fileIDs as $fileID ) {
			$file = new DB\Files( $fileID );
			if( !$file || !$file->ID || $file->ID != $fileID ) {
				Debug::ErrorLog( "IndexClient::indexFolder: {$folderID}, fatal error with file: {$fileID}" );
				continue;
			}
			if( !$file->filesize || !$file->mimeType || !$file->hash ) {
				Debug::ErrorLog( "IndexClient::indexFolder: {$folderID}, fileID: {$fileID}, '{$file->name}', updating metadata" );
				$file->setMetadata( TRUE );
			}
			try {
				Elasticsearch::indexDocument( $file->ID );
			} catch( \Exception $e ) {
				$msg = $e->getMessage();
				Debug::ErrorLog( "IndexClient::indexFolder: {$folderID}, fileID: {$fileID}, -- exception: {$msg}" );
				echo "Exception indexing fileID: {$file->ID} in folderID: {$folderID}, {$msg}\n";
			}
		}
		// $memUsage = memory_get_usage();
		// echo 'Memory Usage -- indexFolder: ', \ClickBlocks\Utils::readableFilesize( $memUsage ), "\n";
	}

}
