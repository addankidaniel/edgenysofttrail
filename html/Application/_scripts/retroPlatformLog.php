<?php

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB,
	ClickBlocks\DB\ServicePlatformLog,
	ClickBlocks\MVC\Edepo;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

/**
 * Back fill PlatformLog from InvoiceCharges
 */

date_default_timezone_set('UTC');

function wlog( $txt )
{
	echo print_r( $txt, TRUE ) . PHP_EOL;
}

$db = DB\ORM::getInstance()->getDB( 'db0' );

session_destroy();
ob_end_clean();

//reportMemoryUsage( 1 );

$clientIDs = $db->cols( "SELECT c.ID FROM Clients c
	INNER JOIN Clients r ON (r.ID=c.resellerID)
	LEFT JOIN PlatformLog pl ON (pl.clientID=c.ID AND pl.optionID=1)
	WHERE (c.typeID='C' OR c.typeID='EC') AND c.deleted IS NULL
	AND r.typeID = 'R' AND r.deleted IS NULL AND r.deactivated IS NULL
	AND pl.ID IS NULL
	ORDER BY c.resellerID,c.created,c.ID" );

unset( $db );

//reportMemoryUsage( 2 );

$i = 0;

foreach( $clientIDs as $clientID ) {
	if( $i >= 5 ) {
		exit;
	}
	processClientHistory( $clientID );
	++$i;
}

function getMonthlyRanges( $created )
{
	$ranges = [];
	$now = time();
	$timeCreated = strtotime( $created );
	$createdMonth = (int)date( 'm', $timeCreated );
	$createdYear = (int)date( 'Y', $timeCreated );
	$lastDay = (int)date( 't', $timeCreated );
	$monthlyTime = strtotime( "{$createdYear}-{$createdMonth}-{$lastDay} 23:59:59" );
	$startDate = date( 'Y-m-01 00:00:00', $monthlyTime );
	for( $i=$createdMonth; $monthlyTime < $now; ++$i ) {
		$range = new \stdClass();
		$range->startDate = $startDate;
		if( $i > 12 ) {
			$i = 1;
			++$createdYear;
		}
		$lastDay = date( 't', strtotime( "{$createdYear}-{$i}-01 23:59:59" ) );
		$monthlyTime = strtotime( "{$createdYear}-{$i}-{$lastDay} 23:59:59" );
		if( $monthlyTime >= $now ) {
			break;
		}
		$range->startMonth = date( 'Y-m-01 00:00:00', $monthlyTime );
		$range->endDate = date( 'Y-m-t 23:59:59', $monthlyTime );
		$ranges[] = $range;
	}
	return $ranges;
}

function reportMemoryUsage( $ID )
{
	wlog( "{$ID} -- Memory Usage: " . number_format( memory_get_usage() ) );
}

function processClientHistory( $clientID )
{
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
	$orchDA = new \ClickBlocks\DB\OrchestraDepositionAttendees();

	$client = new DB\Clients( $clientID );
	if( !$client || !$client->ID || $client->ID != $clientID || !$client->resellerID ) {
		wlog( "Invalid ClientID: {$clientID}: {$client->name}" );
		return;
	}
	$reseller = $client->parentClients1[0];

	wlog( "Processing history for ({$reseller->ID}) {$reseller->name} -- ({$client->ID}) {$client->name}" );

	//Edepo::PRICING_OPTION_BASE_LICENSING / Edepo::PRICING_OPTION_SETUP
	ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_BASE_LICENSING, 1, $client->ID, NULL, NULL, NULL, $client->created );
	ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_SETUP, 1, $client->ID, NULL, NULL, NULL, $client->created );

	//Edepo::PRICING_OPTION_SUBSCRIPTION
	$users = $db->rows( "SELECT ID,created FROM Users WHERE clientID=:clientID", ['clientID'=>$client->ID] );
	$pmoUserSubscription = $orchPMO->getByClientAndOption( $reseller->ID, Edepo::PRICING_OPTION_SUBSCRIPTION );
	if( $pmoUserSubscription['typeID'] == Edepo::PRICING_TYPE_ONETIME ) {
		foreach( $users as $user ) {
			$userID = (int)$user['ID'];
			ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_SUBSCRIPTION, 1, $client->ID, $userID, NULL, NULL, $user['created'] );
		}
	} else {
		$userID = (int)$user['ID'];
		$monthly = getMonthlyRanges( $client->created );
		foreach( $monthly as $range ) {
			$sql = 'SELECT COUNT(ID) FROM Users
				WHERE clientID=:clientID AND created BETWEEN :startDate AND :endDate
				AND (deleted IS NULL OR deleted BETWEEN :startMonth AND :endDate OR deleted > :endDate)
				AND (deactivated IS NULL OR deactivated BETWEEN :startMonth AND :endDate OR deactivated > :endDate)';
			$numUsers = (int)$db->col( $sql, ['clientID'=>$client->ID, 'startDate'=>$range->startDate, 'endDate'=>$range->endDate, 'startMonth'=>$range->startMonth] );
			ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_SUBSCRIPTION, $numUsers, $client->ID, NULL, NULL, NULL, $range->endDate );
		}
	}

	unset( $users, $user, $pmoUserSubscription, $userID, $monthly, $range, $numUsers );
	gc_collect_cycles();
//	reportMemoryUsage( 3 );

	//Edepo::PRICING_OPTION_CASE_SETUP
	$caseIDs = $db->cols( "SELECT ID FROM Cases WHERE clientID=:clientID AND class='Case'", ['clientID'=>$client->ID] );
	foreach( $caseIDs as $caseID ) {
		$case = new DB\Cases( $caseID );
		ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_CASE_SETUP, 1, $client->ID, $case->createdBy, $caseID, NULL, $case->created );

		//Edepo::PRICING_OPTION_CASE_MONTHLY
		foreach( getMonthlyRanges( $case->created ) as $range ) {
			ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_CASE_MONTHLY, 1, $client->ID, $case->createdBy, $caseID, NULL, $range->endDate );
		}
		unset( $range );
		gc_collect_cycles();

		//Edepo::PRICING_OPTION_DEPOSITION_SETUP
		$depoIDs = $db->cols( "SELECT ID FROM Depositions WHERE caseID=:caseID AND class='Deposition'", ['caseID'=>$caseID] );
		foreach( $depoIDs as $depositionID ) {
			$deposition = new DB\Depositions( $depositionID );
			ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_DEPOSITION_SETUP, 1, $client->ID, $deposition->createdBy, $caseID, $depositionID, $deposition->created );

			//Edepo::PRICING_OPTION_UPLOAD_PER_DOC -- no historical data

			//Edepo::PRICING_OPTION_DEPOSITION_MONTHLY
			foreach( getMonthlyRanges( $deposition->created ) as $range ) {
				ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_DEPOSITION_MONTHLY, 1, $client->ID, $deposition->createdBy, $caseID, $depositionID, $range->endDate );
			}
			unset( $deposition, $range );
			gc_collect_cycles();
		}

		unset( $depoIDs, $depositionID );
		gc_collect_cycles();
//		reportMemoryUsage( 4 );

		//Edepo::PRICING_OPTION_WITNESSPREP_ATTENDEE
		$prepIDs = $db->cols( "SELECT ID FROM Depositions WHERE caseID=:caseID AND class='WitnessPrep'", ['caseID'=>$caseID] );
		foreach( $prepIDs as $depositionID ) {
			$wprep = new DB\Depositions( $depositionID );
			$attendees = $orchDA->getAttendeesByRoles( $depositionID, [Edepo::DEPOSITIONATTENDEE_ROLE_MEMBER] );
			foreach( $attendees as $attendee ) {
				ServicePlatformLog::addLog( $client->resellerID, Edepo::PRICING_OPTION_WITNESSPREP_ATTENDEE, 1, $client->ID, $attendee['userID'], $caseID, $depositionID, $wprep->started );
			}
			unset( $wprep, $attendees, $attendee );
			gc_collect_cycles();
		}
		unset( $prepIDs, $depositionID, $deposition );
		gc_collect_cycles();
	}
	DB\ORM::getInstance()->cleanCache();
	DB\Service::cleanCache();
	unset( $client, $clientID, $reseller, $caseIDs, $caseID, $case, $db, $orchPMO, $orchDA );
	gc_collect_cycles();
	reportMemoryUsage( 5 );
}