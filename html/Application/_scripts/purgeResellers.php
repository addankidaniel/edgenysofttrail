<?php

namespace ClickBlocks\MiscScripts;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB,
	Exception;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

ob_end_flush();

class PurgeResellers {

	/**
	 * @var \ClickBlocks\DB\DB
	 */
	private $db = null;

	/**
	 * @var \ClickBlocks\Core\Register
	 */
	private $reg = null;

	/**
	 * @var \ClickBlocks\Core\Config
	 */
	private $config = null;

	private $unlinkChildDepositions = FALSE;
	private $rowWidth = 0;
	private $resellerColumns = [
		'ID'=>['desc'=>'ID', 'width'=>1],
		'name'=>['desc'=>'Reseller Name', 'width'=>1],
		'contactName'=>['desc'=>'Primary Contact','width'=>1],
		'contactEmail'=>['desc'=>'Contact Email', 'width'=>1],
		'URL'=>['desc'=>'URL', 'width'=>1]
	];

	public function __construct()
	{
		$this->db = DB\ORM::getInstance()->getDB( 'db0' );
		$this->db->catchException = TRUE;
		$this->reg = Core\Register::getInstance();
		$this->config = $this->reg->config;
		$this->init();
	}

	private function init()
	{
		$resellers = $this->db->rows( "SELECT ID,name,contactName,contactEmail,URL FROM Clients WHERE typeID IN ('R') AND deactivated IS NOT NULL" );
		if( $resellers ) {
			array_walk( $resellers, [$this, 'getColumnWidth'] );
			foreach( $this->resellerColumns as $colIdx => $column ) {
				$this->rowWidth += $column['width'] + 3; // left & right margins and column seperator
			}
			$this->rowWidth += 2;	//left & right margin (space)
			echo str_repeat( '=', $this->rowWidth ), "\n DEACTIVATED RESELLERS \n", str_repeat( '=', $this->rowWidth ), "\n";
			echo ' |';
			foreach( $this->resellerColumns as $colIdx => $column ) {
				echo ' ', str_pad( $column['desc'], $column['width'] ), ' |';
			}
			echo "\n";
			echo str_repeat( '-', $this->rowWidth ), "\n";
			foreach( $resellers as $reseller ) {
				echo ' |';
				foreach( $this->resellerColumns as $colIdx => $column ) {
					echo ' ', str_pad( $reseller[$colIdx], $column['width'] ), ' |';
				}
				echo "\n";
			}
			echo str_repeat( '-', $this->rowWidth ), "\n\n";
			echo "Enter ID of Reseller(s) to purge (comma seperated): ";
			$line = fgets( STDIN );
			if( trim( $line ) ) {
				$resellerIDs = [];
				$inputIDs = explode( ',', $line );
				foreach( $inputIDs as $resellerID ) {
					$resellerID = (int)trim( $resellerID );
					foreach( $resellers as $reseller ) {
						if( (int)$reseller['ID'] === $resellerID ) {
							$resellerIDs[] = $resellerID;
							break;
						}
					}
				}
				unset( $reseller, $resellerID, $inputIDs, $line );
				if( $resellerIDs ) {
					foreach( $resellerIDs as $resellerID ) {
						$this->removeResellerByID( $resellerID );
						echo "\n";
					}
				}
			}
		} else {
			exit( "No deactivated Resellers found!\n\n" );
		}
	}
	
	private function getColumnWidth( $row, $key )
	{
		foreach( $this->resellerColumns as $colIdx => $column ) {
			$this->resellerColumns[$colIdx]['width'] = max( mb_strlen( $row[$colIdx] ), mb_strlen( $column['desc'] ),  $column['width'] );
		}
	}
	
	private function remove( $sql, $data )
	{
		$success = FALSE;
		try {
			$success = $this->db->execute( $sql, $data );
			echo $this->db->affectedRows, " row(s)\n";
			if( $success == FALSE && $this->db->affectedRows == 0 ) $success = TRUE;
		} catch( Exception $e ) {
			echo "\n", $e->getMessage(), "\n";
			echo json_encode( ['sql'=>$sql, 'data'=>$data] ), "\n";
			return FALSE;
		}
		return $success;
	}


	private function removeResellerByID( $ID )
	{
		$reseller = $this->db->row( "SELECT * FROM Clients WHERE ID=:ID AND typeID IN ('R') AND deactivated IS NOT NULL", ['ID'=>$ID] );
		if( !$reseller || !is_array( $reseller ) || $reseller['ID'] != $ID ) {
			return;
		}
		echo "\nRemoving Reseller: ({$reseller['ID']}) {$reseller['name']}\n";
		$success = $this->removeInvoiceCharges( $reseller['ID'] );
		if( $success ) $success = $this->removeInvoices( $reseller['ID'] );
		$clientIDs = $this->db->cols( 'SELECT ID FROM Clients WHERE resellerID=:ID', ['ID'=>$reseller['ID']] );
		if( $clientIDs ) {
			foreach( $clientIDs as $clientID ) {
				$success = $this->removeClientByID( $clientID );
				echo "\n";
			}
		}
		if( $success ) $success = $this->removePricingModelOptions( $reseller['ID'] );
		if( $success ) {
			echo "-- Removing Reseller ClientNotifications: ";
			$success = $this->remove( "DELETE ClientNotifications FROM ClientNotifications INNER JOIN Users ON (Users.ID = ClientNotifications.userID) WHERE Users.clientID=:clientID", ['clientID'=>$reseller['ID']] );
		}
		if( $success ) {
			echo "-- Removing Reseller User: ";
			$success = $this->remove( "DELETE FROM Users WHERE clientID=:ID AND typeID IN ('R') LIMIT 1", ['ID'=>$reseller['ID']] );
		}
		if( $success ) {
			echo "-- Removing Reseller Client: ";
			$success = $this->remove( "DELETE FROM Clients WHERE ID=:ID AND typeID IN ('R') LIMIT 1", ['ID'=>$reseller['ID']] );
		}
	}

	private function removeClientByID( $clientID )
	{
		$client = $this->db->row( 'SELECT c.ID, c.name FROM Clients c INNER JOIN Clients r ON (c.resellerID = r.ID) WHERE c.ID=:ID AND r.deactivated IS NOT NULL', ['ID'=>$clientID] );
		if( !$client ) return TRUE;
		$success = TRUE;
		echo "\n-- Client: ({$client['ID']}) {$client['name']}\n";
		$success = $this->removeClientDatasources( $clientID );
		if( $success ) $success = $this->removeInvoiceCharges( $clientID );
		if( $success ) $success = $this->removeInvoices( $clientID );
		if( $success ) $success = $this->removePricingModelOptions( $clientID );
		if( $success ) $success = $this->removeCases( $clientID );
		if( $success ) $success = $this->removeClientUsers( $clientID );
		if( $success ) {
			echo "-- Removing Client: ";
			$success = $this->remove( "DELETE FROM Clients WHERE ID=:ID AND typeID='C' LIMIT 1", ['ID'=>$clientID] );
		}
		try {
			$clientFolderPath = "{$_SERVER['DOCUMENT_ROOT']}{$this->config->dirs['media']}/clients/{$clientID}";
			if( is_dir( $clientFolderPath ) ) {
				@rmdir( $clientFolderPath );
			}
		} catch( Exception $e ) {
			//non-empty folder
		}
		return $success;
	}

	private function removeClientDatasources( $clientID )
	{
		$datasources = $this->db->cols( 'SELECT ID FROM Datasources WHERE clientID=:clientID', ['clientID'=>$clientID] );
		if( !$datasources ) return TRUE;
		foreach( $datasources as $datasourceID ) {
			echo "-- Removing DatasourceUsers: ";
			$success = $this->remove( 'DELETE FROM DatasourceUsers WHERE datasourceID=:dsID', ['dsID'=>$datasourceID] );
		}
		echo "-- Removing Datasources: ";
		$success = $this->remove( 'DELETE FROM Datasources WHERE clientID=:clientID', ['clientID'=>$clientID] );
		return $success;
	}

	private function removeInvoices( $clientID )
	{
		echo "-- Removing Invoices: ";
		return $this->remove( 'DELETE FROM Invoices WHERE clientID=:clientID', ['clientID'=>$clientID] );
	}

	private function removeInvoiceCharges( $clientID )
	{
		echo "-- Removing Invoice Charges: ";
		return $this->remove( 'DELETE FROM InvoiceCharges WHERE clientID=:clientID', ['clientID'=>$clientID] );
	}

	private function removePricingModelOptions( $clientID )
	{
		echo "-- Removing Pricing Model Options: ";
		return $this->remove( 'DELETE FROM PricingModelOptions WHERE clientID=:clientID', ['clientID'=>$clientID] );
	}

	private function removeCases( $clientID )
	{
		$success = $this->removeDepositions( $clientID );
		if( !$success ) return FALSE;

		$caseIDs = $this->db->cols( 'SELECT ID FROM Cases WHERE clientID=:clientID', ['clientID'=>$clientID] );
		if( $caseIDs ) {
			foreach( $caseIDs as $caseID ) {
				try {
					$caseFolderPath = "{$_SERVER['DOCUMENT_ROOT']}{$this->config->dirs['media']}/clients/{$clientID}/cases/{$caseID}";
					if( is_dir( $caseFolderPath ) ) {
						$ok = @rmdir( $caseFolderPath );
						if( $ok ) {
							$casesFolder = dirname( $caseFolderPath );
							@rmdir( $casesFolder );
						}
					}
				} catch( Exception $e ) {
					//non-empty folder
					continue;
				}
			}
		}
		
		echo "-- Removing CaseManagers: ";
		$success = $this->remove( "DELETE CaseManagers FROM CaseManagers INNER JOIN Cases ON (Cases.ID = CaseManagers.userID) WHERE Cases.clientID=:clientID", ['clientID'=>$clientID] );
		if( !$success ) return FALSE;
		echo "-- Removing Cases: ";
		return $this->remove( 'DELETE FROM Cases WHERE clientID=:clientID', ['clientID'=>$clientID] );
	}

	private function removeDepositions( $clientID )
	{
		$depositionIDs = $this->db->rows( 'SELECT d.ID,d.caseID FROM Depositions d INNER JOIN Cases c ON (c.ID=d.caseID) WHERE c.clientID=:clientID', ['clientID'=>$clientID] );
		if( !$depositionIDs ) return TRUE;
		foreach( $depositionIDs as $depo ) {
			echo "-- Removing DepositionAssistants: ";
			$success = $this->remove( "DELETE FROM DepositionAssistants WHERE depositionID=:depoID", ['depoID'=>$depo['ID']] );
			if( !$success ) continue;
			echo "-- Removing FileShares: ";
			$success = $this->remove( 'DELETE FROM FileShares WHERE depositionID=:depoID', ['depoID'=>$depo['ID']] );
			if( !$success ) continue;
			$success = $this->removeFolders( $depo['ID'] );
			echo "\n";
			if( !$success ) continue;
			try {
				$depoFolderPath = "{$_SERVER['DOCUMENT_ROOT']}{$this->config->dirs['media']}/clients/{$clientID}/cases/{$depo['caseID']}/depositions/{$depo['ID']}";
				if( is_dir( $depoFolderPath ) ) {
					$ok = @rmdir( $depoFolderPath );
					if( $ok ) {
						$deposFolder = dirname( $depoFolderPath );
						@rmdir( $deposFolder );
					}
				}
			} catch( Exception $e ) {
				//non-empty folder
				continue;
			}
		}

		//guest & witness APISessions for the deposition
		echo "-- Removing APISessions (guest): ";
		$success = $this->remove( "DELETE a FROM APISessions a
			INNER JOIN DepositionAttendees da ON (da.ID = a.attendeeID)
			INNER JOIN Depositions d ON (d.ID = da.depositionID)
			INNER JOIN Cases c ON (c.ID = d.caseID)
			WHERE c.clientID=:clientID", ['clientID'=>$clientID] );
		if( $success ) {
			echo "-- Removing DepositionAttendees: ";
			$success = $this->remove( "DELETE da FROM DepositionAttendees da
				INNER JOIN Depositions d ON (d.ID = da.depositionID)
				INNER JOIN Cases c ON (c.ID = d.caseID)
				WHERE c.clientID=:clientID", ['clientID'=>$clientID] );
		}

		return $success;
	}

	private function removeFolders( $depositionID )
	{
		$folderIDs = $this->db->cols( 'SELECT ID FROM Folders WHERE depositionID=:depoID', ['depoID'=>$depositionID] );
		if( !$folderIDs ) return TRUE;
		echo "-- Removing Folders: ";
		foreach( $folderIDs as $folderID ) {
			$success = $this->removeFiles( $folderID );
			if( !$success ) {
				echo ' ';
				continue;
			}
			try {
				$folder = new DB\Folders( $folderID );
				if( !$folder ) {
					echo ' ';
					continue;
				}
				$folderPath = $folder->getFullPath();
				if( is_dir( $folderPath ) ) {
					$success = @rmdir( $folderPath );
					if( !$success ) {
						echo ' ';
						continue;
					}
					echo 'F ';
					$usersFolder = dirname( $folderPath );
					$ok = @rmdir( $usersFolder );	//might not be empty yet
					if( $ok ) {
						$userFolder = dirname( $usersFolder );
						@rmdir( $userFolder );
					}
				}
			} catch( Exception $e ) {
				//non-empty folder attempted to be removed
				continue;
			}
		}
		return $success;
	}

	private function removeFiles( $folderID )
	{
		$fileIDs = $this->db->cols( 'SELECT ID FROM Files WHERE folderID=:folderID', ['folderID'=>$folderID] );
		if( !$fileIDs ) return TRUE;
		foreach( $fileIDs as $fileID ) {
			$file = new DB\Files( $fileID );
			$filePath = $file->getFullName();
			if( file_exists( $filePath ) ) $success = unlink( $filePath );
			if( !$success ) continue;
			echo 'f';
		}
	}

	private function removeClientUsers( $clientID )
	{
		$success = TRUE;
		echo "-- Removing APISessions (member): ";
		$success = $this->remove( "DELETE APISessions FROM APISessions INNER JOIN Users ON (Users.ID = APISessions.userID) WHERE Users.clientID=:clientID", ['clientID'=>$clientID] );
		if( !$success ) return FALSE;
		echo "-- Removing PasswordResetRequests: ";
		$success = $this->remove( "DELETE PasswordResetRequests FROM PasswordResetRequests INNER JOIN Users ON (Users.ID = PasswordResetRequests.userID) WHERE Users.clientID=:clientID", ['clientID'=>$clientID] );
		if( !$success ) return FALSE;
		echo "-- Removing ClientNotifications: ";
		$success = $this->remove( "DELETE ClientNotifications FROM ClientNotifications INNER JOIN Users ON (Users.ID = ClientNotifications.userID) WHERE Users.clientID=:clientID", ['clientID'=>$clientID] );
		if( !$success ) return FALSE;
		echo "-- Removing Users: ";
		$success = $this->remove( "DELETE FROM Users WHERE clientID=:clientID", ['clientID'=>$clientID] );
		return $success;
	}

}


$PR = new PurgeResellers();