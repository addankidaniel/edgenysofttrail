<?php

/**
 * Cron script to be called every x minutes, checks ClientMessages for any unsent messages and attempts to send.
 */

$status = 0;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;

require_once(__DIR__ . '/../connect.php');

// maximum number of emails that will be sent per execution
define( 'MAX_MESSAGES', 25 );

try {
	$db = DB\ORM::getInstance()->getDB('db0');

	$messages = [];
	$users = [];

	// get a list of unread messages from the ClientNotifications table
	foreach ($db->rows( 'SELECT n.ID FROM ClientNotifications n WHERE n.sentOn IS NULL LIMIT '.MAX_MESSAGES ) as $row)
	{
		// init object for ClientNotification, ClientMessage and User
		$n = foo( new DB\ServiceClientNotifications() )->getByID( $row['ID'] );

		if (!array_key_exists( $n->clientMessageID, $messages ))
		{
			$messages[$n->clientMessageID] = foo( new DB\ServiceClientMessages() )->getByID( $n->clientMessageID );
		}

		$message = $messages[$n->clientMessageID];

		if (!array_key_exists( $n->userID, $users ))
		{
			$users[$n->userID] = foo( new DB\ServiceUsers() )->getByID( $n->userID );
		}

		$user = $users[$n->userID];
		$username = "{$user->firstName} {$user->lastName}";

		$mailMsg = str_replace( '%CLIENT-NAME%', $username, $message->messageBody );

		ClickBlocks\Utils\EmailGenerator::sendAdminNotification( [
			'sendToName' => $username,
			'sendToAddress' => $user->email,
			'subject' => $message->subject,
			'content' => $mailMsg,
		] );

		// once mail has sent, update ClientNotification record to reflect send date
		$n->sentOn = date( 'Y-m-d H:i:s' );
		$n->update();
	}
} catch (PDOException $e) {
    // TODO - log the exception message and return the code
	# echo 'Connection failed: ' . $e->getMessage();
	$status = ($e->getCode() ?: -1);
}

?>
