<?php

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB;

require_once( __DIR__ . '/../connect.php' );

/**
 * ED-2622; fixes permissions of demo depositions, folders and files created by the reseller when creating the client
*/

date_default_timezone_set('UTC');

function wlog( $txt )
{
	echo print_r( $txt, TRUE ) . PHP_EOL;
}

$db = DB\ORM::getInstance()->getDB( 'db0' );

$rows = $db->rows( "SELECT d.ID as depositionID, r.ID as resellerUser, u.ID as ownerID FROM Depositions d
	INNER JOIN Users r ON (r.ID=d.createdBy AND r.typeID='R')
	INNER JOIN Cases c ON (c.ID=d.caseID)
	LEFT JOIN Users u ON (u.clientID=c.clientID AND u.typeID='C')
	WHERE r.typeID='R'" );

$depositionCount = 0;
$folderCount = 0;
$fileCount = 0;

foreach( $rows as $row ) {
	$depositionID = (int)$row['depositionID'];
	$resellerUser = (int)$row['resellerUser'];
	$ownerID = (int)$row['ownerID'];
//	wlog( ['depositionID'=>$depositionID, 'resellerUser'=>$resellerUser, 'ownerID'=>$ownerID] );
	$folders = $db->cols( 'SELECT ID FROM Folders WHERE depositionID=:depositionID AND createdBy=:userID', ['depositionID'=>$depositionID, 'userID'=>$resellerUser] );
	foreach( $folders as $folderID ) {
		$folderID = (int)$folderID;
		$files = $db->cols( 'SELECT ID FROM Files WHERE folderID=:folderID AND createdBy=:userID', ['folderID'=>$folderID, 'userID'=>$resellerUser] );
		foreach( $files as $fileID ) {
			$fileID = (int)$fileID;
			$file = new \ClickBlocks\DB\Files( $fileID );
			if( !$file || !$file->ID || $file->ID != $fileID || $file->folderID != $folderID ) {
				wlog( "Invalid fileID: {$fileID}" );
				continue;
			}
			wlog( "Fixing permissions for File: {$file->ID}: {$file->name}" );
			$file->createdBy = $ownerID;
			$file->sourceUserID = $ownerID;
			$file->update();
			++$fileCount;
		}
		$folder = new \ClickBlocks\DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID || $folder->depositionID != $depositionID ) {
			wlog( "Invalid folderID: {$folderID}" );
			continue;
		}
		wlog( "Fixing permissions for Folder: {$folder->ID}: {$folder->name}" );
		$folder->createdBy = $ownerID;
		$folder->update();
		++$folderCount;
	}
	$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
	if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
		wlog( "Invalid depositionID: {$depositionID}" );
		continue;
	}
	wlog( "Fixing permissions for Deposition: {$deposition->ID}: {$deposition->depositionOf}" );
	$deposition->createdBy = $ownerID;
	$deposition->update();
	++$depositionCount;
}

wlog( "Fixed; Depositions: {$depositionCount}, Folders: {$folderCount}, Files: {$fileCount}" );
