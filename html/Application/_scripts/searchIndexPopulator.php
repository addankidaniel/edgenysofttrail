<?php

namespace ClickBlocks\MiscScripts;

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB,
	Exception,
	ClickBlocks\Elasticsearch;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../connect.php' );

ob_end_flush();

class SearchIndexPopulator {

	public function __construct()
	{
		echo "Constructing $i\n";
		$this->db = DB\ORM::getInstance()->getDB( 'db0' );
		$this->db->catchException = TRUE;
		$this->reg = Core\Register::getInstance();
		$this->init();
	}

	private function init()
	{
		ini_set('memory_limit', '1024M');
		$start = time();
		$count = 0;
		echo 'Initializing at ' . date('Y-m-d H:i:s') . "\n";
		$sql = 'SELECT DISTINCT fi.ID FROM Files fi
			INNER JOIN Folders fl ON (fl.ID=fi.folderID)
			LEFT JOIN Depositions d ON (d.ID=fl.depositionID)
			LEFT JOIN Cases c ON (c.ID=fl.caseID OR c.ID=d.caseID)
			INNER JOIN Clients cl ON (cl.ID=c.clientID)
			LEFT JOIN Clients r ON (r.ID=cl.resellerID)
			WHERE r.deleted IS NULL AND cl.deleted IS NULL AND c.deleted IS NULL AND d.deleted IS NULL
			AND fi.ID > 0
			ORDER BY fi.ID';
		$fileIDs = $this->db->cols( $sql );
		$totalCount = count( $fileIDs );
		echo 'Attempting index of ' . $totalCount . " files...\n";

		//for( $i = $this->startID; $i <= $this->endID; $i++ ){
		foreach ( $fileIDs as $i ){
			$count++;
			echo "(" . number_format($count) . " of " . number_format($totalCount) . ") Indexing file ID " . $i;
			try {
				Elasticsearch::indexDocument( $i );
			}
			catch(Exception $e) {
				echo ' Message: ' .$e->getMessage();
			}
			echo "\n";
		}
		$end = time();
		echo "\n" . 'Finished at ' . date('Y-m-d H:i:s') . "\n";
		echo 'Indexing took ' . $this->SecondsToDHMS( $end - $start ) . "\n";
	}

	private function SecondsToDHMS($seconds) {
		$days = floor($seconds/86400);
		$hrs = floor($seconds/3600);
		$mins = intval(($seconds / 60) % 60);
		$sec = intval($seconds % 60);

		if($days>0){
			$hrs = str_pad($hrs,2,'0',STR_PAD_LEFT);
			$hours=$hrs-($days*24);
			$return_days = $days." Days ";
			$hrs = str_pad($hours,2,'0',STR_PAD_LEFT);
		}else{
			$return_days="";
			$hrs = str_pad($hrs,2,'0',STR_PAD_LEFT);
		}

		$mins = str_pad($mins,2,'0',STR_PAD_LEFT);
		$sec = str_pad($sec,2,'0',STR_PAD_LEFT);

		return $return_days.$hrs.":".$mins.":".$sec;
	}

}

$sip = new SearchIndexPopulator();
