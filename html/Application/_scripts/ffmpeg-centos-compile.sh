#!/bin/bash

yum install autoconf automake gcc gcc-c++ git libtool make pkgconfig zlib-devel
yum remove yasm nasm
mkdir /usr/local/src/ffmpeg_sources
cd /usr/local/src/ffmpeg_sources

#yasm
curl -O http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
tar xzvf yasm-1.3.0.tar.gz
cd yasm-1.3.0
./configure
make && make install
make distclean

ldconfig

#nasm
cd /usr/local/src/ffmpeg_sources
curl -O http://www.nasm.us/pub/nasm/releasebuilds/2.11/nasm-2.11.tar.bz2
tar xjvf nasm-2.11.tar.bz2
cd nasm-2.11
./configure
make && make install
make distclean

ldconfig

#libx264
cd /usr/local/src/ffmpeg_sources
git clone --depth 1 git://git.videolan.org/x264
cd x264
./configure --enable-static
make && make install
make distclean

#libfdk_aac
cd /usr/local/src/ffmpeg_sources
git clone --depth 1 git://git.code.sf.net/p/opencore-amr/fdk-aac
cd fdk-aac
autoreconf -fiv
./configure --disable-shared
make && make install
make distclean

#libmp3lame
cd /usr/local/src/ffmpeg_sources
curl -L -O http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz
tar xzvf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure --disable-shared --enable-nasm
make && make install
make distclean

#libogg
cd /usr/local/src/ffmpeg_sources
curl -O http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.gz
tar xzvf libogg-1.3.2.tar.gz
cd libogg-1.3.2
./configure --disable-shared
make && make install
make distclean

#libvorbis
cd /usr/local/src/ffmpeg_sources
curl -O http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.5.tar.gz
tar xzvf libvorbis-1.3.5.tar.gz
cd libvorbis-1.3.5
./configure --with-ogg --disable-shared
make && make install
make distclean

#libvpx
cd /usr/local/src/ffmpeg_sources
git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
cd libvpx
./configure --disable-examples
make && make install
make clean

ldconfig

#ffmpeg
cd /usr/local/src/ffmpeg_sources
git clone --depth 1 git://source.ffmpeg.org/ffmpeg
cd ffmpeg
./configure --extra-libs=-ldl --enable-gpl --enable-nonfree --enable-libfdk_aac --enable-libmp3lame --enable-libvorbis --enable-libvpx --enable-libx264 --enable-swscale
make && make install
make distclean
