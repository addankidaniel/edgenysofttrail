<?php

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB,
	ClickBlocks\DB\ServicePlatformLog,
	ClickBlocks\API\IEdepoBase;

require_once( __DIR__ . '/../connect.php' );

/**
 * Back fill PlatformLog from InvoiceCharges
 */

date_default_timezone_set('UTC');

function wlog( $txt )
{
	echo print_r( $txt, TRUE ) . PHP_EOL;
}

$db = DB\ORM::getInstance()->getDB( 'db0' );

$optionIDs = [
	IEdepoBase::PRICING_OPTION_BASE_LICENSING,
	IEdepoBase::PRICING_OPTION_SETUP,
	IEdepoBase::PRICING_OPTION_SUBSCRIPTION,
	IEdepoBase::PRICING_OPTION_CASE_SETUP,
	IEdepoBase::PRICING_OPTION_CASE_MONTHLY,
	IEdepoBase::PRICING_OPTION_DEPOSITION_SETUP,
	IEdepoBase::PRICING_OPTION_DEPOSITION_MONTHLY,
	IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1,
	IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2,
	IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3,
	IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1,
	IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2,
	IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3,
	IEdepoBase::PRICING_OPTION_UPLOAD_PER_DOC,
	IEdepoBase::PRICING_OPTION_WITNESSPREP_ATTENDEE
];

$resellerIDs = $db->cols( "SELECT r.ID FROM Clients r WHERE r.typeID = 'R' ORDER BY r.created,r.ID" );

/**
 * @var \ClickBlocks\DB\OrchestraPricingModelOptions
 */
$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();


foreach( $resellerIDs as $resellerID ) {
	$reseller = new DB\Clients( $resellerID );
	if( !$reseller || !$reseller->ID || $reseller->ID != $resellerID ) {
		wlog( "Invalid reseller: {$resellerID}" );
		continue;
	}
	$pricingModelOptions = $orchPMO->getByClient( $reseller->ID );
	$pricingModel = [];
	foreach( $pricingModelOptions as $pmo ) {
		$pricingModel[$pmo['optionID']] = $pmo;
	}
	foreach( $optionIDs as $optionID ) {
		if( !isset( $pricingModel[$optionID] ) ) {
			wlog( "Adding missing pricing option for ({$reseller->ID}) {$reseller->name} -- optionID: {$optionID}" );
			$isMonthly = ($optionID == IEdepoBase::PRICING_OPTION_CASE_MONTHLY || $optionID == IEdepoBase::PRICING_OPTION_DEPOSITION_MONTHLY);
			$priceModelOption = new DB\PricingModelOptions();
			$priceModelOption->clientID = $reseller->ID;
			$priceModelOption->optionID = $optionID;
			$priceModelOption->price = 0.00;
			$priceModelOption->typeID = ($isMonthly) ? IEdepoBase::PRICING_TYPE_MONTHLY : IEdepoBase::PRICING_TYPE_ONETIME;
			$priceModelOption->insert();
		}
	}
}
