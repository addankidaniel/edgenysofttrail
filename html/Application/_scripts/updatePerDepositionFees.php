<?php

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;

require_once(__DIR__ . '/../connect.php');
$db = DB\ORM::getInstance()->getDB('db0');


$resellers = $db->rows('SELECT DISTINCT pmo.clientID as resellerID FROM PricingModelOptions pmo LEFT JOIN Clients c ON c.ID = pmo.clientID WHERE pmo.clientID NOT IN (SELECT DISTINCT clientID FROM PricingModelOptions WHERE optionID IN (10,11,12,13,14,15)) AND c.resellerID IS NULL ORDER BY c.resellerID,pmo.clientID');
$clients = $db->rows('SELECT DISTINCT pmo.clientID FROM PricingModelOptions pmo LEFT JOIN Clients c ON c.ID = pmo.clientID WHERE pmo.clientID NOT IN (SELECT DISTINCT clientID FROM PricingModelOptions WHERE optionID IN (10,11,12,13,14,15)) AND c.resellerID IS NOT NULL ORDER BY c.resellerID,pmo.clientID');

foreach( $resellers as $reseller ) {
	$resellerID = (int)$reseller['resellerID'];
	$db->insert( 'PricingModelOptions', array( 'clientID' => $resellerID, 'optionID' => 10, 'price' => 1.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $resellerID, 'optionID' => 11, 'price' => 1.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $resellerID, 'optionID' => 12, 'price' => 1.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $resellerID, 'optionID' => 13, 'price' => 1.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $resellerID, 'optionID' => 14, 'price' => 1.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $resellerID, 'optionID' => 15, 'price' => 1.00, 'typeID' => 'O' ) );
	echo "Added Pricing Options for ResellerID: {$resellerID}\n";
}

foreach( $clients as $client ) {
	$clientID = (int)$client['clientID'];
	$db->insert( 'PricingModelOptions', array( 'clientID' => $clientID, 'optionID' => 10, 'price' => 2.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $clientID, 'optionID' => 11, 'price' => 2.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $clientID, 'optionID' => 12, 'price' => 2.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $clientID, 'optionID' => 13, 'price' => 2.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $clientID, 'optionID' => 14, 'price' => 2.00, 'typeID' => 'O' ) );
	$db->insert( 'PricingModelOptions', array( 'clientID' => $clientID, 'optionID' => 15, 'price' => 2.00, 'typeID' => 'O' ) );
	echo "Added Pricing Options for ClientID: {$clientID}\n";
}
