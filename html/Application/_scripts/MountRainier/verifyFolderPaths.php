#!/usr/bin/php
<?php

use ClickBlocks\DB;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../../connect.php' );

date_default_timezone_set( 'UTC' );
session_destroy();
ob_end_clean();

$clientIDs = [];
$sessionIDs = [];

if( count( $argv ) > 1 ) {
	foreach( $argv as $arg ) {
		if( strpos( $arg, '-c=' ) !== FALSE ) {
			list( $c, $cv ) = explode( '-c=', $arg );
			$clientIDs[] = (int)$cv;
		}
		if( strpos( $arg, '-s=' ) !== FALSE ) {
			list( $s, $sv ) = explode( '-s=', $arg );
			$sessionIDs[] = (int)$sv;
		}
	}
}

if( !$clientIDs && !$sessionIDs ) {
	findClients(); //main
} else {
	if( !$sessionIDs ) {
		foreach( $clientIDs as $clientID ) {
			verifyFolderPathsForClient( $clientID ); //fork
		}
	} else {
		foreach( $sessionIDs as $sessionID ) {
			$retval = validateFoldersForSession( $sessionID ); //fork
		}
	}
}

function findClients() {
	$cli = $_SERVER['PHP_SELF'];
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$clientIDs = $db->cols( 'SELECT c.ID FROM Clients c WHERE (c.typeID="C" OR c.typeID="EC" OR c.typeID="CRT") AND c.deleted IS NULL ORDER BY c.resellerID,c.created,c.ID' );
	if( !$clientIDs || !is_array( $clientIDs ) ) {
		exit( 'No clients found!' );
	}
	foreach( $clientIDs as $clientID ) {
		echo "Verifying folders for ClientID: {$clientID}\n";
		$cmd = "php {$cli} -c={$clientID}";
		echo shell_exec( $cmd );
	}
}

function verifyFolderPathsForClient( $clientID ) {
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$caseIDs = $db->cols( 'SELECT ID FROM Cases WHERE clientID=:clientID AND deleted IS NULL', ['clientID'=>(int)$clientID] );
	if( $caseIDs && is_array( $caseIDs ) ) {
		foreach( $caseIDs as $caseID ) {
			validateFoldersForCase( $caseID );
		}
	}
//	$memUsage = memory_get_usage();
//	echo 'Memory Usage: ', \ClickBlocks\Utils::readableFilesize( $memUsage ), "\n";
}

function validateFoldersForCase( $caseID ) {
	$orcCases = new DB\OrchestraCases();
	$folders = $orcCases->getCaseFolders( $caseID, 0 );
	if( $folders && is_array( $folders ) ) {
		foreach( $folders as $folder ) {
			validateFolderPath( $folder['ID'] );
		}
	}
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$sessionIDs = $db->cols( 'SELECT ID FROM Depositions WHERE caseID=:caseID AND deleted IS NULL', ['caseID'=>(int)$caseID] );
	if( $sessionIDs && is_array( $sessionIDs ) ) {
		$cli = $_SERVER['PHP_SELF'];
		foreach( $sessionIDs as $sessionID ) {
			echo "Verifying folders for SessionID: {$sessionID}\n";
			$cmd = "php {$cli} -s={$sessionID}";
			echo shell_exec( $cmd );
		}
	}
}

function validateFoldersForSession( $sessionID ) {
	$hasErrors = FALSE;
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$folderIDs = $db->cols( 'SELECT ID FROM Folders WHERE depositionID=:sessionID', ['sessionID'=>(int)$sessionID] );
	if( $folderIDs && is_array( $folderIDs ) ) {
		foreach( $folderIDs as $folderID ) {
			$retval = validateFolderPath( $folderID );
			if( !$retval ) {
				$hasErrors = TRUE;
			}
//			$memUsage = memory_get_usage();
//			echo 'Memory Usage: ', \ClickBlocks\Utils::readableFilesize( $memUsage ), "\n";
		}
	}
	if( $hasErrors ) {
		$session = new DB\Depositions( $sessionID );
		if( $session && $session->ID && $session->ID == $sessionID && $session->isDemo() && !$session->parentID ) {
			echo "Resetting Demo Session; {$sessionID}: '{$session->depositionOf}'\n";
			$session->finish();
		} else {
			echo "Session has errors; {$sessionID}: '{$session->depositionOf}', class: {$session->class}\n";
		}
	}
}

function validateFolderPath( $folderID ) {
	$hasErrors = FALSE;
	$folder = new DB\Folders( $folderID );
	if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
		echo "Folder not found by ID: {$folderID}\n";
		return FALSE;
	}
	$folderPath = $folder->getFullPath();
	$files = $folder->files;
	if( !is_dir( $folderPath ) ) {
		echo "folder does not exist: {$folderPath}\t{$folderID}: '{$folder->name}'\n";
		$umask = umask( 0 );
		mkdir( $folderPath, 02775, TRUE );	//sticky setguid
		umask( $umask );
		return FALSE;
	}
	foreach( $files as $file ) {
		$filePath = $file->getFullName();
		if( !file_exists( $filePath ) ) {
			echo "File does not exist: {$filePath}\t{$file->ID}: '{$file->name}'\n";
			$hasErrors = TRUE;
		}
	}
	return !$hasErrors;
}
