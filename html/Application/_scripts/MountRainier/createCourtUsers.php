#!/usr/bin/php
<?php

use ClickBlocks\DB,
	ClickBlocks\API\IEdepoBase;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../../connect.php' );

//error_reporting( E_ALL );
date_default_timezone_set( 'UTC' );
session_destroy();
ob_end_clean();

if( count( $argv ) != 3 || !is_numeric( $argv[2] ) ) {
	exit( "Usage: {$argv[0]} <list.csv> <client ID>\n" );
}

if( !is_readable( $argv[1] ) ) {
	exit( "Error: unable to read file: {$argv[1]}\n" );
}

$reporters = [];

if( ($fh = fopen( $argv[1], 'r' )) !== FALSE ) {
	$row = 1;
    while( ($data = fgetcsv( $fh, 1024, ',', '"' ) ) !== FALSE ) {
		if( !$data || !is_array( $data ) || count( $data ) !== 6 ) {
			echo "Warning: unexpected data:\n";
			print_r( $data );
			continue;
		}
		$reporters[] = $data;
    }
    fclose( $fh );
}

if( !$reporters ) {
	exit( "Error: nothing to do, nothing found in: {$argv[1]}\n" );
}

$courtClient = new DB\Clients( intval( $argv[2] ) );
if( !$courtClient || !$courtClient->ID || $courtClient->ID != intval( $argv[2] ) ) {
	exit( "Error: invalid client ID: {$argv[2]}\n" );
}

if( $courtClient->typeID !== IEdepoBase::CLIENT_TYPE_COURTCLIENT ) {
	exit( "Error: invalid client ID, not a court client: {$argv[2]}\n" );
}

$columnHeaders = array_shift( $reporters );
$now = date( 'Y-m-d H:i:s' );

foreach( $reporters as $rData ) {
	$user = new DB\Users();
	$user->clientID = $courtClient->ID;
	$user->typeID = IEdepoBase::USER_TYPE_REPORTER;
	$user->countryCode = 'US';
	$user->created = $now;
	$user->termsOfService = 0;
	$user->clientAdmin = 0;
	$user->firstName = trim( $rData[0] );
	$user->lastName = trim( $rData[1] );
	$user->email = mb_strtolower( trim( $rData[2] ), 'UTF-8' );
	$user->username = mb_strtolower( trim( $rData[3] ), 'UTF-8' );
	$user->insert();

	$uAuth = new DB\UserAuth();
	$uAuth->userID = $user->ID;
	$uAuth->spice = DB\UserAuth::generateSpice();
	$passwd = hash( 'SHA1', trim( $rData[4] ) );
	$uAuth->word = hash( 'SHA512', "{$passwd}{$uAuth->spice}" );
	$uAuth->lockCount = 0;
	$uAuth->updated = $now;
	$uAuth->insert();
}
