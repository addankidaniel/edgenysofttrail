<?php

use ClickBlocks\DB;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../../connect.php' );

/**
 * Move folders that should be "createBy" Client Admin
 */

date_default_timezone_set( 'UTC' );

function wlog( $txt ) {
	echo print_r( $txt, TRUE ) . PHP_EOL;
}

$db = DB\ORM::getInstance()->getDB( 'db0' );

session_destroy();
ob_end_clean();

$clients = $db->rows( "SELECT c.ID as clientID, u.ID as adminID FROM Clients c
	INNER JOIN Users u ON (u.clientID=c.ID AND u.typeID='C')
	WHERE (c.typeID='C' OR c.typeID='EC' OR c.typeID='CRT')
	AND c.deleted IS NULL
	ORDER BY c.resellerID,c.created,c.ID"
);

$umask = umask( 0 );
foreach( $clients as $c ) {
	$memUsage = memory_get_usage();
	if( $memUsage > (1024 * 1024 * 64) ) {	// allow up to 64M of memory
		die( "Terminating,... memory usage is too high!\n" );
	}
	$sql = "SELECT f.ID FROM Cases ca
		INNER JOIN Users u ON (u.clientID=ca.clientID AND u.typeID='C')
		INNER JOIN Depositions d ON (d.caseID=ca.ID AND d.deleted IS NULL)
		INNER JOIN Folders f ON (f.depositionID=d.ID AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='CourtesyCopy' OR f.class='Trusted') AND f.createdBy != u.ID)
		WHERE ca.clientID=:clientID AND ca.deleted IS NULL";
	$folderIDs = $db->cols( $sql, ['clientID'=>$c['clientID']] );
	foreach( $folderIDs as $folderID ) {
		$folder = new DB\Folders( $folderID );
		wlog( "Client ID: {$c['clientID']}, Folder: {$folder->ID} -- '{$folder->name}', createdBy: {$folder->createdBy}, Admin ID: {$c['adminID']}" );
		$folderPath = realpath( $folder->getFullPath() );
		wlog( "old: {$folderPath}" );
		if( is_dir( $folderPath ) ) {
			$folder->createdBy = (int)$c['adminID'];
			$newPath = $folder->getFullPath();
			wlog( "new: {$newPath}" );
			$baseDir = dirname( $newPath );
			if( !is_dir( $baseDir ) ) {
				wlog( "creating folder: {$baseDir}" );
				mkdir( $baseDir, 02775, TRUE );
			}
			if( is_dir( $folderPath ) && is_dir( $baseDir ) ) {
				$didMove = rename( $folderPath, $newPath );
				if( $didMove ) {
					$folder->update();
				} else {
					die( "Error: Unable to move folder!\n" );
				}
			}
		} else {
			wlog( 'Unexpected folder path: ' . $folder->getFullPath() );
			print_r( [$folder->getValues(), $c['adminID'], $folderPath] );
			exit;
		}
	}
	unset( $folderIDs, $folderID, $folder );
	wlog( "-- Memory Usage: " . number_format( $memUsage ) );
}
