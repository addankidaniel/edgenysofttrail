#!/usr/bin/php
<?php

use ClickBlocks\DB,
	ClickBlocks\MVC\Edepo;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../../connect.php' );

date_default_timezone_set( 'UTC' );
session_destroy();
ob_end_clean();
gc_enable();

$db = DB\ORM::getInstance()->getDB( 'db0' );
$clientIDs = $db->cols( 'SELECT c.ID FROM Clients c WHERE (c.typeID="C" OR c.typeID="EC" OR c.typeID="CRT") AND c.deleted IS NULL ORDER BY c.resellerID,c.created,c.ID' );

if( $clientIDs && is_array( $clientIDs ) ) {
	foreach( $clientIDs as $clientID ) {
		echo "Processing Client ID: {$clientID} ";
		$caseIDs = $db->cols( 'SELECT c.ID FROM Cases c WHERE c.clientID=:clientID AND c.class != :demo', ['clientID'=>(int)$clientID,'demo'=>Edepo::CASE_CLASS_DEMO] );
		if( $caseIDs && is_array( $caseIDs ) ) {
			foreach( $caseIDs as $caseID ) {
				echo '.';
				deleteDemoSessions( $caseID );
			}
		}
		echo " done.\n";
		gc_collect_cycles();
//		$memUsage = memory_get_usage();
//		echo 'Memory Usage: ', \ClickBlocks\Utils::readableFilesize( $memUsage ), "\n";
	}
}

function deleteDemoSessions( $caseID ) {
	$db = DB\ORM::getInstance()->getDB( 'db0' );
	$bind = [
		'caseID' => (int)$caseID,
		'demo'=>Edepo::DEPOSITION_CLASS_DEMO,
		'wpdemo'=>Edepo::DEPOSITION_CLASS_WPDEMO,
		'demotrial'=>Edepo::DEPOSITION_CLASS_DEMOTRIAL,
	];
	$demoSessionIDs = $db->cols( 'SELECT d.ID FROM Depositions d WHERE d.caseID=:caseID AND d.deleted IS NULL AND d.class IN (:demo,:wpdemo,:demotrial)', $bind );
	if( $demoSessionIDs && is_array( $demoSessionIDs ) ) {
		foreach( $demoSessionIDs as $sessionID ) {
			$session = new DB\Depositions( $sessionID );
			if( $session && $session->ID && $session->ID == $sessionID ) {
				$session->delete();
				echo 'x';
			}
		}
	}
}
