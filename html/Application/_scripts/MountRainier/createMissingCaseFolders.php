<?php

use ClickBlocks\DB;

define( 'NO_GZHANDLER', TRUE );

require_once( __DIR__ . '/../../connect.php' );

/**
 * Create Case Folders for existing Cases
 */

date_default_timezone_set('UTC');

function wlog( $txt )
{
	echo print_r( $txt, TRUE ) . PHP_EOL;
}

$db = DB\ORM::getInstance()->getDB( 'db0' );

session_destroy();
ob_end_clean();

$clientIDs = $db->cols( "SELECT c.ID FROM Clients c
	WHERE (c.typeID='C' OR c.typeID='EC')
	AND c.deleted IS NULL
	ORDER BY c.resellerID,c.created,c.ID"
);

$umask = umask( 0 );
foreach( $clientIDs as $clientID ) {
//	wlog( "{$clientID} -- Memory Usage: " . number_format( memory_get_usage() ) );
	$folderIDs = $db->cols( "SELECT f.ID FROM Folders f
		INNER JOIN Cases c ON (c.ID=f.caseID AND c.clientID=:clientID)
		WHERE f.caseID IS NOT NULL", ['clientID'=>(int)$clientID] );
	foreach( $folderIDs as $folderID ) {
		$Folder = new \ClickBlocks\DB\Folders( $folderID );
		$fullPath = $Folder->getFullPath();
		if( !file_exists( $fullPath ) ) {
			wlog( "creating Case Folder: {$fullPath}" );
			mkdir( $fullPath, 02775, TRUE );
		}
		unset( $Folder, $fullPath );
	}
}
