<?php

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;

require_once(__DIR__ . '/../connect.php');

function wlog($txt) {
   echo print_r($txt,1).PHP_EOL;
}

$code = $argv[1] ?: $_GET['code'];

$cfg = Core\Register::getInstance()->config;

if ($code != $cfg->secretSalt) die('Incorrect action');

$db = DB\ORM::getInstance()->getDB('db0');

$rows = $db->rows("SELECT * FROM Folders WHERE class='Exhibit' AND name='Exhibit'");
$i=0;
foreach ($rows as $row) {
   if (stripos($row['name'], $cfg->logic['exhibitFolderName']) === 0)      continue;
   $bll = new DB\Folders();
   $bll->assign($row);
   $bll->name = $cfg->logic['exhibitFolderName'];
   $bll->update(); $i++;
}

echo 'Renamed '.$i.' Exhibit folders';

$rows = $db->rows("SELECT * FROM Folders WHERE class='Personal' AND name='Personal'");
$i=0;
foreach ($rows as $row) {
   if (stripos($row['name'], $cfg->logic['personalFolderName']) === 0)      continue;
   $bll = new DB\Folders();
   $bll->assign($row);
   $bll->name = $cfg->logic['personalFolderName'];
   $bll->update(); $i++;
}

echo 'Renamed '.$i.' Personal folders';



?>
