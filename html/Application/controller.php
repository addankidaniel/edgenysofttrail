<?php

namespace ClickBlocks\MVC;

require_once(__DIR__ . '/connect.php');

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\MVC\Backend,
    ClickBlocks\DB,
    ClickBlocks\Utils;

class Controller extends MVC
{
	private static $pages = [
		'frontend' => [
//			'/home' => 'PageHome',
		],
		'admin' => [
			'/' => 'PageClients',
			'/resellers' => 'PageClients',
			'/reseller/edit' => 'PageResellerEdit',
			'/reseller/add'  => 'PageResellerEdit',
			'/clients' => 'PageEnterpriseClients',
			'/client/edit' => 'PageEnterpriseClientEdit',
			'/client/add' => 'PageEnterpriseClientEdit',
			'/enterprise/edit' => 'PageEnterpriseEdit',
			'/courtclients' => 'PageCourtClients',
			'/courtclients/add' => 'PageCourtClientEdit',
			'/courtclients/edit' => 'PageCourtClientEdit',
			'/exhibitsdownload' => 'PageExhibitsDownload',
			'/reports' => 'PageReports',
			'/login' => 'PageLogin',
			'/password-change' => 'PageChangePassword',
			'/myaccount' => 'PageMyAccount',
			'/notifications' => 'PageNotifications',
			'/notifications/logs' => 'PageNotificationLogs',
			'/admins' => 'PageAdmins',
			'/admin/edit' => 'PageAdminEdit',
			'/admin/add'  => 'PageAdminEdit',
			'/admin/reports' => 'PageReports',
			'/report/invoices' => 'PageClientInvoices',
			'/report/invoice' => 'PageInvoiceView',
			'/report/detailreport' => 'PageDetailReport',
			'/report/commission' => 'PageCommissionReport',
			'/report/commission/detail' => 'PageCommissionDetail',
			'/reports/user' => 'PageUserReports',
			'/reports/user/detail' => 'PageUserDetail',
//			'/reports/usage' => 'PageUsageReports',
//			'/reports/detail' => 'PageUsageDetail',
			// '/signup' => 'PageSignup',
			'/signup/confirm' => 'PageSignupConfirm'
		],
		'reseller' => [
			'/login' => 'PageLogin',
			'/recovery' => 'PagePasswordRecovery',
			'/password-change' => 'PageChangePassword',
			'/' => 'PageClients',
			'/cases' => 'PageCases',
			'/case/edit' => 'PageCaseEdit',
			'/case/add' => 'PageCaseEdit',
			'/case/view' => 'PageCaseView',
			'/case/viewFile' => 'PageViewFile',
			'/case/fileViewer' => 'PageCaseFileViewer',
			'/case/deposition/add' => 'PageDepositionEdit',
			'/case/deposition/edit'=> 'PageDepositionEdit',
			'/case/deposition/import' => 'PageDepositionImport', // deprecated
			'/case/session/import' => 'PageDepositionImport',
			'/case/import' => 'PageDepositionImport',
			'/case/witnessprep/add' => 'PageWitnessPrepEdit',
			'/case/witnessprep/edit'=> 'PageWitnessPrepEdit',
			'/session/deposition' => 'PageDepositionEdit',
			'/session/demo' => 'PageDepositionDemoEdit',
			'/session/witnessprep' => 'PageWitnessPrepEdit',
			'/session/wpdemo' => 'PageWitnessPrepDemoEdit',
			'/session/trial' => 'PageTrialEdit',
			'/session/demotrial' => 'PageDemoTrialEdit',
			'/session/trialbinder' => 'PageTrialBinderEdit',
			'/session/arbitration' => 'PageArbitrationEdit',
			'/session/mediation' => 'PageMediationEdit',
			'/session/hearing' => 'PageHearingEdit',
			'/session/view' => 'PageSessionView',
			'/users' => 'PageUsers',
			'/users/add' => 'PageUserEdit',
			'/users/edit' => 'PageUserEdit',
			'/myaccount' => 'PageMyAccount',
			'/myaccount/login' => 'PageMyAccount',
			'/myaccount/datasource' => 'PageMyAccountDatasource',
			'/myaccount/pricing' => 'PageResellerPricing',
			'/reports' => 'PageReports',
			// RESELLER only:
			'/clients' => 'PageClients',
			'/client/add' => 'PageClientEdit',
			'/client/edit' => 'PageClientEdit',
			'/enterprise/add' => 'PageEnterpriseEdit',
			'/enterprise/edit' => 'PageEnterpriseEdit',
//			'/reports' => 'PageReports',
			'/reports' => 'PageUsageReports',
			'/reports/deposition' => 'PageDepositionReports',
			'/reports/deposition/detail' => 'PageDepositionDetail',
			'/reports/usage' => 'PageUsageReports',
			'/reports/detail' => 'PageUsageDetail',
			'/reports/user' => 'PageUserReports',
			'/reports/user/detail' => 'PageUserDetail',
			'/myaccount/branding' => 'PageMyAccountBranding',
			// COURT REPORTER:
			'/report' => 'PageTranscript',
			'/report/login' => 'PageLogin',
			'/report/transcript' => 'PageTranscript',
			// REPORTING:
			'/report/invoices' => 'PageClientInvoices',
			'/report/invoice' => 'PageInvoiceView',
			'/report/detailreport' => 'PageDetailReport',
			// TUTORIALS
			'/tutorial/video' => 'PageTutorialVideo'
		],
		// This overrides array above, for Client Admin backend
		'client' => [
			'/' => 'PageCases',
			'/reports' => 'PageCases',
			'/report/case' => 'PageReportsCaseView',
			'/clients' => 'PageCases',
			'/client/add' => 'PageCases',
			'/client/edit' => 'PageCases',
			'/reports' => 'PageCases',
			'/myaccount/branding' => 'PageMyAccount',
			'/myaccount/pricing' => 'PageMyAccount',
			'/reporter' => 'PageCases',
			'/reporter/login' => 'PageCases',
			'/reporter/transcript' => 'PageCases',
			'/report/invoices' => 'PageCases',
			'/report/invoice' => 'PageCases',
			'/report/detailreport' => 'PageCases',
			'/reports/usage' => 'PageCases',
			'/reports/detail' => 'PageCases'
		],
		'enterprise' => [
			'/' => 'PageCases',
			'/login' => 'PageLogin',
			'/recovery' => 'PagePasswordRecovery',
			'/password-change' => 'PageChangePassword',
			'/cases' => 'PageCases',
			'/case/edit' => 'PageCaseEdit',
			'/case/add' => 'PageCaseEdit',
			'/case/view' => 'PageCaseView',
			'/case/viewFile' => 'PageViewFile',
			'/case/fileViewer' => 'PageCaseFileViewer',
			'/case/deposition/add' => 'PageDepositionEdit',
			'/case/deposition/edit'=> 'PageDepositionEdit',
			'/case/deposition/import' => 'PageDepositionImport', // deprecated
			'/case/session/import' => 'PageDepositionImport',
			'/case/import' => 'PageDepositionImport',
			'/case/witnessprep/add' => 'PageWitnessPrepEdit',
			'/case/witnessprep/edit'=> 'PageWitnessPrepEdit',
			'/session/deposition' => 'PageDepositionEdit',
			'/session/demo' => 'PageDepositionDemoEdit',
			'/session/witnessprep' => 'PageWitnessPrepEdit',
			'/session/wpdemo' => 'PageWitnessPrepDemoEdit',
			'/session/trial' => 'PageTrialEdit',
			'/session/demotrial' => 'PageDemoTrialEdit',
			'/session/arbitration' => 'PageArbitrationEdit',
			'/session/mediation' => 'PageMediationEdit',
			'/session/hearing' => 'PageHearingEdit',
			'/session/view' => 'PageSessionView',
			'/users' => 'PageUsers',
			'/users/add' => 'PageUserEdit',
			'/users/edit' => 'PageUserEdit',
			'/myaccount' => 'PageMyAccount',
			'/myaccount/login' => 'PageMyAccount',
			'/myaccount/datasource' => 'PageMyAccountDatasource',
			'/myaccount/branding' => 'PageMyAccountBranding',
			'/myaccount/pricing' => 'PageMyAccount',
			'/reporter' => 'PageTranscript',
			'/reporter/login' => 'PageLogin',
			'/reporter/transcript' => 'PageTranscript'
		],
		'courts' => [
			'/login' => 'PageLogin',
			'/' => 'PageCourtCases',
			'/courts' => 'PageCourtCases',
			'/cases' => 'PageCourtCases',
			'/case/add' => 'PageCourtCaseEdit',
			'/case/edit' => 'PageCourtCaseEdit',
			'/case/view' => 'PageCourtCaseView',
			'/case/viewFile' => 'PageViewFile',
			'/case/fileViewer' => 'PageCaseFileViewer',
			'/exhibits' => 'PageExhibits',
			'/exhibits/login' => 'PageLogin',
			'/exhibits/edit' => 'PageExhibitEdit',
			'/session/view' => 'PageSessionView',
			'/session/trialbinder' => 'PageTrialBinderEdit',
			'/users' => 'PageCourtUsers',
			'/users/add' => 'PageCourtUserEdit',
			'/users/edit' => 'PageCourtUserEdit',
			'/myaccount' => 'PageMyAccount',
			'/reporter' => 'PageTranscript',
			'/reporter/login' => 'PageLogin',
			'/reporter/transcript' => 'PageTranscript'
		]
	];

	public function __construct()
	{
		parent::__construct();
		if (self::isFirstRequest() && $this->reg->config->checkDbVersion) {
			Utils\DbVersionChecker::validate(Core\IO::dir('db-scripts'));
		}
	}

	public function getPage() {
		$path = $this->uri->getPath();
		//\ClickBlocks\Debug::InfoLog( __FILE__, __LINE__, 'getPage() path:', $path );
		$admin = $this->reg->config->adminPath;
		$courtsPath = $this->reg->config->courtsPath;
		$signupPath = $this->reg->config->signupPath;
		$page = null;
		if( mb_substr( $path, 0, mb_strlen( $admin ) ) == $admin ) {
			$ns = 'ClickBlocks\MVC\Backend\\';
			$path = mb_substr( $path, mb_strlen( $admin ) ) ?: '/';
			$page = self::$pages['admin'][$path];
		} elseif( mb_substr( $path, 0, mb_strlen( $signupPath ) ) == $signupPath ) {
			$ns = 'ClickBlocks\MVC\Backend\\';
			$page = self::$pages['admin'][$path];
		} elseif( mb_substr( $path, 0, mb_strlen( $courtsPath ) ) == $courtsPath ) {
			$ns = 'ClickBlocks\MVC\Backend\\';
			$path = mb_substr( $path, mb_strlen( $courtsPath ) ) ? : $courtsPath;
			$page = self::$pages['courts'][$path];
			$pageSess = Backend\Backend::getSubSession();
		} else
		if (!isset(self::$pages['frontend'][$path]) && $this->uri->path[0])
		{
			$reseller = DB\OrchestraClients::getResellerByURL( $this->uri->path[0] );
			if( $reseller['ID'] ) {
				$this->reg->reseller = $reseller;
				if (self::isFirstRequest() && $reseller['deactivated'] != NULL)
				{
					$this->reg->staticMessage = 'This reseller is deactivated.';
					return new \ClickBlocks\MVC\Backend\PageMessage();
				}
				$path = mb_substr($path, mb_strlen($this->uri->path[0])+1) ?: '/';
				$ns = 'ClickBlocks\MVC\Backend\\';
				$pageSess = Backend\Backend::getSubSession();
				if (in_array($pageSess['userTypeID'], array(Backend\Backend::USER_TYPE_CLIENT, Backend\Backend::USER_TYPE_USER)))
				{
					$page = self::$pages['client'][$path];
				}
				if (!$page)
				{
					$page = self::$pages['reseller'][$path];
				}
			} else {
				$eclient = DB\OrchestraClients::getEnterpriseClientByURL( $this->uri->path[0] );
				$client = DB\OrchestraClients::getClientByURL( $this->uri->path[0] );
				if( $eclient['ID'] || $client['ID'] ) {
					$this->reg->reseller = ( $eclient['ID'] )? $eclient : $client;

					if( self::isFirstRequest() && $reseller['deactivated'] != null ) {
						$this->reg->staticMessage = ( $eclient['ID'] )? 'This enterprise client is deactivated.' : 'This client is deactivated';
						return new \ClickBlocks\MVC\Backend\PageMessage();
					}

					$path = mb_substr( $path, mb_strlen( $this->uri->path[0])+1) ?: '/';
					$ns = 'ClickBlocks\MVC\Backend\\';
					$pageSess = Backend\Backend::getSubSession();
					if( $eclient['ID'] ) {
						$page = self::$pages['enterprise'][$path];
					} else {
						$page = self::$pages['client'][$path];
					}
				}
			}
		} else {
			$ns = 'ClickBlocks\MVC\Frontend\\';
			$page = self::$pages['frontend'][$path];
		}
		if ($page)
		{
			$page = $ns . $page;
			return new $page();
		}
		die( 'Page/file not found.' );
	}
}

(new Controller())->execute();
