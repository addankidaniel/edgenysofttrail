<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web;

class PageTutorialVideo extends Backend
{
    public function __construct()
	{
        parent::__construct();
    }


    public function init()
	{
		$realpath = realpath($_SERVER['DOCUMENT_ROOT'].$this->fv['src']);

		while( ob_get_level() > 0 ) {
			ob_end_clean();
		}
		if( ini_get( 'zlib.output_compression' ) ) {
			ini_set( 'zlib.output_compression', 'Off' );
		}
		header( 'Content-Type: video/mp4' );
		if( isset( $_SERVER['HTTP_RANGE'] ) ) {
			\ClickBlocks\EDVideos::rangeDownload( $realpath );
		} else {
			\ClickBlocks\EDVideos::readfileChunked( $realpath );
		}
		return;
    }
}
