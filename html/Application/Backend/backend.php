<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\API,
	ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Debug,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Annotate,
	ClickBlocks\API\IEdepoBase;

class Backend extends \ClickBlocks\MVC\Edepo
{
   /**
    * @var \ClickBlocks\DB\Users
    */
   public $user = null;

  protected $backendType;

  public $basePath;

  /**
   * @var array
   */
  protected $reseller = null;

  const BACKEND_TYPE_ADMIN = 1;
  const BACKEND_TYPE_RESELLER = 2;
  //const BACKEND_TYPE_CLIENT = 3;
  const BACKEND_TYPE_COURT_REPORTER = 4;
  const BACKEND_TYPE_COURTCLIENT = 5;

  /**
   * @var array reference to $_SESSION['__EDEPO__'][$uri->path[0]];
   */
  public $subSession;

  /**
   * @var array reference to $_SESSION['__EDEPO__'][__CLASS__];
   */
  public $pageSession;

  /*const TAB_RESELLERS = 1;
  const TAB_REPORTS = 2;*/

	public function __construct($template = null)
	{
		if ($template != '')
		{
			$temp = Core\IO::dir('backend') . '/' . $template;
			if (is_file($temp)) $template = $temp;
		}
		parent::__construct($template);
		$this->basePath = mb_strtolower( '/'.$this->uri->path[0] );
		if( !isset( $this->session[$this->basePath] ) && isset( $this->session['URIBasePath'] ) ) {
			$this->basePath = $this->session['URIBasePath'];
		}
		$this->subSession =& $this->session[$this->basePath];
		if( (bool)$this->subSession['userID'] ) {
			$this->user = new DB\Users( $this->subSession['userID'] );
			$clientTypeID = $this->user->getClient()->typeID;
			if( $clientTypeID === self::CLIENT_TYPE_CLIENT && !$this->reg->reseller ) {
				$this->reg->reseller = $this->user->clients[0]->parentClients1[0]->getValues();
			} elseif( in_array( $clientTypeID, [self::USER_TYPE_RESELLER, self::CLIENT_TYPE_ENT_CLIENT, self::CLIENT_TYPE_COURTCLIENT] ) ) {
				$this->reg->reseller = $this->user->clients[0]->getValues();
			}
		}
		$this->reseller = &$this->reg->reseller;
		$pageClass = get_class( $this );
		if( !isset( $this->session[$pageClass] ) ) {
			$this->session[$pageClass] = [];
		}
		$this->pageSession =& $this->session[$pageClass];
	}

	public static function getSubSession() {
		$uri = Core\Register::getInstance()->uri;
		$basePath = mb_strtolower( "/{$uri->path[0]}" );
		if( !isset( $_SESSION[self::SESSION_NS][$basePath] ) ) {
			$_SESSION[self::SESSION_NS][$basePath] = [];
		}
		return $_SESSION[self::SESSION_NS][$basePath];
	}

	public function access()
	{
		$this->noAccessURL = $this->basePath.'/login?redir='.urlencode($this->uri->getQueryString());
		return $this->user && $this->user->ID && ($this->isAdminBackend() === $this->user->isAdmin());
	}

	public function init()
	{
		parent::init();
		$this->head->addMeta(new Helpers\Meta('charset', null, 'text/html; charset=' . $this->config->charset, 'Content-Type'));
		$this->head->addMeta(new Helpers\Meta('scripttype', null, 'text/javascript', 'Content-Script-Type'));
		if ($this->ie > 0) $this->js->addTool('json');
		$this->js->addTool('controls')->addTool('ajax')->addTool('jquery-2.0.3.min.js')->addTool( 'polyfills' );

		$css = new Helpers\Style('ie7', null, Core\IO::url('backend-css') . '/common-ie7.css');
	

		$this->css->add( new Helpers\Style( 'admin', null, '/Application/_includes/backend/asset/css/AdminLTE.min.css' ), 'link' );
		$this->css->add( new Helpers\Style( 'custom', null, '/Application/_includes/backend/asset/css/custom-admin.css' ), 'link' );

		
		$css->conditions = 'if IE 7';
		$this->css->add($css, 'link');
		$this->tpl->welcomeName = $this->user->firstName;

		$this->tpl->showTabs = true;
		$this->tpl->appVersion = $this->config->appVersion;
		$this->tpl->adminPath = $this->config->adminPath;
		$this->tpl->basePath = $this->basePath;
		$this->tpl->relPath = substr($this->uri->getPath(),strlen($this->basePath)) ?: '/';
		$this->tpl->isAdmin = $isAdmin = self::isAdminBackend();
		$this->tpl->isCourtClient = $isCourtClient = self::isCourtsBackend();
		$this->tpl->showTutorials = $this->isTutorialAvailable();
		if( !$this->user ) {
			$this->tpl->adminTitle = 'Production User';
		}
		if( $this->user->typeID == self::USER_TYPE_RESELLER ) {
			$this->tpl->adminTitle = 'Reseller Administrator';
		} elseif( $this->user->typeID == self::USER_TYPE_CLIENT && !$isCourtClient ) {
			$this->tpl->adminTitle = 'Legal Administrator'; // Client Admin
		} elseif( $this->user->typeID == self::USER_TYPE_CLIENT && $isCourtClient ) {
			$this->tpl->adminTitle = 'Court Administrator'; // Client Admin
		}

		if( !$isAdmin && !$isCourtClient ) {
			// prepare reseller branding
			if (!is_array($this->reg->reseller))
			{
				throw new \LogicException('Reseller-Type page was instantiated, but $reg->reseller is not set');
			}
			$this->tpl->reseller = $this->reseller;
			if ($this->reseller['logo'])
			{
				$this->tpl->resellerLogo = Core\IO::url('images').'/logos/'.$this->reseller['logo'];
			}
			$cl = explode(',', $this->reseller['bannerColor']);
			$this->tpl->resellerColor = $cl[0];
			$this->tpl->resellerTextColor = $cl[1];

			// prepare branding logo
			$this->tpl->brandingLogo = Core\IO::url( 'enterprise' ) . '/default.png';

			// for enterprise clients only, get sponsor reseller logo
			if ($this->reseller['typeID'] == self::CLIENT_TYPE_ENT_CLIENT && $this->reseller['resellerID'])
			{
				$enterpriseReseller = (new DB\ServiceClients())->getByID( $this->reseller['resellerID'] );
				if ($enterpriseReseller->logo)
				{
					$this->tpl->sponsorLogo = Core\IO::url('images').'/logos/'.$enterpriseReseller->logo;
				}
			}
		} else {
			// for administrators and court clients display eDepoze logo
			$this->tpl->brandingLogo = Core\IO::url( 'enterprise' ) . '/default.png';
		}

		$this->tpl->print = $this->isPrintMode() && $this->isPrintable();
		if( $this->tpl->print ) {
			$this->css->add(new Helpers\Style('printcss', null, Core\IO::url('backend-css') . '/print.css'), 'link');
			$this->js->add(new Helpers\Script('printjs', null, Core\IO::url('backend-js').'/print.js'), 'link');
			if( $_GET['PDF'] == 1 ) {
				$this->tpl->pdf = TRUE;
			}
			if( preg_match( '/Firefox/i', $_SERVER['HTTP_USER_AGENT'] ) ) {
				$this->css->add(new Helpers\Style('printcssff', null, Core\IO::url('backend-css').'/print-firefox.css'), 'link');
			}
		} else {
			$this->css->add(new Helpers\Style('styles', null, Core\IO::url('backend-css') . '/common.css'), 'link');
			$this->setTabs( $this->user->typeID, $this->user->clientID );
			if( $isAdmin ) {
				$this->setBackendType( self::BACKEND_TYPE_ADMIN );
			} elseif( $isCourtClient ) {
				$this->setBackendType( self::BACKEND_TYPE_COURTCLIENT );
			} elseif( $this->uri->path[1] == 'reporter' ) {
				$this->setBackendType( self::BACKEND_TYPE_COURT_REPORTER );
			} else {
				$this->setBackendType( self::BACKEND_TYPE_RESELLER );
			}
		}
	}

  public final function isPrintMode()
  {
     return ($_GET['PRINT'] == 1);
  }

  public function isPrintable()
  {
     return false;
  }

	public function setBackendType( $type )
	{
		$this->backendType = $type;
		$typeMap = [
			self::BACKEND_TYPE_ADMIN => 'superadmin',
			self::BACKEND_TYPE_RESELLER => 'reseller',
			self::BACKEND_TYPE_COURT_REPORTER => 'courtreporter',
			self::BACKEND_TYPE_COURTCLIENT => 'courtclient'
		];
		if( !isset( $typeMap[$type] ) ) {
			throw new \LogicException( 'Incorrect backend type: ' . $type );
		}
		$this->css->add( new Helpers\Style( 'styles-specific', NULL, Core\IO::url( 'backend-css' ) . "/{$typeMap[$type]}.css" ), 'link' );
	}

	public function setTabs( $type, $clientID )
	{
		$client = new \ClickBlocks\DB\Clients( $clientID );
		switch( $type ) {
			case self::USER_TYPE_RESELLER:
				$this->tpl->tabs = [
					1 => ['Client Management', '/clients'],
					2 => ['Reports', '/reports/user', [
							['label'=>'User Reports', 'url'=>'/reports/user'],
							['label'=>'Session Reports','url'=>'/reports/deposition']
						]
					],
					4 => ['My Account', '/myaccount', [
							['label'=>'Login Information','url'=>'/myaccount'],
							['label'=>'Branding','url'=>'/myaccount/branding'],
							['label'=>'Pricing Package','url'=>'/myaccount/pricing']
						]
					]
				];
				if( $client->resellerClass === self::RESELLERCLASS_RESELLER ) {
					unset( $this->tpl->tabs[2][2] );	//remove child-menu for deposition reports
				}
				break;
			case self::USER_TYPE_CLIENT:
				if( $client->typeID == self::CLIENT_TYPE_ENT_CLIENT ) {
					$this->tpl->tabs = [
						1 => array('Case Management','/cases'),
						2 => array('User Management','/users'),
						4 => array('My Account','/myaccount')
					];
				} else {
					$this->tpl->tabs = [
						1 => array('Case Management','/cases'),
						2 => array('User Management','/users'),
						4 => array('My Account','/myaccount')
					];
				}
				break;
			case self::USER_TYPE_USER:
			case self::USER_TYPE_JUDGE:
			case self::USER_TYPE_REPORTER:
				$this->tpl->tabs = array(
					1 => array('Case Management','/cases'),
					4 => array('My Account','/myaccount')
				);
				if( $this->user->clientAdmin ) {
					$this->tpl->tabs[2] = ['User Management','/users'];
					ksort( $this->tpl->tabs );
				}
				break;
			case self::USER_TYPE_ADMIN:
				$this->tpl->tabs = array(
					1 => array('Resellers','/resellers'),
					6 => array('Clients','/clients', [
							['label'=>'Enterprise Clients','url'=>'/clients'],
							['label'=>'Court Clients','url'=>'/courtclients'],
						]),
					2 => array('Reports','/reports/user',[
							['label'=>'User Report','url'=>'/reports/user'],
							['label'=>'Commission Report','url'=>'/report/commission'],
							['label'=>'Usage Report','url'=>'/reports']
						]),
					3 => array('Notify Users','/notifications'),
					4 => array('Get Exhibits','/exhibitsdownload'),
					5 => array('My Account','/myaccount')
				);
				break;
			case self::USER_TYPE_SUPERADMIN:
				$this->tpl->tabs = array(
					1 => array('Resellers','/resellers'),
					6 => array('Clients','/clients', [
							['label'=>'Enterprise Clients','url'=>'/clients'],
							['label'=>'Court Clients','url'=>'/courtclients'],
						]),
					2 => array('Reports','/reports/user',[
							['label'=>'User Report','url'=>'/reports/user'],
							['label'=>'Commission Report','url'=>'/report/commission'],
							['label'=>'Usage Report','url'=>'/reports']
						]),
					3 => array('Notify Users','/notifications'),
					4 => array('Admins','/admins'),
					5 => array('My Account','/myaccount')
				);
				break;
		}
	}

  public static function isAdminBackend()
  {
     $reg = Core\Register::getInstance();
	 $uriPath = "/{$reg->uri->path[0]}";
	 return ($uriPath === $reg->config->adminPath || $uriPath === $reg->config->signupPath);
  }

	public static function isCourtsBackend() {
		$reg = Core\Register::getInstance();
		return '/' . $reg->uri->path[0] === $reg->config->courtsPath;
	}

  public function logout()
  {
    $this->endExistingSession();
    $this->ajax->redirect($this->basePath.'/login');
  }

    public function showPopup($msgType, array $param){
        switch ($msgType) {
            case 'confirm':
                $dlg = $this->get('confirmPopup');
                $dlg->tpl->title = $param['title'];
                $dlg->tpl->warning = $param['warning'];
                $dlg->tpl->message = $param['message'];
                $dlg->get('btnOK')->visible = true;
                $dlg->get('btnOK')->value = $param['OKName'];
                $dlg->get('btnOK')->onclick = $param['OKMethod'];
				if (isset($param['CancelName'])) {
	                $dlg->get('btnCancel')->value = $param['CancelName'];
				}
				if (isset($param['CancelMethod'])) {
	                $dlg->get('btnCancel')->onclick = $param['CancelMethod'];
				}
                $dlg->show();
                break;
            case 'notice':
                $dlg = $this->get('noticePopup');
                $dlg->tpl->header = $param['header'];
                $dlg->tpl->title = $param['title'];
                $dlg->tpl->message = $param['message'];
                $dlg->get('btnCancel')->value = $param['CancelName'];
                $dlg->show();
                break;
        }
    }

   public function showMessage($message, $title = '')
   {
      $dlg = $this->get('confirmPopup');
      $dlg->tpl->title = $title;
      $dlg->tpl->warning = '';
      $dlg->tpl->message = $message;
      $dlg->get('btnOK')->visible = false;
      $dlg->get('btnCancel')->value = 'Close';
      $dlg->show();
   }

    public function hidePopup($msgType){
        switch ($msgType) {
            case 'confirm':
                $this->get('confirmPopup')->hide();
                break;
        }
    }

    public function test_wkhtmltopdf()
    {
       $destination = Core\IO::dir('temp').'/somepdf.pdf';
       $pdf = new Utils\WKHTMLToPDF($this->uri->getURI(), $destination);
       $pdf->setMargins(0,0,0,0)->render();
       Web\Ajax::downloadFile($destination);
    }

    public function isValidDateRangeInURL($startDate = 'startDate', $endDate = 'endDate'/*, $format = 'Y-m-d'*/)
    {
       $startDate = $this->fv[$startDate];
       $endDate = $this->fv[$endDate];
       if (!$startDate || !$endDate) return false;

       $validate = function ($date) use ($format)
       {
          if (!preg_match('/^(\d{4})\-(\d{2})\-(\d{2})$/', $date, $m)) return false;
          return checkdate($m[2], $m[3], $m[1]);
       };
       if (!$validate($startDate) || !$validate($endDate)) return false;

       $startDate = date_create_from_format('Y-m-d', $startDate);
       $endDate = date_create_from_format('Y-m-d', $endDate);
       if (!$startDate || !$endDate) return false;
       return $startDate <= $endDate;
    }

    public function printThisPage()
    {
       $uri = new Utils\URI;
       $uri->query['PRINT']=1;
       $this->ajax->script('window.open("'.addslashes($uri->getQueryString()).'")');
    }

    public function downloadPageAsPDF($uri = null)
    {
       $baseURL = 'http://'.$this->uri->getSource();
       $destination = Core\IO::dir('temp').'/pdf-export-'.$this->user->ID.'.pdf';
       if (is_object($uri)) {
          if (!($uri instanceof Utils\URI)) throw new Core\Exception('Invalid $uri parameter');
       } else {
          $uri = new Utils\URI($uri ? $baseURL.$uri : null);
       }
       $cfg = $this->config->wkhtmltopdf;
       $uri->scheme = $cfg['scheme'];
       //$uri->source = $this->uri->source;
       //$uri->query[session_name()] = session_id();
       $uri->query['PRINT'] = 1;
       $uri->query['PDF'] = 1;
       //echo $uri->getURI();return;
       session_write_close(); // this unlocks session for executed page

       $pdf = new Utils\WKHTMLToPDF($uri->getURI(), $destination);
       $pdf ->setMargins($cfg['margin_top'],$cfg['margin_right'],$cfg['margin_bottom'],$cfg['margin_left'])
            ->setCookie(session_name(), session_id())
            ->setZoom($cfg['zoom'])
            ->setDPI($cfg['dpi'])
            ->setHeader($baseURL.Core\IO::url('backend').'/print/header.php?logo='.$this->tpl->resellerLogo, $cfg['header_spacing'])
            ->setFooter($baseURL.Core\IO::url('backend').'/print/footer.php', $cfg['footer_spacing'])
            ->render();
       session_start(); // resume session for download file to work
       Web\Ajax::downloadFile($destination);
    }

	protected function endExistingSession()
	{
		if (isset( $this->subSession['userID'] ))
		{
			unset( $this->subSession['userID'] );
		}

		if (isset( $this->subSession['userTypeID'] ))
		{
			unset( $this->subSession['userTypeID'] );
		}

		session_destroy();
	}

	public static function priceFromModel( array $model, $optionID, $withTerm=FALSE )
	{
		$withTerm = (bool)$withTerm;
		$cost = 0;
		$typeID = '';
		foreach( $model as $row ) {
			if( $row['optionID'] == $optionID ) {
				$cost = $row['price'];
				$typeID = $row['typeID'];
				break;
			}
		}
		$price = '$' . number_format( $cost, 2 );
		if( !$withTerm ) {
			return $price;
		}
		switch( $optionID ):
			case self::PRICING_OPTION_SUBSCRIPTION:
				$price .= ' (monthly)';
				break;
			case self::PRICING_OPTION_CASE_MONTHLY:
			case self::PRICING_OPTION_DEPOSITION_MONTHLY:
				$price .= '/Month';
				break;
		endswitch;
		return $price;
	}

	public static function termFromModel( array $model, $optionID )
	{
		foreach( $model as $row ) {
			if( $row['optionID'] == $optionID ) {
				return $row['typeID'];
			}
		}
		return self::PRICING_TYPE_MONTHLY;
	}

	public static function exhibitsIntroducedTier( $count )
	{
		$count = (int)$count;
		if( $count > 0 ) {
			if( $count < 16 ) {
				return self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1;
			} elseif( $count < 101 ) {
				return self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2;
			} elseif( $count > 100 ) {
				return self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3;
			}
		}
		return FALSE;
	}

	public static function exhibitsReceivedTier( $count )
	{
		$count = (int)$count;
		if( $count > 0 ) {
			if( $count < 16 ) {
				return self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1;
			} elseif( $count < 31 ) {
				return self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2;
			} elseif( $count > 30 ) {
				return self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3;
			}
		}
		return FALSE;
	}

	public static function createPasscode()
	{
		return str_pad( mt_rand( 0, 99999 ), 5, 0, STR_PAD_LEFT );
	}

	public function isTutorialAvailable() {
		switch($this->user->typeID) {
			case Edepo::USER_TYPE_RESELLER:
				return FALSE;
			case Edepo::USER_TYPE_CLIENT:
				return TRUE;
//				return ($this->user->clients[0]->typeID != self::CLIENT_TYPE_COURTCLIENT);
			case Edepo::USER_TYPE_USER:
			case Edepo::USER_TYPE_REPORTER:
			case Edepo::USER_TYPE_JUDGE:
				return TRUE;
//				$myCMCases = $this->getOrchestraCaseManagers()->getByUser($this->user->ID);
//				if(is_array($myCMCases) && $myCMCases) {
//					return TRUE;
//				}
//				break;
		}
		return FALSE;
	}

	public function showTutorialMenu() {
		$dlg = $this->get( 'tutorialMenu' );
		$menuArray = [];
		$isClientAdmin = FALSE;
		$isCaseMgr = FALSE;
		switch($this->user->typeID) {
//			case Edepo::USER_TYPE_RESELLER:
//				$menuArray = [
//						['name'=>'Reseller Account Management Tutorial', 'url'=>'/videos/webmgr/reseller-account-mgmt.mp4'],
//						['name'=>'Reseller Client Management Tutorial', 'url'=>'/videos/webmgr/reseller-client-mgmt.mp4'],
//						['name'=>'Reseller Reports Tutorial', 'url'=>'/videos/webmgr/reseller-reports.mp4']
//					];
//				break;
			case Edepo::USER_TYPE_CLIENT:
				$isClientAdmin = TRUE;
			case Edepo::USER_TYPE_USER:
			case Edepo::USER_TYPE_REPORTER:
			case Edepo::USER_TYPE_JUDGE:
				$isClientAdmin = ($isClientAdmin || $this->user->clientAdmin);
				$isCaseMgr = (bool)$this->getOrchestraCaseManagers()->getByUser( $this->user->ID );
				$menuArray = [];
				if( $isClientAdmin ) {
					$menuArray[] = ['name' => 'User Management', 'url' => '/videos/webmgr/user-mgmt.mp4'];
				}
				if( $isClientAdmin || $isCaseMgr ) {
					$menuArray[] = ['name' => 'Case Management', 'url' => '/videos/webmgr/case-mgmt.mp4'];
					$menuArray[] = ['name' => 'Session Management', 'url' => '/videos/webmgr/session-mgmt.mp4'];
				}
				$menuArray[] = ['name' => 'My Account Management', 'url' => '/videos/webmgr/account-mgmt.mp4'];
				break;
		}
		$dlg->tpl->tutorialItems = $menuArray;
		$dlg->show();
	}

	public function showTutorialPopup( $src )
	{
		$dlg = $this->get( 'tutorialPopup' );
		$dlg->tpl->mimeType = 'video/mp4';
		$dlg->tpl->src = $this->basePath.'/tutorial/video?src='.$src;
		$dlg->show();
	}

	public function showSaveChangesPrompt( $fileName ) {
		$dlg = $this->get('saveChangesPromptPopup');
		$dlg->tpl->title = 'Abort Changes?';
		$dlg->tpl->warning = '';
		$dlg->tpl->message = 'You may have changes to this document. Are you sure you want to exit without saving?';

		$dlg->get('btnNo')->onclick = 'closeDontSave()';
		$dlg->get('btnCancel')->onclick = 'closeSaveChangesPromptPopup()';
		$dlg->get('btnOK')->onclick = "showSaveFileAnnotationsPopup('{$fileName}', 1);";
		$dlg->show();
	}

	public function showSaveFileAnnotationsPopup( $folderID, $filename )
	{
		$popup = $this->get('saveFilePopup');
		$lastFolderID = (isset( $folderID )) ? $folderID : FALSE;

		// TODO: Validate/sanitize filename?
		$filename = (isset( $filename )) ? $filename : FALSE;
		// Remove .pdf from end of filename
		$filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);

		if( isset( $this->deposition ) ) {
			// Session view
			$session_view = TRUE;

			// Make sure we at least have a personal folder in which to save
			$this->getOrchestraFolders()->createPersonalFolder( $this->deposition->ID, $this->user->ID );
			$folders = $this->getOrchestraFolders()->getFoldersByUserID( $this->deposition->ID, $this->user->ID );
			$this->refreshFolders();
			$this->ajax->script( "selectFolder({$folderID});" );
		}
		elseif( isset( $this->case ) ) {
			// Case view
			$session_view = FALSE;
			$folders = $this->getOrchestraCases()->getCaseFolders( $this->case->ID, $this->user->ID );
		}
		else {
			throw new \LogicException( 'Not a valid view for saving files.' );
		}

		$options = [];
		foreach( $folders as $folder ) {
			if((
				$session_view && in_array( $folder['class'], [IEdepoBase::FOLDERS_PERSONAL, IEdepoBase::FOLDERS_FOLDER, IEdepoBase::FOLDERS_TRUSTED] )
			) ||
			(
				!$session_view && $folder['caseID'] == $this->case->ID
			)) {
				$options[$folder['ID']] = $folder['name'];
			}
		}
		asort( $options );
		$folderList = $popup->get( 'saveFolderList' );
		$folderList->options = $options;
		$folderList->value = (isset( $options[$lastFolderID] )) ? $options[$lastFolderID] : reset( $options );
		$folderList->key = (isset( $options[$lastFolderID] )) ? $lastFolderID : key( $options );

		$popup->get( 'saveFilename' )->value = $filename;
		$popup->get( 'btnCancel' )->value = 'Cancel';
		$popup->show();
	}

	public static function caseClassMap() {
		return [
			self::CASE_CLASS_CASE => self::CASE_CLASS_CASE,
			self::CASE_CLASS_DEMO => 'Demo Case'
		];
	}

	public static function sessionClassMap() {
		return [
			self::DEPOSITION_CLASS_ARBITRATION => self::DEPOSITION_CLASS_ARBITRATION,
			self::DEPOSITION_CLASS_DEMO => 'Demo Deposition',
			self::DEPOSITION_CLASS_DEMOTRIAL => self::DEPOSITION_CLASS_DEMOTRIAL,
			self::DEPOSITION_CLASS_DEPOSITION => self::DEPOSITION_CLASS_DEPOSITION,
			self::DEPOSITION_CLASS_HEARING => self::DEPOSITION_CLASS_HEARING,
			self::DEPOSITION_CLASS_MEDIATION => self::DEPOSITION_CLASS_MEDIATION,
			self::DEPOSITION_CLASS_TRIAL => self::DEPOSITION_CLASS_TRIAL,
			self::DEPOSITION_CLASS_TRIALBINDER => self::DEPOSITION_CLASS_TRIALBINDER,
			self::DEPOSITION_CLASS_WITNESSPREP => 'Witness Prep',
			self::DEPOSITION_CLASS_WPDEMO => 'Demo Witness Prep'
		];
	}
}
