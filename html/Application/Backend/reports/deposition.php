<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageDepositionReports extends Backend {

	public function __construct()
	{
		parent::__construct( 'reports/deposition.html' );
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ($this->user->typeID === self::USER_TYPE_RESELLER);
	}

	public function init()
	{
		parent::init();
		$this->head->name = 'Reports';
		$this->tpl->headName = 'Session Reports';
		$this->tpl->countLabel = 'Clients';
		$this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
	}

}
