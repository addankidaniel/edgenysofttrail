<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageCommissionReport extends Backend
{

    public function __construct()
    {
       parent::__construct("reports/commission".($this->isPrintMode()?'_print':'').".html");
    }

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return in_array($this->user->typeID, [self::USER_TYPE_SUPERADMIN,self::USER_TYPE_ADMIN]);
	}

    public function init()
    {
        parent::init();

		if (!$this->isPrintMode()) {
	        $this->head->name = 'Commission Report';
			$this->tpl->headName = 'Commission Report';
		}
		else {
			$this->head->name = 'Reseller Report';
			$this->tpl->headName = 'Reseller Report';
		}
        $this->tpl->countLabel = 'Count';
        $this->tpl->isAdmin = true;

        if (!$this->isPrintMode()) {
            $this->get('status')->options = array(''=>'All','1'=>'Active','0'=>'Inactive');
            $this->get('status')->onchange = "ajax.doit('->filterStatus')";
			$this->get('month')->options = [
				1=>'January',
				2=>'February',
				3=>'March',
				4=>'April',
				5=>'May',
				6=>'June',
				7=>'July',
				8=>'August',
				9=>'September',
				10=>'October',
				11=>'November',
				12=>'December'
			];
			$month = isset($this->session['WidgetCommissionReport::month']) ? $this->session['WidgetCommissionReport::month'] : date('m');
			$this->get('month')->value = (int)$month;
//			$this->get('month')->onchange = "widgetUser.applyFilter();";
			$yearRange = range( 2014, date('Y') );
			$this->get('year')->options = array_combine( $yearRange, $yearRange );
			$year = (int)isset($this->session['WidgetCommissionReport::year']) ? $this->session['WidgetCommissionReport::year'] : date('Y');
			$this->get('year')->value = (int)$year;
//			$this->get('year')->onchange = "widgetUser.applyFilter();";

			if (isset($this->session['WidgetCommissionReport::isAllSelected'])) {
				if ($this->session['WidgetCommissionReport::isAllSelected'])  {
					if (!$this->get('commission')->isAllSelected()) {
						$this->get('commission')->selectAll();
					}
				}
			} else {
				$this->get('commission')->selectAll();
			}
        }
        if (isset($this->subSession['filter_active'])) {
			if (!$this->isPrintMode()) {
				$this->get('status')->value = $this->subSession['filter_active'];
			}
            $this->get('commission')->active = $this->subSession['filter_active'];
            unset($this->subSession['filter_active']);
        }
    }

    public function filterStatus(){
        $this->get('commission')->active = $this->get('status')->value;
        $this->get('commission')->location = $this->get('location')->value;
		$this->subSession['filter_active'] = $this->get('commission')->active;
		$this->subSession['filter_location'] = $this->get('commission')->location;
    }

    public function exportList(){
        $this->get('commission')->exportToExcel();
    }

    public function isPrintable() {
		return true;
    }

    public function downloadPageAsPDF($uri = null) {
		$this->subSession['filter_active'] = $this->get('commission')->active;
		parent::downloadPageAsPDF($uri);
    }

	public function saveSessionData($key, $value) {
		$this->session[$key] = $value;
	}

	public function getSessionData($key) {
		if (isset($this->session[$key])) {
			return $this->session[$key];
		}
		return null;
	}
}
?>

