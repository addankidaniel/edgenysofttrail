<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo;

class PageUsageDetail extends Backend {

	private $baseURL = '/';
	private $reports = [];

	/**
	 * @var \ClickBlocks\DB\OrchestraDepositionAttendees
	 */
	private $orchDA = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraPricingModelOptions
	 */
	private $orchPMO = NULL;

	public function __construct()
	{
        parent::__construct( 'reports/usagedetail' . ($this->isPrintMode() ? '_print' : '') . '.html' );
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ($this->user->typeID === self::USER_TYPE_RESELLER);
	}

	public function init()
	{
		parent::init();
		$this->orchDA = new \ClickBlocks\DB\OrchestraDepositionAttendees();
		$this->orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		$this->head->name = 'Reports';
		$this->baseURL .= mb_strtolower( $this->reseller['URL'] );
		$this->tpl->url = $this->baseURL;
		$sDate = (int)$this->fv['startDate'];
		$eDate = (int)$this->fv['endDate'];
		if( $sDate <= $eDate ) {
			$this->tpl->startDate = date( 'm/d/Y', $sDate );
			$this->tpl->endDate = date( 'm/d/Y', $eDate );
		} else {
			$this->tpl->startDate = date( 'm/d/Y', $eDate );
			$this->tpl->endDate = date( 'm/d/Y', $sDate );
		}
		$startDate = date( 'm/d/Y', strtotime( $this->tpl->startDate ) );
		$endDate = date( 'm/d/Y', strtotime( $this->tpl->endDate ) );
		if( !isset( $this->fv['PRINT'] ) ) {
			$this->get( 'startDate' )->value = $startDate;
			$this->get( 'endDate' )->value = $endDate;
		}
		if( isset( $this->fv['IDs'] ) && is_array( $this->fv['IDs'] ) ) {
			foreach( $this->fv['IDs'] as $ID ) {
				$client = new \ClickBlocks\DB\Clients( $ID );
				if( !$client || !$client->ID || $client->ID != $ID || $client->resellerID != $this->reseller['ID'] ) {
					continue;
				}
				$this->usageReport( $client );
			}
		}
		if( !count( $this->reports ) ) {
			header( "Location: {$this->baseURL}/reports" );
		}
		$this->tpl->count = count( $this->reports );
		$this->tpl->reports = $this->reports;
		$this->tpl->userPricingOptions = [
			Edepo::PRICING_OPTION_UPLOAD_PER_DOC,
			Edepo::PRICING_OPTION_WITNESSPREP_ATTENDEE
		];
	}

	public function runReport( $fv )
	{
		$sDate = strtotime( $fv['startDate'] );
		$eDate = strtotime( $fv['endDate'] );
		if( $sDate <= $eDate ) {
			$this->tpl->startDate = date( 'm/d/Y', $sDate );
			$this->tpl->endDate = date( 'm/d/Y', $eDate );
		} else {
			$this->tpl->startDate = date( 'm/d/Y', $eDate );
			$this->tpl->endDate = date( 'm/d/Y', $sDate );
		}
		$url = $this->basePath . '/reports/detail';
		$qs = '?IDs[]=' . implode( '&IDs[]=', array_values( $this->fv['IDs'] ) ) . '&startDate=' . strtotime( $this->tpl->startDate ) . '&endDate=' . strtotime( $this->tpl->endDate );
		$this->ajax->redirect( "{$url}{$qs}" );
	}

	/**
	 * Enable page as printable for _print.html counterpart
	 * @return boolean
	 */
	public function isPrintable()
	{
		return TRUE;
	}

	private function usageReport( \ClickBlocks\DB\Clients $client )
	{
		$report = new \stdClass();
		$report->client = $client->getValues();
		$report->client['contactPhone'] = \ClickBlocks\Utils::formatPhone( $client->contactPhone );
		$report->usage = $this->getOrchestraPlatformLog()->getUsageForClientByDates( $client->resellerID, $client->ID, $this->tpl->startDate, $this->tpl->endDate );
		$this->reports[] = $report;
	}

	public function printThisPage()
	{
		$url = '/' . $this->reseller['URL'] . '/reports/detail';
		$qs = '?IDs[]=' . implode( '&IDs[]=', $_REQUEST['IDs'] ) . "&startDate={$_REQUEST['startDate']}&endDate={$_REQUEST['endDate']}&PRINT=1";
		$this->ajax->script( 'window.open("' . addslashes( "{$url}{$qs}" ) . '")' );
	}

	public function exportXLS()
	{
        $xls = \PHPExcel_IOFactory::load(Core\IO::dir( 'backend' ) . '/_templates/clientreport.xls' );
        $sheet = $xls->setActiveSheetIndex( 0 );
        $sheet->setTitle( 'Client Report' );
		$this->fillSpreadSheet( $sheet );
        $writer = new \PHPExcel_Writer_Excel5( $xls );
		$fName = 'Client Report.xls';
        $fileName = Core\IO::dir( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
        $writer->save( $fileName );
        $this->ajax->downloadFile( $fileName, $fName, 'application/vnd.ms-excel' );
    }

	private function fillSpreadSheet( \PHPExcel_Worksheet $sheet )
	{
		$colHeaders = ['Client', 'Client Address', 'Client City', 'Client State', 'Client Zip Code', 'Client Since', 'Status', 'Location', 'Contact', 'Contact Email', 'Contact Phone',
			'Date', 'Description', 'User', 'Case', 'Session', 'Quantity', 'Wholesale Price', 'Line Total'];
		$rowI = 1;

		foreach ($colHeaders as $i => $header)
		{
			$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $header );
		}

		++$rowI;

		foreach ($this->tpl->reports as $report)
		{
			$clientAddress = $report->client['address2'] ? $report->client['address1'] . ' ' . $report->client['address2'] : $report->client['address1'];
			$clientStatus = ($report->client['deactivated']) ? 'Inactive' : 'Active';
			$clientSince = date( 'm/d/Y', strtotime( $report->client['created'] ) );
			$clientInfo = [$report->client['name'], $clientAddress, $report->client['city'], $report->client['state'], $report->client['zipCode'], $clientSince,
				$clientStatus, $report->client['location'], $report->client['contactName'], $report->client['contactEmail'], $report->client['contactPhone']];
			$clientInfoCount = count($clientInfo);

			if (count($report->usage) == 0)
			{
				$colI = 0;
				for ($i = 0; $i < $clientInfoCount; ++$i)
				{
					$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $clientInfo[$i] );
					++$colI;
				}
			}
			else
			{
				foreach ($report->usage as $usage)
				{
					$colI = 0;
					$lineTotal = $usage['price'] * $usage['quantity'];
					$lineTotal = '$' . number_format($lineTotal, 2, ".", ",");
					$usagePrice = '$' . number_format($usage['price'], 2, ".", ",");
					$usageCreated = date( 'm/d/Y', strtotime( $usage['created'] ) );
					$usageInfo = [$usageCreated, $usage['pricingOption'], $usage['userFullName'], $usage['caseName'], $usage['witness'], $usage['quantity'], $usagePrice, $lineTotal];
					$usageInfoCount = count($usageInfo);

					for ($i = 0; $i < $clientInfoCount; ++$i)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $clientInfo[$i] );
						++$colI;
					}
					for ($i = 0; $i < $usageInfoCount; ++$i)
					{
						$sheet->getCellByColumnAndRow( $colI, $rowI )->setValue( $usageInfo[$i] );
						++$colI;
					}
					++$rowI;
				}
			}
			++$rowI;
		}
	}
}