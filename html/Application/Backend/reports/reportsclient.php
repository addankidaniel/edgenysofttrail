<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageReportsClient extends Backend
{
    public function __construct()
    {
       parent::__construct("reports/reportsclient".($this->isPrintMode()?'_print':'').".html");
    }
    
    public function access() {
       if (!parent::access()) return false;
       $this->noAccessURL = $this->basePath.'/';
       return ($this->user->typeID == self::USER_TYPE_CLIENT);
    }

    public function init()
    {
        parent::init();
        $this->head->name = 'Reports';
        $widget = $this->get('cases');
        $widget->clientID = $this->user->clientID;
    }
    
    public function unload() {
       parent::unload();
       $widget = $this->get('cases');
       $this->get('countCases')->text = $widget->getCountCases();
       $this->get('countDepos')->text = $widget->getCountDepositions();
    }


    public function filterStatus(){
        $this->get('cases')->active = $this->get('status')->value;
    }
    
    public function exportClientList() {
      $this->get('cases')->exportToExcel();
    }
    
    public function isPrintable() {
       return true;
    }
}
?>

