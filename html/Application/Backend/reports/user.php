<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageUserReports extends Backend
{

	public function __construct()
	{
		parent::__construct( 'reports/user.html' );
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN, self::USER_TYPE_RESELLER] );
	}

	public function init()
    {
        parent::init();
        $this->head->name = 'Reports';
		$this->tpl->headName = 'User Reports';
	}
}
