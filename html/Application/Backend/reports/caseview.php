<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageReportsCaseView extends Backend
{
   /**
    * @var array of cases
    */
   protected $case;
   public function __construct()
    {
      $print = $this->isPrintMode();
        parent::__construct("reports/caseview".($print?'_print':'').".html");
        $i = 0;
        foreach ((array)$this->fv['ID'] as $caseID) {
         $this->case[$i] = new DB\Cases;
         $this->case[$i]->assignByID($caseID);
         $i++;
        }
    }
    
    public function access() {
       if (!parent::access()) return false;
       foreach ($this->case as $case) if (!$case->ID) return false;
       return true;
    }
    
    public function isPrintable() {
       return true;
    }

    public function init()
    {
        parent::init();
        $this->js->addTool('validators');
        $this->head->name = 'Admin Reports';
        $this->get('repeat')->count = count($this->case);
        $this->get('depositions')->delete();
        foreach ($this->case as $i=>$case) {
           $panel = $this->get('case_'.$i);
            $widget = $panel->get('depositions');
            $widget->clientID = $case-$widget>clientID;
            $widget->caseID = $case->ID;
            if ($this->isPrintMode()) $widget->setTemplate(Core\IO::dir('backend').'/widgets/depositions/depositionCharges_print.html');
            if ($this->isValidDateRangeInURL()) {
              $startDate = $widget->startDate = $_GET['startDate'];
              $endDate = $widget->endDate = $_GET['endDate'];
            } else {
              $startDate = substr($case->created,0,10);
              $endDate = date('Y-m-d');
            }
            $panel->tpl->caseID = $case->ID;
            $panel->tpl->caseName = $case->name;
            $panel->tpl->caseFees = foo(new DB\OrchestraInvoiceCharges)->getSum(
                    $case->clientID, 
                    $case->ID, 
                    null, 
                    array(self::PRICING_OPTION_CASE_SETUP,self::PRICING_OPTION_CASE_MONTHLY), 
                    $startDate, 
                    $endDate);
            $client = $case->clients[0]->getValues();
            $client['startDate'] = Utils\DT::format($client['startDate'], 'Y-m-d', 'm/d/Y');
            $panel->tpl->client = $client;
            $panel->tpl->isLast = ($i == count($this->case)-1);
        }
        $this->tpl->dateFrom = Utils\DT::sql2date($startDate, 'm/d/Y', 1);
        $this->tpl->dateTo = Utils\DT::sql2date($endDate, 'm/d/Y', 1);
        if (!$this->isPrintMode()) {
            $this->get('startDate')->value = $this->tpl->dateFrom;
            $this->get('endDate')->value = $this->tpl->dateTo;
        }
    }
    
    public function unload() {
       //$this->get('repeat.case_0.depositions')->tpl->test = 'TEST';
       parent::unload();
       foreach ($this->case as $i=>$case) {
          $panel = $this->get('case_'.$i);
         $panel->tpl->countDepos = $panel->get('depositions')->count;
       }
    }
    
    public function exportCSV() {
      $this->get('case_0.depositions')->exportToExcel();
    }
    
}
?>

