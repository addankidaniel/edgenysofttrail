<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageReports extends Backend
{

    public function __construct()
    {
       parent::__construct("reports/reports".($this->isPrintMode()?'_print':'').".html");
    }

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return in_array($this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN, self::USER_TYPE_RESELLER]);
	}

    public function init()
    {
        parent::init();
        $this->head->name = 'Reports';
		$this->tpl->headName = 'Usage Reports';
		$this->tpl->countLabel = 'Resellers';
		$this->tpl->isAdmin = true;
        if (!$this->isPrintMode()) {
			if (!isset($this->fv['startDate']))  {
				$this->get('startDate')->value = isset($this->session['WidgetReports::startDate']) ? $this->session['WidgetReports::startDate'] : date('m/') . '01' . date('/Y');
			}
			if (!isset($this->fv['endDate']))  {
				$this->get('endDate')->value = isset($this->session['WidgetReports::endDate']) ? $this->session['WidgetReports::endDate'] : date('m/t/Y');
			}
            $this->get('status')->options = array(''=>'All','1'=>'Active','0'=>'Inactive');
            $this->get('status')->onchange = "ajax.doit('->filterStatus')";
			if (isset($this->session['WidgetReports::isAllSelected'])) {
				if ($this->session['WidgetReports::isAllSelected'])  {
					if (!$this->get('reports')->isAllSelected()) {
						$this->get('reports')->selectAll();
					}
				}
			} else {
				$this->get('reports')->selectAll();
			}
        }
        if (isset($this->subSession['filter_active'])) {
			if (!$this->isPrintMode()) {
				$this->get('status')->value = $this->subSession['filter_active'];
			}
            $this->get('reports')->active = $this->subSession['filter_active'];
            unset($this->subSession['filter_active']);
        }
		if (isset($this->subSession['filter_location'])) {
			if (!$this->isPrintMode()) {
				$this->get('location')->value = $this->subSession['filter_location'];
			}
			$this->get('reports')->location = $this->subSession['filter_location'];
			unset($this->subSession['filter_location']);
		}
    }

    public function filterStatus(){
        $this->get('reports')->active = $this->get('status')->value;
        $this->get('reports')->location = $this->get('location')->value;
		$this->subSession['filter_active'] = $this->get('reports')->active;
		$this->subSession['filter_location'] = $this->get('reports')->location;
    }

    public function exportList(){
        $this->get('reports')->exportToExcel();
    }

    public function isPrintable() {
       return true;
    }

    public function downloadPageAsPDF($uri = null) {
       $this->subSession['filter_active'] = $this->get('reports')->active;
       $this->subSession['filter_location'] = $this->get('reports')->location;
       parent::downloadPageAsPDF($uri);
    }

	public function saveSessionData($key, $value) {
		$this->session[$key] = $value;
	}

	public function getSessionData($key) {
		if (isset($this->session[$key])) {
			return $this->session[$key];
		}
		return null;
	}
}
?>

