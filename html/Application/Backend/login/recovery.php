<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\API,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PagePasswordRecovery extends Backend
{
   private $_user = null;

   public function __construct()
    {
        parent::__construct("login/recovery.html");
    }

    public function access()
    {
        return true;
    }

    public function init()
    {
        parent::init();
        $this->head->name = 'Password recovery';
        $this->tpl->cancelUrl = $this->session['loginPath'] ?: $this->basePath.'/login';
    }

	public function recover($fv)
	{
		if (isset( $fv['username'] ) && $this->verifyUserNameByURL( $fv['username'], substr( $this->basePath, 1 ) ))
		{
			try {
				DB\PasswordResetRequests::sendToUser( $this->_user->ID );
			} catch( Exception $e ) {
				// TODO : write mailer error to log, do NOT throw exception
			}
		}
		$this->session['passwordReset'] = true;
		$this->ajax->redirect($this->session['loginPath'] ?: $this->basePath.'/login');
	}

	private function verifyUserNameByURL( $username, $url )
	{
		$this->_user = $this->getOrchestraUsers()->getUserByUsername( $username );

		if (!$this->_user->ID)
		{
			return false;
		}

		$userClient = (new DB\ServiceClients())->getByID( $this->_user->clientID );

		if (!$userClient->ID)
		{
			return false;
		}

		switch ($userClient->typeID)
		{
			default:
				return false;
			case self::CLIENT_TYPE_RESELLER:
			case self::CLIENT_TYPE_ENT_CLIENT:
				$clientUrl = $userClient->URL;
				break;
			case self::CLIENT_TYPE_CLIENT:
				$reseller = (new DB\ServiceClients())->getByID( $this->_user->clients->resellerID );
				$clientUrl = $reseller->URL;
		}
		return ($url === $clientUrl && ($this->_user->isAdmin() === $this->isAdminBackend()));
	}
}

?>