<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageMessage extends Backend
{
    protected $request;
    
    public function __construct()
    {
        parent::__construct("login/message.html");
    }

    public function access()
    {
       return true;
    }
    
    public function init()
    {
        parent::init();
        $this->head->name = 'Error';
        $this->tpl->text = $this->reg->staticMessage;
    }
}

?>

