<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageLogin extends Backend {
	private $afv;
	protected $isReporterLogin;

	public function __construct() {
		parent::__construct( 'login/login.html' );
		$this->isReporterLogin = ( $this->uri->path[1] == 'exhibits' );
	}

	public function access() {
		if( $this->hasAccessToMainPage() && $this->user && $this->user->termsOfService ) {
			$this->noAccessURL = $this->basePath;
			return false;
		}
		return true;
	}

	public function hasAccessToMainPage() {
		if( $this->isReporterLogin ) {
			$depo = new DB\Depositions( $this->subSession['depositionID'] );
			return (bool)$depo->ID;
		} else {
			return parent::access();
		}
	}

	public function init() {
		// clear session when login request is detected
		if( $this->uri->path[( count( $this->uri->path ) - 1 )] == 'login' ) {
			$this->endExistingSession();
		}

		$this->tpl->webapphost = 'https://'.Core\Register::getInstance()->config['webapp_host'];

		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/jquery.mb.browser.js' ), 'link' );
		$this->tpl->isAdmin = $this->isAdminBackend();
		$this->head->name = 'Web Manager';
		$this->tpl->wasSent = $this->session['passwordReset'];
		if( $this->tpl->wasSent ) {
			unset($this->session['passwordReset']);
		}
		$this->session['loginPath'] = $this->uri->getPath();
		$this->tpl->isReporterLogin = $this->isReporterLogin;
		if( $this->isReporterLogin ) {
			$this->get( 'valFields' )->controls = 'depositionKey,email,password';
		}
	}

	public function loginCheck() {
		$fv = $this->afv;
		$user = NULL;
		$orcUsr = $this->getOrchestraUsers();
		if( $this->isReporterLogin ) {
			$auth = $orcUsr->loginCourtReporter( $fv['depositionKey'], $fv['email'], $fv['passauth'] );
			if( $auth && is_array( $auth ) && $auth['sessionID'] ) {
				$this->subSession['backendType'] = self::BACKEND_TYPE_COURT_REPORTER;
				$this->subSession['depositionID'] = $auth['sessionID'];
				$this->subSession['user'] = $auth['email'];
				return TRUE;
			}
		}
		if( !$this->isAdminBackend() && !$this->isCourtsBackend() ) {
			$user = $orcUsr->loginReseller( $fv['username'], $fv['passauth'], $this->reseller['ID'] );
		} else {
			$user = $orcUsr->loginAdmin( $fv['username'], $fv['passauth'] );
			if( !isset( $user['ID'] ) || !$user['ID'] ) {
				$user = $orcUsr->loginCourtClient( $fv['username'], $fv['passauth'] );
				if( isset( $user['ID'] ) && $user['ID'] ) {
					$userObj = new \ClickBlocks\DB\Users( $user['ID'] );
					$client = $userObj->clients[0];
					$basePath = $this->config->courtsPath;
					$this->fv['redir'] = $basePath;	//redirect login to reseller URL
					$this->session[$basePath] = [];
					$this->subSession =& $this->session[$basePath];	//fix subSession to URL basePath
					$this->session['URIBasePath'] = $basePath;
					$this->reseller = $this->reg->reseller = ['URL'=>$basePath];
				} else {
					// Allow login from the admin url and redirect to appropriate reseller
					$user = $orcUsr->loginReseller( $fv['username'], $fv['passauth'], $this->reseller['ID'] );
					if( isset( $user['ID'] ) && $user['ID'] ) {
						$userObj = new \ClickBlocks\DB\Users( $user['ID'] );
						$client = $userObj->clients[0];
						$resellerTypes = [self::CLIENT_TYPE_RESELLER, self::CLIENT_TYPE_ENT_CLIENT];
						$resellerURL = in_array( $userObj->clients->typeID, $resellerTypes ) ? $client->URL : $client->parentClients1[0]->URL;
						$basePath = '/' . $resellerURL;
						$this->fv['redir'] = $basePath;	//redirect login to reseller URL
						$this->session[$basePath] = [];
						$this->subSession =& $this->session[$basePath];	//fix subSession to reseller URL basePath
						$this->session['URIBasePath'] = $basePath;
						$this->reseller = $this->reg->reseller = \ClickBlocks\DB\OrchestraClients::getResellerByURL( $resellerURL );
					}
				}
			}
		}
		if( isset( $user['ID'] ) && $user['ID'] ) {
			session_regenerate_id( TRUE );
			$current_session_id = session_id();
			$this->subSession['userID'] = $user['ID'];
			$this->subSession['userTypeID'] = $user['typeID'];
			$this->subSession['termsOfService'] = $user['termsOfService'];
			$this->user = new \ClickBlocks\DB\Users( $user['ID'] );

			if( $this->user->phpSessionID ) {
				// Kill any previous session for this user.
				session_write_close();

				session_id( $this->user->phpSessionID );
				session_start();
				session_destroy();

				session_id( $current_session_id );
				session_start();
			}

			$this->user->lastLogin = date( 'Y-m-d H:i:s' );
			$this->user->phpSessionID = $current_session_id;
			$this->user->update();
			return TRUE;
		}
		if( isset( $user['unfederated'] ) ) {
			$this->get( 'valLogin' )->text = 'As part of our security improvements a password reset request has been sent to the account owner. Please check your email for instructions.';
		} elseif( isset( $user['activated'] ) ) {
			$this->subSession['activationEmail'] = $user['email'];
			$this->get( 'valLogin' )->text = '** Your account has not been confirmed.<br/>Please check your email and follow the instructions to confirm your account **';
			$dlg = $this->get( 'confirmDialog' );
			$dlg->tpl->message = 'Your account has not been confirmed.<br/>Would you like to re-send the email to confirm your account?';
			$dlg->get( 'btnOK' )->value = 'Send';
			$dlg->get( 'btnOK' )->class = 'big-button blue';
			$dlg->get( 'btnOK' )->onclick = 'ajax.doit("->sendActivationEmail");';
			$dlg->show();
		} elseif( isset( $user['accountBanned'] ) ) {
			$this->get( 'valLogin' )->text = '** Account locked due to too many failed attempts. Please contact your administrator. **';
		} elseif( isset( $user['accountLocked'] ) ) {
			$this->get( 'valLogin' )->text = '** Account locked due to too many failed attempts. Please try again later. **';
		} else {
			$this->get( 'valLogin' )->text = '** The username and/or password you entered is incorrect. **';
		}
		return FALSE;
	}

	public function loginCheckActivate() {
		if( $this->subSession['userID']) {
			$user = foo( new DB\ServiceUsers() )->getByID( $this->subSession['userID'] );
		}
		return true;
	}

	protected function getRedirectUrl() {
		if( $this->fv['redir'] ) {
			return $this->fv['redir'];
		} else {
			return $this->isReporterLogin ? ( $this->basePath . '/exhibits' ) : $this->basePath.'/';
		}
	}

	public function login($fv) {
		$this->afv = $fv;
		$this->session['timeZoneOffset'] = $fv['timeZone'];

		if( !$this->validators->isValid( 'login_serv', true ) ) {
			return false;
		}

		if( isset($this->subSession['termsOfService'] ) && !$this->subSession['termsOfService'] ) {
			$this->showTermsOfService();
		} else {
			$this->ajax->redirect( $this->getRedirectUrl() );
		}

		return true;
	}

	public function showErrorDialog( $b ) {
		$name = "{$b['name']}";
		$version = "{$b['majorVersion']}";
		$popup = $this->get( 'confirmDialog' );
		$popup->tpl->title = 'ERROR';
		$popup->tpl->message = "You are currently using {$name} version {$version}.<br/>It is required to upgrade to a newer version.<br/><span style=\"color:#fe2a30;\">You must update your browser to continue.</span>";
		$popup->get( 'btnCancel' )->value = 'Close';
		$popup->get( 'btnOK' )->visible = FALSE;
		$popup->show();
	}

	public function acceptTermsOfService( $didAccept ) {
		$didAccept = (bool)$didAccept;

		if( $didAccept ) {
			$this->user->termsOfService = 1;
			$this->subSession['termsOfService'] = 1;
			$this->user->update();
			if( isset( $this->session['URIBasePath'] ) ) {
				unset( $this->session['URIBasePath'] );
			}
			$redir = $this->getRedirectUrl();
			$resellerURL = "/{$this->reseller['URL']}";

			// adjust URLs for court clients
			if( in_array( $this->user->typeID, [self::USER_TYPE_CLIENT,self::USER_TYPE_JUDGE,self::USER_TYPE_REPORTER,self::USER_TYPE_USER] ) ) {
				$client = new DB\Clients( $this->user->clientID );
				if( $client && $client->ID && $client->typeID == self::CLIENT_TYPE_COURTCLIENT ) {
					$resellerURL = $this->config->courtsPath;
					if( stripos( $redir, $this->config->adminPath ) !== FALSE ) {
						$redir = $resellerURL;
					}
				}
			}

			$redir = (stripos( $redir, $resellerURL ) === 0) ? $redir : $resellerURL;
			$this->ajax->redirect( $redir );
			return;
		}

		$this->get( 'termsOfService' )->hide();
		$this->endExistingSession();
	}

	public function showTermsOfService() {
		$popup = $this->get( 'termsOfService' );
		$popup->get( 'btnCancel' )->onclick = 'ajax.doit(\'->acceptTermsOfService\', \'' . 0 . '\');';
		$popup->get( 'btnOK' )->onclick = 'ajax.doit(\'->acceptTermsOfService\', \'' . 1 . '\');';
		$popup->show();
	}

	public function sendActivationEmail() {
		if( $this->subSession['activationEmail'] ) {
			PageSignup::sendActivationEmail( $this->subSession['activationEmail'] );
			unset( $this->subSession['activationEmail'] );
		}
		$this->get( 'confirmDialog' )->hide();
	}
}
