<?php

namespace ClickBlocks\UnitTest\MVC;

if(!defined('PHPUnit_MAIN_METHOD')){
    define('PHPUnit_MAIN_METHOD', 'DoorTests::main');
}
require_once(__DIR__ . '/../../../connect_phpunit.php');


class DoorTests{
    public static function main() {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
    public static function suite(){
        $ts= new \PHPUnit_Framework_TestSuite('User Classes');
        $ts->addTestSuite('ClickBlocks\UnitTest\MVC\Backend\PageLogin\LoginCheckTest');
        return $ts;
    }
}
if(PHPUnit_MAIN_METHOD == 'AppTests::main')
    DoorTests::main();

?>