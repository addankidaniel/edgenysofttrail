<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageEnterpriseClients extends Backend
{

	public function __construct()
	{
		parent::__construct( 'eclients/clients.html' );
	}

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return ($this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN);
	}

	public function init()
	{
		parent::init();
		$this->tpl->isAdmin = in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN] );
		// TODO: if not Admin, should not have access to this page

		$this->head->name = $this->tpl->headName = 'Enterprise Client Management';
		$this->tpl->tab = 1;
		$this->tpl->clientEditURL = '/admin/client/add';

		$this->get( 'eclients' )->typeID = self::CLIENT_TYPE_ENT_CLIENT;
		$this->get( 'eclients' )->update();
	}

	public function search()
	{
		$this->get( 'eclients' )->searchValue = trim( $this->get( 'searchText' )->value );
		$this->get( 'eclients' )->update();
	}
}
