<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageEnterpriseClientEdit extends Backend
{
    protected $client = null;
    protected $userclient = null;

    public function __construct()
	{
        parent::__construct( 'eclients/clientEdit.html' );

        $ID = isset($this->fv['ID']) ? (int) $this->fv['ID'] : 0;
        $this->client = (new DB\ServiceClients())->getByID( $ID );

		if ($this->client->ID)
		{
			if ($this->user->typeID == self::CLIENT_TYPE_RESELLER && ($this->client->resellerID != $this->user->clientID))
			{
				$ID = 0;
				$this->client = null;
				throw new \LogicException( "Client ID ({$this->fv['ID']}) not found" );
			}

			if ($this->client->typeID !== self::CLIENT_TYPE_ENT_CLIENT)
			{
				throw new \LogicException( "Client ID ({$this->fv['ID']}) is not Enterprise Client" );
			}
		}

        $userID = (new DB\OrchestraUsers())->getUsersByClient( $ID, self::USER_TYPE_CLIENT );
        $this->userclient = (new DB\ServiceUsers())->getByID( $userID );
    }

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return ($this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN);
	}

    public function init()
	{
        parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url('backend-js').'/sha1.js' ), 'link' );
        $this->tpl->tab = 1;

        // TODO : if $this->user->typeID is not admin/superadmin, throw an exception (??)
		$this->head->name = 'Enterprise Client Edit/Add'; // TODO 'Edit' OR 'Add', not both
		$this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
		$this->tpl->typeName = 'Client'; // 'Enterprise Client';
		$this->tpl->isAdmin = true;

		$rowPricingModelOptions = (new DB\OrchestraPricingModelOptions())->getByClient($this->reg->reseller['ID']);
		$this->tpl->resellerSubcriptionFee = '$' . $this->getPriceFromModel($rowPricingModelOptions, self::PRICING_OPTION_SUBSCRIPTION)
			. ($this->getTermFromModel($rowPricingModelOptions, self::PRICING_OPTION_SUBSCRIPTION)=='M'?'/Month':'');

		$this->tpl->sponsorResellerID = $this->client->resellerID;

        $this->get('step1')->get('state')->options = (array) self::getUSAStates();
        $this->get('step1')->get('state')->value = 'AL';

        $this->get('imgEditor')->showLoader = false;
        $this->get('imgEditor')->uploads = array($this->get('step1')->get('btnUploadLogo')->uniqueID => array(
                'callBack' => '->uploadLogo',
                'maxSize'  => 1024*1024*100, // 100 Mb
                'extension' => array('gif', 'png', 'jpg', 'jpeg', 'pbm', 'tga', 'dds', 'ico', 'bmp', 'svg'),
                'cropWidth' => 344,
                'cropHeight' => 114));

        if ($this->client->ID > 0) {
            $this->tpl->headerName = $this->client->name;
            $this->tpl->resellerName = $this->client->name;
            $this->get('step1')->get('logoLabel')->visible = false;
            $this->tpl->isNew = false;

			$this->tpl->ID = $this->client->ID;

            $this->get('step3')->get('valpassword')->groups = 'ignore';
            $this->get('step3')->get('valconfirmpassword')->groups = 'ignore';

            $this->tpl->deactivated = $this->client->deactivated;
            $this->get('step1')->get('deactivated')->value = $this->client->deactivated ? '1' : '0';
            $this->get('step1')->get('name')->value = $this->client->name;
            $this->get('step1')->get('resellerID')->value = $this->client->resellerID;
			$this->get('step1')->get('salesRep')->value = $this->client->salesRep;
            $this->get('step1')->get('startDate')->value = date_format(date_create($this->client->startDate), 'm/d/Y');
            $this->get('step1')->get('contactName')->value = $this->client->contactName;
            $this->get('step1')->get('contactPhone')->value = $this->client->contactPhone = \ClickBlocks\Utils::formatPhone($this->client->contactPhone);
            $this->get('step1')->get('contactEmail')->value = $this->client->contactEmail;
            $this->get('step1')->get('address1')->value = $this->client->address1;
            $this->get('step1')->get('address2')->value = $this->client->address2;
            $this->get('step1')->get('city')->value = $this->client->city;
            $this->get('step1')->get('ZIP')->value = $this->client->ZIP;
            $this->get('step1')->get('state')->value = $this->client->state;
            $this->get('step1')->get('description')->text = $this->client->description;
            $this->get('step1')->get('bannerColor')->value = $this->client->getBannerColor();
            $this->get('step1')->tpl->textcolor = $this->get('step1')->get('bannerTextColor')->value = $this->client->getBannerTextColor();
            $this->get('step1')->get('URL')->value = mb_strtolower( $this->client->URL );
            if ($this->client->logo && file_exists(Core\IO::dir('logos') . '/' . $this->client->logo) ){
                $this->get('step1')->get('logoImage')->src = Core\IO::url('logos') . '/' . $this->client->logo;
                $this->get('step1')->get('logoName')->value = $this->client->logo;
            } else {
                $this->get('step1')->get('logoLabel')->visible = true;
                $this->get('step1')->get('logoImage')->visible = false;
            }

			$this->get('step3.firstName')->value = $this->userclient->firstName;
			$this->get('step3.lastName')->value = $this->userclient->lastName;
			$this->get('step3.email')->value = $this->userclient->email;
            $this->get('step3.username')->value = $this->userclient->username;

			$auth = (new DB\ServiceUserAuth())->getByUserID( $this->userclient->ID );
			$this->tpl->isAccountLocked = ($auth->hardLock !== null);
        } else {
            $this->tpl->headerName = 'Add New Account';
            $this->tpl->resellerName = 'Add new account';
            $this->get('step1')->get('logoImage')->visible = false;
            $this->get('step1')->get('startDate')->value = date('m/d/Y');
            $this->tpl->isNew = true;
        }

        $this->openTab( 1 );
    }

    public function toStep1($fv) {
        $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step1')->uniqueID . "', '');");
        //$this->get('step1')->visible = true;
        $this->ajax->script("changeTab(0,1)");
    }

    public function toStep2($fv, $isBack = false) {
        if ($isBack) {
            $this->ajax->script("controls.display('" . $this->get('step3')->uniqueID . "', 'none');");

            $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', '');");
            //$this->get('step2')->visible = true;

            $this->ajax->script("changeTab(0,2)");
        } else {
            if ($this->validators->isValid('step1')) {
                $this->ajax->script("controls.display('" . $this->get('step1')->uniqueID . "', 'none');");

                $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', '');");
                //$this->get('step2')->visible = true;

                $this->ajax->script("changeTab(0,2)");
            }
        }
    }

    public function toStep3($fv) {
        if ($this->validators->isValid('step2')) {
            $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', 'none');");
            $this->ajax->script("controls.display('" . $this->get('step3')->uniqueID . "', '');");

            //$this->get('step3')->visible = true;
            $this->ajax->script("changeTab(0,3)");
        }
    }

    public function openTab($step) {
        $this->ajax->script("controls.display('" . $this->get('step1')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step3')->uniqueID . "', 'none');");

        $this->ajax->script("controls.display('" . $this->get('step' . $step)->uniqueID . "', '');");
        //$this->get('step' . $step)->visible = true;
    }

	public function unlock()
	{
		DB\UserAuth::unlockAccount( $this->userclient->ID );
        $this->ajax->redirect( $this->config->adminPath.'/clients' );
	}

    public function checkUsername($ctrls) {
        return foo(new DB\OrchestraUsers)->checkUniqueUsername($this->getByUniqueID($ctrls[0])->value, $this->userclient->ID);
    }

	public function validatePassword( $ctrls )
	{
		$password = $this->getByUniqueID( $ctrls[0] )->value;
		if (strlen( $password ) < 1)
		{
			return true;
		}
		return DB\UserAuth::validatePassword( $password );
	}

    public function checkUrl($ctrls) {
        if (!empty($this->client->URL) && $this->client->URL == $this->getByUniqueID($ctrls[0])->value) {
            return true;
        }
        return foo(new DB\OrchestraClients)->checkUniqueUrl($this->getByUniqueID($ctrls[0])->value);
    }

    public function save()
	{
        $this->ajax->script('btnLock = false');

        $isValid = false;
        if ($this->tpl->isNew)
            $isValid = $this->validators->isValid('step3');
        else
            $isValid = $this->validators->isValid('step1') && $this->validators->isValid('step2') && $this->validators->isValid('step3');
        if (!$isValid)
            return;

        $client = $this->client;
		$client->typeID = self::CLIENT_TYPE_ENT_CLIENT;

		// $client->courtReporterEmail = $this->get( 'step1' )->get( 'courtReporterEmail' )->value;
		$client->startDate = date_format(date_create_from_format('m/d/Y', $this->get('step1')->get('startDate')->value), 'Y-m-d');
        $client->deactivated = $this->get('step1')->get('deactivated')->value == '1' ? 'NOW()' : null;
		$client->resellerID = $this->get('step1')->get('resellerID')->value;
		$client->salesRep = $this->get('step1')->get('salesRep')->value;
        $client->name = $this->get('step1')->get('name')->value;
        $client->contactName = $this->get('step1')->get('contactName')->value;
		$client->contactEmail = $this->get('step1')->get('contactEmail')->value;
        $client->contactPhone = $this->get('step1')->get('contactPhone')->value;
        $client->address1 = $this->get('step1')->get('address1')->value;
        $client->address2 = $this->get('step1')->get('address2')->value;
        $client->city = $this->get('step1')->get('city')->value;
        $client->state = $this->get('step1')->get('state')->value;
        $client->ZIP = $this->get('step1')->get('ZIP')->value;
		$client->URL = empty($this->get('step1')->get('URL')->value) ? null : mb_strtolower( $this->get('step1')->get('URL')->value );

		$client->description = $this->get('step1')->get('description')->text;
		$client->setBannerTextColor($this->get('step1')->get('bannerTextColor')->value);
        $client->setBannerColor($this->get('step1')->get('bannerColor')->value);

		$destDir = Core\IO::dir('logos');
		Core\IO::createDirectories($destDir);
		if ($this->tpl->isNew)
		{
			$client->created = 'NOW()';
            $client->casesDemoDefault = 1;
			$client->logo = $this->get('step1')->get('logoName')->value;
			if ($client->logo)
				rename2(Core\IO::dir('temp') . '/' . $client->logo, $destDir . '/' . $client->logo);
			(new DB\ServiceClients())->insert($client);
		} else {
			if ($client->logo != $this->get('step1')->get('logoName')->value)
			{
				if (is_file($destDir . '/' . $client->logo))
					unlink($destDir . '/' . $client->logo);
				$client->logo = $this->get('step1')->get('logoName')->value;
				rename2(Core\IO::dir('temp') . '/' . $client->logo, $destDir . '/' . $client->logo);
			}
			(new DB\ServiceClients())->update($client);
		}

        // Update PricingModelOptions data from step 2
        $this->insertPricingModelOptions($client->ID, self::PRICING_OPTION_SUBSCRIPTION, $this->get('step2')->get('subscriptionFee')->value, self::PRICING_TYPE_MONTHLY);

		$user = $this->userclient;

        // Data from step 3
        $user->firstName = $this->get('step3.firstName')->value;
		$user->lastName = $this->get('step3.lastName')->value;
        $user->email = $this->get('step3.email')->value;
        $user->username = $this->get('step3.username')->value;
		$user->typeID = self::USER_TYPE_CLIENT;

        if (!$this->tpl->isNew)
		{
            (new DB\ServiceUsers())->update($user);
        } else {
            $user->created = 'NOW()';
            $user->clientID = $client->ID;
            (new DB\ServiceUsers())->insert($user);
            $client->chargeUponCreation();
        }

        $password = $this->get('step3')->get('passauth')->value;
        if (!empty($password) && strlen( $password ) >= DB\UserAuth::ED_MIN_PASSWORD_LENGTH)
		{
			DB\UserAuth::updatePassword( $user->ID, $password, true );
		}


		$this->ajax->redirect( $this->config->adminPath . '/clients' );
    }

	private function insertPricingModelOptions($ID, $optionID, $price, $typeID = self::PRICING_TYPE_MONTHLY)
	{
		$pricingOption = (new DB\ServicePricingModelOptions())->getByID();
		$pricingOption->clientID = $ID;
		$pricingOption->optionID = $optionID;
		$pricingOption->typeID = $typeID;
		$pricingOption->price = floatval(substr($price, 0, 1) == '$' ? substr($price, 1) : $price);
		if ($this->tpl->isNew)
		{
			(new DB\ServicePricingModelOptions())->insert( $pricingOption );
		} else {
			(new DB\ServicePricingModelOptions())->update( $pricingOption );
		}
	}

    private function getPriceFromModel(array $arrOptions, $optionID) {
        foreach ($arrOptions as $row) {
            if ($row['optionID'] == $optionID)
                return $row['price'];
        }
        return 0.00;
    }

    private function getTermFromModel(array $arrOptions, $optionID) {
        foreach ($arrOptions as $row) {
            if ($row['optionID'] == $optionID)
                return $row['typeID'];
        }
        return self::PRICING_TYPE_MONTHLY;
    }

    public function uploadLogo($uniqueID) {
        $info = Utils\UploadFile::getInfo($uniqueID);
        $this->get('step1')->get('logoImage')->src = $info['url'] . '?' . microtime();
        $this->get('step1')->get('logoName')->value = $info['name'];

        $this->ajax->script("controls.display('" . $this->get('step1')->get('logoLabel')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step1')->get('logoImage')->uniqueID . "', '');");
        $this->get('step1')->get('logoLabel')->visible = false;
        $this->get('step1')->get('logoImage')->visible = true;
    }

    public function confirmDeleteAccount() {
        $client = $this->client;
        if (0 == $client->ID)
            return;
        $param = array();
        if ($this->user->typeID != self::USER_TYPE_RESELLER){
            $param['title'] = 'Delete Reseller';
            $param['warning'] = 'Warning!!! This is not recoverable! If you delete this reseller account, all the information related to this account will be lost, including and of the reseller\'s clients and their files.';
            $param['message'] = 'Are you sure you would like to delete this reseller?';
        } else {
            $param['title'] = 'Delete Client';
            $param['message'] = 'Are you sure you would like to delete this client?';
        }
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteAccount(1);";
        $this->showPopup('confirm', $param);
    }

    public function deleteAccount()
	{
        // Hide dialog popup
        $this->hidePopup('confirm');
        $typeID = $this->user->typeID;
        if ($this->client->typeID == self::CLIENT_TYPE_RESELLER)
        {
            // Physically delete Reseller, his Child-clients, his users, with all Cases,Depositions and files & folders!
            $this->client->delete();
        }
        else
        {
           // Only mark Client as deleted (when deleted by Reseller), all Cases, Depositions, Users, Files will remain
           $this->client->setAsDeleted();
        }
        $this->ajax->redirect( $this->config->adminPath.'/clients' );
    }

	public static function getEnterpriseResellers()
	{
		$options = [0 => '&nbsp;'];

		foreach (DB\OrchestraClients::getEnterpriseResellers() as $reseller)
		{
			$options[$reseller['ID']] = $reseller['name'];
		}

		return $options;
	}
}

?>
