<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetReportsCases extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'cases/reportscases.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraCases->getWidgetReportsCasesData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : 1;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['clientID'] = null;
        $this->properties['userID'] = null;
        $this->properties['active'] = null;
        $this->properties['hasCheckboxes'] = true;
        $this->properties['excelFields'] = array('number'=>1, 'name'=>1, 'clientAccount'=>1, 'clientName'=>1, 'CaseManager'=>1);
        $this->properties['excelTemplate'] = 'reportscases.xls';
    }

    public function init() {
       parent::init();
    }

    protected function getData() {
        $html = parent::getData();
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        //$this->tpl->isAdmin = $this->properties['isAdmin'];
        $this->tpl->links = array(1=>$this->sortLink(1, 'Case #'),
            2=>$this->sortLink(2, 'Case Name'),
            3=>$this->sortLink(3, 'Client Account #'),
            4=>$this->sortLink(4, 'Client Name'),
            5=>$this->sortLink(5, 'Case Manager'),
            6=>$this->sortLink(6, 'Active'));
        return $html;
    }
      
    public function getCountCases()
    {
       return (int)$this->properties['count'];
    }

    public function getCountDepositions() {
       $cases = array();
       foreach ((array)$this->properties['rows'] as $row) $cases[] = $row['ID'];
       return (int)foo(new DB\OrchestraDepositions)->getCountByCases($cases);
    }
    
   protected function getSelectedIDs() 
   {
      $ids = array();
      if (count($this->properties['ids']) > 0) {
         $ids = $this->properties['ids'];
      } else {
         $rows = $this->getRowsNoPagination();
         foreach ($rows as $row) $ids[$row['ID']] = 1;
      }
      return $ids;
   }

    public function printSelectedReports() 
    {
       $ids = $this->getSelectedIDs();
       $url = $this->page->basePath.'/report/case?PRINT=1';
       $i = 0;
       foreach ($ids as $caseID=>$v) {
          $url .= '&ID['.$i.']='.(int)$caseID;
          $i++;
       }
       $this->page->downloadPageAsPDF($url);
       //$this->ajax->script("window.open('{$url}')");
    }
    
    public function exportSelectedReports() 
    {
      $xls = \PHPExcel_IOFactory::load(Core\IO::dir('backend').'/_templates/depositioncharges.xls');
      $sheet = $xls->getSheet(0);
      $templateSheet = $sheet->copy();
      $i = 0;
      $params = array();
      $orIC = new DB\OrchestraInvoiceCharges;
      $svC = new DB\ServiceCases;
      $clientID = $this->page->user->clientID;
      $ids = $this->getSelectedIDs();
      foreach ($ids as $caseID=>$v) {
         if ($i > 0) {
            $sheet = $templateSheet->copy();
            $xls->addSheet($sheet);
         }
         $params['caseID'] = $caseID;
         $rows = $orIC->getWidgetDepositionChargesData('rows', $params);
         WidgetDepositionCharges::fillSpreadSheetStatic($sheet, $rows, array(
             'caseFees'=>foo(new DB\OrchestraInvoiceCharges)->getSum($clientID, $caseID, null, array(self::PRICING_OPTION_CASE_SETUP,self::PRICING_OPTION_CASE_MONTHLY)),
             'total' => foo(new DB\OrchestraInvoiceCharges)->getSum($clientID, $caseID)));
         $case = $svC->getByID($caseID);
         $sheet->setTitle($case->name);
         $i++;
      }
      $writer = new \PHPExcel_Writer_Excel5($xls);
      $fileName = Core\IO::dir('temp').'/export-'.$this->page->user->ID.'.xls';
      $writer->save($fileName);
      $this->ajax->downloadFile($fileName, null, 'application/vnd.ms-excel');
    }

}
