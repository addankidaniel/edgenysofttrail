<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetCases extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'cases/cases.html');

        $this->properties['source'] = 'ClickBlocks\DB\OrchestraCases->getWidgetCaseData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : $this->getSortBy();
        $this->properties['clientID'] = null;
        $this->properties['ids'] = null;
        $this->properties['adminids'] = null;
        $this->properties['managerids'] = null;
		$this->properties['depoownerids'] = null;
        $this->properties['depoassistids'] = null;
        $this->properties['depoattendids'] = null;
        $this->properties['democases'] = null;
        $this->properties['userID'] = null;
    }

	public function init()
	{
		$this->tpl->clientID = $this->reg->fv['clientID'];
	}

	protected function getData() {
		$html = parent::getData();
		$sortByMap = [1=>'number', 2=>'name', 3=>'depositions', 4=>'created'];
		$this->tpl->rows = $this->sortRows($this->properties['rows'], $sortByMap);
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
		$this->tpl->isAdmin = $this->properties['isAdmin'];
		$this->tpl->links = [
			$this->sortLink( 1, 'Case #' ),
			$this->sortLink( 2, 'Case Name' ),
			$this->sortLink( 3, 'Sessions' ),
			$this->sortLink( 4, 'Created' )
		];
		return $html;
	}

    public function refresh() {
        $this->init();
        $this->update();
    }

    public function confirmDeleteCase($id) {
        $case = foo(new DB\ServiceCases())->getByID($id);
        if (0 == $case->ID) return;
        $param = array();
        $param['title'] = 'Delete Case';
        $param['message'] = 'Are you sure you want to delete this case & all its content?';
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteCase(event, {$case->ID}, 1);";
        $this->page->showPopup('confirm', $param);
    }

    public function deleteCase( $caseID )
	{
		$caseID = (int)$caseID;
        // Hide dialog popup
        $this->page->hidePopup( 'confirm' );
        $case = foo( new DB\ServiceCases() )->getByID( $caseID );
        if( !$case || !$case->ID || $case->ID != $caseID ) {
			return;
		}
        $case->setAsDeleted();
        $this->update();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $caseID );
	}

	function getSortBy() {
		$prefs = $this->getSortPrefs('Cases');
		switch ($prefs['sortBy']) {
			case 'Case Number':
				$sortBy = 1;
				break;
			case 'Name':
				$sortBy = 2;
				break;
		}
		if ($prefs['sortOrder'] == 'Asc') {
			$sortBy = -1 * $sortBy;
		}
		return $sortBy;
	}

	function getSortPrefs($type) {
		$sortPrefs = (new DB\OrchestraUserSortPreferences())->getSortPreferencesForUserIDBySortObject($this->page->user->ID, $type);
		$sortBy = '';
		$sortOrder = '';
		if ( !empty($sortPrefs) ) {
			switch ($sortPrefs['sortBy']) {
				case 'Name':
					$sortBy = 'Name';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
				case 'Case Number':
					$sortBy = 'Case Number';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
			}
		} else {
			$sortBy = 'Name';
			$sortOrder = 'Asc';
		}

		return ['sortBy'=>$sortBy, 'sortOrder'=>$sortOrder];
	}

	function sortRows($data, $sortByMap) {
		if (!empty($data)) {
			$sortOrder = ($this->properties['sortBy'] > 0) ? SORT_DESC : SORT_ASC;

			foreach ($data as $key => $row) {
				$primary[$key]  = $row[$sortByMap[abs($this->properties['sortBy']) ] ];
				$secondary[$key] = $row['ID'];
			}

			array_multisort($primary, $sortOrder, SORT_NATURAL | SORT_FLAG_CASE, $secondary, SORT_DESC, SORT_NATURAL, $data);
		}

		return $data;
	}
}
