<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetDepositions extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'depositions/depositions.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraDepositions->getWidgetDepositionData';
        $this->properties['searchValue'] = null;
        $this->properties['sortBy'] = -1;
        $this->properties['pageSize'] = 10;	//ignored
        $this->properties['caseID'] = 0;
        $this->properties['userID'] = NULL;
    }

    protected function getData() {
        $html = parent::getData();
        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->count = $this->properties['count'];
        $this->tpl->sortBy = $this->properties['sortBy'];

        return $html;
    }

    public function chooseDepo($id){
        $this->page->chooseDepo($id);
    }
}
