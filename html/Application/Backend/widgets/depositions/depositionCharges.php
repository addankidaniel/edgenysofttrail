<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetDepositionCharges extends WidgetBackend
{

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'depositions/depositionCharges.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraInvoiceCharges->getWidgetDepositionChargesData';
        $this->properties['startDate'] = null;
        $this->properties['endDate'] = null;
        $this->properties['sortBy'] = -1;
        $this->properties['pageSize'] = NULL;
        $this->properties['caseID'] = NULL;
        $this->properties['clientID'] = NULL;
        $this->properties['excelTemplate'] = 'depositioncharges.xls';
    }

    protected function getData() {
        $html = parent::getData();
        //$this->tpl->rows = $this->properties['rows'];
        //$this->tpl->count = $this->properties['count'];
        //$this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->caseID = $this->caseID;
        $this->tpl->total = foo(new DB\OrchestraInvoiceCharges)->getSum($this->clientID, $this->caseID, null, null, $this->startDate, $this->endDate);
        return $html;
    }

    public function fillSpreadSheet(\PHPExcel_Worksheet $sheet) {
       self::fillSpreadSheetStatic($sheet, $this->getRowsNoPagination(),
           array('caseFees'=>$this->page->tpl->caseFees, 'total'=>$this->tpl->total));
    }

   public static function fillSpreadSheetStatic(\PHPExcel_Worksheet $sheet, array $rows, $params = array())
   {

      $rowI = 2;
      $colI = 1;
      $val = function($v) use ($sheet, &$colI, &$rowI)
      {
         $sheet->getCellByColumnAndRow($colI++, $rowI)->setValue($v);
      };
      foreach ($rows as $row) {
         $colI = 1;
         $val($row['uKey']);
         $val('Deposition of '.$row['depositionOf']);
         $val($row['Date']);
         $val(sprintf('$%.2f',$row['SetupFee']));
         $val(sprintf('$%.2f',$row['HostingFee']));
         $val((int)$row['CountDocuments']);
         $rowI++;
      }
      $sheet->setTitle('Case Deposition Details');
      $sheet->getCellByColumnAndRow(2, $rowI+1)->setValue('Case Charges');
      $sheet->getCellByColumnAndRow(3, $rowI+1)->setValue(sprintf('$%.2f',$params['caseFees']));
      $sheet->getCellByColumnAndRow(6, $rowI+1)->setValue('Total');
      $sheet->getCellByColumnAndRow(7, $rowI+1)->setValue(sprintf('$%.2f',$params['total']));
   }
}
?>
