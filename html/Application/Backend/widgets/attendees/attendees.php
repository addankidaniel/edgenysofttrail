<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetAttendees extends WidgetBackend
{
    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'attendees/attendees.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraDepositionAttendees->getWidgetAttendeesData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -1;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 10;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['depositionID'] = NULL;
        $this->properties['file'] = NULL;
        $this->properties['isFileExist'] = false;
        $this->properties['hasCheckboxes'] = true;
        $this->properties['idsFieldName'] = 'email';
    }

	public function init()
	{
		parent::init();
	}

    protected function getData() {
        $html = parent::getData();
        $this->tpl->isFileExist = $this->properties['isFileExist'];
        $this->tpl->emails = $this->properties['ids'];
        $this->tpl->links = array($this->sortLink(1, 'Attendee'), $this->sortLink(2, 'Email'), 'Type');
        return $html;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

    public function selectEmail($email, $name)
    {
      return $this->selectID($email, $name);
    }

    public function selectEmails($select = null)
    {
      return $this->selectAll();
    }

    public function sendEmails($type, $email = null, $name='')
    {
      $emails = array();
      $msg = $this->page->get('noticePopup');

      if($email === null)
      {
        // The ids list has a key and value of the email, so we need to look up
        // the string of the name in the rows array.
        foreach($this->properties['ids'] as $email => $name)
        {
          foreach($this->properties['rows'] as $row)
          {
            if($row['email'] == $email)
            {
              $emails[$email] = $row['name'];
              break;
            }
          }
        }
      }
      else
        $emails[$email] = $name;
      if(!(count($emails) > 0))
        $msg->tpl->title = 'Please select your email(s).';
      else
      {
         $result = $this->page->sendEmails($type, $emails);
        if (count($result) > 0)
          $msg->tpl->title = 'Error sending email(s):'.implode(',',$result);
        else
          $msg->tpl->title = 'Sent email(s) successful.';
      }
      $msg->show();
    }

}
