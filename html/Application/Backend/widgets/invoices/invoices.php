<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetInvoices extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'invoices/invoices.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraInvoices->getWidgetInvoiceData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -1;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['clientID'] = null;
        $this->properties['type'] = 0;
    }

	public function init()
	{
		$this->tpl->clientID = $this->reg->fv['clientID'];
		$this->pageSizeInit();
	}

    protected function getData() {
        $html = parent::getData();

        $this->get('pageSize')->value = $this->get('pageSize1')->value = $this->properties['pageSize'];
        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        $this->tpl->type = $this->properties['type'];
        

        if ($this->properties['type'] == 0) {
            $this->tpl->links = array($this->sortLink(1, 'Invoice ID'),
                $this->sortLink(2, 'Total'),
                $this->sortLink(3, 'Date'),
            );
        } else {
            $this->tpl->links = array($this->sortLink(1, 'Detail Reports'),
                $this->sortLink(2, 'Total'),
                $this->sortLink(3, 'Date'),
            );
        }

        return $html;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }
}
