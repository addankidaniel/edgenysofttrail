<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetInvoiceCharges extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'invoices/invoiceCharges.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraInvoiceCharges->getWidgetInvoiceChargesData';
        $this->properties['searchValue'] = null;
        $this->properties['sortBy'] = -1;
        $this->properties['pageSize'] = 10;
        $this->properties['clientID'] = null;
        $this->properties['ID'] = null;
        $this->properties['type'] = 0;
        $this->properties['startDate'] = null;
        $this->properties['endDate'] = null;
    }

    public function init() {
        $this->tpl->clientID = $this->reg->fv['clientID'];
    }

    public function sort($order) {
        $this->properties['sortBy'] = -$order;
    }

    protected function getData() {
        $html = parent::getData();

        $sum = 0;
        foreach( $this->properties['rows'] as &$row ) {
            $sum += $row['total'];
			$row['numExhibits'] = '';
			if( $row['optionID'] == '10' || $row['optionID'] == '11' || $row['optionID'] == '12') {
				$row['numExhibits'] = 0;
				//exhibits received
				$row['numExhibits'] = (int)foo(new DB\OrchestraDepositions)->getExhibitsReceivedCount( (int)$row['depositionID'], (int)$row['userID'] );
			} elseif( $row['optionID'] == '13' || $row['optionID'] == '14' || $row['optionID'] == '15') {
				$row['numExhibits'] = 0;
				//exhibits introduced
				$row['numExhibits'] = (int)foo(new DB\OrchestraDepositions)->getExhibitsIntroducedCount( (int)$row['depositionID'], (int)$row['userID'] );
				//$sql = "SELECT COUNT(fi.ID) FROM Folders fl INNER JOIN Files fi ON fi.folderID = fl.ID WHERE depositionID = :depoID AND (fl.class = 'Exhibit' OR fi.isExhibit) AND fi.sourceUserID = :userID";
				//$row['numExhibits'] = (int)$this->getDB()->col( $sql, array( 'depoID' => (int)$row['depositionID'], 'userID' => (int)$row['userID'] ) );
				//(int)$this->getDAL()->getDB()->col( $sql, array( 'depoID' => $this->ID, 'userID' => $userID ) );
			}
        }

        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );

        $this->tpl->headers = array('Date','Description','User','Deposition','# of Exhibits','Qty','Unit Price','Line Total');

        $this->page->tpl->sum = $sum;
        $this->tpl->sum = $sum;
        return $html;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

}
