<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetReports extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'reports/reports.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraClients->getWidgetReportData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['typeID'] = \ClickBlocks\MVC\Edepo::CLIENT_TYPE_RESELLER;
        $this->properties['resellerID'] = null;
        $this->properties['active'] = '';
		$this->properties['location'] = null;
        $this->properties['hasCheckboxes'] = true;
    }

    public function init() {
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
		$this->tpl->clientID = $this->reg->fv['clientID'];
		if (!$this->page->isPrintMode())  {
			$ids = $this->page->getSessionData('WidgetReports::ids');
			if (isset($ids))  {
				foreach ($ids as $id)  {
					$this->selectID($id);
				}
			}
		}
	}

    protected function getData() {
        $html = parent::getData();
		// Update the phone number format.
		foreach ($this->properties['rows'] as &$row) {
		   $row['contactPhone'] = \ClickBlocks\Utils::formatPhone($row['contactPhone']);
		}

		$this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        $this->tpl->isAdmin = $this->properties['resellerID'] ? false : true;
        $this->properties['excelTemplate'] = $this->tpl->isAdmin ? 'reportsresellers.xls' : 'reportsclients.xls';

        $this->tpl->isAllSelected = $this->isAllSelected();
        $this->tpl->emails = $this->properties['ids'];

        $this->tpl->links = array(
			$this->sortLink(1, 'ID'),
            $this->sortLink(2, $this->tpl->isAdmin ? 'Name' : 'Client Name'),
            $this->sortLink(3, 'City'),
            $this->sortLink(4, 'State'),
            $this->sortLink(5, 'Primary Contact'),
            $this->sortLink(6, 'Status'),
        );

        $this->page->tpl->count = $this->properties['count'];

        return $html;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

    public function fillSpreadSheet(\PHPExcel_Worksheet $sheet)
	{
		$rows = $this->getRowsNoPagination();
		$arr = array();
		foreach ($rows as $row) {
			$row['contactPhone'] = \ClickBlocks\Utils::formatPhone($row['contactPhone']);
			$arr[] = array(
				$row['ID'],
				$row['name'],
				date_format(date_create($row['startDate']), 'm/d/Y'),
				$row['address1'],
				$row['address2'],
				$row['city'],
				$row['state'],
				$row['zip'],
				($row['deactivated'] ? 'Inactive' : 'Active'),
				$row['contactName'],
				$row['contactEmail'],
				$row['contactPhone'],
				 $row['salesRep']
				);
		}
		$sheet->fromArray($arr, null, 'B3');
		$sheet->getCellByColumnAndRow(2, 2)->setValue("Today's Date: " . date('m/d/Y'));
	}

	protected function getSelectedIDs()
	{
		$ids = array();
		if (count($this->properties['ids']) > 0) {
			$ids = $this->properties['ids'];
		} else {
			$rows = $this->getRowsNoPagination();
			foreach ($rows as $row) $ids[$row['ID']] = 1;
		}
		return $ids;
	}

    public function viewSelectedReports()
    {
		$ids = $this->getSelectedIDs();
		$url = $this->page->basePath.'/report/detailreport';

		//Add start date and end date.
		$url .= '?startDate=' . date_format(date_create($this->get('startDate')->value), 'Ymd');
        $url .= '&endDate=' . date_format(date_create($this->get('endDate')->value), 'Ymd');

		// Only run the report for the items that are checked and visable as part of the filter.
		$i = 0;
		foreach ($ids as $clientID=>$v) {
			$rows = $this->getRowsNoPagination();
			foreach($rows as $row) {
				if (intval($row['ID']) === intval($v)) {
					$url .= '&rID['.$i.']='.(int)$clientID;
					$i++;
				}
			}
		}

		// Save the current date selection.
		$this->page->saveSessionData('WidgetReports::startDate', $this->get('startDate')->value);
		$this->page->saveSessionData('WidgetReports::endDate', $this->get('endDate')->value);
		$this->page->saveSessionData('WidgetReports::isAllSelected', $this->isAllSelected());
		$this->page->saveSessionData('WidgetReports::ids', count($ids) ? $ids : null);

		$this->ajax->redirect($url);
	}

    public function printSelectedReports($detailReports = false, $toPDF = false)
    {
		$ids = $this->getSelectedIDs();
		$url = $this->page->basePath.'/report/'.($detailReports?'detailreport':'invoice').'?PRINT=1';

		// Only run the report for the items that are checked and visable as part of the filter.
		$i = 0;
		foreach ($ids as $clientID=>$v) {
			foreach($this->rows as $row) {
				if (intval($row['ID']) === intval($v)) {
					$url .= '&rID['.$i.']='.(int)$clientID;
					$i++;
				}
			}
		}
		if ($toPDF) $this->page->downloadPageAsPDF($url);
		else $this->ajax->script("window.open('".addslashes($url)."')");
    }
}
