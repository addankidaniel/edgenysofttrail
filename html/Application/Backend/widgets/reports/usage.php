<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
	ClickBlocks\MVC\Edepo,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetUsage extends WidgetBackend
{
	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $user = NULL;

	/**
	 * @var \ClickBlocks\DB\OrchestraClients
	 */
	protected $orchestraClients = NULL;

	public function __construct( $id, $template=NULL )
	{
		parent::__construct( $id, ($template) ? : 'reports/usage.html' );
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraPlatformLog->getWidgetUsageData';
		$this->properties['resellerID'] = $this->page->reg->reseller['ID'];
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['startDate'] = isset( $this->page->pageSession['startDate'] ) ? $this->page->pageSession['startDate'] : date( 'm/01/Y' );
		$this->properties['endDate'] = isset( $this->page->pageSession['endDate'] ) ? $this->page->pageSession['endDate'] : date( 'm/t/Y' );
		$this->properties['active'] = isset( $this->page->pageSession['active'] ) ? $this->page->pageSession['active'] : '';
		$this->properties['location'] = isset( $this->page->pageSession['location'] ) ? $this->page->pageSession['location'] : '';
		$this->properties['ids'] = isset( $this->page->pageSession['ids'] ) ? $this->page->pageSession['ids'] : [];
		$this->properties['hasCheckboxes'] = TRUE;
		$this->user = $this->page->user;
		$this->orchestraClients = new \ClickBlocks\DB\OrchestraClients();
		if( !isset( $this->page->pageSession['ids'] ) ) {
			$this->selectAll();
		}
	}

	public function init()
	{
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
		$this->tpl->clientID = $this->reg->fv['clientID'];
		$this->get('startDate')->value = $this->properties['startDate'];
		$this->get('endDate')->value = $this->properties['endDate'];
		$this->get('status')->options = [''=>'All', '1'=>'Active', '0'=>'Inactive'];
		$this->get('status')->onchange = "widgetUsage.applyFilter();";
		$this->get('status')->value = $this->properties['active'];
		$results = $this->orchestraClients->getResellerLocations( $this->properties['resellerID'] );
		$locations = [''=>'All'];
		if( $results && is_array( $results ) && count( $results ) ) {
			foreach( $results as $row ) {
				$locations[$row['location']] = $row['location'];
			}
		}
		$this->get('location')->options = $locations;
		$this->get('location')->onchange = "widgetUsage.applyFilter()";
		if( in_array( $this->properties['location'], $locations ) ) {
			$this->get('location')->value = $this->properties['location'];
		}
	}

	protected function getData()
	{
		$html = parent::getData();
		$this->filterSelected();
		$this->tpl->rows = $this->properties['rows'];
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
		$this->tpl->isAllSelected = $this->isAllSelected();
		$this->tpl->IDs = $this->properties['ids'];
		$this->tpl->links = [
			$this->sortLink( 1, 'ID' ),
			$this->sortLink( 2, 'Client Name' ),
			$this->sortLink( 3, 'Location' ),
			$this->sortLink( 4, 'City' ),
			$this->sortLink( 5, 'State' ),
			$this->sortLink( 6, 'Primary Contact' ),
			$this->sortLink( 7, 'Status' )
		];

		$this->page->tpl->count = $this->properties['count'];
		return $html;
	}

	public function applyFilter( $fv )
	{
		$sDate = strtotime( $fv['startDate'] );
		$eDate = strtotime( $fv['endDate'] );
		if( $sDate <= $eDate ) {
			$this->properties['startDate'] = date( 'm/d/Y', $sDate );
			$this->properties['endDate'] = date( 'm/d/Y', $eDate );
		} else {
			$this->properties['startDate'] = date( 'm/d/Y', $eDate );
			$this->properties['endDate'] = date( 'm/d/Y', $sDate );
			$this->get( 'startDate' )->value = $this->properties['startDate'];
			$this->get( 'endDate' )->value = $this->properties['endDate'];
		}
		$this->properties['active'] = $fv['status'];
		$this->properties['location'] = $fv['location'];
		$this->savePageSession();
		$this->update();
	}

	protected function savePageSession()
	{
		parent::savePageSession();
		$this->page->pageSession['startDate'] = $this->properties['startDate'];
		$this->page->pageSession['endDate'] = $this->properties['endDate'];
		$this->page->pageSession['active'] = $this->properties['active'];
		$this->page->pageSession['location'] = $this->properties['location'];
		$this->page->pageSession['ids'] = $this->properties['ids'];
	}

	public function selectID( $id, $info=NULL )
	{
		parent::selectID( $id, $info );
		$this->savePageSession();
	}

	public function viewDetail( $fv )
	{
		if( $this->properties['ids'] ) {	//selected count > 0
			$url = $this->page->basePath . '/reports/detail';
			$qs = '?IDs[]=' . implode( '&IDs[]=', array_values( $this->properties['ids'] ) ) . '&startDate=' . strtotime( $fv['startDate'] ) . '&endDate=' . strtotime( $fv['endDate'] );
			$this->ajax->redirect( "{$url}{$qs}" );
			return;
		}
		$dlg = $this->page->get( 'noticePopup' );
		$dlg->tpl->message = '<div style="text-align:center;">No clients have been selected.<br/>Please select a client to view the report.</div>';
		$dlg->get( 'btnCancel' )->value = 'OK';
		$dlg->show();
	}
}
