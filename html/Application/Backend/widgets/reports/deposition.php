<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetDepositionReports extends WidgetBackend {

    public function __construct( $id, $template=NULL )
	{
        parent::__construct( $id, ($template) ? : 'reports/deposition.html' );
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraDepositions->getWidgetDepositionReportData';
        $this->properties['searchValue'] = NULL;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['resellerID'] = $this->reg->reseller['ID'];
        $this->properties['hasCheckboxes'] = TRUE;
		$this->properties['startDate'] = isset( $this->page->pageSession['startDate'] ) ? $this->page->pageSession['startDate'] : date( 'm/01/Y' );
		$this->properties['endDate'] = isset( $this->page->pageSession['endDate'] ) ? $this->page->pageSession['endDate'] : date( 'm/t/Y' );
		$this->properties['ids'] = isset( $this->page->pageSession['ids'] ) ? $this->page->pageSession['ids'] : [];
		$this->properties['excelTemplate'] = 'reportsdeposition.xls';
    }

    public function init()
	{
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
		$this->get( 'startDate' )->value = $this->properties['startDate'];
		$this->get( 'endDate' )->value = $this->properties['endDate'];
	}

	protected function getData()
	{
		$html = parent::getData();
		$this->filterSelected();

		$rowIDs = [];
		foreach( $this->properties['rows'] as $row ) {
			$rowIDs[] = (int)$row['ID'];
		}

		$this->modifyFields($this->properties['rows']);
		$sortByMap = [1=>'ID', 2=>'started', 3=>'clientName', 4=>'caseName', 5=>'depositionOf', 6=>'class'];
        $this->tpl->rows = $this->sortRows($this->properties['rows'], $sortByMap);
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
		$this->tpl->isAllSelected = $this->isAllSelected();
		$this->tpl->selected = $this->properties['ids'];
		$this->tpl->rowIDs = $rowIDs;
		
		$this->tpl->links = [
			$this->sortLink( 1, 'ID' ),
			$this->sortLink( 2, 'Date' ),
			$this->sortLink( 3, 'Client' ),
			$this->sortLink( 4, 'Case' ),
			$this->sortLink( 5, 'Session Name' ),
			$this->sortLink( 6, 'Session Type' )
		];

		$this->page->tpl->count = $this->properties['count'];

		$this->savePageSession();

		return $html;
	}

	public function applyFilter( $fv )
	{
		$sDate = strtotime( $fv['startDate'] );
		$eDate = strtotime( $fv['endDate'] );
		if( $sDate <= $eDate ) {
			$this->properties['startDate'] = date( 'm/d/Y', $sDate );
			$this->properties['endDate'] = date( 'm/d/Y', $eDate );
		} else {
			$this->properties['startDate'] = date( 'm/d/Y', $eDate );
			$this->properties['endDate'] = date( 'm/d/Y', $sDate );
			$this->get( 'startDate' )->value = $this->properties['startDate'];
			$this->get( 'endDate' )->value = $this->properties['endDate'];
		}
		$this->update();
	}

	protected function savePageSession()
	{
		$this->page->pageSession['sortBy'] = $this->properties['sortBy'];
		$this->page->pageSession['pageSize'] = $this->properties['pageSize'];
		$this->page->pageSession['pos'] = $this->properties['pos'];
		$this->page->pageSession['startDate'] = $this->properties['startDate'];
		$this->page->pageSession['endDate'] = $this->properties['endDate'];
		$this->page->pageSession['ids'] = $this->properties['ids'];
	}

	public function selectAll()
	{
		$rowIDs = $this->tpl->rowIDs;
		if( count( $this->properties['ids'] ) == count( $rowIDs ) ) {
			$rowIDs = [];
		}
		$this->properties['ids'] = [];
		foreach( $rowIDs as $ID ) {
			$this->properties['ids'][$ID] = $ID;
		}
		$this->tpl->selected = $this->properties['ids'];
	}

	public function selectID( $id, $info=NULL )
	{
		return parent::selectID( $id, $info );
	}

	public function detailReport( $fv )
	{
		if( $this->properties['ids'] ) {	//selected count > 0
			$url = $this->page->basePath . '/reports/deposition/detail';
			$qs = '?IDs[]=' . implode( '&IDs[]=', array_values( $this->properties['ids'] ) );
			$this->ajax->redirect( "{$url}{$qs}" );
			return;
		}
		$dlg = $this->page->get( 'noticePopup' );
		$dlg->tpl->message = '<div style="text-align:center;">No depositions have been selected.<br/>Please select a deposition to view the report.</div>';
		$dlg->get( 'btnCancel' )->value = 'OK';
		$dlg->show();
	}

	function sortRows($data, $sortByMap) {
		if (!empty($data)) {
			$primarySortOrder = ($this->properties['sortBy'] > 0) ? SORT_DESC : SORT_ASC;

			foreach ($data as $key => $row) {
				$primary[$key]  = $row[$sortByMap[abs($this->properties['sortBy']) ] ];
				if ($sortByMap[abs($this->properties['sortBy']) ] !== 'class') {
					$secondary[$key] = $row['ID'];
					$secondarySortOrder = SORT_DESC;
				} else {
					$secondary[$key] = $row['depositionOf'];
					$secondarySortOrder = SORT_ASC;
				}
			}

			array_multisort($primary, $primarySortOrder, SORT_NATURAL | SORT_FLAG_CASE, $secondary, $secondarySortOrder, SORT_NATURAL | SORT_FLAG_CASE, $data);
		}
		
		return $data;
	}
	
	function modifyFields(&$data) {
		$classMap = ['Deposition'=>'Deposition', 'WitnessPrep'=>'Witness Prep', 'Trial'=>'Trial', 'Arbitration'=>'Arbitration', 'Mediation'=>'Mediation', 'Hearing'=>'Hearing'];
		
		foreach ($data as $key => &$row) {
			$row['class'] = $classMap[$row['class']];
		}
	}
}
