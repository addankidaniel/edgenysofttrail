<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetSummaryPeriod extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'reports/summaryPeriod.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraInvoiceCharges->getWidgetInvoiceChargesData';
        $this->properties['searchValue'] = null;
        $this->properties['sortBy'] = -1;
        $this->properties['pageSize'] = 10;
        $this->properties['clientID'] = null;
        $this->properties['ID'] = null;
        $this->properties['type'] = 0;
        $this->properties['startDate'] = null;
        $this->properties['endDate'] = null;
        $this->properties['sum'] = 0.00;
        $this->properties['isSummary'] = true;
    }

    public function init() {
        $this->tpl->clientID = $this->reg->fv['clientID'];
    }

    public function sort($order) {
        $this->properties['sortBy'] = -$order;
    }

    protected function getData() {
        $html = parent::getData();
        
        $sum = 0;
        foreach ($this->properties['rows'] as $row){
            $sum += $row['total'];
        }

        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        $this->tpl->total = $sum;
        
        $this->tpl->links = array($this->sortLink(1, 'Summary for Period'),
            $this->sortLink(2, 'Quantity'),
        );
        
        $this->properties['sum'] = $sum;
        $this->get('sum')->text = $sum;

        return $html;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

}
