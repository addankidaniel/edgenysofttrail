<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

abstract class WidgetBackend extends Widget implements \ClickBlocks\API\IEdepoBase
{
   protected $allRows = null;

   public function __construct($id, $template = null)
   {
      parent::__construct($id, ($template) ? Core\IO::dir('backend') . '/widgets/' . $template : '');
      $this->properties['source'] = null;
      $this->properties['ids'] = array();
      $this->properties['idsFieldName'] = 'ID';
      $this->properties['hasCheckboxes'] = false;
      $this->properties['excelRows'] = null;
      $this->properties['print'] = false;
   }

	public function init()
	{
		parent::init();
		$this->tpl->class = get_class( $this );
		$this->tpl->uniqueID = $this->attributes['uniqueID'];
		if( $this->page->isPrintMode() ) {
			$this->print = TRUE;
		}
		$this->pageSizeInit();
	}

	protected function pageSizeInit()
	{
		foreach( ['pageSize','pageSize1','pageSize2'] as $ctrl ) {
			$ps = $this->get( $ctrl );
			if( $ps !== FALSE ) {
				$ps->onchange = $this->method( '__set', ['pageSize', 'js::this.innerHTML'] );
				$ps->value = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
			}
		}
	}

	public function sort( $order )
	{
		$this->properties['sortBy'] = $this->page->pageSession['sortBy'] = -$order;
	}

   public function getRowsNoPagination()
   {
      if (!$this->allRows) {
         $oldSize = $this->properties['pageSize'];
         $this->properties['pageSize'] = null;
         $this->allRows = $this->getRows();
         $this->properties['pageSize'] = $oldSize;
      }
      return $this->allRows;
   }

   public function getCount()
   {
      $method = new Core\Delegate($this->properties['source']);
      return $method('count', $this->properties);
   }

   public function getRows()
   {
      $method = new Core\Delegate($this->properties['source']);
      return $method('rows', $this->properties);
   }

   protected function execute()
   {
      if ($this->properties['disabled']) return;
      if ($this->properties['pageSize']) {
        $this->properties['count'] = $this->getCount();
        $this->normalizeProperties();
        $this->properties['rows'] = $this->getRows();
      } else
      {
        $this->properties['rows'] = $this->getRows();
        $this->properties['count'] = count($this->properties['rows']);
      }
      return $this;
   }

   protected function getData()
   {
      if ($this->properties['count'])
      {
         $from = $this->properties['pageSize'] * $this->properties['pos'] + 1;
         $to = $from + $this->properties['pageSize'] - 1;
         if ($to > $this->properties['count']) $to = $this->properties['count'];
      }
      $this->tpl->from = (int)$from;
      $this->tpl->to = (int)$to;
      if ($this->properties['hasCheckboxes']) {
         $this->tpl->ids = $this->properties['ids'];
         $this->tpl->isAllSelected = $this->isAllSelected();
      }
      $this->tpl->rows = $this->properties['rows'];
      $this->tpl->sortBy = $this->properties['sortBy'];
      $this->tpl->print = $this->properties['print'];
   }

   protected function sortLink($sortBy, $title)
   {
      if (abs($this->properties['sortBy']) == $sortBy)
      {
         $sortBy = $this->properties['sortBy'];
         if ($this->properties['sortBy'] < 0)
         {
             $arr = '<span class="sort" onclick="'.$this->method('sort', array((int)$sortBy)).'"></span>';
         }
         else
         {
             $arr = '<span class="sort top" onclick="'.$this->method('sort', array((int)$sortBy)).'"></span>';
         }
      }
      $text = new Helpers\StaticText(null, $title);
      $text->tag = 'span';
      $text->addStyle('cursor', 'pointer');
      //$text->addStyle('float', 'left');
      $text->onclick = $this->method('sort', array((int)$sortBy));
      return $text . $arr;
   }

	public function __set($param, $value)
	{
		if( $param == 'pageSize' ) {
			foreach( ['pageSize','pageSize1','pageSize2'] as $ctrl ) {
				$ps = $this->get( $ctrl );
				if( $ps ) {
					$ps->value = $value;
				}
			}
		} elseif( $param == 'searchText' && !empty( $this->properties['ids'] ) ) {
		   $this->properties['ids'] = [];
		} elseif( $param == 'print' && $value == 1 ) {
		   $this->properties['pageSize'] = null;
		} elseif( $param === 'pos' ) {
			$this->page->pageSession['pos'] = $value;
		}
		parent::__set($param, $value);
	}

   public function setPrintMode($mode = true)
   {
      $this->print = $mode;
   }

   public function selectID($id, $info = null)
   {
      if (!isset($this->properties['ids'][$id]))
          $this->properties['ids'][$id] = ($info ?: $id);
      else
          unset($this->properties['ids'][$id]);
   }

   public function selectAll()
   {
       $idf = $this->properties['idsFieldName'];
       $rows = $this->getRowsNoPagination();
       if (!$this->isAllSelected()) {
           foreach ($rows as $row)
               $this->properties['ids'][$row[$idf]] = $row[$idf];
       } else {
           foreach ($rows as $row)
               unset($this->properties['ids'][$row[$idf]]);
       }
   }

	public function isAllSelected()
	{
		if( !$this->properties['ids'] ) {
			return FALSE;
		}
		$rows = $this->getRowsNoPagination();
		if( !count( $rows ) ) {
			return FALSE;
		}
		$idf = $this->properties['idsFieldName'];
		foreach( $rows as $row ) {
			$rowID = $row[$idf];
			if( empty( $this->properties['ids'][$rowID] ) ) {
				return FALSE;
			}
		}
		return TRUE;
	}

	public function filterSelected()
	{
		if( !isset( $this->properties['ids'] ) || !is_array( $this->properties['ids'] ) || !$this->properties['ids'] ) {
			return;
		}
		if( !isset( $this->allRows ) ) {
			$this->getRowsNoPagination();
		}
		$allIDs = [];
		$idf = $this->properties['idsFieldName'];
		foreach( $this->allRows as $row ) {
			$allIDs[] = $row[$idf];
		}
		foreach( $this->properties['ids'] as $rowID => $selected ) {
			if( !in_array( $selected, $allIDs ) ) {
				unset( $this->properties['ids'][$rowID] );
			}
		}
	}

   public function fillSpreadSheet(\PHPExcel_Worksheet $sheet)
   {
      if (!$this->properties['excelFields']) return false;
      $rows = $this->getRowsNoPagination();
      foreach ($rows as $rowI=>$row) {
        $colI = 1;
        foreach ($this->properties['excelFields'] as $field=>$info) {
          $cell = $sheet->getCellByColumnAndRow($colI,$rowI+2);
          $dt=$cell->getDataType();
          $cell->setValueExplicit($row[$field],$dt);
          //$cell->setDataType($dt);
          $colI++;
        }
      }
   }

   public function exportToExcel()
   {
      $xls = \PHPExcel_IOFactory::load(Core\IO::dir('backend').'/_templates/'.$this->properties['excelTemplate']);
      /*$xls->getProperties()->setCreator($this->user->firstName . ' ' . $this->user->lastName)
              ->setTitle("Survey List")
              ->setDescription("Survey list export");*/

      $sheet = $xls->setActiveSheetIndex(0);
      //$sheet->setTitle('Survey List');
      $this->fillSpreadSheet($sheet);
      $writer = new \PHPExcel_Writer_Excel5($xls);

	  if ($this->properties['id']) {
		 switch (strtolower($this->properties['id'])) {
			 case 'notifications':
				 $fName = 'Notification Email List';
				 break;
			 case 'notificationlogs':
				 $fName = 'Notification Logs';
				 break;
			 case 'reports':
				 if ($this->properties['typeID'] == 'R') {
					 $fName = 'Reseller List';
				 } else {
					 $fName = 'Client List';
				 }

				 break;
		 }
	  } else {
		  $fName = 'export';
	  }

	  $fName .= '.xls';

      $fileName = Core\IO::dir('temp').'/'.\ClickBlocks\Utils::createUUID();
      $writer->save($fileName);
      Web\Ajax::downloadFile($fileName, $fName, 'application/vnd.ms-excel');
   }

   protected function savePageSession()
	{
		$this->page->pageSession['sortBy'] = $this->properties['sortBy'];
		$this->page->pageSession['pageSize'] = $this->properties['pageSize'];
		$this->page->pageSession['pos'] = $this->properties['pos'];
	}
}