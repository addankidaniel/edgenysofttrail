<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetEClients extends WidgetBackend
{
    public function __construct($id, $template = null)
    {
        parent::__construct($id, ($template) ? : 'clients/eclients.html');

		$clientsSession = $_SESSION[Edepo::SESSION_NS]['clients'];

        $this->properties['source'] = 'ClickBlocks\DB\OrchestraClients->getWidgetClientData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['typeID'] = Edepo::CLIENT_TYPE_RESELLER;
        $this->properties['resellerID'] = null;
		$this->properties['showDeleted'] = TRUE;
    }

    public function init()
    {
		$this->tpl->clientID = $this->reg->fv['clientID'];
		$this->pageSizeInit();
    }

    protected function getData()
    {
    	$html = parent::getData();
        $this->get('pageSize')->value = $this->get('pageSize1')->value = $this->properties['pageSize'];
        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->searchValue = $this->properties['searchValue'];
        if( in_array( $this->properties['typeID'], [Edepo::CLIENT_TYPE_RESELLER] ) ) {
            $this->tpl->links = [
				$this->sortLink(1, 'ID'),
				$this->sortLink(2, 'Reseller Name'),
				$this->sortLink(4, 'Primary Contact'),
				$this->sortLink(5, 'Reseller Since'),
				$this->sortLink(6, 'Status')
			];
		} else
		if (in_array( $this->properties['typeID'], [Edepo::CLIENT_TYPE_CLIENT, Edepo::CLIENT_TYPE_ENT_CLIENT] ))
		{
            $this->tpl->links = [
				$this->sortLink(1, 'ID'),
				$this->sortLink(2, 'Client Name'),
				$this->sortLink(4, 'Primary Contact'),
				$this->sortLink(5, 'Client Since'),
				$this->sortLink(6, 'Status'),
				$this->sortLink(7, 'Sponsor')
			];
            $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        }
        return $html;
    }

    public function refresh()
    {
        $this->init();
        $this->update();
    }

    public function confirmActivateAccount($id){
        $client = foo(new DB\ServiceClients())->getByID($id);
        if (0 == $client->ID) return;
        $param = array();
        $param['title']=$client->deactivated ? 'Activate Account' : 'Deactivate Account';
        $param['message']='Are you sure you want to '. ($client->deactivated ? 'activate ' : 'deactivate ') . $client->name.'?';
        $param['OKName']=$client->deactivated ? 'Activate' : 'Deactivate';
        $param['OKMethod']="activateAccount({$client->ID}, 1);";
        $this->page->showPopup('confirm', $param);
    }

    public function activateAccount($id){
        // Hide dialog popup
        $this->page->hidePopup('confirm');
        $service = new DB\ServiceClients();
        $client = $service->getByID($id);
        if (0 == $client->ID) return;
        $client->deactivated = $client->deactivated ? NULL : 'NOW()';
        //if (empty($client->URL)) $client->URL = NULL;
        $service->save($client);
        $this->update();
    }
}
