<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetDatasources extends WidgetBackend
{
	public function __construct( $id, $template=NULL )
	{
		parent::__construct( $id, ($template) ? : 'datasources/datasources.html' );
		$this->properties['source'] = 'ClickBlocks\MVC\Backend\PageMyAccountDatasource->getWidgetDatasourcesData';
		$this->properties['searchValue'] = NULL;
		$this->properties['sortBy'] = -1;
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['clientID'] = NULL;
		$this->properties['depositionID'] = NULL;
		$this->properties['file'] = NULL;
		$this->properties['isFileExist'] = FALSE;
		$this->properties['hasCheckboxes'] = FALSE;
	}

	protected function getData()
	{
		$html = parent::getData();
		$this->tpl->links = ['Platform', 'Name', 'URL'];
		return $html;
	}

	public function refresh()
	{
		$this->init();
		$this->update();
	}
}
