<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\DB,
	ClickBlocks\MVC\Backend\Backend;

class WidgetCourtClients extends WidgetBackend {

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $curUser;

	public function __construct( $id, $template=NULL ) {
		parent::__construct( $id, ($template) ? : 'courts/clients.html' );
		$this->curUser = $this->page->user;
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraClients->getWidgetCourtClientsData';
		$this->properties['searchValue'] = NULL;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : -2;
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['clientID'] = NULL;
	}

	public function init() {
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
	}

	protected function getData() {
		$html = parent::getData();
		$this->get( 'pageSize1' )->value = $this->get( 'pageSize' )->value = $this->properties['pageSize'];
		$this->tpl->rows = $this->properties['rows'];
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->links = [
			$this->sortLink( 1, 'ID' ),
			$this->sortLink( 2, 'Client Name' ),
			$this->sortLink( 3, 'Primary Contact' ),
			$this->sortLink( 4, 'Client Since' ),
			$this->sortLink( 5, 'Status' )
		];
		return $html;
	}

	public function refresh() {
		$this->init();
		$this->update();
	}

	public function search() {
		$this->properties['searchValue'] = $this->tpl->searchValue = $this->page->get( 'searchText' )->value;
		$this->ajax->script( 'controls.focus("' . $this->get( 'searchText' )->uniqueID . '")', 100 );
	}

	public function activateAccount( $clientID ) {
		$this->page->get( 'confirmPopup' )->hide(); // hide popup dialog
		$client = new DB\Clients( $clientID );
		if( !$this->curUser->isAdmin() || !$client || !$client->ID ) {
			return;
		}
		$client->deactivated = ($client->deactivated ? NULL : date( 'Y-m-d H:i:s' ));
		$client->update();
		$this->update();
	}

	public function confirmActivateAccount( $clientID ) {
        $client = new DB\Clients( $clientID );
        $active = $client->deactivated;
        if( !$this->curUser->isAdmin() || !$client || !$client->ID ) {
			return;
		}
        $dlg = $this->page->get( 'confirmPopup' );
        $dlg->tpl->title = $active ? 'Activate Client' : 'Deactivate Client';
        $dlg->tpl->message = 'Are you sure you want to '. ($active ? 'activate ' : 'deactivate ') . '<b>' .$client->name . '</b>?';
        $dlg->get( 'btnOK' )->value = $active ? 'Activate' : 'Deactivate';
        $dlg->get( 'btnOK' )->onclick = "activateAccount({$client->ID},1);";
        $dlg->show();
    }
}
