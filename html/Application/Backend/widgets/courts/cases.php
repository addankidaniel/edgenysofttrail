<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\DB;

class WidgetCourtCases extends WidgetBackend {

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $curUser;

	public function __construct($id, $template = null) {
		parent::__construct( $id, ($template) ? : 'courts/cases.html' );
		$this->curUser = $this->page->user;
		$this->properties['source'] = 'ClickBlocks\DB\OrchestraCases->getWidgetCaseData';
		$this->properties['searchValue'] = NULL;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : $this->getSortBy();
		$this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
		$this->properties['caseIDs'] = NULL;
		$this->properties['adminIDs'] = NULL;
		$this->properties['clientID'] = NULL;
		$this->properties['userID'] = NULL;
	}

	public function init() {
		parent::init();
		$this->sort( $this->sortBy );	//fix opposite sorting from parent::init()
		$this->tpl->clientID = $this->reg->fv['clientID'];
	}

	protected function getData() {
		$html = parent::getData();
		$sortByMap = [1=>'number', 2=>'name', 3=>'depositions', 4=>'created'];
		$this->tpl->rows = $this->sortRows($this->properties['rows'], $sortByMap);
		$this->tpl->sortBy = $this->properties['sortBy'];
		$this->tpl->url = $this->reg->config->courtsPath;
		$this->tpl->isClientAdmin = ($this->curUser->typeID == self::USER_TYPE_CLIENT || $this->curUser->clientAdmin);
		$this->tpl->links = [
			$this->sortLink( 1, 'Case #' ),
			$this->sortLink( 2, 'Case Name' ),
			$this->sortLink( 3, 'Sessions' ),
			$this->sortLink( 4, 'Created' )
		];
		return $html;
	}

	public function refresh() {
		$this->init();
		$this->update();
	}

	public function confirmDeleteCase( $caseID ) {
		$case = new DB\Cases( $caseID );
		if( !$case->ID || $case->clientID != $this->curUser->clientID ) {
			return;
		}
		$param = [];
		$param['title'] = 'Delete Case';
		$param['message'] = 'Are you sure you want to delete this case and all its contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteCase(event,{$case->ID}, 1);";
		$this->page->showPopup( 'confirm', $param );
	}

	public function deleteCase( $caseID )
	{
		$caseID = (int)$caseID;
		// Hide dialog popup
		$this->page->hidePopup( 'confirm' );
		$case = new DB\Cases( $caseID );
		if( !$case || !$case->ID || $case->ID != $caseID || $case->clientID != $this->curUser->clientID ) {
			return;
		}
		$case->setAsDeleted();
		$this->update();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $caseID );
	}
	
	function getSortBy() {
		$prefs = $this->getSortPrefs('Cases');
		switch ($prefs['sortBy']) {
			case 'Case Number':
				$sortBy = 1;
				break;
			case 'Name':
				$sortBy = 2;
				break;
		}
		if ($prefs['sortOrder'] == 'Asc') {
			$sortBy = -1 * $sortBy;
		}
		return $sortBy;
	}
	
	function getSortPrefs($type) {
		$sortPrefs = (new DB\OrchestraUserSortPreferences())->getSortPreferencesForUserIDBySortObject($this->page->user->ID, $type);
		$sortBy = '';
		$sortOrder = '';
		if ( !empty($sortPrefs) ) {
			switch ($sortPrefs['sortBy']) {
				case 'Name':
					$sortBy = 'Name';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
				case 'Case Number':
					$sortBy = 'Case Number';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
			}
		} else {
			$sortBy = 'Name';
			$sortOrder = 'Asc';
		}
		
		return ['sortBy'=>$sortBy, 'sortOrder'=>$sortOrder];
	}
	
	function sortRows($data, $sortByMap) {
		if (!empty($data)) {
			$sortOrder = ($this->properties['sortBy'] > 0) ? SORT_DESC : SORT_ASC;

			foreach ($data as $key => $row) {
				$primary[$key]  = $row[$sortByMap[abs($this->properties['sortBy']) ] ];
				$secondary[$key] = $row['ID'];
			}

			array_multisort($primary, $sortOrder, SORT_NATURAL | SORT_FLAG_CASE, $secondary, SORT_DESC, SORT_NATURAL, $data);
		}
		
		return $data;
	}
}
