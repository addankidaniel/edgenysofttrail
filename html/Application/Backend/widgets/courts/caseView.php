<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
	ClickBlocks\MVC\Edepo,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetCourtCaseView extends WidgetBackend {

	public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'courts/caseView.html');

		$orcCMs = new DB\OrchestraCaseManagers();
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraDepositions->getWidgetCourtDepositionData';
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : $this->getSortBy();
        $this->properties['userID'] = ($orcCMs->checkCaseManager( $this->page->fv['ID'], $this->page->user->ID ) ? NULL : $this->page->user->ID);
        $this->properties['caseID'] = $this->page->fv['ID'];
    }

	public function init() {
		$this->tpl->clientID = $this->reg->fv['clientID'];
		$this->pageSizeInit();
	}

    protected function getData() {
		$html = parent::getData();

		$orchestra = new DB\OrchestraUsers();
		$orcDepo = new DB\OrchestraDepositions();
		$orcDepoAssist = new DB\OrchestraDepositionAssistants();
		foreach ($this->properties['rows'] as $idx => $row) {
			switch ($this->properties['rows'][$idx]['statusID']) {
				case 'N':
					$this->properties['rows'][$idx]['date'] = $this->properties['rows'][$idx]['openDateTime'];
					break;
				case 'I':
					$this->properties['rows'][$idx]['date'] = $this->properties['rows'][$idx]['started'];
					break;
				case 'F':
					$this->properties['rows'][$idx]['date'] = $this->properties['rows'][$idx]['finished'];
			}

			$ownerFirstLastName = $orchestra->getUserFirstAndLastNameByUserID( $row['ownerID'] );
			$this->properties['rows'][$idx]['owner'] = implode(' ', $ownerFirstLastName);

			// Check isOwner
			$this->properties['rows'][$idx]['isOwner'] = FALSE;
			if ( $orcDepo->getOwnerID( $row['ID'] ) === $this->page->user->ID ) {
				$this->properties['rows'][$idx]['isOwner'] = TRUE;
			}
			if( $orcDepoAssist->checkDepositionAssistant( $row['ID'], $this->page->user->ID ) ) {
				$this->properties['rows'][$idx]['isAssistant'] = TRUE;
			}

			$this->properties['rows'][$idx]['viewURL'] = $this->reg->config->courtsPath . "/session/view" . "?caseID={$row['caseID']}&depoID={$row['ID']}";
			$this->properties['rows'][$idx]['editURL'] = $this->reg->config->courtsPath . '/session/' . str_replace( ' ', '', strtolower( $row['class'] ) ) . "?caseID={$row['caseID']}&depoID={$row['ID']}";
		}

		$orcCases = new DB\OrchestraCases();
		$isAdmin = ($this->page->user->typeID == self::USER_TYPE_CLIENT || $this->page->user->clientAdmin);
		$isMgr = $orcCases->checkCaseAdmin( $this->properties['caseID'], $this->page->user->ID );

		$this->tpl->isAdmin = ($isAdmin || $isMgr);
		$this->modifyFields($this->properties['rows']);
		$sortByMap = [1=>'ID', 2=>'date', 3=>'depositionOf', 4=>'volume', 5=>'class', 6=>'status', 7=>'jobNumber', 8=>'location'];
        $this->tpl->rows = $this->sortRows($this->properties['rows'], $sortByMap);
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = $this->reg->config->courtsPath;

        $this->tpl->links = array(
			$this->sortLink(1, 'ID'),
            $this->sortLink(2, 'Date'),
            $this->sortLink(3, 'Name'),
            $this->sortLink(4, 'Volume'),
            $this->sortLink(5, 'Type'),
            $this->sortLink(6, 'Status'),
			$this->sortLink(7, 'Matter'),
			$this->sortLink(8, 'Location')
		);

        return $html;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

    public function confirmDeleteCase($id) {
        $case = foo(new DB\ServiceCases())->getByID($id);
        if (0 == $case->ID) return;
        $param = array();
        $param['title'] = 'Delete Case';
        $param['message'] = 'Are you sure you want to delete this case & all its content?';
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteCase({$case->ID}, 1);";
        $this->page->showPopup('confirm', $param);
    }

    public function deleteCase( $caseID )
	{
		$caseID = (int)$caseID;
        // Hide dialog popup
        $this->page->hidePopup( 'confirm' );
        $case = foo( new DB\ServiceCases() )->getByID( $caseID );
        if( !$case || !$case->ID || $case->ID != $caseID ) {
			return;
		}
        $case->setAsDeleted();
        $this->update();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $caseID );
	}

	public function confirmDeleteDepo($id) {
		$depo = foo( new DB\ServiceDepositions() )->getByID($id);
		if (0 == $depo->ID)
		  return;
		$param = array();
		$param['title'] = 'Delete Session';
		$param['message'] = 'Are you sure you want to delete this session and all its contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteDepo({$depo->ID}, 1);";
		$this->page->showPopup('confirm', $param);
	}

	public function deleteDepo( $depositionID )
	{
		$depositionID = (int)$depositionID;
		$this->page->hidePopup( 'confirm' );
		$depo = foo( new DB\ServiceDepositions() )->getByID( $depositionID );
		if( !$depo->ID || $depo->ID != $depositionID ) {
			return;
		}
		$depo->setAsDeleted();
		$this->update();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $this->page->case->ID );
	}

	function getSortBy() {
		$prefs = $this->getSortPrefs('Depositions');
		switch ($prefs['sortBy']) {
			case 'Date':
				$sortBy = 2;
				break;
			case 'Name':
				$sortBy = 3;
				break;
			case 'Type':
				$sortBy = 5;
				break;
		}
		if ($prefs['sortOrder'] == 'Asc') {
			$sortBy = -1 * $sortBy;
		}

		return $sortBy;
	}

	function getSortPrefs($type) {
		$sortPrefs = (new DB\OrchestraUserSortPreferences())->getSortPreferencesForUserIDBySortObject($this->page->user->ID, $type);
		$sortBy = '';
		$sortOrder = '';
		if ( !empty($sortPrefs) ) {
			switch ($sortPrefs['sortBy']) {
				case 'Date':
					$sortBy = 'Date';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
				case 'Name':
					$sortBy = 'Name';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
				case 'Type':
					$sortBy = 'Type';
					$sortOrder = $sortPrefs['sortOrder'];
					break;
			}
		} else {
			$sortBy = 'Name';
			$sortOrder = 'Asc';
		}

		return ['sortBy'=>$sortBy, 'sortOrder'=>$sortOrder];
	}

	function sortRows($data, $sortByMap) {
		if (!empty($data)) {
			$primarySortOrder = ($this->properties['sortBy'] > 0) ? SORT_DESC : SORT_ASC;

			foreach ($data as $key => $row) {
				$primary[$key]  = $row[$sortByMap[abs($this->properties['sortBy']) ] ];
				if ($sortByMap[abs($this->properties['sortBy']) ] !== 'class') {
					$secondary[$key] = $row['ID'];
					$secondarySortOrder = SORT_DESC;
				} else {
					$secondary[$key] = $row['depositionOf'];
					$secondarySortOrder = SORT_ASC;
				}
			}

			array_multisort($primary, $primarySortOrder, SORT_NATURAL | SORT_FLAG_CASE, $secondary, $secondarySortOrder, SORT_NATURAL | SORT_FLAG_CASE, $data);
		}

		return $data;
	}

	function modifyFields(&$data) {
		$statusMap = ['I'=>'Active', 'F'=>'Completed', 'N'=>'Scheduled'];
		$classMap = ['Deposition'=>'Deposition', 'Demo'=>'Demo Deposition', 'WitnessPrep'=>'Witness Prep', 'WPDemo'=>'Demo Witness Prep', 'Trial'=>'Trial', 'Demo Trial'=>'Demo Trial',
			'Trial Binder'=>'Trial Binder', 'Arbitration'=>'Arbitration', 'Mediation'=>'Mediation', 'Hearing'=>'Hearing'];

		foreach ($data as $key => &$row) {
			$row['status'] = $statusMap[$row['statusID']];
			$row['class'] = $classMap[$row['class']];
		}
	}
}
