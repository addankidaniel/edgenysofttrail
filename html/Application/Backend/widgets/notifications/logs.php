<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetNotificationLogs extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'notifications/logs.html');
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraClientNotifications->getWidgetLogData';
        $this->properties['searchValue'] = null;
        $this->properties['sortBy'] = 1;
        $this->properties['pageSize'] = 10;
    }

    public function init() {
       parent::init();
        $this->tpl->clientID = $this->reg->fv['clientID'];
    }

    public function sort($order) {
        $this->properties['sortBy'] = -$order;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

    protected function getData()
	{
        $html = parent::getData();
        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        $this->properties['excelTemplate'] = 'notificationlogs.xls';

        $this->tpl->isAllSelected = $this->isAllSelected();

        $this->tpl->links = [
			$this->sortLink( 1, 'Sent On' ),
            $this->sortLink( 2, 'Sent To' ),
            $this->sortLink( 3, 'Name' ),
            $this->sortLink( 4, 'Message Subject' ),
        ];

        $this->page->tpl->count = $this->properties['count'];

        return $html;
    }

    public function fillSpreadSheet(\PHPExcel_Worksheet $sheet) {
        $rowI = 1;
        $colI = 1;
        $printHeader = false;

        if (!$printHeader) {
            $printHeader = true;
            $sheet->getCellByColumnAndRow(1, $rowI)->setValue('Sent On');
            $sheet->getCellByColumnAndRow(2, $rowI)->setValue('Sent To');
            $sheet->getCellByColumnAndRow(3, $rowI)->setValue('Name');
            $sheet->getCellByColumnAndRow(4, $rowI)->setValue('Message Subject');

            $row++;
        }
        $rows = $this->getRowsNoPagination();
        $arr = array();

        foreach ($rows as $row) {
            $row['sentOn'] = date_format( date_create( $row['sentOn'] ), 'M. j Y g:i:s a' );
            $arr[] = array(
                $row['sentOn'],
                $row['email'],
                $row['firstName'].' '.$row['lastName'],
                $row['subject']
            );
        }
        $sheet->fromArray($arr, null, 'B2');
    }
}