<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class WidgetNotifications extends WidgetBackend {

    public function __construct($id, $template = null) {
        parent::__construct($id, ($template) ? : 'notifications/notifications.html');
        $this->properties['idsFieldName'] = 'userID';
        $this->properties['source'] = 'ClickBlocks\DB\OrchestraUsers->getWidgetNotificationData';
        $this->properties['searchValue'] = null;
		$this->properties['sortBy'] = isset( $this->page->pageSession['sortBy'] ) ? $this->page->pageSession['sortBy'] : 1;
        $this->properties['pageSize'] = isset( $this->page->pageSession['pageSize'] ) ? $this->page->pageSession['pageSize'] : 30;
		$this->properties['pos'] = isset( $this->page->pageSession['pos'] ) ? $this->page->pageSession['pos'] : 0;
        $this->properties['typeID'] = \ClickBlocks\MVC\Edepo::USER_TYPE_RESELLER;
    }

    public function refresh() {
        $this->init();
        $this->update();
    }

	public function exportEmailList($opts){
		$this->properties['options'] = $opts;
		$this->exportToExcel();
	}

	public function sendEmail( $opts )
	{
		$recurseType = null;

		if (isset( $opts['includeCA'] ) && $opts['includeCA']) {
			$recurseType = 'C'; // Include Client Admins
		} else if (isset( $opts['includeAU'] ) && $opts['includeAU']) {
			$recurseType = 'A'; // Include All Users
		} else {
			$recurseType = 'R'; // Resellers only
		}

		$resellerIDs = $this->getSelectedIDs();

		$newMessageID = $this->insertClientMessage( $opts['subject'], $opts['message'] );

		foreach ($resellerIDs as $rID)
		{
			$rows = (new DB\OrchestraClientNotifications())->getUserInfoByResellerID( $rID, $recurseType);

			foreach($rows as $row)
			{
				$row['name'] = $row['firstName'] . ' ' . $row['lastName'];

				$this->insertClientNotification( $row, $newMessageID );

				if ($row['typeID'] === 'R' || $row['typeID'] === 'C') {
					$primaryContact = ['ID'=>null, 'name'=>$row['contactName'], 'email'=>$row['contactEmail']];
					$this->insertClientNotification( $primaryContact, $newMessageID );
				}
			}
		}
	}

	protected function insertClientMessage( $subject, $message )
	{
		$newMessage = (new DB\ServiceClientMessages())->getByID();
		$newMessage->subject = $subject;
		$newMessage->messageBody = $message;
		$newMessage->insert();
		$newID = $newMessage->getLastInsertId();
		unset( $newMessage );

		return $newID;
	}

	protected function insertClientNotification( $user, $clientMessageID )
	{
		$newNotification = (new DB\ServiceClientNotifications())->getByID();
		$newNotification->userID = $user['ID'];
		$newNotification->name = $user['name'];
		$newNotification->email = $user['email'];
		$newNotification->clientMessageID = $clientMessageID;
		$newNotification->insert();
		$newID = $newNotification->getLastInsertId();
		unset( $newNotification );

		return $newID;
	}

   protected function getSelectedIDs()
   {
      $ids = array();
      if (count($this->properties['ids']) > 0) {
         $ids = $this->properties['ids'];
      } else {
         $rows = $this->getRowsNoPagination();
         foreach ($rows as $row) $ids[$row['userID']] = 1;
      }
      return $ids;
   }

    protected function getData() {
        $html = parent::getData();
        $this->tpl->rows = $this->properties['rows'];
        $this->tpl->sortBy = $this->properties['sortBy'];
        $this->tpl->url = mb_strtolower( $this->page->reg->reseller['URL'] );
        $this->properties['excelTemplate'] = 'notificationemaillist.xls';

        $this->tpl->isAllSelected = $this->isAllSelected();
        $this->tpl->emails = $this->properties['ids'];

        $this->tpl->links = array(
			$this->sortLink(1, 'ID'),
            $this->sortLink(2, 'Reseller Name'),
            $this->sortLink(3, 'Reseller Since'),
            $this->sortLink(4, 'City'),
            $this->sortLink(5, 'State'),
            $this->sortLink(6, 'Primary Contact'),
        );

        $this->page->tpl->count = $this->properties['count'];

        return $html;
    }

    public function fillSpreadSheet(\PHPExcel_Worksheet $sheet) {
        $rowI = 1;
        $arr = array();

        $sheet->getCellByColumnAndRow(1, $rowI)->setValue('Reseller');
        $sheet->getCellByColumnAndRow(2, $rowI)->setValue('Client');
        $sheet->getCellByColumnAndRow(3, $rowI)->setValue('Name');
        $sheet->getCellByColumnAndRow(4, $rowI)->setValue('Username');
        $sheet->getCellByColumnAndRow(5, $rowI)->setValue('Email Address');
        $sheet->getCellByColumnAndRow(6, $rowI)->setValue('User Type');

        $recurseType = null;

        if ($this->properties['options']['includeCA']) {
            $recurseType = 'C';
        } else if ($this->properties['options']['includeAU']) {
            $recurseType = 'A';
        } else {
            $recurseType = 'R';
        }

        $resellerIDs = $this->getSelectedIDs();

        foreach ($resellerIDs as $rID) {

            $rows = (new DB\OrchestraClientNotifications())->getUserInfoByResellerID( $rID, $recurseType);

            foreach ($rows as $row) {
                if ($row['typeID'] === 'R') {
                    $resellerName = $row['name'];
                    break;
                }
            }

            foreach ($rows as $row) {
                switch ($row['typeID']) {
                    case 'CU':
                        $row['typeID'] = 'User';

						$arr[] = array(
                            $resellerName,
                            $row['name'],
                            $row['firstName'].' '.$row['lastName'],
                            $row['username'],
                            $row['email'],
                            $row['typeID']
                        );
                        break;
                    case 'C':
                        $row['typeID'] = 'Client Admin';

						$arr[] = array(
                            $resellerName,
                            $row['name'],
                            $row['name'],
                            $row['username'],
                            $row['email'],
                            $row['typeID']
                        );
						// Add row to spreadsheet for client's primary contact
						$arr[] = array(
                            $resellerName,
                            $row['name'],
                            $row['contactName'],
                            '',
                            $row['contactEmail'],
                            'Primary Contact'
						);
                        break;
                    case 'R':
                        $row['typeID'] = 'Reseller';

						$arr[] = array(
                            $row['name'],
                            '',
                            $row['name'],
                            $row['username'],
                            $row['email'],
                            $row['typeID']
                        );
						// Add row to spreadsheet for reseller's primary contact
						$arr[] = array(
                            $row['name'],
                            '',
                            $row['contactName'],
                            '',
                            $row['contactEmail'],
                            'Primary Contact'
						);
                        break;
                }
            }
            $sheet->fromArray($arr, null, 'B2');
        }
    }
}