<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\API\IEdepoBase;

class WidgetUsers extends WidgetUserList {

	public function __construct( $id, $template=NULL ) {
		parent::__construct( $id, ( $template ) ? : 'users/list.html' );
		$this->properties['typeID'] = IEdepoBase::USER_TYPE_USER;
	}

	protected function getData() {
		$html = parent::getData();
		$reg = Core\Register::getInstance();
		$this->tpl->links = [$this->sortLink( 2, 'First Name' ), $this->sortLink( 3, 'Last Name' )];
		$this->tpl->elink = '/' . $reg->uri->path[0] . '/users/edit?ID=';
		return $html;
	}
}
