<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageExhibitsDownload extends Backend
{
	public function __construct() {
		parent::__construct( 'exhibitsdownload/exhibitsdownload.html' );
	}

	public function access() {
		if ( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ( $this->user->typeID === self::USER_TYPE_ADMIN );
	}

	public function init() {
        parent::init();
        $this->head->name = 'Get Exhibits';
		$this->tpl->headName = 'Get Exhibits';

		$this->clearPanel();
	}

	public function getDownloadExhibitsDetails() {
		$data = $this->getData();

		if ( $data['canDownload'] === FALSE ) {
			$this->clearPanel();
			$this->showErrorMessage( $data['error']['header'], $data['error']['message'] );
			return;
		}

		$totalSizeBytes = 0;
		foreach ($data['folder']->files as $file) {
            if( !$file->ID ) {
				continue;
			}
			if( !$file->filesize ) {
				$file->setMetadata();
			}
			$totalSizeBytes += $file->filesize;
		}
		$totalSize = Utils::readableFilesize( $totalSizeBytes, 1 );

		$sessionInfoPanel = $this->get('sessionInfoPanel');
		$sessionInfoPanel->tpl->reseller = $data['reseller'];
		$sessionInfoPanel->tpl->client = $data['client'];
		$sessionInfoPanel->tpl->session = $data['session'];
		$sessionInfoPanel->tpl->folder = $data['folder'];
		$sessionInfoPanel->tpl->exhibitCount = count($data['folder']->files);
		$sessionInfoPanel->tpl->exhibitsSize = $totalSize;
		$sessionInfoPanel->tpl->canDownload = TRUE;
		$sessionInfoPanel->update();
	}

	public function downloadExhibits() {
		$sessionInfoPanel = $this->get('sessionInfoPanel');
		$session = $sessionInfoPanel->tpl->session;
		$folder = $sessionInfoPanel->tpl->folder;

		if ( $sessionInfoPanel->tpl->exhibitCount < 1 ) {
			$this->showErrorMessage( 'No Official Exhibits', 'There are no exhibits to download' );
			return;
		}

		$fullName = Core\IO::dir('temp').'/'.$session->depositionOf.'_'.time().'.zip';
		$zip = new \ZipArchive();
		$zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

		foreach ($folder->files as $file) {
			if (!$file->ID) continue;
			$zip->addFile( $folder->getFullPath().DIRECTORY_SEPARATOR.$file->name, $file->name );
		}
		$zip->close();
		$this->ajax->downloadFile($fullName, $session->depositionOf.'.zip');
	}

	public function getData() {
		$valid = TRUE;
		$message = '';
		$sessionID = $this->get( 'sessionID' )->value;

		if ( $sessionID === NULL ) {
			$valid = FALSE;
			$message = '<div style="text-align:center;">No session ID entered<br/>Please enter a valid parent session ID to see details and download exhibits.</div>';
		}
		if ( $valid ) {
			$session = new DB\Depositions( $sessionID );
			if ( !$session || !$session->ID || $session->ID !== $sessionID ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Session could not be found or does not exist.<br/>Please try another ID.</div>';
			} else if ( $session->deleted ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Session has been deleted.<br/>The exhibits are not available.</div>';
			} else if ( $session->parentID ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Session is a child session.<br/>Please enter a valid parent session ID.</div>';
			}
		}
		if ( $valid ) {
			$case = new DB\Cases( $session->caseID );
			if ( !$case || !$case->ID || $case->ID !== $session->caseID ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Something went wrong.<br/>The case corresponding to this session could not be found or does not exist.</div>';
			} else if ( $case->deleted ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">The case corresponding to this session has been deleted.<br/>The exhibits are not available.</div>';
			}
		}
		if ( $valid ) {
			$client = new DB\Clients( $case->clientID );
			if ( !$client || !$client->ID || $client->ID !== $case->clientID ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Something went wrong.<br/>The client corresponding to this session could not be found or does not exist.</div>';
			} else if ( $client->deleted ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">The client corresponding to this session has been deleted.<br/>The exhibits are not available.</div>';
			} else if ( $client->deactivated ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">The client corresponding to this session has been deactivated.<br/>The exhibits are not available.</div>';
			}
		}
		if ( $valid ) {
			$reseller = new DB\Clients( $client->resellerID );
			if ( !$reseller || !$reseller->ID || $reseller->ID !== $client->resellerID ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Something went wrong.<br/>The reseller corresponding to this session could not be found or does not exist.</div>';
			} else if ( $reseller->deleted ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">The reseller corresponding to this session has been deleted.<br/>The exhibits are not available.</div>';
			} else if ( $reseller->deactivated ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">The reseller corresponding to this session has been deactivated.<br/>The exhibits are not available.</div>';
			}
		}
		if ( $valid ) {
			$folder = (new DB\OrchestraFolders())->getExhibitFolderObject( $session->ID );
			if( !$folder || !$folder->ID ) {
				$valid = FALSE;
				$message = '<div style="text-align:center;">Something went wrong.<br/>The Official Exhibits Folder could not be found or does not exist.</div>';
			}
		}

		if ( $valid ) {
			$data['reseller'] = $reseller;
			$data['client'] = $client;
			$data['session'] = $session;
			$data['folder'] = $folder;
		} else {
			$data['error']['header'] = 'Invalid Session ID';
			$data['error']['message'] = $message;
		}
		$data['canDownload'] = $valid;

		return $data;
	}

	public function clearPanel() {
		$sessionInfoPanel = $this->get('sessionInfoPanel');
		$sessionInfoPanel->tpl->reseller = '';
		$sessionInfoPanel->tpl->client = '';
		$sessionInfoPanel->tpl->session = '';
		$sessionInfoPanel->tpl->folder = '';
		$sessionInfoPanel->tpl->exhibitCount = '';
		$sessionInfoPanel->tpl->exhibitsSize = '';
		$sessionInfoPanel->tpl->canDownload = FALSE;
		$sessionInfoPanel->update();
	}

	public function showErrorMessage( $header, $message ) {
		$dlg = $this->get( 'noticePopup' );
		$dlg->tpl->header = $header;
		$dlg->tpl->message = $message;
		$dlg->get( 'btnCancel' )->value = 'OK';
		$dlg->show();
	}
}
