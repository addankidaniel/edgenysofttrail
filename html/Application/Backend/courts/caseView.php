<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Utils;

class PageCourtCaseView extends Backend {

	protected $isClientAdmin = FALSE;
	protected $isCaseMgr = FALSE;
	protected $isDepoOwner = FALSE;
	protected $isDepoAssist = FALSE;
	protected $isDepoAttendee = FALSE;


	public function __construct() {
        parent::__construct( 'courts/caseView.html' );
        $ID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
        $this->case = new DB\Cases( $ID );
    }

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$orcCases = $this->getOrchestraCases();
		$this->noAccessURL = $this->basePath . '/cases';
		$this->isClientAdmin = ($this->user->clientID == $this->case->clientID && $this->user->clients[0]->typeID === Edepo::CLIENT_TYPE_COURTCLIENT && ($this->user->typeID === Edepo::USER_TYPE_CLIENT || $this->user->clientAdmin));
		$this->isCaseMgr = $this->getOrchestraCaseManagers()->checkCaseManager( $this->case->ID, $this->user->ID );
		$this->isDepoOwner = $orcCases->checkSessionOwner( $this->case->ID, $this->user->ID );
		$this->isDepoAssist = $orcCases->checkDepoAssist( $this->case->ID, $this->user->ID );
		$this->isDepoAttendee = $orcCases->checkDepoAttendee( $this->case->ID, $this->user->ID );
		return ($this->isClientAdmin || $this->isCaseMgr || $this->isDepoOwner || $this->isDepoAssist || $this->isDepoAttendee);
	}

    public function init()
	{
        parent::init();
		$this->head->name = 'Case View';
        $this->tpl->url = $this->config->courtsPath;
        $this->tpl->caseID = $this->case->ID;

        $this->tpl->scrumName = $this->case->name;
		$this->tpl->caseNumber = '#' . $this->case->number;
        $this->tpl->headerName = $this->case->name;

        $this->tpl->isAdmin = $this->isClientAdmin;
		$this->tpl->isCaseMgr = $this->isCaseMgr;
		$this->tpl->isCourtReporter = ($this->user->typeID == self::USER_TYPE_REPORTER);

        $this->tpl->progressKeys = array();
        $this->tpl->filesAI = 1;

		//case menubutton
		$caseMenuOptions = [];
		if( $this->isClientAdmin || $this->isCaseMgr ) {
			$caseMenuOptions['Edit Case'] = ['href' => "{$this->tpl->url}/case/edit?ID={$this->case->ID}&view=true"];
		}
		if( $this->user->typeID == Edepo::USER_TYPE_REPORTER ) {
			$caseMenuOptions['Download Case'] = ['onclick'=>'ajax.doit(\'->downloadCase\');'];
			$caseMenuOptions['Exhibit Log'] = ['onclick'=>'ajax.doit(\'->caseExhibitLog\');'];
		}
		$this->get('case_btn_menu')->options = $caseMenuOptions;

		//add session menubutton
		$addMenuOptions = [];
		if( $this->isClientAdmin || $this->isCaseMgr ) {
			$addMenuOptions['Trial Binder'] = ['href' => "{$this->tpl->url}/session/trialbinder?caseID={$this->case->ID}"];
			$this->get('add_btn_menu')->options = $addMenuOptions;
		}
	}
	function downloadCase() {
		$depositions = (new DB\OrchestraDepositions())->getWidgetCourtDepositionData( $this->case->ID, ['userID'=>$this->user->ID, 'caseID'=>$this->case->ID] );

		if( !$depositions ) {
			$this->showMessage( 'There are no sessions to download!', 'Download Case' );
			return;
		}

        $fullName = Core\IO::dir('temp').'/'.$this->case->name.'_'.time().'.zip';
        $zip = new \ZipArchive();
        $ret = $zip->open( $fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );

		if( $ret !== TRUE ) {
			\ClickBlocks\Debug::ErrorLog( "PageCourtCaseView::downloadCase; Failure to open archive: {$fullName}, return value: {$ret}" );
			$this->showMessage( 'Unable to download case', 'Error!' );
			return;
		}

        foreach( $depositions as $row ) {
            $this->addDepositionToArchive( $zip, $row['ID'], true );
        }

		$zip->close();

        if( !file_exists( $fullName ) ) {
			\ClickBlocks\Debug::ErrorLog( "PageCourtCaseView::downloadCase; Failure with archive: {$fullName}" );
			$this->showMessage( 'Unable to download case', 'Error!' );
			return;
		}
		$this->ajax->downloadFile( $fullName, $this->case->name.'.zip' );
    }

	private function addDepositionToArchive( \ZipArchive &$archive, $depositionID, $caseZip=false )
	{
		$deposition = (new DB\ServiceDepositions())->getByID( $depositionID );
		$folders = (new DB\OrchestraFolders())->getFoldersByDepo( $deposition->ID );
		foreach ($folders as $folder)
		{
			if ($folder['class'] == DB\Folders::CLASS_COURTESYCOPY)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_WITNESSANNOTATIONS && $this->user->ID != $deposition->ownerID)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_PERSONAL && $this->user->ID != $folder['createdBy'])
			{
				continue;
			}
			$files = (new DB\OrchestraFiles())->getFilesByFolder( $folder['ID'] );
			foreach ($files as $file)
			{
				$f = (new DB\ServiceFiles())->getByID( $file['ID'] );
				$user = (new DB\ServiceUsers())->getByID( $f->createdBy );
				$client = (new DB\ServiceClients())->getByID( $this->case->clientID );
				if( $user->clientID !== $this->case->clientID && ( $user->clientID !== $client->resellerID && $f->createdBy !== $user->ID && $deposition->isDemo() ) )
				{
					continue;
				}
				$folderPath = $user->email.'/'.$folder['name'].'/'.$f->name;
				if ($caseZip)
				{
					// remove any symbols that cause conflict with folder/file naming conventions
					$depositionName = preg_replace( '/[<_>:"\/\\\|\?\*]/', '', $deposition->depositionOf );
					$folderPath = ($depositionName.'_'.$deposition->volume.'_'.$deposition->ID).'/'.$folderPath;
				}
				$archive->addFile( $f->getFullName(), $folderPath );
			}
		}
	}

	function viewFile( $fileID )
	{
		$fileID = (int)$fileID;
		$file = new \ClickBlocks\DB\Files( $fileID );
        if( !$file || !$file->ID || $file->ID != $fileID ) {
            return;
        }
		$this->checkUserAccess( NULL, $file->folderID );

		if( !$file->mimeType ) {
			$file->setMetadata();
		}
		$mimeType = $file->mimeType;
		if( mb_stristr( $mimeType, 'audio' ) ) {
			$dlg = $this->get( 'viewAudioPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile=$fileID";
		} elseif( mb_stristr( $mimeType, 'video' ) ) {
			$dlg = $this->get( 'viewVideoPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile=$fileID";
		} else {
			$dlg = $this->get( 'viewFilePopup' );
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/fileViewer?ID={$this->fv['ID']}&viewFile=$fileID";
		}
		$dlg->tpl->title = $file->name;
		$dlg->show();
    }

	private function checkUserAccess( $depositionID=NULL, $folderID=NULL )
	{
		$depositionID = (int)$depositionID;
		$folderID = (int)$folderID;
		$isAllowed = FALSE;
		if ( $depositionID ) {
			$isAllowed = $this->getOrchestraDepositions()->checkUserAccessToDeposition( $this->user->ID, $depositionID );
			if (!$isAllowed) {
				throw new \LogicException( "Unable to find Deposition ID ({$depositionID})" );
			}
		} else {
			$folder = new \ClickBlocks\DB\Folders( $folderID );
			if( !$this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $folder->depositionID ) ) {
				if( $folder->class == self::FOLDERS_EXHIBIT ) {
					$folderCreator = new \ClickBlocks\DB\Users( $folder->createdBy );
					if( $this->case->clientID !== $folderCreator->clientID ) {
						throw new \LogicException( "Unable to find Exhibit Folder ID ({$folderID})" );
					}
				} elseif( $folder->createdBy != $this->user->ID ) {
					throw new \LogicException( "Unable to find Folder ID ({$folderID})" );
				}
			}
		}
	}

	public function showAddPlaceholder()
	{
		$dlg = $this->get( 'addPlaceholderPopup' );
		$dlg->get( 'filename' )->value = '';
		$dlg->get( 'description' )->text = '';
		$dlg->tpl->filenameError = FALSE;
		$dlg->tpl->folderListError = FALSE;
		$dlg->get( 'btnOK' )->onclick = 'addPlaceholder()';
		$lastFolderID = (isset( $this->subSession['LAST_FOLDER_ID'] )) ? $this->subSession['LAST_FOLDER_ID'] : FALSE;
		$folders = $this->getOrchestraFolders()->getFoldersByUserID( $this->subSession['SELECTED_DEPOSITION_ID'], $this->user->ID );
		$options = [];
		foreach( $folders as $folder ) {
			if( $folder['class'] == 'Personal' || $folder['class'] == 'Folder' || $folder['class'] == 'Trusted') {
				$options[$folder['ID']] = $folder['name'];
			}
		}
		asort( $options );
		$folderList = $dlg->get( 'phFolderList' );
		$folderList->options = $options;
		$folderList->value = (isset( $options[$lastFolderID] )) ? $options[$lastFolderID] : reset( $options );
		$dlg->get('btnOK')->visible = (count( $options ) > 0);
		$dlg->show();
	}

	public function addPlaceholder( $fv )
	{
		$dlg = $this->get( 'addPlaceholderPopup' );
		$dlg->get( 'phFolderList' )->value = $fv['phFolderList'];
		$hasErrors = FALSE;
		if( !preg_match( '/^[a-zA-Z0-9_\s-]+$/', $fv['filename'] ) ) {
			$dlg->tpl->filenameError = 'ERROR: Invalid file name';
			$hasErrors = TRUE;
		} else {
			$dlg->tpl->filenameError = FALSE;
		}
		if( !preg_match( '/^[a-zA-Z0-9_\s-]+$/', $fv['phFolderList'] ) ) {
			$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
			$hasErrors = TRUE;
		} else {
			$dlg->tpl->folderListError = FALSE;
		}
		foreach( $this->config->logic as $key => $reservedFolder ) {
			if( $key === 'personalFolderName' || $key === 'potentialExhibitsFolderName') continue;
			if( strcasecmp( $fv['phFolderList'], $reservedFolder ) === 0 ) {
				$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
				$hasErrors = TRUE;
			}
		}
		if( $hasErrors ) {
			$dlg->update();
			$dlg->show();
			return;
		}
		$now = date( 'Y-m-d H:i:s' );
		$depositionID = $this->subSession['SELECTED_DEPOSITION_ID'];
		$folders = $this->getOrchestraFolders()->getFoldersByName( $depositionID, $fv['phFolderList'] );
		if( $folders ) {
			$folder = new \ClickBlocks\DB\Folders( $folders[0]['ID'] );
			$dlg->get( 'phFolderList' )->value = $folder->name;
		} else {
			$folder = new \ClickBlocks\DB\Folders();
			$folder->class = 'Folder';
			$folder->name = $fv['phFolderList'];
			$deposition = $this->getService( 'Depositions' )->getByID( $depositionID );
			$trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap( $deposition->ID );
            $folder->created = $now;
			$folder->lastModified = $now;
            $folder->createdBy = ( isset( $trustedUsers[$this->user->ID] ) ) ? $deposition->createdBy : $this->user->ID;
            $folder->depositionID = $deposition->ID;
			$folder->insert();
		}
		$this->subSession['LAST_FOLDER_ID'] = $folder->ID;
		$dlg->update();
		$dlg->show();

		$tmpHTML = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.html';
		$tmpPDF = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.pdf';
		$htmlString = '<html><body>
			<div style="font-family:sans-serif;text-align:center;font-size:36px;">
				<span>Placeholder for Exhibit</span>
			</div>
			<div style="margin:50px 20px 20px 20px;border:1px solid #000;">
				<p style="padding:10px;margin:0;">' . $fv['description'] . '</p>
			</div>
			</body></html>';
		$fh = fopen( $tmpHTML, 'w+' );
		if( flock( $fh, LOCK_EX ) ) {
			fwrite( $fh, $htmlString );
			fflush( $fh );
			flock( $fh, LOCK_UN );
		}
		fclose( $fh );
		$pdf = new Utils\WKHTMLToPDF( $tmpHTML, $tmpPDF );
		$pdf->setPageSize( 'Letter' );
		$pdf->setOrientation( 'Portrait' );
		$pdf->addParam( '--no-background', TRUE );
		$okToGo = $pdf->render();
		unlink( $tmpHTML );
		if( !$okToGo ) {
			unlink( $tmpPDF );
			$dlg->tpl->filenameError = 'ERROR: An error occurred while creating the PDF';
			$dlg->update();
			$dlg->show();
			return;
		}

		// Is this a demo? Set watermark
		if( (new \ClickBlocks\DB\Depositions($depositionID))->class == 'Demo' ) {
			$wmPDF = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.pdf';
			$result = \ClickBlocks\PDFTools::watermark( $tmpPDF, $wmPDF );
			if( $result === true ) {
				copy( $wmPDF, $tmpPDF );
				unlink( $wmPDF );
			}
		} else {
			//clean wkhtmltopdf generated PDFs
			$pdfConverter = new EDPDFConvert();
			if( !$pdfConverter->pdfCleanup( $tmpPDF ) ) {
				$dlg->tpl->filenameError = 'ERROR: An error occurred while cleaning up the PDF';
				$dlg->update();
				$dlg->show();
				return;
			}
			unset( $pdfConverter );
		}

		$file = new DB\Files();
		$file->name = $fv['filename'] . '.pdf';
		$file->folderID = $folder->ID;
		$file->createdBy = $folder->createdBy;
		$file->created = $now;
		$file->setSourceFile( $tmpPDF );
		$file->insert();
		if( $file->ID ) {
			$dlg->hide();
			$this->ajax->reload();
		}
		$folder->lastModified = $now;
    	$this->notifyNodeJSUsers( $folder->ID );
	}

	public function depoExhibitLog()
	{
		$deposition = new \ClickBlocks\DB\Depositions( $this->subSession['SELECTED_DEPOSITION_ID'] );
		$rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $deposition->ID, TRUE );
		$dlg = $this->get( 'exhibitLogPopup' );
		$dlg->tpl->type = 'depo';
		$dlg->tpl->rows = $rows;
		$dlg->tpl->headingName = "{$deposition->depositionOf} (vol: {$deposition->volume})";
		$dlg->get( 'btnExport' )->visible = (count( $rows ));
		$dlg->get( 'btnExportWithExhibits' )->visible = (count( $rows ));
		$dlg->update();
		$dlg->show();
	}

	public function caseExhibitLog()
	{
		$rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForCase( $this->case->ID );
		$dlg = $this->get( 'exhibitLogPopup' );
		$dlg->tpl->type = 'case';
		$dlg->tpl->rows = $rows;
		$dlg->tpl->headingName = $this->case->name;
		$dlg->get( 'btnExport' )->visible = (count( $rows ));
		$dlg->get( 'btnExportWithExhibits' )->visible = (count( $rows ));
		$dlg->update();
		$dlg->show();
	}

	public function exportExhibitLog()
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.csv';
		$fh = fopen( $outPath, 'w' );
		if( $dlg->tpl->type == 'case' ) {
			fputcsv( $fh, ['Time','Deposition Of', 'Volume','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['depositionOf'], $row['volume'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		} else {
			fputcsv( $fh, ['Time','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		}
		fclose( $fh );

        if( file_exists( $outPath ) ) {
			$uid = ($dlg->tpl->type == 'case') ? "case_{$rows[0]['caseID']}" : "deposition_{$rows[0]['depositionID']}";
			$this->ajax->downloadFile( $outPath, "{$uid}_exhibit_logs.csv", 'application/octet-stream', FALSE );
		} else {
			$this->showMessage( 'Error generating export', 'Export Exhibit Log' );
		}
	}

	public function exportExhibitLogWithExhibits()
	{
		$dlg = $this->get( 'exhibitLogPopup' );

		$folderIDs = [];
		if ( $dlg->tpl->type == 'case' ) {
			$rows = $dlg->tpl->rows;
			$depoIDs = [];
			foreach ( $rows as $row ) {
				$depoIDs[$row['depositionID']] = $row['depositionID'];
			}
			foreach ( $depoIDs as $depoID ) {
				$deposition = new \ClickBlocks\DB\Depositions( $depoID );
				if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
					continue;
				}
				$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
				$folderIDs[] = $officialExhibitsFolder['ID'];
			}
			$exhibitLogXLSPath = $this->createExhibitLogXLS(true);
			$downloadFolderName = $this->case->name . " Log and Exhibits";
		} else {
			$depoID = $this->tpl->depositionID;
			$deposition = new \ClickBlocks\DB\Depositions( $depoID );
			if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
				return;
			}
			$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
			$folderIDs[] = $officialExhibitsFolder['ID'];
			$downloadFolderName = $deposition->depositionOf . " ID-" . $deposition->ID . " vol-" . $deposition->volume;
			$exhibitLogXLSPath = $this->createExhibitLogXLS(false);
		}
		if (!$folderIDs) {
			return;
		}

		$this->downloadExhibitLogFolder($folderIDs, $downloadFolderName, $exhibitLogXLSPath);
	}

	public function createExhibitLogXLS($isCaseLog)
	{
		if ($isCaseLog) {
			$template = '/_templates/caseexhibitlog.xls';
		} else {
			$template = '/_templates/exhibitlog.xls';
		}
		$xls = \PHPExcel_IOFactory::load(Core\IO::dir('backend') . $template);
        $sheet = $xls->setActiveSheetIndex(0);
        $sheet->setTitle('Exhibit Log');
		$this->fillExhibitLogSpreadSheet( $sheet, $isCaseLog );
        $writer = new \PHPExcel_Writer_Excel5($xls);
        $fileName = Core\IO::dir('temp').'/'.\ClickBlocks\Utils::createUUID().'.xls';
        $writer->save($fileName);
        return $fileName;
	}

	public function fillExhibitLogSpreadSheet(\PHPExcel_Worksheet $sheet, $isCaseLog)
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;

		if ($isCaseLog) {
			$colValueIndexes = ['lIntroducedDate', 'depositionOf', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		} else {
			$colValueIndexes = ['lIntroducedDate', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		}

		// xls template has column titles in first row, start filling spreadsheet on second row
		$rowI = 2;

		foreach ($rows as $row)
		{
			foreach ($colValueIndexes as $i => $valueIndex)
			{
				if (!isset( $row[$valueIndex] ))
				{
					continue;
				}
				$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $row[$valueIndex] );
				if ($valueIndex == 'exhibitFilename')
				{
					$sheet->getCellByColumnAndRow( $i, $rowI )->getHyperlink($row[$valueIndex])->setUrl($row[$valueIndex]);
				}
			}
			$rowI++;
		}
	}

	private function downloadExhibitLogFolder( $folderIDs, $downloadFolderName, $exhibitLogFilePath ) {
		if (!$folderIDs) {
			return;
		}

		$archiveName = Core\IO::dir( 'temp' ) . DIRECTORY_SEPARATOR . $this->case->name . '_' . time() . '.zip';
		$zip = new \ZipArchive();
		$zip->open( $archiveName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );

		$zip->addFile( $exhibitLogFilePath, 'Exhibit Log.xls' );

		foreach ($folderIDs as $folderID) {
			$folder = new \ClickBlocks\DB\Folders( $folderID );

			if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
				$this->showMessage('Folder not found!', 'Export Log With Exhibits');
				return;
			}
			$this->checkUserAccess( NULL, $folder->ID );

			$files = (new DB\OrchestraFiles())->getFilesByUserID( $folder->ID, $this->user->ID );
			if (count( $files ) < 1) {
				$this->showMessage( 'Folder is empty!', 'Export Log With Exhibits' );
				return;
			}
			foreach ($files as $fileRow) {
				if ( !$fileRow['ID'] ) {
					continue;
				}
				$zip->addFile( $folder->getFullPath().DIRECTORY_SEPARATOR.$fileRow['name'], $fileRow['name'] );
			}

		}
		$zip->close();

		$this->ajax->downloadFile( $archiveName, $downloadFolderName.'.zip' );
	}
}
