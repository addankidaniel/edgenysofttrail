<?php

namespace ClickBlocks\MVC\Backend;

class PageCourtCases extends Backend {

	protected $isClientAdmin = FALSE;

	public function __construct() {
		parent::__construct( 'courts/cases.html' );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/login';
		$this->isClientAdmin = ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
		return ($this->isClientAdmin || $this->user->clients[0]->typeID == self::CLIENT_TYPE_COURTCLIENT);
	}

	public function init() {
		parent::init();
		$this->tpl->tab = 0;
		$this->head->name = 'Case Management';
		$this->tpl->url = mb_strtolower( $this->config->courtsPath );
		$this->tpl->isClientAdmin = $this->isClientAdmin;

		$this->get('cases')->clientID = $this->user->clientID;
		$this->get('cases')->userID = $this->user->ID;
	}

}
