<?php

namespace ClickBlocks\MVC\Backend;

class PageCourtUsers extends Backend {

	public function __construct() {
	   parent::__construct( 'courts/users.html' );
	}

	 public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
	}

	public function init() {
		parent::init();
		$this->tpl->tab = 2;
		$this->head->name = 'User Management';
		$this->get( 'users' )->clientID = $this->user->clientID;
		$this->tpl->editPath = $this->config->courtsPath . '/users/edit';
	}
}
