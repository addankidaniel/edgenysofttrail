<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web\UI\Helpers;

class PageCourtUserEdit extends Backend {

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $userC = NULL;

	protected $clientTypeID = NULL;

	public function __construct() {
		parent::__construct( 'courts/userEdit.html' );
		$ID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->userC = new DB\Users( $ID );
		if( !$this->userC->ID ) {
			if( $ID > 0 ) {
				$this->userC = NULL;
				$this->redirect( $this->basePath . '/users/add' );
			}
		} else {
			if( $this->user->clientID !== $this->userC->clientID || $this->user->ID === $this->userC->ID ) {
				$this->userC = NULL;
				$this->redirect( $this->basePath . '/users' );
			}
		}
		$this->clientTypeID = $this->user->clients[0]->typeID;
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		if( $this->uri->query['ID'] > 0 ) {
			return (($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin) && $this->user->clientID == $this->userC->clientID);
		} else {
			return ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
		}
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->tpl->showTabs = FALSE;
		$this->tpl->isNew = TRUE;
		$this->tpl->isClientAdmin = FALSE;
		$this->tpl->clientTypeID = $this->clientTypeID;
		$userTypes = Backend::getCourtUserTypes();
		$this->get( 'userType' )->options = $userTypes;
		$this->get( 'userType' )->value = Backend::USER_TYPE_USER;

		if( $this->userC->ID > 0 ) {
			$this->head->name = 'User Profile';
			$this->tpl->htitle = $this->userC->firstName . ' ' . $this->userC->lastName;
			$this->tpl->isNew = FALSE;
			$this->tpl->isClientAdmin = $this->userC->clientAdmin;
			$this->tpl->ID = $this->userC->ID;
			$this->get( 'mainform' )->assign( $this->userC->getValues() );
			$this->get( 'userType' )->value = $this->userC->typeID;
			$this->get( 'clientAdmin' )->value = $this->userC->clientAdmin;
			$ct = $this->get('valFields')->controls;
			$this->get('valFields')->controls = array_diff($ct, array($this->get('password')->uniqueID, $this->get('confirmPassword')->uniqueID));
			$this->tpl->isAccountLocked = ((new DB\ServiceUserAuth())->getByUserID( $this->userC->ID )->hardLock !== null);
		} else {
			$this->head->name = $this->tpl->htitle = 'Add new User';
		}
	}

	public function checkUsername( $ctrls ) {
		return $this->getOrchestraUsers()->checkUniqueUsername( $this->getByUniqueID( $ctrls[0] )->value, $this->userC->ID );
	}

 	public function save( Array $params ) {
  		if( !$this->validators->isValid( 'reg' ) ) {
			return;
		}
		if( $this->tpl->isNew ) {
			$this->userC->clientID = $this->user->clientID;
		}
		$this->userC->firstName = $params['firstName'];
		$this->userC->lastName = $params['lastName'];
		$this->userC->address1 = $params['address1'];
		$this->userC->address2 = $params['address2'];
		$this->userC->city = $params['city'];
		$this->userC->state = $params['state'];
		$this->userC->ZIP = $params['ZIP'];
		$this->userC->countryCode = 'US';
		$this->userC->phone = $params['phone'];
		$this->userC->email = $params['email'];
		$this->userC->username = $params['username'];
		$this->userC->typeID = $params['userType'];
		$this->userC->clientAdmin = $params['clientAdmin'];
		$this->userC->save();
		$password = $this->get('passauth')->value;
        if( $password ) {
			DB\UserAuth::updatePassword( $this->userC->ID, $password, true );
		}
  		$this->ajax->redirect($this->basePath.'/users');
 	}

 	public function deleteUser( $ID ) {
 	  $user = $this->userC;
 	  if ((int)$user->ID == 0) return;
 	  $this->get('confirmPopup')->hide();
 	  $user->delete();
 	  $this->ajax->redirect($this->basePath.'/users');
 	}

 	public function showDelPopup($ID = null) {
 	  $user = (new DB\ServiceUsers())->getByID($ID);
 	  $popup = $this->get('confirmPopup');
 	  $this->cleanByUniqueID($popup->uniqueID);
 	  $popup->get('btnOK')->value = 'Delete';
 	  $popup->get('btnOK')->onclick = 'ajax.doit(\'->deleteUser\', \'' . (int)$user->ID . '\');';
 	  $popup->tpl->title = 'Delete User?';
 	  $popup->tpl->message = 'Are you sure you want to delete <b>' . $user->firstName . ' ' . $user->lastName.'</b>?';
 	  $popup->show();
 	}

	public function validateUsername( $ctrls, $mode )
	{
		$ctUsername = $this->getByUniqueID( $ctrls[0] )->value;
		return preg_match( '/^demo_/i', $ctUsername );
	}

	public function validatePassword( $ctrls )
	{
		$password = $this->getByUniqueID( $ctrls[0] )->value;
		if (strlen( $password ) < 1)
		{
			return true;
		}
		return DB\UserAuth::validatePassword( $password );
	}

	public function unlock()
	{
		DB\UserAuth::unlockAccount( $this->userC->ID );
		$this->ajax->redirect( $this->basePath.'/users' );
	}
}
