<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers;

class PageCaseFileViewer extends Backend
{
	public function __construct()
	{
		parent::__construct('cases/caseFileViewer.html', false);
		$ID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
	}

	public function access()
	{
		return true;
	}

    public function init()
	{
		// Assume all documents will be *.pdf in 1.6 release, no need to check file type (except possibly as sanity check)
		/*
		$contentType = mime_content_type( $file->getFullName() );

		if ($contentType === 'application/pdf') {
			$dlg->tpl->src = '/' . implode('/', $this->uri->path) . 'File' . '?ID='. $this->fv['ID'] . '&amp;viewFile=' . $id;
			$dlg->tpl->fileContents = false;
		}
		else {
			$dlg->tpl->fileContents = file_get_contents($file->getFullName());
			if ($dlg->tpl->fileContents === false) {
				// We had a problem reading the file.
				$dlg->tpl->fileContents = "";
			}
		} */

		$folderID = foo( new DB\OrchestraFolders() )->getFolderIDbyFileID( $this->fv['viewFile'] );
		$hasAccess = foo( new DB\OrchestraFolders() )->checkPermissionToFolder( $this->user->ID, $folderID );

		if (!$hasAccess){
			return;
		}

		$this->head->addMeta( new Helpers\Meta( 'viewport', null, 'width=device-width, initial-scale=1, maximum-scale=1' ) );
		$this->head->addMeta( new Helpers\Meta( 'google', null, 'notranslate' ) );

		$this->css->add( new Helpers\Style( 'general', null, '/Application/_includes/common/css/jquery-ui-1.11.0.min.css' ), 'link' );
		$this->css->add( new Helpers\Style( 'general', null, '/Framework/_engine/web/js/pdfjs/viewer.css' ), 'link' );

		$this->head->addLink( new Helpers\MetaLink( 'resource', 'application/l10n', '/Framework/_engine/web/js/pdfjs/locale/locale.properties' ) );

		// $this->Html->meta( 'icon', '/img/edepoze/icon.png' );

		$this->js->addTool('jquery-2.0.3.min.js')->addTool( 'jquery-ui-1.11.0.min.js' )->addTool( 'pdfjs' );

		$this->tpl->fileSrc = 'https://'.$_SERVER['HTTP_HOST']
			. "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$this->fv['viewFile']}";
    }
}
