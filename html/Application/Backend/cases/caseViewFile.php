<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web;

class PageViewFile extends Backend
{
    public function __construct()
	{
        parent::__construct('cases/caseViewFile.html');
        $ID = isset($this->fv['ID']) ? (int) $this->fv['ID'] : 0;
    }

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$folderID = foo( new DB\OrchestraFolders() )->getFolderIDbyFileID( $this->fv['viewFile'] );
		return foo( new DB\OrchestraFolders() )->checkPermissionToFolder( $this->user->ID, $folderID );
	}

    public function init()
	{
		//View the file.
		$id = (int)$this->fv['viewFile'];
		if( !$this->getOrchestraFolders()->checkPermissionToFile( $this->user->ID, $id ) ) {
			return;
		}

		$file = new DB\Files( $id );
		if( !$file || !$file->ID ) {
			return;
		}
		if( !$file->mimeType ) {
			$file->setMetadata();
		}
		$contentType = $file->mimeType;

//		\ClickBlocks\Debug::ErrorLog( print_r( $contentType, TRUE ) );

		$fullName = $file->getFullName();

		if( in_array( $contentType, ['video/mp4','audio/mp3','audio/mpeg'] ) ) {
//			\ClickBlocks\Debug::ErrorLog( print_r( $_SERVER['HTTP_RANGE'], TRUE ) );
			while( ob_get_level() > 0 ) {
				ob_end_clean();
			}
			if( ini_get( 'zlib.output_compression' ) ) {
				ini_set( 'zlib.output_compression', 'Off' );
			}
			header( "Content-Type: {$contentType}" );
			if( isset( $_SERVER['HTTP_RANGE'] ) ) {
				\ClickBlocks\EDVideos::rangeDownload( $fullName );
			} else {
				\ClickBlocks\EDVideos::readfileChunked( $fullName );
			}
			return;
		}
		$this->ajax->downloadFile($fullName, $file->name, $contentType, true);
    }
}
