<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageCaseEdit extends Backend {

	protected $case = null;
	protected $canDelete = false;
	protected $isAdmin = false;
	protected $isCaseMgr = false;
	protected $isDemoCase = false;

	public function __construct() {
		parent::__construct('cases/caseEdit.html');

		$caseID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->case = new DB\Cases( $caseID );
		$this->isDemoCase = ($this->case->isDemo() && $this->case->clientID === $this->user->clientID);
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/cases';
		$this->isCaseMgr = $this->getOrchestraCaseManagers()->checkCaseManager( $this->case->ID, $this->user->ID );
		if( !$this->case || !$this->case->ID ) {
			$this->isAdmin = ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
		} else {
			$this->isAdmin = ($this->case->clientID == $this->user->clientID && ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin));
		}
		$this->canDelete = ($this->isAdmin || (!$this->isDemoCase && $this->isCaseMgr) );
		return (bool)((($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin) && $this->isAdmin) || $this->isCaseMgr || $this->isDemoCase);
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/country_toggle.js' ), 'link' );
		$this->head->name = 'Case Edit';
		$this->tpl->tab = 1;
		$this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
		$this->tpl->canDelete = $this->canDelete;
		$this->tpl->isAdmin = $this->isAdmin;
		if (isset($this->fv['view'])){
			$this->tpl->cancelURL = $this->basePath . '/case/view?ID='.$this->case->ID;
		} else {
			$this->tpl->cancelURL = $this->basePath . '/cases';
		}
		$client = ( new DB\ServiceClients() )->getByID( $this->user->clientID );

		$this->get('clientState')->options = (array) self::getUSAStates();
		$this->get('clientCountryCode')->options = (array) self::getCountries(false);
		$this->get('clientProvince')->options = (array) self::getCanadianProvinces();

		if ($this->case->ID > 0) {
			$this->tpl->scrumName = 'Edit: '.($this->isDemoCase?'Demo ':'').'Case';
			$this->tpl->caseID = $this->case->ID;
			$this->tpl->isNew = false;

			$formValues = $this->case->getValues();
			$formValues['clientProvince'] = $formValues['clientState'];
			$formValues['clientPostCode'] = $formValues['clientZIP'];

			$this->get('mainform')->assign($formValues);

			$caseManagers = $this->getOrchestraCaseManagers()->getByCase( $this->case->ID );
			$ids = array();
			foreach ($caseManagers as $caseManager) {
				array_push($ids, $caseManager['userID']);
			}
			$this->get('users')->ids = $ids;
		} else {
			$this->tpl->scrumName = 'Add a New Case';
			$this->tpl->isNew = true;

			// Default to client address
			$clientValues = $client->getValues();
			$formValues = [
				'clientAddress' => $clientValues[ 'address1' ],
				'clientAddress2' => $clientValues[ 'address2' ],
				'clientCity' => $clientValues[ 'city' ],
				'clientState' => $clientValues[ 'state' ],
				'clientZIP' => $clientValues[ 'ZIP' ],
				'clientCountryCode' => $clientValues[ 'countryCode' ]
			];
			$this->get('mainform')->assign($formValues);
		}
	}

	public function searchUsers($name, $uniqueID) {
		$panel = $this->get('userAutofillTemplate');
		$rows = foo(new DB\OrchestraUsers())->getUsersForDeposition($name, false, $this->user->clientID, $this->get('users')->ids);
		if (!$rows)
			return false;
		$panel->tpl->rows = $rows;
		$panel->tpl->uniqueID = $uniqueID;
		return trim($panel->getInnerHTML());
	}

	public function addUser($userID) {
		$ids = (array) $this->get('users')->ids;
		//$userID = $this->get('userIDSearch')->value;
		$user = foo(new DB\ServiceUsers())->getByID($userID);
		if ($user->ID > 0) {
			if (in_array($user->ID, $ids))
				return;
			array_push($ids, $user->ID);
			$this->get('users')->ids = $ids;
			$this->get('users')->update();
		}
		$this->get('userSearch')->value = '';
	}

	public function save(array $vs) {
		$this->ajax->script('btnLock = false');

		/*--------------ED3 - Phase1 ------------

		if (count((array)$this->get('users')->ids) == 1){
			$this->ajax->script('btnLock = false;alert("Please add a case manager");');
			return;
		}*/

		if (!$this->validators->isValid('case'))
			return;

		switch( $vs['clientCountryCode'] ) {
			case 'US':
				if( !$this->validators->isValid('countryUS') ){
					return;
				}
				break;
			case 'CA':
				if( !$this->validators->isValid('countryCA') ){
					return;
				}
				break;
			case 'OT':
				if( !$this->validators->isValid('countryOT') ){
					return;
				}
				break;
		}

		if ($this->tpl->isNew) {
			$this->case->created = date( 'Y-m-d H:i:s' );
			$this->case->createdBy = $this->user->ID;
			$this->case->clientID = $this->user->clientID;
			$vs['class'] = self::CASE_CLASS_CASE;
		} else {
			$vs['class'] = $this->case->class;
		}

		if( !isset($vs['clientRegion']) ) {
			$vs[clientRegion] = '';
		}
		else {
			$vs[clientState] = '';
		}
		if( $vs['clientProvince'] ) {
			$vs['clientState'] = $vs['clientProvince'];
			unset( $vs['clientProvince'] );
		}
		if( $vs['clientPostCode'] ) {
			$vs['clientZIP'] = $vs['clientPostCode'];
			unset( $vs['clientPostCode'] );
		}

		$this->case->setValues($vs);
		(new DB\ServiceCases())->save($this->case);

		// Delete management
		//foo(new DB\OrchestraCaseManagers())->deleteByCase($this->case->ID);
		// Save management
		$userIDs = (array)$this->get('users')->ids;
		$userIDs = array_combine($userIDs, $userIDs);
		$svDA = new DB\ServiceDepositionAssistants;
		foreach ($this->case->caseManagers as $rec)
		{
			if (isset($userIDs[$rec->userID]))
				unset($userIDs[$rec->userID]);
			else {
				foreach ($this->case->depositions as $depo)
				{
					if ($rec->userID == $depo->ownerID && !$svDA->getByID( ['depositionID'=>$depo->ID,'userID'=>$rec->userID] )->userID)
						$depo->resetOwner();
				}
				$rec->delete();
			}
		}
		foreach ($userIDs as $id)
		{
			if ($id == 0)
				continue;
			$caseManager = new DB\CaseManagers();
			$caseManager->caseID = $this->case->ID;
			$caseManager->userID = $id;
			$caseManager->created = 'NOW()';
			$caseManager->insert();
		}

		$node = new \ClickBlocks\Utils\NodeJS( TRUE );
		$node->notifyCasesUpdated( $this->case->ID );

		$this->ajax->redirect($this->tpl->cancelURL);
	}

	public function confirmDeleteCase($id) {
		$case = foo(new DB\ServiceCases())->getByID($id);
		if (0 == $case->ID)
			return;
		$param = array();
		$param['title'] = 'Delete Case';
		$param['message'] = 'Are you sure you want to delete this case, all its depositions and their contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteCase({$case->ID}, 1);";
		$this->showPopup('confirm', $param);
	}

	public function deleteCase( $caseID )
	{
		$caseID = (int)$caseID;
		// Hide dialog popup
		$this->hidePopup( 'confirm' );
		$case = foo( new DB\ServiceCases() )->getByID( $caseID );
		if(!$case || !$case->ID || $case->ID != $caseID ) {
			return;
		}
		$case->setAsDeleted();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $caseID );
		$this->ajax->redirect( '/' . $this->tpl->url . '/cases' );
	}

	public function showManagers() {
		$popup = $this->get('noticePopup');
		$popup->tpl->header = 'Case Managers';
		$popup->tpl->title = 'Case Managers have the ability to:';
		$popup->tpl->message = '* Edit the case they\'re assigned to <br> * Add/Edit sessions <br> * Assign Assistants to sessions <br> * Add/Edit/View folders for a session <br>
			* Upload/Download files in a session';
		$popup->show();
	}

	/*--------------ED3 - Phase1 ------------*/
	public function validateCaseMgr() {
			if (count((array)$this->get('users')->ids) == 1){
			return false;
		}else{
			return true;
		}
    }
	

}

?>
