<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\DB\UserSortPreferences,
	ClickBlocks\Utils,
	ClickBlocks\Debug,
	ClickBlocks\EDPDFConvert,
	ClickBlocks\EDMMConvert,
	ClickBlocks\FFProbe,
	ClickBlocks\PDFTools,
	ClickBlocks\Annotate,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Cache\CacheRedis,
	ZipArchive,
	PHPExcel_IOFactory,
	PHPExcel_Writer_Excel5,
	Exception,
	LogicException,
	ClickBlocks\Elasticsearch;

class PageCaseView extends Backend {

	protected $isAdmin = FALSE;
	protected $isClientAdmin = FALSE;
	protected $isCaseManager = FALSE;
	protected $isDemoAdmin = FALSE;
	protected $isDepoOwner = FALSE;
	protected $isDepoAssist = FALSE;
	protected $isDepoAttendee = FALSE;
	protected $isTrustedUser = FALSE;

	/**
	 * @var \ClickBlocks\DB\Cases
	 */
	protected $case;

	/**
	 * @var \ClickBlocks\Web\UI\POM\Panel
	 */
	protected $foldersPanel;

	/**
	 * @var \ClickBlocks\Web\UI\POM\Panel
	 */
	protected $filesPanel;

	protected $clientAdminID = 0;

	public function __construct() {
		parent::__construct( 'cases/caseView.html' );
		$ID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->case = new DB\Cases();
		if( $ID ) {
			$this->case->assignByID( $ID );
		}
		$this->clientAdminID = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$orcCases = $this->getOrchestraCases();
		$this->noAccessURL = $this->basePath . '/cases';
		$this->isClientAdmin = ($this->user->clientID == $this->case->clientID && ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin));
		$this->isCaseManager = $this->getOrchestraCaseManagers()->checkCaseManager( $this->case->ID, $this->user->ID );
		$this->isDemoAdmin = ($this->user->clientID == $this->case->clientID && $this->case->isDemo());
		$this->isDepoOwner = $orcCases->checkSessionOwner( $this->case->ID, $this->user->ID );
		$this->isDepoAssist = $orcCases->checkDepoAssist( $this->case->ID, $this->user->ID );
		$this->isDepoAttendee = $orcCases->checkDepoAttendee( $this->case->ID, $this->user->ID );
		$this->isTrustedUser = ($this->isCaseManager || $this->isDemoAdmin);
		return ($this->isCaseManager || $this->isDemoAdmin || $this->isDepoOwner || $this->isDepoAssist || $this->isDepoAttendee);
	}

	public function init() {
		parent::init();
		$this->js->addTool( 'ui' )->addTool( 'uploadbutton' )->addTool( 'mark' );
		$this->head->name = 'Case View';
		$this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->caseName = $this->case->name;
		$this->tpl->caseNumber = '#' . $this->case->number;
		$this->tpl->headerName = $this->case->name;

		$this->tpl->isTrustedUser = $this->isTrustedUser;
		$this->tpl->isClientAdmin = $this->isClientAdmin;
		$this->tpl->isCaseManager = $this->isCaseManager;
		$this->tpl->isDepoOwner = false;
		$this->tpl->isDemoAdmin = $this->isDemoAdmin;

		$this->tpl->hasDatasources = $this->getOrchestraDatasources()->hasDatasources( $this->user->clientID );

		//case menubutton
		$caseMenuOptions = [];
		if( $this->isClientAdmin || $this->isTrustedUser ) {
			$caseMenuOptions['Edit Case'] = ['href' => "/{$this->tpl->url}/case/edit?ID={$this->case->ID}&view=true"];
		}
		if( $this->isTrustedUser ) {
			$caseMenuOptions['Download All Case Files'] = ['onclick' => "ajax.doit('->showDownloadCase','all');"];
			$caseMenuOptions['Download Case Exhibits'] = ['onclick' => "ajax.doit('->showDownloadCase','exhibits');"];
			$caseMenuOptions['Exhibit Log'] = ['onclick' => "ajax.doit('->caseExhibitLog');"];
		}
		$this->get('case_btn_menu')->options = $caseMenuOptions;

		//add session menubutton
		$addMenuOptions = [];
		if( $this->isTrustedUser ) {
			if( !$this->case->isDemo() ) {
				$addMenuOptions['Deposition'] = ['href' => "/{$this->tpl->url}/session/deposition?caseID={$this->case->ID}"];
				$addMenuOptions['Witness Prep'] = ['href' => "/{$this->tpl->url}/session/witnessprep?caseID={$this->case->ID}"];
				$addMenuOptions['Trial'] = ['href' => "/{$this->tpl->url}/session/trial?caseID={$this->case->ID}"];
				//$addMenuOptions['Trial Binder'] = ['href' => "/{$this->tpl->url}/session/trialbinder?caseID={$this->case->ID}"];
				$addMenuOptions['Arbitration'] = ['href' => "/{$this->tpl->url}/session/arbitration?caseID={$this->case->ID}"];
				$addMenuOptions['Mediation'] = ['href' => "/{$this->tpl->url}/session/mediation?caseID={$this->case->ID}"];
				//$addMenuOptions['Hearing'] = ['href' => "/{$this->tpl->url}/session/hearing?caseID={$this->case->ID}"];
			} else {
				$addMenuOptions['Demo Deposition'] = ['href' => "/{$this->tpl->url}/session/demo?caseID={$this->case->ID}"];
				$addMenuOptions['Demo Witness Prep'] = ['href' => "/{$this->tpl->url}/session/wpdemo?caseID={$this->case->ID}"];
				$addMenuOptions['Demo Trial'] = ['href' => "/{$this->tpl->url}/session/demotrial?caseID={$this->case->ID}"];
			}
		}
		$this->get('add_btn_menu')->options = $addMenuOptions;

		$this->getCaseFolders();
	}

	protected function getCaseFolders() {
		//Folders
		$this->getFoldersPanel();
		$folderList = $this->sortList( $this->getOrchestraCases()->getCaseFolders( $this->case->ID, $this->clientAdminID ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS ) );
		$this->tpl->folderSortPrefs = $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS );
		$this->tpl->fileSortPrefs = $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FILES );
		$this->foldersPanel->tpl->folderList = $folderList;
		$selectedFolder = reset( $folderList );
		$this->tpl->selectedFolderID = $selectedFolder['ID'];
		$this->foldersPanel->tpl->canManageSelected = (bool)$selectedFolder['canManage'];
		$this->foldersPanel->tpl->folderCount = count( $folderList );
		$this->foldersPanel->tpl->folderSortBy = $this->tpl->folderSortPrefs['sortBy'];
		$this->foldersPanel->tpl->folderSortOrder = ( $this->tpl->folderSortPrefs['sortBy'] === UserSortPreferences::SORTBY_CUSTOM ? FALSE : $this->tpl->folderSortPrefs['sortOrder'] );

		//Files
		$this->getFilesPanel();
		$Folder = new DB\Folders( $this->tpl->selectedFolderID );
		$this->fileList( $Folder );
		$this->filesPanel->tpl->selectedFileIDs = [];
		$this->filesPanel->tpl->fileSortBy = $this->tpl->fileSortPrefs['sortBy'];
		$this->filesPanel->tpl->fileSortOrder = ( $this->tpl->fileSortPrefs['sortBy'] === UserSortPreferences::SORTBY_CUSTOM ? FALSE : $this->tpl->fileSortPrefs['sortOrder'] );
		$this->filesPanel->tpl->isContentSearch = FALSE;
		$this->filesPanel->tpl->folderCanUpload = $selectedFolder['canManage'];
		$this->foldersPanel->tpl->folderFileCount = count( $this->filesPanel->tpl->fileList );

		//Search
		$searchOptions = $this->getSearchOptions();
		$this->filesPanel->tpl->searchOptions = $searchOptions;
	}

	protected function getFoldersPanel() {
		if( !$this->foldersPanel ) {
			$this->foldersPanel = $this->get( 'foldersPanel' );
		}
		return $this->foldersPanel;
	}

	protected function getFilesPanel() {
		if( !$this->filesPanel ) {
			$this->filesPanel = $this->get( 'filesPanel' );
		}
		return $this->filesPanel;
	}

	protected function getSortPrefs( $sortObject ) {
		$sortPrefs = $this->getOrchestraUserSortPreferences()->getSortPreferencesForUserIDBySortObject( $this->user->ID, $sortObject );
		$sortPref = [
			'sortBy' => UserSortPreferences::SORTBY_NAME,
			'sortOrder' => 'Asc',
			'sortOn' => 'name'
		];
		if( is_array( $sortPrefs ) && $sortPrefs ) {
			$sortPref['sortBy'] = $sortPrefs['sortBy'];
			$sortPref['sortOrder'] = $sortPrefs['sortOrder'];
			$sortPref['sortOn'] = $this->getSortOn( $sortPref['sortBy'] );
		}
		return $sortPref;
	}

	protected function getSortOn( $sortBy ) {
		$sortOn = '';
		switch( $sortBy ):
			case UserSortPreferences::SORTBY_NAME:
				$sortOn = 'name';
				break;
			case UserSortPreferences::SORTBY_DATE:
				$sortOn = 'created';
				break;
			case UserSortPreferences::SORTBY_CUSTOM:
				$sortOn = 'sortPos';
				break;
			case 'Rank':
				$sortOn = 'rank';
				break;
		endswitch;

		return $sortOn;
	}

	protected function sortList( $list, $sortPrefs ) {
		if( !is_array( $list ) || !$list ) {
			return [];
		}

		foreach( $list as $key => $row ) {
			$primary[$key]  = $row[ $sortPrefs['sortOn'] ];
			$secondary[$key] = (int)$row['ID'];
		}
		$sortOrder = ( $sortPrefs['sortOrder'] === UserSortPreferences::SORTORDER_ASC ) ? SORT_ASC : SORT_DESC;
		array_multisort( $primary, $sortOrder, SORT_NATURAL|SORT_FLAG_CASE, $secondary, $sortOrder, SORT_NATURAL, $list );

		return $list;
	}

	protected function fileList( DB\Folders $Folder ) {
		$files = $this->sortList( $this->getOrchestraFiles()->getFilesWithSortPos( $Folder->ID, $this->user->ID, $this->clientAdminID ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FILES ) );
		$fileList = [];
		foreach( $files as $file ) {
			$File = new DB\Files( $file['ID'] );
			if( !$File || !$File->ID || $File->ID != $file['ID'] ) {
				continue;
			}
			if( !$File->filesize ) {
				$File->setMetadata();
			}
			$fileSize = Utils::readableFilesize( $File->filesize, 1 );
			$fileList[] = [
				'ID' => $file['ID'],
				'name' => $file['name'],
				'created' => $file['created'],
				'folderID' => $file['folderID'],
				'caseFolderID' => $file['caseFolderID'],
				'sortPos' => $file['sortPos'],
				'size' => $fileSize,
				'canManage' => (!$file['isExhibit'] || ($Folder->class == self::FOLDERS_TRANSCRIPT && $this->isTrustedUser))
			];
		}
		$this->getFilesPanel();
		$this->filesPanel->tpl->fileList = $fileList;
		$this->filesPanel->tpl->selectdFolderName = $Folder->name;
		$this->filesPanel->tpl->fileListCount = count( $fileList );
	}

	public function initPanel() {
		$this->getFoldersPanel();
		return [
			'selectedFolderID' => (int)$this->tpl->selectedFolderID,
			'canManageFolder' => (bool)$this->foldersPanel->tpl->canManageSelected,
			'hasDatasources' => (bool)$this->tpl->hasDatasources
		];
	}

	public function selectFolder( $folderID ) {
		if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			return;
		}
		$this->getFoldersPanel();
		$this->getFilesPanel();
		$Folder = new DB\Folders( $folderID );
		if( $Folder && $Folder->ID && $Folder->ID == $folderID ) {
			$this->tpl->selectedFolderID = $Folder->ID;
			$this->fileList( $Folder );
			$this->filesPanel->tpl->selectedFileIDs = [];
			$canManage = !in_array( $Folder->class, [self::FOLDERS_EXHIBIT, self::FOLDERS_TRANSCRIPT] );
			$this->filesPanel->tpl->canManageFolder = $canManage;
			$this->foldersPanel->tpl->canManageSelected = $canManage;
			$this->foldersPanel->tpl->folderFileCount = count( $this->filesPanel->tpl->fileList );
		}
		if( $this->filesPanel->tpl->isContentSearch === TRUE ) {
			$sortPrefs = $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FILES );
			$this->filesPanel->tpl->fileSortBy = $sortPrefs['sortBy'];
			$this->filesPanel->tpl->fileSortOrder = $sortPrefs['sortOrder'];
			$this->filesPanel->tpl->isContentSearch = FALSE;
		}

		$this->foldersPanel->update();
		$this->filesPanel->update();
		$this->ajax->script( 'fileMgmt.reload();' );
		return [
			'selectedFolderID' => (int)$this->tpl->selectedFolderID,
			'canManageFolder' => (int)$this->foldersPanel->tpl->canManageSelected
		];
	}

	public function selectFile( $fv_fileID ) {
		$fileID = (int)$fv_fileID;
		if( !$fileID || !($fileID > 0) ) {
			return;
		}
		$this->getFilesPanel();
		if( !$this->filesPanel->tpl->selectedFileIDs ) {
			$this->filesPanel->tpl->selectedFileIDs = [];
		}
		$selectedFileID = NULL;
		foreach( $this->filesPanel->tpl->fileList as $file ) {
			if( $file['ID'] == $fileID ) {
				$selectedFileID = $fileID;
				break;
			}
		}
		if( $selectedFileID ) {
			if( isset( $this->filesPanel->tpl->selectedFileIDs[$selectedFileID] ) ) {
				unset( $this->filesPanel->tpl->selectedFileIDs[$selectedFileID] );
			} else {
				$this->filesPanel->tpl->selectedFileIDs[$selectedFileID] = $selectedFileID;
			}
		}
		return ['canDelete'=>$this->canDeleteFilesInCaseFolder()];
	}

	public function selectAll() {
		$this->getFilesPanel();
		if( count( $this->filesPanel->tpl->selectedFileIDs ) != count( $this->filesPanel->tpl->fileList ) ) {
			foreach( $this->filesPanel->tpl->fileList as $file ) {
				$this->filesPanel->tpl->selectedFileIDs[$file['ID']] = $file['ID'];
			}
		} else {
			$this->filesPanel->tpl->selectedFileIDs = [];
		}
		return ['canDelete'=>$this->canDeleteFilesInCaseFolder()];
	}

	protected function canDeleteFilesInCaseFolder() {
		$canDelete = TRUE;
		$this->getFilesPanel();
		foreach( $this->filesPanel->tpl->selectedFileIDs as $sFileID ) {
			foreach( $this->filesPanel->tpl->fileList as $file ) {
				if( $file['ID'] == $sFileID ) {
					if( $file['folderID'] != $file['caseFolderID'] ) {
						$canDelete = FALSE;
						break 2;
					}
					break;
				}
			}
		}
		return $canDelete;
	}

	public function closePopup( $uniqueID ) {
		$dlg = $this->get( $uniqueID );
		if( $dlg ) {
			$dlg->hide();
		}
	}

	public function showEditFolder( $folderID ) {
		$dlg = $this->get( 'folderEditPopup' );
		if( $folderID ) {
			$folder = new DB\Folders( $folderID );
			$dlg->tpl->title = 'Edit Folder';
			$dlg->tpl->ID = $folder->ID;
			$dlg->tpl->name = $folder->name;
			$dlg->get( 'folderName' )->value = $folder->name;
		} else {
			$dlg->tpl->title = 'New Folder';
			$dlg->tpl->ID = 0;
			$dlg->tpl->name = '';
			$dlg->get( 'folderName' )->value = '';
		}
		$dlg->get( 'btnOK' )->onclick = "fileMgmt.saveFolder({$dlg->tpl->ID});";
		$dlg->show();
	}

	public function showEditFile( $fileID ) {
		$dlg = $this->get( 'fileEditPopup' );
		$file = new DB\Files( $fileID );
		if( $file->ID == $fileID ) {
			$dlg->tpl->ID = $file->ID;
			$dlg->get('fileName')->value = pathinfo( $file->getFullName(), PATHINFO_FILENAME );
		}
		$dlg->get( 'btnOK' )->onclick = "fileMgmt.saveFile({$dlg->tpl->ID});";
		$dlg->show();
	}

	public function showUploadFiles() {
		$folderID = (int)$this->tpl->selectedFolderID;
		if( !$folderID ) {
			return;
		}
		$folder = new DB\Folders( $folderID );
		if( !$this->isTrustedUser ) {
			return;
		}

		$dlg = $this->get( 'uploadFilesPopup' );
		$dlg->tpl->folderID = $folder->ID;
		$dlg->tpl->folderName = $folder->name;
		$dlg->get( 'dropzone' )->tpl->files = [];
		$dlg->show();
	}

	public function showMoveFolder( $folderID, $isCopy=TRUE ) {
		if( $folderID != $this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageCaseView::showMoveFolder; permission denied to folder: {$folderID}" );
			return;
		}
		$action = ($isCopy) ? 'Copy' : 'Move';
		$dlg = $this->get( 'moveFilesPopup' );
		$dlg->get( 'errormsg')->text = '';
		$dlg->get( 'btnCancel' )->removeClass( 'inactive' );
		$dlg->get( 'btnCancel' )->onclick = "ajax.doit( '->closePopup', 'moveFilesPopup' );";
		$dlg->get( 'caseList' )->onchange = '';
		$dlg->get( 'sessionLabel' )->visible = FALSE;
		$dlg->get( 'depoList' )->visible = FALSE;
		$dlg->get( 'folderList' )->visible = FALSE;
		$dlg->get( 'folderLabel' )->visible = FALSE;
		$dlg->tpl->title = "{$action} to";
		$dlg->get('btnCopy')->value = $action;
		$dlg->get('btnCopy')->onclick = 'fileMgmt.' . mb_strtolower ( $action ) . "Folder(event, '{$folderID}');";
		$this->loadCaseList( $isCopy );
		$dlg->show();
	}

	public function showMoveFiles( $isCopy=TRUE ) {
		$action = ($isCopy) ? 'Copy' : 'Move';
		$dlg = $this->get( 'moveFilesPopup' );
		$dlg->get( 'errormsg')->text = '';
		$dlg->get( 'btnCancel' )->removeClass( 'inactive' );
		$dlg->get( 'btnCancel' )->onclick = "ajax.doit( '->closePopup', 'moveFilesPopup' );";
		$dlg->get( 'caseList' )->onchange = 'loadFolderList();';
		$dlg->get( 'sessionLabel' )->visible = FALSE;
		$dlg->get( 'depoList' )->visible = FALSE;
		$dlg->get( 'folderList' )->visible = TRUE;
		$dlg->get( 'folderLabel' )->visible = TRUE;
		$dlg->tpl->title = "{$action} to";
		$dlg->get('btnCopy')->value = $action;
		$dlg->get('btnCopy')->onclick = 'fileMgmt.' . mb_strtolower ( $action ) . "Files(event, true);";
		$this->loadCaseList( TRUE );
		$this->loadFolderList( $isCopy );
		$dlg->show();
	}

	public function showAddPlaceholder() {
		$dlg = $this->get( 'addPlaceholderPopup' );
		$dlg->get( 'filename' )->value = '';
		$dlg->get( 'description' )->text = '';
		$dlg->tpl->filenameError = FALSE;
		$dlg->tpl->folderListError = FALSE;
		$dlg->get( 'btnOK' )->onclick = 'fileMgmt.addPlaceholder()';
		$lastFolderID = (isset( $this->tpl->selectedFolderID )) ? $this->tpl->selectedFolderID : FALSE;
		$this->getFoldersPanel();
		$options = [];
		foreach( $this->foldersPanel->tpl->folderList as $folder ) {
			if( in_array( $folder['class'], [self::FOLDERS_PERSONAL, self::FOLDERS_FOLDER, self::FOLDERS_TRUSTED] ) ) {
				$options[$folder['ID']] = $folder['name'];
			}
		}
		$folderList = $dlg->get( 'phFolderList' );
		$folderList->options = $options;
		$folderList->value = (isset( $options[$lastFolderID] )) ? $options[$lastFolderID] : reset( $options );
		$folderList->key = (isset( $options[$lastFolderID] )) ? $lastFolderID : '';
		$dlg->get( 'btnOK' )->visible = (count( $options ) > 0);
		$dlg->show();
	}

	public function addPlaceholder( $fv ) {
//		Debug::ErrorLog( print_r( [$fv,$this->get('phFolderList')->uniqueID], TRUE ) );
		$folderName = $fv['phFolderList'];
		$fileName = $fv['filename'];
		$dlg = $this->get( 'addPlaceholderPopup' );
		$phFolderList = $dlg->get('phFolderList');
		$phFolderList->value = $folderName;
		$hasErrors = FALSE;
		if( !preg_match( '/^[a-zA-Z0-9_\ @()#!,.&+=^\'\-]+$/', $fileName ) ) {
			$dlg->tpl->filenameError = 'ERROR: Invalid file name';
			$hasErrors = TRUE;
		} else {
			$dlg->tpl->filenameError = FALSE;
		}
		if( !preg_match( '/^[a-zA-Z0-9_\ @()#!,.&+=^\'\-]+$/', $folderName ) ) {
			$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
			$hasErrors = TRUE;
		} else {
			$dlg->tpl->folderListError = FALSE;
		}
		if( Edepo::isReservedFolder( $folderName ) ) {
			$dlg->tpl->folderListError = 'ERROR: Reserved folder name';
			$hasErrors = TRUE;
		}
		if( $hasErrors ) {
			$dlg->update();
			$dlg->show();
			return;
		}
		$isNewFolder = FALSE;
		$folder = new DB\Folders();
		$now = date( 'Y-m-d H:i:s' );
		$pk = (int)$fv["key_{$phFolderList->uniqueID}"];
		if( $pk ) {
			$folder->assignByID( $pk );
			if( $folder->ID && $folder->ID == $pk ) {
				if( strcasecmp( $folderName, $folder->name ) !== 0 ) {
					//create new folder
					$isNewFolder = TRUE;
					$folder = new DB\Folders();
					$folder->created = $now;
					$folder->class = self::FOLDERS_TRUSTED;
					$folder->createdBy = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
					$folder->caseID = $this->case->ID;
				}
			}
		}
		$folder->name = $folderName;
		$folder->lastModified = $now;
//		Debug::ErrorLog( print_r( $folder->getValues(), TRUE ) );
		$folder->save();
		$dlg->update();
		$dlg->show();

		//create placeholder
		$tmpHTML = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.html';
		$tmpPDF = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';
		$htmlString = '<html><body>
			<div style="font-family:sans-serif;text-align:center;font-size:36px;">
			<span>Placeholder for Exhibit</span>
			</div>
			<div style="margin:50px 20px 20px 20px;border:1px solid #000;">
			<p style="padding:10px;margin:0;">' . $fv['description'] . '</p>
			</div>
			</body></html>';
		$fh = fopen( $tmpHTML, 'w+' );
		if( flock( $fh, LOCK_EX ) ) {
			fwrite( $fh, $htmlString );
			fflush( $fh );
			flock( $fh, LOCK_UN );
		}
		fclose( $fh );
		$pdf = new Utils\WKHTMLToPDF( $tmpHTML, $tmpPDF );
		$pdf->setPageSize( 'Letter' );
		$pdf->setOrientation( 'Portrait' );
		$pdf->addParam( '--no-background', TRUE );
		$okToGo = $pdf->render();
		unlink( $tmpHTML );
		if( !$okToGo ) {
			unlink( $tmpPDF );
			$dlg->tpl->filenameError = 'ERROR: An error occurred while creating the Placeholder';
			$dlg->update();
			$dlg->show();
			return;
		}

		// Is this a demo? Set watermark
		if( $this->case->isDemo() ) {
			$wmPDF = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';
			$result = PDFTools::watermark( $tmpPDF, $wmPDF );
			if( $result === TRUE ) {
				copy( $wmPDF, $tmpPDF );
				unlink( $wmPDF );
			}
		} else {
			//clean wkhtmltopdf generated PDFs
			$pdfConverter = new EDPDFConvert();
			if( !$pdfConverter->pdfCleanup( $tmpPDF ) ) {
				$dlg->tpl->filenameError = 'ERROR: An error occurred while cleaning the PDF';
				$dlg->update();
				$dlg->show();
				return;
			}
			unset( $pdfConverter );
		}

		$file = new DB\Files();
		$file->name = "{$fileName}.pdf";
		$file->folderID = $folder->ID;
		$file->createdBy = $folder->createdBy;
		$file->created = $now;
		$file->setSourceFile( $tmpPDF );
		$file->insert();
		if( $file->ID ) {
			$dlg->hide();
			if( $this->tpl->selectedFolderID == $folder->ID ) {
				$this->getFilesPanel();
				$this->fileList( $folder );
				$this->filesPanel->update();
				$this->ajax->script( 'fileMgmt.reload();' );
			}
		}
		//		$this->notifyNodeJSUsers( $folder->ID );
	}

	public function saveFolder( $folderID=0 ) {
		if( !$this->validators->isValid( 'folder' ) ) {
			return;
		}
		$folder = new DB\Folders( $folderID );
		if( !$folder->ID ) {
			$now = date( 'Y-m-d H:i:s' );
			$folder->class = self::FOLDERS_TRUSTED;
			$folder->created = $now;
			$folder->lastModified = $now;
			$folder->createdBy = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
			$folder->caseID = $this->case->ID;
			$folder->depositionID = NULL;
		} elseif( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageCaseView::saveFolder; permission denied to folder: {$folderID}" );
			return;
		}
		$folder->name = $this->get( 'folderEditPopup' )->get( 'folderName' )->value;
		$folder->save();
		$this->notifyNodeJSUsers( $folder->ID );
		$this->getFoldersPanel();
		$this->foldersPanel->tpl->folderList = $this->sortList( $this->getOrchestraCases()->getCaseFolders( $this->case->ID, $this->clientAdminID ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS ) );
		$this->selectFolder( $folder->ID );
		$this->ajax->script( "var list=$('#folderList'); list.scrollTop( ($('#folder_{$folder->ID}',list).offset().top - list.offset().top) );" );
		$this->get( 'folderEditPopup' )->hide();
	}

	public function saveFile( $fileID ) {
		if( !$this->validators->isValid( 'file' ) ) {
			Debug::ErrorLog( "PageCaseView::saveFile; validation failed for fileID: {$fileID}" );
			return;
		}
		$this->get( 'fileEditPopup' )->hide();
		$file = new DB\Files( $fileID );
		if( !$file || !$file->ID || $file->ID != $fileID ) {
			Debug::ErrorLog( "PageCaseView::saveFile; file not found by ID: {$fileID}" );
			return;
		}
		$folder = new DB\Folders( $file->folderID );
		if( !$folder || !$folder->ID || $folder->ID != $file->folderID ) {
			Debug::ErrorLog( "PageCaseView::saveFile; folder not found by ID: {$file->folderID}" );
			return;
		}
		if( $file->isExhibit || $folder->class == self::FOLDERS_EXHIBIT ) {
			Debug::ErrorLog( "PageCaseView::saveFile; permission denied to rename exhibit file: {$file->ID}" );
			return;
		}
		$file->name = $this->get( 'fileEditPopup.fileName' )->value . '.' . pathinfo( $file->getFullName(), PATHINFO_EXTENSION );
		$file->update();
		$this->selectFolder( $this->tpl->selectedFolderID );
		$this->notifyNodeJSUsers( $file->folderID );
	}

	public function copyFolder( $fv, $folderID ) {
		$caseID = isset( $fv['caseList'] ) ? (int)$fv['caseList'] : 0;
		if( !$caseID || !$this->getOrchestraCases()->checkCaseAdmin( $caseID, $this->user->ID ) ) {
			Debug::ErrorLog( 'PageCaseView::copyFolder; missing destination case for folder' );
			return;
		}
		if( !$folderID || $folderID != $this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageCaseView::copyFolder; premission denied to folder: {$folderID}" );
			return;
		}
		$srcFolder = new DB\Folders( $folderID );
		if( !$srcFolder || !$srcFolder->ID || $srcFolder->ID != $folderID ) {
			Debug::ErrorLog( "PageCaseView::copyFolder; missing folder: {$folderID}" );
			return;
		}
		$this->subSession['moveFilesPopup']['caseID'] = $caseID;
		$now = date( 'Y-m-d H:i:s' );
		$newFolder = $srcFolder->copy( FALSE );	//copy files done manually to show progress
		$newFolder->caseID = $caseID;
		$newFolder->depositionID = NULL;
		$newFolder->created = $now;
		$newFolder->lastModified = $now;
		$newFolder->createdBy = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
		$newFolder->insert();
		$srcFiles = $srcFolder->files;
		if( $newFolder->ID && is_array( $srcFiles ) && $srcFiles ) {
			$redis = new CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
			$this->pageSession['progressKey'] = 'progress:' . hash( 'sha1', uniqid() );
			$max = count( $srcFiles );
			$redis->setJSON( $this->pageSession['progressKey'], ['value'=>0, 'max'=>$max], 60 * 60 );	// expire in 1 hour
			session_write_close(); // close session so progress requests don't get locked out
			foreach( $srcFiles as $i => $srcFile ) {
				$newFile = $srcFile->copy();
				$newFile->folderID = $newFolder->ID;
				$newFile->createdBy = $newFolder->createdBy;
				$newFile->isExhibit = 0;
				$newFile->sourceUserID = $this->user->ID;
				$newFile->created = $now;
				$newFile->sourceID = NULL;
				$newFile->insert();
				$redis->setJSON( $this->pageSession['progressKey'], ['value'=>++$i, 'max'=>$max], 60 * 60 );	// expire in 1 hour
			}
		}
		$this->notifyNodeJSUsers( $newFolder->ID );
		$dlg = $this->get( 'moveFilesPopup' );

		$btnCopy = $dlg->get( 'btnCopy' );
		$btnCopy->value = 'Close';
		$btnCopy->onclick = "ajax.doit( '->closePopup', 'moveFilesPopup' );";
		$btnCopy->removeClass( 'inactive' );

		$btnCancel = $dlg->get( 'btnCancel' );
		$btnCancel->onclick = null;
		$btnCancel->addClass( 'inactive' );

	}

	public function getCopyProgress() {
		$redis = new CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
		if( !isset( $this->pageSession['progressKey'] ) || !($status = $redis->getJSON( $this->pageSession['progressKey'] )) ) {
			return;
		}
		$this->ajax->script( "$('#actionProgress').attr( 'max', '{$status->max}' ); $('#actionProgress').attr( 'value', '{$status->value}' );" );
		if( $status->value == $status->max ) {
			$redis->delete( $this->pageSession['progressKey'] );
			unset( $this->pageSession['progressKey'] );
		}
		return TRUE;
	}

	public function moveFolder( $fv, $folderID ) {
		$caseID = isset( $fv['caseList'] ) ? (int)$fv['caseList'] : 0;
		if( !$caseID || !$this->getOrchestraCases()->checkCaseAdmin( $caseID, $this->user->ID ) ) {
			Debug::ErrorLog( 'PageCaseView::moveFolder; missing destination case for folder' );
			return;
		}
		if( !$folderID || $folderID != $this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageCaseView::moveFolder; premission denied to folder: {$folderID}" );
			return;
		}
		$srcFolder = new DB\Folders( $folderID );
		if( !$srcFolder || !$srcFolder->ID || $srcFolder->ID != $folderID ) {
			Debug::ErrorLog( "PageCaseView::moveFolder; missing folder: {$folderID}" );
			return;
		}
		$this->subSession['moveFilesPopup']['caseID'] = $caseID;
		$srcFolder->caseID = $caseID;
		$srcFolder->update();
		$this->notifyNodeJSUsers( $srcFolder->ID );
		$node = new Utils\NodeJS( TRUE );
		$sessionIDs = $this->getOrchestraSessionCaseFolders()->getSessionsForFolder( $srcFolder->ID );
		if( !empty( $sessionIDs ) ) {
			$node->notifyUsersRemovedFolder( $srcFolder->ID, $sessionIDs );
			$this->getOrchestraSessionCaseFolders()->unlinkAllCaseFolders( $srcFolder->ID );
		}
		$this->getCaseFolders();
		$this->foldersPanel->update();
		$this->get( 'moveFilesPopup' )->hide();
	}

	public function copyFiles( $fv ) {
		$caseID = isset( $fv['caseList'] ) ? (int)$fv['caseList'] : 0;
		if( !$caseID || !$this->getOrchestraCases()->checkCaseAdmin( $caseID, $this->user->ID ) ) {
			Debug::ErrorLog( 'PageCaseView::copyFiles; missing destination case for folder' );
			return;
		}
		$orcFolders = new DB\OrchestraFolders();
		$folderID = isset( $fv['folderList'] ) ? (int)$fv['folderList'] : 0;
		if( !$folderID || $folderID != $orcFolders->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageCaseView::copyFiles; permission denied to folder: {$folderID}" );
			return;
		}
		$srcFolder = new DB\Folders( $this->tpl->selectedFolderID );
		if( !$srcFolder || $srcFolder->ID != $orcFolders->checkPermissionToCaseFolder( $this->user->ID, $srcFolder->ID ) ) {
			Debug::ErrorLog( "PageCaseView::copyFiles; permission denied to source folder: {$srcFolder->ID}" );
			return;
		}
		$fileIDs = [];
		foreach( $srcFolder->files as $srcFile ) {
			$fileIDs[] = $srcFile->ID;
		}
		$this->getFilesPanel();
		$selectedFileIDs = [];
		foreach( $this->filesPanel->tpl->selectedFileIDs as $fileID ) {
			if( !in_array( $fileID, $fileIDs ) ) {
				Debug::ErrorLog( "PageCaseView::CopyFiles; invalid file, not in source folder: {$fileID}" );
				continue;
			}
			$selectedFileIDs[] = $fileID;
		}
		$this->subSession['moveFilesPopup']['caseID'] = $caseID;
		$this->subSession['moveFilesPopup']['folderID'] = $folderID;
		$dstFolder = new DB\Folders( $folderID );
		$redis = new CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
		$this->pageSession['progressKey'] = 'progress:' . hash( 'sha1', uniqid() );
		$max = count( $selectedFileIDs );
		$redis->setJSON( $this->pageSession['progressKey'], ['value'=>0, 'max'=>$max], 60 * 60 );	// expire in 1 hour
		session_write_close(); // close session so progress requests don't get locked out
		$now = date( 'Y-m-d H:i:s' );
		foreach( $selectedFileIDs as $i => $srcFileID ) {
			$srcFile = new DB\Files( $srcFileID );
			if( !$srcFile || !$srcFile->ID || $srcFile->ID != $srcFileID ) {
				Debug::ErrorLog( "PageCaseView::CopyFiles; missing file: {$srcFileID}" );
				continue;
			}
			$newFile = $srcFile->copy();
			$newFile->folderID = $dstFolder->ID;
			$newFile->createdBy = $dstFolder->createdBy;
			$newFile->isExhibit = 0;
			$newFile->sourceUserID = $this->user->ID;
			$newFile->created = $now;
			$newFile->sourceID = NULL;
			$newFile->insert();
			$redis->setJSON( $this->pageSession['progressKey'], ['value'=>++$i, 'max'=>$max], 60 * 60 );	// expire in 1 hour
		}
		$dstFolder->lastModified = $now;
		$dstFolder->update();

		//TODO notify users

		$dlg = $this->get( 'moveFilesPopup' );
		$btnCopy = $dlg->get( 'btnCopy' );
		$btnCopy->value = 'Close';
		$btnCopy->onclick = "ajax.doit( '->closePopup', 'moveFilesPopup' );";
		$btnCopy->removeClass( 'inactive' );
		$btnCancel = $dlg->get( 'btnCancel' );
		$btnCancel->onclick = null;
		$btnCancel->addClass( 'inactive' );
	}

	public function moveFiles( $fv ) {
		$caseID = isset( $fv['caseList'] ) ? (int)$fv['caseList'] : 0;
		if( !$caseID || !$this->getOrchestraCases()->checkCaseAdmin( $caseID, $this->user->ID ) ) {
			Debug::ErrorLog( 'PageCaseView::moveFiles; missing destination case for folder' );
			return;
		}
		$orcFolders = new DB\OrchestraFolders();
		$folderID = isset( $fv['folderList'] ) ? (int)$fv['folderList'] : 0;
		if( !$folderID || $folderID != $orcFolders->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageCaseView::moveFiles; permission denied to folder: {$folderID}" );
			return;
		}
		$dstFolder = new DB\Folders( $folderID );
		if( !$dstFolder || !$dstFolder->ID || $dstFolder->ID != $folderID ) {
			Debug::ErrorLog( "PageCaseView::moveFiles; missing folder: {$folderID}" );
			return;
		}
		$srcFolder = new DB\Folders( $this->tpl->selectedFolderID );
		if( !$srcFolder || $srcFolder->ID != $orcFolders->checkPermissionToCaseFolder( $this->user->ID, $srcFolder->ID ) ) {
			Debug::ErrorLog( "PageCaseView::moveFiles; permission denied to source folder: {$srcFolder->ID}" );
			return;
		}
		$this->subSession['moveFilesPopup']['caseID'] = $caseID;
		$this->subSession['moveFilesPopup']['folderID'] = $folderID;
		$now = date( 'Y-m-d H:i:s' );
		$fileIDs = [];
		foreach( $srcFolder->files as $srcFile ) {
			$fileIDs[] = $srcFile->ID;
		}
		$this->getFilesPanel();
		foreach( $this->filesPanel->tpl->selectedFileIDs as $fileID ) {
			if( !in_array( $fileID, $fileIDs ) ) {
				Debug::ErrorLog( "PageCaseView::moveFiles; invalid file, not in source folder: {$fileID}" );
				continue;
			}
			$srcFile = new DB\Files( $fileID );
			if( !$srcFile || !$srcFile->ID || $srcFile->ID != $fileID ) {
				Debug::ErrorLog( "PageCaseView::moveFiles; missing file: {$fileID}" );
			}
			if( in_array( $srcFolder->class, [self::FOLDERS_EXHIBIT, self::FOLDERS_TRANSCRIPT] ) ) {
				Debug::ErrorLog( "PageCaseView::moveFiles; invalid file, cannot move files from session folder(s): {$fileID}" );
				continue;
			}
			$srcFile->folderID = $folderID;
			$srcFile->update();
		}
		$srcFolder->lastModified = $now;
		$srcFile->update();
		$dstFolder->lastModified = $now;
		$dstFolder->update();

		$this->notifyNodeJSUsers( $dstFolder->ID );
		$nodeJS = new Utils\NodeJS( TRUE );
		$nodeJS->notifyUsersRemovedFile( [$srcFolder->ID], $dstFolder->ID );

		$this->selectFolder( $this->tpl->selectedFolderID );
		$this->get( 'moveFilesPopup' )->hide();
	}

	public function checkFolderName( $ctrls ) {
		$folderEdit = $this->get( 'folderEditPopup' )->tpl->name;
		$folderName = $this->getByUniqueID( $ctrls[0] )->value;
		if( $folderEdit && strcasecmp( $folderEdit, $folderName ) === 0 ) {
			return TRUE;
		}
		if( Edepo::isReservedFolder( $folderName ) ) {
			return FALSE;
		}
		return $this->getOrchestraFolders()->checkUniqueCaseFolder( $this->case->ID, $folderName );
	}

	public function checkFileName( $ctrls ) {
		$file = new DB\Files( $this->get( 'fileEditPopup' )->tpl->ID );
		$fileEdit = $file->name;
		$fileName = $this->getByUniqueID( $ctrls[0])->value . '.' . pathinfo( $file->getFullName(), PATHINFO_EXTENSION );
		if( $fileEdit && $fileEdit == $fileName ) {
			return TRUE;
		}
		return $this->getOrchestraFiles()->checkUniqueFile( $file->folderID, $fileName );
	}

	public function folderUploadDocument( $folderID, $uniqueID, $uploadID ) {
		$results = (object)['args'=>(object)[
			'uniqueID' => $uniqueID,
			'uploadID' => $uploadID,
			'folderID' => $folderID],
			'success'=>TRUE
				];

		try {
			$upload = new Utils\FileUpload();
			$extensions = trim( preg_replace( '/\s+/', '', $this->config->valid_files['extensions'] ) );
			$upload->extensions = explode( ',', $extensions );
			$documentTypesStr = trim( preg_replace( '/\s+/', '', $this->config->valid_files['document_types'] ) );
			$documentTypes = explode( ',', $documentTypesStr );
			$multimediaTypesStr = trim( preg_replace( '/\s+/', '', $this->config->valid_files['multimedia_types'] ) );
			$multimediaTypes = explode( ',', $multimediaTypesStr );
			$upload->types = array_merge( $documentTypes, $multimediaTypes );

			$info = $upload->upload( $_FILES[$uniqueID] );

			if( $upload->error === UPLOAD_ERR_INI_SIZE ) {
				throw new Exception( 'File is too large.' );
			}

			if( $upload->error === UPLOAD_ERR_EXTENSION ) {
				throw new Exception( 'File type is not accepted.' );
			}

			if( !$info ) {
				throw new Exception( 'File upload is invalid' );
			}

			// prepare meta info
			$folder = new DB\Folders( $folderID );

			// Is this a demo, and are we at the limit for demo files?
			if( $this->case->isDemo() ) {
				//TODO limit case demo files?
				//                if( $deposition->getFileCount() - $deposition->getDemoCannedFileCount() >= $this->config->demoDocLimit ) {
				//                    throw new Exception( 'Demo documents limited to ' . $this->config->demoDocLimit . '.' );
				//                }
			}

			if( !file_exists( $info['fullname'] ) ) {
				throw new Exception( 'Document not found: "'.$info['fullname'].'"' );
			}

			$filename = $info['originalName'];
			$filepathname = $info['fullname'];

			if( !in_array( $info['type'], $documentTypes ) && !in_array( $info['type'], $multimediaTypes ) ) {
				unlink( $info['fullname'] );
				throw new Exception( 'Invalid file type ('.$info['type'].')' );
						}

						$isPDF = ($info['type'] == 'application/pdf');
						$isMultimedia = in_array( $info['type'], $multimediaTypes );
						$pathInfo = pathinfo( $filename );

						// if file is not a pdf AND not one of the multimedia types, run conversion
						if( !$isPDF && !$isMultimedia ) {
						$pdfConverter = new EDPDFConvert();
						$pdfConverter->setFilename( $filepathname );
						$filepathname = $pdfConverter->convert();
						if( !$this->case->isDemo() ) {
						$pdfConverter->pdfCleanup( $filepathname );
						}
						$pdfConverter->reset();
						$filename = $pathInfo['filename'] . '.pdf';
						unlink( $info['fullname'] );
						unset( $pdfConverter );
						} elseif( $isMultimedia ) {
						// if the file is one of the multimedia types, convert it to our supported type
							$mmConvert = new EDMMConvert();
							$ffprobe = new FFProbe( $filepathname );
							if( $ffprobe->isVideo() ) {
								$isSupportedMP4 = $ffprobe->videoIsH264withAAC();
								if( !$isSupportedMP4 ) {
									$filename = $pathInfo['filename'] . '.mp4';
									$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.mp4';
									$success = $mmConvert->toMP4( $filepathname, $outPath );
									if( !$success ) {
										Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . "; Conversion from {$filepathname} to {$outPath} failed." );
										throw new Exception( 'Video conversion failed.' );
									}
									copy( $outPath, $filepathname );
									unlink( $outPath );
								}
							} else {
								$mimeType = mime_content_type( $filepathname );
								//mp3
								if( !$ffprobe->audioIsMP3() || !in_array( $mimeType, $multimediaTypes ) ) {
									Debug::ErrorLog( "PageCaseView::folderUploadDocument; convert to MP3 from mimeType: {$mimeType}" );
									$filename = $pathInfo['filename'] . '.mp3';
									$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.mp3';
									$success = $mmConvert->toMP3( $filepathname, $outPath );
									if( !$success ) {
										Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . "; Conversion from {$filepathname} to {$outPath} failed." );
										throw new Exception( 'Audio conversion failed.' );
									}
									copy( $outPath, $filepathname );
									unlink( $outPath );
								}
							}
							unset( $mmConvert );
						}

						$now = date( 'Y-m-d H:i:s' );

						// copy the file into a permanent folder
						$file = new DB\Files();
						$file->name = $filename;
						$file->setSourceFile( $filepathname );
						$file->created = $now;
						$file->folderID = $folder->ID;
						$file->sourceUserID = $this->user->ID;
						$file->isUpload = 1;
						$file->createdBy = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
						$file->isExhibit = (in_array( $folder->class, [self::FOLDERS_EXHIBIT, self::FOLDERS_TRANSCRIPT] ));
						$file->isTranscript = ($folder->class == self::FOLDERS_TRANSCRIPT);
						$file->insert();
						if( $folder->lastModified != $now ) {
							$folder->lastModified = $now;
							$folder->update();
						}

						//TODO Charge user for uploaded file
						//			if( !$deposition->isDemo() && !$deposition->isWitnessPrep() ) {
						//				DB\ServiceInvoiceCharges::charge( $this->case->clientID, self::PRICING_OPTION_UPLOAD_PER_DOC, 1, $deposition->caseID, $deposition->ID );
						//			}
						$this->fileList( $folder );

		} catch( Exception $e ) {
			$results->success = false;
			$results->error = $e->getMessage();
			Debug::ErrorLog( print_r( ['folderUploadDocument',$e], TRUE ) );
		}

		//ED-691; Watermark Demo PDFs
		if( $this->case->isDemo() && $results->success && !$isMultimedia ) {
			$inPath = $file->getFullName();
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID();
			//Debug::ErrorLog( "folderUploadDocument; Watermark --\nin: {$inPath}\nout: {$outPath}\n" );
			if( PDFTools::watermark( $inPath, $outPath ) ) {
				//Debug::ErrorLog( "folderUploadDocument; Watermark -- success" );
				rename2( $outPath, $inPath );
			} else {
				//Debug::ErrorLog( "folderUploadDocument; Watermark -- failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
			}
		}

		$this->ajax->script( 'var result = ' . json_encode( $results ) );
		//		$this->ajax->script( "selectFolder({$folder->ID});" );
	}

	public function folderUploadComplete( $folderID, $successCount ) {
		$folder = new DB\Folders( $folderID );
		if( $successCount > 0 ) {
			$this->notifyNodeJSUsers( $folder->ID );
		}
		$this->fileList( $folder );
		if($this->filesPanel == null) {
			$this->getFilesPanel();
		}
		$this->filesPanel->update();
		$this->ajax->script( 'fileMgmt.reload();' );
	}

	function notifyUsersSessionUpdated( $folderID, $byUserID ) {
		//getNodeJS()
		$node = new Utils\NodeJS( TRUE );
		if( $this->config->isDebug ) {
			$logCallback = function( $text ) {
				Debug::ErrorLog( "Sending to NodeJS: {$text}" );
			};
			$node->setLogCallback( $logCallback );
		}
		$node->notifyDepositionUpload( (int)$folderID, (int)$byUserID );
	}

	public function confirmDeleteFolder( $folderID ) {
		if( $folderID != $this->tpl->selectedFolderID ) {
			Debug::ErrorLog( "PageCaseView::confirmDeleteFolder; Selected folder doesn't match input: folderID: {$folderID}, selectedFolderID: {$this->tpl->selectedFolderID}" );
			return;
		}
		if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			return;
		}
		$folder = new DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			return;
		}
		$this->showPopup( 'confirm', [
				'title' => 'Delete Folder?',
				'message' => "Are you sure you want to delete the folder &ldquo;<strong>{$folder->name}</strong>&rdquo; and all its contents?",
				'OKName' => 'Delete',
				'OKMethod' => "fileMgmt.deleteFolder(true);"
				] );
	}

	public function deleteFolder( $folderID ) {
		if( $folderID != $this->tpl->selectedFolderID ) {
			Debug::ErrorLog( "PageCaseView::deleteFolder; Selected folder doesn't match input: folderID: {$folderID}, selectedFolderID: {$this->tpl->selectedFolderID}" );
			return;
		}
		if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			return;
		}
		$folder = new DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			return;
		}
		$this->hidePopup( 'confirm' );	// Hide dialog popup
		$nodeJS = new Utils\NodeJS( TRUE );
		$sessionIDs = $this->getOrchestraSessionCaseFolders()->getSessionsForFolder( $folder->ID );
		if( !empty( $sessionIDs ) ) {
			$nodeJS->notifyUsersRemovedFolder( $folder->ID, $sessionIDs );
		}
		$folder->delete();
		$this->getFoldersPanel();
		$folderList = $this->sortList( $this->getOrchestraCases()->getCaseFolders( $this->case->ID, $this->clientAdminID ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS ) );
		$this->foldersPanel->tpl->folderList = $folderList;
		$selectedFolder = reset( $folderList );
		$this->selectFolder( $selectedFolder['ID'] );
	}

	public function confirmDeleteFiles() {
		$this->getFilesPanel();
		$singleFile = (count( $this->filesPanel->tpl->selectedFileIDs ) === 1);
		$this->showPopup( 'confirm', [
				'title' => ($singleFile ? 'Delete Document?' : 'Delete Documents?'),
				'message' => ($singleFile ? 'Are you sure you want to delete this document?' : 'Are you sure you want to delete these documents?'),
				'OKName' => 'Delete',
				'OKMethod' => "fileMgmt.deleteFiles(null,true);"
				] );
	}

	public function deleteFiles() {
		// Hide dialog popup
		$this->hidePopup( 'confirm' );

		$this->getFilesPanel();
		$fileIDs = $this->filesPanel->tpl->selectedFileIDs;
		$folders = [];
		$now = date( 'Y-m-d H:i:s' );
		$orcFolders = new DB\OrchestraFolders();
		foreach( $fileIDs as $fileID ) {
			$file = new DB\Files( $fileID );
			if( !$file || !$file->ID || $file->ID != $fileID ) {
				Debug::ErrorLog( "PageCaseView::deleteFile; fileID: {$fileID}, not found" );
				continue;
			}

			$folder = $file->folders[0];
			if( !$folder || !$folder->ID || $folder->ID != $file->folderID ) {
				Debug::ErrorLog( "PageCaseView::deleteFile; folderID: {$file->folderID} not found" );
				continue;
			}
			if( !$folder->isCaseFolder() || $folder->ID != $orcFolders->checkPermissionToCaseFolder( $this->user->ID, $folder->ID ) ) {
				Debug::ErrorLog( "PageCaseView::deleteFile; permission denied to folder: {$folder->ID} '{$folder->name}'" );
				continue;
			}

			if( $file->folderID != $this->tpl->selectedFolderID ) {
				Debug::ErrorLog( "PageCaseView::deleteFile; cannot delete file from aggregate folder: {$file->folderID}" );
				continue;
			}

			$file->delete();

			if( !isset( $folders[$folder->ID] ) ) {
				$folder->lastModified = $now;
				$folder->save();
				$folders[$folder->ID] = $folder->ID;
			}
		}
		$nodeJS = new Utils\NodeJS( TRUE );
		$nodeJS->notifyUsersRemovedFile( $folders );
		$this->selectFolder( $this->tpl->selectedFolderID );
	}

	public function toggleSortOrder( $sortObject ) {
		$this->getFilesPanel();
		if( $this->filesPanel->tpl->isContentSearch === TRUE ) {
			$this->filesPanel->tpl->fileSortOrder = ($this->filesPanel->tpl->fileSortOrder === UserSortPreferences::SORTORDER_ASC) ? UserSortPreferences::SORTORDER_DESC : UserSortPreferences::SORTORDER_ASC;
			$sortPrefs = [
				'sortOn' => $this->getSortOn( $this->filesPanel->tpl->fileSortBy ),
				'sortOrder' => $this->filesPanel->tpl->fileSortOrder
			];
			$this->filesPanel->tpl->fileList = $this->sortList( $this->filesPanel->tpl->fileList, $sortPrefs );
			$this->filesPanel->update();
		} else {
			switch( mb_strtolower( $sortObject ) ) {
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FOLDERS ):
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FILES ):
					$sortPref = new DB\UserSortPreferences( $this->user->ID, $sortObject );
					break;
			}
			if( !isset( $sortPref ) || !$sortPref ) {
				return;
			}
			if( !$sortPref || !$sortPref->userID || !$sortPref->sortObject || $sortPref->userID != $this->user->ID || $sortPref->sortObject != $sortObject ) {
				$sortPref->userID = $this->user->ID;
				$sortPref->sortObject = $sortObject;
			}
			$sortPref->setSortPreference( $sortPref->sortBy, ($sortPref->sortOrder == UserSortPreferences::SORTORDER_ASC ? UserSortPreferences::SORTORDER_DESC : UserSortPreferences::SORTORDER_ASC) );
			$sortPrefArray = [
				'sortOn'=>$this->getSortOn( $sortPref->sortBy ),
				'sortOrder'=>$sortPref->sortOrder
			];
			if( $sortObject == UserSortPreferences::SORTOBJECT_FOLDERS ) {
				$this->tpl->folderSortPrefs = $sortPref->getValues();
				$this->getFoldersPanel();
				$this->foldersPanel->tpl->folderSortOrder = $sortPref->sortOrder;
				$this->foldersPanel->tpl->folderList = $this->sortList( $this->foldersPanel->tpl->folderList, $sortPrefArray );
				$this->foldersPanel->update();
			} elseif( $sortObject == UserSortPreferences::SORTOBJECT_FILES ) {
				$this->tpl->fileSortPrefs = $sortPref->getValues();
				$this->filesPanel->tpl->fileSortOrder = $sortPref->sortOrder;
				$this->filesPanel->tpl->fileList = $this->sortList( $this->filesPanel->tpl->fileList, $sortPrefArray );
				$this->filesPanel->update();
			}
		}
		$this->ajax->script( 'fileMgmt.reload();' );
	}

	public function changeSort( $sortObject, $sortBy ) {
		$sortBys = UserSortPreferences::sortBys();
		if( $this->filesPanel->tpl->isContentSearch === TRUE ) {
			$sortBys[] = 'Rank';
		}
		if( !in_array( $sortBy, $sortBys ) ) {
			Debug::ErrorLog( "PageCaseView::changeSort; Invalid sortBy: {$sortBy}" );
			return;
		}
		$this->getFilesPanel();
		if( $this->filesPanel->tpl->isContentSearch === TRUE ) {
			$this->filesPanel->tpl->fileSortBy = $sortBy;
			$sortPrefs = [
				'sortOn' => $this->getSortOn( $this->filesPanel->tpl->fileSortBy ),
				'sortOrder' => $this->filesPanel->tpl->fileSortOrder
			];
			$this->filesPanel->tpl->fileList = $this->sortList( $this->filesPanel->tpl->fileList, $sortPrefs );
			$this->filesPanel->update();
		} else {
			switch( mb_strtolower( $sortObject ) ) {
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FOLDERS ):
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FILES ):
					$sortPref = new DB\UserSortPreferences( $this->user->ID, $sortObject );
					break;
			}
			if( !isset( $sortPref ) || !$sortPref ) {
				return;
			}
			if( !$sortPref || !$sortPref->userID || !$sortPref->sortObject || $sortPref->userID != $this->user->ID || $sortPref->sortObject != $sortObject ) {
				$sortPref->userID = $this->user->ID;
				$sortPref->sortObject = $sortObject;
			}
			$sortPref->setSortPreference( $sortBy, $sortPref->sortOrder );
			$sortPrefArray = [
				'sortOn' => $this->getSortOn( $sortPref->sortBy ),
				'sortOrder' => $sortPref->sortOrder
			];

			if( $sortObject == UserSortPreferences::SORTOBJECT_FOLDERS ) {
				$this->tpl->folderSortPrefs = $sortPref->getValues();
				$this->getFoldersPanel();
				$this->foldersPanel->tpl->folderSortBy = $sortPref->sortBy;
				$this->foldersPanel->tpl->folderSortOrder = ( $sortPref->sortBy === UserSortPreferences::SORTBY_CUSTOM ? FALSE : $sortPref->sortOrder );
				$this->foldersPanel->tpl->folderList = $this->sortList( $this->foldersPanel->tpl->folderList, $sortPrefArray );
				$this->foldersPanel->update();
			} elseif( $sortObject == UserSortPreferences::SORTOBJECT_FILES ) {
				$this->filesPanel->tpl->fileSortBy = $sortBy;
				$this->filesPanel->tpl->fileSortOrder = ( $sortBy === UserSortPreferences::SORTBY_CUSTOM ? FALSE : $sortPref->sortOrder );
				$this->filesPanel->tpl->fileList = $this->sortList( $this->filesPanel->tpl->fileList, $sortPrefArray );
				$this->filesPanel->update();
			}
		}
		$this->ajax->script( 'fileMgmt.reload();' );
	}

	public function setCustomSort( $sortObject, $sortPosList ) {
		if( !in_array( $sortObject, [UserSortPreferences::SORTOBJECT_FOLDERS, UserSortPreferences::SORTOBJECT_FILES] ) ) {
			Debug::ErrorLog( "PageCaseView::setCustomSort; invalid sort object: {$sortObject}" );
			return;
		}
		$userID = $this->isTrustedUser ? $this->clientAdminID : $this->user->ID;
		$sortPosMap = [];
		$listMap = [];
		$sortPositions = [];

		switch( $sortObject ) {
			case UserSortPreferences::SORTOBJECT_FOLDERS:
				$this->getFoldersPanel();
				foreach( $this->foldersPanel->tpl->folderList as $idx => $folder ) {
					$sortPosMap[$idx] = $folder['sortPos'];
					$listMap[$folder['ID']] = $folder;
				}
				foreach( $sortPosList as $idx => $sortPos ) {
					$folderID = (int)$sortPos;
					$folder = $listMap[$folderID];
					$sortPositions[$folder['ID']] = $sortPosMap[$idx];
				}
				DB\UserCustomSortFolders::setCustomSort( $userID, $sortPositions );
				break;
			case UserSortPreferences::SORTOBJECT_FILES:
				$this->getFilesPanel();
				foreach( $this->filesPanel->tpl->fileList as $idx => $file ) {
					$sortPosMap[$idx] = $file['sortPos'];
					$listMap[$file['ID']] = $file;
				}
				foreach( $sortPosList as $idx => $sortPos ) {
					$fileID = (int)$sortPos;
					$file = $listMap[$fileID];
					$sortPositions[$file['ID']] = $sortPosMap[$idx];
				}
				DB\UserCustomSortFiles::setCustomSort( $userID, $sortPositions );
				break;
		}
	}

	function downloadFolder() {
		$folder = new DB\Folders( $this->tpl->selectedFolderID );
		if( !$folder || !$folder->ID || $folder->ID != $this->tpl->selectedFolderID ) {
			return;
		}
		if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folder->ID ) ) {
			return;
		}
		$fileIDs = [];
		foreach( $folder->files as $file ) {
			$fileIDs[$file->ID] = ['name'=>$file->name, 'path'=>$file->getFullName()];
		}
		if( !$fileIDs ) {
			$this->showMessage( 'Folder is empty!', 'Download folder' );
			return;
		}
		$fullName = Core\IO::dir( 'temp' ) . '/' . Utils::createUUID() . '.zip';
		$zip = new ZipArchive();
		$didOpen = $zip->open( $fullName,  ZipArchive::CREATE | ZipArchive::OVERWRITE );
		if( $didOpen !== TRUE ) {
			Debug::ErrorLog( "Unable to create ZipArchive: {$fullName}" );
			return;
		}
		foreach( $fileIDs as $fData ) {
			$zip->addFile( $fData['path'], $fData['name'] );
		}
		$zip->close();
		$this->ajax->downloadFile( $fullName, "{$folder->name}.zip" );
	}

	function downloadFiles() {
		$folder = new DB\Folders( $this->tpl->selectedFolderID );
		if( !$folder || !$folder->ID || $folder->ID != $this->tpl->selectedFolderID ) {
			Debug::ErrorLog( "Invalid folder: {$this->tpl->selectedFolderID}" );
			return;
		}
		if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folder->ID ) ) {
			Debug::ErrorLog( "Access denied to folder: {$folder->ID}" );
			return;
		}
		$files = [];
		$this->getFilesPanel();
		foreach( $this->filesPanel->tpl->selectedFileIDs as $fileID ) {
			$file = new DB\Files( $fileID );
			if( !$file || !$file->ID || $file->ID != $fileID || ($file->folderID != $folder->ID && !$folder->isCaseFolder() && $folder->caseID != $this->case->ID) ) {
				Debug::ErrorLog( "Invalid folder: {$file->folderID}, expected: {$this->tpl->selectedFolderID}" );
				continue;
			}
			$files[$file->ID] = ['name'=>$file->name, 'path'=>$file->getFullName()];
		}
		if( !$files ) {
			Debug::ErrorLog( "No files to download" );
			return;
		}
		$singleFile = (count( $files ) === 1);
		$fullName = '';
		$downloadAs = '';
		if( $singleFile ) {
			$fData = array_shift( $files );
			$fullName = $fData['path'];
			$downloadAs = $fData['name'];
		} else {
			$fullName = Core\IO::dir( 'temp' ) . '/' . Utils::createUUID() . '.zip';
			$zip = new ZipArchive();
			$didOpen = $zip->open( $fullName,  ZipArchive::CREATE | ZipArchive::OVERWRITE );
			if( $didOpen !== TRUE ) {
				Debug::ErrorLog( "Unable to create ZipArchive: {$fullName}" );
				return;
			}
			foreach( $files as $fData ) {
				$zip->addFile( $fData['path'], $fData['name'] );
			}
			$zip->close();
			$downloadAs = "{$this->case->name}-{$folder->name}.zip";
		}
		$this->ajax->downloadFile( $fullName, $downloadAs );
	}

	public function searchQuery( $query ) {
		$this->getFilesPanel();
		$this->filesPanel->tpl->searchQuery = ($query === FALSE) ? FALSE : "{$query}";
	}

	protected function loadCaseList( $isCopy=TRUE ) {
		$dlg = $this->get( 'moveFilesPopup' );
		if( !$dlg ) {
			return;
		}
		if( !isset( $this->subSession['moveFilesPopup'] ) ) {
			$this->subSession['moveFilesPopup'] = ['caseID' => 0, 'sessionID' => 0, 'folderID' => 0, 'caseFolder' => TRUE];
		}
		$dlgSession =& $this->subSession['moveFilesPopup'];

		$cases = $this->getOrchestraCases()->getCasesByUser( $this->user->ID );
		usort( $cases, function( $a, $b ) {
			$aCreated = strtotime( $a['created'] );
			$bCreated = strtotime( $b['created'] );
			return strcasecmp( $aCreated, $bCreated );
		} );
		$caseOptions = [];
		foreach( $cases as $case ) {
			if( !$isCopy && $case['ID'] == $this->case->ID ) {
				continue;
			}
			$caseOptions[$case['ID']] = $case['name'];
		}
		$dlg->get('caseList')->options = $caseOptions;
		$dlg->get('caseList')->value = (isset( $caseOptions[$dlgSession['caseID']] ) && $dlgSession['caseID']) ? $dlgSession['caseID'] : reset( $cases )['ID'];
	}

	public function loadFolderList( $isCopy=TRUE ) {
		$dlg = $this->get( 'moveFilesPopup' );
		if( $dlg->get( 'folderList' )->visible == FALSE ) {
			return;
		}
		if( !isset( $this->subSession['moveFilesPopup'] ) ) {
			$this->subSession['moveFilesPopup'] = ['caseID' => 0, 'sessionID' => 0, 'folderID' => 0, 'caseFolder' => TRUE];
		}
		$dlgSession =& $this->subSession['moveFilesPopup'];

		$folderList = $this->sortList( $this->getOrchestraCases()->getCaseFolders( $dlg->get( 'caseList' )->value, $this->user->ID ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS ) );
		$folderOptions = [];
		foreach( $folderList as $folder ) {
		   if( $folder['class'] != self::FOLDERS_TRUSTED || (!$isCopy && $folder['ID'] == $this->tpl->selectedFolderID ) ) {
			   continue;
		   }
		   $folderOptions[$folder['ID']] = $folder['name'];
		}
		$keys = array_keys( $folderOptions );
		$list = $dlg->get( 'folderList' );
		$list->options = $folderOptions;
		$list->value = (isset( $folderOptions[$dlgSession['folderID']] ) && $dlgSession['folderID']) ? $dlgSession['folderID'] : reset( $keys );
		$dlg->get('btnCopy')->visible = (count( $folderOptions ) > 0);
	}

	public function importToCase() {
		$this->ajax->redirect( "{$this->basePath}/case/import?caseID={$this->case->ID}" );
		return TRUE;
	}

	function notifyNodeJSUsers( $folderID ) {
		$node = new Utils\NodeJS( TRUE );
		$sessionIDs = $this->getOrchestraSessionCaseFolders()->getSessionsForFolder( $folderID );
		if( $sessionIDs && is_array( $sessionIDs ) ) {
			foreach( $sessionIDs as $sessionID ) {
				$node->notifyDepositionUpload( $folderID, $this->user->ID, $sessionID );
			}
		}
	}

	public function dismissDownloadCase() {
		// Debug::ErrorLog( 'dismissDownloadCase;' );
		$dlg = $this->get( 'downloadCasePopup' );
		$dlg->hide();
		$status = $this->cache->getJSON( $dlg->tpl->cacheKey );
		if( property_exists( $status, 'completed' ) && !$status->completed ) {
			$status->abort = 1;
			$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
		}
	}

	public function showDownloadCase( $opt='all' ) {
		$onlyExhibits = ($opt === 'exhibits');
		$dlg = $this->get( 'downloadCasePopup' );
		$dlg->tpl->title = ($onlyExhibits ? "Download Case Exhibits: " : "Download Case: ") . $this->case->name;
		$btnCancel = $dlg->get( 'btnCancel' );
		$btnCancel->onclick = 'ajax.doit("->dismissDownloadCase");';
		$dlg->tpl->caseName = $this->case->name;
		$tmpDir = Core\IO::dir( 'temp' );
		$this->subSession[ 'archiveUUID' ] = $uuid = Utils::createUUID();
		$this->subSession[ 'caseArchive' ] = "{$tmpDir}/{$uuid}.zip";
		$dlg->tpl->folderIDs = $folderIDs = $this->getOrchestraCases()->getAllFoldersInCaseForUser( $this->case->ID, $this->user->ID, $onlyExhibits );
		$dlg->tpl->cacheKey = "cache:archive-case:{$this->subSession[ 'archiveUUID' ]}";
		$dlg->tpl->cacheTTL = (60 * 60 * 24); // 24 hours
		$dlg->show();
		$this->ajax->script( 'cdDialog.show()' );
	}

	public function archiveCase() {
		$dlg = $this->get( 'downloadCasePopup' );
		$status = $this->cache->getJSON( $dlg->tpl->cacheKey );
		if( !$status ) {
			$status = new \stdClass();
			$status->started = 0;
			$status->completed = 0;
			$status->percent = 0;
			$status->index = 0;
			$status->total = count( $dlg->tpl->folderIDs );
			$status->statusMsg = '';
			// $this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
		}
		if( $status->started !== 0 ) {
			Debug::ErrorLog( 'PageCaseView::archiveCase; already started' );
			return $status;
		}
		$status->started = 1;
		$status->completed = 0;
		$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
		$didArchive = FALSE;
		$dlg->tpl->archiveSize = 0;
		try {
			$zip = new ZipArchive();
			$didOpen = $zip->open( $this->subSession[ 'caseArchive' ], ZipArchive::CREATE | ZipArchive::OVERWRITE );
			if( $didOpen !== TRUE ) {
				Debug::ErrorLog( "PageCaseView::archiveCase; Failure to open archive: {$this->subSession[ 'caseArchive' ]}, return value: {$didOpen}" );
				// $this->showMessage( 'Unable to archive Case', 'Error!' );
				return $status;
			}
			$zipNumFiles = $zip->numFiles;
			$zip->setArchiveComment( "{$this->case->number} -- {$this->case->name}" );
			$dlg->tpl->casePath = $casePath = preg_replace( '/[<>:"\/\\\|\?\*]/', '', $this->case->name );

			// export Exhibit Log
			$elpDlg = $this->get( 'exhibitLogPopup' );
			$elpDlg->tpl->type = 'case';
			$elpDlg->tpl->rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForCase( $this->case->ID );
			$elpDlg->tpl->headingName = $this->case->name;
			$exhibitLog = $this->createExhibitLogXLS( TRUE );
			$zip->addFile( $exhibitLog, "{$casePath}/Case Exhibit Log.xls" );
			if( $zip->numFiles > 0 ) {
				$zip->close();
			}
			unset( $zip );

			// folders
			foreach( $dlg->tpl->folderIDs as $idx => $folderID ) {
				$status = $this->cache->getJSON( $dlg->tpl->cacheKey );
				if( property_exists( $status, 'abort' ) && $status->abort ) {
					Debug::ErrorLog( "status; caseID: {$this->case->ID} -- abort" );
					$dlg->tpl->cacheTTL = 60;
					$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
					if( is_readable( $exhibitLog ) ) {
						unlink( $exhibitLog );
					}
					if( is_readable( $this->subSession[ 'caseArchive' ] ) ) {
						unlink( $this->subSession[ 'caseArchive' ] );
					}
					return $status;
				}
				$status->index = ++$idx;
				$folder = new DB\Folders( $folderID );
				$files = $folder->files;
				$fileCount = count( $files );
				if( !$fileCount ) {
					$status->percent = round( ($status->index / $status->total) * 100 );
					$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
					Debug::ErrorLog( "status; caseID: {$this->case->ID}, folderID: {$folderID}, numFiles: {$zipNumFiles} -- {$status->index}/{$status->total} {$status->percent}%" );
					continue;
				}
				$label = ($folder->isCaseFolder() ? 'Case' : 'Session') . ' Folder:';
				$numFiles = number_format( $fileCount, 0 );
				$fileStr = 'file' . ($fileCount > 1 ? 's' : ''); // if plural
				$status->statusMsg = "<strong>{$label} {$folder->name}</strong>&nbsp;&nbsp;({$numFiles} {$fileStr})";
				$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
				// Debug::ErrorLog( "status; caseID: {$this->case->ID}, folderID: {$folderID}, {$fileCount} file(s)" );
				$zip = new ZipArchive();
				$didOpen = $zip->open( $this->subSession[ 'caseArchive' ], ZipArchive::CHECKCONS );
				if( $didOpen !== TRUE ) {
					Debug::ErrorLog( "PageCaseView::archiveCase; Failure to open archive: {$this->subSession[ 'caseArchive' ]}, return value: {$didOpen}" );
					return $status;
				}
				$this->addFolderToArchive( $zip, $folder );
				$zipNumFiles = $zip->numFiles;
				// Debug::ErrorLog( "status; caseID: {$this->case->ID}, folderID: {$folderID} -- closing archive" );
				$zip->close();
				unset( $zip );
				$status = $this->cache->getJSON( $dlg->tpl->cacheKey );
				$status->percent = round( ($status->index / $status->total) * 100 );
				Debug::ErrorLog( "status; caseID: {$this->case->ID}, folderID: {$folderID}, numFiles: {$zipNumFiles} -- {$status->index}/{$status->total} {$status->percent}%" );
				$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
			}
			$didArchive = TRUE;
		} catch( \Exception $e ) {
			Debug::ErrorLog( [ '[PageCaseView::archiveCase] Exception:', [ 'message' => $e->getMessage(), 'code' => $e->getCode(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ] );
		} finally {
			// Debug::ErrorLog( 'PageCageView::archiveCase -- finally' );
			$status = $this->cache->getJSON( $dlg->tpl->cacheKey );
			$status->started = 0;
			$status->completed = 1;
			$status->percent = 100;
			$status->statusMsg = '';
			if( !$didArchive ) {
				$status->fatalError = '<div class="error2">Unable to archive Case</div>';
				$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
			} else {
				$numFiles = number_format( $zipNumFiles, 0 );
				$archiveSize = Utils::readableFilesize( $dlg->tpl->archiveSize, 1 );
				$status->statusMsg = "<strong>{$numFiles} files &mdash; {$archiveSize}</strong>";
			}
			$this->cache->setJSON( $dlg->tpl->cacheKey, $status, $dlg->tpl->cacheTTL );
			Debug::ErrorLog( "status; caseID: {$this->case->ID}, numFiles: {$zipNumFiles} -- {$status->index}/{$status->total} {$status->percent}%" );
			if( is_readable( $exhibitLog ) ) {
				unlink( $exhibitLog );
			}
		}
		return $status;
	}

	private function addFolderToArchive( ZipArchive &$zip, \ClickBlocks\DB\Folders $folder ) {
		// Debug::ErrorLog( "PageCaseView::addFolderToArchive; folderID: {$folder->ID}" );
		$dlg = $this->get( 'downloadCasePopup' );
		$casePath = $dlg->tpl->casePath;
		$folderName = preg_replace( '/[<>:"\/\\\|\?\*]/', '', $folder->name );
		$folderPath = "{$casePath}/";
		if( $folder->isCaseFolder() ) {
			$folderPath .= $folderName;
		} else {
			$session = $folder->depositions[ 0 ];
			$sessionName = preg_replace( '/[<>:"\/\\\|\?\*]/', '', $session->depositionOf );
			$folderPath .= "{$sessionName} ({$session->ID})/{$folderName}";
		}
		foreach( $folder->files as $file ) {
			$dlg->tpl->archiveSize += $file->filesize;
			$zip->addFile( $file->getFullName(), "{$folderPath}/{$file->name}" );
		}
	}

	public function getArchiveStatus() {
		$status = $this->cache->getJSON( "cache:archive-case:{$this->subSession[ 'archiveUUID' ]}" );
		// Debug::ErrorLog( [ 'getArchiveStatus' => $status ] );
		return $status;
	}

	public function downloadArchive() {
		if( !is_readable( $this->subSession[ 'caseArchive' ] ) ) {
			Debug::ErrorLog( "PageCaseView::downloadArchive; missing archive: {$this->subSession[ 'caseArchive' ]}" );
			return;
		}
		$this->ajax->downloadFile( $this->subSession[ 'caseArchive' ], "{$this->case->name}.zip" );
	}






	//TODO verify before feature complete

	function _downloadCase() {
		$depositions = (new DB\OrchestraDepositions())->getAllByCase( $this->case->ID );

		if( !$depositions ) {
			$this->showMessage( 'There are no sessions to download!', 'Download Case' );
			return;
		}

		$fullName = Core\IO::dir( 'temp' ) . '/' . $this->case->name . '_' . time() . '.zip';
		$zip = new ZipArchive();
		$ret = $zip->open( $fullName, ZipArchive::CREATE | ZipArchive::OVERWRITE );

		if( $ret !== TRUE ) {
			\ClickBlocks\Debug::ErrorLog( "PageCaseView::downloadCase; Failure to open archive: {$fullName}, return value: {$ret}" );
			$this->showMessage( 'Unable to download case', 'Error!' );
			return;
		}

		foreach( $depositions as $row ) {
			$this->addDepositionToArchive( $zip, $row['ID'], TRUE );
		}

		$zip->close();

		if( !file_exists( $fullName ) ) {
			\ClickBlocks\Debug::ErrorLog( "PageCaseView::downloadCase; Failure with archive: {$fullName}" );
			$this->showMessage( 'Unable to download case', 'Error!' );
			return;
		}
		$this->ajax->downloadFile( $fullName, $this->case->name.'.zip' );
	}

	private function addDepositionToArchive( ZipArchive &$archive, $depositionID, $caseZip=false )
	{
		$deposition = (new DB\ServiceDepositions())->getByID( $depositionID );
		$folders = (new DB\OrchestraFolders())->getFoldersByDepo( $deposition->ID );
		foreach( $folders as $folder ) {
			if ($folder['class'] == DB\Folders::CLASS_COURTESYCOPY)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_WITNESSANNOTATIONS && $this->user->ID != $deposition->ownerID)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_PERSONAL && $this->user->ID != $folder['createdBy'])
			{
				continue;
			}
			$files = (new DB\OrchestraFiles())->getFilesByFolder( $folder['ID'] );
			foreach ($files as $file)
			{
				$f = (new DB\ServiceFiles())->getByID( $file['ID'] );
				$user = (new DB\ServiceUsers())->getByID( $f->createdBy );
				$client = (new DB\ServiceClients())->getByID( $this->case->clientID );
				if( $user->clientID !== $this->case->clientID && ( $user->clientID !== $client->resellerID && $f->createdBy !== $user->ID && $deposition->isDemo() ) )
				{
					continue;
				}
				$folderPath = $user->email.'/'.$folder['name'].'/'.$f->name;
				if ($caseZip)
				{
					// remove any symbols that cause conflict with folder/file naming conventions
					$depositionName = preg_replace( '/[<_>:"\/\\\|\?\*]/', '', $deposition->depositionOf );
					$folderPath = ($depositionName.'_'.$deposition->volume.'_'.$deposition->ID).'/'.$folderPath;
				}
				$archive->addFile( $f->getFullName(), $folderPath );
			}
		}
	}

	function viewFile( $fileID ) {
		$fileID = (int)$fileID;
		$file = new DB\Files( $fileID );
		if( !$file || !$file->ID || $file->ID != $fileID ) {
			return;
		}
		$this->checkUserAccess( NULL, $file->folderID );

		if( !$file->mimeType ) {
			$file->setMetadata();
		}

		$mimeType = $file->mimeType;
		if( mb_stristr( $mimeType, 'audio' ) ) {
			$dlg = $this->get( 'viewAudioPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} elseif( mb_stristr( $mimeType, 'video' ) ) {
			$dlg = $this->get( 'viewVideoPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} else {
			$dlg = $this->get( 'viewFilePopup' );
			$dlg->tpl->src = "https://{$this->config->pdfviewer['host']}{$this->config->pdfviewer['path']}{$fileID}/viewer/";
		}
		$dlg->tpl->title = $file->name;
		$dlg->show();
	}

	private function checkUserAccess( $depositionID=NULL, $folderID=NULL ) {
		$depositionID = (int)$depositionID;
		$folderID = (int)$folderID;
		$isAllowed = FALSE;
		if( $depositionID ) {
			$isAllowed = $this->getOrchestraDepositions()->checkUserAccessToDeposition( $this->user->ID, $depositionID );
			if (!$isAllowed) {
				throw new LogicException( "Unable to find Deposition ID ({$depositionID})" );
			}
		} else {
			$folder = new DB\Folders( $folderID );
			if( $folder->isCaseFolder() ) {
				if( $folder->ID != $this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folder->ID ) ) {
					throw new LogicException( "Permission denied to folderID ({$folderID})" );
				}
			} else {
				if( !$this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $folder->depositionID ) ) {
					if( $folder->class == self::FOLDERS_EXHIBIT ) {
						$folderCreator = new DB\Users( $folder->createdBy );
						if( $this->case->clientID !== $folderCreator->clientID ) {
							throw new LogicException( "Unable to find Exhibit Folder ID ({$folderID})" );
						}
					} elseif( $folder->createdBy != $this->user->ID ) {
						throw new LogicException( "Unable to find Folder ID ({$folderID})" );
					}
				}
			}
		}
	}

	public function depoExhibitLog()
	{
		$deposition = new DB\Depositions( $this->subSession['SELECTED_DEPOSITION_ID'] );
		$rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $deposition->ID, TRUE );
		$dlg = $this->get( 'exhibitLogPopup' );
		$dlg->tpl->type = 'depo';
		$dlg->tpl->rows = $rows;
		$dlg->tpl->headingName = "{$deposition->depositionOf} (vol: {$deposition->volume})";
		$dlg->get( 'btnExport' )->visible = (count( $rows ));
		$dlg->get( 'btnExportWithExhibits' )->visible = (count( $rows ));
		$dlg->update();
		$dlg->show();
	}

	public function caseExhibitLog()
	{
		$rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForCase( $this->case->ID );
		$dlg = $this->get( 'exhibitLogPopup' );
		$dlg->tpl->type = 'case';
		$dlg->tpl->rows = $rows;
		$dlg->tpl->headingName = $this->case->name;
		$dlg->get( 'btnExport' )->visible = (count( $rows ));
		$dlg->get( 'btnExportWithExhibits' )->visible = (count( $rows ));
		$dlg->update();
		$dlg->show();
	}

	public function exportExhibitLog()
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.csv';
		$fh = fopen( $outPath, 'w' );
		if( $dlg->tpl->type == 'case' ) {
			fputcsv( $fh, ['Time','Deposition Of', 'Volume','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['depositionOf'], $row['volume'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		} else {
			fputcsv( $fh, ['Time','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		}
		fclose( $fh );

		if( file_exists( $outPath ) ) {
			$uid = ($dlg->tpl->type == 'case') ? "case_{$rows[0]['caseID']}" : "deposition_{$rows[0]['depositionID']}";
			$this->ajax->downloadFile( $outPath, "{$uid}_exhibit_logs.csv", 'application/octet-stream', FALSE );
		} else {
			$this->showMessage( 'Error generating export', 'Export Exhibit Log' );
		}
	}

	public function exportExhibitLogWithExhibits()
	{
		$dlg = $this->get( 'exhibitLogPopup' );

		$folderIDs = [];
		if ( $dlg->tpl->type == 'case' ) {
			$rows = $dlg->tpl->rows;
			$depoIDs = [];
			foreach ( $rows as $row ) {
				$depoIDs[$row['depositionID']] = $row['depositionID'];
			}
			foreach ( $depoIDs as $depoID ) {
				$deposition = new DB\Depositions( $depoID );
				if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
					continue;
				}
				$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
				$folderIDs[] = $officialExhibitsFolder['ID'];
			}
			$exhibitLogXLSPath = $this->createExhibitLogXLS( TRUE );
			$downloadFolderName = $this->case->name . " Log and Exhibits";
		} else {
			$depoID = $this->tpl->depositionID;
			$deposition = new DB\Depositions( $depoID );
			if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
				return;
			}
			$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
			$folderIDs[] = $officialExhibitsFolder['ID'];
			$downloadFolderName = $deposition->depositionOf . " ID-" . $deposition->ID . " vol-" . $deposition->volume;
			$exhibitLogXLSPath = $this->createExhibitLogXLS(false);
		}
		if (!$folderIDs) {
			return;
		}

		$this->downloadExhibitLogFolder($folderIDs, $downloadFolderName, $exhibitLogXLSPath);
	}

	public function createExhibitLogXLS($isCaseLog)
	{
		if ($isCaseLog) {
			$template = '/_templates/caseexhibitlog.xls';
		} else {
			$template = '/_templates/exhibitlog.xls';
		}
		$xls = PHPExcel_IOFactory::load(Core\IO::dir('backend') . $template);
		$sheet = $xls->setActiveSheetIndex(0);
		$sheet->setTitle('Exhibit Log');
		$this->fillExhibitLogSpreadSheet( $sheet, $isCaseLog );
		$writer = new PHPExcel_Writer_Excel5($xls);
		$fileName = Core\IO::dir('temp').'/'.Utils::createUUID().'.xls';
		$writer->save($fileName);
		return $fileName;
	}

	public function fillExhibitLogSpreadSheet(\PHPExcel_Worksheet $sheet, $isCaseLog)
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;

		if ($isCaseLog) {
			$colValueIndexes = ['lIntroducedDate', 'depositionOf', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		} else {
			$colValueIndexes = ['lIntroducedDate', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		}

		// xls template has column titles in first row, start filling spreadsheet on second row
		$rowI = 2;

		foreach ($rows as $row)
		{
			foreach ($colValueIndexes as $i => $valueIndex)
			{
				if (!isset( $row[$valueIndex] ))
				{
					continue;
				}
				$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $row[$valueIndex] );
				if ($valueIndex == 'exhibitFilename')
				{
					$sheet->getCellByColumnAndRow( $i, $rowI )->getHyperlink($row[$valueIndex])->setUrl($row[$valueIndex]);
				}
			}
			$rowI++;
		}
	}

	private function downloadExhibitLogFolder( $folderIDs, $downloadFolderName, $exhibitLogFilePath ) {
		if (!$folderIDs) {
			return;
		}

		$archiveName = Core\IO::dir( 'temp' ) . DIRECTORY_SEPARATOR . $this->case->name . '_' . time() . '.zip';
		$zip = new ZipArchive();
		$zip->open( $archiveName, ZipArchive::CREATE | ZipArchive::OVERWRITE );

		$zip->addFile( $exhibitLogFilePath, 'Exhibit Log.xls' );

		foreach ($folderIDs as $folderID) {
			$folder = new DB\Folders( $folderID );

			if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
				$this->showMessage('Folder not found!', 'Export Log With Exhibits');
				return;
			}
			$this->checkUserAccess( NULL, $folder->ID );

			$files = (new DB\OrchestraFiles())->getFilesByUserID( $folder->ID, $this->user->ID );
			if (count( $files ) < 1) {
				$this->showMessage( 'Folder is empty!', 'Export Log With Exhibits' );
				return;
			}
			foreach ($files as $fileRow) {
				if ( !$fileRow['ID'] ) {
					continue;
				}
				$zip->addFile( $folder->getFullPath().DIRECTORY_SEPARATOR.$fileRow['name'], $fileRow['name'] );
			}

		}
		$zip->close();

		$this->ajax->downloadFile( $archiveName, $downloadFolderName.'.zip' );
	}

	public function annotateFile( $form_values )
	{
		$dlg = $this->get( 'saveFilePopup' );
		$overwrite_dlg = $this->get('ConfirmFileOverwritePopup');

		$annotate = new Annotate();
		$annotate_result = $annotate->annotateFile( 'case', $form_values, $this->case->ID, $this->user->ID, $dlg );

		if( $annotate_result && $annotate_result['file_exists_id'] && $form_values['overwrite'] !== 'overwrite' ) {
			$overwrite_dlg->get( 'btnCancel' )->value = 'Cancel';
			$overwrite_dlg->show();
			return;
		} elseif( $annotate_result ) {
			$dlg->hide();
			$overwrite_dlg->hide();
			$this->notifyNodeJSUsers( $annotate_result['fileID'] );
			$this->ajax->script( 'fileMgmt.selectFolder(' . $annotate_result['folderID'] . ')' );

			$notice_dlg = $this->get( 'noticeSavePopup' );
			$notice_dlg->get( 'btnCancel' )->value = 'Ok';

			if( (int)$annotate_result[ 'fileID' ] === (int)$form_values[ 'sourceFileID' ] ) {
				$notice_dlg->tpl->message = 'The document was successfully resaved! ';
			} else {
				$notice_dlg->tpl->message = 'The document was successfully saved!';
			}
			$notice_dlg->show();
		}
	}

	protected function getSearchOptions() {
		$searchOptions = [];
		$storedOptions = $this->session['ContentSearch'];

		if( $storedOptions['caseOptions'] ) {
			$searchOptions['allDocs'] = (!$storedOptions['caseOptions']['caseFolders'] && !$storedOptions['caseOptions']['folderID']);
			$searchOptions['caseFolders'] = $storedOptions['caseOptions']['caseFolders'];
			$searchOptions['folderID'] = (!$storedOptions['caseOptions']['caseFolders'] && $storedOptions['caseOptions']['folderID']);
			$searchOptions['searchContent'] = $storedOptions['searchContent'];
			$searchOptions['includeFilename'] = $storedOptions['includeFilename'];
		} else {
			$searchOptions['allDocs'] = TRUE;
			$searchOptions['caseFolders'] = FALSE;
			$searchOptions['folderID'] = FALSE;
			$searchOptions['searchContent'] = TRUE;
			$searchOptions['includeFilename'] = FALSE;
		}

		return $searchOptions;
	}

	public function storeSearchOptions( array $searchOptions ) {
		$options['sessionOptions'] = $this->session['ContentSearch']['sessionOptions'];
		$options['caseOptions'] = [
			'caseFolders' => $searchOptions['caseFolders'],
			'folderID' => $searchOptions['folderID']
		];
		$options['searchContent'] = $searchOptions['searchContent'];
		$options['includeFilename'] = $searchOptions['includeFilename'];
		$this->session['ContentSearch'] = $options;
	}

	public function search( $options ) {
		//Debug::ErrorLog( print_r( ['PageCaseView::search', $options], TRUE ) );
		$searchOptions['allDocs'] = (!$options['caseFolders'] && !$options['folderID']);
		$searchOptions['caseFolders'] = $options['caseFolders'];
		$searchOptions['folderID'] = (!$options['caseFolders'] && $options['folderID']);
		$searchOptions['searchContent'] = $options['searchContent'];
		$searchOptions['includeFilename'] = $options['includeFilename'];

		if( !$searchOptions['allDocs'] && !$searchOptions['caseFolders'] && !$searchOptions['folderID'] ) {
			$searchOptions['allDocs'] = TRUE;
		}
		if( !$searchOptions['searchContent'] && !$searchOptions['includeFilename'] ) {
			$searchOptions['searchContent'] = TRUE;
		}

		if( !$options['folderID'] ) {
			$options['folderID'] = [];
			$folders = $this->getOrchestraCases()->getAllFoldersInCaseForUser( $this->case->ID, $this->user->ID );
			foreach( $folders as $folderID ) {
				$options[ 'folderID' ][] = $folderID;
			}
		}

		$options[ 'caseID' ] = $this->case->ID;
		$result = Elasticsearch::search( "client:{$this->case->clientID}", $options[ 'searchTerms' ], $options );
		$searchTermsArray = [];
		$fileList = [];

		if( $result[ 'total' ] > 0 ) {
			$searchTermsArray = isset($result['searchedTerms']) && is_array($result['searchedTerms']) ? $result['searchedTerms'] : [];
			foreach( $result[ 'hits' ] as $file ) {
				$fileList[] = [
					'ID' => $file[ 'fileID' ],
					'name' => $file[ 'fileName' ],
					'rank' => number_format( ($file[ '_score' ] * 100), 0 ),
					'size' => $file[ 'fileSize' ],
					'session' => $file[ 'sessionName' ],
					'folder' => $file[ 'folderName' ]
				];
			}
		}

		$this->getFilesPanel();
		if( $this->filesPanel->tpl->isContentSearch === FALSE ) {
			$this->filesPanel->tpl->fileSortBy = 'Rank';
			$this->filesPanel->tpl->fileSortOrder = UserSortPreferences::SORTORDER_DESC;
			$this->filesPanel->tpl->isContentSearch = TRUE;
		}
		$sortPrefs = [
			'sortOn' => $this->getSortOn( $this->filesPanel->tpl->fileSortBy ),
			'sortOrder' => $this->filesPanel->tpl->fileSortOrder
		];
		$this->filesPanel->tpl->fileList = $this->sortList( $fileList, $sortPrefs );
		$this->filesPanel->tpl->fileListCount = count( $fileList );
		$this->filesPanel->tpl->searchOptions = $searchOptions;
		$this->filesPanel->update();
		$contentSearchTerms = json_encode($searchTermsArray);
		$this->ajax->script( 'contentSearchTerms = ' . (($contentSearchTerms != '') ? $contentSearchTerms : 'null') );
		$this->ajax->script( 'fileMgmt.showSearchResults();' );
	}

	public function showSearchCancel()
	{
		if( $this->config->elasticsearch[ 'search_disabled' ] ) {
			$notice_dlg = $this->get( 'searchCancelPopup' );
			$notice_dlg->get( 'btnSearchCancel' )->value = 'Ok';
			$notice_dlg->tpl->title = 'Currently Unavailable';
			$notice_dlg->tpl->showSpinner = FALSE;
			$notice_dlg->tpl->message = 'Content search is not currently available while the indices are being rebuilt';
			$notice_dlg->show();
			return;
		}

		$notice_dlg = $this->get( 'searchCancelPopup' );
		$notice_dlg->get( 'btnSearchCancel' )->value = 'Cancel';
		$notice_dlg->tpl->title = 'Searching Documents';
		$notice_dlg->tpl->message = 'Press cancel to end the search before it completes.';
		$notice_dlg->tpl->showSpinner = TRUE;
		$notice_dlg->show();
		$this->ajax->script( 'fileMgmt.doSearch();' );
	}
	/*--------------ED4 - Phase1 ------------*/
	 public function showCaseFolder() {
        $popup = $this->get('noticePopup');
        $popup->tpl->header = 'Case Folders';
        $popup->tpl->title = 'Case Folders have the ability to:';
        $popup->tpl->message = 'Looking to streamline how you prepare for your depositions? <br> once while still having the ability to utilize those documents <br> in any session you create within the case <br> Upload once, save time!';
        $popup->show();
    }
}
