<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageClientInvoices extends Backend
{

    public function __construct()
    {
       parent::__construct("invoices/invoices.html");
    }

	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/';
		return in_array( $this->user->typeID, [self::USER_TYPE_SUPERADMIN, self::USER_TYPE_ADMIN, self::USER_TYPE_RESELLER] );
	}

    public function init()
    {
        parent::init();
        $this->head->name = 'Reports';
        if ($this->user->typeID == self::USER_TYPE_RESELLER){
            $this->tpl->headName = 'Client Reports';
        } else {
            $this->tpl->headName = 'Reseller Reports';
            $this->tpl->isAdmin = true;
        }

        $this->tpl->url = $this->basePath . '/reports';

        $clientID = isset($this->fv['clientID']) ? (int) $this->fv['clientID'] : 0;
        $client = foo(new DB\ServiceClients())->getByID($clientID);

		switch( strtolower( $this->user->typeID ) ):
			case 'r':
				if ( $client->resellerID != $this->user->clientID )
				{
					$client = NULL;
					throw new \LogicException( "Client ID ({$this->fv['clientID']}) not found" );
				}
				break;
			default:
				break;
		endswitch;

        $this->tpl->totalClients = foo(new DB\OrchestraClients())->getTotalClients($clientID);

        $this->get('invoices')->clientID = $client->ID;
        $this->tpl->clientName = $client->name;
        $this->tpl->clientTypeID = $client->typeID;
        $this->tpl->clientID = $client->ID;
        $this->tpl->contact = $client->contactName;
        $this->tpl->sinceDate = date_format(date_create($client->startDate), 'm/d/Y');
        $client->contactPhone = \ClickBlocks\Utils::formatPhone($client->contactPhone);
        $this->tpl->phone = $client->contactPhone;
        $this->tpl->address = $client->address1 . ', ' . $client->city . ', ' . $client->state;
        $this->tpl->active = $client->deactivated ? 0 : 1;
        $this->tpl->type = $this->get('invoices')->type = ($_GET['reports'] ? 1 : 0);
    }

    public function refreshInvoiceList($type){
        $this->get('invoices')->type = $type;
    }

    public function runInvoice($type){
        $viewLink = '';
        if ($this->tpl->isAdmin){
            $viewLink .= '/admin/report/';
        } else {
            $viewLink .= '/'.mb_strtolower( $this->reg->reseller['URL'] ).'/report/';
        }
        $viewLink .= $type ? 'detailreport?' : 'invoice?';
        $viewLink .= 'clientID='.$this->tpl->clientID;

        $viewLink .= '&startDate='. date_format(date_create($this->get('startDate')->value), 'Ymd');
        $viewLink .= '&endDate='. date_format(date_create($this->get('endDate')->value), 'Ymd');

        $this->ajax->redirect($viewLink);
    }
}
?>

