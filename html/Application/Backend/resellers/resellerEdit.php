<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageResellerEdit extends Backend
{
   /**
    * @var \ClickBlocks\DB\Clients
    */
    protected $client = null;
    protected $userclient = null;

    public function __construct()
	{
		parent::__construct('resellers/resellerEdit.html');
		$ID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->client = (new DB\ServiceClients())->getByID( $ID );
		//\ClickBlocks\Debug::ErrorLog( "Client->ID: {$this->client->ID} Client->resellerID: {$this->client->resellerID}" );

		if (!$this->client->ID)
		{
			if ($ID > 0)
			{
				$this->client = null;
				$this->redirect( $this->basePath.($this->user->typeID === self::USER_TYPE_RESELLER ? '/client/add' : '/reseller/add') );
			}
		} else {
			if( $this->user->typeID === self::USER_TYPE_SUPERADMIN || $this->user->typeID === self::USER_TYPE_ADMIN ) {
				if( $this->client->typeID !== self::CLIENT_TYPE_RESELLER ) {
					$this->client = NULL;
					$this->redirect( $this->basePath . '/resellers' );
				}
			} else
			if( $this->user->typeID === self::USER_TYPE_RESELLER ) {
				//ED-124;
				if( ($this->client->typeID !== self::CLIENT_TYPE_CLIENT && $this->client->typeID !== self::CLIENT_TYPE_ENT_CLIENT) || $this->client->resellerID !== $this->user->clientID ) {
					$this->client = NULL;
					$this->redirect( $this->basePath . '/clients' );
				}
			}
		}

		$userType = ($this->user->typeID == self::USER_TYPE_RESELLER ? self::USER_TYPE_CLIENT : self::USER_TYPE_RESELLER);
		$this->userclient = (new DB\ServiceUsers())->getByID( (new DB\OrchestraUsers())->getUsersByClient( $ID, $userType ) );

	}

	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/';

		if ($this->user->typeID == self::USER_TYPE_RESELLER) {
			if ($this->client && $this->client->ID) { // Edit Client
				$hasAccess = ($this->client->resellerID == $this->user->clientID);
			} else { // Add Client
				$hasAccess = true;
			}
		} else if ($this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN) {
			$hasAccess = true;
		} else {
			$hasAccess = false;
		}

		return $hasAccess;
	}

    public function init() {
        parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url('backend-js').'/sha1.js' ), 'link' );
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/country_toggle.js' ), 'link' );
        $this->tpl->tab = 1;
		$this->tpl->httpHost = $this->config->http_host;

        if ($this->user->typeID == self::USER_TYPE_RESELLER) {
            $this->head->name = 'Client Edit/Add';
            $this->tpl->url = mb_strtolower( $this->reg->reseller['URL'] );
            $this->tpl->typeName = 'Client';
			$this->tpl->isAdmin = false;

            $rowPricingModelOptions = foo(new DB\OrchestraPricingModelOptions())->getByClient($this->reg->reseller['ID']);
            $this->tpl->resellerSubcriptionFee = '$' . $this->getPriceFromModel($rowPricingModelOptions, self::PRICING_OPTION_SUBSCRIPTION)
                . ($this->getTermFromModel($rowPricingModelOptions, self::PRICING_OPTION_SUBSCRIPTION)=='M'?'/Month':'');
        } else {
            $this->head->name = 'Reseller Edit/Add';
            $this->tpl->typeName = 'Reseller';
			$this->tpl->isAdmin = TRUE;
		}

        $this->get('step1')->get('state')->options = (array) self::getUSAStates();
        $this->get('step1')->get('state')->value = 'AK';
		$this->get( 'step1.resellerLevel' )->options = self::getResellerLevels();
		$this->get( 'step1.resellerLevel' )->value = self::RESELLERLEVEL_NONE;
		$this->get( 'step1.resellerClass' )->options = self::getResellerClasses();
		$this->get( 'step1.resellerClass' )->value = self::RESELLERCLASS_SCHEDULING;

		$this->get('step1.countryCode')->options = (array) self::getCountries(false);
		$this->get('step1.province')->options = (array) self::getCanadianProvinces();

        $this->get('imgEditor')->showLoader = false;
        $this->get('imgEditor')->uploads = array($this->get('step1')->get('btnUploadLogo')->uniqueID => array(
                'callBack' => '->uploadLogo',
                'maxSize'  => 1024*1024*100, // 100 Mb
                'extension' => array('gif', 'png', 'jpg', 'jpeg', 'pbm', 'tga', 'dds', 'ico', 'bmp', 'svg'),
                'cropWidth' => 344,
                'cropHeight' => 114));

		if( !$this->tpl->isAdmin ) {
			$this->get( 'step1.valLiveTranscriptsEmail' )->groups = 'ignore';
		}

        if( $this->client->ID > 0 ) {
            $this->tpl->headerName = $this->client->name;
            $this->tpl->resellerName = $this->client->name;
            $this->get('step1')->get('logoLabel')->visible = false;
            $this->tpl->isNew = false;

            $this->get('step3')->get('valpassword')->groups = 'ignore';
            $this->get('step3')->get('valconfirmpassword')->groups = 'ignore';

            $this->tpl->deactivated = $this->client->deactivated;
            $this->get('step1')->get('deactivated')->value = $this->client->deactivated ? '1' : '0';
            $this->get('step1')->get('name')->value = $this->client->name;
            $this->get('step1')->get('resellerID')->value = $this->tpl->ID = $this->client->ID;
			$this->get('step1')->get('salesRep')->value = $this->client->salesRep;
            $this->get('step1')->get('startDate')->value = date_format(date_create($this->client->startDate), 'm/d/Y');
			$this->get('step1')->get('location')->value = $this->client->location;
            $this->get('step1')->get('contactName')->value = $this->client->contactName;
            $this->get('step1')->get('contactPhone')->value = $this->client->contactPhone = \ClickBlocks\Utils::formatPhone($this->client->contactPhone);
            $this->get('step1')->get('contactEmail')->value = $this->client->contactEmail;
            $this->get('step1')->get('address1')->value = $this->client->address1;
            $this->get('step1')->get('address2')->value = $this->client->address2;
            $this->get('step1')->get('city')->value = $this->client->city;
			$this->get('step1')->get('ZIP')->value = $this->client->ZIP;
			$this->get('step1')->get('postCode')->value = $this->client->ZIP;
			$this->get('step1')->get('state')->value = $this->client->state;
			$this->get('step1')->get('province')->value = $this->client->state;
			$this->get('step1')->get('region')->value = $this->client->region;
			$this->get('step1')->get('countryCode')->value = $this->client->countryCode;
			$this->get( 'step1.liveTranscriptsEmail' )->value = $this->client->liveTranscriptsEmail;
			$this->get( 'step1.resellerLevel' )->value = $this->client->resellerLevel;
			$this->get( 'step1.resellerClass' )->value = $this->client->resellerClass;
            $this->get('step1')->get('description')->text = $this->get('step1')->get('comments')->text = $this->client->description;
            $this->get('step1')->get('bannerColor')->value = $this->client->getBannerColor();
            $this->get('step1')->tpl->textcolor = $this->get('step1')->get('bannerTextColor')->value = $this->client->getBannerTextColor();
            $this->get('step1')->get('URL')->value = mb_strtolower( $this->client->URL );
            // Remove this for now. Should always be defaulted to '1' for on TODO : Do we want this in here, or somewhere else?
            //$this->get('step1')->get('casesDemoDefault')->value = $this->tpl->casesDemoDefault = (int)$this->client->casesDemoDefault;

            if ($this->client->logo && file_exists(Core\IO::dir('logos') . '/' . $this->client->logo) ){
                $this->get('step1')->get('logoImage')->src = Core\IO::url('logos') . '/' . $this->client->logo;
                $this->get('step1')->get('logoName')->value = $this->client->logo;
            } else {
                $this->get('step1')->get('logoLabel')->visible = true;
                $this->get('step1')->get('logoImage')->visible = false;
            }

            $rowPricingModelOptions = foo(new DB\OrchestraPricingModelOptions())->getByClient($this->client->ID);
            $this->get('step2')->get('subscriptionFee')->value = '$' . $this->getPriceFromModel($rowPricingModelOptions, self::PRICING_OPTION_SUBSCRIPTION);

            $this->get('step3.firstName')->value = $this->userclient->firstName;
			$this->get('step3.lastName')->value = $this->userclient->lastName;
			$this->get('step3.email')->value = $this->userclient->email;
            $this->get('step3.username')->value = $this->userclient->username;

			$auth = (new DB\ServiceUserAuth())->getByUserID( $this->userclient->ID );
			$this->tpl->isAccountLocked = ($auth->hardLock !== null);
        } else {
            $this->tpl->headerName = 'Add New Account';
            $this->tpl->resellerName = 'Add new account';
            $this->get('step1')->get('logoImage')->visible = false;
            $this->get('step1')->get('startDate')->value = date('m/d/Y');
            $this->tpl->isNew = true;
        }

		/*
        if ($this->user->typeID == self::USER_TYPE_RESELLER) {

            $ct1 = $this->get('step2.includedAttendees');
            $ct2 = $this->get('step2.bundleSize');
            $ct1->readonly = $ct2->readonly = $ct1->disabled = $ct2->disabled = true;
            $this->get('valFields2')->controls = array();
            if (!$this->client->ID) {
               $reseller = $this->user->clients[0];
               $ct1->value = $reseller->includedAttendees;
               $ct2->value = $reseller->attendeeBundleSize;
            }
        }
		*/

        //$this->get('step1')->visible = true;
        //$this->get('step2')->visible = false;
        //$this->get('step3')->visible = false;

        $this->openTab(1);
    }

    public function toStep1($fv) {
        $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step1')->uniqueID . "', '');");
        //$this->get('step1')->visible = true;
        $this->ajax->script("changeTab(0,1)");
    }

    public function toStep2($fv, $isBack = false) {
        if ($isBack) {
            $this->ajax->script("controls.display('" . $this->get('step3')->uniqueID . "', 'none');");

            $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', '');");
            //$this->get('step2')->visible = true;

            $this->ajax->script("changeTab(0,2)");
        } else {
            if ($this->validators->isValid('step1')) {
				$this->ajax->script("controls.display('" . $this->get('step1')->uniqueID . "', 'none');");
				$this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', '');");
				$this->ajax->script("changeTab(0,2)");
            }
        }
    }

    public function toStep3($fv) {
        if ($this->validators->isValid('step2')) {
            $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', 'none');");
            $this->ajax->script("controls.display('" . $this->get('step3')->uniqueID . "', '');");

            //$this->get('step3')->visible = true;
            $this->ajax->script("changeTab(0,3)");
        }
    }

    public function openTab($step) {
        $this->ajax->script("controls.display('" . $this->get('step1')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step2')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step3')->uniqueID . "', 'none');");

        $this->ajax->script("controls.display('" . $this->get('step' . $step)->uniqueID . "', '');");
        //$this->get('step' . $step)->visible = true;
    }

	public function unlock()
	{
		DB\UserAuth::unlockAccount( $this->userclient->ID );
		$sendTo = ($this->user->typeID == self::USER_TYPE_RESELLER) ? $this->basePath.'/clients' : $this->config->adminPath.'/resellers';
        $this->ajax->redirect( $sendTo );
	}

    public function checkUsername($ctrls) {
        return foo(new DB\OrchestraUsers)->checkUniqueUsername($this->getByUniqueID($ctrls[0])->value, $this->userclient->ID);
    }

    public function checkUrl($ctrls) {
        if (!empty($this->client->URL) && $this->client->URL == $this->getByUniqueID($ctrls[0])->value) {
            return true;
        }
        return foo(new DB\OrchestraClients)->checkUniqueUrl($this->getByUniqueID($ctrls[0])->value);
    }

	public function validatePassword( $ctrls )
	{
		$password = $this->getByUniqueID( $ctrls[0] )->value;
		if (strlen( $password ) < 1)
		{
			return true;
		}
		return DB\UserAuth::validatePassword( $password );
	}

	public function save() {
        $this->ajax->script( 'btnLock = false' );

		$fv['name'] = $this->get( 'step1.name' )->value;
		$fv['URL'] = mb_strtolower( $this->get( 'step1.URL' )->value );
		$fv['username'] = $this->get( 'step3.username' )->value;

        $isValid = TRUE;
//        if ($this->tpl->isNew) {
//            $isValid &= $this->validators->isValid( 'step3' );
//		} else {
            $isValid &= $this->validators->isValid( 'step1' );
			$isValid &= $this->validators->isValid( 'step2' );
			$isValid &= $this->validators->isValid( 'step3' );
//		}

		switch( $this->get('step1')->get('countryCode')->value ) {
			case 'US':
				$isValid &= $this->validators->isValid( 'countryUS' );
				break;
			case 'CA':
				$isValid &= $this->validators->isValid( 'countryCA' );
				break;
			case 'OT':
				$isValid &= $this->validators->isValid( 'countryOT' );
				break;
		}

        if( !$isValid ) return;

        $client = $this->client;
        $user = $this->userclient;

		$txEmail = $client->liveTranscriptsEmail;

        $client->name = $this->get('step1')->get('name')->value;
		$client->salesRep = $this->get('step1')->get('salesRep')->value;
        $client->startDate = date_format(date_create_from_format('m/d/Y', $this->get('step1')->get('startDate')->value), 'Y-m-d');
        $client->deactivated = $this->get('step1')->get('deactivated')->value == '1' ? 'NOW()' : null;
		$client->location = trim($this->get('step1')->get('location')->value) == false ? null : $this->get('step1')->get('location')->value;

        $client->contactName = $this->get('step1')->get('contactName')->value;
        $client->contactPhone = $this->get('step1')->get('contactPhone')->value;
        $client->contactEmail = $this->get('step1')->get('contactEmail')->value;

        $client->URL = empty($this->get('step1')->get('URL')->value) ? null : mb_strtolower( $this->get('step1')->get('URL')->value );
		$client->countryCode = $this->get('step1')->get('countryCode')->value;
        $client->address1 = $this->get('step1')->get('address1')->value;
        $client->address2 = $this->get('step1')->get('address2')->value;
        $client->city = $this->get('step1')->get('city')->value;
        $client->state = $this->get('step1')->get('state')->value;
        $client->region = $this->get('step1')->get('region')->value;
        $client->ZIP = $this->get('step1')->get('ZIP')->value;
        // ED-1425 REMOVE THIS FOR NOW. Default to '1' TODO : Do we want this in here, or somewhere else?
        //$client->casesDemoDefault = $this->get('step1')->get('casesDemoDefault')->value;
        $client->casesDemoDefault = 1;

		switch( $this->get('step1')->get('countryCode')->value ) {
			case 'US':
				$client->region = '';
				break;
			case 'CA':
				$client->state = $this->get('step1')->get('province')->value;
				$client->ZIP = $this->get('step1')->get('postCode')->value;
				$client->region = '';
				break;
			case 'OT':
				$client->state = '';
				$client->ZIP = $this->get('step1')->get('postCode')->value;
				break;
		}

		$client->liveTranscriptsEmail = $this->get( 'step1.liveTranscriptsEmail' )->value;

        if ($this->user->typeID == self::USER_TYPE_RESELLER) {
            $client->typeID = self::CLIENT_TYPE_CLIENT;
            $client->resellerID = $this->reg->reseller['ID'];
            $client->description = $this->get('step1')->get('comments')->text;
        } elseif( $this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN ) {
			$client->typeID = self::CLIENT_TYPE_RESELLER;
			$client->resellerClass = self::RESELLERCLASS_SCHEDULING;
            $client->description = $this->get( 'step1.description' )->text;
			$client->resellerLevel = $this->get( 'step1.resellerLevel' )->value;
			$client->resellerClass = $this->get( 'step1.resellerClass' )->value;
        }

        $client->setBannerColor($this->get('step1')->get('bannerColor')->value);
        $client->setBannerTextColor($this->get('step1')->get('bannerTextColor')->value);
        if ($this->user->typeID == self::USER_TYPE_RESELLER) {
            if ($this->tpl->isNew) {
                $client->created = 'NOW()';
                foo(new DB\ServiceClients())->insert($client);
            } else {
                foo(new DB\ServiceClients())->update($client);
            }
        } else {

            $destDir = Core\IO::dir('logos');
            Core\IO::createDirectories($destDir);
            if ($this->tpl->isNew) {
                $client->created = 'NOW()';
                $client->logo = $this->get('step1')->get('logoName')->value;
                if ($client->logo)
                    rename2(Core\IO::dir('temp') . '/' . $client->logo, $destDir . '/' . $client->logo);
                foo(new DB\ServiceClients())->insert($client);
            } else {
                if ($client->logo != $this->get('step1')->get('logoName')->value) {
                    if (is_file($destDir . '/' . $client->logo))
                        unlink($destDir . '/' . $client->logo);
                    $client->logo = $this->get('step1')->get('logoName')->value;
					if ($client->logo) {
						rename2(Core\IO::dir('temp') . '/' . $client->logo, $destDir . '/' . $client->logo);
					}
                }

                foo(new DB\ServiceClients())->update($client);
            }
        }

        // Update PricingModelOptions data from step 2
        $this->insertPricingModelOptions($client->ID, self::PRICING_OPTION_SUBSCRIPTION, $this->get('step2')->get('subscriptionFee')->value, self::PRICING_TYPE_MONTHLY);

        // Data from step 3
        $user->email = $this->get('step3.email')->value;
        $user->username = $this->get('step3.username')->value;
        $user->firstName = $this->get('step3.firstName')->value;
		$user->lastName = $this->get('step3.lastName')->value;
        $user->typeID = $this->user->typeID == self::USER_TYPE_RESELLER ? self::USER_TYPE_CLIENT : self::USER_TYPE_RESELLER;
        if (!$this->tpl->isNew) {
			(new DB\ServiceUsers())->update($user);
        } else {
            $user->created = 'NOW()';
            $user->clientID = $client->ID;
			$user->countryCode = $this->get('step1')->get('countryCode')->value;
            (new DB\ServiceUsers())->insert($user);
            $client->chargeUponCreation();
        }

        // Do we want a demo case created?
        if( ( $client->typeID == 'C' || $client->typeID == 'EC' ) && !$client->hasDemoCase() ){
            // Add a demo case for the new client
            $demoCase = new DB\Cases();
            $demoCase->createDemoCase( $client->ID, $user->ID );
            $demoCase->save();

            /* ED-1426: Do NOT make all users case managers
            // Make all client users (if any) case managers
            $clientObj = new DB\OrchestraUsers();
            $clientUsers = $clientObj->getAllUsersByClient( $client->ID, 'CU', true );

            if($clientUsers){
                $CMObj = new DB\ServiceCaseManagers();

                foreach ($clientUsers as $clientUser) {
                    $CMObj->setCM($demoCase->ID, $clientUser['ID']);
                }
            }
            */

            // Add a demo deposition for the new demo case
            $demoDepo = new DB\Depositions();
            $demoDepo->createDemoDeposition( $demoCase, $user->ID );
        }

		$password = $this->get('step3')->get('passauth')->value;
        if( $password ) {
			DB\UserAuth::updatePassword( $user->ID, $password, true );
		}

		\ClickBlocks\Debug::ErrorLog( print_r( [$txEmail, $client->liveTranscriptsEmail], TRUE ) );

		// ED-2661; live transcript email changed, propigate to any not set
		if( $txEmail != $client->liveTranscriptsEmail ) {
			$client->propigateLiveTranscriptsEmail();
		}

        if ($this->user->typeID == self::USER_TYPE_RESELLER)
            $this->ajax->redirect('/'. $this->tpl->url . '/clients');
        else
            $this->ajax->redirect($this->config->adminPath . '/resellers');
    }

    private function insertPricingModelOptions($ID, $optionID, $price, $typeID=self::PRICING_TYPE_MONTHLY)
	{
        $price = floatval(substr($price, 0, 1) == '$' ? substr($price, 1) : $price);

		$pricingOption = new DB\PricingModelOptions( $ID, $optionID );
		if( $pricingOption->clientID == $ID && $pricingOption->price == $price && $pricingOption->typeID == $typeID ) {
			return;
		}

		$pricingOption->price = $price;
		$pricingOption->typeID = $typeID;

		if (!$pricingOption->clientID || !$pricingOption->optionID)
		{
			$pricingOption->clientID = $ID;
			$pricingOption->optionID = $optionID;
			(new DB\ServicePricingModelOptions())->insert( $pricingOption );
		} else {
			(new DB\ServicePricingModelOptions())->update( $pricingOption );
			DB\ServiceInvoiceCharges::insertFeeAdjustment( $pricingOption->clientID, $pricingOption->optionID, $pricingOption->price, $this->user->ID );
		}
    }

    private function getPriceFromModel(array $arrOptions, $optionID) {
        foreach ($arrOptions as $row) {
            if ($row['optionID'] == $optionID)
                return $row['price'];
        }
        return 0.00;
    }

    private function getTermFromModel(array $arrOptions, $optionID) {
        foreach ($arrOptions as $row) {
            if ($row['optionID'] == $optionID)
                return $row['typeID'];
        }
        return self::PRICING_TYPE_MONTHLY;
    }

    public function uploadLogo($uniqueID) {
        $info = Utils\UploadFile::getInfo($uniqueID);
        $this->get('step1')->get('logoImage')->src = $info['url'] . '?' . microtime();
        $this->get('step1')->get('logoName')->value = $info['name'];

        $this->ajax->script("controls.display('" . $this->get('step1')->get('logoLabel')->uniqueID . "', 'none');");
        $this->ajax->script("controls.display('" . $this->get('step1')->get('logoImage')->uniqueID . "', '');");
        $this->get('step1')->get('logoLabel')->visible = false;
        $this->get('step1')->get('logoImage')->visible = true;
    }

    public function confirmDeleteAccount() {
        $client = $this->client;
        if (0 == $client->ID)
            return;
        $param = array();
        if ($this->user->typeID != self::USER_TYPE_RESELLER){
            $param['title'] = 'Delete Reseller';
            $param['warning'] = 'Warning!!! This is not recoverable! If you delete this reseller account, all the information related to this account will be lost, including and of the reseller\'s clients and their files.';
            $param['message'] = 'Are you sure you would like to delete this reseller?';
        } else {
            $param['title'] = 'Delete Client';
            $param['message'] = 'Are you sure you would like to delete this client?';
        }
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteAccount(1);";
        $this->showPopup('confirm', $param);
    }

    public function deleteAccount()
	{
        // Hide dialog popup
        $this->hidePopup('confirm');
        if ($this->client->typeID == self::CLIENT_TYPE_RESELLER)
        {
            // Physically delete Reseller, his Child-clients, his users, with all Cases,Depositions and files & folders!
            $this->client->delete();
        }
        else
        {
           // Only mark Client as deleted (when deleted by Reseller), all Cases, Depositions, Users, Files will remain
           $this->client->setAsDeleted();
        }

        $sendTo = ($this->user->typeID == self::USER_TYPE_RESELLER) ? $this->basePath.'/clients' : $this->config->adminPath.'/resellers';
        $this->ajax->redirect( $sendTo );
    }
}

?>
