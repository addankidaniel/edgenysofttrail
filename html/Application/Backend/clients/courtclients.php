<?php

namespace ClickBlocks\MVC\Backend;

class PageCourtClients extends Backend {

	public function __construct() {
		parent::__construct( 'clients/courtclients.html' );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return $this->user->isAdmin();
	}

	public function init() {
		parent::init();
		$this->tpl->tab = 1;
		$this->tpl->headName = $this->head->name = 'Court Clients';
		$this->tpl->editPath = $this->config->adminPath . '/courtclients/edit?ID=';
	}

	public function search() {
		$txt = $this->get( 'searchText' );
		$this->get( 'clients' )->searchValue = $this->tpl->searchValue = trim( $txt->value );
		$this->get( 'clients' )->update();
	}
}
