<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageEnterpriseEdit extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Clients
	 */
	protected $client = NULL;

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $clientUser = NULL;

	/**
	 * @var boolean record exists flag
	 */
	protected $isEdit = FALSE;

	/**
	 * @var string reseller URL path
	 */
	protected $resellerURL = NULL;

	/**
	 * @var boolean user is superadmin role
	 */
	protected $isEdepozeAdmin = FALSE;

	public function __construct()
	{
		parent::__construct( 'clients/enterprise.html' );
		$clientID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->client = new DB\Clients( $clientID );
		if( $clientID ) {
			if( $this->client && $this->client->ID == $clientID ) {
				$this->isEdit = TRUE;
			} else {
				$this->client = NULL;
				$this->redirect( $this->basePath . '/clients' );
			}
		}
		$clientUserID = $this->getOrchestraUsers()->getUsersByClient( $this->client->ID, self::USER_TYPE_CLIENT );
		$this->clientUser = new \ClickBlocks\DB\Users( $clientUserID );
		$this->isEdepozeAdmin = ($this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN);
		$this->resellerURL = ($this->user->typeID === self::USER_TYPE_RESELLER) ? $this->user->clients[0]->URL : '';
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		if( $this->user->typeID === self::USER_TYPE_RESELLER ) {
			if( $this->isEdit ) {
				$hasAccess = ($this->client->resellerID == $this->user->clientID);
			} else {
				$hasAccess = TRUE;	//allow to create new
			}
		} elseif( $this->isEdepozeAdmin && $this->isEdit ) {
			$hasAccess = TRUE;
		} else {
			$hasAccess = FALSE;
		}
		return $hasAccess;
	}

    public function init()
	{
        parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->js->add( new Helpers\Script( null, null, Core\IO::url('backend-js').'/country_toggle.js' ), 'link' );
		$this->head->name = 'Enterprise Client';
		$this->tpl->tab = 1;
		$this->tpl->isEdepozeAdmin = $this->isEdepozeAdmin;
		$this->tpl->cancelLink = ($this->isEdepozeAdmin) ? "{$this->config->adminPath}/clients" : "/{$this->resellerURL}/clients";
		$this->tpl->pageAction = ($this->isEdit) ? $this->client->name : 'Add new enterprise';
		$this->tpl->isEdit = $this->isEdit;
		$this->tpl->httpHost = $this->config->http_host;
		$tplStep1 = $this->get( 'step1' );
		$tplStep2 = $this->get( 'step2' );
		$tplStep1->get( 'startDate' )->value = date( 'm/d/Y' );

		$countriesSelect = self::getCountries(false);
		$statesSelect = self::getUSAStates();
		$provincesSelect = self::getCanadianProvinces();

		$tplStep1->get( 'countryCode')->options = $countriesSelect;
		$tplStep1->get( 'countryCode')->value = reset($countriesSelect);
		$tplStep1->get( 'state' )->options = $statesSelect;
		$tplStep1->get( 'state' )->value = reset($statesSelect);
		$tplStep1->get( 'province')->options = $provincesSelect;
		$tplStep1->get( 'province')->value = reset($provincesSelect);

		$logoLabel = $tplStep1->get( 'logoLabel' );
		$logoImage = $tplStep1->get( 'logoImage' );
		$logoLabel->visible = TRUE;
		$logoImage->visible = FALSE;
		$this->get( 'imgEditor' )->showLoader = false;
        $this->get( 'imgEditor' )->uploads = [
			$tplStep1->get( 'btnUploadLogo' )->uniqueID => [
                'callBack' => '->uploadLogo',
                'maxSize'  => 1024*1024*15, // 15MB
                'extension' => ['gif', 'png', 'jpg', 'jpeg', 'pbm', 'tga', 'dds', 'ico', 'bmp', 'svg'],
                'cropWidth' => 344,
                'cropHeight' => 114
			]
		];

		if( $this->isEdit ) {
			//STEP1
			$sponsor = $this->client->parentClients1[0];
			$this->tpl->deactivated = $this->client->deactivated;
			$tplStep1->get( 'deactivated' )->value = $this->client->deactivated ? '1' : '0';
			$tplStep1->get( 'name' )->value = $this->client->name;
			$tplStep1->get( 'sponsoringReseller' )->value = $sponsor->name;
			$tplStep1->get( 'salesRep' )->value = $this->client->salesRep;
			$tplStep1->get( 'location' )->value = $this->client->location;
			$tplStep1->get( 'startDate' )->value = date( 'm/d/Y', strtotime( $this->client->startDate ) );
			$tplStep1->get( 'contactName' )->value = $this->client->contactName;
			$tplStep1->get( 'contactPhone' )->value = $this->client->contactPhone = \ClickBlocks\Utils::formatPhone($this->client->contactPhone);
			$tplStep1->get( 'contactEmail' )->value = $this->client->contactEmail;
			$tplStep1->get( 'address1' )->value = $this->client->address1;
			$tplStep1->get( 'address2' )->value = $this->client->address2;
			$tplStep1->get( 'city' )->value = $this->client->city;
			$tplStep1->get( 'ZIP' )->value = $this->client->ZIP;
			$tplStep1->get( 'postCode' )->value = $this->client->ZIP;
			$tplStep1->get( 'state' )->value = $this->client->state;
			$tplStep1->get( 'province' )->value = $this->client->state;
			$tplStep1->get( 'region' )->value = $this->client->region;
			$tplStep1->get( 'countryCode' )->value = $this->client->countryCode;

			$tplStep1->get( 'description' )->text = $this->client->description;
			$tplStep1->get( 'bannerColor' )->value = $this->client->getBannerColor();
			$tplStep1->tpl->textcolor = $tplStep1->get( 'bannerTextColor' )->value = $this->client->getBannerTextColor();
			$tplStep1->get( 'URL' )->value = mb_strtolower( $this->client->URL );
			if( $this->client->logo && file_exists( Core\IO::dir( 'logos' ) . '/' . $this->client->logo ) ) {
				$tplStep1->get( 'logoName' )->value = $this->client->logo;
				$logoImage->src = Core\IO::url( 'logos' ) . '/' . $this->client->logo;
				$logoLabel->visible = FALSE;
				$logoImage->visible = TRUE;
			}
			if( $this->isEdepozeAdmin ) {
				$tplStep1->get( 'name' )->disabled = TRUE;
				$tplStep1->get( 'startDate' )->disabled = TRUE;
				$tplStep1->get( 'location' )->disabled = TRUE;
				$tplStep1->get( 'contactName' )->disabled = TRUE;
				$tplStep1->get( 'contactEmail' )->disabled = TRUE;
				$tplStep1->get( 'contactPhone' )->disabled = TRUE;
				$tplStep1->get( 'address1' )->disabled = TRUE;
				$tplStep1->get( 'address2' )->disabled = TRUE;
				$tplStep1->get( 'city' )->disabled = TRUE;
				$tplStep1->get( 'state' )->disabled = TRUE;
				$tplStep1->get( 'ZIP' )->disabled = TRUE;
				$tplStep1->get( 'description' )->disabled = TRUE;
				$tplStep1->get( 'URL' )->disabled = TRUE;
			}

			//STEP2
			$tplStep2->get( 'firstName' )->value = $this->clientUser->firstName;
			$tplStep2->get( 'lastName' )->value = $this->clientUser->lastName;
			$tplStep2->get( 'email' )->value = $this->clientUser->email;
            $tplStep2->get( 'username' )->value = $this->clientUser->username;
			$tplStep2->get( 'valpassword' )->groups = 'ignore';
            $tplStep2->get( 'valconfirmpassword' )->groups = 'ignore';
		}
    }

	public function openTab( $step )
	{
        $this->ajax->script( "controls.display('" . $this->get( 'step1' )->uniqueID . "', 'none');" );
        $this->ajax->script( "controls.display('" . $this->get( 'step2' )->uniqueID . "', 'none');" );
        $this->ajax->script( "controls.display('" . $this->get( 'step' . $step )->uniqueID . "', '');" );
    }

	public function toStep1( $fv )
	{
		$this->ajax->script( "controls.display('" . $this->get( 'step2' )->uniqueID . "', 'none');" );
		$this->ajax->script( "controls.display('" . $this->get( 'step1' )->uniqueID . "', '');" );
		$this->ajax->script( "changeTab( 0, 1 )" );
	}

	public function toStep2( $fv )
	{
		$isValid = TRUE;
		$isValid &= $this->validators->isValid( 'step1' );

		switch( $this->get('step1')->get('countryCode')->value ) {
			case 'US':
				$isValid &= $this->validators->isValid( 'countryUS' );
				break;
			case 'CA':
				$isValid &= $this->validators->isValid( 'countryCA' );
				break;
			case 'OT':
				$isValid &= $this->validators->isValid( 'countryOT' );
				break;
		}

		if( !$isValid ) return;

		$this->ajax->script( "controls.display('" . $this->get( 'step1' )->uniqueID . "', 'none');" );
		$this->ajax->script( "controls.display('" . $this->get( 'step2' )->uniqueID . "', '');" );
		$this->ajax->script( "changeTab( 0, 2 )" );
	}

	public function checkUrl( $ctrls )
	{
		if( !empty( $this->client->URL ) && $this->client->URL == $this->getByUniqueID( $ctrls[0] )->value ) {
			return true;
		}
		return $this->getOrchestraClients()->checkUniqueUrl( $this->getByUniqueID( $ctrls[0] )->value );
	}

	public function checkUsername( $ctrls )
	{
		return $this->getOrchestraUsers()->checkUniqueUsername( $this->getByUniqueID( $ctrls[0] )->value, $this->clientUser->ID );
    }

	public function uploadLogo( $uniqueID )
	{
		$info = Utils\UploadFile::getInfo( $uniqueID );
		$logoImage = $this->get( 'step1.logoImage' );
		$logoName = $this->get( 'step1.logoName' );
		$logoLabel = $this->get( 'step1.logoLabel' );
		$logoImage->src = $info['url'] . '?' . microtime();
		$logoName->value = $info['name'];
		$this->ajax->script( "controls.display('" . $logoLabel->uniqueID . "', 'none');" );
		$this->ajax->script( "controls.display('" . $logoImage->uniqueID . "', '');" );
		$logoLabel->visible = FALSE;
		$logoImage->visible = TRUE;
	}

	public function save()
	{
		$this->ajax->script( 'btnLock = false' );

		$fv['name'] = $this->get( 'step1.name' )->value;
		$fv['URL'] = mb_strtolower( $this->get( 'step1.URL' )->value );
		$fv['username'] = $this->get( 'step2.username' )->value;

		$isValid = TRUE;
		$isValid &= $this->validators->isValid( 'step1' );
		$isValid &= $this->validators->isValid( 'step2' );

		switch( $this->get('step1')->get('countryCode')->value ) {
			case 'US':
				$isValid &= $this->validators->isValid( 'countryUS' );
				break;
			case 'CA':
				$isValid &= $this->validators->isValid( 'countryCA' );
				break;
			case 'OT':
				$isValid &= $this->validators->isValid( 'countryOT' );
				break;
		}

		if( !$isValid ) {
			return;
		}

		$now = date( 'Y-m-d H:i:s' );
		$client = $this->client;
		$client->typeID = self::CLIENT_TYPE_ENT_CLIENT;
		$client->casesDemoDefault = 1;

		if( !$this->isEdit ) {
			$client->created = $now;
		}

		$client->name = $this->get( 'step1.name' )->value;
		$client->salesRep = $this->get( 'step1.salesRep' )->value;
		$client->startDate = date( 'Y-m-d', strtotime( $this->get('step1.startDate' )->value ) );
		$client->deactivated = $this->get( 'step1.deactivated' )->value == '1' ? $now : NULL;
		$client->location = trim( $this->get( 'step1.location' )->value ) == FALSE ? NULL : trim( $this->get( 'step1.location' )->value );
		$client->contactName = $this->get( 'step1.contactName' )->value;
		$client->contactPhone = $this->get( 'step1.contactPhone' )->value;
		$client->contactEmail = $this->get( 'step1.contactEmail' )->value;
		$client->URL = !( trim( $this->get( 'step1.URL' )->value ) ) ? NULL : mb_strtolower( trim( $this->get( 'step1.URL')->value ) );
		$client->countryCode = $this->get('step1.countryCode')->value;
		$client->address1 = $this->get( 'step1.address1' )->value;
		$client->address2 = $this->get( 'step1.address2' )->value;
		$client->city = $this->get( 'step1.city' )->value;
		$client->state = $this->get( 'step1.state' )->value;
		$client->region = $this->get('step1.region')->value;
		$client->ZIP = $this->get( 'step1.ZIP' )->value;
		$client->description = $this->get( 'step1.comments' )->text;
		$client->setBannerColor( $this->get( 'step1.bannerColor' )->value );
		$client->setBannerTextColor( $this->get( 'step1.bannerTextColor' )->value );

		switch( $this->get('step1.countryCode')->value ) {
			case 'US':
				$client->region = '';
				break;
			case 'CA':
				$client->state = $this->get('step1.province')->value;
				$client->ZIP = $this->get('step1.postCode')->value;
				$client->region = '';
				break;
			case 'OT':
				$client->state = '';
				$client->ZIP = $this->get('step1.postCode')->value;
				break;
		}

		$destDir = Core\IO::dir( 'logos' );
		if( !$this->isEdit ) {
			$client->resellerID = $this->reseller['ID'];
			$client->deactivated = $now;
			Core\IO::createDirectories( $destDir );
			$client->logo = $this->get( 'step1.logoName' )->value;
			if( $client->logo ) {
				rename2( Core\IO::dir( 'temp' ) . '/' . $client->logo, $destDir . '/' . $client->logo );
			}
		} else {
			if( $client->logo != $this->get( 'step1.logoName' )->value ) {
				if( is_file( $destDir . '/' . $client->logo ) ) {
					unlink( $destDir . '/' . $client->logo );
				}
				$client->logo = $this->get( 'step1.logoName' )->value;
				if( $client->logo ) {
					rename2( Core\IO::dir( 'temp' ) . '/' . $client->logo, $destDir . '/' . $client->logo );
				}
			}
		}
		$client->save();

		if( !$this->isEdit ) {
			// email superadmin about enterprise client activation
			Utils\EmailGenerator::sendEnterpriseAdminNotification($client, $this->user, $this->getOrchestraUsers());
		}

        // Data from step 2
        $this->clientUser->email = $this->get( 'step2.email' )->value;
        $this->clientUser->username = $this->get( 'step2.username' )->value;
        $this->clientUser->firstName = $this->get( 'step2.firstName' )->value;
		$this->clientUser->lastName = $this->get( 'step2.lastName' )->value;
		$this->clientUser->countryCode = $client->countryCode;
		$this->clientUser->state = $client->state;
		$this->clientUser->city = $client->city;
		$this->clientUser->ZIP = $client->ZIP;
        $this->clientUser->typeID = self::USER_TYPE_CLIENT;
        if( !$this->isEdit ) {
			$this->clientUser->created = $now;
			$this->clientUser->clientID = $client->ID;
			$this->clientUser->insert();
			$this->client->chargeUponCreation();
        } else {
            $this->clientUser->update();
        }

		// Do we want a demo case created?
		if( $client->typeID == 'EC' && !$client->hasDemoCase() ) {
			// Add a demo case for the new client
			$demoCase = new \ClickBlocks\DB\Cases();
			$demoCase->createDemoCase( $client->ID, $this->clientUser->ID );
			$demoCase->save();

			// Add a demo deposition for the new demo case
			$demoDepo = new \ClickBlocks\DB\Depositions();
			$demoDepo->createDemoDeposition( $demoCase, $this->user->ID );
		}

		$password = $this->get( 'step2.passauth' )->value;
        if( $password ) {
			\ClickBlocks\DB\UserAuth::updatePassword( $this->clientUser->ID, $password, TRUE );
		}
        if( $this->user->typeID == self::USER_TYPE_RESELLER ) {
            $this->ajax->redirect( '/'. $this->resellerURL . '/clients' );
		} else {
			$this->ajax->redirect( $this->config->adminPath . '/clients' );
		}
    }

	public function confirmDeleteAccount()
	{
		$client = $this->client;
		if( !$client->ID ) {
			return;
		}
		$param = [
			'title' => 'Delete Client',
			'message' => 'Are you sure you would like to delete this client?',
			'OKName' => 'Delete',
			'OKMethod' => "deleteAccount(1);"
		];
		$this->showPopup( 'confirm', $param );
	}

	public function deleteAccount()
	{
		// Hide dialog popup
		$this->hidePopup( 'confirm' );
		$this->client->setAsDeleted();
		$sendTo = ($this->user->typeID == self::USER_TYPE_RESELLER) ? $this->basePath . '/clients' : $this->config->adminPath . '/resellers';
		$this->ajax->redirect( $sendTo );
	}
}