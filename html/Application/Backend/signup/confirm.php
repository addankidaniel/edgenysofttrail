<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\Debug;

class PageSignupConfirm extends Backend {

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $account;
	protected $email;

	public function __construct() {
		parent::__construct( 'signup/confirm.html' );
		$a = filter_input( INPUT_GET, 'a', FILTER_SANITIZE_STRING );
		if( !$a ) {
			Debug::ErrorLog( "PageSignupConfirm; Invalid input: {$a}" );
			return;
		}
		$email = base64_decode( $a );
		if( $email && mb_strlen( $email ) > 3 ) {
			$this->email = $email;
			$user = $this->getOrchestraUsers()->getUnactivatedUser( $email );
			if( !$user || !$user->ID ) {
				Debug::ErrorLog( "PageSignupConfirm; user not found by email: {$email}" );
				return;
			}
			$this->account = $user;
		} else {
			Debug::ErrorLog( "PageSignupConfirm; Invalid input: {$a} email: {$email}" );
		}
	}

	public function access() {
		$this->noAccessURL = '/';
		if( !$this->config->signup['resellerID'] ) {
			Debug::ErrorLog( "PageSignupConfirm; sign up is disabled" );
			return FALSE;
		}
		return ($this->account && $this->account->username == $this->email);
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( NULL, NULL, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->css->add( new Helpers\Style( 'signup-general', NULL, Core\IO::url( 'backend-css' ) . '/signup-general.css' ), 'link' );
		$this->head->addMeta( new Helpers\Meta( 'viewport', 'viewport', 'width=device-width, initial-scale=1.0' ) );
		$this->head->name = 'Confirm';
		$this->tpl->hasAccount = ($this->account->ID);
		$this->tpl->isActivated = ($this->account->activated === 0);
	}

	public function confirm( $fv ) {
		array_walk( $fv, 'trim' );
		$isValid = $this->validators->isValid( 'signup' );
		if( !$isValid ) {
			Debug::ErrorLog( 'PageSignupConfirm::confirm; failed form validation' );
			return;
		}
		DB\UserAuth::updatePassword( $this->account->ID, $fv['password'] );
		$this->account->activated = 1;
		$this->account->update();

		$dlg = $this->get( 'confirmDialog' );
		$dlg->tpl->frameTitle = '';
		$dlg->tpl->title = '<strong>Your account is ready to use.</strong>';
		$dlg->tpl->message = '<div class="t_a_c">You may now login using your eDepoze&trade; account.</div>';
		$dlg->get( 'btnOK' )->visible = FALSE;
		$dlg->get( 'btnCancel' )->value = 'OK';
		$dlg->get( 'btnCancel' )->onclick = 'ajax.doit("->confirmComplete");';
		$dlg->show();
	}

	public function confirmComplete() {
		$dlg = $this->get( 'confirmDialog' );
		$dlg->hide();
		$this->ajax->redirect( '/' );
	}
}
