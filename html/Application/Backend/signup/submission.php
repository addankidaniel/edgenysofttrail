<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Core;

class PageSignupSubmission extends Backend {

	public function __construct() {
        parent::__construct( 'signup/submission.html' );
    }

    public function access() {
		$this->noAccessURL = '/';
		if( !$this->config->signup['resellerID'] ) {
			return FALSE;
		}
		return !isset( $this->reg->reseller );
    }

    public function init() {
        parent::init();
		$this->css->add( new Helpers\Style( 'signup-general', NULL, Core\IO::url( 'backend-css' ) . '/signup-general.css' ), 'link' );
		$this->head->addMeta( new Helpers\Meta( 'viewport', 'viewport', 'width=device-width, initial-scale=1.0' ) );
		$this->head->name = 'Thank you';
	}
}
