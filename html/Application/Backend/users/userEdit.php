<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

class PageUserEdit extends Backend {

	/**
	 * @var \ClickBlocks\DB\Users
	 */
	protected $userC = NULL;

	protected $clientTypeID = NULL;

	public function __construct() {
		parent::__construct( 'users/userEdit.html' );
		$ID = isset( $this->fv['ID'] ) ? (int)$this->fv['ID'] : 0;
		$this->userC = new DB\Users( $ID );
		if( !$this->userC->ID ) {
			if( $ID > 0 ) {
				$this->userC = NULL;
				$this->redirect( $this->basePath . '/users/add' );
			}
		} else {
			if( $this->user->clientID !== $this->userC->clientID || $this->user->ID === $this->userC->ID ) {
				$this->userC = NULL;
				$this->redirect( $this->basePath . '/users' );
			}
		}
		$this->clientTypeID = $this->user->clients[0]->typeID;
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		if( $this->uri->query['ID'] > 0 ) {
			return (($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin) && $this->user->clientID == $this->userC->clientID);
		} else {
			return ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
		}
	}

	public function init() {
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/country_toggle.js' ), 'link' );
		$this->tpl->showTabs = FALSE;
		$this->tpl->isNew = TRUE;
		$this->tpl->clientTypeID = $this->clientTypeID;
		$this->tpl->isClientAdmin = $this->userC->clientAdmin;

		if( $this->userC->ID > 0 ) {
			$this->head->name = 'User Profile';
			$this->tpl->htitle = $this->userC->firstName . ' ' . $this->userC->lastName;
			$this->tpl->isNew = FALSE;
			$this->tpl->ID = $this->userC->ID;

			$formValues = $this->userC->getValues();
			$formValues['province'] = $formValues['state'];
			$formValues['postCode'] = $formValues['ZIP'];

			$this->get('mainform')->assign($formValues);
			$phone = $this->get('phone');
			$phone->value = \ClickBlocks\Utils::formatPhone($phone->value);
			$ct = $this->get('valFields')->controls;
			$this->get('valFields')->controls = array_diff($ct, array($this->get('password')->uniqueID, $this->get('confirmPassword')->uniqueID));
			$this->tpl->isAccountLocked = ((new DB\ServiceUserAuth())->getByUserID( $this->userC->ID )->hardLock !== null);
		} else {
			$this->head->name = $this->tpl->htitle = 'Add new User';
		}
	}

	public function checkUsername($ctrls) {
		return (new DB\OrchestraUsers)->checkUniqueUsername($this->getByUniqueID($ctrls[0])->value, $this->userC->ID);
	}

	public function save(array $params)
	{
		if (!$this->validators->isValid('reg')) return;

		switch( $vs['clientCountryCode'] ) {
			case 'US':
				if( !$this->validators->isValid('countryUS') ){
					return;
				}
				break;
			case 'CA':
				if( !$this->validators->isValid('countryCA') ){
					return;
				}
				break;
			case 'OT':
				if( !$this->validators->isValid('countryOT') ){
					return;
				}
				break;
		}

		# $client->name = $this->get('step1')->get('name')->value;
		$this->userC->typeID = \ClickBlocks\MVC\Edepo::USER_TYPE_USER;
		if( $this->tpl->isNew ) {
			$this->userC->clientID = $this->user->clientID;
		}

		if( !$params['region'] ) {
			$params[region] = '';
		}
		else {
			$params['state'] = '';
			$params['province'] = '';
		}
		if( isset( $params['postCode'] ) ) {
			$params['ZIP'] = $params['postCode'];
		}
		if( $params['province'] ) {
			$params['state'] = $params['province'];
		}

		$this->userC->firstName = $params['firstName'];
		$this->userC->lastName = $params['lastName'];
		$this->userC->affiliateCode = $params['affiliateCode'];
		$this->userC->address1 = $params['address1'];
		$this->userC->address2 = $params['address2'];
		$this->userC->city = $params['city'];
		$this->userC->state = $params['state'];
		$this->userC->ZIP = $params['ZIP'];
		$this->userC->countryCode = $params['countryCode'];
		$this->userC->region = $params['region'];
		$this->userC->phone = $params['phone'];
		$this->userC->email = $params['email'];
		$this->userC->username = $params['username'];
		$this->userC->clientAdmin = $params['clientAdmin'];
		$this->userC->save();

		$password = $this->get('passauth')->value;
		if( $password ) {
			DB\UserAuth::updatePassword( $this->userC->ID, $password, true );
		}

		/* ED-1426: Do NOT make user case manager of demo case
		// Make user manager of any demo client cases.
		$clientObj = new DB\OrchestraClients();
		$clientDemoCases = $clientObj->getClientDemoCases( $this->userC->clientID );
		if( $clientDemoCases ){
			$CMObj = new DB\ServiceCaseManagers();

			foreach( $clientDemoCases as $case ){
				// Make the user a case manager
				$CMObj->setCM( $case['ID'], $this->userC->ID );
			}
		}
		*/

  		$this->ajax->redirect($this->basePath.'/users');
 	}

 	public function deleteUser($ID)
 	{
 	  $user = $this->userC;
 	  if ((int)$user->ID == 0) return;
 	  $this->get('confirmPopup')->hide();
 	  $user->delete();
 	  $this->ajax->redirect($this->basePath.'/users');
 	}

 	public function showDelPopup($ID = null)
 	{
 	  $user = (new DB\ServiceUsers())->getByID($ID);
 	  $popup = $this->get('confirmPopup');
 	  $this->cleanByUniqueID($popup->uniqueID);
 	  $popup->get('btnOK')->value = 'Delete';
 	  $popup->get('btnOK')->onclick = 'ajax.doit(\'->deleteUser\', \'' . (int)$user->ID . '\');';
 	  $popup->tpl->title = 'Delete User?';
 	  $popup->tpl->message = 'Are you sure you want to delete <b>' . $user->firstName . ' ' . $user->lastName.'</b>?';
 	  $popup->show();
 	}

	public function validateUsername( $ctrls, $mode )
	{
		$ctUsername = $this->getByUniqueID( $ctrls[0] )->value;
		return preg_match( '/^demo_/i', $ctUsername );
	}

	public function validatePassword( $ctrls )
	{
		$password = $this->getByUniqueID( $ctrls[0] )->value;
		if (strlen( $password ) < 1)
		{
			return true;
		}
		return DB\UserAuth::validatePassword( $password );
	}

	public function unlock()
	{
		DB\UserAuth::unlockAccount( $this->userC->ID );
		$this->ajax->redirect( $this->basePath.'/users' );
	}
}
?>
