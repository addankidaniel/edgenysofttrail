<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\DB;

class PageUsers extends Backend {

	public function __construct() {
	   parent::__construct( 'users/users.html' );
	}

	public function access() {
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/';
		return ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
	}

	public function init() {
		parent::init();
		$this->tpl->tab = 2;
		$this->head->name = 'User Management';
		$usersWidget = $this->get( 'users' );
		$usersWidget->clientID = $this->user->clientID;
	}

	public function showDelPopup( $ID=NULL ) {
		$user = new DB\Users( $ID );
		$popup = $this->get( 'confirmPopup' );
		$this->cleanByUniqueID( $popup->uniqueID );
		$popup->get( 'btnOK' )->value = 'Delete';
		$popup->get( 'btnOK' )->onclick = 'ajax.doit(\'->deleteUser\', \'' . (int)$user->ID . '\');';
		$popup->tpl->title = 'Delete User?';
		$popup->tpl->message = 'Are you sure you want to delete <b>' . $user->firstName . ' ' . $user->lastName.'</b>?';
		$popup->show();
	}

	public function deleteUser( $ID ) {
		$user = new DB\Users( $ID );
		if( (int)$user->ID == 0 ) {
			return;
		}
		$this->get( 'confirmPopup' )->hide();
		$user->delete();
		$this->ajax->redirect( $this->basePath.'/users' );
	}
}
