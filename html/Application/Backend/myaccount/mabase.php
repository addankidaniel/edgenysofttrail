<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class BaseMyAccount extends Backend
{
	const TAB_LOGIN = 1;
	const TAB_PRICING = 2;
	const TAB_BRANDING = 3;
	const TAB_DATASOURCE = 4;
	const TAB_RESELLER_PRICING = 5;
	const TAB_RESELLER_FEES = 6;

	protected $pricingModel = [];

	public function __construct()
	{
		$tpl_postfix = $this->isAdminBackend() ? 'admin' : 'reseller';
		parent::__construct( "myaccount/myaccount_{$tpl_postfix}.html" );
	}

	public function init()
	{
		parent::init();
		$this->tpl->tab = 4;
		$this->head->name = 'My Account';
		$client = $this->getService( 'Clients' )->getByID( $this->user->clientID );
		if( $client ) {
			$this->setMyAccountTabs( $client->typeID );
		}
		$this->tpl->mpanels = [
			self::TAB_LOGIN => [
				'id' => 'accountPnl',
				'url' => Core\IO::dir( 'backend' ) . '/myaccount/myaccount.html'
			],
			self::TAB_PRICING => [
				'id' => 'pricingPnl',
				'url' => Core\IO::dir( 'backend' ) . '/myaccount/pricing.html'
			],
			self::TAB_BRANDING => [
				'id' => 'brandingPnl',
				'url' => Core\IO::dir( 'backend' ) . '/myaccount/branding.html'
			],
			self::TAB_DATASOURCE => [
				'id' => 'datasourcePnl',
				'url' => Core\IO::dir( 'backend' ) . '/myaccount/datasource.html'
			],
			self::TAB_RESELLER_PRICING => [
				'id' => 'rPricingPnl',
				'url' => Core\IO::dir( 'backend' ) . '/myaccount/reseller_pricing.html'
			],
			self::TAB_RESELLER_FEES => [
				'id' => 'rFeesPnl',
				'url' => Core\IO::dir( 'backend' ) . '/myaccount/reseller_fees.html'
			]
		];
	}

	public function setMyAccountTabs( $type )
	{
		switch( $type ) {
			case \ClickBlocks\MVC\Edepo::CLIENT_TYPE_RESELLER:
				$this->tpl->matabs = [
					self::TAB_LOGIN => ['Login Information', '/myaccount/login'],
					self::TAB_BRANDING => ['Branding', '/myaccount/branding'],
					self::TAB_RESELLER_PRICING => ['Pricing Package', '/myaccount/pricing']
				];
				break;
			case \ClickBlocks\MVC\Edepo::CLIENT_TYPE_CLIENT:
				$this->tpl->matabs = [
					self::TAB_LOGIN => ['Login Information', '/myaccount/login'],
					self::TAB_DATASOURCE => ['Datasources', '/myaccount/datasource'],
				];
				break;
			case \ClickBlocks\MVC\Edepo::CLIENT_TYPE_ENT_CLIENT:
				$this->tpl->matabs = [
					self::TAB_LOGIN => ['Login Information', '/myaccount/login'],
					self::TAB_BRANDING => ['Branding', '/myaccount/branding'],
					self::TAB_DATASOURCE => ['Datasources', '/myaccount/datasource']
				];
				break;
			default:
				break;
		 }
	}

	public function checkEmail($ctrls) {
		if (!empty($this->user->email) && $this->user->email == $this->getByUniqueID($ctrls[0])->value) {
			return true;
		}
		return foo(new DB\OrchestraUsers)->checkUniqueEmail($this->getByUniqueID($ctrls[0])->value);
	}

	public function resetMyAccount()
	{
		$this->get('email')->value = '';
		$this->get('password')->value = '';
		$this->get('confirmPassword')->value = '';
		$this->ajax->redirect('/'.implode('/', $this->uri->path));
	}

	protected function savePricingModelOption( $clientID, $optionID, $price, $typeID=self::PRICING_TYPE_MONTHLY )
	{
		$price = floatval( $price[0] == '$' ? substr( $price, 1 ) : $price );
		$pricingOption = new \ClickBlocks\DB\PricingModelOptions( $clientID, $optionID );
		if( $pricingOption->price == $price && $pricingOption->typeID == $typeID ) {
			return;
		}
		$pricingOption->price = $price;
		$pricingOption->typeID = $typeID;
		if( !$pricingOption->clientID || !$pricingOption->optionID ) {
			$pricingOption->clientID = $clientID;
			$pricingOption->optionID = $optionID;
			$pricingOption->insert();
		} else {
			$pricingOption->update();
		}
    }
}
