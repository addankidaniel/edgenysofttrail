<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageMyAccountPricing extends BaseMyAccount
{
    protected $client = null;

	public function init()
    {
        parent::init();
        $this->tpl->mtab = self::TAB_PRICING;
        $this->head->name = 'My Account';

		$pnl = $this->get('pricingPnl');
		$pnl->setTemplate(Core\IO::dir('backend') . '/myaccount/pricing.html');

        $client = (new DB\ServiceClients())->getByID( $this->user->clientID );
        $clientID = $client->ID;

		$this->tpl->isEnterprise = $isEnterprise = ($client->typeID === self::CLIENT_TYPE_ENT_CLIENT);
		$reseller = (new DB\ServiceClients())->getByID( $client->resellerID );
		$contactPhone = \ClickBlocks\Utils::formatPhone( $reseller->contactPhone );

        $rowPricingModelOptions = foo(new DB\OrchestraPricingModelOptions())->getByClient($clientID);
        $this->tpl->subscriptionFee = '$'.$this->getPriceFromModel($rowPricingModelOptions, self::PRICING_OPTION_SUBSCRIPTION);

		$this->tpl->billingContact = ($isEnterprise) ? 'billing@edepoze.com' : $contactPhone;
    }

	private function getPriceFromModel(array $arrOptions, $optionID)
	{
        foreach ($arrOptions as $row) {
            if ($row['optionID'] == $optionID) return $row['price'];
        }
        return 0.00;
    }

    private function getTermFromModel(array $arrOptions, $optionID)
	{
        $map = array('O'=>'One Time', 'M'=>'Monthly');
        foreach ($arrOptions as $row) {
            if ($row['optionID'] == $optionID) return $map[$row['typeID']];
        }
        return $map[self::PRICING_TYPE_MONTHLY];
    }
}
?>

