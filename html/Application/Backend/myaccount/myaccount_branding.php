<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

class PageMyAccountBranding extends BaseMyAccount
{
	/**
	 * @var \ClickBlocks\DB\Clients
	 */
  protected $client = NULL;

  public function __construct()
  {
    parent::__construct();
    $this->client = foo(new DB\ServiceClients())->getByID($this->user->clientID);
  }

	public function access()
    {
	    if (!parent::access()) {
            return false;
		}
        $this->noAccessURL = $this->basePath . '/';

		return ($this->user->typeID == self::USER_TYPE_RESELLER || $this->client->typeID == self::CLIENT_TYPE_ENT_CLIENT);
    }

  public function init()
  {
    parent::init();
	$this->tpl->mtab = self::TAB_BRANDING;
    $this->head->name = 'My Account';
    $pnl = $this->get('brandingPnl');
    $pnl->setTemplate(Core\IO::dir('backend') . '/myaccount/branding.html');
	$pnl->tpl->isEnterprise = ($this->client->typeID == self::CLIENT_TYPE_ENT_CLIENT);

    $pnl->get('bannerColor')->value = $pnl->tpl->rcolor = $this->client->getBannerColor();
    $pnl->get('bannerTextColor')->value = $pnl->tpl->textcolor = $this->client->getBannerTextColor();
    if ($this->client->logo && file_exists(Core\IO::dir('logos'). '/' .  $this->client->logo))
        $pnl->get('logoImage')->src = Core\IO::url('logos'). '/' .  $this->client->logo;
   else
        $pnl->get('logoImage')->src = Core\IO::url('backend-i'). '/' .  'logo_header.png';
	$pnl->get('liveTranscriptsEmail')->value = $pnl->tpl->liveTranscriptsEmail = $this->client->liveTranscriptsEmail;
    $this->get('imgEditor')->showLoader = false;
    $this->get('imgEditor')->uploads = array($pnl->get('btnUploadLogo')->uniqueID => array(
            'callBack' => '->uploadLogo',
            'maxSize'  => 1024*1024*100, // 100 Mb
            'extension' => array('gif','png','jpg', 'jpeg', 'pbm', 'tga', 'dds', 'ico', 'bmp', 'svg'),
            'cropWidth' => 344,
            'cropHeight' => 114));
  }

  public function uploadLogo($uniqueID) {
    $info = Utils\UploadFile::getInfo($uniqueID);
    $this->get('brandingPnl')->get('img_name')->value = $info['name'];
    $this->get('brandingPnl')->get('logoImage')->src = $info['url'].'?'.  microtime();
  }

	public function save()
	{
		$filename = '';
		$pnl = $this->get('brandingPnl');
		$vals = $pnl->getFormValues();
		$blogo = $pnl->get('logoImage')->src;
		$ext = explode('.', $pnl->get('img_name')->value);

		$txEmail = $this->client->liveTranscriptsEmail;

		$this->client->courtReporterEmail = $this->get( 'courtReporterEmail' )->value;
		$this->client->liveTranscriptsEmail = $this->get( 'liveTranscriptsEmail' )->value;

		if ($ext[1] != '')
		{
			$destination = Core\IO::dir('logos');
			$from = explode('?',Core\IO::dir($blogo));

			Core\IO::createDirectories($destination);
			$filename = md5(microtime(1)). '.' .$ext[1];
			if (is_file($destination . '/' . $this->client->logo))
				unlink($destination . '/' . $this->client->logo);
			rename2($from[0], $destination . '/' . $filename);
			$this->client->logo = $filename;
		}
		$this->client->setBannerColor($vals['bannerColor']);
		$this->client->setBannerTextColor($vals['bannerTextColor']);

		foo(new DB\ServiceClients())->save($this->client);

		// ED-2661; live transcript email changed, propigate to any not set
		if( $txEmail != $this->client->liveTranscriptsEmail ) {
			$this->client->propigateLiveTranscriptsEmail();
		}

		$msg = $this->get('confirmPopup');
		$msg->get('btnOK')->visible = '0';
		$msg->get('btnCancel')->visible = '0';
		$msg->tpl->title = 'My Account';
		$msg->tpl->message = 'Your branding information has been successfully updated.';
		$msg->show(0.5, true, 0, 3000, 0, false);
		$this->ajax->redirect('/'.implode('/', $this->uri->path));
	}

	public function resetMyAccount()
    {
        $this->ajax->redirect('/'.implode('/', $this->uri->path));
    }
}
?>

