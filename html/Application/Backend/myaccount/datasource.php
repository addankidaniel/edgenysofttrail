<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageMyAccountDatasource extends BaseMyAccount
{
	protected $client = NULL;
	protected $page = NULL;

	public function __construct()
	{
//		$this->expired = 86400;	//disable cache
		parent::__construct();
	}

	public function init()
	{
		parent::init();
		$this->tpl->mtab = self::TAB_DATASOURCE;
		$this->head->name = 'My Account';
		$pnl = $this->get('datasourcePnl');
		$pnl->setTemplate(Core\IO::dir('backend') . '/myaccount/datasource.html');
		// Since we are loading a popup late, we need to call the init on it.
		$dsPopup = $pnl->get('datasourcePopup');
		$dsPopup->init();
		$dsWidget = $pnl->get('datasourceList');
		$dsWidget->init();
	}

	public function showAddDatasource()
	{
		$this->tpl->row = [];
		$action = 'Add';
		$dlg = $this->get( 'datasourcePopup' );
		$dlg->get( 'vendor' )->value = 'Relativity';
		$dlg->get( 'name' )->value = '';
		$dlg->get( 'uri' )->value = '';
		$dlg->get( 'btnOK' )->value = $this->tpl->action = $action;
		$dlg->get( 'btnOK' )->visible = TRUE;
		$dlg->get( 'btnOK' )->onclick = 'addDatasource()';
		$dlg->show();
	}

	public function addDatasource( $fv )
	{
		$datasource = new \ClickBlocks\DB\Datasources();
		$datasource->clientID = $this->user->clientID;
		$datasource->vendor = $fv['vendor'];
		$datasource->name = $fv['name'];
		$datasource->URI = $fv['uri'];
		$datasource->insert();
		if( $datasource->ID ) {
			$this->get('datasourcePopup')->hide();
			$this->get('datasourceList')->rows = $this->getOrchestraDatasources()->getDatasources( $this->user->clientID );
			$this->clearPageCache();
		}
		return;
	}

	public function showEditDatasource( $id )
	{
		$id = (int)$id;
		$row = $this->getOrchestraDatasources()->getDatasourceByID( $this->user->clientID, $id );
		if( !$row ) return;
		$this->tpl->row = $row;
		$this->tpl->action = 'Edit';
		$dlg = $this->get( 'datasourcePopup' );
		$dlg->get( 'datasourceID' )->value = $row['ID'];
		$dlg->get( 'vendor' )->value = $row['vendor'];
		$dlg->get( 'name' )->value = $row['name'];
		$dlg->get( 'uri' )->value = $row['URI'];
		$dlg->get( 'btnOK' )->value = 'Save';
		$dlg->get( 'btnOK' )->visible = TRUE;
        $dlg->get( 'btnOK' )->onclick = 'updateDatasource()';
        $dlg->show();
	}

	public function updateDatasource( $fv )
	{
		$ID = (int)$fv['datasourceID'];
		$datasource = $this->getOrchestraDatasources()->getDatasourceByID( $this->user->clientID, $ID );
		if( $datasource && $datasource['ID'] ) {
			$svcDatasource = $this->getService( 'Datasources' )->getByID( $ID );
			$svcDatasource->vendor = $fv['vendor'];
			$svcDatasource->name = $fv['name'];
			$svcDatasource->URI = $fv['uri'];
			$result = $svcDatasource->update();
			if( $result ) {
				$this->get('datasourcePopup')->hide();
				$this->get('datasourceList')->rows = $this->getOrchestraDatasources()->getDatasources( $this->user->clientID );
				$this->clearPageCache();
			}
		}
	}

	public function confirmDeleteDatasource( $id )
	{
		$datasource = foo( new DB\ServiceDatasources() )->getByID( $id );
		if( 0 == $datasource->ID ) return;
		$param = array();
		$param['title'] = 'Delete Datasource';
		$param['message'] = 'Are you sure you want to delete this datasource?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteDatasource({$datasource->ID}, 1);";
		$this->showPopup( 'confirm', $param );
	}

    public function deleteDatasource( $id )
	{
		// Hide dialog popup
		$this->hidePopup( 'confirm' );
		$datasource = foo( new DB\ServiceDatasources() )->getByID( $id );
		if( 0 == $datasource->ID ) return;
		$datasource->delete();
		$this->get('datasourceList')->rows = $this->getOrchestraDatasources()->getDatasources( $this->user->clientID );
		$this->clearPageCache();
	}

	public function getWidgetDatasourcesData( $type, array $params )
	{
		//\ClickBlocks\Debug::ErrorLog( print_r( $params, TRUE ) );
		$orchDs = $this->getOrchestraDatasources();
		switch( strtolower( $type ) ) {
			case 'count':
				return $orchDs->countDatasources( $this->user->clientID );
				break;
			default:
			case 'rows':
				$params['clientID'] = $this->user->clientID;
				return $orchDs->getWidgetData( $params );
				break;
		}
	}
}
