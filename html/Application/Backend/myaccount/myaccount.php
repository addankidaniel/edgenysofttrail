<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

class PageMyAccount extends BaseMyAccount
{
	protected $isAdmin = FALSE;
	protected $isCourtClient = FALSE;

	public function __construct()
	{
		parent::__construct();

		if( $this->user->typeID === self::USER_TYPE_SUPERADMIN || $this->user->typeID === self::USER_TYPE_ADMIN ) {
			$this->isAdmin = TRUE;
		}
		$this->isCourtClient = ($this->user->clients[0]->typeID == self::CLIENT_TYPE_COURTCLIENT);
	}

	public function init()
	{
		parent::init();
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/sha1.js' ), 'link' );
		$this->js->add( new Helpers\Script( null, null, Core\IO::url( 'backend-js' ) . '/country_toggle.js' ), 'link' );
		$this->tpl->mtab = self::TAB_LOGIN;
		$this->tpl->isAdmin = $this->isAdmin;
		$this->tpl->isCourtClient = $this->isCourtClient;
		$pnl = $this->get( 'accountPnl' );
		$pnl->setTemplate( Core\IO::dir( 'backend' ) . '/myaccount/myaccount.html' );

		$countriesSelect = self::getCountries(false);
		$statesSelect = self::getUSAStates();
		$provincesSelect = self::getCanadianProvinces();

		$this->get('state')->options = $statesSelect;
		$this->get('state')->value = reset($statesSelect);
		$this->get('countryCode')->options = $countriesSelect;
		$this->get('countryCode')->value = $this->user->countryCode;
		$this->get('province')->options = $provincesSelect;
		$this->get('province')->value = $this->user->state;

		$this->get( 'firstName' )->value = $this->user->firstName;
		$this->get( 'lastName' )->value = $this->user->lastName;
		$this->get( 'address1' )->value = $this->user->address1;
		$this->get( 'address2' )->value = $this->user->address2;
		$this->get( 'city' )->value = $this->user->city;
		$this->get( 'state' )->value = $this->user->state;
		$this->get( 'region' )->value = $this->user->region;
		$this->get( 'ZIP' )->value = $this->user->ZIP;
		$this->get( 'postCode' )->value = $this->user->ZIP;
		$this->get( 'email' )->value = $this->user->email;
		$this->get( 'username' )->value = $this->user->username;
	}

	public function save( array $params )
	{
		$this->ajax->script( 'btnLock = false' );

		$isValid = TRUE;
		$isValid &= $this->validators->isValid( 'reg' );

		if( !$this->isAdmin ) {
			$isValid &= $this->validators->isValid( 'address' );
			switch( $params['countryCode'] ) {
				case 'US':
					$isValid &= $this->validators->isValid( 'countryUS' );
					break;
				case 'CA':
					$isValid &= $this->validators->isValid( 'countryCA' );
					break;
				case 'OT':
					$isValid &= $this->validators->isValid( 'countryOT' );
					break;
			}
		}

		if( !$isValid ){
			return;
		}

		if( !$params['region'] ) {
			$params[region] = '';
		}
		else {
			$params['state'] = '';
			$params['province'] = '';
		}
		if( isset( $params['postCode'] ) ) {
			$params['ZIP'] = $params['postCode'];
		}
		if( $params['province'] ) {
			$params['state'] = $params['province'];
			unset( $params['province'] );
		}

		if( isset( $params['password'] ) ) {
			unset( $params['password'] );
		}

		if( $this->get( 'passauth' )->value ) {
			DB\UserAuth::updatePassword( $this->user->ID, $this->get( 'passauth' )->value, TRUE );
		}

		unset( $params['username'] ); // disabled

		$u = new \ClickBlocks\DB\Users( $this->user->ID );
		$u->setValues( $params );
		$u->save();

		$msg = $this->get( 'confirmPopup' );
		$msg->get( 'btnOK' )->visible = FALSE;
		$msg->get( 'btnCancel' )->visible = FALSE;
		$msg->tpl->title = 'My Account';
		$msg->tpl->message = 'Your profile information has been successfully updated.';
		$msg->show( 0.5, TRUE, 0, 3000, 0, FALSE );
		$this->ajax->redirect( '/' . implode( '/', $this->uri->path ), 3000 );
	}

	public function checkOldPassword( $ctrls )
	{
		$result = (new DB\OrchestraUserAuth())->comparePassword( $this->user->ID, $this->getByUniqueID( $ctrls[0] )->value );
		return (intval( $result ) > 0);
	}

  public function checkUsername($ctrls) {
    $value = $this->getByUniqueID($ctrls[0])->value;
    return foo(new DB\OrchestraUsers)->checkUniqueUsername($value, $this->user->ID);
  }

	public function validatePassword( $ctrls )
	{
		$password = $this->getByUniqueID( $ctrls[0] )->value;
		if (strlen( $password ) < 1)
		{
			return true;
		}
		return DB\UserAuth::validatePassword( $password );
	}
}
?>

