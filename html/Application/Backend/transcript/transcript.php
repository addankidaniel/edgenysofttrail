<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

class PageTranscript extends Backend
{
  protected $deposition;

  public function __construct()
  {
    parent::__construct("transcript/transcript.html");
    $this->deposition = new DB\Depositions($this->subSession['depositionID']);
  }

  public function access() {
    $this->noAccessURL = $this->basePath . '/reporter/login?redir='.urlencode($this->uri->getQueryString());
    return $this->deposition->ID;
  }

  public function logout()
  {
    unset($this->subSession['depositionID']);
    $this->ajax->redirect($this->basePath.'/reporter/login');
  }

  public function init()
  {
    parent::init();
    $this->head->name = 'Transcript upload';
    $this->tpl->showTabs = false;
    $this->tpl->depositionName = $this->deposition->depositionOf;
    $this->tpl->depoID = $this->get('attendees')->depositionID = $this->deposition->ID;
    $file = $this->getOrchestraFiles()->getTranscriptFile($this->deposition->ID);
    $this->tpl->file = $this->get('attendees')->file = $file;
    $this->tpl->isFileExist = $this->get('attendees')->isFileExist = (bool)$file;
    $this->get('sendTranscriptPopup')->show();
  }

  public function uploadFile( $uniqueID )
  {
		$upload = new Utils\UploadFile( microtime() );
		$upload->extensions = ['pdf','txt'];
		$upload->unique = TRUE;
		$upload->mode = Utils\UploadFile::UPLOAD_MODE_TEMP;

		$msg = $this->get( 'noticePopup' );
		$msg->get( 'btnCancel' )->onclick = "window.location='{$this->basePath}/reporter/transcript'";

		$info = $upload->upload( $_FILES[$uniqueID] );
		if( $info === FALSE ) {
			$msg->tpl->title = 'File uploaded is invalid.';
			$msg->tpl->message = '';
		} else {
			if( $info['type'] != 'application/pdf' ) {
				$info['originalName'] = pathinfo( $info['originalName'], PATHINFO_FILENAME ) . '.pdf';
				$converter = new \ClickBlocks\EDPDFConvert( $info['fullname'] );
				$pdfFile = $converter->convert();
				if( $pdfFile && file_exists( $pdfFile ) ) {
					copy( $pdfFile, $info['fullname'] );
					unlink( $pdfFile );
				} else {
					//failed
					return;
				}
			}
			$serviceFiles = $this->getService( 'Files' );
			$orchestraFolders = $this->getOrchestraFolders();
			$folder = $orchestraFolders->getTranscriptFolderObject( $this->deposition->ID, NULL, $this->config->logic['transcriptFolderName'] );
			if( !$folder->ID ) {
				DB\ServiceFolders::createFolder( $this->config->logic['transcriptFolderName'], $this->deposition->createdBy, $this->deposition->ID, DB\Folders::CLASS_TRANSCRIPT );
				$folder = $orchestraFolders->getTranscriptFolderObject( $this->deposition->ID, NULL, $this->config->logic['transcriptFolderName'] );
			}
			if( $this->tpl->file['ID'] ) {
				$serviceFiles->getByID( $this->tpl->file['ID'] )->delete();
			}

			$now = date( 'Y-m-d H:i:s' );
			$file = new DB\Files();
			$file->setSourceFile( $info['fullname'] );
			$file->name = $info['originalName'];
			$file->folderID = $folder->ID;
			$file->createdBy = $folder->createdBy;
			$file->created = $now;
			$file->isExhibit = 1;
			$file->isTranscript = 1;
			$file->insert();

			$folder->lastModified = $now;
			$folder->save();

			$this->tpl->file = $file->getValues();

			// also add the uploaded file to any child depositions at the same time
			$orchestraDepositions = $this->getOrchestraDepositions();
			$orchestraFiles = $this->getOrchestraFiles();

			$childDepositionsIDs = $orchestraDepositions->getChildDepositionIDs( $this->deposition->ID );
			if( is_array( $childDepositionsIDs ) ) {
				foreach( $childDepositionsIDs as $childDepositionID ) {
					$childFolder = $orchestraFolders->getTranscriptFolderObject( $childDepositionID, NULL, $this->config->logic['transcriptFolderName'] );
					if( !$childFolder->ID ) {
						DB\ServiceFolders::createFolder( $this->config->logic['transcriptFolderName'], $orchestraDepositions->getCreatorID( $childDepositionID ), $childDepositionID, DB\Folders::CLASS_TRANSCRIPT );
						$childFolder = $orchestraFolders->getTranscriptFolderObject( $childDepositionID, NULL, $this->config->logic['transcriptFolderName'] );
					}

					// check for a pre-existing transcript and remove if found
					$existingFile = $orchestraFiles->getTranscriptFile( $childDepositionID );
					if( $existingFile['ID'] ) {
						$serviceFiles->getByID( $existingFile['ID'] )->delete();
					}

					$childFile = new DB\Files();
					$childFile->setSourceFile( $file->getFullName(), TRUE );
					$childFile->name = $file->name;
					$childFile->folderID = $childFolder->ID;
					$childFile->createdBy = $childFolder->createdBy;
					$childFile->created = $now;
					$childFile->isExhibit = 1;
					$childFile->isTranscript = 1;
					$childFile->insert();

					$childFolder->lastModified = $now;
					$childFolder->save();
				}
			}

			/*
				$msg->tpl->title = 'Transcript Uploaded!';
				$msg->tpl->message = 'The transcript has been uploaded to the Exhibit Folder in the repository.<br>Would you like to email the transcript to all Attendees now?';
				$msg->get('btnSend')->onclick = "ajax.doit('->sendAttendeesEmails', {$file->ID}, {$this->deposition->ID});";
			 */
			/* ED-328, transcriptPopoup options are now available from the web page and obstructing flow. mhill
				$msg = $this->get('sendTranscriptPopup');
				$msg->get('btnCancel')->value = 'Skip for Now';
				$msg->get('btnCancel')->onclick = 'window.location=\''.$this->basePath.'/reporter/transcript\'';
			 */

			$msg->tpl->title = 'Transcript Uploaded';
			$msg->tpl->message = '"'.$file->name.'" has been uploaded to the ' . $this->config->logic['transcriptFolderName'] . ' folder.';
		}

		$msg->show();
  }

  public function test()
  {
     $this->get('sendTranscriptPopup')->show();
  }

  public function sendEmailsToAllAttendees($type)
  {
    $emails = array();
    $attendees = $this->getOrchestra('DepositionAttendees')->getNonWitnessAttendessForDepo($this->deposition->ID);
    $msg = $this->get('noticePopup');

    foreach($attendees as $email)
      $emails[$email['email']] = $email['name'];

    if(!(count($emails) > 0))
        $msg->tpl->title = 'Empty Attendee.';
    else
    {
        $result = $this->sendEmails($type, $emails);
        if (count($result) > 0)
          $msg->tpl->title = 'Error sending email(s):'.implode(',',$result);
        else
          $msg->tpl->title = 'Sent email(s) successful.';
    }
    $msg->get('btnCancel')->onclick = 'window.location=\''.$this->basePath.'/reporter/transcript\'';
    $this->get('sendTranscriptPopup')->hide();
    $msg->show();
  }

  public function sendEmails($type, $emails)
  {
     $attachments = array();
     if ($type == 'exhibit' || $type == 'all') {
        $folder = foo(new DB\OrchestraFolders())->getExhibitFolderObject($this->deposition->ID);
        $zip = new \ZipArchive();
        $zipfile = Core\IO::dir('temp').'/'.uniqid('zipemail').'.zip';
        $zip->open($zipfile, \ZipArchive::CREATE);
        foreach ($folder->files as $file) $zip->addFile($file->getFullName(), $file->name);
        if ($zip->numFiles == 0) $zip->addEmptyDir('no files');
        if ($zip->close() === FALSE) throw new \Exception('Could not create ZIP file at '.$zipfile.'. Check permissions.');
        $attachments[$folder->name.'.zip'] = $zipfile;
      }
      if ($type == 'transcript' || $type == 'all') {
        $attachments[] = foo(new DB\ServiceFiles())->getByID($this->tpl->file['ID'])->getFullName();
      }
      switch ($type) {
         case 'all': $result = Utils\EmailGenerator::sendTranscriptAndExhibits($emails, $attachments); break;
         case 'transcript': $result = Utils\EmailGenerator::sendOfficialTranscript($emails, $attachments); break;
         case 'exhibit': $result = Utils\EmailGenerator::sendExhibits($emails, $attachments); break;
      }
     if ($type == 'exhibit' || $type == 'all') unlink($zipfile);
     return $result;
  }

  public function viewFile()
  {
    $file = $this->getService('Files')->getByID($this->tpl->file['ID']);
    if (!$file->ID)
       return;
    $this->ajax->downloadFile($file->getFullName(), $file->name);
  }

  public function deleteFile() {
     $this->hidePopup('confirm');
     $file = $this->getService('Files')->getByID($this->tpl->file['ID']);
     if (0 == $file->ID)
       return;
     $file->delete();
     $this->ajax->redirect($this->basePath.'/reporter/transcript');
  }

  public function confirmDeleteFile() {
    $file = $this->getService('Files')->getByID($this->tpl->file['ID']);
    if (0 == $file->ID)
      return;
    $param = array();
    $param['title'] = 'Delete Transcript';
    $param['message'] = 'Are you sure you want to delete this file?<br>Attendees will no longer be able to access it through their repository.';
    $param['OKName'] = 'Delete';
    $param['OKMethod'] = "ajax.doit('->deleteFile', {$file->ID});";
    $this->showPopup('confirm', $param);
  }

  public function downloadExhibits()
  {
    $folder = foo(new DB\OrchestraFolders())->getExhibitFolderObject($this->deposition->ID);
    $name = $folder->name.'_'.time().'.zip';
    $fullName = Core\IO::dir('temp').'/'.$name;
    $zip = new \ZipArchive();
    $zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
    foreach ($folder->files as $file) {
       $zip->addFile($file->getFullName(),$file->name);
    }
    if ($zip->numFiles == 0) $zip->addEmptyDir('no files');
    if ($zip->close() === FALSE) throw new \Exception('Could not create ZIP file at '.$fullName.'. Check permissions.');
    $this->ajax->downloadFile($fullName, $folder->name.'.zip');
  }
}
?>

