<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\DB\UserSortPreferences,
	ClickBlocks\Utils,
	ClickBlocks\Utils\NodeJS,
	ClickBlocks\EDPDFConvert,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\PDFTools,
	ClickBlocks\EDMMConvert,
	ClickBlocks\FFProbe,
	ClickBlocks\Cache\CacheRedis,
	ClickBlocks\Debug,
	ClickBlocks\Annotate,
	ClickBlocks\Elasticsearch;

//defined( 'MOVE_ITEM_TOKEN' ) or define( 'MOVE_ITEM_TOKEN', 'moveitemmem' );

class PageSessionView extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	protected $deposition = NULL;
	protected $case = null;
	protected $exhibit = 'Exhibit';
	protected $canDelete = false;
	protected $isClientAdmin = FALSE;
	protected $isAdmin = false;
	protected $isManager = FALSE;
	protected $isCreator = false;
	protected $isDepoOwner = false;
	protected $isDepoAssist = false;
	protected $isDemoAdmin = false;
	protected $disableUpload = false;
	protected $isTrustedUser = false;
	protected $clientAdminID = 0;
	protected $isCourtClient = FALSE;
	protected $isTrialBinder = FALSE;

	public function __construct() {
		parent::__construct("depositions/sessionView.html");

		$dpid = isset($this->fv['depoID']) ? (int) $this->fv['depoID'] : 0;
		$cid = isset($this->fv['caseID']) ? (int) $this->fv['caseID'] : 0;
		$this->deposition = foo(new DB\ServiceDepositions)->getByID($dpid);
		$this->case = foo(new DB\ServiceCases)->getByID($cid);
		$this->client = foo(new DB\ServiceClients)->getByID( $this->case->clientID );
		$this->disableUpload  = ( in_array($this->deposition->class, [self::DEPOSITION_CLASS_WPDEMO, self::DEPOSITION_CLASS_DEMOTRIAL]) ) ? '1' : '0';
		$this->clientAdminID = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
		$this->isCourtClient = ($this->user->clients[0]->typeID == self::CLIENT_TYPE_COURTCLIENT);
		$this->isTrialBinder = ($this->deposition->class == self::DEPOSITION_CLASS_TRIALBINDER);
	}

	public function access() {
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/cases';

		$this->isClientAdmin = ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin);
		$this->isAdmin = ($this->case->ID && (new DB\OrchestraCases())->checkCaseAdmin($this->case->ID, $this->user->ID));
		$this->isDemoAdmin = ($this->user->clientID == $this->case->clientID && $this->case->class == 'Demo');
		$this->isManager = ( new DB\OrchestraCaseManagers() )->checkCaseManager( $this->case->ID, $this->user->ID );
		$this->isDepoOwner = ($this->deposition->ID && $this->deposition->ownerID == $this->user->ID);
		$this->isDepoAssist = foo( new DB\OrchestraCases() )->checkDepoAssist( $this->case->ID, $this->user->ID );
		$this->isDepoAttend = foo( new DB\OrchestraCases() )->checkDepoAttendee( $this->case->ID, $this->user->ID );
		$this->isAssistant = (bool)(new DB\ServiceDepositionAssistants)->getByID(array('userID'=>$this->user->ID,'depositionID'=>$this->deposition->ID))->userID;
		$this->isTrustedUser = $this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $this->deposition->ID );

		if( $this->isClientAdmin || $this->isManager || $this->isDemoAdmin ) {
			$this->canDelete = TRUE;
		} else {
			$this->canDelete = FALSE;
		}

		// ED-3772; administrator roles inherit case manager permissions
		// ED-3715; restrict Trial Binder access to trusted users
		// if( $this->deposition->class == self::DEPOSITION_CLASS_TRIALBINDER ) {
		// 	return ($this->isManager || $this->isDepoOwner || $this->isDepoAssist);
		// }

		return ($this->isClientAdmin || $this->isDemoAdmin || $this->isManager || $this->isDepoOwner || $this->isDepoAssist || $this->isDepoAttend);
//		return ($this->isDepoOwner || $this->canDelete);
	}

	public function init()
	{
		parent::init();
		$this->js->addTool( 'ui' )->addTool( 'mark' );

		$this->tpl->isAdmin = $this->isClientAdmin;
		$this->tpl->isDemoAdmin = $this->isDemoAdmin;
		$this->tpl->isManager = $this->isManager;
		$this->tpl->isDepoOwner = $this->isDepoOwner;
		$this->tpl->isDepoAssist = $this->isDepoAssist;
		$this->tpl->admin = 1;
		$this->tpl->maxFilesize = ($this->deposition->class == self::DEPOSITION_CLASS_TRIALBINDER ? $this->config->trialbinder['maxFilesize'] : $this->config->maxFilesize);
		$this->tpl->isCourtClient = $this->isCourtClient;
		$this->tpl->isTrialBinder = $this->isTrialBinder;

		$this->head->name = 'Session View';
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->ID = $this->deposition->ID;
		$this->tpl->hname = $this->deposition->depositionOf;
		$this->tpl->cname = $this->case->name;

		$depoInfoPanel = $this->get('depoInfoPanel');
		$depoInfoPanel->tpl->ID = $this->deposition->ID;
		$depoInfoPanel->tpl->depositionOf = $this->deposition->depositionOf;
		$depoInfoPanel->tpl->openDateTime = $this->deposition->openDateTime;
		$depoInfoPanel->tpl->volume = $this->deposition->volume;
		$depoInfoPanel->tpl->class = $this->deposition->class;
		$depoInfoPanel->tpl->statusID = $this->deposition->statusID;

		$this->tpl->hasDatasources = $this->getOrchestraDatasources()->hasDatasources( $this->user->clientID );
		$this->tpl->disableUpload = $this->disableUpload;

		$isChild = ($this->deposition->parentID !== null) ? TRUE : FALSE;
		$isFinished = ($this->deposition->statusID == 'F') ? TRUE : FALSE;
		$isWitnessPrep = $this->deposition->isWitnessPrep();

		$isTrustedUser = ($this->isManager || $this->deposition->createdBy == $this->user->ID || $this->isDepoOwner || $this->isDemoAdmin || $this->isDepoAssist);

		$isDemo = $this->deposition->isDemo();
		$active = ($this->deposition->statusID === self::DEPOSITION_STATUS_IN_PROCESS);
		$started = $this->deposition->started ? 1 : 0;
        $finished = $this->deposition->finished ? 1 : 0;

		$folders = (new DB\OrchestraFolders())->getDepositionFoldersForUser( $this->deposition->ID, $this->user->ID, TRUE );
		$folders = $this->sortList( $folders, $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS ) );
		$folderPanel = $this->get('folder_panel');
		$addFolderParams = [];
        foreach ($folders as $idx => $folder) {
			$addFolderParams[] = [
				'ID' => $folder['ID'],
				'name' => $folder['name'],
				'sortPos' => $folder['sortPos'],
				'canRename' => ( ($folder['class'] === 'Folder' || $folder['class'] === 'Trusted') && (!$started || $finished) && !$folder['caseID'] ),
				'caseFolder' => ($folder['caseID']),
				'linked' => (isset( $folder['linked'] ) && $folder['linked'] ? $folder['linked'] : FALSE)
			];
        }
		$folderPanel->tpl->folderList = $addFolderParams;

		//Needed for the Documents sort header info to show up
		$prefs = $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS );
		$this->updateSortHeader( $prefs['sortBy'], $prefs['sortOrder'], 'Folders' );
		$prefs = $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FILES );
		$this->updateSortHeader( $prefs['sortBy'], $prefs['sortOrder'], 'Files' );

		$sortMenuOptions = [
			['name'=>'Name'],
			['name'=>'Date'],
			['name'=>'Custom']
		];

		$this->get( 'sortMenuPopup' )->tpl->sortMenuItems = $sortMenuOptions;

		$addMenuOptions = [];
		if( $this->isClientAdmin || $this->isManager || $this->isDemoAdmin || $this->isDepoOwner || $this->isDepoAssist ) {
			if( (!$this->isCourtClient && ($this->isManager || $this->isDepoOwner || $this->isDemoAdmin)) || ($this->isCourtClient && ($this->isTrustedUser) && $this->isTrialBinder) ) {
				$addMenuOptions['Edit Session'] = ['href'=>"{$this->basePath}/session/" . str_replace(' ', '', strtolower($this->deposition->class)) . "?caseID={$this->case->ID}&depoID={$this->deposition->ID}&view=true"];
			}
			if( (!$this->isCourtClient && ($this->isTrustedUser)) || ($this->isCourtClient && $this->user->typeID == self::USER_TYPE_REPORTER) ) {
				$addMenuOptions['Download Session'] = ['onclick'=>'ajax.doit(\'->downloadDeposition\');'];
			}
			if( !$this->isTrialBinder && (!$this->isCourtClient && ($this->isClientAdmin || $this->isManager || $this->isDepoOwner || $this->isDepoAssist || $this->isDemoAdmin)) || ($this->isCourtClient && ($this->user->typeID == self::USER_TYPE_REPORTER)) ) {
				$addMenuOptions['Exhibit Log'] = ['onclick'=>'ajax.doit(\'->depoExhibitLog\');'];
			}
			if( $this->deposition->isDemo() ) {
				$addMenuOptions['Reset Session'] = ['onclick'=>'resetDemoSession(\''.$this->deposition->ID.'\')'];
			}
			if( $this->isManager && !$isChild && !$isFinished && $this->deposition->class == self::DEPOSITION_CLASS_DEPOSITION ) {
				// $addMenuOptions['Request Live Transcript'] = ['onclick'=>'showRequestLiveTranscript()'];
			}
			if( !$isFinished && $isTrustedUser && !$isChild && $isWitnessPrep ) {
				$addMenuOptions['Send Witness Invitation'] = ['onclick'=>'showWitnessInvite()'];
			}
//			if ( $this->canDelete && $this->deposition->statusID != 'I') {
//				$addMenuOptions['Delete'] = ['onclick'=>'ajax.doit(\'->confirmDeleteDepo\', \'' . $this->deposition->ID . '\');'];
//			}
		}
		$this->get('session_btn_menu')->options = $addMenuOptions;
		if( $addMenuOptions ) {
			$this->tpl->showSessionMenu = TRUE;
		}
	}

	public function sortList( $list, $sortPrefs ) {
		if( !is_array( $list ) || !$list ) {
			return [];
		}

		foreach( $list as $key => $row ) {
			$primary[$key]  = $row[ $sortPrefs['sortOn'] ];
			$secondary[$key] = (int)$row['ID'];
		}
		$sortOrder = ( $sortPrefs['sortOrder'] === UserSortPreferences::SORTORDER_ASC ) ? SORT_ASC : SORT_DESC;
		array_multisort( $primary, $sortOrder, SORT_NATURAL|SORT_FLAG_CASE, $secondary, $sortOrder, SORT_NATURAL, $list );

		return $list;
	}

	function getSortPrefs( $sortObject ) {
		$sortPrefs = $this->getOrchestraUserSortPreferences()->getSortPreferencesForUserIDBySortObject( $this->user->ID, $sortObject );
		$sortPref = [
			'sortBy' => UserSortPreferences::SORTBY_NAME,
			'sortOrder' => 'Asc',
			'sortOn' => 'name'
		];
		if( is_array( $sortPrefs ) && $sortPrefs ) {
			$sortPref['sortBy'] = $sortPrefs['sortBy'];
			$sortPref['sortOrder'] = $sortPrefs['sortOrder'];
			$sortPref['sortOn'] = $this->getSortOn( $sortPref['sortBy'] );
		}
		return $sortPref;
	}

	protected function getSortOn( $sortBy ) {
		$sortOn = '';
		switch( $sortBy ):
			case UserSortPreferences::SORTBY_NAME:
				$sortOn = 'name';
				break;
			case UserSortPreferences::SORTBY_DATE:
				$sortOn = 'created';
				break;
			case UserSortPreferences::SORTBY_CUSTOM:
				$sortOn = 'sortPos';
				break;
			case 'Rank':
				$sortOn = 'rank';
				break;
		endswitch;

		return $sortOn;
	}

	function updateSortHeader( $sortBy, $sortOrder, $type ) {
		$sortOrder = ($sortBy == 'Custom') ? '' : $sortOrder;

		if( $type == 'Folders' ) {
			$folderSortPanel = $this->get('folderSortPanel');
			$folderSortPanel->tpl->folderSortBy = $sortBy;
			$folderSortPanel->tpl->folderSortOrder = $sortOrder;
			$folderSortPanel->update();
		} else if( $type == 'Files' ) {
			$fileSortPanel = $this->get('fileSortPanel');
			$fileSortPanel->tpl->fileSortBy = $sortBy;
			$fileSortPanel->tpl->fileSortOrder = $sortOrder;
			$fileSortPanel->update();
		}
	}

	public function toggleSortOrder( $sortObject ) {
		$filePanel = $this->get( 'file_panel' );
		if( $filePanel->tpl->isContentSearch === TRUE ) {
			$fileSortPanel = $this->get('fileSortPanel');
			$fileSortPanel->tpl->fileSortOrder = ($fileSortPanel->tpl->fileSortOrder === UserSortPreferences::SORTORDER_ASC) ? UserSortPreferences::SORTORDER_DESC : UserSortPreferences::SORTORDER_ASC;
			$sortPrefs = [
				'sortOn' => $this->getSortOn( $fileSortPanel->tpl->fileSortBy ),
				'sortOrder' => $fileSortPanel->tpl->fileSortOrder
			];
			$filePanel->tpl->fileList = $this->sortList( $filePanel->tpl->fileList, $sortPrefs );
			$filePanel->update();
			$fileSortPanel->update();
			$this->ajax->script( 'fillFileList(true);' );
		} else {
			switch( mb_strtolower( $sortObject ) ) {
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FOLDERS ):
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FILES ):
					$sortPref = new DB\UserSortPreferences( $this->user->ID, $sortObject );
					break;
			}
			if( !isset( $sortPref ) || !$sortPref ) {
				return;
			}
			if( !$sortPref || !$sortPref->userID || !$sortPref->sortObject || $sortPref->userID != $this->user->ID || $sortPref->sortObject != $sortObject ) {
				$sortPref->userID = $this->user->ID;
				$sortPref->sortObject = $sortObject;
			}
			$sortPref->setSortPreference( $sortPref->sortBy, ($sortPref->sortOrder == UserSortPreferences::SORTORDER_ASC ? UserSortPreferences::SORTORDER_DESC : UserSortPreferences::SORTORDER_ASC) );
			$sortPrefArray = [
				'sortOn'=>$this->getSortOn( $sortPref->sortBy ),
				'sortOrder'=>$sortPref->sortOrder
			];
			if( $sortObject == UserSortPreferences::SORTOBJECT_FOLDERS ) {
				$this->tpl->folderSortPrefs = $sortPref->getValues();
				$folderPanel = $this->get('folder_panel');
				$folderSortPanel = $this->get('folderSortPanel');
				$folderSortPanel->tpl->folderSortOrder = $sortPref->sortOrder;
				$folderPanel->tpl->folderList = $this->sortList( $folderPanel->tpl->folderList, $sortPrefArray );
				$folderPanel->update();
				$folderSortPanel->update();
			} elseif( $sortObject == UserSortPreferences::SORTOBJECT_FILES ) {
				$fileSortPanel = $this->get('fileSortPanel');
				$this->tpl->fileSortPrefs = $sortPref->getValues();
				$fileSortPanel->tpl->fileSortOrder = $sortPref->sortOrder;
				$filePanel->tpl->fileList = $this->sortList( $filePanel->tpl->fileList, $sortPrefArray );
				$filePanel->update();
				$fileSortPanel->update();
				$this->ajax->script( 'fillFileList(true);' );
			}
		}
	}

	public function changeSort( $sortObject, $sortBy ) {
		$filePanel = $this->get( 'file_panel' );
		$sortBys = UserSortPreferences::sortBys();
		if( $filePanel->tpl->isContentSearch === TRUE ) {
			$sortBys[] = 'Rank';
		}
		if( !in_array( $sortBy, $sortBys ) ) {
			Debug::ErrorLog( "PageSessionView::changeSort; Invalid sortBy: {$sortBy}" );
			return;
		}
		if( $filePanel->tpl->isContentSearch === TRUE ) {
			$fileSortPanel = $this->get('fileSortPanel');
			$fileSortPanel->tpl->fileSortBy = $sortBy;
			$sortPrefs = [
				'sortOn' => $this->getSortOn( $fileSortPanel->tpl->fileSortBy ),
				'sortOrder' => $fileSortPanel->tpl->fileSortOrder
			];
			$filePanel->tpl->fileList = $this->sortList( $filePanel->tpl->fileList, $sortPrefs );
			$filePanel->update();
			$fileSortPanel->update();
			$this->ajax->script( 'fillFileList(true);' );
		} else {
			switch( mb_strtolower( $sortObject ) ) {
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FOLDERS ):
				case mb_strtolower( UserSortPreferences::SORTOBJECT_FILES ):
					$sortPref = new DB\UserSortPreferences( $this->user->ID, $sortObject );
					break;
			}
			if( !isset( $sortPref ) || !$sortPref ) {
				return;
			}
			if( !$sortPref || !$sortPref->userID || !$sortPref->sortObject || $sortPref->userID != $this->user->ID || $sortPref->sortObject != $sortObject ) {
				$sortPref->userID = $this->user->ID;
				$sortPref->sortObject = $sortObject;
			}
			$sortPref->setSortPreference( $sortBy, $sortPref->sortOrder );
			$sortPrefArray = [
				'sortOn' => $this->getSortOn( $sortPref->sortBy ),
				'sortOrder' => $sortPref->sortOrder
			];

			if( $sortObject == UserSortPreferences::SORTOBJECT_FOLDERS ) {
				$this->tpl->folderSortPrefs = $sortPref->getValues();
				$folderPanel = $this->get( 'folder_panel' );
				$folderSortPanel = $this->get('folderSortPanel');
				$folderSortPanel->tpl->folderSortBy = $sortPref->sortBy;
				$folderSortPanel->tpl->folderSortOrder = ( $sortPref->sortBy === UserSortPreferences::SORTBY_CUSTOM ? FALSE : $sortPref->sortOrder );
				$folderPanel->tpl->folderList = $this->sortList( $folderPanel->tpl->folderList, $sortPrefArray );
				$folderPanel->update();
				$folderSortPanel->update();
			} elseif( $sortObject == UserSortPreferences::SORTOBJECT_FILES ) {
				$fileSortPanel = $this->get('fileSortPanel');
				$fileSortPanel->tpl->fileSortBy = $sortBy;
				$fileSortPanel->tpl->fileSortOrder = ( $sortBy === UserSortPreferences::SORTBY_CUSTOM ? FALSE : $sortPref->sortOrder );
				$filePanel->tpl->fileList = $this->sortList( $filePanel->tpl->fileList, $sortPrefArray );
				$filePanel->update();
				$fileSortPanel->update();
				$this->ajax->script( 'fillFileList(true);' );
			}
		}
	}

	public function setCustomSort( $sortObject, $sortPosList ) {
		if( !in_array( $sortObject, [UserSortPreferences::SORTOBJECT_FOLDERS, UserSortPreferences::SORTOBJECT_FILES] ) ) {
			return;
		}
		$userID = $this->isTrustedUser ? $this->clientAdminID : $this->user->ID;
		$sortPosMap = [];
		$listMap = [];
		$sortPositions = [];

		switch( $sortObject ) {
			case UserSortPreferences::SORTOBJECT_FOLDERS:
				$flPanel = $this->get( 'folder_panel' );
				if( !$flPanel ) {
					return;
				}
				$folderList = $flPanel->tpl->folderList;
				if( !$folderList || !is_array( $folderList ) ) {
					return;
				}
				foreach( $this->get( 'folder_panel' )->tpl->folderList as $idx => $folder ) {
					$sortPosMap[$idx] = $folder['sortPos'];
					$listMap[$folder['ID']] = $folder;
				}
				foreach( $sortPosList as $idx => $item ) {
					$_folderID = (int)$item;
					$_folder = $listMap[$_folderID];
					$sortPos = $sortPosMap[$idx];
					$_folder['sortPos'] = $sortPos;
					$sortPositions[$_folder['ID']] = $_folder['sortPos'];
				}
				DB\UserCustomSortFolders::setCustomSort( $userID, $sortPositions );
				break;
			case UserSortPreferences::SORTOBJECT_FILES:
				foreach( $this->get( 'file_panel' )->tpl->fileList as $idx => $file ) {
					$sortPosMap[$idx] = $file['sortPos'];
					$listMap[$file['ID']] = $file;
				}
				foreach( $sortPosList as $idx => $sortPos ) {
					$fileID = (int)$sortPos;
					$file = $listMap[$fileID];
					$sortPositions[$file['ID']] = $sortPosMap[$idx];
				}
				DB\UserCustomSortFiles::setCustomSort( $userID, $sortPositions );
				break;
		}
	}

	public function refreshFolders() {
		$folders = $this->sortList( $this->getOrchestraFolders()->getDepositionFoldersForUser( $this->deposition->ID, $this->user->ID, TRUE ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FOLDERS ));
		$folderPanel = $this->get('folder_panel');
		$addFolderParams = [];
		foreach( $folders as $folder ) {
			$addFolderParams[] = [
				'ID' => $folder['ID'],
				'name' => $folder['name'],
				'sortPos' => $folder['sortPos'],
				'canRename' => ( ($folder['class'] === 'Folder' || $folder['class'] === 'Trusted') && (!$this->deposition->started || $this->deposition->finished) && !$folder['caseID'] ),
				'caseFolder' => ($folder['caseID']),
				'linked' => (isset( $folder['linked'] ) && $folder['linked'] ? $folder['linked'] : FALSE)
			];
		}
		$folderPanel->tpl->folderList = $addFolderParams;
		$folderPanel->update();
		$this->ajax->script( 'updateFolderCount();' );
		$this->ajax->script( 'fillFolderList();' );
	}

	public function selectFolder( $folderID ) {
		$filePanel = $this->get( 'file_panel' );
		if( $filePanel->tpl->isContentSearch ) {
			$sortMenuOptions = [
				['name'=>'Name'],
				['name'=>'Date'],
				['name'=>'Custom']
			];

			$sortPopup = $this->get( 'sortMenuPopup' );
			$sortPopup->tpl->sortMenuItems = $sortMenuOptions;
			$sortPopup->update();
			$prefs = $this->getSortPrefs( $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FILES ) );
			$this->updateSortHeader( $prefs['sortBy'], $prefs['sortOrder'], 'Files' );
		}
		$folder = new DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			Debug::ErrorLog( "PageSessionView::selectFolder; folder not found by ID: {$folderID}" );
			return;
		}
		if( $folder->isCaseFolder() ) {
			if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
				Debug::ErrorLog( "PageSessionView::selectFolder; Access denied to folderID: {$folderID} for userID: {$this->user->ID}" );
				return;
			}
		} else {
			if( !$this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $folderID ) ) {
				Debug::ErrorLog( "PageSessionView::selectFolder; Access denied to folderID: {$folderID} for userID: {$this->user->ID}" );
				return;
			}
		}
		$isTranscript = ($folder->class === self::FOLDERS_TRANSCRIPT);
		$files = $this->sortList( $this->getOrchestraFiles()->getFilesWithSortPos( $folderID, $this->user->ID, ($this->isTrustedUser ? $this->clientAdminID : $this->user->ID) ), $this->getSortPrefs( UserSortPreferences::SORTOBJECT_FILES ) );
		$fileList = [];
		foreach ($files as $file) {
			$File = new DB\Files( $file['ID'] );
			if( !$File || !$File->ID || $File->ID != $file['ID'] ) {
				continue;
			}
			if( !$File->filesize ) {
				$File->setMetadata();
			}
			$fileSize = Utils::readableFilesize( $File->filesize, 1 );
			$fileList[] = [
				'ID' => $file['ID'],
				'name' => $file['name'],
				'created' => $file['created'],
				'sortPos' => $file['sortPos'],
				'size' => $fileSize,
				'canRename' => ( (!$file['isExhibit'] || ($isTranscript && $this->isTrustedUser) ) && (!$this->deposition->started || $this->deposition->finished) )
			];
		}

		$this->updateFileMgmtActions( $folderID, count( $files ) );

		$filePanel->tpl->fileList = $fileList;
		$filePanel->tpl->isContentSearch = false;
		$filePanel->update();
		$this->ajax->script( 'updateFileCount();' );
		$this->ajax->script( 'updateSelectedFilesCount('. 0 .');' );
		$this->ajax->script( 'fillFileList();' );
	}

	public function updateFileMgmtActions( $folderID, $filesCount=FALSE ) {
		$folder = new DB\Folders( $folderID );
		$filesCount = ($filesCount !== FALSE) ? $filesCount : count( $folder->files );

		$actions = [
			'downloadFolder' => 0,
			'copyFolder' => 0,
			'moveFolder' => 0,
			'deleteFolder' => 0,
			'downloadFile' => 0,
			'copyFile' => 0,
			'moveFile' => 0,
			'deleteFile' => 0,
			'addFolder' => 0,
			'import' => 0,
			'addDocs' => 0,
			'addPlaceholder' => 0
		];

		if ($filesCount > 0 ) {
			$actions['downloadFolder'] = 1; //download
		}

		switch( $folder->class ) {
			case self::FOLDERS_EXHIBIT:
				$actions['copyFolder'] = 1; //copy
				if( $this->isTrialBinder && $this->isTrustedUser ) {
					$actions['addDocs'] = 1; //add
					$actions['deleteFile'] = ($folder->isCaseFolder() ? 0 : 1); //delete
				}
				break;
			case self::FOLDERS_TRANSCRIPT:
				$actions['copyFolder'] = 1; //copy
				if ($this->isTrustedUser && !$this->disableUpload) {
					$actions['addDocs'] = 1; //add
				}
				break;
			case self::FOLDERS_PERSONAL:
				$actions['copyFolder'] = 1; //copy
				if (!$this->disableUpload) {
					$actions['addDocs'] = 1; //add
				}
				break;
			case self::FOLDERS_WITNESSANNOTATIONS:
				$actions['copyFolder'] = 1; //copy
				break;
			default:
				$actions['copyFolder'] = 1; //copy
				$actions['moveFolder'] = ($folder->isCaseFolder() ? 0 : 1); //move
				$actions['deleteFolder'] = ($folder->isCaseFolder() ? 0 : 1); //delete
				if (!$this->disableUpload) {
					$actions['addDocs'] = 1; //add
				}
		}

		if ($filesCount > 0) {
			$actions['downloadFile'] = 1; //download
			if ($folder->class == self::FOLDERS_EXHIBIT) {
				$actions['copyFile'] = 1; //copy
			} else if ($folder->class == self::FOLDERS_TRANSCRIPT) {
				$actions['copyFile'] = 1; //copy
				if ($this->isTrustedUser) {
					$actions['moveFile'] = ($folder->isCaseFolder() ? 0 : 1); //move
					$actions['deleteFile'] = ($folder->isCaseFolder() ? 0 : 1); //delete
				}
			} else {
				$actions['copyFile'] = 1; //copy
				$actions['moveFile'] = ($folder->isCaseFolder() ? 0 : 1); //move
				$actions['deleteFile'] = ($folder->isCaseFolder() ? 0 : 1); //delete
			}
		}

		if (!$this->disableUpload) {
			$actions['addFolder'] = 1;
			$actions['import'] = (int)$this->tpl->hasDatasources;
			$actions['addPlaceholder'] = 1;
		}

		$this->ajax->script( 'updateFileMgmtActions(' . implode( ',', $actions ) . ');' );
	}

	public function showEditFolder($id) {
        $dlg = $this->get('folderEditPopup');

        $folder = foo(new DB\ServiceFolders())->getByID($id);
        if ($folder->ID > 0) {
            $dlg->tpl->title = 'Edit Folder';
            $dlg->tpl->ID = $folder->ID;
            $dlg->tpl->name = $folder->name;
            $dlg->get('folderName')->value = $folder->name;
        } else {
            $dlg->tpl->title = 'New Folder';
            $dlg->tpl->ID = 0;
            $dlg->tpl->name = '';
            $dlg->get('folderName')->value = '';
        }
        $dlg->get('btnOK')->onclick = 'saveFolder(' . $dlg->tpl->ID . ')';
        $dlg->show();
    }

	public function checkFolderName( $ctrls ) {
		$folderEdit = $this->get( 'folderEditPopup' )->tpl->name;
		$folderName = $this->getByUniqueID( $ctrls[0] )->value;
		if( $folderEdit && strcasecmp( $folderEdit, $folderName ) === 0 ) {
			return TRUE;
		}
		if( Edepo::isReservedFolder( $folderName ) ) {
			return FALSE;
		}
		return $this->getOrchestraFolders()->checkUniqueFolder( $this->deposition->ID, $folderName );
    }

	public function saveFolder( $folderID=0 ) {
        if( !$this->validators->isValid( 'folder' ) ) {
            return;
		}

		$depositionID = $this->deposition->ID;
		$folder = new DB\Folders( $folderID );
        if( !$folder->ID ) {
			$deposition = new DB\Depositions( $depositionID );
			if ( $this->isTrustedUser ) {
				//trusted users
				$createdBy = $deposition->createdBy;
				$folder->class = self::FOLDERS_TRUSTED;
			} else {
				//attendee
				$createdBy = $this->user->ID;
			}
			$now = date( 'Y-m-d H:i:s' );
            $folder->created = $now;
			$folder->lastModified = $now;
            $folder->createdBy = $createdBy;
            $folder->depositionID = $depositionID;
        }
        $folder->name = $this->get( 'folderEditPopup' )->get( 'folderName' )->value;
        $folder->save();

		$this->notifyNodeJSUsers( $folder->ID );

		$this->refreshFolders();
		$this->ajax->script( "selectFolder({$folder->ID});" );
		$this->get( 'folderEditPopup' )->hide();
    }

	public function showImportFolder()
	{
		$dlg = $this->get( 'folderImportPopup' );
		$dlg->get( 'errormsg' )->text = '';
		$dlg->tpl->title = 'Import ...';
		$dlg->tpl->ID = 0;
		//$dlg->tpl->depoID = $this->get( 'depositionID' )->value;
		$datasources = array();
		if( $this->user->typeID == self::USER_TYPE_CLIENT || $this->isManager ) {
			$datasources = foo(new DB\OrchestraDatasources())->getDatasources( $this->user->clientID );
		}
		$options = array();
		foreach( $datasources as $api ) {
			$options[$api['ID']] = "{$api['vendor']}: {$api['name']}";
		}
		$dlg->get( 'datasources' )->options = $options;
		$dlg->get( 'datasources' )->value = key( $options );
		$dlg->get( 'depositionID' )->value = $this->get( 'depositionID' )->value;
        $dlg->get( 'btnOK' )->visible = (count( $options ) > 0);
        $dlg->get( 'btnOK' )->onclick = 'importFromAPI()';
        $dlg->show();
	}

	public function confirmDeleteFolder($id) {
        $folder = foo(new DB\ServiceFolders())->getByID($id);
        if (0 == $folder->ID)
            return;
        $param = array();
        $param['title'] = 'Delete Folder?';
        $param['message'] = 'Are you sure you want to delete this folder & all its content?';
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteFolder({$folder->ID}, 1);";
        $this->showPopup('confirm', $param);
    }

    public function deleteFolder( $folderID ) {
		$folderID = (int)$folderID;
		if( $folderID != $this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageSessionView::deleteFolder; permission denied to folder: {$folderID}" );
			return;
		}
		// Hide dialog popup
		$this->hidePopup( 'confirm' );
		$folder = new DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID == $folderID ) {
			$nodeJS = new NodeJS( TRUE );
			$nodeJS->notifyUsersRemovedFolder( $folderID, [$this->deposition->ID] );
			$folder->delete();
			$this->refreshFolders();
			$this->ajax->script( "selectFolder();" );
		}
    }

	public function showEditFile( $fileID ) {
		$dlg = $this->get( 'fileEditPopup' );
		$file = new DB\Files( $fileID );
		if( $file->ID == $fileID ) {
			$dlg->tpl->ID = $file->ID;
			$dlg->get('fileName')->value = pathinfo( $file->getFullName(), PATHINFO_FILENAME );
		}
		$dlg->get('btnOK')->onclick = 'saveFile(' . $file->ID . ')';
		$dlg->show();
	}

	public function checkFileName($ctrls) {
        $file = foo(new DB\ServiceFiles())->getByID($this->get('fileEditPopup')->tpl->ID);
        $fileEdit = $file->name;
        $pos = strrpos($file->name, '.');
        $fileName = $this->getByUniqueID($ctrls[0])->value . substr($file->name, $pos);
        if (!empty($fileEdit) && $fileEdit == $fileName) {
            return true;
        }
        return foo(new DB\OrchestraFiles())->checkUniqueFile($file->folderID, $fileName);
    }

	public function saveFile($id) {
        if (!$this->validators->isValid('file'))
            return;
        $this->get('fileEditPopup')->hide();
        $file = foo(new DB\ServiceFiles())->getByID($id);
        $pos = strrpos($file->name, '.');
        $file->name = $this->get('fileEditPopup.fileName')->value . substr($file->name, $pos);
        foo(new DB\ServiceFiles())->save($file);
		$this->ajax->script( "selectFolder(selectedFolderID);" );

		$this->notifyNodeJSUsers( $file->folderID );
    }

	public function confirmDeleteFile($ids) {
        $param = array();
		// See if we are deleting more than one document.
		if (strpos($ids, ',') === false) {
			$param['title'] = 'Delete Document?';
			$param['message'] = 'Are you sure you want to delete this document?';
		} else {
			$param['title'] = 'Delete Documents?';
			$param['message'] = 'Are you sure you want to delete these documents?';
		}
        $param['OKName'] = 'Delete';
        $param['OKMethod'] = "deleteFile('" . $ids . "', 1);";
        $this->showPopup('confirm', $param);
    }

	public function deleteFile( $IDs )
	{
		// Hide dialog popup
		$this->hidePopup( 'confirm' );

		$fileIDs = explode( ',', $IDs );
		$folders = [];
		$now = date( 'Y-m-d H:i:s' );

		foreach( $fileIDs as $fileID ) {
			//file
			$file = new DB\Files( $fileID );
			if( !$file || !$file->ID || $file->ID != $fileID ) {
//				Debug::ErrorLog( "PageSessionView::deleteFile; fileID: {$fileID} not found" );
				continue;
			}

			//folder
			$folder = new DB\Folders( $file->folderID );
			if( !$folder || !$folder->ID || $folder->ID != $file->folderID ) {
//				Debug::ErrorLog( "PageSessionView::deleteFile; folderID: {$file->folderID} not found" );
				continue;
			}

			//deposition
			$deposition = new DB\Depositions( $folder->depositionID );
			if( !$deposition || !$deposition->ID || $deposition->ID != $folder->depositionID ) {
				Debug::ErrorLog( "PageCaseView::deleteFile; depositionID: {$folder->depositionID} not found" );
				continue;
			}
			if( $folder->class == self::FOLDERS_EXHIBIT ) {
				if( !($this->isTrialBinder && $this->isTrustedUser) ) {
					continue;
				}
			}

			$file->delete();

			if( !isset( $folders[$folder->ID] ) ) {
				$folder->lastModified = $now;
				$folder->save();
				$folders[$folder->ID] = $folder->ID;
			}
		}
		$nodeJS = new NodeJS( TRUE );
		$nodeJS->notifyUsersRemovedFile( $folders );
		$this->ajax->script( "selectFolder({$folder->ID});" );

		unset( $deposition, $folder, $file );
	}

	public function showUploadFiles( $folderID ) {
		$folder = new DB\Folders( $folderID );

		if( $folder->class == self::FOLDERS_EXHIBIT || $this->disableUpload || ($folder->class == self::FOLDERS_TRANSCRIPT && !$this->isTrustedUser) ) {
			if( !($folder->class == self::FOLDERS_EXHIBIT && $this->isTrialBinder) ) {
				return;
			}
		}

		$this->tpl->folderID = $folder->ID;
		$this->tpl->folderName = $folder->name;
		$this->tpl->fileArray = array();
        $this->tpl->fileSizeArray = array();

        $dlg = $this->get('uploadFilesPopup');
        $dlg->get('dropzone')->tpl->files = array();
        $dlg->show();
    }

	public function folderUploadDocument( $folderID, $uniqueID, $uploadID )
	{
        $results = ['args'=>(object)['uniqueID' => $uniqueID, 'uploadID' => $uploadID, 'folderID' => $folderID], 'success'=>TRUE];

		try {
			$upload = new Utils\FileUpload();
			$upload->maxsize = $this->tpl->maxFilesize;
			$extensions = trim( preg_replace( '/\s+/', '', $this->config->valid_files['extensions'] ) );
			$upload->extensions = explode( ',', $extensions );
			$documentTypesStr = trim( preg_replace( '/\s+/', '', $this->config->valid_files['document_types'] ) );
			$documentTypes = explode( ',', $documentTypesStr );
			$multimediaTypesStr = trim( preg_replace( '/\s+/', '', $this->config->valid_files['multimedia_types'] ) );
			$multimediaTypes = explode( ',', $multimediaTypesStr );
			$upload->types = array_merge( $documentTypes, $multimediaTypes );

//			Debug::ErrorLog( print_r( $upload->extensions, TRUE ) );
//			Debug::ErrorLog( print_r( $upload->types, TRUE ) );

			$info = $upload->upload( $_FILES[$uniqueID] );

			if( $upload->error === UPLOAD_ERR_INI_SIZE ) {
				throw new \Exception( 'File is too large.' );
			}

			if( $upload->error === UPLOAD_ERR_EXTENSION ) {
				throw new \Exception( 'File type is not accepted.' );
			}

			if( !$info ) {
				throw new \Exception( 'File upload is invalid' );
			}

			// prepare meta info
			$folder = new DB\Folders( $folderID );
//			$deposition = new DB\Depositions( $folder->depositionID );
			$trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap( $this->deposition->ID );

            // Is this a demo, and are we at the limit for demo files?
            if( $this->deposition->isDemo() ) {
                if( $this->config->demoDocLimit && ($this->deposition->getFileCount() - $this->deposition->getDemoCannedFileCount()) >= $this->config->demoDocLimit ) {
                    throw new \Exception( 'Demo documents limited to ' . $this->config->demoDocLimit . '.' );
                }
            }

			if( !file_exists( $info['fullname'] ) ) {
				throw new \Exception( 'Document not found: "'.$info['fullname'].'"' );
			}

			$filename = $info['originalName'];
			$filepathname = $info['fullname'];
			$isMultimedia = FALSE;

			// ED-1519: Allow multimedia conversions
			//Debug::ErrorLog( "\$info['type']: ".$info['type'] );
			//Debug::ErrorLog( "\$info: " . print_r($info, 1) );

			if( !in_array( $info['type'], $documentTypes ) && !in_array( $info['type'], $multimediaTypes ) ) {
				unlink( $info['fullname'] );
				throw new \Exception( 'Invalid file type ('.$info['type'].')' );
			}

			$isPDF = ($info['type'] == 'application/pdf');
			$isMultimedia = in_array( $info['type'], $multimediaTypes );
			$pathInfo = pathinfo( $filename );


			//FIX: Remove weird characters, for example, there was a - which isnt the real -, was storing in DB as â€“ and in real-file was the fake -, so the file was never found
			$pathInfo['filename'] = preg_replace('/[[:^print:]]/', '', $filename);
			$filename = preg_replace('/[[:^print:]]/', '', $filename);



			// If file is not a pdf AND not one of the multimedia types, run conversion
			if( !$isPDF && !$isMultimedia ) {
				$pdfConverter = new EDPDFConvert();
				$pdfConverter->setFilename( $filepathname );
				$filepathname = $pdfConverter->convert();
				if( !$this->deposition->isDemo() ) {
					$pdfConverter->pdfCleanup( $filepathname );
				}
				$pdfConverter->reset();
				$filename = $pathInfo['filename'] . '.pdf';
				unlink( $info['fullname'] );
				unset( $pdfConverter );
			} elseif( $isMultimedia ) {	// If file is one of the multimedia types, convert it to our supported type
				$mmConvert = new EDMMConvert();
				$ffprobe = new FFProbe( $filepathname );
				if( $ffprobe->isVideo() ) {
					$isSupportedMP4 = $ffprobe->videoIsH264withAAC();
					if( !$isSupportedMP4 ) {
						$filename = $pathInfo['filename'] . '.mp4';
						$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.mp4';
						$success = $mmConvert->toMP4( $filepathname, $outPath );
						if( !$success ) {
							Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . "; Conversion from {$filepathname} to {$outPath} failed." );
							throw new \Exception( 'Video conversion failed.' );
						}
						copy( $outPath, $filepathname );
						unlink( $outPath );
					}
				} else {
					$mimeType = mime_content_type( $filepathname );
					//mp3
					if( !$ffprobe->audioIsMP3() || !in_array( $mimeType, $multimediaTypes ) ) {
						Debug::ErrorLog( "PageSessionView::folderUploadDocument; convert to MP3 from mimeType: {$mimeType}" );
						$filename = $pathInfo['filename'] . '.mp3';
						$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.mp3';
						$success = $mmConvert->toMP3( $filepathname, $outPath );
						if( !$success ) {
							Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . "; Conversion from {$filepathname} to {$outPath} failed." );
							throw new \Exception( 'Audio conversion failed.' );
						}
						copy( $outPath, $filepathname );
						unlink( $outPath );
					}
				}
				unset( $mmConvert );
			}

			$now = date( 'Y-m-d H:i:s' );

			// copy the file into a permanent folder
			$file = new DB\Files();
			$file->name = $filename;
			$file->setSourceFile( $filepathname );
			$file->created = $now;
			$file->folderID = $folder->ID;
			$file->sourceUserID = $this->user->ID;
			$file->isUpload = 1;

			// assign to deposition leader if attendee is member of Trusted Users, otherwise assign to attendee
			$file->createdBy = isset( $trustedUsers[$this->user->ID] ) ? $folder->createdBy : $this->user->ID;

			$file->isExhibit = (in_array( $folder->class, [IEdepoBase::FOLDERS_EXHIBIT, IEdepoBase::FOLDERS_TRANSCRIPT] ) ? 1 : 0);
			$file->isTranscript = ($folder->class == IEdepoBase::FOLDERS_TRANSCRIPT ? 1 : 0);
			Debug::ErrorLog( print_r( $file->getValues(), TRUE ) );
			Debug::ErrorLog( $filepathname );
			$file->insert();

			if( $folder->lastModified != $now ) {
				$folder->lastModified = $now;
				$folder->update();
			}

			// Charge user for uploaded file
			if( !$this->deposition->isDemo() && !$this->deposition->isWitnessPrep() ) {
				DB\ServiceInvoiceCharges::charge( $this->case->clientID, self::PRICING_OPTION_UPLOAD_PER_DOC, 1, $this->deposition->caseID, $this->deposition->ID );
			}
		} catch( \Exception $e ) {
			$results['success'] = FALSE;
			$results['error'] = $e->getMessage();
			Debug::ErrorLog( print_r( $results, TRUE ) );
		}

		//ED-691; Watermark Demo PDFs
		if( $this->deposition->isDemo() && $results['success'] && !$isMultimedia ) {
			$inPath = $file->getFullName();
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID();
			//Debug::ErrorLog( "folderUploadDocument; Watermark --\nin: {$inPath}\nout: {$outPath}\n" );
			if( PDFTools::watermark( $inPath, $outPath ) ) {
				//Debug::ErrorLog( "folderUploadDocument; Watermark -- success" );
				copy( $outPath, $inPath );
				unlink( $outPath );
			} else {
				//Debug::ErrorLog( "folderUploadDocument; Watermark -- failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
			}
		}

		$this->ajax->script( 'var result = '.json_encode( (object)$results ) );
		//$this->ajax->script( "selectFolder({$folder->ID});" );
	}

	public function folderUploadComplete( $folderID, $successCount ) {
		$this->ajax->script( "selectFolder({$folderID});" );
		//$folder = (new DB\ServiceFolders())->getByID( $folderID );
		//$deposition = (new DB\ServiceDepositions())->getByID( $folder->depositionID );
		if( $successCount > 0 ) {
			$this->notifyNodeJSUsers( $folderID );
		}
	}

	public function showMoveItem($ids, $isCopy, $isFile) {
        $dlg = $this->get('moveFilesPopup');
        $dlg->get('errormsg')->text = '';
		$dlg->get('btnCancel')->removeClass( 'inactive' );
		$dlg->get('btnCancel')->onclick = 'closeFileMovePopup()';

		// load options for Case drop-down
		$this->loadCaseList();

		$dlg->get('folderList')->visible = $dlg->get('folderLabel')->visible = ((bool)$isFile);

        // load options for Depositions and Folder (if applicable) drop-downs
		$this->loadDepositionList();

		$action = ($isCopy) ? 'Copy' : 'Move';

		$dlg->tpl->title = "$action to";
		$dlg->get('btnCopy')->value = $action;
		$dlg->get('btnCopy')->onclick = (($isFile) ? 'moveFiles' : 'moveFolder') . "('$ids', $isCopy)";

		$dlg->show();
    }

	public function importToSession() {
		$this->ajax->redirect( "{$this->basePath}/case/session/import?sessionID={$this->deposition->ID}" );
		return TRUE;
	}

	public function loadCaseList() {
		$dlg = $this->get('moveFilesPopup');

		$lastCaseId = null;
		$lastCaseIdFound = false;
		if ( isset( $_SESSION[MOVE_ITEM_TOKEN] ) && isset( $_SESSION[MOVE_ITEM_TOKEN]['caseID'] ) ) {
			$lastCaseId = $_SESSION[MOVE_ITEM_TOKEN]['caseID'];
		}

        $cases = foo(new DB\OrchestraCases())->getCasesByUser($this->user->ID);
		usort( $cases, function( $caseA, $caseB ) {
			$createdA = strtotime( $caseA['created'] );
			$createdB = strtotime( $caseB['created'] );
			return ($createdA > $createdB ? 1 : ($createdA < $createdB ? -1 : 0));
		} );
        $optionCase = array();
        foreach ($cases as $case) {
            $optionCase[$case['ID']] = $case['name'];
			if ( $case['ID'] == $lastCaseId ) {
				$lastCaseIdFound = true;
			}
        }
        $dlg->get('caseList')->options = $optionCase;
		$dlg->get('caseList')->value = ($lastCaseId !== null && $lastCaseIdFound ? $lastCaseId : $cases[0]['ID']);
	}

    public function loadDepositionList() {
        $dlg = $this->get('moveFilesPopup');
        $caseID = $dlg->get('caseList')->value;
        $depositions = null;

		$lastDepoId = null;
		$lastDepoIdFound = false;
		if ( isset( $_SESSION[MOVE_ITEM_TOKEN] ) && isset( $_SESSION[MOVE_ITEM_TOKEN]['depoID'] ) ) {
			$lastDepoId = $_SESSION[MOVE_ITEM_TOKEN]['depoID'];
		}

		if ($this->user->typeID == self::USER_TYPE_CLIENT || $this->isManager)
		{
            $depositions = foo(new DB\OrchestraDepositions())->getAllByCase($caseID);
        } else {
            $depositions = foo(new DB\OrchestraDepositions())->getTrustedDepositions($caseID, $this->user->ID);
        }
		usort( $depositions, function( $depoA, $depoB ) {
			$createdA = strtotime( $depoA['created'] );
			$createdB = strtotime( $depoB['created'] );
			if( $createdA < $createdB ) return 1;
			if( $createdA > $createdB ) return -1;
			if( $depoA['ID'] < $depoB['ID'] ) return 1;
			if( $depoA['ID'] > $depoB['ID'] ) return -1;
			if( $depoA['ID'] == $depoB['ID'] ) return 0;
		} );
        $options = [];
        foreach( $depositions as $depo ) {
			if( $depo['class'] == self::DEPOSITION_CLASS_WPDEMO ) {
				continue;
			}
			$options[$depo['ID']] = ($depo['volume']) ? "{$depo['depositionOf']} (Volume: {$depo['volume']})" : $depo['depositionOf'];
			if ( $depo['ID'] == $lastDepoId ) {
				$lastDepoIdFound = TRUE;
			}
        }
        $dlg->get('depoList')->options = $options;
        $dlg->get('depoList')->value = ($lastDepoId !== null && $lastDepoIdFound ? $lastDepoId : key($options));
        $dlg->get('btnCopy')->visible = (count($options) > 0);
        $this->loadFolderList();
    }

    public function loadFolderList() {
		$dlg = $this->get( 'moveFilesPopup' );
		if( $dlg->get( 'folderList' )->visible == FALSE) {
			return;
		}
		$lastFolderId = NULL;
		$lastFolderIdFound = FALSE;
		if( isset( $_SESSION[MOVE_ITEM_TOKEN] ) && isset( $_SESSION[MOVE_ITEM_TOKEN]['folderID'] ) ) {
			$lastFolderId = $_SESSION[MOVE_ITEM_TOKEN]['folderID'];
		}
		$depoID = $dlg->get( 'depoList' )->value;
		$depo = new DB\Depositions( $depoID );
		$folders = $this->getOrchestraFolders()->getFoldersByUserID( $depoID, $this->user->ID );
		$options = array();
		foreach( $folders as $folder ) {
			if( $folder['class'] != self::FOLDERS_FOLDER && $folder['class'] != self::FOLDERS_PERSONAL && $folder['class'] != self::FOLDERS_TRUSTED ) {
				if( !( $folder['class'] == self::FOLDERS_EXHIBIT && $depo->class == self::DEPOSITION_CLASS_TRIALBINDER) ) {
					continue;
				}
			}
			$options[$folder['ID']] = $folder['name'];
			if ($folder['ID'] === $lastFolderId) {
				$lastFolderIdFound = true;
			}
		}
		$list = $dlg->get('folderList');
		$list->options = $options;
		$list->value = ($lastFolderId !== null && $lastFolderIdFound ? $lastFolderId : key($options));
		$dlg->get('btnCopy')->visible = (count($options) > 0);
    }

	public function closePopup( $id )
	{
		$this->get( $id )->hide();
	}

	public function moveFolder( $id, $depoID )
	{
		$oldFolder = new DB\Folders( $id );
		$deposition = new DB\Depositions( $depoID );
		//trustedUser
		$createdBy = ($this->isTrustedUser ? $deposition->createdBy : $this->user->ID);

		$oldFolder->depositionID = $depoID;
		$oldFolder->createdBy = $createdBy;
		$oldFolder->save();
		$this->notifyNodeJSUsers( $oldFolder->ID );
		$node = new Utils\NodeJS( TRUE );
		$node->notifyUsersRemovedFolder( $oldFolder->ID, [$this->deposition->ID] );
		$this->refreshFolders();
		$this->ajax->script( "selectFolder();" );
    }

	public function copyFolder( $folderID, $depoID ) {
		$srcFolder = new DB\Folders( $folderID );
		if( !$srcFolder || !$srcFolder->ID || $srcFolder->ID != $folderID ) {
			Debug::ErrorLog( "PageSessionView::copyFolder; Folder not found by ID: {$folderID}" );
			return;
		}
		$deposition = new DB\Depositions( $depoID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
			Debug::ErrorLog( "PageSessionView::copyFolder; Session not found by ID: {$depoID}" );
			return;
		}

		$newFolder = $srcFolder->copy();
		$newFolder->caseID = NULL;
		$newFolder->depositionID = $depoID;
		$newFolder->created = date( 'Y-m-d H:i:s' );
		$newFolder->class = ($this->isTrustedUser ? self::FOLDERS_TRUSTED : self::FOLDERS_FOLDER);
		$newFolder->createdBy = ($this->isTrustedUser ? $deposition->createdBy : $this->user->ID);

		switch( $newFolder->class ) {
			case IEdepoBase::FOLDERS_EXHIBIT:
				if( !$srcFolder->isCaseFolder() ) {
					$newFolder->name = "{$this->deposition->depositionOf} Exhibits";
				}
				break;
			case IEdepoBase::FOLDERS_TRANSCRIPT:
			case IEdepoBase::FOLDERS_PERSONAL:
			case IEdepoBase::FOLDERS_WITNESSANNOTATIONS:
				$siblings = $this->getOrchestraFolders()->getFolderNameMapByDepo( $deposition->ID, $newFolder->createdBy );
				$siblings[$newFolder->name] = true;	// force conflict
				$newFolder->name = DB\Folders::getFirstUniqueName( $newFolder->name, $siblings );
				break;
		}
		$newFolder->insert();

		$copyFileIDs = [];
		foreach( $srcFolder->files as $file ) {
			$copyFileIDs[] = $file->ID;
		}
		$this->ajax->script( 'copyFolderFiles( '.$newFolder->ID.', "'.implode( ',', $copyFileIDs ).'");' );
	}

	public function copyFolderFile( $newFolderID, $oldFileID )
	{
		$newFolder = new DB\Folders( $newFolderID );
		if( !$newFolder || !$newFolder->ID || $newFolder->ID != $newFolderID ) {
			Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . " -- Folder not found by ID: {$newFolderID}" );
			return;
		}
		$oldFile = new DB\Files( $oldFileID );
		if( !$oldFile || !$oldFile->ID || $oldFile->ID != $oldFileID ) {
			Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . " -- File not found by ID: {$oldFileID}" );
			return;
		}
		$oldDepoID = $oldFile->folders[0]->depositionID;
		$newDepoID = $newFolder->depositionID;
		$now = date( 'Y-m-d H:i:s' );

		$newFile = $oldFile->copy();
		$newFile->folderID = $newFolder->ID;
		$newFile->createdBy = $newFolder->createdBy;
		$newFile->isExhibit = 0;
		$newFile->sourceUserID = $this->user->ID;
		$newFile->created = $now;
		$newFile->sourceID = $this->getOrchestraFiles()->getSourceFileID( $oldFile->ID );
		if( $oldDepoID != $newDepoID ) {
			$newFile->sourceID = NULL;
		}
		$newFile->insert();
		$newFolder->lastModified = $now;
		$newFolder->update();
	}

	public function prepareMoveFiles( $fv ) {
		//Debug::ErrorLog( print_r( ['prepareMoveFiles'=>$fv], TRUE ) );
		$sessionID = (int)$fv['sessionID'];
		$folderID = (int)$fv['folderID'];
		$fileIDs = $fv['fileIDs'];
		$isCopy = (bool)$fv['copy'];

		$session = new DB\Depositions( $sessionID );
		$folder = new DB\Folders( $folderID );

		$validParams = TRUE;
		if( !$session || !$session->ID || $sessionID != $sessionID ) {
			Debug::ErrorLog( "PageSessionView::prepareMoveFiles; Invalid sessionID: {$fv['sessionID']}" );
			$validParams = FALSE;
		}
		if( !$folder || !$folder->ID || $folder->ID != $folderID || ($folder->depositionID != $sessionID && $folder->caseID != $session->caseID) ) {
			Debug::ErrorLog( "PageSessionView::prepareMoveFiles; Invalid folderID: {$fv['folderID']}" );
			$validParams = FALSE;
		}
		if( !is_array( $fileIDs ) || !$fileIDs ) {
			Debug::ErrorLog( "PageSessionView::prepareMoveFiles; Invalid fileIDs" );
			$validParams = FALSE;
		}

		if( !$validParams ) {
			$popup = $this->get( 'moveFilesPopup' );
			$btnCopy = $popup->get('btnCopy');
			$btnCopy->value = 'Close';
			$btnCopy->onclick = 'closePopup("moveFilesPopup");';
			$btnCopy->removeClass( 'inactive' );
			$btnCancel = $popup->get( 'btnCancel' );
			$btnCancel->onclick = null;
			$btnCancel->addClass( 'inactive' );

			$dlg = $this->get( 'noticePopup' );
			$dlg->tpl->header = 'Invalid Request';
			$dlg->tpl->title = '';
			$dlg->tpl->message = 'Error: Unable to valididate your request.';
			$dlg->get( 'btnCancel' )->value = 'Close';
			$dlg->show();
			return;
		}

		$filesizeTest = TRUE;
		if( $session->class != self::DEPOSITION_CLASS_TRIALBINDER ) {
			foreach( $fileIDs as $fileID ) {
				$file = new DB\Files( (int)$fileID );
				if( $file->filesize > $this->config->maxFilesize ) {
					$filesizeTest = FALSE;
				}
			}
		}


		if( !$filesizeTest ) {
			$popup = $this->get( 'moveFilesPopup' );
			$btnCopy = $popup->get('btnCopy');
			$btnCopy->removeClass( 'inactive' );
			$btnCopy->update();
			$btnCancel = $popup->get( 'btnCancel' );
			$btnCancel->onclick = null;
			$btnCancel->addClass( 'inactive' );
			$this->ajax->script( '$("#actionProgress").css("display","none"); $("#list_' . ID('caseList') . '").css( "visibility", "" );$("#list_' . ID('depoList') .'").css( "visibility", "" );$("#list_' . ID('folderList') .'").css( "visibility", "" );' );

			$hrSize = Utils::readableFilesize( $this->config->maxFilesize, 0 );
			$dlg = $this->get( 'noticePopup' );
			$dlg->tpl->header = '';
			$dlg->tpl->title = 'Filesize limit exceeded';
			$dlg->tpl->message = "One or more of the selected files exceeds the size limit ({$hrSize}) for the selected session.";
			$dlg->get( 'btnCancel' )->value = 'Close';
			$dlg->show();
		} else {
			$this->ajax->script( "fileMove();" );
		}
	}

	public function moveFile( $fileID, $folderID, $isCopy ) {
		Debug::ErrorLog( print_r( ['method'=>'moveFiles', 'fileID'=>$fileID, 'folderID'=>$folderID, 'isCopy'=>$isCopy], TRUE ) );
		$srcFile = new DB\Files( $fileID );
		if( !$srcFile || !$srcFile->ID || $srcFile->ID != $fileID ) {
			return;
		}
		$srcFolder = $srcFile->folders[0];
		$dstFolder = new DB\Folders( $folderID );
		if( !$dstFolder || !$dstFolder->ID || $dstFolder->ID != $folderID ) {
			return;
		}

		$removeSourceID = FALSE;
		if( $srcFolder->isCaseFolder() || $dstFolder->isCaseFolder() ) {
			$srcCase = $srcFolder->cases[0];
			$dstCase = $dstFolder->cases[0];
			if( $srcCase && $srcCase->ID && $dstCase && $dstCase->ID ) {
				$removeSourceID = ($srcCase->ID != $dstCase->ID);
			} elseif( $srcCase && $srcCase->ID ) {
				$dstSession = $dstFolder->depositions[0];
				$removeSourceID = ($srcCase->ID != $dstSession->caseID);
			} elseif( $dstCase && $dstCase->ID ) {
				$srcSession = $srcFolder->depositions[0];
				$removeSourceID = ($srcSession->caseID != $dstCase->ID);
			}
		} else {
			$srcSession = $srcFolder->depositions[0];
			$dstSession = $dstFolder->depositions[0];
			if( $srcFile->filesize > $this->config->maxFilesize && $srcSession->class == self::DEPOSITION_CLASS_TRIALBINDER && $dstSession->class != self::DEPOSITION_CLASS_TRIALBINDER ) {
				Debug::ErrorLog( print_r( ['PageSessionView::moveFile; too large:', $srcFile->getValues()], TRUE ) );
				return;
			}
			$removeSourceID = ($srcSession->ID != $dstSession->ID);
		}

		if( $this->isTrustedUser ) {
			$createdBy = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
		} else {
			$createdBy = $this->user->ID;
		}

		if( $isCopy ) {
			$newFile = $srcFile->copy();
			$newFile->folderID = $dstFolder->ID;
			$newFile->isExhibit = ($dstFolder->class == IEdepoBase::FOLDERS_EXHIBIT);
			$newFile->createdBy = $createdBy;
			$newFile->sourceID = ($removeSourceID ? NULL : $this->getOrchestraFiles()->getSourceFileID( $srcFile->ID ));
			$newFile->insert();
		} else {
			$srcFile->folderID = $dstFolder->ID;
			$srcFile->isExhibit = ($dstFolder->class == IEdepoBase::FOLDERS_EXHIBIT);
			$srcFile->createdBy = $createdBy;
			$srcFile->update();
		}

		// apply watermark if needed
		if( !$this->deposition->isDemo() ) {
			if( ($dstFolder->isCaseFolder() && $dstCase->isDemo()) || (!$dstFolder->isCaseFolder() && $dstSession->isDemo()) ) {
				$this->watermarkPDFOnMove( $newFile, IEdepoBase::DEPOSITION_CLASS_DEMO, $this->deposition->class );
			}
		}
		$dstFolder->lastModified = date( 'Y-m-d H:i:s' );
		$dstFolder->update();
	}

    public function watermarkPDFOnMove( \ClickBlocks\DB\Files $file, $newClass, $oldClass ) {
		if( $newClass == 'Demo' && $oldClass != 'Demo' ) {
			$inPath = $file->getFullName();
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . 'pdf';
			if( PDFTools::watermark( $inPath, $outPath ) ) {
				copy( $outPath, $inPath );
				unlink( $outPath );
			} elseif( file_exists( $outPath ) ) {
				unlink( $outPath );
			}
		}
    }

	public function moveFilesFinalize( $folderID, $refreshFileList )
	{
		$dlg = $this->get('moveFilesPopup');

		// save the choices used to session to be re-used on next move attempt
		$lastmove = array(
			'caseID' => $dlg->get( 'caseList' )->value,
			'depoID' => $dlg->get( 'depoList' )->value,
		);
		$_SESSION[MOVE_ITEM_TOKEN] = $lastmove;

		$dlg->get('btnCopy')->value = 'Close';
		$dlg->get('btnCopy')->onclick = 'closePopup("moveFilesPopup");';
		$dlg->get('btnCopy')->removeClass( 'inactive' );

		$dlg->get('btnCancel')->onclick = null;
		$dlg->get('btnCancel')->addClass( 'inactive' );

		if (isset( $folderID ))
		{
			$this->notifyNodeJSUsers( $folderID );

			$folder = new DB\Folders( $folderID );

			if ($this->deposition->ID == $folder->depositionID) {
				$this->refreshFolders();
				$this->ajax->script( "selectFolder({$folder->ID});" );
			} else if ($refreshFileList) {
				$this->ajax->script( "selectFolder(selectedFolderID);" );
			}
		}
	}

	public function notifyMovedFiles( $folderID, $dstSessionID )
	{
		$folder = new DB\Folders( $folderID );
		$srcSessionID = (int)$folder->depositionID;
		$dstSessionID = (int)$dstSessionID;
		if( $srcSessionID === $dstSessionID ) {
			return;
		}
		$nodeJS = new NodeJS( TRUE );
		$nodeJS->notifyUsersRemovedFile( [$folderID] );
	}

	public function showAddPlaceholder( $folderID )
	{
		$dlg = $this->get( 'addPlaceholderPopup' );
		$dlg->get( 'filename' )->value = '';
		$dlg->get( 'description' )->text = '';
		$dlg->tpl->filenameError = FALSE;
		$dlg->tpl->folderListError = FALSE;
		$dlg->get( 'btnOK' )->onclick = 'addPlaceholder()';
		$lastFolderID = (isset( $folderID )) ? $folderID : FALSE;
		$folders = $this->getOrchestraFolders()->getFoldersByUserID( $this->deposition->ID, $this->user->ID );
		$options = [];
		foreach( $folders as $folder ) {
			if( $folder['class'] == 'Personal' || $folder['class'] == 'Folder' || $folder['class'] == 'Trusted') {
				$options[$folder['ID']] = $folder['name'];
			}
		}
		asort( $options );
		$folderList = $dlg->get( 'phFolderList' );
		$folderList->options = $options;
		$folderList->value = (isset( $options[$lastFolderID] )) ? $options[$lastFolderID] : reset( $options );
		$dlg->get('btnOK')->visible = (count( $options ) > 0);
		$dlg->show();
	}

	public function addPlaceholder( $fv )
	{
		$dlg = $this->get( 'addPlaceholderPopup' );
		$dlg->get( 'phFolderList' )->value = $fv['phFolderList'];
		$hasErrors = FALSE;
		if( !preg_match( '/^[a-zA-Z0-9_\s-]+$/', $fv['filename'] ) ) {
			$dlg->tpl->filenameError = 'ERROR: Invalid file name';
			$hasErrors = TRUE;
		} else {
			$dlg->tpl->filenameError = FALSE;
		}
		if( !preg_match( '/^[a-zA-Z0-9_\s-@()#!,.&+=\'-^]+$/', $fv['phFolderList'] ) ) {
			$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
			$hasErrors = TRUE;
		} else {
			$dlg->tpl->folderListError = FALSE;
		}
		if( Edepo::isReservedFolder( $fv['phFolderList'], TRUE ) ) {
			$dlg->tpl->folderListError = 'ERROR: Invalid folder name';
			$hasErrors = TRUE;
		}
		if( $hasErrors ) {
			$dlg->update();
			$dlg->show();
			return;
		}
		$now = date( 'Y-m-d H:i:s' );
		$folders = $this->getOrchestraFolders()->getFoldersByName( $this->deposition->ID, $fv['phFolderList'] );
		if( $folders ) {
			$folder = new DB\Folders( $folders[0]['ID'] );
			$dlg->get( 'phFolderList' )->value = $folder->name;
		} else {
			$folder = new DB\Folders();
			$folder->class = 'Folder';
			$folder->name = $fv['phFolderList'];
			$deposition = $this->getService( 'Depositions' )->getByID( $this->deposition->ID );
			$trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap( $deposition->ID );
            $folder->created = $now;
			$folder->lastModified = $now;
            $folder->createdBy = ( isset( $trustedUsers[$this->user->ID] ) ) ? $deposition->createdBy : $this->user->ID;
            $folder->depositionID = $deposition->ID;
			$folder->insert();
		}
//		$this->subSession['LAST_FOLDER_ID'] = $folder->ID;
		$dlg->update();
		$dlg->show();

		$tmpHTML = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.html';
		$tmpPDF = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';
		$htmlString = '<html><body>
			<div style="font-family:sans-serif;text-align:center;font-size:36px;">
				<span>Placeholder for Exhibit</span>
			</div>
			<div style="margin:50px 20px 20px 20px;border:1px solid #000;">
				<p style="padding:10px;margin:0;">' . $fv['description'] . '</p>
			</div>
			</body></html>';
		$fh = fopen( $tmpHTML, 'w+' );
		if( flock( $fh, LOCK_EX ) ) {
			fwrite( $fh, $htmlString );
			fflush( $fh );
			flock( $fh, LOCK_UN );
		}
		fclose( $fh );
		$pdf = new Utils\WKHTMLToPDF( $tmpHTML, $tmpPDF );
		$pdf->setPageSize( 'Letter' );
		$pdf->setOrientation( 'Portrait' );
		$pdf->addParam( '--no-background', TRUE );
		$okToGo = $pdf->render();
		unlink( $tmpHTML );
		if( !$okToGo ) {
			unlink( $tmpPDF );
			$dlg->tpl->filenameError = 'ERROR: An error occurred while creating the PDF';
			$dlg->update();
			$dlg->show();
			return;
		}

		// Is this a demo? Set watermark
		if( (new DB\Depositions($this->deposition->ID))->class == 'Demo' ) {
			$wmPDF = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';
			$result = PDFTools::watermark( $tmpPDF, $wmPDF );
			if( $result === true ) {
				copy( $wmPDF, $tmpPDF );
				unlink( $wmPDF );
			}
		} else {
			//clean wkhtmltopdf generated PDFs
			$pdfConverter = new EDPDFConvert();
			if( !$pdfConverter->pdfCleanup( $tmpPDF ) ) {
				$dlg->tpl->filenameError = 'ERROR: An error occurred while cleaning up the PDF';
				$dlg->update();
				$dlg->show();
				return;
			}
			unset( $pdfConverter );
		}

		$file = new DB\Files();
		$file->name = $fv['filename'] . '.pdf';
		$file->folderID = $folder->ID;
		$file->createdBy = $folder->createdBy;
		$file->created = $now;
		$file->setSourceFile( $tmpPDF );
		$file->insert();
		if( $file->ID ) {
			$dlg->hide();
			$this->ajax->reload();
		}
		$folder->lastModified = $now;
    	$this->notifyNodeJSUsers( $folder->ID );
		$this->ajax->script( "selectFolder({$folder->ID});" );
	}

	public function showRequestLiveTranscript()
	{
		$dlg = $this->get( 'requestLiveTranscriptPopup' );
		$userClient = $this->user->clients[0];
		$isEnterprise = ($userClient->typeID == self::CLIENT_TYPE_ENT_CLIENT);
		if( $isEnterprise ) {
			$depoID = $this->deposition->ID;
			$deposition = new DB\Depositions( $depoID );
			if( !$deposition || !$deposition->ID || $deposition->ID != $depoID || $deposition->class != self::DEPOSITION_CLASS_DEPOSITION ) {
				$dlg->get( 'errormsg' )->text = 'Unable to send request.';
				return FALSE;
			}
			$reseller = $deposition->enterpriseReseller[0];
		}
		$dlg->get( 'reporterEmail' )->value = $isEnterprise ? $reseller->liveTranscriptsEmail : $userClient->liveTranscriptsEmail;
		$dlg->get( 'errormsg' )->text = '';
		$dlg->get( 'btnOK' )->onclick = 'sendRequestLiveTranscript()';
		$dlg->show();
	}

	public function sendRequestLiveTranscript()
	{
		$dlg = $this->get( 'requestLiveTranscriptPopup' );
		$dlg->get( 'errormsg' )->text = '';

		$depoID = (int)$this->deposition->ID;
		$deposition = new DB\Depositions( $depoID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depoID || $deposition->class != self::DEPOSITION_CLASS_DEPOSITION ) {
			$dlg->get( 'errormsg' )->text = 'Unable to send request.';
			return FALSE;
		}

		if( $deposition->statusID == self::DEPOSITION_STATUS_FINISHED ) {
			$dlg->get( 'errormsg' )->text = 'Unable to send request for a finished session.';
			return FALSE;
		}

		if( !$this->validators->isValid( 'livetranscripts' ) ) {
			return;
		}

		//okay to go
		$deposition->liveTranscript = 'Y';
		$deposition->update();

		$reporterEmail = $dlg->get( 'reporterEmail' )->value;

		if( $reporterEmail === NULL || strlen( $reporterEmail ) < 1 ) {
			$dlg->get( 'errormsg' )->text = 'Please enter a valid email address.';
			return FALSE;
		}
		Utils\EmailGenerator::sendRequestLiveTranscript( $deposition, $this->user, $reporterEmail );

		$dlg->get( 'reporterEmail' )->value = '';
		$dlg->hide();

		$notice = $this->get( 'noticePopup' );
		$notice->tpl->title = 'Request Sent';
		$message = "Your request has been sent to: <strong>{$reporterEmail}</strong>";
		$notice->tpl->message = $message;
		$notice->show();
	}

	public function showWitnessInvite()
	{
		$dlg = $this->get( 'sendWitnessInvitePopup' );
		$dlg->get( 'errormsg' )->text = '';
		$dlg->get( 'btnOK' )->onclick = 'sendWitnessInvite()';
		$dlg->show();
	}

	public function sendWitnessInvite()
	{
		$dlg = $this->get( 'sendWitnessInvitePopup' );
		$dlg->get( 'errormsg' )->text = '';

		$depoID = (int)$this->deposition->ID;
		$deposition = new DB\Depositions( $depoID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depoID || !$deposition->isWitnessPrep() ) {
			$dlg->get( 'errormsg' )->text = 'Unable to send invitation.';
			return FALSE;
		}

		if( $deposition->statusID == self::DEPOSITION_STATUS_FINISHED ) {
			$dlg->get( 'errormsg' )->text = 'Unable to send invitation for a finished session.';
			return FALSE;
		}

		if( !$this->validators->isValid( 'witnessinvite' ) ) {
            return;
		}

		//okay to go
		$didStart = FALSE;
		if( $deposition->statusID == self::DEPOSITION_STATUS_NEW ) {
			if( $deposition->class == self::DEPOSITION_CLASS_WPDEMO ) {
				if( $this->user->ID != $deposition->ownerID ) {
					$redis = new CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
					$isOwned = $redis->get( 'demo:' . $deposition->ID );
					if( !$isOwned ) {
						$deposition->setDemoOwner( $this->user->ID );
					}
				}
			}
			$deposition->start();
			$didStart = TRUE;
			$nodeJS = new NodeJS( TRUE );
			$nodeJS->sendPostCommand( 'deposition_start', NULL, ['date'=>date( 'Y-m-d H:i:s' ), 'depositionID'=>$deposition->ID] );
			$nodeJS->notifyCasesUpdated( $deposition->caseID, $deposition->ID );
			if( !$this->getOrchestraDepositionAttendees()->checkUserIsDepositionAttendee( $deposition->ID, $deposition->ownerID ) ) {
				$attendee = new DB\DepositionAttendees();
				$attendee->depositionID = $deposition->ID;
				$attendee->userID = $deposition->ownerID;
				$attendee->role = self::DEPOSITIONATTENDEE_ROLE_MEMBER;
				$attendee->insert();
				$attendee->onFirstAttendedDepo( $deposition );
				$client = $deposition->getClient();
				DB\ServiceInvoiceCharges::charge( $client->ID, self::PRICING_OPTION_WITNESSPREP_ATTENDEE, 1, $deposition->caseID, $deposition->ID, $deposition->ownerID );
				DB\ServicePlatformLog::addLog( $client->resellerID, self::PRICING_OPTION_WITNESSPREP_ATTENDEE, 1, $client->ID, $deposition->ownerID, $deposition->caseID, $deposition->ID );
			}
		}

		$witnessEmail = $dlg->get( 'witnessEmail' )->value;

		if( $witnessEmail === NULL || strlen( $witnessEmail ) < 1 ) {
			$dlg->get( 'errormsg' )->text = 'Please enter a valid email address.';
			return FALSE;
		}
		Utils\EmailGenerator::sendWitnessInvite( $deposition, $this->user, $witnessEmail );

		$dlg->get( 'witnessEmail' )->value = '';
		$dlg->hide();

		$notice = $this->get( 'noticePopup' );
		$notice->tpl->title = 'Invitation Sent';
		$message = "Your invitation has been sent to: <strong>{$witnessEmail}</strong>";
		if( $didStart ) {
			$message .= "<br/><br/><em>This Witness Prep session has been automatically started.</em>";

			$depoInfoPanel = $this->get('depoInfoPanel');
			$depoInfoPanel->tpl->statusID = $deposition->statusID;
			$depoInfoPanel->update();
		}
		$notice->tpl->message = $message;
		$notice->show();
	}

	function viewFile( $fileID ) {
		$fileID = (int)$fileID;
		$file = new DB\Files( $fileID );
        if( !$file || !$file->ID || $file->ID != $fileID ) {
            return;
        }
		$this->checkUserAccess( NULL, $file->folderID );

		if( !$file->mimeType ) {
			$file->setMetadata();
		}
		$mimeType = $file->mimeType;
		if( mb_stristr( $mimeType, 'audio' ) ) {
			$dlg = $this->get( 'viewAudioPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} elseif( mb_stristr( $mimeType, 'video' ) ) {
			$dlg = $this->get( 'viewVideoPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} else {
			$dlg = $this->get( 'viewFilePopup' );
			$dlg->tpl->src = "https://{$this->config->pdfviewer['host']}{$this->config->pdfviewer['path']}{$fileID}/viewer/";
		}
		$dlg->tpl->title = $file->name;
		$dlg->show();
    }

	function notifyNodeJSUsers( $folderID ) {
		//getNodeJS()
		$node = new Utils\NodeJS( TRUE );
		$node->notifyDepositionUpload( $folderID, $this->user->ID, $this->deposition->ID );
	}

	private function checkUserAccess( $depositionID=NULL, $folderID=NULL )
	{
		$depositionID = (int)$depositionID;
		$folderID = (int)$folderID;
		$isAllowed = FALSE;
		if ( $depositionID ) {
			$isAllowed = $this->getOrchestraDepositions()->checkUserAccessToDeposition( $this->user->ID, $depositionID );
			if (!$isAllowed) {
				throw new \LogicException( "Unable to find Deposition ID ({$depositionID})" );
			}
		} else {
			$folder = new DB\Folders( $folderID );
			if( !$this->isTrustedUser ) {
				if( $folder->class == self::FOLDERS_EXHIBIT ) {
					$folderCreator = new DB\Users( $folder->createdBy );
					if( $this->case->clientID !== $folderCreator->clientID ) {
						throw new \LogicException( "Unable to find Exhibit Folder ID ({$folderID})" );
					}
				} elseif( $folder->createdBy != $this->user->ID ) {
					throw new \LogicException( "Unable to find Folder ID ({$folderID})" );
				}
			}
		}
	}

	function downloadFolder( $folderID ) {
		if( !$this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "PageSessionView::downloadFolder; permission denied to folder: {$folderID}" );
			return;
		}
        $folder = new DB\Folders( $folderID );
		$fileIDs = [];
		foreach( $folder->files as $file ) {
			$fileIDs[$file->ID] = ['name'=>$file->name, 'path'=>$file->getFullName()];
		}
		if( !$fileIDs ) {
		   $this->showMessage( 'Folder is empty!', 'Download folder' );
		   return;
		}
		$fullName = Core\IO::dir( 'temp' ) . '/' . Utils::createUUID() . '.zip';
		$zip = new \ZipArchive();
		$didOpen = $zip->open( $fullName,  \ZipArchive::CREATE | \ZipArchive::OVERWRITE );
		if( $didOpen !== TRUE ) {
			Debug::ErrorLog( "Unable to create ZipArchive: {$fullName}" );
			return;
		}
		foreach( $fileIDs as $fData ) {
			$zip->addFile( $fData['path'], $fData['name'] );
		}
		$zip->close();
		$this->ajax->downloadFile( $fullName, "{$folder->name}.zip" );
    }

    function downloadSelectedFiles( $fileIDs ) {
        $this->checkUserAccess( $this->deposition->ID );
		$orcFolders = $this->getOrchestraFolders();
		$accessToFolders = [];
		$files = [];
        foreach( explode( ",", $fileIDs ) as $fileID ) {
            $file = new DB\Files( $fileID );
            if( !$file->ID || (!isset( $accessToFolders[$file->folderID] ) && !$orcFolders->checkPermissionToFolder( $this->user->ID, $file->folderID )) ) {
				continue;
			}
			$accessToFolders[$file->folderID] = TRUE;
			$files[$file->ID] = [
				'name'=>$file->name,
				'path'=>$file->getFullName()
			];
        }

		if( !$files ) {
			Debug::ErrorLog( "No files to download" );
			return;
		}
		$singleFile = (count( $files ) === 1);
		$fullName = '';
		$downloadAs = '';
		if( $singleFile ) {
			$fData = array_shift( $files );
			$fullName = $fData['path'];
			$downloadAs = $fData['name'];
		} else {
			$fullName = Core\IO::dir( 'temp' ) . '/' . Utils::createUUID() . '.zip';
			$zip = new \ZipArchive();
			$didOpen = $zip->open( $fullName,  \ZipArchive::CREATE | \ZipArchive::OVERWRITE );
			if( $didOpen !== TRUE ) {
				Debug::ErrorLog( "Unable to create ZipArchive: {$fullName}" );
				return;
			}
			foreach( $files as $fData ) {
				$zip->addFile( $fData['path'], $fData['name'] );
			}
			$zip->close();
			$downloadAs = "{$this->deposition->depositionOf}.zip";
		}
		$this->ajax->downloadFile( $fullName, $downloadAs );
    }

	function downloadFile($id) {
        $file = (new DB\ServiceFiles())->getByID($id);
        if (!$file->ID) {
            return;
        }
        $this->checkUserAccess( NULL, $file->folderID );

        $this->ajax->downloadFile($file->getFullName(), $file->name);
    }

	function downloadDeposition()
	{
        $deposition = (new DB\ServiceDepositions())->getByID( $this->deposition->ID );
        $this->checkUserAccess( $deposition->ID );

        $fullName = Core\IO::dir('temp').'/'.$deposition->depositionOf.'_'.time().'.zip';
        $zip = new \ZipArchive();
        $zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
		$this->addDepositionToArchive( $zip, $deposition->ID );
        $zip->close();
        if (!file_exists($fullName))
		{
			$this->showMessage('Deposition is empty!', 'Download Deposition');
		} else {
			$this->ajax->downloadFile( $fullName, $deposition->depositionOf.'.zip' );
		}
    }

	private function addDepositionToArchive( \ZipArchive &$archive, $depositionID, $caseZip=false )
	{
		$deposition = (new DB\ServiceDepositions())->getByID( $depositionID );
		$folders = (new DB\OrchestraFolders())->getFoldersByDepo( $deposition->ID );
		foreach ($folders as $folder)
		{
			if ($folder['class'] == DB\Folders::CLASS_COURTESYCOPY)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_WITNESSANNOTATIONS && $this->user->ID != $deposition->ownerID)
			{
				continue;
			}
			if ($folder['class'] == DB\Folders::CLASS_PERSONAL && $this->user->ID != $folder['createdBy'])
			{
				continue;
			}
			$files = (new DB\OrchestraFiles())->getFilesByFolder( $folder['ID'] );
			foreach ($files as $file)
			{
				$f = (new DB\ServiceFiles())->getByID( $file['ID'] );
				$user = (new DB\ServiceUsers())->getByID( $f->createdBy );
				$client = (new DB\ServiceClients())->getByID( $this->case->clientID );
				if( $user->clientID !== $this->case->clientID && ( $user->clientID !== $client->resellerID && $f->createdBy !== $user->ID && $deposition->isDemo() ) )
				{
					continue;
				}
				$folderPath = $user->email.'/'.$folder['name'].'/'.$f->name;
				if ($caseZip)
				{
					// remove any symbols that cause conflict with folder/file naming conventions
					$depositionName = preg_replace( '/[<_>:"\/\\\|\?\*]/', '', $deposition->depositionOf );
					$folderPath = ($depositionName.'_'.$deposition->volume.'_'.$deposition->ID).'/'.$folderPath;
				}
				$archive->addFile( $f->getFullName(), $folderPath );
			}
		}
	}

	public function confirmDeleteDepo($id) {
		$depo = $this->getService('Depositions')->getByID($id);
		if (0 == $depo->ID)
		  return;
		$param = array();
		$param['title'] = 'Delete Session';
		$param['message'] = 'Are you sure you want to delete this session and all its contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteDepo({$depo->ID}, 1);";
		$this->showPopup('confirm', $param);
	}

	public function deleteDepo( $depositionID )
	{
		$depositionID = (int)$depositionID;
		$this->hidePopup( 'confirm' );
		$depo = $this->getService( 'Depositions' )->getByID( $depositionID );
		if( !$depo->ID || $depo->ID != $depositionID ) {
			return;
		}
		$depo->setAsDeleted();
		$nodeJS = new NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $this->case->ID );
		$this->ajax->redirect( $this->basePath . '/case/view?ID=' . $this->case->ID );
	}

	public function confirmResetDemo( $id )
	{
		$param = array();
		$param['title'] = 'Reset Demonstration?';
		$param['message'] = 'Are you sure you want to reset this demonstration session?';
		$param['OKName'] = 'Reset';
		$param['OKMethod'] = "resetDemoSession('{$id}', 1);";
		$this->showPopup( 'confirm', $param );
	}

	public function resetDemoSession( $depositionID )
	{
		$deposition = new DB\Depositions( $depositionID );
		if( $deposition->ID != $depositionID ) {
			return;
		}
		$this->hidePopup( 'confirm' );
		$deposition->resetDemoSession( $deposition->createdBy );
		$this->ajax->redirect( $this->basePath . "/session/view?caseID={$deposition->caseID}&depoID={$deposition->ID}" );
	}

	public function depoExhibitLog()
	{
		$deposition = new DB\Depositions( $this->deposition->ID );
		$rows = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $deposition->ID, TRUE );
		$dlg = $this->get( 'exhibitLogPopup' );
		$dlg->tpl->type = 'depo';
		$dlg->tpl->rows = $rows;
		$dlg->tpl->headingName = $deposition->depositionOf . ($deposition->volume ? " (vol: {$deposition->volume})" : '');
		$dlg->get( 'btnExport' )->visible = (count( $rows ));
		$dlg->get( 'btnExportWithExhibits' )->visible = (count( $rows ));
		$dlg->update();
		$dlg->show();
	}

	public function exportExhibitLog()
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.csv';
		$fh = fopen( $outPath, 'w' );
		if( $dlg->tpl->type == 'case' ) {
			fputcsv( $fh, ['Time','Deposition Of', 'Volume','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['depositionOf'], $row['volume'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		} else {
			fputcsv( $fh, ['Time','Exhibit Name','Introduced By','Original File Name'] );
			foreach( $rows as $row ) {
				$data = [$row['lIntroducedDate'], $row['exhibitFilename'], $row['introducedBy'], $row['sourceFilename'] ];
				fputcsv( $fh, $data );
			}
		}
		fclose( $fh );

        if( file_exists( $outPath ) ) {
			$uid = ($dlg->tpl->type == 'case') ? "case_{$rows[0]['caseID']}" : "deposition_{$rows[0]['depositionID']}";
			$this->ajax->downloadFile( $outPath, "{$uid}_exhibit_logs.csv", 'application/octet-stream', FALSE );
		} else {
			$this->showMessage( 'Error generating export', 'Export Exhibit Log' );
		}
	}

	public function exportExhibitLogWithExhibits()
	{
		$dlg = $this->get( 'exhibitLogPopup' );

		$folderIDs = [];
		if ( $dlg->tpl->type == 'case' ) {
			$rows = $dlg->tpl->rows;
			$depoIDs = [];
			foreach ( $rows as $row ) {
				$depoIDs[$row['depositionID']] = $row['depositionID'];
			}
			foreach ( $depoIDs as $depoID ) {
				$deposition = new DB\Depositions( $depoID );
				if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
					continue;
				}
				$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
				$folderIDs[] = $officialExhibitsFolder['ID'];
			}
			$exhibitLogXLSPath = $this->createExhibitLogXLS(true);
			$downloadFolderName = $this->case->name . " Log and Exhibits";
		} else {
			$depoID = $this->deposition->ID;
			$deposition = new DB\Depositions( $depoID );
			if ( !$deposition || !$deposition->ID || $deposition->ID != $depoID ) {
				return;
			}
			$officialExhibitsFolder = $this->getOrchestraFolders()->getExhibitFolder( $deposition->ID );
			$folderIDs[] = $officialExhibitsFolder['ID'];
			$downloadFolderName = $deposition->depositionOf . " ID-" . $deposition->ID . " vol-" . $deposition->volume;
			$exhibitLogXLSPath = $this->createExhibitLogXLS(false);
		}
		if (!$folderIDs) {
			return;
		}

		$this->downloadExhibitLogFolder($folderIDs, $downloadFolderName, $exhibitLogXLSPath);
	}

	public function createExhibitLogXLS($isCaseLog)
	{
		if ($isCaseLog) {
			$template = '/_templates/caseexhibitlog.xls';
		} else {
			$template = '/_templates/exhibitlog.xls';
		}
		$xls = \PHPExcel_IOFactory::load(Core\IO::dir('backend') . $template);
        $sheet = $xls->setActiveSheetIndex(0);
        $sheet->setTitle('Exhibit Log');
		$this->fillExhibitLogSpreadSheet( $sheet, $isCaseLog );
        $writer = new \PHPExcel_Writer_Excel5($xls);
        $fileName = Core\IO::dir('temp').'/'.Utils::createUUID().'.xls';
        $writer->save($fileName);
        return $fileName;
	}

	public function fillExhibitLogSpreadSheet(\PHPExcel_Worksheet $sheet, $isCaseLog)
	{
		$dlg = $this->get( 'exhibitLogPopup' );
		$rows = $dlg->tpl->rows;
		if( !is_array( $rows ) || !$rows ) return;

		if ($isCaseLog) {
			$colValueIndexes = ['lIntroducedDate', 'depositionOf', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		} else {
			$colValueIndexes = ['lIntroducedDate', 'exhibitFilename', 'introducedBy', 'sourceFilename'];
		}

		// xls template has column titles in first row, start filling spreadsheet on second row
		$rowI = 2;

		foreach ($rows as $row)
		{
			foreach ($colValueIndexes as $i => $valueIndex)
			{
				if (!isset( $row[$valueIndex] ))
				{
					continue;
				}
				$sheet->getCellByColumnAndRow( $i, $rowI )->setValue( $row[$valueIndex] );
				if ($valueIndex == 'exhibitFilename')
				{
					$sheet->getCellByColumnAndRow( $i, $rowI )->getHyperlink($row[$valueIndex])->setUrl($row[$valueIndex]);
				}
			}
			$rowI++;
		}
	}

	private function downloadExhibitLogFolder( $folderIDs, $downloadFolderName, $exhibitLogFilePath ) {
		if (!$folderIDs) {
			return;
		}

		$archiveName = Core\IO::dir( 'temp' ) . DIRECTORY_SEPARATOR . $this->case->name . '_' . time() . '.zip';
		$zip = new \ZipArchive();
		$zip->open( $archiveName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );

		$zip->addFile( $exhibitLogFilePath, 'Exhibit Log.xls' );

		foreach ($folderIDs as $folderID) {
			$folder = new DB\Folders( $folderID );

			if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
				$this->showMessage('Folder not found!', 'Export Log With Exhibits');
				return;
			}
			$this->checkUserAccess( NULL, $folder->ID );

			$files = (new DB\OrchestraFiles())->getFilesByUserID( $folder->ID, $this->user->ID );
			if (count( $files ) < 1) {
				$this->showMessage( 'Folder is empty!', 'Export Log With Exhibits' );
				return;
			}
			foreach ($files as $fileRow) {
				if ( !$fileRow['ID'] ) {
					continue;
				}
				$zip->addFile( $folder->getFullPath().DIRECTORY_SEPARATOR.$fileRow['name'], $fileRow['name'] );
			}

		}
		$zip->close();

		$this->ajax->downloadFile( $archiveName, $downloadFolderName.'.zip' );
	}

	public function showNoFilesSelectedNotice() {
		$popup = $this->get('noticePopup');
        $popup->tpl->message = '<div style="text-align:center;">No files have been selected.<br/>Please select one or more files.</div>';
		$popup->get( 'btnCancel' )->value = 'OK';
        $popup->show();
	}

	public function linkCaseFolder( $folderID ) {
		if( !$this->getOrchestraFolders()->checkPermissionToCaseFolder( $this->user->ID, $folderID ) ) {
			Debug::ErrorLog( "linkCaseFolder; permission deniend to folder: {$folderID}" );
			return;
		}
		$nodejs = new NodeJS( TRUE );
		$scf = new DB\SessionCaseFolders( $folderID, $this->deposition->ID );
		if( !$scf || $scf->folderID != $folderID || $scf->sessionID != $this->deposition->ID ) {
			$scf->folderID = $folderID;
			$scf->sessionID = $this->deposition->ID;
			$scf->insert();
			$this->ajax->script( '$("#cf_' . $folderID . '").addClass( "checked" );' );
			$nodejs->notifyDepositionUpload( $folderID, $this->user->ID, $this->deposition->ID );
		} else {
			$scf->delete();
			$this->ajax->script( '$("#cf_' . $folderID . '").removeClass( "checked" );' );
			$nodejs->notifyUsersRemovedFolder( $folderID, [$this->deposition->ID] );
		}
	}

	public function annotateFile( $form_values )
	{
		$dlg = $this->get( 'saveFilePopup' );
		$overwrite_dlg = $this->get('ConfirmFileOverwritePopup');

		$annotate = new Annotate();
		$annotate_result = $annotate->annotateFile( 'session', $form_values, $this->deposition->ID, $this->user->ID, $dlg );

		if( $annotate_result && $annotate_result['file_exists_id'] && $form_values['overwrite'] !== 'overwrite' ) {
			$overwrite_dlg->get( 'btnCancel' )->value = 'Cancel';
			$overwrite_dlg->show();
			return;
		} elseif( $annotate_result ) {
			$dlg->hide();
			$overwrite_dlg->hide();
			$this->notifyNodeJSUsers( $annotate_result['fileID'] );
			$this->refreshFolders();
			$this->ajax->script( 'selectFolder(' . $annotate_result['folderID'] . ')' );

			$notice_dlg = $this->get( 'noticeSavePopup' );
			$notice_dlg->get( 'btnCancel' )->value = 'Ok';

			if( (int)$annotate_result[ 'fileID' ] === (int)$form_values[ 'sourceFileID' ] ) {
				$notice_dlg->tpl->message = 'The document was successfully resaved! ';
			} else {
				$notice_dlg->tpl->message = 'The document was successfully saved!';
				$this->ajax->script( 'updateFileDetails("' . $annotate_result['fileName'] . '",' . $annotate_result['fileID'] . ');' );
			}
			$notice_dlg->show();
		}
	}

	public function getSearchOptions() {
		$storedOptions = $this->session['ContentSearch'];
		$options['sessionOptions'] = $storedOptions['sessionOptions'];
		$options['searchContent'] = $storedOptions['searchContent'];
		$options['includeFilename'] = $storedOptions['includeFilename'];
		return $options;
	}

	public function storeSearchOptions( array $searchOptions ) {
		$options['caseOptions'] = $this->session['ContentSearch']['caseOptions'];
		$options['sessionOptions'] = ['folderID' => $searchOptions['folderID']];
		$options['searchContent'] = $searchOptions['searchContent'];
		$options['includeFilename'] = $searchOptions['includeFilename'];
		$this->session['ContentSearch'] = $options;
	}

	public function search( array $options ) {
		$filePanel = $this->get( 'file_panel' );
		if( !$filePanel->tpl->isContentSearch ) {
			$sortMenuOptions = [
				['name'=>'Name'],
				['name'=>'Date'],
				['name'=>'Rank']
			];

			$sortPopup = $this->get( 'sortMenuPopup' );
			$sortPopup->tpl->sortMenuItems = $sortMenuOptions;
			$sortPopup->update();
			$this->updateSortHeader('Rank', 'Desc', 'Files');
		}

		$options[ 'sessionID' ] = $this->deposition->ID;

		if( !$options[ 'folderID' ] ) {
			$options[ 'folderID' ] = [];
			$folders = (new DB\OrchestraFolders())->getDepositionFoldersForUser( $this->deposition->ID, $this->user->ID, TRUE );
			foreach( $folders as $folder ) {
				$options[ 'folderID' ][] = $folder[ 'ID' ];
			}
		}

		$result = Elasticsearch::search( "client:{$this->case->clientID}", $options[ 'searchTerms' ], $options );
		$searchTermsArray = [];
		$fileList = [];

		if( $result['total'] > 0 ) {
			$searchTermsArray = isset($result['searchedTerms']) && is_array($result['searchedTerms']) ? $result['searchedTerms'] : [];
			foreach( $result['hits'] as $file ) {
				$fileList[] = [
					'ID' => $file[ 'fileID' ],
					'name' => $file[ 'fileName' ],
					'rank' => number_format( ($file[ '_score' ] * 100), 0 ),
					'size' => $file[ 'fileSize' ],
					'folder' => $file[ 'folderName' ]
				];
			}
		}

		$contentSearchTerms = json_encode($searchTermsArray);
		$filePanel->tpl->fileList = $fileList;
		$filePanel->tpl->isContentSearch = true;
		$filePanel->update();
		$this->ajax->script( 'fillFileList(true);' );
		$this->ajax->script( 'contentSearchTerms = ' . (($contentSearchTerms != '') ? $contentSearchTerms : 'null') );
		$this->ajax->script( 'showSearchResults();' );
	}

	public function showSearchCancel()
	{
		if( $this->config->elasticsearch[ 'search_disabled' ] ) {
			$notice_dlg = $this->get( 'searchCancelPopup' );
			$notice_dlg->get( 'btnSearchCancel' )->value = 'Ok';
			$notice_dlg->tpl->title = 'Currently Unavailable';
			$notice_dlg->tpl->showSpinner = false;
			$notice_dlg->tpl->message = 'Content search is not currently available while the indices are being rebuilt';
			$notice_dlg->show();
			return;
		}

		$notice_dlg = $this->get( 'searchCancelPopup' );
		$notice_dlg->get( 'btnSearchCancel' )->value = 'Cancel';
		$notice_dlg->tpl->title = 'Searching Documents';
		$notice_dlg->tpl->message = 'Press cancel to end the search before it completes.';
		$notice_dlg->tpl->showSpinner = true;
		$notice_dlg->show();
		$this->ajax->script( 'doSearch();' );
	}
}
