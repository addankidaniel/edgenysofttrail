<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
ClickBlocks\DB,
ClickBlocks\Web,
ClickBlocks\Web\UI\POM,
ClickBlocks\Web\UI\Helpers,
ClickBlocks\Utils;

class PageDemoTrialEdit extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	protected $deposition = NULL;
	protected $case = null;
	protected $exhibit = 'Exhibit';
	protected $canDelete = false;
	protected $isAdmin = false;
	protected $isCaseManager = false;
	protected $isCreator = false;
	protected $isDepoOwner = false;
	protected $isDemoAdmin = false;
	protected $isNew = false;

	public function __construct()
	{
		parent::__construct("depositions/demotrial.html");
		$dpid = isset($this->fv['depoID']) ? (int) $this->fv['depoID'] : 0;
		$cid = isset($this->fv['caseID']) ? (int) $this->fv['caseID'] : 0;
		$this->deposition = foo(new DB\ServiceDepositions)->getByID($dpid);
		$this->case = foo(new DB\ServiceCases)->getByID($cid);
		$this->client = foo(new DB\ServiceClients)->getByID( $this->case->clientID );
		$this->isNew = (!$this->case->ID || !$this->deposition->ID);
	}

	public function access() {
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/cases';
		if( $this->deposition->ID && $this->deposition->isWitnessPrep() ) {
			//redirect
			$this->noAccessURL = "{$this->basePath}/case/witnessprep/edit?caseID={$this->fv['caseID']}&depoID={$this->fv['depoID']}";
			return FALSE;
		}
		if( $this->deposition->ID && $this->deposition->class !== self::DEPOSITION_CLASS_DEMOTRIAL ) {
			return FALSE;
		}
		$this->isAdmin = ($this->case->ID && (new DB\OrchestraCases())->checkCaseAdmin($this->case->ID, $this->user->ID));
		$this->isCaseManager = ($this->case->ID && (new DB\OrchestraCaseManagers())->checkCaseManager( $this->case->ID, $this->user->ID ));
		$this->isDepoOwner = ($this->deposition->ID && $this->deposition->ownerID == $this->user->ID);
		$this->isDemoAdmin = ($this->case->isDemoCase() && $this->user->clientID == $this->case->clientID);
		if ( $this->isAdmin || $this->isCaseManager || $this->isDemoAdmin ) {
			$this->canDelete = TRUE;
		} else {
			$this->canDelete = FALSE;
		}
		return ($this->isDepoOwner || $this->canDelete);
	}

	public function init()
	{
		parent::init();

		$this->tpl->cname = $this->case->name;
		$this->tpl->canDelete = $this->canDelete;
		$this->tpl->isAdmin = $this->isAdmin;
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->depoID = $this->deposition->ID;
		$this->tpl->depoStatusID = $this->deposition->statusID;
		$this->tpl->isDemoCase = ($this->case->class == self::CASE_CLASS_DEMO);

		$this->tpl->showOwnerPanel = $this->isNew || ($this->deposition->createdBy == $this->user->ID) || $this->isAdmin || $this->isCaseManager || $this->isDemoAdmin;
		if ($this->tpl->showOwnerPanel)
		{
			if ($this->isNew) {
				$own = $this->user;
			} else {
				$own = $this->deposition->owner[0];
				if ($this->deposition->statusID != self::DEPOSITION_STATUS_NEW)
					$this->get('ownerSearch')->disabled = true;
			}
			$this->get('ownerID')->value = $own->ID;
			$this->get('ownerName')->text = $own->firstName.' '.$own->lastName;
		}

		if( $this->isNew ) {
			$localTime = time() + ($this->session['timeZoneOffset'] * 60);
			$this->get('date')->value = date('m/d/Y', $localTime);
			if( $this->case->ID == 0 ) {
				header("Location: " . $this->basePath . "/cases");
				die();
			}
			$this->head->name = $this->tpl->hname = 'Add: Demo Trial';
			$this->tpl->isNew = TRUE;

			$this->get('depositionOf')->value = $this->case->name;
			$this->get('sPasscode')->value = 'Demo321';

			$this->tpl->cancelURL = $this->basePath . '/case/view?ID=' . $this->case->ID;
		}
		else
		{
			$openDate = date_create_from_format('Y-m-d H:i:s', $this->deposition->openDateTime);
			$this->get('date')->value = $openDate->format('m/d/Y');

			$this->head->name = $this->tpl->hname = 'Edit: Demo Trial';
			$this->tpl->isNew = FALSE;

			$this->get( 'mainform' )->assign( $this->deposition->getValues() );

			$da = new \ClickBlocks\DB\DepositionsAuth( $this->deposition->ID );
			if( $da->ID ) {
				$this->get('sPasscode')->value = $da->getPasscode();
			}

			$this->get('valpass')->groups = 'ignore';

			$dpAssistants = foo(new DB\OrchestraDepositionAssistants())->getByDepo($this->deposition->ID);
			if (count($dpAssistants) > 0)
			{
				$ids = array();
				foreach ($dpAssistants as $dpAssistant)
				{
					$ids[] = $dpAssistant['userID'];
				}
			}
			$this->get('users')->ids = $ids;

			if ( isset($this->fv['view']) ) {
				$this->tpl->cancelURL = $this->basePath . '/session/view?caseID=' . $this->case->ID . '&depoID=' . $this->deposition->ID;
			} else {
				$this->tpl->cancelURL = $this->basePath . '/case/view?ID=' . $this->case->ID;
			}
		}
	}

	public function save( array $params )
	{
		$this->ajax->script( 'btnLock = false' );
		if( !$this->validators->isValid( 'depo' ) ) {
			return;
		}

		$params['courtReporterEmail'] = NULL;
		$params['openDateTime'] = date( 'Y-m-d H:i:s', strtotime( "{$params['date']} 08:00:00" ) );
		$params['caseID'] = $this->case->ID;
		if( !$params['ownerID'] ) {
			$params['ownerID'] = $this->user->ID;
		}

		$needsDemoFiles = FALSE;
		$now = date( 'Y-m-d H:i:s' );
		if( !$this->deposition->ID ) {
			$params['created'] = $now;
			$params['uKey'] = time() . mt_rand( 1000000, 9999999 );
			$params['createdBy'] = $this->user->ID;
			$params['class'] = self::DEPOSITION_CLASS_DEMOTRIAL;
			$needsDemoFiles = TRUE;
		}
		$params['depositionOf'] = str_replace( ' (DEMO)', '', $params['depositionOf']) . ' (DEMO)';

		$this->deposition->setValues( $params );
		$this->deposition->save();

		if( isset( $params['sPasscode'] ) && $params['sPasscode'] ) {
			$da = new \ClickBlocks\DB\DepositionsAuth( $this->deposition->ID );
			if( $da->ID ) {
				$da->spice = DB\OrchestraDepositionsAuth::generateSpice();
				$da->code = $params['sPasscode'];
				$da->update();
			} else {
				$da->ID = $this->deposition->ID;
				$da->spice = DB\OrchestraDepositionsAuth::generateSpice();
				$da->code = $params['sPasscode'];
				$da->insert();
			}
		}
		// check if uKey exists
		if( $this->deposition->uKey != $this->deposition->ID ) {
			// Update uKey by deposition ID
			$this->deposition->uKey = $this->deposition->ID;
//			(new DB\ServiceDepositions())->save($this->deposition);
			$this->deposition->update();
		}

		$userIDs = (array)$this->get( 'users' )->ids;
		$userIDs = array_combine( $userIDs, $userIDs );
		$svCM = new \ClickBlocks\DB\ServiceCaseManagers();
		foreach( $this->deposition->depositionAssistants as $rec ) {
			if( isset( $userIDs[$rec->userID] ) ) {
				unset( $userIDs[$rec->userID] );
			} else {
				if( $rec->userID == $this->deposition->ownerID && !$svCM->getByID( ['caseID'=>$this->deposition->caseID, 'userID'=>$rec->userID] )->userID ) {
					$this->deposition->resetOwner();
				}
				$rec->delete();
			}
		}
		//foo(new DB\OrchestraDepositionAssistants())->deleteByDeposition($this->deposition->ID);
		foreach( $userIDs as $id ) {
			if( $id == 0 ) {
				continue;
			}
			$dpAssistant = new \ClickBlocks\DB\DepositionAssistants();
			$dpAssistant->depositionID = $this->deposition->ID;
			$dpAssistant->userID = $id;
			$dpAssistant->insert();
		}

		$node = new \ClickBlocks\Utils\NodeJS( TRUE );
		$node->notifyCasesUpdated( $this->deposition->caseID, $this->deposition->ID );

		if ( $this->isNew ) {
			$this->ajax->redirect( $this->basePath . '/session/view?caseID=' . $this->deposition->caseID . '&depoID=' . $this->deposition->ID );
		} else {
			$this->ajax->redirect($this->tpl->cancelURL);
		}
	}

	public function searchUsers($name, $uniqueID)
	{
		$panel = $this->get('userAutofillTemplate');
		$ids = $this->get('users')->ids;
		$isOwner = (stripos($uniqueID, 'owner') === 0);
		if ($isOwner) {
			// $rows = foo(new DB\OrchestraUsers())->searchTrustedUsers($this->case->ID, $this->deposition->ID, $name);
			$rows = foo(new DB\OrchestraUsers())->getUsersForDeposition($name, false, $this->user->clientID, array($this->get('ownerID')->value));
			$panel->tpl->callback = '->setOwner';
		} else {
			$ids[] = $this->user->ID;
			$rows = foo(new DB\OrchestraUsers())->getUsersForDeposition($name, false, $this->user->clientID, $ids);
			$panel->tpl->callback = '->addUser';
		}
		if (!$rows) return false;
		$panel->tpl->rows = $rows;
		$panel->tpl->uniqueID = $uniqueID;
		return trim($panel->getInnerHTML());
	}

	public function addUser($userID) {
		$ids = (array)$this->get('users')->ids;
		$user = foo(new DB\ServiceUsers())->getByID($userID);
		if ($user->ID > 0)
		{
			if (in_array($user->ID,$ids)) return;
			array_push($ids, $user->ID);
			$this->get('users')->ids = $ids;
			$this->get('users')->update();
		}
		$this->get('userSearch')->value = '';
		$this->ajax->script( 'adjustBorder();' );
	}

	public function setOwner($userID)
	{
		$this->get('ownerID')->value = $userID;
		$own = foo(new DB\ServiceUsers)->getByID($userID);
		$this->get('ownerName')->text = $own->firstName.' '.$own->lastName;
		$this->get('ownerSearch')->value = '';
	}

	public function showAssistants()
	{
		$popup = $this->get('noticePopup');
		$this->cleanByUniqueID($popup->uniqueID);
		$popup->tpl->title = 'As members of the team, assistants can:';
		$popup->tpl->message = '* Receive and view all documents and exhibits in sessions <br> * Save annotated documents to the repository <br> * Upload/Download files in a session';
		$popup->show();
	}

	public function confirmDeleteDepo($id) {
		$depo = $this->getService('Depositions')->getByID($id);
		if (0 == $depo->ID)
			return;
		$param = array();
		$param['title'] = 'Delete Session';
		$param['message'] = 'Are you sure you want to delete this session and all its contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteDepo({$depo->ID}, 1);";
		$this->showPopup('confirm', $param);
	}

	public function deleteDepo( $depositionID )
	{
		$depositionID = (int)$depositionID;
		$this->hidePopup( 'confirm' );
		$depo = $this->getService( 'Depositions' )->getByID( $depositionID );
		if( !$depo->ID || $depo->ID != $depositionID ) {
			return;
		}
		$depo->setAsDeleted();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $this->case->ID );
		$this->ajax->redirect( $this->basePath . '/case/view?ID=' . $this->case->ID );
	}

}
