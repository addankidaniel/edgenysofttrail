<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageWitnessPrepEdit extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	protected $deposition = NULL;
	/**
	 * @var \ClickBlocks\DB\Cases
	 */
	protected $case = NULL;
	/**
	 * @var \ClickBlocks\DB\Clients
	 */
	protected $client = NULL;
	protected $caseID = 0;
	protected $canDelete = FALSE;
	protected $isAdmin = FALSE;
	protected $isCaseManager = FALSE;
	protected $isDepoOwner = FALSE;
	protected $isNew = false;

	public function __construct()
	{
		parent::__construct("depositions/witnessprep.html");
		$depositionID = isset( $this->fv['depoID'] ) ? (int)$this->fv['depoID'] : 0;
		$this->caseID = (int)$this->fv['caseID'];
		$this->deposition = new DB\Depositions( $depositionID );
		$this->case = new DB\Cases( $this->caseID );
		$this->client = new DB\Clients( $this->user->clientID );
		$this->isNew = (!$this->case->ID || !$this->deposition->ID);
	}

	public function access()
	{
		if( !parent::access() ) {
			return FALSE;
		}
		$this->noAccessURL = $this->basePath . '/case/view?ID=' . $this->caseID;
		if( !$this->case || !$this->case->ID || $this->case->clientID != $this->client->ID || $this->case->class == self::CASE_CLASS_DEMO ) {
			return FALSE;
		}
		if( $this->deposition->ID && $this->deposition->class !== self::DEPOSITION_CLASS_WITNESSPREP ) {
			return FALSE;
		}
		$this->isAdmin = $this->getOrchestraCases()->checkCaseAdmin( $this->case->ID, $this->user->ID );
		$this->isCaseManager = $this->getOrchestraCaseManagers()->checkCaseManager( $this->case->ID, $this->user->ID );
		$this->isDepoOwner = ($this->deposition->ID && $this->deposition->ownerID == $this->user->ID);
		if ( $this->isAdmin || $this->isCaseManager ) {
			$this->canDelete = TRUE;
		} else {
			$this->canDelete = FALSE;
		}
		return ($this->isDepoOwner || $this->canDelete);
	}

	public function init()
	{
		parent::init();

		$this->tpl->cname = $this->case->name;
		$this->tpl->canDelete = $this->canDelete;
		$this->tpl->isAdmin = $this->isAdmin;
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->depoStatusID = $this->deposition->statusID;
		$this->tpl->isEnterprise = ($this->client->typeID == self::CLIENT_TYPE_ENT_CLIENT);
		$this->tpl->isDemoCase = ($this->case->class == self::CASE_CLASS_DEMO);
		$this->tpl->isNew = (!$this->deposition->ID);

		$this->tpl->showOwnerPanel = ($this->isNew || $this->deposition->createdBy == $this->user->ID || $this->isAdmin || $this->isCaseManager);
		if( $this->tpl->showOwnerPanel ) {
			if( !$this->isNew ) {
				$own = $this->deposition->owner[0];
				if( $this->deposition->statusID != self::DEPOSITION_STATUS_NEW ) {
					$this->get('ownerSearch')->disabled = TRUE;
				}
				$this->get('ownerID')->value = $own->ID;
				$this->get('ownerName')->text = "{$own->firstName} {$own->lastName}";
			} else {
				$this->tpl->hasOwner = FALSE;
			}
		}

		if( $this->isNew ) {
			$localTime = time() + ($this->session['timeZoneOffset'] * 60);
			$this->get('date')->value = date( 'm/d/Y', $localTime );
			if( !$this->case->ID ) {
				header("Location: " . $this->basePath . "/cases");
				exit();
			}
			$this->head->name = $this->tpl->hname = 'Add: Witness Prep';

			$this->get('sPasscode')->value = self::createPasscode();

			$this->tpl->cancelURL = $this->basePath . '/case/view?ID=' . $this->case->ID;
		} else {
			$openDate = date_create_from_format( 'Y-m-d H:i:s', $this->deposition->openDateTime );
			$this->get('date')->value = $openDate->format( 'm/d/Y' );

			$this->head->name = $this->tpl->hname = 'Edit: Witness Prep';

			$this->get( 'mainform' )->assign( $this->deposition->getValues() );

			$da = new \ClickBlocks\DB\DepositionsAuth( $this->deposition->ID );
			if( $da->ID ) {
				$this->get('sPasscode')->value = $da->getPasscode();
			}

			$this->tpl->depoID = $this->deposition->ID;
			$this->get('valpass')->groups = 'ignore';

			$dpAssistants = $this->getOrchestraDepositionAssistants()->getByDepo( $this->deposition->ID );
			if( $dpAssistants ) {
				$IDs = [];
				foreach( $dpAssistants as $dpAssistant ) {
					$IDs[] = $dpAssistant['userID'];
				}
			}
			$this->get('users')->ids = $IDs;

			if ( isset($this->fv['view']) ) {
				$this->tpl->cancelURL = $this->basePath . '/session/view?caseID=' . $this->case->ID . '&depoID=' . $this->deposition->ID;
			} else {
				$this->tpl->cancelURL = $this->basePath . '/case/view?ID=' . $this->case->ID;
			}
		}
	}

	public function save( Array $params )
	{
		$this->ajax->script( 'btnLock = false;' );
		if( !$this->validators->isValid( 'depo' ) ) {
			return;
		}
		$params['openDateTime'] = date( 'Y-m-d H:i:s', strtotime( "{$params['date']} 08:00:00" ) );
		$params['caseID'] = $this->case->ID;
		if( !$params['ownerID'] ) {
			$params['ownerID'] = $this->user->ID;
		}
		if( !$this->deposition->ID ) {
			$params['created'] = date( 'Y-m-d H:i:s' );
			$params['uKey'] = time() . mt_rand( 1000000, 9999999 );
			$params['createdBy'] = $this->user->ID;
			$params['class'] = self::DEPOSITION_CLASS_WITNESSPREP;
			$params['statusID'] = self::DEPOSITION_STATUS_NEW;
			$params['volume'] = '';
		}
		$this->deposition->setValues( $params );
		$this->deposition->save();

		if( isset( $params['sPasscode'] ) && $params['sPasscode'] ) {
			$da = new \ClickBlocks\DB\DepositionsAuth( $this->deposition->ID );
			if( $da->ID ) {
				$da->spice = DB\OrchestraDepositionsAuth::generateSpice();
				$da->code = $params['sPasscode'];
				$da->update();
			} else {
				$da->ID = $this->deposition->ID;
				$da->spice = DB\OrchestraDepositionsAuth::generateSpice();
				$da->code = $params['sPasscode'];
				$da->insert();
			}
		}
		// check if uKey exists
		if( $this->deposition->uKey != $this->deposition->ID ) {
			$this->deposition->uKey = $this->deposition->ID;
			$this->deposition->update();
		}

		$userIDs = (array)$this->get('users')->ids;

		foreach( $this->deposition->depositionAssistants as $rec ) {
			$idx = array_search( $rec->userID, $userIDs );
			if( $idx ) {
				unset( $userIDs[$idx] );
			} else {
				$caseMgr = new DB\CaseManagers( $this->case->ID, $rec->userID );
				if( $rec->userID == $this->deposition->ownerID && !$caseMgr->userID ) {
					$this->deposition->resetOwner();
				}
				$rec->delete();
			}
		}
		foreach( $userIDs as $id ) {
			if( !$id ) {
				continue;
			}
			$dpAssistant = new DB\DepositionAssistants;
			$dpAssistant->depositionID = $this->deposition->ID;
			$dpAssistant->userID = $id;
			$dpAssistant->insert();
		}

		$node = new \ClickBlocks\Utils\NodeJS( TRUE );
		$node->notifyCasesUpdated( $this->case->ID, $this->deposition->ID );

		if ( $this->isNew ) {
			$this->ajax->redirect( $this->basePath . '/session/view?caseID=' . $this->deposition->caseID . '&depoID=' . $this->deposition->ID );
		} else {
			$this->ajax->redirect($this->tpl->cancelURL);
		}
	}

	public function searchUsers( $name, $uniqueID )
	{
		$panel = $this->get('userAutofillTemplate');
		$ids = $this->get('users')->ids;
		$isOwner = (stripos($uniqueID, 'owner') === 0);
		if ($isOwner) {
			$excludeIDs = [];
			if( $this->get('ownerID')->value ) {
				$excludeIDs[] = (int)$this->get('ownerID')->value;
			}
			$rows = $this->getOrchestraUsers()->getUsersForDeposition( $name, FALSE, $this->user->clientID, $excludeIDs );
			$panel->tpl->callback = '->setOwner';
		} else {
			$ids[] = $this->user->ID;
			$rows = $this->getOrchestraUsers()->getUsersForDeposition( $name, FALSE, $this->user->clientID, $ids );
			$panel->tpl->callback = '->addUser';
		}
		if( !$rows ) {
			return FALSE;
		}
		$panel->tpl->rows = $rows;
		$panel->tpl->uniqueID = $uniqueID;
		return trim( $panel->getInnerHTML() );
	}

	public function addUser( $userID )
	{
		$ids = (array)$this->get('users')->ids;
		$user = foo(new DB\ServiceUsers())->getByID($userID);
		if( $user->ID > 0 ) {
			if( in_array( $user->ID, $ids ) ) {
				return;
			}
			array_push( $ids, $user->ID );
			$this->get('users')->ids = $ids;
			$this->get('users')->update();
		}
		$this->get('userSearch')->value = '';
		$this->ajax->script( 'adjustBorder();' );
	}

	public function setOwner( $userID )
	{
		$own = new \ClickBlocks\DB\Users( $userID );
		if( !$own || !$own->ID || $own->ID != $userID ) {
			return FALSE;
		}
		$this->get('ownerID')->value = $userID;
		$this->get('ownerName')->text = $own->firstName . ' ' . $own->lastName;
		$this->get('ownerSearch')->value = '';
		$this->tpl->hasOwner = TRUE;
		$this->get( 'depoOwner' )->update();
		$this->ajax->script( 'setInputsClassOfValidators();validators.validate("depo", "error", "");' );
	}

	public function showAssistants()
	{
		$popup = $this->get('noticePopup');
		$this->cleanByUniqueID( $popup->uniqueID );
		$popup->tpl->title = 'As members of the team, assistants can:';
		$popup->tpl->message = '* Receive and view all documents and exhibits in sessions <br> * Save annotated documents to the repository <br> * Upload/Download files in a session';
		$popup->show();
	}

	public function confirmDeleteDepo( $id )
	{
		$depo = $this->getService('Depositions')->getByID($id);
		if( !$depo->ID ) {
			return;
		}
		$param = [];
		$param['title'] = 'Delete Session';
		$param['message'] = 'Are you sure you want to delete this session and all its contents?';
		$param['OKName'] = 'Delete';
		$param['OKMethod'] = "deleteDepo({$depo->ID}, 1);";
		$this->showPopup( 'confirm', $param );
	}

	public function deleteDepo( $depositionID )
	{
		$depositionID = (int)$depositionID;
		$this->hidePopup( 'confirm' );
		$depo = $this->getService( 'Depositions' )->getByID( $depositionID );
		if( !$depo->ID || $depo->ID != $depositionID ) {
			return;
		}
		$depo->setAsDeleted();
		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		$nodeJS->notifyCasesUpdated( $this->case->ID );
		$this->ajax->redirect( $this->basePath . '/case/view?ID=' . $this->case->ID );
	}
}
