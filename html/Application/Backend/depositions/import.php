<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils,
	ClickBlocks\Utils\NodeJS,
	\ClickBlocks\API\IEdepoBase,
	ClickBlocks\Debug;

class PageDepositionImport extends Backend
{
	/**
	 * @var \ClickBlocks\DB\Depositions
	 */
	protected $deposition = NULL;
	/**
	 *
	 * @var \ClickBlocks\DB\Cases
	 */
	protected $case = NULL;
	protected $isAdmin = FALSE;
	protected $isCreator = FALSE;
	protected $isManager = FALSE;
	protected $isDemoAdmin = FALSE;
	protected $isTrustedUser = FALSE;
	protected $datasources = [];
	protected $datasource = NULL;
	protected $tabPrefix = '';
	/**
	 * @var \EDRelativityClient
	 */
	protected $restClient = NULL;
	protected $formValues = NULL;
	protected $importData = NULL;
	protected $importToCase = FALSE;

	/**
	 *
	 */
	public function __construct() {
		parent::__construct( 'depositions/import.html' );
		$this->maxUploadBytes = (($this->deposition && $this->deposition->class == IEdepoBase::DEPOSITION_CLASS_TRIALBINDER) ? $this->config->trialbinder['maxFilesize'] : $this->config->maxFilesize);
		$sessionID = filter_input( INPUT_GET, 'sessionID', FILTER_SANITIZE_NUMBER_INT );
		$caseID = filter_input( INPUT_GET, 'caseID', FILTER_SANITIZE_NUMBER_INT );
		if( $sessionID ) {
			if( $this->getOrchestraDepositions()->checkUserAccessToDeposition( $this->user->ID, $sessionID ) ) {
				$this->importToCase = FALSE;
				$this->deposition = new DB\Depositions( $sessionID );
				$this->case = $this->deposition->cases[0];
			}
		} elseif( $caseID ) {
			if( $this->getOrchestraCases()->checkCaseAdmin( $caseID, $this->user->ID ) ) {
				$this->importToCase = TRUE;
				$this->deposition = NULL;
				$this->case = new DB\Cases( $caseID );
			}
		}
		$this->isClientAdmin = ($this->user->clientID == $this->case->clientID && ($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->clientAdmin));
		$this->isCaseManager = $this->getOrchestraCaseManagers()->checkCaseManager( $this->case->ID, $this->user->ID );
		$this->isDemoAdmin = ($this->user->clientID == $this->case->clientID && $this->case->isDemo());
		$this->isTrustedUser = ($this->isCaseManager || $this->isClientAdmin || $this->isDemoAdmin);
	}

	/**
	 *
	 * @return boolean
	 */
	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath . '/cases';
		$accessToSession = (isset( $this->deposition ) && $this->deposition->ID > 0);
		$accessToCase = (isset( $this->case ) && !$this->deposition && $this->case->ID > 0);
		return (($this->user->typeID == self::USER_TYPE_CLIENT || $this->user->typeID == self::USER_TYPE_USER) && ($accessToSession || $accessToCase));
	}

	public function init() {
		parent::init();
		$this->tpl->basePath = $this->basePath;
		$this->tpl->caseID = $this->case->ID;
		$this->tpl->caseName = $this->case->name;
		$this->tpl->importToCase = $this->importToCase;
		$this->tpl->sessionID = $this->importToCase ? 0 : $this->deposition->ID;
		$this->tpl->sessionName = $this->importToCase ? '' : $this->deposition->depositionOf;
		$this->head->name = $this->tpl->hname = 'Import to ' . ($this->importToCase ? 'Case' : 'Session');
		$this->tpl->contentHeader = "Import To: {$this->case->name} " . ($this->importToCase ? '' : "/ {$this->deposition->depositionOf}");
		$this->tpl->documents = [];
		$this->tpl->selectedDocs = 0;
		$this->tpl->totalDocs = 0;
		$this->tpl->importedDocs = 0;
		$this->tpl->totalImported = 0;

		$datasources = $this->getOrchestraDatasources()->getDatasources( $this->user->clientID );
		$datasourceOptions = [];
		$datasourceUser = NULL;

		if( $datasources && is_array( $datasources ) ) {
			$this->datasource = reset( $datasources );
			foreach( $datasources as $datasource ) {
				$datasourceOptions[$datasource['ID']] = "[{$datasource['vendor']}] {$datasource['name']}";
			}
			$this->get( 'datasources' )->options = $datasourceOptions;
			$this->get( 'datasources' )->value = key( $datasourceOptions );

			$datasourceUser = $this->getOrchestraDatasourceUsers()->getDatasourceUsers( $this->user->ID, $this->datasource['ID'] );
		}

		$this->subSession['importData'] = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();

		switch( strtolower( $this->datasource['vendor'] ) ) {
			case 'relativity':
			default:
				$this->tabPrefix = 'relativity_';
				if( $datasourceUser && is_array( $datasourceUser ) ) {
					$this->tpl->rememberMe = 'true';
					$loginPanel = $this->get( 'mainform.relativityTabs.relativity_tab1.relativityLoginPanel' );
					$loginPanel->get( 'relativity_remember_me' )->value = 'true';
					$loginPanel->get( 'relativity_username' )->value = \ClickBlocks\Utils::decrypt( $datasourceUser['username'], $this->config->general['secretSalt'] );
					$loginPanel->get( 'relativity_password' )->value = \ClickBlocks\Utils::decrypt( $datasourceUser['password'], $this->config->general['secretSalt'] );
				}
				break;
		}
	}

	private function readImportData()
	{
		if( is_readable( $this->subSession['importData'] ) ) {
			$raw = file_get_contents( $this->subSession['importData'] );
			$this->importData = unserialize( $raw );
		} else {
			$this->importData = [];
		}
	}

	private function saveImportData()
	{
		file_put_contents( $this->subSession['importData'], serialize( $this->importData ) );
	}

	/**
	 * openTab
	 * @param integer $tab
	 */
	public function openTab( $tab )
	{
		$this->tabPrefix = 'relativity_';
		$this->ajax->script( "$('.detail_part').css('display', 'none');" );
		$this->ajax->script( "$('.import_option li a').removeClass( 'active' );" );
		$this->ajax->script( "controls.display('" . $this->get( $this->tabPrefix . 'tab' . $tab )->uniqueID . "', '');" );
		$this->ajax->script( "controls.addClass( '{$this->tabPrefix}tab{$tab}', 'active' );" );
        $this->get( $this->tabPrefix . 'tab' . $tab )->visible = TRUE;
	}

	/**
	 * selectDatasource
	 * @param array $fv
	 */
	public function selectDatasource( $fv )
	{
		if( $this->subSession['importData'] && file_exists( $this->subSession['importData'] ) ) unlink( $this->subSession['importData'] );
		$this->subSession['importData'] = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
		$datasource = $this->getOrchestraDatasources()->getDatasourceByID( $this->user->clientID, $fv['datasources'] );
		$datasourceUser = $this->getOrchestraDatasourceUsers()->getDatasourceUsers( $this->user->ID, $datasource['ID'] );
		$rmRemoveClass = ($datasourceUser) ? 'r_off' : 'r_on';
		$rmAddClass = ($datasourceUser) ? 'r_on' : 'r_off';
		$loginPanel = $this->get( 'mainform.relativityTabs.relativity_tab1.relativityLoginPanel' );
		$loginPanel->tpl->errorMessage = '';
		$loginPanel->tpl->rememberMe = ($datasourceUser) ? 'true' : 'false';
		$loginPanel->get( 'relativity_remember_me' )->value = ($datasourceUser) ? 'true' : 'false';
		$loginPanel->get( 'relativity_username' )->value = ($datasourceUser) ? \ClickBlocks\Utils::decrypt( $datasourceUser['username'], $this->config->general['secretSalt'] ) : '';
		$loginPanel->get( 'relativity_password' )->value = ($datasourceUser) ? \ClickBlocks\Utils::decrypt( $datasourceUser['password'], $this->config->general['secretSalt'] ) : '';
		$loginPanel->update();
		$this->ajax->script( "$('#chkRememberMe').removeClass( '" . $rmRemoveClass . "' ).addClass( '" . $rmAddClass . "' );" );
		$workspacePanel = $this->get( 'mainform.relativityTabs.relativity_tab2.relativityWorkspacePanel' );
		$workspacePanel->get( 'relativity_workspace' )->options = ['-1'=>'No Workspaces'];
		$workspacePanel->get( 'relativity_workspace' )->value = -1;
		$workspacePanel->update();
		$binderPanel = $this->get( 'mainform.relativityTabs.relativity_tab3.relativityBinderPanel' );
		$binderPanel->get( 'relativity_binder' )->options = ['-1'=>'No Binders'];
		$binderPanel->get( 'relativity_binder' )->value = -1;
		$binderPanel->update();
		$docsPanel = $this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel' );
		$docsPanel->tpl->documents = [];
		$this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel.relativity_folder' )->value = '';
		$docsPanel->update();
		$this->ajax->script( "$( '#relativity_tab2' ).addClass( 'disabled' ); $( '#relativity_tab3' ).addClass( 'disabled' ); $( '#relativity_tab4' ).addClass( 'disabled' ); " );
		$this->openTab( 1 );
	}

	/**
	 * login
	 * @param array $fv
	 */
	public function login( $fv )
	{
		$this->formValues = $fv;
		$this->datasource = $this->getOrchestraDatasources()->getDatasourceByID( $this->user->clientID, $fv['datasources'] );
		switch( strtolower( $this->datasource['vendor'] ) ) {
			case 'relativity':
			default:
				if( $fv['relativity_remember_me'] == 'true' ) {
					$this->saveCredentials();
				} else {
					$dsU = $this->getOrchestraDatasourceUsers()->getDatasourceUsers( $this->user->ID, $this->datasource['ID'] );
					if( $dsU['ID'] ) {
						$datasourceUser = foo(new \ClickBlocks\DB\ServiceDatasourceUsers())->getByID( $dsU['ID'] );
						$datasourceUser->delete();
					}
				}
				$this->relativityGetWorkspaces();
				return;
		}
	}

	/**
	 * Save username/password to DatasourceUsers
	 */
	public function saveCredentials()
	{
		$dsU = $this->getOrchestraDatasourceUsers()->getDatasourceUsers( $this->user->ID, $this->datasource['ID'] );
		$encUsername = \ClickBlocks\Utils::encrypt( $this->formValues['relativity_username'], $this->config->general['secretSalt'] );
		$encPassword = \ClickBlocks\Utils::encrypt( $this->formValues['relativity_password'], $this->config->general['secretSalt'] );
		if( $dsU['ID'] ) {
			$datasourceUser = foo(new \ClickBlocks\DB\ServiceDatasourceUsers())->getByID( $dsU['ID'] );
			$datasourceUser->username = $encUsername;
			$datasourceUser->password = $encPassword;
			$datasourceUser->update();
		} else {
			$datasourceUser = new \ClickBlocks\DB\DatasourceUsers();
			$datasourceUser->userID = $this->user->ID;
			$datasourceUser->datasourceID = $this->datasource['ID'];
			$datasourceUser->username = $encUsername;
			$datasourceUser->password = $encPassword;
			$datasourceUser->insert();
		}
	}

	/**
	 * relativityGetWorkspaces
	 * @param array $fv
	 */
	protected function relativityGetWorkspaces()
	{
		$this->subSession['ds_username'] = $this->formValues['relativity_username'];
		$this->subSession['ds_password'] = $this->formValues['relativity_password'];
		$this->readImportData();

		$location = parse_url( $this->datasource['URI'] );
		$location['path'] = '/Relativity.REST/Relativity/Workspace';
		$location['query'] = $location['fragment'] = NULL;
		$requestURL = \ClickBlocks\Utils::buildURL( $location );

		$this->restClient = new \EDRelativityClient( $requestURL, "{$this->subSession['ds_username']}:{$this->subSession['ds_password']}" );
		$this->restClient->verbose = FALSE;

		$loginPanel = $this->get( 'mainform.relativityTabs.relativity_tab1.relativityLoginPanel' );
		$loginPanel->tpl->errorMessage = '';

		$restResponse = $this->restClient->exec();

		if( !$restResponse || $this->restClient->errno ) {
			if( $this->restClient->error ) {
				$loginPanel->tpl->errorMessage = $this->restClient->error;
			} else {
				$loginPanel->tpl->errorMessage = 'Error: Unable to process the request';
			}
			$loginPanel->update();
			$this->ajax->script( "resetTimeout();" );
			return;
		}

		$this->subSession['ds_location'] = $this->datasource['URI'];

		$workspaces = [];
		$optionWorkspace = [];

		$workspacePanel = $this->get( 'mainform.relativityTabs.relativity_tab2.relativityWorkspacePanel' );

		$response = json_decode( $restResponse );
		if( !$response || !isset( $response->Results ) ) {
			$loginPanel->tpl->errorMessage = 'Error: Invalid server response, unable to proceed';
		} else {
			foreach( $response->Results as $workspace ) {
				if( stripos( $workspace->Keywords, '#BindersEnabled#' ) !== FALSE ) {
					$optionWorkspace[$workspace->{'Artifact ID'}] = $workspace->{'Relativity Text Identifier'};
					$workspaces[$workspace->{'Artifact ID'}] = $workspace;
				}
			}
		}

		asort( $optionWorkspace );

		$this->importData['Workspaces'] = $workspaces;

		if( $workspaces ) {
			$workspacePanel->get( 'relativity_workspace' )->options = $optionWorkspace;
			$workspacePanel->get( 'relativity_workspace' )->value = key( $optionWorkspace );
		} else {
			$workspacePanel->get( 'relativity_workspace' )->options = [-1=>'No Workspaces'];
			$workspacePanel->get( 'relativity_workspace' )->value = -1;
		}

		$loginPanel->update( 1 );
		$this->ajax->script( "$( '.import_content_block li a.active' ).removeClass( 'active' ); $( '#relativity_tab2' ).removeClass( 'disabled' ); resetTimeout(); " );
		$this->openTab( 2, 'relativity_tab2' );

		$this->saveImportData();
	}

	/**
	 * selectWorkspace
	 * @param array $fv
	 */
	public function selectWorkspace( $fv )
	{
		$this->formValues = $fv;
		$workspaceID = $fv['relativity_workspace'];
		$this->readImportData();
		$workspace = $this->importData['Workspaces'][$workspaceID];
		$this->subSession['WorkspaceID'] = $workspaceID;

		$location = parse_url( $workspace->{'__Location'} );
		$location['path'] = "/Relativity.REST/Workspace/{$workspaceID}/Binder";
		$location['query'] = $location['fragment'] = NULL;
		$requestURL = \ClickBlocks\Utils::buildURL( $location );

		$this->restClient = new \EDRelativityClient( $requestURL, "{$this->subSession['ds_username']}:{$this->subSession['ds_password']}" );
		$this->restClient->fixLocation( $this->subSession['ds_location'] );
		$this->restClient->verbose = FALSE;

		$workspacePanel = $this->get( 'mainform.relativityTabs.relativity_tab2.relativityWorkspacePanel' );
		$workspacePanel->tpl->errorMessage = '';

		$restResponse = $this->restClient->exec();

		if( !$restResponse || $this->restClient->errno ) {
			if( $this->restClient->error ) {
				$workspacePanel->tpl->errorMessage = $this->restClient->error;
			} else {
				$workspacePanel->tpl->errorMessage = 'Error: Unable to process the request';
			}
			$workspacePanel->update( 1 );
			$this->ajax->script( "resetTimeout();" );
			return;
		}

		$binders = [];
		$optionBinders = [];

		$binderPanel = $this->get( 'mainform.relativityTabs.relativity_tab3.relativityBinderPanel' );

		$response = json_decode( $restResponse );
		if( !$response || !isset( $response->Results ) ) {
			$workspacePanel->tpl->errorMessage = 'Error: Invalid server response, unable to proceed';
		} else {
			foreach( $response->Results as $binder ) {
				$optionBinders[$binder->{'Artifact ID'}] = $binder->{'Relativity Text Identifier'};
				$binders[$binder->{'Artifact ID'}] = $binder;
			}
		}

		asort( $optionBinders );

		$this->importData['Binders'] = $binders;

		if( $binders ) {
			$binderPanel->get( 'relativity_binder' )->options = $optionBinders;
			$binderPanel->get( 'relativity_binder' )->value = key( $optionBinders );
		} else {
			$binderPanel->get( 'relativity_binder' )->options = [-1=>'No Binders'];
			$binderPanel->get( 'relativity_binder' )->value = -1;
		}

		$workspacePanel->update( 1 );
		$this->ajax->script( "$( '.import_content_block li a.active' ).removeClass( 'active' ); $( '#relativity_tab3' ).removeClass( 'disabled' ); resetTimeout(); " );
		$this->openTab( 3, 'relativity_tab3' );

		$this->saveImportData();
	}

	/**
	 * selectBinder
	 * @param array $fv
	 */
	public function selectBinder( $fv )
	{
		$this->formValues = $fv;
		$binderID = $fv['relativity_binder'];
		$this->readImportData();
		$binder = $this->importData['Binders'][$binderID];
		$workspaceID = $this->subSession['WorkspaceID'];
		$this->subSession['BinderID'] = $binderID;

		$location = parse_url( $binder->{'__Location'} );
		$location['path'] = "/Relativity.REST/Workspace/{$workspaceID}/Binder%20Document/QueryResult";
		$location['query'] = $location['fragment'] = NULL;
		$requestURL = \ClickBlocks\Utils::buildURL( $location );

		//$query = json_encode( array( 'condition' => " 'Binder' == {$binderID}", 'fields'=>array('*') ) );
		$query = json_encode( array( 'condition' => " 'Binder' == {$binderID}", 'fields'=>array('File','File File Size','Document') ) );

		$this->restClient = new \EDRelativityClient( $requestURL, "{$this->subSession['ds_username']}:{$this->subSession['ds_password']}", $query );
		$this->restClient->fixLocation( $this->subSession['ds_location'] );
		$this->restClient->verbose = FALSE;

		$binderPanel = $this->get( 'mainform.relativityTabs.relativity_tab3.relativityBinderPanel' );
		$binderPanel->tpl->errorMessage = '';

		$restResponse = $this->restClient->exec();

		if( !$restResponse || $this->restClient->errno ) {
			if( $this->restClient->error ) {
				$binderPanel->tpl->errorMessage = $this->restClient->error;
			} else {
				$binderPanel->tpl->errorMessage = 'Error: Unable to process the request';
			}
			$binderPanel->update();
			$this->ajax->script( "resetTimeout();" );
			return;
		}

		$documents = [];
		$controlNumbers = [];

		$response = json_decode( $restResponse );
		if( !$response || !isset( $response->Results ) ) {
			$binderPanel->tpl->errorMessage = 'Error: Invalid server response, unable to proceed';
		} else {
			foreach( $response->Results as $document ) {
				$artifactID = $document->Document->{'Artifact ID'};
				$document->Status = '';
				$document->FileSize = \ClickBlocks\Utils::readableFilesize( $document->{'File File Size'}, 1 );
				$document->allowDownload = ($document->{'File File Size'} <= $this->maxUploadBytes);
				$documents[$artifactID] = $document;
				$artifactIDs[] = $artifactID;
			}
		}

		//get related information
		if( $artifactIDs ) {
			$location = parse_url( $binder->{'__Location'} );
			$location['path'] = "/Relativity.REST/Workspace/{$workspaceID}/Document/QueryResult";
			$location['query'] = $location['fragment'] = NULL;
			$requestURL = \ClickBlocks\Utils::buildURL( $location );
			$artIDs = implode( ',', $artifactIDs );
			$query = json_encode( ['condition'=>" 'Artifact ID' IN [{$artIDs}]", 'fields'=>['Relativity Text Identifier']] );

			$this->restClient = new \EDRelativityClient( $requestURL, "{$this->subSession['ds_username']}:{$this->subSession['ds_password']}", $query );
			$this->restClient->fixLocation( $this->subSession['ds_location'] );
			$this->restClient->verbose = FALSE;
			$restResponse = $this->restClient->exec();

			if( !$restResponse || $this->restClient->errno ) {
				if( $this->restClient->error ) {
					$binderPanel->tpl->errorMessage = $this->restClient->error;
				} else {
					$binderPanel->tpl->errorMessage = 'Error: Unable to process the request';
				}
				$binderPanel->update();
				$this->ajax->script( "resetTimeout();" );
				return;
			}

			$response = json_decode( $restResponse );
			if( !$response || !isset( $response->Results ) ) {
				$binderPanel->tpl->errorMessage = 'Error: Invalid server response, unable to proceed';
				$binderPanel->update();
				$this->ajax->script( "resetTimeout();" );
				return;
			} else {
				foreach( $response->Results as $result ) {
					$docID = $result->{'Artifact ID'};
					$document = $documents[$docID];
					$document->Document->{'Relativity Text Identifier'} = $result->{'Relativity Text Identifier'};
				}
			}
		}

		$binderPanel->update();

		//ED-862; Sorting Document list
		$sortDocs = [];
		$sortedDocs = [];
		foreach( $documents as $docID => $document ) {
			$sortDocs[$docID] = $document->Document->{'Relativity Text Identifier'};
		}
		asort( $sortDocs );
		foreach( $sortDocs as $docID => $documentName ) {
			$sortedDocs[$docID] = $documents[$docID];
		}
		$documents = $sortedDocs;

		$this->importData['Documents'] = $documents;

		$docsPanel = $this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel' );
		$docsPanel->tpl->documents = $documents;
		$docsPanel->update();

		$this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel.relativity_folder' )->value = $binder->{'Relativity Text Identifier'};

		$statusPanel = $this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel.relativity_import_status' );
		$statusPanel->tpl->totalDocs = count( $documents );
		$statusPanel->tpl->selectedDocs = 0;
		$statusPanel->tpl->importedDocs = 0;
		$statusPanel->tpl->totalImported = 0;
		$statusPanel->update();

		$this->ajax->script( "$( '.import_content_block li a.active' ).removeClass( 'active' ); $( '#relativity_tab4' ).removeClass( 'disabled' ); resetTimeout(); selectedDocs = {}; allSelected = false; " );
		$this->openTab( 4, 'relativity_tab4' );

		$this->saveImportData();
	}

	/**
	 * importToFolder
	 * @param array $fv
	 */
	public function importToFolder( $fv ) {
		$this->formValues = $fv;
		if( !$fv['relativity_selected_docs'] ) {
			Debug::ErrorLog( 'importToFolder; no documents selected' );
			echo 'No documents selected';
		}
		$this->readImportData();

		$queueInfo = [];
		//$maxSelected = 500;

		$selectedIDs = explode( ',', $fv['relativity_selected_docs'] );
		if( !is_array( $selectedIDs ) || !$selectedIDs ) {
			Debug::ErrorLog( 'importToFolder; no selected documents' );
		} else {
			// ED-3683; removing max files import limit
			//if( count( $selectedIDs ) > $maxSelected ) {
			//	$selectedIDs = array_slice( $selectedIDs, 0, $maxSelected );
			//}

			//ED-862; Sort import queue
			$sortDocs = [];
			foreach( $selectedIDs as $docID ) {
				$doc = $this->importData['Documents'][$docID];
				if( !$doc ) {
					Debug::ErrorLog( "importToFolder; document not found for selected ID: {$docID}" );
					continue;
				}
				$sortDocs[$docID] = $doc->Document->{'Relativity Text Identifier'} . '.pdf';
			}
			asort( $sortDocs );
			$selectedIDs = array_keys( $sortDocs );

			foreach( $selectedIDs as $ID ) {
				$doc = $this->importData['Documents'][$ID];
				if( !$doc ) {
					Debug::ErrorLog( "importToFolder; document not found for selected ID: {$ID}" );
					continue;
				}
				$item = new \stdClass();
				$item->ID = $ID;
				$item->UUID = \ClickBlocks\Utils::createUUID();
				$item->ArtifactID = $doc->{'Artifact ID'};
				$item->fileName = $doc->Document->{'Relativity Text Identifier'} . '.pdf';
				$item->fileSize = $doc->{'File File Size'};
				$item->downloadAttempt = 0;
				$item->success = FALSE;
				$queueInfo[$ID] = $item;
			}
		}

		if( !$queueInfo ) {
			Debug::ErrorLog( 'importToFolder; no documents found for import' );
			$this->ajax->script( "$('.import_activity').hide(); if( activityTimeout ) { clearTimeout( activityTimeout ); activityTimeout = null; }" );
			return;
		}

		$this->importData['importQueue'] = new \stdClass();
		$this->importData['importQueue']->queueInfo = $queueInfo;
		$this->importData['importQueue']->queue = array_keys( $queueInfo );
		$this->importData['importQueue']->created = date( 'Y-m-d H:i:s' );
		$this->importData['importQueue']->importedCount = 0;
		$this->importData['importQueue']->workspaceID = $this->subSession['WorkspaceID'];
		$this->importData['importQueue']->binderID = $this->subSession['BinderID'];

		//destination folder
		$folderName = trim( $fv['relativity_folder'] );

		foreach( $this->config->logic as $idx => $reservedFolder ) {
			if( in_array( $idx, ['personalFolderName', 'miscellaneousFilesFolderName', 'attorneyNotesFolderName'] ) ) {
				continue;
			}
			if( strcasecmp( $folderName, $reservedFolder ) === 0 ) {
				//abort!
				Debug::ErrorLog( '--importToFolder; attempt to use reserved folder, "' . $reservedFolder . '". Aborting.' );
				echo 'Unable to import documents to protected folder: "' . $reservedFolder . '"';
				$this->ajax->script( "$('.import_activity').hide(); if( activityTimeout ) { clearTimeout( activityTimeout ); activityTimeout = null; }" );
				return;
			}
		}

		$orcFolders = $this->getOrchestraFolders();
		$folder = new DB\Folders();
		if( $this->importToCase ) {
			$fInfo = $orcFolders->getCaseFolderByName( $this->case->ID, $folderName );
			$folder->assign( $fInfo );
		} else {
			$folders = $orcFolders->getFoldersByName( $this->deposition->ID, $folderName );
			if( $folders && is_array( $folders ) ) {
				$folder->assign( $folders[0] );
			}
		}
		if( $folder->ID ) {
			Debug::ErrorLog( '--importToFolder; using folder: (' . $folder->ID . ') "' . $folder->name . '"' );
//			Debug::ErrorLog( print_r( $folder->getValues(), TRUE ) );
			$folder->lastModified = $this->importData['importQueue']->created;
		} else {
			Debug::ErrorLog( '--importToFolder; creating folder: "' . $folderName . '"' );
			$folder->name = $folderName;
			$folder->caseID = ($this->importToCase) ? $this->case->ID : NULL;
			$folder->depositionID = ($this->importToCase) ? NULL : $this->deposition->ID;
			$isTrustedUser = ($this->importToCase) ? $this->isTrustedUser : $this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $this->deposition->ID );
			$folder->created = $folder->lastModified = $this->importData['importQueue']->created;
			$folder->createdBy = ($isTrustedUser) ? $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID ) : $this->user->ID;
			$folder->class = ($isTrustedUser) ? self::FOLDERS_TRUSTED : self::FOLDERS_FOLDER;
//			Debug::ErrorLog( print_r( $folder->getValues(), TRUE ) );
			$folder->insert();
		}

		$this->importData['importQueue']->folderID = $folder->ID;

		$statusPanel = $this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel.relativity_import_status' );
		$statusPanel->tpl->totalDocs = count( $this->importData['Documents'] );
		$statusPanel->tpl->selectedDocs = count( $queueInfo );
		$statusPanel->tpl->importedDocs = 0;
		$statusPanel->tpl->totalImported = count( $queueInfo );
		$statusPanel->update();

		$script = '';
		$max = min( count( $this->importData['importQueue']->queue ), 1 );
		for( $i=0; $i<$max; ++$i ) {
			$itemID = array_shift( $this->importData['importQueue']->queue );
			$delay = $i * 500;	//helping the first request win the race as the first request (ED-862)
			if( $delay ) {
				$script .= "setTimeout(function(){ processQueueItem( {$itemID} );}, {$delay});";
			} else {
				$script .= "processQueueItem( {$itemID} );";
			}
		}
		if( $script ) $this->ajax->script( $script );

		$this->saveImportData();
	}

	/**
	 * processImportQueue
	 * @param string $ID
	 */
	public function processImportQueue( $ID ) {
		$this->readImportData();
		Debug::ErrorLog( 'processImportQueue; ' . print_r( $ID, TRUE ) );
		$item =& $this->importData['importQueue']->queueInfo[$ID];
		if( !$item ) {
			Debug::ErrorLog( "processImportQueue; item not found for ID: {$ID}" );
			$this->ajax->script( "$('#statusCell_' + {$ID}).html( 'Failed.' );" );
			$this->prepareNextQueueItem();
			return;
		}
		Debug::ErrorLog( 'queue item: ' . print_r( $item, TRUE ) );

		$binder = $this->importData['Binders'][$this->importData['importQueue']->binderID];
		if( !$binder ) {
			Debug::ErrorLog( "processImportQueue; binder not found for ID: {$this->importData['importQueue']->binderID}" );
			$this->ajax->script( "$('#statusCell_' + {$ID}).html( 'Failed.' );" );
			$this->prepareNextQueueItem();
			return;
		}

		$workspaceID = $this->importData['importQueue']->workspaceID;
		$location = parse_url( $binder->{'__Location'} );
		$location['path'] = "/Relativity.REST/Workspace/{$workspaceID}/Binder%20Document/{$item->ArtifactID}/Fields/File/File";
		$location['query'] = $location['fragment'] = NULL;
		$requestURL = \ClickBlocks\Utils::buildURL( $location );

		//download and create db objects
		++$item->downloadAttempt;
		Debug::ErrorLog( "Attempt {$item->downloadAttempt} to download ID: {$item->ArtifactID} as {$item->UUID}" );
		$tmpFile = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . $item->UUID;
		$fh = fopen( $tmpFile, 'w+' );
		if( flock( $fh, LOCK_EX ) ) {
			Debug::ErrorLog( 'Locked: Writing to: ' . $tmpFile );
			$this->restClient = new \EDRelativityClient( $requestURL, "{$this->subSession['ds_username']}:{$this->subSession['ds_password']}" );
			$this->restClient->fixLocation( $this->subSession['ds_location'] );
			$this->restClient->verbose = FALSE;
			$restResponse = $this->restClient->exec();
			if( $restResponse && !$this->restClient->errno ) {
				Debug::ErrorLog( "Success on attempt {$item->downloadAttempt} to download ID: {$item->ArtifactID} as {$item->UUID}" );
				$item->success = TRUE;
				++$this->importData['importQueue']->importedCount;
				fwrite( $fh, $restResponse );
				fflush( $fh );
			}
			flock( $fh, LOCK_UN );
		} else {
			//unable to get file lock
			Debug::ErrorLog( 'processImportQueue; unable to get file lock on ' . $tmpFile );
		}
		fclose( $fh );

		if( !$item->success ) {
			if( $item->downloadAttempt <= 3 ) {
				$this->importData['importQueue']->queue[] = $ID;
				$this->ajax->script( "$('#statusCell_{$ID}').html( 'Retrying...' );" );
			} else {
				$this->ajax->script( "$('#statusCell_{$ID}').html( '<span class=\"err\">Failed: Unable to download</span>' );" );
			}
		} else {
			$statusPanel = $this->get( 'mainform.relativityTabs.relativity_tab4.relativityDocumentsPanel.relativity_import_status' );
			$statusPanel->tpl->importedDocs = $this->importData['importQueue']->importedCount;
			$statusPanel->update();

			//ED-1438; Watermark docs for Demo depositions
			if( !$this->importToCase && $this->deposition->isDemo() ) {
				Debug::ErrorLog( "[Relativity Import] Watermarking DEMO Document" );
				$tmpFileWM = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . $item->UUID . '_wm.pdf';
				if( \ClickBlocks\PDFTools::watermark( $tmpFile, $tmpFileWM ) ) {
					copy( $tmpFileWM, $tmpFile );
					if( file_exists( $tmpFileWM ) ) {
						unlink( $tmpFileWM );
					}
				} else {
					Debug::ErrorLog( "Failed to watermark Artifact ID: {$item->ArtifactID}" );
					$this->ajax->script( "$('#statusCell_{$ID}').html( '<span class=\"err\">Failed: Unable to process document</span>' );" );
					if( file_exists( $tmpFile ) ) {
						unlink( $tmpFile );
					}
					if( file_exists( $tmpFileWM ) ) {
						unlink( $tmpFileWM );
					}
					$this->prepareNextQueueItem();
					$this->saveImportData();
					return;
				}
			}

			$isTrustedUser = $this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $this->deposition->ID );

			$file = new \ClickBlocks\DB\Files();
			$file->folderID = $this->importData['importQueue']->folderID;
			$file->created = $this->importData['importQueue']->created;
			$file->createdBy = ( $isTrustedUser ) ? $this->deposition->createdBy : $this->user->ID;
			$file->setSourceFile( $tmpFile, FALSE );
			$file->name = $item->fileName;
			$file->isUpload = 1;
			$file->insert();

//			Debug::ErrorLog( print_r( $file->getValues(), TRUE ) );

			\ClickBlocks\DB\ServiceInvoiceCharges::charge( $this->user->clientID, self::PRICING_OPTION_UPLOAD_PER_DOC, 1, $this->deposition->caseID, $this->deposition->ID );

			$this->ajax->script( "$('#statusCell_{$ID}').html( 'Saved: {$file->name}' );" );
		}

		$this->prepareNextQueueItem();
		$this->saveImportData();
	}

	/**
	 * prepareNextQueueItem
	 */
	private function prepareNextQueueItem()
	{
		//process remaining queue
		if( $this->importData['importQueue']->queue ) {
			$itemID = array_shift( $this->importData['importQueue']->queue );
			Debug::ErrorLog( 'prepareNextQueueItem: ' . print_r( $itemID, TRUE ) );
			$this->ajax->script( "processQueueItem( {$itemID} );" );
		} else {
			//getNodeJS()
			$node = new Utils\NodeJS( TRUE );
			if( $this->config->isDebug )
			{
				$logCallback = function( $text ) {
					$text = 'Calling NodeJS: ' . $text;
					@file_put_contents( Core\IO::dir( 'logs' ) . '/api.log', $text . PHP_EOL, FILE_APPEND );
				};
				$node->setLogCallback( $logCallback );
			}
			$node->notifyDepositionUpload( $this->importData['importQueue']->folderID, $this->user->ID );

			$this->ajax->script( "$('.import_activity').hide(); if( activityTimeout ) { clearTimeout( activityTimeout ); activityTimeout = null; }" );
		}
	}

}
