<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageExhibits extends Backend {
	public function __construct() {
		parent::__construct( 'exhibits/exhibits.html' );
	}

	public function access() {
		$depositionID = $this->getDepositionID();
		if( $depositionID ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function init() {
		parent::init();
		$this->head->name = 'Exhibits';
		$deposition = new DB\Depositions( $this->getDepositionID() );
		$this->get('exhibits')->depoID = $deposition->ID;
	}

	private function getDepositionID() {
		$path = $this->getDepositionURL();
		return $this->session['/' . $path]['depositionID'];
	}

	private function getDepositionURL() {
		$path = $this->uri->getPath();
		return explode('/', $path)[1];
	}

	function viewFile( $fileID ) {
		$fileID = (int)$fileID;
		$file = new \ClickBlocks\DB\Files( $fileID );
        if( !$file || !$file->ID || $file->ID != $fileID ) {
            return;
        }

		if( !$file->mimeType ) {
			$file->setMetadata();
		}
		$mimeType = $file->mimeType;
		if( mb_stristr( $mimeType, 'audio' ) ) {
			$dlg = $this->get( 'viewAudioPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} elseif( mb_stristr( $mimeType, 'video' ) ) {
			$dlg = $this->get( 'viewVideoPopup' );
			$dlg->tpl->mimeType = $mimeType;
			$dlg->tpl->src = "/{$this->uri->path[0]}/case/viewFile?ID={$this->fv['ID']}&viewFile={$fileID}";
		} else {
			$dlg = $this->get( 'viewFilePopup' );
//			$dlg->tpl->src = "/{$this->uri->path[0]}/case/fileViewer?ID={$this->fv['ID']}&viewFile={$fileID}";
			$dlg->tpl->src = "https://{$this->config->pdfviewer['host']}{$this->config->pdfviewer['path']}{$fileID}/viewer/";
		}
		$dlg->tpl->title = $file->name;
		$this->ajax->script( 'disableAnnotationControls()' );
		$dlg->show();
    }
	
	public function downloadExhibits() {
		$deposition = new DB\Depositions($this->getDepositionID());
		$_folder = (new DB\OrchestraFolders())->getExhibitFolder($deposition->ID);
		
		$folder = new \ClickBlocks\DB\Folders( $_folder['ID'] );
		if( !$folder || !$folder->ID ) {
			$this->showMessage('Folder not found!', 'Download Exhibits');
			return;
		}
		
		$files = (new DB\OrchestraFiles())->getFilesByUserID( $folder->ID, $this->user->ID );
		if (count( $files ) < 1) {
			$this->showMessage( 'Folder is empty!', 'Download Exhibits' );
			return;
		}
        $fullName = Core\IO::dir('temp').'/'.$deposition->depositionOf.'_'.time().'.zip';
        $zip = new \ZipArchive();
        $zip->open($fullName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        foreach ($files as $file) {
            if (!$file['ID']) continue;
			$zip->addFile( $folder->getFullPath().DIRECTORY_SEPARATOR.$file['name'], $file['name'] );
        }
        $zip->close();
        $this->ajax->downloadFile($fullName, $deposition->depositionOf.'.zip');
	}
}
