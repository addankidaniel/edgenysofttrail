<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Utils;

class PageNotifications extends Backend
{
    public function __construct()
    {
       parent::__construct( 'notifications/notifications.html' );
    }

	public function access()
	{
		if (!parent::access()) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return ($this->user->typeID == self::USER_TYPE_SUPERADMIN || $this->user->typeID == self::USER_TYPE_ADMIN);
	}

    public function init()
    {
		parent::init();

        $this->tpl->tab = 1;
        $this->head->name = 'Notify Users';
		$this->tpl->headName = 'Send Mass Notification';
		$this->tpl->isAdmin = true;
    }


}

?>