<?php

namespace ClickBlocks\MVC\Backend;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\Web\UI\Helpers,
	ClickBlocks\Utils;

class PageAdmins extends Backend {
	public function __construct() {
		parent::__construct('admins/admins.html');
	}

	public function access() {
		if( !parent::access() ) {
			return false;
		}
		$this->noAccessURL = $this->basePath.'/';
		return ( $this->user->typeID == self::USER_TYPE_SUPERADMIN );
	}

	public function init() {
		parent::init();
		$this->tpl->tab = 3;
		$this->head->name = 'Admin Management';
		$this->setBackendType( self::BACKEND_TYPE_ADMIN );
		$this->get( 'admins' )->clientID = $this->user->clientID;
	}

	public function search(){
		$txt = $this->get( 'searchText' );
		$this->get( 'admins' )->searchValue = trim( $txt->value );
		$this->get( 'admins' )->update();
	}
}
