<?php 

error_reporting(E_ALL^E_NOTICE);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <style>
  body {margin: 0 0;}
  .container {
    width:960px;
    margin: 0 auto;
    position:relative;
  }
  .header {
    padding: 0px 0 20px;
    height:47px;
  }
  h1 {
    width:193px;
    height:47px;	
  }
  h1 img {
    width:193px;
    height:54px;
  }
  h1 .logo {
    display:block;
    width:193px;
    height:54px;
    overflow:hidden;
    white-space:nowrap;
    text-indent: 100%;
    background: url(/Application/_includes/backend/img/logo.png) 0 0 no-repeat;
      background-size: 193px 54px;
      -moz-background-size: 193px 54px;
      -webkit-background-size: 193px 54px;
  }
  </style>
</head>
<body>
<div class="container">
	<div class="header">
     <? if ($_GET['logo']): ?>
         <h1><img src="<?= $_GET['logo']; ?>"/></h1>
      <? else: ?>
         <h1><span class="logo"></span></h1>
      <? endif; ?>
    </div>
</div>
</body>
</html>