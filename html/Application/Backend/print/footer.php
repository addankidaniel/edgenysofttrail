<?php 

error_reporting(E_ALL^E_NOTICE);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <style>
  body {margin: 0 0;}
  
  .footer {
      margin: 40px 0 0;
      position:relative;
      color:#FFFFFF;
      font-size:11px;
      padding: 0;
      height:40px;
      line-height: 20px;
      text-align:right;
      background: #214da1;
      padding: 15px 0;
   }
	.footer-content {
		margin: 0 auto;
		width: 760px;
		text-align: right;
		position:relative;
	}
	.footer .logo {
		width:134px;
		height:39px;
		overflow:hidden;
		white-space:nowrap;
		text-indent: 100%;
		background: url(/Application/_includes/backend/img/print-footer-logo.png) 0 0 no-repeat;
		display: inline-block;
	}
	.footer-content .page {
		position: absolute;
		left:0;
		top:-50px;
		color:#676767;
		font-size: 16px;
	}
  </style>
</head>
<body>

<div class="footer">
<div class="footer-content">
  <div class="page">page <?=$_GET['page'].'/'.$_GET['topage'];?></div>
	<div class="logo">Logo</div>
</div>
</div>
</body>
</html>