<?php

namespace ClickBlocks\UnitTest\MVC;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\MVC,
    ClickBlocks\UnitTest,
    ClickBlocks\Exceptions,
    ClickBlocks\Core;

/**
 * This TestCase class should be used to test single method (AJAX, probably) of Page Class
 * WORK IN PROGRESS
 * @property \ClickBlocks\Web\JS $js The mock instance of a JS class.
 */
abstract class PageMethodTest extends UnitTest\TestCase implements \ClickBlocks\API\IEdepoBase
{
  protected $params;
  protected $return;
  
  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $orc;
  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $svc;
  
   /**
    * The mock instance of a JS class.
    * 
    * @var    \ClickBlocks\Web\JS
    * @access public
    */
   public $js = null;
   
   /**
    * The mock instance of a FileSystem class.
    *
    * @var    \ClickBlocks\Utils\FileSystem
    * @access public
    */
   public $fs = null;

   /**
    * The mock instance of a CSS class.
    *
    * @var    \ClickBlocks\Web\CSS
    * @access public
    */
   public $css = null;

   /**
    * The mock instance of a HTML class.
    *
    * @var    \ClickBlocks\Web\HTML
    * @access public
    */
   public $html = null;

   /**
    * The mock instance of a AJAX class.
    *
    * @var    \ClickBlocks\Web\AJAX
    * @access public
    */
   public $ajax = null;

   /**
    * The mock instance of a ClickBlocks\Web\UI\Helpers class.
    *
    * @var    \ClickBlocks\Web\UI\Helpers
    * @access public
    */
   public $xhtml = null;

   /**
    * The mock instance of a WebForms\Validators class.
    *
    * @var    \ClickBlocks\Web\UI\Validators
    * @access public
    */
   public $validators = null;
   
   /**
    * The mock of xhtml-temlate of a page.
    * @var    string
    * @access private
    */
   public $tpl = null;
  
  private $mockOrchestras;
  private $mockServices;
  private $mockPage;
  private $properties = array();
  private $currentUser;
  private $invoked = false;
  
  public static function setUpBeforeClass()
  {
    parent::setUpBeforeClass();
  }
   
  public static function tearDownAfterClass()
  {
    parent::tearDownAfterClass();
  }
   
  public function setUp() 
  {
    parent::setUp();
    $this->invoked = false;
    $this->currentUser = null;
    $this->mockPage = null;
    $this->mockOrchestras = array();
    $this->mockServices = array();
  }
  
  public function tearDown()
  {
    parent::tearDown();
  }
  
  /**
   * @param string $origClass
   * @param array $stubMethods array of methods to stub, only used for Controller! 
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function setUpPageMock($origClass, array $stubMethods = null)
  {    
    $cls = substr($origClass, strrpos($origClass, '\\')+1);
    if (is_subclass_of($origClass,'\ClickBlocks\DB\Orchestra')) {
      return $this->mockOrchestras[substr($cls, 9)] = $this->getFullMock($origClass);
    } elseif (is_subclass_of($origClass,'\ClickBlocks\DB\Service')) {
      return $this->mockServices[substr($cls, 7)] = $this->getFullMock($origClass);
//    } elseif (is_subclass_of($origClass,'\ClickBlocks\MVC\IPage')) {
//      if (!$stubMethods)
//        throw new \Exception('Trying to mock Page with empty method list (would result in all methods stubbed - not expected behaviour');
//      return $this->mockPage = $this->getMock($origClass, $stubMethods );
    } else
      throw new \Exception("Type of $origClass is unknown");
  }
  
  protected function setUpPageProperty($name, $value)
  {
     $this->properties[$name] = $value;
  }

//  protected function setUpMockPage(array $stubMethods = null)
//  {
//     return $this->setUpPageMock($this->getPageClassName(), $stubMethods);
//  }
  
  protected function setUpMockService($shortName, array $stubMethods = null)
  {
     return $this->setUpPageMock('\ClickBlocks\DB\Service'.$shortName, $stubMethods);
  }
  
  protected function setUpMockOrchestra($shortName, array $stubMethods = null)
  {
     return $this->setUpPageMock('\ClickBlocks\DB\Orchestra'.$shortName, $stubMethods);
  }
  
  protected function getMethodName() { }
  protected function getPageClassName() { }
  
  private function getPage() {
     //if ($this->mockPage !== NULL) return $this->mockPage;
     $pageClass = $this->getPageClassName();
     return $this->getMockBuilder($pageClass)->disableOriginalConstructor()->setMethods(null)->getMock();
  }

  /**
   * @param mixed controller parameters 
   */
  protected function invokeThis($param1 = null, $param2 = null)
  {
     // TODO: page method invocation test (ajax request test?)
      $page = $this->getPage();
      if (!is_subclass_of($page, '\ClickBlocks\MVC\IPage')) 
         throw new \Exception('Incorrect page class defined in getPageClassName()');
      $method = $this->getMethodName();
      if (!$method) {
         $cls = get_class($this);
         if (stripos($cls,'Test')!==FALSE) $method = substr($cls, strrpos($cls, '\\')+1, -4);
      }
      if (!$method) throw new \Exception('Method for page '.$this->getPageClassName ().' is not defined');
      if (!is_callable(array($page, $method)))
          throw new \Exception('Page '.$this->getPageClassName().' does not have method '.$method.'()');
      foreach (array('ajax'=>'\ClickBlocks\Web\AJAX','js'=>'\ClickBlocks\Web\JS','css'=>'\ClickBlocks\Web\CSS','validators'=>'\ClickBlocks\Web\UI\POM\Validators','fs'=>'\ClickBlocks\Utils\FileSystem','tpl'=>'\ClickBlocks\Core\Template') as $property=>$class) {
         $mock = $this->getFullMock($class);
         $page->attachMock(self::MOCK_TYPE_PROPERTY, $property, $mock);
         $this->{$property} = $mock;
      }
      foreach ($this->properties as $name=>$value) $page->attachMock(self::MOCK_TYPE_PROPERTY, $name, $value);
      $user = $this->currentUser;
      if ($user === NULL) $user = new DB\QuickBLL(array(), 'ID');
      $page->attachMock(self::MOCK_TYPE_PROPERTY, 'user', $user);
      foreach ($this->mockOrchestras as $k=>$v) $page->attachMock(self::MOCK_TYPE_ORCHESTRA, $k, $v);
      foreach ($this->mockServices as $k=>$v) $page->attachMock(self::MOCK_TYPE_SERVICE, $k, $v);
      /**
       * TODO: should we call these ? 
       * $page->input();
         $page->assign();
         $page->load();
         $page->process();
         $page->unload();
         $page->redraw();
         $page->perform();
       */
      $this->return = call_user_func_array(array($page, $method), func_get_args());
      return $this->return;
  }
  
  /**
   *
   * @param array $userData 
   */
  protected function setCurrentUser(array $userData)
  {
      if (!$userData['ID']) {
        throw new \PHPUnit_Framework_Exception('userID or key required');
      }
      $this->currentUser = $this->getMock('ClickBlocks\DB\QuickBLL', array('update', 'insert', 'delete', 'save', 'replace'), array( $userData, 'userID' ));
      return $this->currentUser;
  }

    /**
   * @param string $short
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function getOrchestra($short)
  {
    if (!isset($this->mockOrchestras[$short]))
      throw new \Exception('Mock '.$short.' not found! Use setUpPageMock() to set up');
    return $this->mockOrchestras[$short];
  }
  
  /**
   * @param string $short
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function getService($short)
  {
    if (!isset($this->mockServices[$short]))
      throw new \Exception('Mock '.$short.' not found! Use setUpPageMock() to set up');
    return $this->mockServices[$short];
  }
  
  /**
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function getController()
  {
    if (!isset($this->mockPage))
      throw new \Exception('Mock controller not found! Use setUpPageMock() to set up');
    return $this->mockPage;
  }
  

}

?>