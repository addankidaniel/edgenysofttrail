<?php

namespace ClickBlocks\UnitTest;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\Core;

abstract class FailStub implements \ArrayAccess
{
  public function __call($m, $v) { self::fail(); }
  public function __get($k) { self::fail(); }
  public function __set($k,$v) { self::fail(); }
  public function offsetGet($k) { self::fail(); }
  public function offsetSet($k,$v) { self::fail(); }
  public function offsetExists($k) { self::fail(); }
  public function offsetUnset($k) { self::fail(); }
  
  protected static function getFailMessage() {}

  protected static function fail() {
    throw new \PHPUnit_Framework_Exception(static::getFailMessage());
  }
}

abstract class TestCase extends \PHPUnit_Framework_TestCase 
{
  protected static $reg;
  private static $setUpBeforeClass = false;
  protected static $convertClass = array();

  public static function setUpBeforeClass()
  {
    self::$reg = Core\Register::getInstance();
    //Core\Loader::getInstance()->onPreload = __CLASS__.'::onPreloadClass';
    self::$setUpBeforeClass = true;
  }

  public static function tearDownAfterClass()
  {
    // self::$db0 = self::$db1 = null;
  }
  
  /**
   * NOT WORKING...
   * @param string $class
   * @param array $classes
   * @return string
   */
  public static function fakeAutoLoad($class, array $classes)
  {
    if ($class == 'ClickBlocks\DB\DB')
    {
      $class = 'ClickBlocks\UnitTest\DBFailStub';
      $cs = new Utils\InfoClass();
      $cs->extraction($class);
      $cs->{$class}->name = '\ClickBlocks\DB\DB';
      eval($cs->getCodeClass($class));
      return;
    }
    return $class;
  }
   
  public function setUp() 
  {
//    $reg = Core\Register::getInstance();
//    
//    //print_r($reg->loader);
//    $reg->loader->onPreload = '\Clickblocks\UnitTest\TestCase::onPreloadClass';
  }
  
  public static function mockClass($originalClassName) {
     return self::$convertClass[$originalClassName] = true;
  }

  public static function onPreloadClass($class) {
    if (isset(self::$convertClass[$class]))
        return self::$convertClass[$class];
    else return $class;
  }
  
  

  public static function assertArrayContainsArray(array $needle, array $haystack) {
    if ($needle != array_intersect_key($haystack, $needle))
      throw new \PHPUnit_Framework_ExpectationFailedException("Array1 doesn't contain array2! ".PHP_EOL
          .'Expected: '.print_r($needle,1).PHP_EOL
          .'Actual: '.print_r($haystack,1));
  }
  
  /**
   * returns mock object with all methods stubbed, incl. constructor
   * @param string $className
   * @return PHPUnit_Framework_MockObject_MockObject 
   */
  public function getFullMock($className) {
    return $this->getMockBuilder($className)->disableOriginalConstructor()->disableOriginalClone()->getMock();
  }
  
  public static function getMockWrapper(\PHPUnit_Framework_MockObject_MockObject $mock) {
     return new MockWrapper($mock);
  }
  
  public static function initUnitTests()
  {
      $reg = Core\Register::getInstance();
      $reg->unitTestMode = true;
      $config = $reg->config;
      $config->useCacheForDataObjects = false;
      $config->orm['lazyLoading'] = true;
      $orm = DB\ORM::getInstance();
      $mockDB = \PHPUnit_Framework_MockObject_Generator::getMock('\ClickBlocks\DB\DB', array('connect','execute','insert','delete','replace','update','col','cols','row','rows','disconnect'), array(), '', FALSE);
      $mockDB->expects(self::any())->method('connect')->will(self::returnValue(true));
      //$mockDB->expects(self::any())->method('execute')->will(self::throwException(new \PHPUnit_Framework_Exception('Attempt to access ORM layer in Unit Test!')));
      foreach (array('db0') as $alias) $orm->addDB($alias, $mockDB);
      //$config->db0 = $config->db1 = $config->db2 = new FailStub;
  }

}

?>