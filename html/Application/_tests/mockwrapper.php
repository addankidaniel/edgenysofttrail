<?php

namespace ClickBlocks\UnitTest;

class MockWrapper
{
   protected $mock;
   
   public function __construct($mock) {
      if (!method_exists($mock, 'expects')) throw new \Exception('Incorrect Mock object');
      $this->mock = $mock;
   }
   
   public function __call($name, $arguments) {
      return call_user_func_array(array($this->mock, $name), $arguments);
   }
   
   public function __set($name, $value) {
      return $this->mock->{$name} = $value;
   }
   
   public function __get($name) {
      return $this->mock->{$name};
   }
   
   public function getMock() {
      return $this->mock;
   }
   
   public function once() {
      return $this->expects(1);
   }
   
   public function never() {
      return $this->expects(0);
   }
   
   public function exactly($n)
   {
      return $this->exactly($n);
   }

   public function atLeastOnce() {
      return $this->expects(new \PHPUnit_Framework_MockObject_Matcher_InvokedAtLeastOnce);
   }

   public function expects($count = null) {
      if (is_numeric($count)) $count = new \PHPUnit_Framework_MockObject_Matcher_InvokedCount($count);
      elseif ($count === NULL) $count = new \PHPUnit_Framework_MockObject_Matcher_AnyInvokedCount();
      //if (!is_object($count) || !($count instanceof \PHPUnit_Framework_MockObject_Matcher_Invocation)) throw new \Exception('Incorrect expectation');
      return new InvocationMocker($this->mock->expects($count));
   }
}

?>