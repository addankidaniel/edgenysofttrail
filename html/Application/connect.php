<?php

$mt = microtime(true);

function foo($foo) {return $foo;}
function castToClass($class, $object) {return unserialize(preg_replace('/^O:\d+:"[^"]++"/', 'O:' . strlen($class) . ':"' . $class . '"', serialize($object)));}
function err_msg($const, array $params = null)
{
   $err = constant($const); $count = 1;
   if ($params !== null) foreach ($params as $key => $value)
   { 
      if (!is_string($key)) $key = '/\[{var}\]/';
      $err = preg_replace($key, $value, $err, 1);
   }
   return 'Token: ' . $const . '. ' . $err;
}
function ID($id){return \ClickBlocks\Core\Register::getInstance()->page->get($id)->uniqueID;}
/**
 * Replacement for rename() function that uses copy+unlink. Use this to move files to NFS folders.
 * @param string $src
 * @param string $dst
 * @return boolean
 */
function rename2($src, $dst, $context = null) {
  $args = array($src, $dst);
  if ($context) $args[] = $context;
  if (!call_user_func_array('copy', $args)) return false;
  array_splice($args, 1, 1);
  return call_user_func_array('unlink', $args);
}

set_time_limit(0);

error_reporting(E_ALL & ~E_NOTICE);

date_default_timezone_set('UTC');

if (!defined('NO_GZHANDLER')) ob_start('ob_gzhandler');

$_SERVER['DOCUMENT_ROOT'] = substr(__DIR__, 0, -12); // the length of '/Application' constant equals 12

require_once($_SERVER['DOCUMENT_ROOT'] . '/Application/_config/constants.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/errors.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/register.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/io.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/logger.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/logexception.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/debugger.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/core/loader.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Framework/_engine/cache/cache.php');

$config = new \ClickBlocks\Core\Config();
$config->init($config->root . '/Application/_config/config.ini');
$config->init($config->root . '/Application/_config/.local.ini');
$config->siteUniqueID = md5($config->root);

$reg = \ClickBlocks\Core\Register::getInstance();
$reg->config = $config;
$reg->logger = \ClickBlocks\Core\Logger::getInstance($mt);
$reg->debugger = \ClickBlocks\Core\Debugger::getInstance();
$reg->cache = \ClickBlocks\Cache\Cache::factory($config->cache['type'], $config->cache);
$reg->loader = \ClickBlocks\Core\Loader::getInstance();


if( $config->isDebug ) {
	//error_reporting( E_ALL ); // TODO -- fix nasty notices (~E_NOTICE)
	ini_set( 'log_errors', 1 );
	ini_set( 'error_log', $_SERVER['DOCUMENT_ROOT'] . \ClickBlocks\Core\IO::url( 'logs' ) . DIRECTORY_SEPARATOR . 'php-error.log' );
}

// cannot be set via ini_set, must be in php.ini
//ini_set( 'short_open_tag', 1 );
//ini_set( 'upload_max_filesize', '15M' );
//ini_set( 'post_max_size', '256M' );
//ini_set( 'max_file_uploads', '500' );

ini_set( 'memory_limit', $config->memoryLimit );
ini_set( 'date.timezone', $config->dateTimezone );
ini_set( 'session.save_handler', $config->session['save_handler'] );
ini_set( 'session.save_path', $config->session['save_path'] );
ini_set( 'session.name', $config->session['name'] );
ini_set( 'session.cookie_domain', $config->session['cookie_domain'] );
ini_set( 'session.gc_maxlifetime', $config->session['gc_maxlifetime'] );

if( isset( $_GET[session_name()] ) ) {
	session_id( $_GET[session_name()] );
}
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/Utilities/tempcleaner.php');
foo(new \ClickBlocks\Utils\TempFolderCleaner())->cleanWithProbability();

if (isset($_GET['__DEBUG_INFORMATION__']) && isset($_SESSION['__DEBUG_INFORMATION__'][$_GET['__DEBUG_INFORMATION__']]))
{
  \ClickBlocks\Core\Debugger::$output = $_SESSION['__DEBUG_INFORMATION__'][$_GET['__DEBUG_INFORMATION__']];
  exit;
}

/**
 * Initialize using the source, or default if source index or property doesn't exist
 * 
 * @param string|int $element - Index or Property
 * @param array|object $source - Array or Object
 * @param mixed $default
 * @return mixed
 */
function esd($element,&$source,$default='') {
	if(is_array($source)) {
		if(isset($source[$element])) {
			return $source[$element];
		}
	}
	elseif(is_object($source)) {
		if($source instanceof ClickBlocks\DB\BLLTable) {
			$dals = $source->getDALs();
			foreach($dals as $dal) {
				if(is_array($dal->getField($element)))
					return $source->$element;
			}
		}
		elseif(property_exists($source,$element)) {
			return $source->$element;
		}
	}
	return $default;
}
