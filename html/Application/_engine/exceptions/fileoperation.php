<?php

namespace ClickBlocks\Core;

class FileOperationException extends \Exception
{
   public function __construct($message, $code, $previous) {
      if ($previous instanceof \Exception) {
         if (!$message) $message = $previous->getMessage ();
         if (!$code) $code = $previous->getCode ();
      }
      parent::__construct($message, $code, $previous);
   }
}

?>
