<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\UserCustomSortFolders;

class ServiceUserCustomSortFolders extends Service {

	public function __construct() {
		parent::__construct( UserCustomSortFolders::NSCLASS );
	}
}
