<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property enum $sortObject
 * @property enum $sortBy
 * @property enum $sortOrder
 */
class ServiceUserSortPreferences extends Service {
	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\UserSortPreferences' );
	}
}
