<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceExhibitHistory extends Service
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\ExhibitHistory' );
	}
}
