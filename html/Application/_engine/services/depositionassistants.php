<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceDepositionAssistants extends Service
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\DepositionAssistants');
  }
  
  public function setDA($depoID, $userID)
  {
     $da = $this->getByID(array('depositionID'=>$depoID,'userID'=>$userID));
     if (!$da->userID) {
         $da->depositionID = $depoID;
         $da->userID = $userID;
         $da->insert();
     }
     return true;
  }
  
  public function unsetDA($depoID, $userID)
  {
     $da = $this->getByID(array('depositionID'=>$depoID,'userID'=>$userID));
     if ($da->userID) {
         $da->delete();
     }
     return true;
  }
}

?>