<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceCaseManagers extends Service
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\CaseManagers');
  }
  
  public function setCM($caseID, $userID)
  {
     $cm = $this->getByID(array('caseID'=>$caseID,'userID'=>$userID));
     if (!$cm->userID) {
         $cm->caseID = $caseID;
         $cm->userID = $userID;
         $cm->created = new SQLNOWValue();
         $cm->insert();
     }
     return true;
  }
  
  public function unsetCM($caseID, $userID)
  {
     $cm = $this->getByID(array('caseID'=>$caseID,'userID'=>$userID));
     if ($cm->userID) {
         $cm->delete();
     }
     return true;
  }
}

?>