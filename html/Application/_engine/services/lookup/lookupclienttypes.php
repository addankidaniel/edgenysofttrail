<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceLookupClientTypes extends Service
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\LookupClientTypes');
  }
}

?>