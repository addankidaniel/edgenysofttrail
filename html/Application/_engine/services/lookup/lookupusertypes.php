<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceLookupUserTypes extends Service
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\LookupUserTypes');
  }
}

?>