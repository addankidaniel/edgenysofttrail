<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceCases extends Service
{
	public function __construct()
	{
		parent::__construct('\ClickBlocks\DB\Cases');
	}
}

?>