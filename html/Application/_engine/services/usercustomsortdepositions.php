<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property bigint $depositionID
 * @property bigint $sortPos
 */
class ServiceUserCustomSortDepositions extends Service
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\UserCustomSortDepositions' );
	}
}
