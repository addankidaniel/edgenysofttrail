<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\Presentations;

/**
 * @property bigint $depositionID
 * @property bigint $fileID
 */
class ServicePresentations extends Service
{

	public function __construct()
	{
		parent::__construct( Presentations::BLL_CLASSNAME );
	}
}
