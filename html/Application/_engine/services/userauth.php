<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceUserAuth extends Service {

	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\UserAuth' );
	}

	public function getByUserID( $userID ) {
		return $this->getByID( $this->getOrchestra( 'UserAuth' )->getIDByUserID( $userID ) );
	}
}
