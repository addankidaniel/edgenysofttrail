<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\API,
    ClickBlocks\Cache;

class ServicePlatformLog extends Service
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\PlatformLog' );
	}

	public static function addLog( $resellerID, $optionID, $quantity, $clientID=NULL, $userID=NULL, $caseID=NULL, $depositionID=NULL, $created=NULL )
	{
		$optionID = (int)$optionID;
		$pLog = new \ClickBlocks\DB\PlatformLog();
		$reseller = new \ClickBlocks\DB\Clients( $resellerID );
		if( !$reseller || !$reseller->ID || $reseller->ID != $resellerID || $reseller->typeID != \ClickBlocks\API\IEdepoBase::CLIENT_TYPE_RESELLER ) {
			\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid Reseller: {$resellerID}" );
			return;
		}
		if( !in_array( $optionID, \ClickBlocks\MVC\Edepo::getPricingOptions() ) ) {
			\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid Pricing Option: {$optionID}" );
			return;
		}
		$quantity = (int)$quantity;
		if( !$quantity || $quantity <= 0 ) {
			\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid Quantity: {$quantity}" );
			return;
		}
		$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		$pmo = $orchPMO->getByClientAndOption( $resellerID, $optionID );
		if( !$pmo || !is_array( $pmo ) || !isset( $pmo['price'] ) || !isset( $pmo['typeID'] ) ) {
			\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Pricing Model not found -- reseller: {$resellerID}, option: {$optionID}" );
			return;
		}
		$pLog->resellerID = $reseller->ID;
		$pLog->optionID = $optionID;
		$pLog->pricingTerm = $pmo['typeID'];
		$pLog->created = ($created) ? date( 'Y-m-d H:i:s', strtotime( $created ) ) : date( 'Y-m-d H:i:s' );
		$pLog->price = round( floatval( $pmo['price'] ), 2 );
		$pLog->quantity = $quantity;
		if( $clientID ) {
			$client = new \ClickBlocks\DB\Clients( $clientID );
			if( !$client || !$client->ID || $client->ID != $clientID ) {
				\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid Client: {$clientID}" );
				return;
			}
			$pLog->clientID = $client->ID;
		}
		if( $userID ) {
			$user = new \ClickBlocks\DB\Users( $userID );
			if( !$user || !$user->ID || $user->ID != $userID ) {
				\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid User: {$userID}" );
				return;
			}
			$pLog->userID = $user->ID;
		}
		if( $caseID ) {
			$case = new \ClickBlocks\DB\Cases( $caseID );
			if( !$case || !$case->ID || $case->ID != $caseID ) {
				\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid Case: {$caseID}" );
				return;
			}
			$pLog->caseID = $case->ID;
			if( in_array( $optionID, [API\IEdepoBase::PRICING_OPTION_CASE_SETUP, API\IEdepoBase::PRICING_OPTION_CASE_MONTHLY] ) && $case->isDemo() ) {
//				$pLog->price = 0;
				return;
			}
			if( $depositionID ) {
				$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
				if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID || $deposition->caseID != $caseID ) {
					\ClickBlocks\Debug::ErrorLog( "PlatformLog::addLog(); Invalid Deposition: {$depositionID}" );
					return;
				}
				$pLog->depositionID = $deposition->ID;
				$demoOptions = [API\IEdepoBase::PRICING_OPTION_DEPOSITION_SETUP, API\IEdepoBase::PRICING_OPTION_DEPOSITION_MONTHLY, API\IEdepoBase::PRICING_OPTION_WITNESSPREP_ATTENDEE];
				if( in_array( $optionID, $demoOptions ) && $deposition->isDemo() ) {
//					$pLog->price = 0;
					return;
				}
			}
		}
		$pLog->insert();
	}
}