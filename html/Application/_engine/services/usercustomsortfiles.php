<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\UserCustomSortFiles;

/**
 * @property bigint $userID
 * @property bigint $fileID
 * @property bigint $sortPos
 */
class ServiceUserCustomSortFiles extends Service {

	public function __construct() {
		parent::__construct( UserCustomSortFiles::NSCLASS );
	}

}
