<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\API,
    ClickBlocks\Cache;

class ServiceInvoiceCharges extends Service
{
	public function __construct()
	{
	  parent::__construct('\ClickBlocks\DB\InvoiceCharges');
	}

	public static function charge( $client, $optionID, $quantity, $caseID=NULL, $depoID=NULL, $userID=NULL )
	{
//		$debugInfo = ['method' => 'ServiceInvoiceCharges::charge', 'client' => $client, 'optionID' => $optionID, 'quantity' => $quantity, 'caseID' => $caseID, 'depoID' => $depoID, 'userID' => $userID];
//		\ClickBlocks\Debug::ErrorLog( print_r( $debugInfo, TRUE ) );
		if( (int)$quantity===0 || !$client || !$optionID ) {
			throw new Core\Exception( 'Invalid parameters' );
		}

		// ED-1459: All demos are free!
		if( $depoID ) {
			$deposition = new \ClickBlocks\DB\Depositions( $depoID );
			if( $deposition->isDemo() ) {
				return;
			}
		}
		if( $caseID ) {
			$case = new \ClickBlocks\DB\Cases( $caseID );
			if( $case->isDemo() ) {
				return;
			}
		}

		if( is_scalar( $client ) ) {
		   $client = new \ClickBlocks\DB\Clients( $client );
//		   $client->assignByID( $id );
		}

		$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		$orchPEO = new \ClickBlocks\DB\OrchestraPricingEnterpriseOptions();

		if( $client->typeID == API\IEdepoBase::CLIENT_TYPE_COURTCLIENT ) {
			\ClickBlocks\Debug::ErrorLog( "No charges for Court Client(s); OptionID: {$optionID}, Qty: {$quantity}" );
			return;
		}

		// Charge Client for performing this action
		if( $client->typeID == API\IEdepoBase::CLIENT_TYPE_CLIENT ) {
			$clientOption = $orchPMO->getByClientAndOption( $client->ID, $optionID );
			if ($clientOption['typeID'] == 'O' && (float)$clientOption['price'] > 0) {
				// only charge if this option is one-time
				$price = (float)$clientOption['price'] * (int)$quantity;
				self::chargeOneClient( $client->ID, NULL, $optionID, $quantity, $price, $caseID, $depoID, $userID );
			}

			// Check if a Reseller to charge
			if( $client->resellerID ) {
				// Charge Reseller for reselling to client
				$resellerOption = $orchPMO->getByClientAndOption( $client->resellerID, $optionID );
				if( $resellerOption['typeID'] == 'O' && (float)$resellerOption['price'] > 0 ) {
					// only charge if this option is one-time
					$price = round( ((float)$resellerOption['price'] * (int)$quantity), 2 );
					self::chargeOneClient( $client->resellerID, $client->ID, $optionID, $quantity, $price, $caseID, $depoID, $userID );
				}
			}
			return;
		}
		if( $client->typeID == API\IEdepoBase::CLIENT_TYPE_ENT_CLIENT ) {
			$orchDepo = new \ClickBlocks\DB\OrchestraDepositions();
			$resellerID = ($depoID) ? $orchDepo->getEnterpriseResellerID( $depoID ) : NULL;
			
			$ecPricingOptions = [
				API\IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1,
				API\IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2,
				API\IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3,
				API\IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1,
				API\IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2,
				API\IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3
			];

			if( in_array( $optionID, $ecPricingOptions ) ) {
				if( !$resellerID ) {
					throw new \LogicException( 'Deposition must be defined for Enterprise Exhibit Charges.' );
				}
				$clientOption = $orchPEO->getByClientResellerAndOption( $client->ID, $resellerID, $optionID );
			} else {
				$clientOption = $orchPMO->getByClientAndOption( $client->ID, $optionID );
				if( !isset( $clientOption['isEnterprise'] ) || $clientOption['isEnterprise'] != 1 ) {
					$resellerID = NULL;
				}
			}

			if( $clientOption['typeID'] == 'O' && (float)$clientOption['price'] > 0 ) {
				// only charge if this option is one-time
				$price = (float)$clientOption['price']*(int)$quantity;
				self::chargeOneClient($client->ID, null, $optionID, $quantity, $price, $caseID, $depoID, $userID);
			}

			// Check if a Reseller to charge
			if( $resellerID ) {
				// Charge Reseller for reselling to client
				$resellerOption = $orchPMO->getByClientAndOption( $resellerID, $optionID );
				if( $resellerOption['typeID'] == 'O' && (float)$resellerOption['price'] > 0 ) {
					// only charge if this option is one-time
					$price = (float)$resellerOption['price']*(int)$quantity;
					self::chargeOneClient( $resellerID, $client->ID, $optionID, $quantity, $price, $caseID, $depoID, $userID );
				}
			}
			return;
		}

		throw new \LogicException( 'Only Client can be charged directly.' );
	}

	public static function insertFeeAdjustment( $resellerID, $optionID, $price, $userID=null)
	{
		$charge = new InvoiceCharges();

		$charge->clientID = $resellerID;
		$charge->price = $price;
		$charge->optionID = $optionID;
		$charge->quantity = 0;

		if ($userID)
		{
			$charge->userID = $userID;
		}

		$charge->insert();
	}

	protected static function chargeOneClient( $clientID, $childClientID, $optionID, $quantity, $price, $caseID=NULL, $depoID=NULL, $userID=NULL )
	{
		$charge = new InvoiceCharges();
		$charge->clientID = $clientID;
		$charge->childClientID = $childClientID;
		$charge->optionID = $optionID;
		$charge->quantity = (int)$quantity;
		$charge->price = $price;
		if( $caseID ) {
			$charge->caseID = $caseID;
		}
		if( $depoID ) {
			$charge->depositionID = $depoID;
		}
		if( $userID ) {
			$charge->userID = $userID;
		}
		$charge->insert();
	}

	public static function addResellerCharges( $resellerID, $clientID, $optionID, $quantity, $price, $caseID=NULL, $depositionID=NULL, $userID=NULL )
	{
		$created = date( 'Y-m-d H:i:s' );

		//reseller
		$resellerCharge = new InvoiceCharges();
		$resellerCharge->clientID = (int)$resellerID;
		$resellerCharge->childClientID = (int)$clientID;
		$resellerCharge->optionID = (int)$optionID;
		$resellerCharge->quantity = (int)$quantity;
		$resellerCharge->price = (float)$price;
		if( $caseID ) {
			$resellerCharge->caseID = (int)$caseID;
		}
		if( $depositionID ) {
			$resellerCharge->depositionID = (int)$depositionID;
			$depo = new \ClickBlocks\DB\Depositions( $resellerCharge->depositionID );
			if( $depo->ID === $resellerCharge->depositionID ) {
				$created = $depo->started;
			}
		}
		if( $userID ) {
			$resellerCharge->userID = (int)$userID;
		}
		$resellerCharge->created = $created;
//		\ClickBlocks\Debug::ErrorLog( "Reseller charges: " . print_r( $resellerCharge->getValues(), TRUE ) );
		$resellerCharge->insert();
	}
}
