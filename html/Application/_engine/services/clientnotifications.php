<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceClientNotifications extends Service
{
	public function __construct()
	{
		parent::__construct('\ClickBlocks\DB\ClientNotifications');
	}
}

?>