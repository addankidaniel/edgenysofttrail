<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceEnterpriseAgreements extends Service
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\EnterpriseAgreements' );
	}

	public function getByResellerIDClientID( $rid, $cid, $expire=null)
	{
		$pk = (new OrchestraEnterpriseAgreements())->getAgreementByResellerIDClientID( $rid, $cid );
		return $this->getByID( $pk['ID'], $expire );
	}
}

?>