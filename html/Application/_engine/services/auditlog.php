<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceAuditLog extends Service
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\AuditLog');
  }

  public static function log($actionID, $actionBy, $relatedTo = null, $description = null)
  {
     $log = new DB\AuditLog;
     $log->createdBy = $actionBy;
     $log->relatedTo = $relatedTo;
     $log->description = $description;
     $log->actionID = $actionID;
     $log->insert();
  }
}

?>