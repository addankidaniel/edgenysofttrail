<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class ServiceDepositions extends Service
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\Depositions');
  }

  public function useCache() {
	  return FALSE;
  }
}

?>