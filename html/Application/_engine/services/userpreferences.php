<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property enum $prefKey
 * @property string $value
 */
class ServiceUserPreferences extends Service {
	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\UserPreferences' );
	}
}
