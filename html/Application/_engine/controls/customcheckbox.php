<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers;

class CustomCheckBox extends CheckBox
{
   
   public function __construct($id, $value = null, $caption = null) {
      parent::__construct($id, $value, $caption);
      $this->properties['checkBoxTemplate'] = 1;
   }

   public function JS()
   {
      if (!$this->properties['disabled'])
      {
        if ($this->ajax->isSubmit()) $p = 'parent.';
        $script = $p .'chbox.add(\'' . $this->attributes['uniqueID'] . '\','.(int)$this->properties['checked'].');';
        if (Web\Ajax::isAction()) $this->ajax->script($script, $this->updated, true);
        else 
        {
         $this->js->add(new Helpers\Script('chbox', null, Core\IO::url('common-js') . '/customcheckbox.js'), 'link');
         $this->js->add(new Helpers\Script('chbox_' . $this->attributes['uniqueID'], $script), 'foot');
        }
      }
      return $this;
   }
   
   public function render()
   {
      if (!$this->properties['visible']) return $this->invisible();
      $this->addClass('chkbox-block');
      $attrOnClick = $this->attributes['onclick'];
      if (!$this->properties['disabled'])
      {
         $onclick = 'controls.stopEvent(event);chbox.click(\''.$this->attributes['uniqueID'] . '\', event);';
         $this->attributes['onclick'] = $onclick . $attrOnClick;
      } else {
         $this->attributes['onclick'] = '';
      }
      if ($this->properties['caption'] != '') {
         switch ($this->properties['checkBoxTemplate']) {
            case 2:
               $label = '<label class="f_l m_l15 p_t1">' . $this->properties['caption'] . '</label>';
               break;
            default:
               $label = '<label>' . $this->properties['caption'] . '</label>';
         }
      }
      if ($this->properties['align'] == 'left') $html .= $label;
      $UID = $this->attributes['uniqueID'];
      $this->attributes['uniqueID'] = 'block_' . $this->attributes['uniqueID'];
      $html .= '<div' . $this->getParams() . '>';
      $this->attributes['uniqueID'] = $UID;
      $html .= '<input type="checkbox" runat="server" ' . ($this->properties['checked'] ? 'checked="checked"' : '') . ' style="display:none;" id="' . $this->attributes['uniqueID'] . '" value="' . htmlspecialchars($this->attributes['value']) . '" name="' . htmlspecialchars($this->attributes['name']) . '" />';
      switch ($this->properties['checkBoxTemplate']) {
         case 2:
            $html .= '<a id="box_' . $this->attributes['uniqueID'].'" class="custom_checkbox '. ($this->properties['checked'] ? 'checked' : '') .'"></a>';
            break;
         default:
            $html .= '<div id="box_' . $this->attributes['uniqueID'].'" class="chkbox '. ($this->properties['checked'] ? 'checked' : '') .'"><div>&nbsp;</div></div>';
      }
      if ($this->properties['align'] == 'right') $html .= $label;
      $html .= '</div>';
      $this->attributes['onclick'] = $attrOnClick;
      return $html;
   }

   protected function repaint()
   {
      parent::repaint();
      if (!$this->properties['visible']) return;
      $this->JS();
   }
}

?>
