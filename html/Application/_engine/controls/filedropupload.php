<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Web;

/**
 * Allows for Drag&Drop upload of files, dragged to this panel.
 * IE doesn't support Drag&Drop upload of files.
 */
class FileDropUpload extends Panel
{
   public function __construct($id)
   {
      parent::__construct($id);
      $this->properties['onDragEnter'] = null;
      $this->properties['onDragLeave'] = null;
      //$this->properties['onUpload'] = null;
      $this->properties['onDone'] = null;
      $this->properties['onProgress'] = null;
      $this->properties['validateFunction'] = null;
      $this->properties['callback'] = null;
   }
   
   public function JS() {
      if (!$this->properties['disabled'])
      {
        if ($this->ajax->isSubmit()) $p = 'parent.';
        $evts = array();
        //foreach (array('onDone','onDragEnter','onDragLeave') as $evt) $evts[$evt] = ($this->properties[$evt]) ? 'function(){ '.$this->properties[$evt] .'}' : null;
        $params = array($this->attributes['uniqueID'], $this->properties['callback']);
        foreach ($params as &$param) $param = json_encode($param);
        foreach (array('onDone','onDragEnter','onDragLeave','onProgress','validateFunction') as $evt) $params[] = $p.$this->properties[$evt] ?: 'null';
        $script = $p .'fd.initializeControl(' . implode(', ', $params) . ');';
        if (Web\Ajax::isAction()) $this->ajax->script($script, $this->updated ? $this->updated+100 : 0, true);
        else 
        {
         $this->js->add(new Helpers\Script('filedrop', null, Core\IO::url('common-js') . '/filedrop.js'), 'link');
         $this->css->add(new Helpers\Style('filedrop', null, Core\IO::url('common-css') . '/filedrop.css'), 'link');
         $this->js->add(new Helpers\Script('filedrop_' . $this->attributes['uniqueID'], $script), 'foot');
        }
      }
      return $this;
   }
   
   protected function repaint() {
      parent::repaint();
      $this->JS();
   }


   /*public function callOnDone() {
      if (!Web\Ajax::isAction() || !$_REQUEST['fd-callback']) return;
      $this->ajax->script("try { window.top.{$_REQUEST['fd-callback']}(); } catch (e) { }", 1);
   }*/

}

?>
