<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
	ClickBlocks\Web,
	ClickBlocks\Web\UI\Helpers;

class PageSize extends CustomDropDownBox
{
	public function __construct( $id, $value=NULL )
	{
		parent::__construct( $id, $value );
		$this->properties['options'] = ['10'=>10,'20'=>20,'30'=>30];
	}

	public function __set( $param, $value )
	{
		parent::__set( $param, $value );
		if( $param === 'value' ) {
			$this->page->pageSession['pageSize'] = (int)$value;
		}
	}

	public function render()
	{
		if( isset( $this->page->pageSession['pageSize'] ) ) {
			$pageSize = (int)$this->page->pageSession['pageSize'];
		}
		return parent::render();
	}
}