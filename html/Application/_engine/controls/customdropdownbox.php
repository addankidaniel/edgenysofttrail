<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core,
    ClickBlocks\Web,
	   ClickBlocks\Web\UI\Helpers;

class CustomDropDownBox extends DropDownBox
{
  public function __construct($id, $value = null)
  {
    parent::__construct($id, $value);
    $this->properties['tag'] = 'div';
	   $this->properties['rounded'] = false;
  }

  public function JS()
  {
    if (!$this->properties['disabled'])
	   {
      if ($this->ajax->isSubmit()) $p = 'parent.';
	     $script = $p . 'cddb.initialize(\'' . $this->attributes['uniqueID'] . '\');';
      if (Web\Ajax::isAction()) $this->ajax->script($script, $this->updated, true);
      else 
	     {
	       $this->js->add(new Helpers\Script('cddb', null, Core\IO::url('common-js') . '/customddb.js'), 'link');
	       $this->js->add(new Helpers\Script('customddb_' . $this->attributes['uniqueID'], $script), 'foot');
	     }
	   }
    return $this;
  }

  public function render()
  {
	   if (!$this->properties['visible']) return $this->invisible();
	   $stack = new \SplStack();
	   $stack->push($this->attributes['style']);
	   $stack->push($this->attributes['class']);
	   $stack->push($this->attributes['onchange']);
	   $this->attributes['style'] = 'display:none;';
	   $this->attributes['class'] = $this->hasClass('validate') ? 'validate' : '';
	   $this->attributes['onchange'] = '';
    $ddb = parent::render();
    $this->attributes['onchange'] = $stack->pop();
	   $this->attributes['class'] = $stack->pop();
	   $this->attributes['style'] = $stack->pop();
	   if ($this->properties['rounded']) $this->addClass('round-input');
	   else $this->addClass('custom-select');
	   if ($this->properties['disabled']) $this->addClass('disabled');
	   else $this->removeClass('disabled');
    if (!$this->hasStyle('width')) $this->addStyle('width', '100px');
	   if ($this->properties['defaultValue'] && $this->properties['value'] == '') $value = $this->properties['defaultValue'];
	   else
	   {
	     $value = $this->properties['value'] != '' ? $this->properties['options'][$this->properties['value']] : current((array)$this->properties['options']);
	   }
	   $html = '<' . $this->properties['tag'] . ' id="container_' . $this->attributes['uniqueID'] . '" class="' . htmlspecialchars($this->attributes['class']) . '" style="' . htmlspecialchars($this->attributes['style']) . '">';
    preg_match('/(\-?\d+)(.*)/', $this->getStyle('width'),$match);
	   $width = (int)$match[1] - ($this->properties['rounded'] ? 20 : 0);
    $unit = $match[2];
	   if ($this->properties['rounded']) $html .= '<div class="left"></div><div class="center p_r0"><div class="custom-select" style="width:' . $width . $unit.';">';
	   $html .= '<span id="select_' . $this->attributes['uniqueID'] . '" class="arrow button" onclick="cddb.open(\''.$this->attributes['uniqueID'].'\',event)"></span>';
    $html .= '<div id="val_' . $this->attributes['uniqueID'] . '" class="field" style="width:' . ($width - 27) . 'px;" onclick="cddb.open(\''.$this->attributes['uniqueID'].'\',event)">' . htmlspecialchars($value) . '</div>';
	   if (!is_array($this->properties['options'])) $html .= $this->properties['options'];
	   else
	   {
      $ops = $this->properties['options'];
      if ($this->properties['defaultValue'] != '') $ops = array('' => $this->properties['defaultValue']) + $ops;
		    $html .= '<ul id="list_' . $this->attributes['uniqueID'] . '" style="display:none;width:' . ($width - 2 + ($this->properties['rounded'] ? 20 : 0)) . $unit.';" class="_custom_select_list">';
      foreach ($ops as $key => $option)
      {
        if (is_array($option)) continue;
        $html .= '<li onclick="cddb.select(\'' . $this->attributes['uniqueID'] . '\', \'' . addslashes(htmlspecialchars($key)) . '\', this.innerHTML);' . $this->attributes['onchange'] . '">' . htmlspecialchars($option) . '</li>';
      }
		    $html .= '</ul>';
	   }
	   $html .= $ddb;
	   if ($this->properties['rounded']) $html .= '</div></div><div class="right"></div>';
	   $html .= '</' . $this->properties['tag'] . '>';
    return $html;
  }

  protected function repaint()
  {
    parent::repaint();
    if (!$this->properties['visible']) return;
    $this->JS();
  }

  protected function getRepaintID()
  {
    return 'container_' . $this->attributes['uniqueID'];
  }
}
