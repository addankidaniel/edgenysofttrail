<?php

namespace ClickBlocks\DB;


class OrchestraUserSortPreferences extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\UserSortPreferences' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\UserSortPreferences';
	}

	/**
	 * User Sort Preferences for User
	 * @param bigint $userID
	 * @return array
	 */
	public function getSortPreferencesForUserID( $userID )
	{
		$userID = (int)$userID;
		$sql = 'SELECT * FROM UserSortPreferences WHERE userID=:userID';
		return $this->db->rows( $sql, ['userID'=>$userID] );
	}
	
	/**
	 * User Sort Preferences for User By Sort Object
	 * @param bigint $userID
	 * @param string $sortObject
	 * @return array
	 */
	public function getSortPreferencesForUserIDBySortObject( $userID, $sortObject ) {
		$sql = 'SELECT * FROM UserSortPreferences WHERE userID=:userID AND sortObject=:sortObject';
		return $this->db->row( $sql, ['userID' => (int)$userID, 'sortObject' => $sortObject] );
	}
}
