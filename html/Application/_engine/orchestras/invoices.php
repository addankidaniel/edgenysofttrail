<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraInvoices extends OrchestraEdepo
{
  public function __construct()
  {
        parent::__construct('\ClickBlocks\DB\Invoices');
    }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\Invoices';
   }

  public function getWidgetInvoiceData($type, array $params)
  {
        // Build WHERE query
        $w = ' WHERE i.clientID = ' . $params['clientID'];

        switch ($type) {
            case 'count':
                return $this->db->col('SELECT COUNT(*) FROM Invoices i ' . $w);
            case 'rows':
                switch (abs($params['sortBy'])) {
                    case 1:
                        $sortBy = 'i.ID';
                        break;
                    case 2:
                        $sortBy = 'total';
                        break;
                    case 3:
                        $sortBy = 'i.created';
                        break;
                }
                if ($params['sortBy'] > 0)
                    $sortBy .= ' DESC';
                else
                    $sortBy .= ' ASC';
                $groupBy = ' GROUP BY i.ID, i.clientID, i.created';
                $sortBy = ' ORDER BY ' . $sortBy;
                $limit = '';
                if ($params['pageSize'])
                    $limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
                $sql = 'SELECT i.ID, i.created, IFNULL(SUM(ic.price),0) as total
                   FROM Invoices i LEFT OUTER JOIN InvoiceCharges ic ON i.clientID = ic.clientID AND date_format(ic.created,\'%Y%m%d\') BETWEEN date_format(i.startDate,\'%Y%m%d\') AND date_format(i.endDate,\'%Y%m%d\') ' . $w . $groupBy. $sortBy . $limit;

                return $this->db->rows($sql);
        }
    }
    
    public function getLastInvoiceByClient($clientID){
        $sql = "SELECT IFNULL(MAX(ID),0) FROM Invoices WHERE clientID = ?";
        return $this->db->col($sql, array($clientID));
    }
}

?>