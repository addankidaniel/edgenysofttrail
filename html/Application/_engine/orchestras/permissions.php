<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraPermissions extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\Permissions');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\Permissions';
   }
}

?>