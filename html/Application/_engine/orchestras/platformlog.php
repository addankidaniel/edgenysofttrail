<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache,
	ClickBlocks\API\IEdepoBase;

class OrchestraPlatformLog extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\PlatformLog' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\PlatformLog';
	}

	public function getWidgetUsageData( $type, array $params )
	{
		$p = ['resellerID' => (int)$params['resellerID']];
		
		$query = 'FROM Clients c WHERE ';
		
		if ($params['typeID'] === IEdepoBase::CLIENT_TYPE_RESELLER) {
			$query .= 'c.resellerID=:resellerID AND c.deleted IS NULL';
		} else {
			$query .= 'c.typeID="'.IEdepoBase::CLIENT_TYPE_RESELLER.'" AND c.deleted IS NULL';
		}

		if( $params['active'] == '0' || $params['active'] == '1' ) {
			$isNull = ($params['active'] == '0') ? 'NOT' : '';
			$query .= " AND c.deactivated IS {$isNull} NULL";
		}
		if( $params['location'] != '' ) {
			$query .= ' AND c.location=:location';
			$p['location'] = $params['location'];
		}
		if( $type == 'count' ) {
			return (int)$this->db->execute( "SELECT c.ID {$query}", $p );	//return num rows, without limit or order
		}
		$sortDir = ($params['sortBy'] > 0) ? 'DESC' : 'ASC';
		switch( abs( $params['sortBy'] ) ) {
			case 1:
				$sortBy = "c.ID {$sortDir}";
				break;
			case 2:
				$sortBy = "c.name {$sortDir}, c.ID ASC";
				break;
			case 3:
				$sortBy = "c.location {$sortDir}, c.name ASC, c.ID ASC";
				break;
			case 4:
				$sortBy = "c.city {$sortDir}, c.name ASC, c.ID ASC";
				break;
			case 5:
				$sortBy = "c.state {$sortDir}, c.name ASC, c.ID ASC";
				break;
			case 6:
				$sortBy = "c.contactName {$sortDir}, c.name ASC, c.ID ASC";
				break;
			case 7:
				$sortBy = "c.deactivated {$sortDir}, c.name ASC, c.ID ASC";
				break;
		}
		$query .= ' ORDER BY ' . $sortBy;
		if( (int)$params['pageSize'] ) {
			$query .= ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
		}
		$sql = "SELECT c.ID,c.name,c.city,c.state,c.contactName,c.contactEmail,c.deactivated,c.location {$query}";
//		\ClickBlocks\Debug::ErrorLog( print_r( [$sql, $p], TRUE ) );
		return $this->db->rows( $sql, $p );
	}

	public function getUsageForClientByDates( $resellerID, $clientID, $startDate, $endDate )
	{
		$params = [
			'resellerID' => (int)$resellerID,
			'clientID' => (int)$clientID,
			'startDate' => date( 'Y-m-d 00:00:00', strtotime( $startDate ) ),
			'endDate' => date( 'Y-m-d 23:59:59', strtotime( $endDate ) )
		];
		$sql = 'SELECT pl.*, lpo.name as pricingOption, CONCAT(u.firstName," ",u.lastName) as userFullName, c.name as caseName, d.depositionOf as witness, d.volume
			FROM PlatformLog pl
			LEFT JOIN LookupPricingOptions lpo ON (lpo.ID=pl.optionID)
			LEFT JOIN Users u ON (u.ID=pl.userID)
			LEFT JOIN Cases c ON (c.ID=pl.caseID)
			LEFT JOIN Depositions d ON (d.ID=pl.depositionID)
			WHERE pl.resellerID=:resellerID AND pl.clientID=:clientID AND pl.created BETWEEN :startDate and :endDate
			ORDER BY pl.created,pl.ID';
//		\ClickBlocks\Debug::ErrorLog( print_r( ['OrchestraPlatformLog::getUsageForClientByDates', $sql, $params], TRUE ) );
		return $this->db->rows( $sql, $params );
	}
}