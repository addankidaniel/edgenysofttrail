<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupAuditActions extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupAuditActions');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupAuditActions';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupAuditActions`');
   }
}

?>