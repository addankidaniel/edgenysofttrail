<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupPricingTerms extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupPricingTerms');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupPricingTerms';
   }

	public static function getLookup()
	{
		return self::getDB()->couples( 'SELECT * FROM `LookupPricingTerms`' );
	}
}
