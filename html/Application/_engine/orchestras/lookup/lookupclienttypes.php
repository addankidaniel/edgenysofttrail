<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupClientTypes extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupClientTypes');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupClientTypes';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupClientTypes`');
   }
}

?>