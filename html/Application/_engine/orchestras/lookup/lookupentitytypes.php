<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupEntityTypes extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupEntityTypes');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupEntityTypes';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupEntityTypes`');
   }
}

?>