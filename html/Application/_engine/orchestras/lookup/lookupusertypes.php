<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupUserTypes extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupUserTypes');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupUserTypes';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupUserTypes`');
   }
}

?>