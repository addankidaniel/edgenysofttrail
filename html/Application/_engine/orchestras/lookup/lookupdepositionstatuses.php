<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupDepositionStatuses extends OrchestraEdepo
{
  public function __construct()
  {
    parent::__construct('\ClickBlocks\DB\LookupDepositionStatuses');
    }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupDepositionStatuses';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupDepositionStatuses`');
   }
}

?>