<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraLookupPricingOptions extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\LookupPricingOptions');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\LookupPricingOptions';
   }

  public static function getLookup()
  {
      return self::getDB()->couples('SELECT * FROM `LookupPricingOptions`');
   }
}

?>