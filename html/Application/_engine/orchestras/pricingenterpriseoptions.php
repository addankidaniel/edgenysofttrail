<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\API,
    ClickBlocks\Cache;

class OrchestraPricingEnterpriseOptions extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct('\ClickBlocks\DB\PricingEnterpriseOptions');
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\PricingEnterpriseOptions';
	}

	public function getByClient($clientID)
	{
		return $this->db->rows('SELECT resellerID,optionID,price,typeID FROM PricingEnterpriseOptions WHERE clientID=?', [$clientID]);
	}

	public function getByClientReseller( $clientID, $resellerID )
	{
		return $this->db->rows('SELECT optionID,price,typeID FROM PricingEnterpriseOptions WHERE clientID=? AND resellerID=?', [$clientID,$resellerID]);
	}

	public function getByClientAndOption($clientID, $optionID)
	{
		return $this->db->row('SELECT * FROM PricingEnterpriseOptions p JOIN LookupPricingOptions o ON o.ID=p.optionID WHERE p.clientID=? AND p.optionID=?', [$clientID,$optionID]);
	}

	public function getByClientResellerAndOption( $clientID, $resellerID, $optionID )
	{
		return $this->db->row('SELECT * FROM PricingEnterpriseOptions p JOIN LookupPricingOptions o ON o.ID=p.optionID WHERE p.clientID=? AND p.resellerID=? AND p.optionID=?', [$clientID,$resellerID,$optionID]);
	}

	public function deleteByClient($clientID)
	{
		$this->db->execute('DELETE FROM PricingEnterpriseOptions WHERE clientID=?', [$clientID]);
	}

	public function deleteByReseller( $resellerID )
	{
		$this->db->execute('DELETE FROM PricingEnterpriseOptions WHERE resellerID=?', [$resellerID]);
	}

	public function deleteByClientReseller( $clientID, $resellerID )
	{
		$this->db->execute('DELETE FROM PricingEnterpriseOptions WHERE clientID=? AND resellerID=?', [$clientID, $resellerID]);
	}

	public function getPriceByOption( $resellerID, $clientID, $optionID )
	{
		$resellerID = (int)$resellerID;
		$clientID = (int)$clientID;
		$optionID = (int)$optionID;
		if( !$resellerID || !$clientID || !$optionID ) {
			return FALSE;
		}
		$sql = 'SELECT price FROM PricingEnterpriseOptions WHERE resellerID=:resellerID AND clientID=:clientID AND optionID=:optionID';
		$params = ['resellerID'=>$resellerID, 'clientID'=>$clientID, 'optionID'=>$optionID];
//		\ClickBlocks\Debug::ErrorLog( print_r( [$sql, $params], TRUE ) );
		return $this->db->col( $sql, $params );
	}
}