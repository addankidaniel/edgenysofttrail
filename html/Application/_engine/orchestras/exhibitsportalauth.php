<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\ExhibitsPortalAuth;

class OrchestraExhibitsPortalAuth extends OrchestraEdepo {

	public function __construct() {
		parent::__construct( ExhibitsPortalAuth::BLL_CLASSNAME );
	}

	public static function getBLLClassName() {
		return ExhibitsPortalAuth::BLL_CLASSNAME;
	}

}
