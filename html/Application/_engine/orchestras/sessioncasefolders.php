<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\SessionCaseFolders;

class OrchestraSessionCaseFolders extends OrchestraEdepo {

	public function __construct() {
		parent::__construct( SessionCaseFolders::NSCLASS );
	}

	public static function getBLLClassName() {
		return SessionCaseFolders::NSCLASS;
	}

	public function getSessionsForFolder( $folderID ) {
		$sql = 'SELECT sessionID FROM SessionCaseFolders WHERE folderID=:folderID';
		return $this->db->cols( $sql, ['folderID'=>(int)$folderID] );
	}

	public function unlinkCaseFolders( $sessionID ) {
		return $this->db->execute( 'DELETE FROM SessionCaseFolders WHERE sessionID=:sessionID', ['sessionID' => (int)$sessionID] );
	}

	public function unlinkAllCaseFolders( $folderID ) {
		return $this->db->execute( 'DELETE FROM SessionCaseFolders WHERE folderID=:folderID', ['folderID' => (int)$folderID] );
	}

}
