<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraUserAuth extends OrchestraEdepo {

	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\UserAuth' );
	}

	public static function getBLLClassName() {
		return '\ClickBlocks\DB\UserAuth';
	}

	/* public static function getPasswordHash( $password ) {} */

	public function getIDByUserID( $userID ) {
		return (int)$this->db->col( 'SELECT ID FROM UserAuth WHERE userID=:userID AND expired IS NULL', [':userID'=>(int)$userID] );
	}

	public function comparePassword( $userID, $pwd ) {
		return (int)$this->db->col( 'SELECT COUNT(*) FROM UserAuth WHERE userID=:userID AND word=sha2(CONCAT(:word,spice), 512)', ['userID'=>(int)$userID, 'word'=>$pwd] );
	}
}
