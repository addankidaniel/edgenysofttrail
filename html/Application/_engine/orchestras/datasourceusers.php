<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraDatasourceUsers extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\DatasourceUsers' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\DatasourceUsers';
	}

	/**
	 *
	 * @param bigint $userID
	 * @param bigint $datasourceID
	 */
	public function getDatasourceUsers( $userID, $datasourceID )
	{
		$sql = 'SELECT * FROM DatasourceUsers WHERE userID=:userID AND datasourceID=:datasourceID';
		return $this->db->row( $sql, array( 'userID' => $userID, 'datasourceID' => $datasourceID ) );
	}
}
