<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\UserCustomSortFolders;


class OrchestraUserCustomSortFolders extends OrchestraEdepo {

	public function __construct() {
		parent::__construct( UserCustomSortFolders::NSCLASS );
	}

	public static function getBLLClassName() {
		return UserCustomSortFolders::NSCLASS;
	}

}
