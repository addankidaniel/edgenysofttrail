<?php                

namespace ClickBlocks\UnitTest\DB\OrchestraFiles;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\UnitTest\DB\OrchestraMethodTest;

/**
 * @group testMethod 
 */ 
class testMethodTest extends OrchestraMethodTest
{
   protected function getOrchestraClassName() {
      return '\ClickBlocks\DB\OrchestraFiles';
   }
   
   protected static $okParams = array('email'=>'test@example.com', 'password'=>'pass');


   public function setUp()
   {
     parent::setUp();
     
   }
   
   public function test_something()
   {
      $this->db->expects(self::once())->method('rows')->will(self::returnValue(array(
          array('one'=>'two'),
      )));
      $this->invokeThis(array('123','abc'));
      $this->assertEquals('two', $this->return[0]['one']);
   }

}

?>