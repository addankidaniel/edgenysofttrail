<?php

namespace ClickBlocks\MVCTest;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Exceptions,
    ClickBlocks\Utils\DT,
    ClickBlocks\Core;

if(!defined('PHPUnit_MAIN_METHOD')){
    define('PHPUnit_MAIN_METHOD', 'DoorTests::main');
}
require_once(__DIR__ . '/../../../../connect_phpunit.php');


class filesTests{
    public static function main() {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
    public static function suite(){
        $ts= new \PHPUnit_Framework_TestSuite('User Classes');
        $ts->addTestSuite('ClickBlocks\UnitTest\DB\OrchestraFiles\testMethodTest');
        return $ts;
    }
}
if(PHPUnit_MAIN_METHOD == 'AppTests::main')
    DoorTests::main();

?>