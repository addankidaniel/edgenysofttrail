<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Debug;

class OrchestraFiles extends OrchestraEdepo
{
  public function __construct()
  {
		parent::__construct('\ClickBlocks\DB\Files');
	}

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\Files';
   }

  public function testMethod($a, $b = null)
  {
		$this->fs->file_put_contents(Core\IO::dir('temp').'/test.file', 'testdata');
		return $this->db->rows('SELECT SOMETHING FROM SOMETHING');
	}

  public function getFilesByFolder($folderID, $createdBy = null, $sort = null, $pageSize = null, $page = null)
  {
     $sql = 'SELECT ID, name, created, isExhibit FROM Files WHERE folderID = ?';
     $bv = array($folderID);
     if ($createdBy) {
        $sql .= ' AND createdBy = ?';
        $bv[] = $createdBy;
     }
     if ($sort)
     {
        if (is_array($sort)) {
           $dir = ($sort['dir'] == 'DESC') ? 'DESC' : 'ASC';
           $sort = $sort['by'];
        } else $dir = 'ASC';
        if ($sort) $sql .= ' ORDER BY '.$sort.' '.$dir;
     }
     if ($pageSize) $sql .= ' LIMIT '.((int)$pageSize * (int)$page).','.(int)$pageSize;
	  return $this->db->rows($sql, $bv);
	}

	public function getFilesWithSortPos( $folderID, $userID, $csUserID=NULL ) {
		if( !$csUserID ) {
			$csUserID = (int)$userID;
		}
		$bind  = ['folderID' => (int)$folderID, 'csUserID' => (int)$csUserID, 'WP' => Edepo::DEPOSITION_CLASS_WITNESSPREP, 'WPDemo' => Edepo::DEPOSITION_CLASS_WPDEMO];
		$sql = '(SELECT fi.ID, fi.name, fi.folderID, fl.ID as caseFolderID, fi.created, fi.isExhibit, IFNULL(cs.sortPos,fi.ID) as sortPos FROM Files fi
				INNER JOIN Folders fl ON (fl.ID=fi.folderID)
				LEFT JOIN Depositions d ON (d.ID=fl.depositionID)
				LEFT JOIN UserCustomSortFiles cs ON (cs.fileID=fi.ID AND cs.userID=:csUserID)
				WHERE d.deleted IS NULL AND fl.ID=:folderID)
				UNION
				(SELECT fi.ID, fi.name, fi.folderID, fl.ID as caseFolderID, fi.created, fi.isExhibit, IFNULL(cs.sortPos,fi.ID) as sortPos FROM Folders fl
				INNER JOIN Cases ca ON (ca.ID=fl.caseID AND (fl.class="' . self::FOLDERS_EXHIBIT . '" OR fl.class="' . self::FOLDERS_TRANSCRIPT . '"))
				INNER JOIN Depositions d ON (d.caseID=ca.ID  AND d.class != :WP AND d.class != :WPDemo)
				INNER JOIN Folders cf ON (cf.depositionID=d.ID AND cf.class=fl.class)
				INNER JOIN Files fi ON (fi.folderID=cf.ID)
				LEFT JOIN UserCustomSortFiles cs ON (cs.fileID=fi.ID AND cs.userID=:csUserID)
				WHERE d.deleted IS NULL AND fl.ID=:folderID)';
		// Debug::ErrorLog( print_r( ['sql'=>$sql, 'bind' => $bind], TRUE ) );
		return $this->db->rows( $sql, $bind );
	}

  public function getFileNameMapByFolder($folderID)
  {
		return $this->db->couples('SELECT name,ID FROM Files WHERE folderID = ?',array($folderID));
	}

  public function getFilesByUserID($folderID, $userID = 0)
  {
		return $this->db->rows('SELECT * FROM Files WHERE folderID = ? AND ( createdBy = ? OR isExhibit = 1 )',array($folderID, $userID));
	}

  public function checkUserFileID($userID, $fileID)
  {
    return $this->db->rows('SELECT ID FROM Files WHERE createdBy = ? AND ID = ?',array($userID, $fileID)) ;
	}

  public function getTranscriptFile($depoID)
  {
		 return $this->db->row('SELECT fi.* FROM `Files` fi INNER JOIN `Folders` fo ON fi.folderID = fo.`ID` WHERE fi.isTranscript=1 AND fo.depositionID=? LIMIT 1',array($depoID));
     /*$file = new Files;
     $file->assign($row);
     return $file;*/
	}

  public function getFilesByName($depoID, $name, $isExhibit = 0)
  {
	  $where = ' WHERE fo.`depositionID` = ' . (int)$depoID . ' AND fi.`name` like ' . $this->db->quote('%' . $name . '%');
    if($isExhibit > 0)
		  $where .= " AND fo.class = 'Exhibit' ";
		return $this->db->rows('SELECT fi.ID, fi.name FROM `Files` fi INNER JOIN `Folders` fo ON fi.folderID = fo.`ID` ' . $where);
	}

	public function getFileIDByName( $depoID, $name )
	{
		$sql = 'SELECT fi.ID FROM Files fi INNER JOIN Folders fo ON fo.ID = fi.folderID WHERE fo.depositionID = :depoID AND fi.name = :fileName';
		return $this->db->col( $sql, array( 'depoID'=>(int)$depoID, 'fileName'=>$name ) );
	}

	public function getFolderFileIDByName( $folderID, $fileName )
	{
		$sql = 'SELECT fi.ID FROM Files fi WHERE fi.folderID = :folderID AND fi.name = :fileName';
		return $this->db->col( $sql, array( 'folderID'=>(int)$folderID, 'fileName'=>$fileName ) );
	}

  public function checkUniqueFile($folderID, $fileName)
  {
        return !($this->db->col('SELECT COUNT(*) FROM Files WHERE folderID = ? AND name = ?', array($folderID, $fileName)) > 0);
    }

	public function getSourceFileID( $fileID )
	{
		$lastFileID = NULL;
		$fileID = (int)$fileID;
		$i = 0;
		$maxLoops = 3;	//prevent infinite loops
		while( $i < $maxLoops && $fileID && $fileID !== $lastFileID ) {
			++$i;
			$lastFileID = $fileID;
			$sql = 'SELECT sourceID FROM Files WHERE ID=:fileID';
			//\ClickBlocks\Debug::ErrorLog( "{$sql} :: {$fileID}" );
			$fileID = $this->db->col( $sql, array( 'fileID'=>$fileID ) );
			//\ClickBlocks\Debug::ErrorLog( "result: {$fileID}" );
		}
		//\ClickBlocks\Debug::ErrorLog( "getSourceFileID :: {$lastFileID}" );
		return $lastFileID;
	}

	public function getSortPosition( $fileID, $userID )
	{
		$fileID = (int)$fileID;
		$sql = "SELECT IFNULL(sortPos,{$fileID}) as sortPos FROM UserCustomSortFiles WHERE fileID=:fileID AND userID=:userID";
		return $this->db->col( $sql, ['fileID'=>$fileID, 'userID'=>$userID] );
	}

	public function getContentSearchResults( array $_fileIDs ) {
		// sanitize fileIDs
		$fileIDs = [];
		foreach( $_fileIDs as $_fileID ) {
			$fileID = (int)$_fileID;
			if( $fileID && $fileID > 0 ) {
				$fileIDs[] = $fileID;
			}
		}
		if( !$fileIDs ) {
			return FALSE;
		}
		$sql_fileIDs = implode( ',', $fileIDs );
		$sql = "SELECT fi.ID,fi.name,fi.filesize,fl.name as folderName,d.depositionOf as sessionName,
			CONCAT(ROUND(fi.filesize/POW(1024,IFNULL(TRUNCATE(LOG(1024,fi.filesize),0),1)),1),ELT(IFNULL(TRUNCATE(LOG(1024,fi.filesize),0),1) + 1,'B','K','M')) as hrFileSize
			FROM Files fi
			INNER JOIN Folders fl ON (fl.ID=fi.folderID)
			LEFT JOIN Depositions d ON (d.ID=fl.depositionID)
			WHERE fi.ID IN ({$sql_fileIDs})";
		//Debug::ErrorLog( ['OrchestraFiles::getContentSearchResults', $sql] );
		$rows = $this->db->rows( $sql );
		if( $rows ) {
			$indexed = [];
			foreach( $rows as $row ) {
				$indexed[$row['ID']] = $row;
			}
			return $indexed;
		}
		return $rows;
	}
}
