<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraFileShares extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\FileShares');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\FileShares';
   }

  public function checkAttendeeAccessToFile($attendeeID, $fileID)
  {
      return (bool)$this->db->col('SELECT ID FROM FileShares WHERE attendeeID=? AND fileID=? LIMIT 1', array($attendeeID,$fileID));
   }

	public function getFileShareIDForFile( $fileID, $userID, $isCopy=FALSE, $isGuest=FALSE )
	{
		$fileCol = ($isCopy) ? 'copyFileID' : 'fileID';
		$userCol = ($isGuest) ? 'attendeeID' : 'userID';
		$sql = "SELECT ID FROM FileShares WHERE {$fileCol} = :fileID AND {$userCol} = :userID";
		return $this->db->col( $sql, array( 'fileID'=>(int)$fileID, 'userID'=>(int)$userID ) );
	}
}

?>