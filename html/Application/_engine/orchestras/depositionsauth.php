<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\Cache;

class OrchestraDepositionsAuth extends OrchestraEdepo
{

	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\DepositionsAuth' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\DepositionsAuth';
	}

	public static function generateSpice() {
		return BLLTable::generateSpice();
	}

	public static function getPasswordHash( $password )
	{
		return md5( $password . (Core\Register::getInstance()->config->secretSalt) );
	}
}