<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraRolePermissions extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\RolePermissions');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\RolePermissions';
   }
}

?>