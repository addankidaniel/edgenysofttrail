<?php

namespace ClickBlocks\DB;


class OrchestraUserCustomSortDepositions extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\UserCustomSortDepositions' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\UserCustomSortDepositions';
	}
}
