<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraEnterpriseAgreements extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct('\ClickBlocks\DB\EnterpriseAgreements');
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\EnterpriseAgreements';
	}

	public function checkHasAgreement($rid, $cid)
	{
		$query = 'SELECT COUNT(*) FROM EnterpriseAgreements WHERE deleted IS NULL AND enterpriseResellerID=:resellerID AND enterpriseClientID=:clientID';
		return (bool)$this->db->col( $query, ['clientID'=>$cid, 'resellerID'=>$rid] );
	}

	/**
	 * Reseller and Client have an active agreement
	 * @param bigint $resellerID
	 * @param bigint $clientID
	 * @return boolean
	 */
	public function hasActiveAgreement( $resellerID, $clientID )
	{
		$resellerID = (int)$resellerID;
		$clientID = (int)$clientID;
		if( !$resellerID || !$clientID ) {
			return FALSE;
		}
		$sql = 'SELECT COUNT(ID) FROM EnterpriseAgreements WHERE enterpriseResellerID=:resellerID AND enterpriseClientID=:clientID AND isInactive=0 AND isDisabled=0 AND accepted IS NOT NULL AND deleted IS NULL';
		return (bool)$this->db->col( $sql, ['resellerID'=>$resellerID, 'clientID'=>$clientID] );
	}

	public function getUnacceptedAgreementsByResellerID( $rid )
	{
		$query = 'SELECT * FROM EnterpriseAgreements WHERE enterpriseResellerID=:resellerID AND accepted IS NULL AND deleted IS NULL';
		return $this->db->rows( $query, [':resellerID' => $rid] );
	}

	public function getAcceptedAgreementsByResellerID( $rid )
	{
		$query = 'SELECT * FROM EnterpriseAgreements WHERE enterpriseResellerID=:resellerID AND accepted IS NOT NULL AND deleted IS NULL';
		return $this->db->rows( $query, [':resellerID' => $rid] );
	}

	public function getAgreementsByClientID( $cid )
	{
		$query = 'SELECT * FROM EnterpriseAgreements WHERE enterpriseClientID=:clientID AND deleted IS NULL AND isInactive=0';
		return $this->db->rows( $query, [':clientID' => $cid] );
	}

	public function getEnterpriseResellersByClientID( $cid )
	{
		$query = 'SELECT r.* FROM Clients r'
			. ' JOIN EnterpriseAgreements a ON a.enterpriseResellerID=r.ID'
			. ' WHERE r.typeID="R" AND a.enterpriseClientID=:clientID AND a.deleted IS NULL AND a.isDisabled=0 AND a.isInactive=0';
		return $this->db->rows( $query, [':clientID' => $cid] );
	}

	public function getAvailableEnterpriseResellersByClientID( $cid )
	{
		$query = 'SELECT r.* FROM Clients r
			INNER JOIN Clients ec ON (ec.ID=:clientID)
			WHERE r.typeID="R"
			AND r.ID NOT IN (
				SELECT dr.ID FROM Clients dr
				LEFT JOIN EnterpriseAgreements a ON a.enterpriseResellerID=dr.ID
				WHERE dr.typeID="R" AND a.enterpriseClientID=:clientID AND a.deleted IS NULL
			)
			AND ec.resellerID != r.ID
			AND r.resellerLevel != "9:Demo"
			ORDER BY r.resellerLevel, r.name';
		return $this->db->rows( $query, [':clientID' => $cid] );
	}

	public function getAcceptedEnterpriseResellersByClientID( $cid )
	{
		$query = 'SELECT r.* FROM Clients r
			LEFT JOIN EnterpriseAgreements a ON (a.enterpriseResellerID=r.ID AND a.enterpriseClientID=:clientID)
			LEFT JOIN Clients ec ON (ec.resellerID=r.ID AND ec.ID=:clientID)
			WHERE r.typeID="'.self::CLIENT_TYPE_RESELLER.'" AND r.resellerClass="'.self::RESELLERCLASS_SCHEDULING.'"
			AND ((a.isInactive=0 AND a.isDisabled=0 AND a.accepted IS NOT NULL AND a.deleted IS NULL) OR (r.ID=ec.resellerID))
			ORDER BY -ec.resellerID DESC, r.resellerLevel ASC, r.name ASC';
		$params = ['clientID'=>$cid];
//		\ClickBlocks\Debug::ErrorLog( print_r( [$query,$params], TRUE ) );
		return $this->db->rows( $query, $params );
	}

	public function getAgreementByResellerIDClientID( $rid, $cid )
	{
		$query = 'SELECT * FROM EnterpriseAgreements'
			. ' WHERE deleted IS NULL AND enterpriseResellerID=:resellerID AND enterpriseClientID=:clientID';
		return $this->db->row( $query, [
			':clientID' => $cid,
			':resellerID' => $rid,
		] );
	}

	public function countAgreementByResellerIDClientID( $rid, $cid )
	{
		$query = 'SELECT COUNT(*) FROM EnterpriseAgreements'
			. ' WHERE deleted IS NULL AND enterpriseResellerID=:resellerID AND enterpriseClientID=:clientID';
		return $this->db->row( $query, [
			':clientID' => $cid,
			':resellerID' => $rid,
		] );
	}

	public function widgetResellerAgreements( $type, array $params )
	{
		$where = ' WHERE r.typeID="R" AND a.deleted IS NULL AND a.isInactive=0';

		if ($params['clientID'])
		{
			$where .= ' AND a.enterpriseClientID='.$this->db->quote( $params['clientID'] );
		}

		if ($params['searchValue'])
		{
			$searchValue = $this->db->quote('%'.$params['searchValue'].'%');
			$where .= " AND (r.name LIKE $searchValue OR r.contactName LIKE $searchValue)";
		}

		if ($type == 'count')
		{
			return $this->db->col( "SELECT COUNT(a.ID) FROM EnterpriseAgreements a JOIN Clients r ON r.ID=a.enterpriseResellerID $where" );
		}

		if ($type == 'rows')
		{
			switch (abs( $params['sortBy'] ))
			{
				case 1:
					$sortBy = 'r.ID';
					break;
				case 2:
					$sortBy = 'r.name';
					break;
				case 3:
					$sortBy = 'r.contactName';
					break;
				case 4:
					$sortBy = 'a.created';
					break;
				case 5:
					$sortBy = 'IF(a.accepted IS NULL, -1, a.isDisabled)';
					break;
			}

			$sortBy = " ORDER BY $sortBy " . ($params['sortBy'] > 0 ? 'DESC' : 'ASC');

			$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];

			$sql = 'SELECT r.ID, r.name, r.contactName, a.created, IF(a.accepted IS NULL, "Pending", IF(a.isDisabled=0, "Enabled", "Disabled")) as `status`, a.isDisabled, a.ID as `agreementID`'
				. ' FROM EnterpriseAgreements a JOIN Clients r ON r.ID=a.enterpriseResellerID'
				. $where . $sortBy . $limit;

			return $this->db->rows($sql);
		}
	}

	public function widgetClientAgreements( $type, array $params )
	{
		$from = ' FROM EnterpriseAgreements a JOIN Clients c ON c.ID=a.enterpriseClientID';
		$where = ' WHERE c.typeID="EC" AND a.deleted IS NULL';

		if ($params['resellerID'])
		{
			$where .= ' AND a.enterpriseResellerID='.$this->db->quote( $params['resellerID'] );
		}

		if ($params['searchValue'])
		{
			$searchValue = $this->db->quote('%'.$params['searchValue'].'%');
			$where .= " AND (c.name LIKE $searchValue OR c.contactName LIKE $searchValue)";
		}

		if ($type == 'count')
		{
			return $this->db->col( "SELECT COUNT(a.ID) $from $where" );
		}

		if ($type == 'rows')
		{
			switch (abs( $params['sortBy'] ))
			{
				case 1:
					$sortBy = 'c.ID';
					break;
				case 2:
					$sortBy = 'c.name';
					break;
				case 3:
					$sortBy = 'c.contactName';
					break;
				case 4:
					$sortBy = 'a.created';
					break;
				case 5:
					$sortBy = 'IF(a.accepted IS NULL, -1, a.isInactive)';
					break;
			}

			$sortBy = " ORDER BY $sortBy " . ($params['sortBy'] > 0 ? 'DESC' : 'ASC');

			$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];

			$sql = 'SELECT c.ID, c.name, c.contactName, a.created, IF(a.accepted IS NULL, "Pending", IF(a.isInactive=0, "Active", "Inactive")) as `status`, a.isInactive, a.ID as `agreementID`'
				. $from	. $where . $sortBy . $limit;

			return $this->db->rows($sql);
		}
	}
}
