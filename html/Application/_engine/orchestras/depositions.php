<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\Cache,
	ClickBlocks\API,
	ClickBlocks\API\IEdepoBase;

class OrchestraDepositions extends OrchestraEdepo
{

	public function __construct()
	{
		parent::__construct('\ClickBlocks\DB\Depositions');
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\Depositions';
	}

	public function checkDepositionUKey( $uKey, $checkDeleted=FALSE ) {
		if( $checkDeleted ) {
			$sql = 'SELECT d.ID FROM Depositions d INNER JOIN Cases c ON (c.ID=d.caseID AND c.deleted IS NULL) WHERE d.uKey=:uKey AND d.deleted IS NULL';
		} else {
			$sql = 'SELECT ID FROM Depositions WHERE uKey=:uKey';
		}
		return $this->db->col( $sql, ['uKey'=>$uKey] );
	}

	public function checkDepositionID($ID)
	{
		return $this->db->col('SELECT ID FROM Depositions WHERE ID = ?', array($ID));
	}

	public function getWidgetDepositionData( $type, array $params )
	{
		$f = 'FROM Depositions d';
		$w = 'WHERE d.caseID = ' . (int)$params['caseID'] . ' AND d.deleted IS NULL';
		$bv = [];

		if( $params['userID'] && !$params['isDemoCase']) {
			$f .= ' INNER JOIN Users u ON (u.ID=:userID) '
				. 'LEFT JOIN DepositionAssistants ds ON (ds.depositionID=d.ID AND ds.userID=u.ID) '
				. 'LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND da.userID=u.ID) ';
			$w .= ' AND (d.ownerID=u.ID OR ds.userID=u.ID OR da.userID=u.ID)';
			$bv['userID'] = (int)$params['userID'];
		}
		$f .= ' LEFT JOIN Users us ON (us.ID=d.ownerID) ';

		if( $params['statusID'] ) {
			$w .= ' AND d.statusID=:statusID';
			$bv['statusID'] = $params['statusID'];
		}

		if( $type === 'count' ) {
			return $this->db->col( "SELECT COUNT(d.ID) {$f} {$w}", $bv );
		}

		//		switch( abs( $params['sortBy'] ) ):
		//			case 1:
		//				$sortBy = 'd.ID';
		//				break;
		//			case 2:
		//				$sortBy = 'd.openDateTime';
		//				break;
		//			case 3:
		//				$sortBy = '';
		//				break;
		//			case 4:
		//				$sortBy = '';
		//				break;
		//			case 5:
		//				$sortBy = '';
		//				break;
		//			case 6:
		//				$sortBy = 'd.location';
		//				break;
		//			case 7:
		//				$sortBy = 'owner';
		//				break;
		//			case 8:
		//				$sortBy = '';
		//				break;
		//		endswitch;
		//
		//		if ($sortBy) {
		//			$sortBy =  "ORDER BY {$sortBy} " . ($params['sortBy'] > 0 ? 'DESC' : 'ASC') . ', ID DESC';
		//		}

		//		$limit = 'LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];

		//		$sql = "SELECT d.* {$f} {$w} {$sortBy} {$limit}";
		//		$sql = "SELECT d.*, concat(us.firstName, ' ', us.lastName) as owner {$f} {$w} {$sortBy}";
		$sql = "SELECT d.*, concat(us.firstName, ' ', us.lastName) as owner {$f} {$w}";
		//\ClickBlocks\Debug::ErrorLog( print_r( [$sql, $bv], TRUE ) );
		return $this->db->rows( $sql, $bv );
	}

	public function getWidgetCourtDepositionData( $type, array $params )
	{
		$f = 'FROM Depositions d';
		$w = 'WHERE d.caseID = ' . (int)$params['caseID'] . ' AND d.deleted IS NULL';
		$bv = [];

		if( $params['userID'] ) {
			$f .= ' INNER JOIN Users u ON (u.ID=:userID) '
				. 'LEFT JOIN DepositionAssistants ds ON (ds.depositionID=d.ID AND ds.userID=u.ID) '
				. 'LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND da.userID=u.ID) ';
			$w .= ' AND (d.ownerID=u.ID OR ds.userID=u.ID OR da.userID=u.ID OR u.typeID=:clientAdmin OR u.clientAdmin=1)';
			$bv['userID'] = (int)$params['userID'];
			$bv['clientAdmin'] = IEdepoBase::USER_TYPE_CLIENT;
		}

		if( $params['statusID'] ) {
			$w .= ' AND d.statusID=:statusID';
			$bv['statusID'] = $params['statusID'];
		}

		if( $type === 'count' ) {
			return $this->db->col( "SELECT COUNT(d.ID) {$f} {$w}", $bv );
		}

		//		switch( abs( $params['sortBy'] ) ):
		//			case 1:
		//				$sortBy = 'd.ID';
		//				break;
		//			case 2:
		//				$sortBy = 'd.openDateTime';
		//				break;
		//			case 3:
		//				$sortBy = 'd.depositionOf';
		//				break;
		//			case 4:
		//				$sortBy = 'd.volume';
		//				break;
		//			case 5:
		//				$sortBy = '';
		//				break;
		//			case 6:
		//				$sortBy = 'd.location';
		//				break;
		//			case 7:
		//				$sortBy = '';
		//				break;
		//			case 8:
		//				$sortBy = '';
		//				break;
		//		endswitch;
		//		
		//		if ($sortBy) {
		//			$sortBy =  "ORDER BY {$sortBy} " . ($params['sortBy'] > 0 ? 'DESC' : 'ASC') . ', ID DESC';
		//		}

		//		$limit = 'LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];

		//		$sql = "SELECT d.* {$f} {$w} {$sortBy} {$limit}";
		$sql = "SELECT d.* {$f} {$w}";
		//		$sql = "SELECT d.* {$f} {$w} {$sortBy}";
		//\ClickBlocks\Debug::ErrorLog( print_r( ['getWidgetCourtDepositionData',$sql, $bv], TRUE ) );
		return $this->db->rows( $sql, $bv );
	}

	public function getAllByCase($caseID)
	{
		$depos = $this->db->rows('SELECT * FROM Depositions WHERE caseID=? AND deleted IS NULL', array($caseID));

		// We don't want the password hashes coming out of here. Remove them.
		foreach($depos as $key=>$depo){
			unset( $depos[$key]['password'], $depos[$key]['courtReporterPassword'] );
		}
		return $depos;
	}

	/**
	 * Returns all depositions, where user has access (one of following):
	 * - is creator
	 * - is owner
	 * - is deposition assistant
	 * @param type $caseID
	 * @param type $userID
	 * @return type
	 */
	public function getTrustedDepositions($caseID, $userID)
	{
		return $this->db->rows('SELECT d.* FROM Depositions d LEFT JOIN DepositionAssistants da ON (da.depositionID = d.ID AND da.userID = :userID)
			WHERE d.caseID = :caseID AND d.deleted IS NULL AND (d.createdBy=:userID OR d.ownerID=:userID OR da.userID IS NOT NULL)', array('userID' => $userID, 'caseID' => $caseID));
	}

	public function getDepositionForUserByID( $userID, $depoID )
	{
		$sql = 'SELECT d.* FROM Depositions d
			LEFT JOIN CaseManagers cm ON (cm.caseID = d.caseID AND cm.userID = :userID)
			LEFT JOIN DepositionAssistants da ON (da.depositionID = d.ID AND da.userID = :userID)
			LEFT JOIN DepositionAttendees att ON (att.depositionID = d.ID AND att.userID = :userID)
			WHERE d.ID = :depoID
			AND d.deleted IS NULL
			AND (d.createdBy = :userID OR d.ownerID = :userID OR da.userID = :userID OR att.userID = :userID)';
		return $this->db->row( $sql, ['depoID'=>$depoID, 'userID'=>$userID] );
	}

	public function getByAttendee( $caseID, $userID=NULL )
	{
		if( (int)$userID ) {
			$sql = 'SELECT d.* FROM Depositions d
				LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND da.userID=:userID)
				LEFT JOIN CaseManagers cm ON (cm.caseID=d.caseID AND cm.userID=:userID)
				WHERE d.caseID=:caseID
				AND d.deleted IS NULL
				AND (da.ID IS NOT NULL OR cm.userID IS NOT NULL)';
			return $this->db->rows( $sql, ['userID'=>$userID, 'caseID'=>$caseID] );
		} else {
			return $this->db->rows( 'SELECT * FROM Depositions WHERE caseID=? AND deleted IS NULL', [$caseID] );
		}
	}

	/**
	 * @param int $userID
	 * @param bool $isPast
	 * @param string $date in format Y-m-d strictly!
	 * @return array list of depos
	 */
	public function getAPIUserDepositions( $userID, $date=NULL, $onlyScheduled=FALSE )
	{
		$date = $date ? (" AND d.finished BETWEEN '$date' AND DATE_ADD( '$date', INTERVAL 1 DAY)") : '';
		$w = 'd.deleted IS NULL
			AND ca.deleted IS NULL
			AND (((da.ID IS NOT NULL AND da.banned IS NULL)
			OR das.userID=u.ID
			OR cm.userID=u.ID
			OR d.createdBy=u.ID
			OR d.ownerID=u.ID)
			OR ca.class="Demo")
			AND ((d.class != :trialBinder) OR (d.class = :trialBinder AND (cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID)))';
		if( $onlyScheduled ) {
			$w .= ' AND d.started IS NULL';
		}
		$sql = 'SELECT d.*,da.banned, IFNULL(cs.sortPos,d.ID) as sortPos FROM Depositions d
			INNER JOIN Cases ca ON (ca.ID=d.caseID)
			INNER JOIN Users u ON (u.ID=:userID AND u.clientID=ca.clientID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=d.caseID AND cm.userID=:userID)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=:userID)
			LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND da.userID=:userID)
			LEFT JOIN UserCustomSortDepositions cs ON (cs.depositionID=d.ID AND cs.userID=u.ID)
			WHERE ' . $w . $date . ' ORDER BY ca.name,d.title';
		return $this->db->rows( $sql, ['userID' => $userID, 'trialBinder' => self::DEPOSITION_CLASS_TRIALBINDER] );
		// AND  ($isPast?'':'NOT ').' d.statusID=\'F\' '.
	}

	public function getOwnerID($depoID)
	{
		return $this->db->col('SELECT d.ownerID FROM Depositions d WHERE d.ID = ?', array($depoID));
	}

	public function getCreatorID($depoID)
	{
		return $this->db->col('SELECT d.createdBy FROM Depositions d WHERE d.ID = ?', array($depoID));
	}

	/**
	 *
	 * @param bigint $ownerID
	 * @return array rows
	 */
	public function getActiveByOwner($ownerID)
	{
		return $this->db->rows( 'SELECT d.*,c.name FROM Depositions d INNER JOIN Cases c ON c.ID=d.caseID WHERE d.statusID = "I" AND d.ownerID = ? AND d.deleted IS NULL AND d.parentID IS NULL AND c.deleted IS NULL', array($ownerID));
	}

	public function getChildDepositionForClient($parentDepositionID, $clientID)
	{
		return $this->db->row('SELECT d.* FROM Depositions AS d INNER JOIN Cases AS c ON (c.ID=d.caseID) WHERE c.deleted IS NULL AND d.deleted IS NULL AND d.parentID=:depoID AND c.clientID=:clientID', ['depoID'=>(int)$parentDepositionID, 'clientID'=>(int)$clientID] );
	}

	public function getChildDepositionIDs( $depoID )
	{
		$sql = 'SELECT ID FROM Depositions WHERE parentID = :depoID';
		return $this->db->cols( $sql, array( 'depoID'=>(int)$depoID ) );
	}

	/**
	 * Returns map clientID=>depoID for all depos linked to $depoID (including $depoID itself)
	 * @param type $depoID
	 * @return type
	 */
	public function getClient2DepoMapForLinkedTo($depoID)
	{
		return $this->db->couples('SELECT ca.clientID, d.ID FROM Depositions d INNER JOIN Cases ca ON (ca.ID=d.caseID) WHERE d.ID=:depoID OR d.parentID=:depoID',array(':depoID'=>$depoID));
	}

	/**
	 * Returns clientID for a deposition/session
	 * @param type $depoID
	 * @return type
	 */
	public function getSessionClientID( $depoID )
	{
		$sql = 'SELECT ca.clientID FROM Depositions d
			LEFT JOIN Cases ca ON (ca.ID=d.caseID)
			WHERE d.ID=:depoID';
		return $this->db->col($sql, array( ':depoID'=>$depoID ) );
	}

	public function getDefaultAttendeeLimit($depoID)
	{
		return $this->db->col('SELECT cl.includedAttendees FROM Depositions d INNER JOIN Cases c ON c.ID=d.caseID INNER JOIN Clients cl ON cl.ID=c.clientID WHERE d.ID=?',array($depoID));
	}

	/**
	 * @param int|array $caseID caseID or array of caseID's
	 * @return int
	 */
	public function getCountByCases($caseID)
	{
		$caseID = (array)$caseID;
		if (count($caseID)==0) return 0;
		foreach ($caseID as $k=>$v) $caseID[$k] = (int)$v;
		return (int)$this->db->col('SELECT COUNT(*) FROM Depositions WHERE caseID IN ('.implode(',', $caseID).')');
	}

	public function finishByParentID($parentID)
	{
		return $this->db->execute('UPDATE Depositions SET finished=NOW(), statusID=\'F\' WHERE parentID=?',array($parentID));
	}

	public function checkUserAccessToDeposition( $userID, $depoID ) {
		$isAttendee = ($userID == $this->db->col( 'SELECT userID FROM DepositionAttendees WHERE userID=:userID AND depositionID=:depoID', ['userID'=>(int)$userID, 'depoID'=>(int)$depoID] ));
		if( $isAttendee ) {
			return TRUE;
		}
		return $this->isTrustedUser( $userID, $depoID );
	}

	public function isTrustedUser( $userID, $depoID ) {
		$sql = "SELECT COUNT(d.ID) FROM Depositions d
			INNER JOIN Cases c ON (c.ID=d.caseID AND c.deleted IS NULL)
			INNER JOIN Users u ON (u.ID=:userID AND u.clientID=c.clientID)
			LEFT JOIN DepositionAssistants da ON (da.depositionID=d.ID AND da.userID=u.ID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			WHERE d.deleted IS NULL AND d.ID=:depoID
			AND (d.createdBy=u.ID OR d.ownerID=u.ID OR da.depositionID=d.ID OR cm.caseID=c.ID OR c.class='Demo' OR ((u.typeID='C' OR u.clientAdmin=1) AND u.clientID=c.clientID))";
		//\ClickBlocks\Debug::ErrorLog( print_r( ['OrchestraDepositions::isTrustedUser', $sql, $userID, $depoID], TRUE) );
		return (bool)$this->db->col( $sql, ['userID'=>(int)$userID, 'depoID'=>(int)$depoID] );
	}

	/**
	 * Select ALL users that are either Case Manager, Depo Assistant OR Client Admin related to this deposition
	 * @param bigint $depoID
	 * @return array userID=>email
	 */
	public function getAllTrustedUsersAsMap( $depoID ) {
		$depoID = (int)$depoID;
		$sql = 'SELECT u.ID,u.email FROM Users u
			INNER JOIN Depositions d ON (d.ID=:depoID)
			INNER JOIN Cases ca ON (ca.ID=d.caseID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=d.caseID AND cm.userID=u.ID)
			LEFT JOIN DepositionAssistants da ON (da.depositionID=d.ID AND da.userID=u.ID)
			WHERE ca.clientID=u.clientID AND u.deleted IS NULL
			AND (ca.class="Demo" OR d.ownerID=u.ID OR da.userID=u.ID OR cm.userID=u.ID OR u.typeID=:clientAdmin OR u.clientAdmin=1)';
		return $this->db->couples( $sql, ['depoID'=>$depoID, 'clientAdmin'=>IEdepoBase::USER_TYPE_CLIENT] );
	}

	/**
	 *
	 * @param type $caseID
	 * @param type $ownerID
	 * @return int number of depositions, owned by given $ownerID in given $caseID
	 */
	public function checkOwnedByCase($caseID, $ownerID)
	{
		return (int)$this->db->col('SELECT COUNT(*) FROM Depositions WHERE caseID=? AND ownerID=?',array($caseID,$ownerID));
	}

	/**
	 *
	 * @param bigint $depoID Deposition ID
	 * @param bigint $userID User ID
	 * @return int number of exhibits receieved for given $userID for given $depoID
	 */
	public function getExhibitsReceivedCount( $depoID, $userID ) {
		$sql = "SELECT COUNT(fi.ID) FROM Folders fl INNER JOIN Files fi ON fi.folderID = fl.ID WHERE depositionID = :depoID AND (fl.class = 'Exhibit' OR fi.isExhibit) AND fi.sourceUserID != :userID";
		return (int)$this->db->col( $sql, array( 'depoID' => $depoID, 'userID' => $userID ) );
	}

	/**
	 *
	 * @param bigint $depoID Deposition ID
	 * @param bigint $userID User ID
	 * @return int number of exhibits introduced for given $userID for given $depoID
	 */
	public function getExhibitsIntroducedCount( $depoID, $userID ) {
		$sql = "SELECT COUNT(fi.ID) FROM Folders fl INNER JOIN Files fi ON fi.folderID = fl.ID WHERE depositionID = :depoID AND (fl.class = 'Exhibit' OR fi.isExhibit) AND fi.sourceUserID = :userID";
		return (int)$this->db->col( $sql, array( 'depoID' => $depoID, 'userID' => $userID ) );
	}

	public function setSpeakerIDForChildDepositions( $depoID, $speakerID )
	{
		$sql = 'UPDATE Depositions set speakerID = :speakerID WHERE parentID = :depoID';
		$this->db->execute( $sql, array( 'speakerID' => (int)$speakerID, 'depoID' => (int)$depoID ) );
	}

	public function getUserChildDeposition( $userID, $depoID )
	{
		$depoID = (int)$depoID;	//never allow null
		$sql = 'SELECT d.ID FROM Depositions d INNER JOIN DepositionAttendees da ON (da.depositionID=d.ID) WHERE d.parentID IS NOT NULL AND d.parentID=:depoID AND da.userID=:userID';
		return (int)$this->db->col( $sql, ['depoID'=>$depoID, 'userID'=>$userID] );
	}

	public function getEnterpriseResellerID( $depoID )
	{
		$sql = "SELECT enterpriseResellerID FROM Depositions WHERE ID=:depoID";
		return $this->db->col( $sql, ['depoID'=>$depoID] );
	}

	public function getDepositionsByCreator( $userID )
	{
		return $this->db->cols( 'SELECT `ID` FROM `Depositions` WHERE `createdBy`=?', [$userID] );
	}

	public function getDepositionsByOwner( $userID )
	{
		return $this->db->cols( 'SELECT `ID` FROM `Depositions` WHERE `ownerID`=?', [$userID] );
	}

	public function getAPIDepositions( $userID, $scheduled=TRUE )
	{
		$userID = (int)$userID;
		$sql = "SELECT d.*, NULL as `password`, NULL as `courtReporterPassword` FROM Depositions d
			INNER JOIN Cases ca ON (ca.ID=d.caseID)
			INNER JOIN Users u ON (u.ID=:userID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=d.caseID AND cm.userID=u.ID)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND da.userID=u.ID)
			WHERE d.deleted IS NULL AND ca.deleted IS NULL
			AND (
				(das.userID=u.ID OR cm.userID=u.ID OR d.createdBy=u.ID OR d.ownerID=u.ID OR da.userID=u.ID)
				OR (ca.class='Demo' AND ca.clientID=u.clientID)
			)";
		$sql .= ((bool)$scheduled) ? ' AND d.finished IS NULL' : ' AND d.finished IS NOT NULL';
		return $this->db->rows( $sql, ['userID'=>$userID] );
	}

	public function getSortPosition( $depositionID, $userID )
	{
		$depositionID = (int)$depositionID;
		$sql = "SELECT IFNULL(sortPos,{$depositionID}) as sortPos FROM UserCustomSortDepositions WHERE depositionID=:depoID AND userID=:userID";
		return $this->db->col( $sql, ['depoID'=>$depositionID, 'userID'=>$userID] );
	}

	public function getWidgetDepositionReportData( $type, array $params )
	{
		$select = 'SELECT d.ID, d.depositionOf, d.volume, d.started, d.class, c.ID as caseID, c.number as caseNumber, c.name as caseName, cl.ID as clientID, cl.name as clientName';
		//		\ClickBlocks\Debug::ErrorLog( "getWidgetDepositionReportData: " . print_r( $params, TRUE ) );
		$from = ' FROM Depositions d
			INNER JOIN Cases c ON (c.ID=d.caseID)
			INNER JOIN Clients cl ON (cl.ID=c.clientID AND ((d.enterpriseResellerID IS NULL AND cl.resellerID=:resellerID) OR d.enterpriseResellerID=:resellerID))';
		$where = " WHERE d.statusID='F' AND d.parentID IS NULL AND d.class IN (:deposition, :witnessPrep, :trial, :trialBinder, :arbitration, :mediation, :hearing) AND d.started BETWEEN :startDate AND :endDate";
		$vars = ['resellerID'=>(int)$params['resellerID'],
			'startDate'=>date( 'Y-m-d 00:00:00', strtotime( $params['startDate'] ) ),
			'endDate'=>date( 'Y-m-d 23:59:59', strtotime( $params['endDate'] ) ),
			'deposition'=> IEdepoBase::DEPOSITION_CLASS_DEPOSITION,
			'witnessPrep'=> IEdepoBase::DEPOSITION_CLASS_WITNESSPREP,
			'trial'=> IEdepoBase::DEPOSITION_CLASS_TRIAL,
			'trialBinder'=> IEdepoBase::DEPOSITION_CLASS_TRIALBINDER,
			'arbitration'=> IEdepoBase::DEPOSITION_CLASS_ARBITRATION,
			'mediation'=> IEdepoBase::DEPOSITION_CLASS_MEDIATION,
			'hearing'=> IEdepoBase::DEPOSITION_CLASS_HEARING,
		];
		
		if( $type == 'count' ) {
			return (int)$this->db->col( "SELECT COUNT(d.ID) {$from} {$where}", $vars );
		}
		
		$limit = '';
		if( $params['pageSize'] ) {
			$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
		}
		
		$sql = "{$select} {$from} {$where} {$limit}";
		//		\ClickBlocks\Debug::ErrorLog( "getWidgetDepositionReportData: " . print_r( [$sql, $vars], TRUE ) );
		return $this->db->rows( $sql, $vars );
	}

	public function getSessionParentID( $sessionID ) {
		$sql = 'SELECT IFNULL(d.parentID,d.ID) as parentID FROM Depositions d WHERE d.ID=:sessionID';
		return (int)$this->db->col( $sql, ['sessionID' => (int)$sessionID] );
	}

	public function getDepositionIdByParentId( $parentID, $clientID ) {
		$sql = 'SELECT d.ID FROM Depositions d
			INNER JOIN Cases c ON (d.caseID = c.ID)
			WHERE c.deleted IS NULL AND c.clientID = :clientID
			AND d.deleted IS NULL AND d.parentID = :parentID';
		return (int)$this->db->col( $sql, [ 'parentID'=>(int)$parentID, 'clientID'=>(int)$clientID ] );
	}
}
