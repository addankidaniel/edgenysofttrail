<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraDatasources extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\Datasources' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\Datasources';
	}

	/**
	 *
	 * @param bigint $clientID
	 */
	public function getDatasources( $clientID )
	{
		$sql = 'SELECT * FROM Datasources WHERE clientID=:clientID';
		return $this->db->rows( $sql, array( 'clientID' => $clientID ) );
	}

	/**
	 *
	 * @param bigint $clientID
	 * @param bigint $datasourceID
	 */
	public function getDatasourceByID( $clientID, $datasourceID )
	{
		$sql = 'SELECT * FROM Datasources WHERE clientID=:clientID AND ID=:datasourceID';
		return $this->db->row( $sql, array( 'clientID' => $clientID, 'datasourceID' => $datasourceID ) );
	}

	/**
	 *
	 * @param bigint $clientID
	 * @return boolean
	 */
	public function hasDatasources( $clientID )
	{
		$sql = 'SELECT COUNT(ID) FROM Datasources WHERE clientID=:clientID';
		$result = $this->db->col( $sql, array( 'clientID' => $clientID ) );
		return ($result > 0);
	}

	/**
	 *
	 * @param bigint $clientID
	 * @return bigint
	 */
	public function countDatasources( $clientID )
	{
		$sql = 'SELECT COUNT(ID) FROM Datasources WHERE clientID=:clientID';
		return $this->db->col( $sql, array( 'clientID' => $clientID ) );
	}

	/**
	 *
	 * @param array $params
	 */
	public function getWidgetData( array $params )
	{
		$start = $params['pageSize'] * $params['pos'];
		$end = $params['pageSize'];
		$sql = "SELECT * FROM Datasources WHERE clientID=:clientID LIMIT {$start},{$end}";
		return $this->db->rows( $sql, ['clientID'=>$params['clientID']] );
	}
}
