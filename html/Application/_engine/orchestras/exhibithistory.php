<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\Debug;

class OrchestraExhibitHistory extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\ExhibitHistory' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\ExhibitHistory';
	}

	public function exhibitHistoryForExhibit( $fileID )
	{
		$sql = 'SELECT * FROM ExhibitHistory WHERE exhibitFileID=:fileID ORDER BY introducedDate DESC, ID DESC';
		return $this->db->row( $sql, ['fileID'=>$fileID] );	//only first result
	}

	public function exhibitHistoryForSource( $fileID )
	{
		$sql = 'SELECT * FROM ExhibitHistory WHERE sourceFileID=:fileID ORDER BY introducedDate DESC, ID DESC';
		return $this->db->row( $sql, ['fileID'=>$fileID] );
	}

	public function exhibitHistoryForDeposition( $depoID, $log=FALSE )
	{
		$offset = '-0:00';
		$tzOffset = $_SESSION['__EDEPO__']['timeZoneOffset'];
		if( $tzOffset ) {
			$mins = $tzOffset;
			$sgn = ($mins < 0 ? -1 : 1);
			$mins = abs( $mins );
			$hrs = floor( $mins / 60 );
			$mins -= $hrs * 60;
			$offset = sprintf( '%+d:%02d', $hrs * $sgn, $mins );
		}
		$sql = "SELECT h.*, DATE_FORMAT(CONVERT_TZ(h.introducedDate,'-0:00','{$offset}'),'%c/%e/%Y %l:%i:%s %p') as lIntroducedDate, sf.name as sourceFilename
			FROM ExhibitHistory h
			LEFT JOIN Files ef ON (h.exhibitFileID=ef.ID)
			LEFT JOIN Files sf ON (h.sourceFileID=sf.ID)
			WHERE depositionID=:depoID
			ORDER BY introducedDate ASC, ID ASC";
		$rows = $this->db->rows( $sql, ['depoID'=>$depoID] );
		if( $log ) {
			return $rows;
		}
		$exhibitHistory = [];
		foreach( $rows as $row ) {
			$exhibitHistory[$row['exhibitFileID']] = $row;
		}
		return array_values( $exhibitHistory );
	}

	public function exhibitHistoryForCase( $caseID )
	{
		$offset = '-0:00';
		$tzOffset = $_SESSION['__EDEPO__']['timeZoneOffset'];
		if( $tzOffset ) {
			$dateTime = new \DateTime();
			$mins = $tzOffset;
			$sgn = ($mins < 0 ? -1 : 1);
			$mins = abs( $mins );
			$hrs = floor( $mins / 60 );
			$mins -= $hrs * 60;
			$offset = sprintf( '%+d:%02d', $hrs * $sgn, $mins );
		}
		$bind = [
			'caseID' => (int)$caseID,
			'WP' => IEdepoBase::DEPOSITION_CLASS_WITNESSPREP,
			'WPDemo' => IEdepoBase::DEPOSITION_CLASS_WPDEMO,
			'TB' => IEdepoBase::DEPOSITION_CLASS_TRIALBINDER
		];
		$sql = "SELECT h.*, DATE_FORMAT(CONVERT_TZ(h.introducedDate,'-0:00','{$offset}'),'%c/%e/%Y %l:%i:%s %p') as lIntroducedDate, sf.name as sourceFilename, d.depositionOf, d.volume, d.deleted as sessionDeleted
			FROM ExhibitHistory h
			INNER JOIN Depositions d ON (h.depositionID=d.ID AND (d.class != :WP AND d.class != :WPDemo AND d.class != :TB))
			LEFT JOIN Files ef ON (h.exhibitFileID=ef.ID)
			LEFT JOIN Files sf ON (h.sourceFileID=sf.ID)
			WHERE h.caseID=:caseID
			ORDER BY d.started ASC, h.introducedDate ASC, h.ID ASC";
		//Debug::ErrorLog( [ 'OrchestraExhibitHistory::exhibitHistoryForCase', $sql, $bind ] );
		return $this->db->rows( $sql, $bind );
	}

	public function getWidgetExhibitData($type, array $params) {
		$f = 'FROM ExhibitHistory h '
			. 'LEFT JOIN Files ef ON (h.exhibitFileID=ef.ID) '
			. 'LEFT JOIN Files sf ON (h.sourceFileID=sf.ID) '
			. 'WHERE depositionID=:depoID';

		switch ($type) {
			case 'count':
				return $this->db->col('SELECT COUNT(*) ' . $f, ['depoID'=>$params['depoID']] );
			case 'rows':
				$offset = '-0:00';
				$tzOffset = $_SESSION['__EDEPO__']['timeZoneOffset'];
				if( $tzOffset ) {
					$mins = $tzOffset;
					$sgn = ($mins < 0 ? -1 : 1);
					$mins = abs( $mins );
					$hrs = floor( $mins / 60 );
					$mins -= $hrs * 60;
					$offset = sprintf( '%+d:%02d', $hrs * $sgn, $mins );
				}
				$sql = "SELECT h.*, DATE_FORMAT(CONVERT_TZ(h.introducedDate,'-0:00','{$offset}'),'%c/%e/%Y %l:%i:%s %p') as lIntroducedDate, sf.name as sourceFilename " . $f;
				return $this->db->rows( $sql, ['depoID'=>$params['depoID']] );
		}
	}
}
