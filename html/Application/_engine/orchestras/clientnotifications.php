<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
ClickBlocks\Cache;

class OrchestraClientNotifications extends OrchestraEdepo
{
	const BLLClassName = '\ClickBlocks\DB\ClientNotifications';
	const TableName = 'ClientNotifications';

	public function __construct()
	{
		parent::__construct( self::BLLClassName );
	}

	public static function getBLLClassName()
	{
		return self::BLLClassName;
	}

	public function getNotificationMessages( $sort=null, $pageSize=null, $page=null )
	{
		$bv = [];

		$sql = 'SELECT n.*, m.*, u.* FROM '.self::TableName.' n'
			. ' JOIN `ClientMessages` m ON m.ID=n.clientMessageID'
			. ' JOIN `Users` u ON u.ID=n.userID';

		if (!is_array( $sort ))
		{
			if ($sort)
			{
				$sort = 'n.'.$sort;
			}
		} else {
			if (isset( $sort['by'] ) && $sort['by'])
			{
				$sort['by'] = 'n.'.$sort['by'];
			}
		}

		$sql .= $this->prepareSelectSorting( $sort );
		$sql .= $this->prepareSelectPaging( $pageSize, $page );

		return $this->db->rows( $sql, $bv );
	}


	public function getNotificationsByClientID( $userID, $sort=null, $pageSize=null, $page=null )
	{
		$bv = [];

		$sql = 'SELECT ID, userID, clientMessageID, createdOn, sendOn FROM '.self::TableName.' WHERE userID=?';
		$bv[] = $userID;

		$sql .= $this->prepareSelectSorting( $sort );
		$sql .= $this->prepareSelectPaging( $pageSize, $page );

		return $this->db->rows( $sql, $bv );
	}

	public function getNotificationMessagesByClientID( $userID, $sort=null, $pageSize=null, $page=null )
	{
		$bv = [];

		$sql = 'SELECT n.ID, n.userID, n.clientMessageID, n.createdOn, n.sendOn FROM '.self::TableName.' n'
			. ' JOIN `ClientMessages` m ON m.ID=n.clientMessageID WHERE userID=?';
		$bv[] = $userID;

		if (!is_array( $sort ))
		{
			if ($sort)
			{
				$sort = 'n.'.$sort;
			}
		} else {
			if (isset( $sort['by'] ) && $sort['by'])
			{
				$sort['by'] = 'n.'.$sort['by'];
			}
		}

		$sql .= $this->prepareSelectSorting( $sort );
		$sql .= $this->prepareSelectPaging( $pageSize, $page );

		return $this->db->rows( $sql, $bv );
	}

	public function getUserIDsByResellerID( $resellerID, $userType=null )
	{
		$bv = [];

		$t = ' FROM Users u JOIN Clients c ON c.ID=u.clientID JOIN Users r ON r.clientID=c.resellerID';

		$sql = "SELECT u.ID $t WHERE r.ID=? AND r.typeID='R'";
		$bv[] = $resellerID;

		if ($userType)
		{
			$sql .= ' AND u.typeID=?';
			$bv[] = $userType;
		}

		return $this->db->rows( $sql, $bv );
	}

	public function getUserInfoByResellerID($resellerID, $userType=null)
	{
		$bv = ['rID'=>$resellerID];

		$sql = 'SELECT u.ID, u.typeID, u.firstName, u.lastName, u.username, u.email, c.name, c.contactName, c.contactEmail FROM Users u
			INNER JOIN Clients c ON (c.ID=u.clientID AND c.deactivated IS NULL)
			INNER JOIN Users ur ON ((ur.ID=:rID AND ur.clientID=c.ID) OR (ur.ID=:rID AND ur.clientID=c.resellerID))';

		switch ($userType) {
			default:
			case 'A': // Include All Users
				break;
			case 'C': // Include Client Admins
				$sql .= " AND u.typeID IN ('R', 'C')";
				break;
			case 'R': // Resellers only
				$sql .= " AND u.typeID='R'";
				break;
		}

		return $this->db->rows( $sql, $bv );
	}

	public function getWidgetLogData($type, array $params)
	{
        // Build WHERE query
		$t = ' FROM ClientNotifications n JOIN Users u ON u.ID=n.userID JOIN ClientMessages m ON m.ID=n.clientMessageID';
        $w = ' WHERE n.sentOn IS NOT NULL';

		if ($type === 'count')
		{
			return $this->db->col( 'SELECT COUNT(*)' . $t . $w );
		} else
		if ($type === 'rows') {
			$searchCols = [
				1 => 'n.sentOn',
				2 => 'u.email',
				3 => 'u.firstName',
				4 => 'u.lastName',
				5 => 'm.subject',
				6 => 'm.messageBody',
			];

			$sortOn = isset( $params['sortBy'] ) ? $searchCols[abs( $params['sortBy'] )] : $searchCols[1];
			$sortBy = ' ORDER BY '.$sortOn.' '.(isset( $params['sortBy'] ) ? ($params['sortBy'] > 0 ? 'DESC' : 'ASC') : 'DESC');

            if ($params['pageSize'])
			{
				$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
			}

            $sql = 'SELECT ' . implode( ', ', $searchCols ) . $t . $w . $sortBy . $limit;

			return $this->db->rows($sql);
		}
    }
}

?>
