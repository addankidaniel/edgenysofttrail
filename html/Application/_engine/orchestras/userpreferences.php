<?php

namespace ClickBlocks\DB;


class OrchestraUserPreferences extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct( '\ClickBlocks\DB\UserPreferences' );
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\UserPreferences';
	}

	/**
	 * User Preferences
	 * @param bigint $userID
	 * @return array
	 */
	public function getPreferencesForUserID( $userID )
	{
		$userID = (int)$userID;
		$sql = 'SELECT * FROM UserPreferences WHERE userID=:userID';
		return $this->db->rows( $sql, ['userID'=>$userID] );
	}
}
