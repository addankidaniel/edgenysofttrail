<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraAPISessions extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\APISessions');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\APISessions';
   }

  public function deleteExpired($lifeTime = null)
  {
     if (!$lifeTime) $lifeTime = $this->config->api['sessionLifetime'];
     return $this->db->execute('DELETE FROM APISessions WHERE created<DATE_SUB(NOW(),INTERVAL :sec SECOND)', array(':sec'=>(int)$lifeTime));
   }

  public function deleteByUser($userID)
  {
     return $this->db->execute('DELETE FROM APISessions WHERE userID=?', array((int)$userID));
   }

  public function deleteByAttendee($attendeeID)
  {
     return $this->db->execute('DELETE FROM APISessions WHERE attendeeID=?', array((int)$attendeeID));
   }

  public function getBySKey($sKey)
  {
     return $this->getObjectByQuery(array('sKey'=>$sKey));
   }

	public function userHasSession( $userID ) {
		return (bool)$this->db->col( "SELECT ID FROM APISessions WHERE userID=:userID", ['userID' => (int)$userID] );
	}
}
