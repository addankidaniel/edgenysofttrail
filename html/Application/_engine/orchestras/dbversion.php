<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraDbVersion extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\DbVersion');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\DbVersion';
   }

  public function getDbVersion()
  {
       return $this->db->col('SELECT version FROM _db_version');
   }
}

?>