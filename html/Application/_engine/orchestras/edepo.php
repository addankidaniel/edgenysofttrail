<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\Cache;

class OrchestraEdepo extends Orchestra implements \ClickBlocks\API\IEdepoBase
{
   /**
    * @var \ClickBlocks\Utils\FileSystem
    */
   protected $fs;

   public function __construct($className = null) {
      parent::__construct($className);
      $this->fs = new Utils\FileSystem();
   }

   public function attachMock($name, $mock) {
      if (!in_array($name, array('fs','db')) || $this->{$name}!=NULL)
        throw new \Exception(__METHOD__.'() failed. '.$name.' was already defined.');
      $this->{$name} = $mock;
   }

   /**
    *
    * @param string $type
    * @param array $params
    * @param string $select select part of query
    * @param string $from table name followed by all joins
    * @param string|array $where array will be imploded with AND
    * @param string $afterWhere add your group by and/or having clauses here
    * @param array $orderMap
    * @return type
    */
   protected function _getWidgetData($type, array $params, $select, $from, $where, $afterWhere = '', array $orderMap = array()) {
      if (stripos(trim($from), 'FROM')!==0) $from = 'FROM '.$from;
      if (is_array($where)) $where = 'WHERE '.implode(' AND ', $where);
      elseif (stripos(trim($where), 'WHERE')!==0) $where = 'WHERE '.$where;
      //if ($groupBy && stripos(trim($groupBy), 'GROUP BY')!==0) $groupBy = 'GROUP BY '.$groupBy;
      switch ($type)
      {
      case 'aggregate':
         return $this->db->col('SELECT ' . $select . ' ' . $from. ' '. $where.' '.$afterWhere);
      case 'count':
         return $this->db->col('SELECT COUNT(*) ' . $from. ' '. $where.' '.$afterWhere);
      case 'rows':
         if (count($orderMap)>0) {
            $sortBy = 'ORDER BY '.($orderMap[$sortBy] ?: reset($orderMap));
            if ($params['sortBy'] > 0) $sortBy .= ' DESC';
            else $sortBy .= ' ASC';
         }
         else $sortBy = '';
         if (!$select) $select = '*';
         if (stripos(trim($select), 'SELECT')!==0) $select = 'SELECT '.$select;
         if ($params['pageSize'] && $params['pageSize']!==-1) $limit = 'LIMIT ' . ($params['pageSize'] * (int)$params['pos']) . ', ' . $params['pageSize'];
         else $limit = '';
         $sql = $select.' '.$from.' '.$where.' '.$afterWhere.' '.$sortBy.' '.$limit;
		 if (isset($_REQUEST['debug'])) \ClickBlocks\Debug::ErrorLog( $sql );
         return $this->db->rows($sql);
      }
   }

	/**
	 *
	 * @todo Modify to handle multiple column sorting
	 * @param type $sort
	 * @return string
	 */
	protected function prepareSelectSorting( $sort )
	{
		$order = '';

		if ($sort)
		{
			if (!is_array( $sort ))
				$order = ' ORDER BY '.$sort;
			else if (isset( $sort['by'] ) && $sort['by'])
				$order = ' ORDER BY '.$sort['by'].' '.(isset( $sort['dir'] ) && $sort['dir'] === 'DESC' ? 'DESC' : 'ASC');
		}

		return $order;
	}

   protected function prepareSelectPaging( $pageSize=null, $page=null)
   {
	   return ($pageSize && $pageSize > 0 ? ' LIMIT '.((int)$pageSize * (int)$page).','.(int)$pageSize : '');
   }

   protected function joinIn(array $in)
   {
      foreach ($in as $k=>$v) $_in[] = $this->db->quote($v);
      return ' IN ('.implode(',', $_in).') ';
   }

}

?>