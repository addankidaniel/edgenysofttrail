<?php

namespace ClickBlocks\DB;

use ClickBlocks\API\IEdepoBase;

class OrchestraClients extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\Clients');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\Clients';
   }
	public static function getClientByURL( $url ) {
		if( !$url ) return NULL;
		$q = 'SELECT * FROM Clients WHERE URL=? AND deleted IS NULL';
		return self::getDB()->row( $q, [$url] );
	}

  /**
    *
    * @param string $url - reseller URL to search for
    * @return array Reseller values, or NULL if not found
    */
  public static function getResellerByURL($url)
  {
      if (!$url) return NULL;
	  $q = 'SELECT * FROM Clients WHERE URL=? AND typeID = "'.IEdepoBase::CLIENT_TYPE_RESELLER.'" AND deleted IS NULL';
      return self::getDB()->row( $q, [$url] );
   }

	public static function getEnterpriseClientByURL( $url )
	{
		if (!$url)
		{
			return null;
		}
		$q = 'SELECT * FROM Clients WHERE URL=? AND typeID="'.IEdepoBase::CLIENT_TYPE_ENT_CLIENT.'" AND deleted IS NULL';
		return self::getDB()->row( $q, [$url] );
	}

	public static function getEnterpriseResellers()
	{
		$q = 'SELECT ID, name FROM Clients WHERE typeID="'.IEdepoBase::CLIENT_TYPE_RESELLER.'" AND deactivated IS NULL AND deleted IS NULL';
		return self::getDB()->rows( $q );
	}

	public function getWidgetClientData( $type, array $params )
	{
		$resellerID = (isset( $params['resellerID'] )) ? (int)$params['resellerID'] : NULL;
		// Build WHERE query
		$w = ' WHERE (c.deleted IS NULL';
		if( $params['showDeleted'] ) {
			$w .= ' OR c.deleted IS NOT NULL)';
		} else {
			$w .= ')';
		}
		if( $params['typeID'] === IEdepoBase::CLIENT_TYPE_RESELLER ) {
			$w .= ' AND c.typeID="' .IEdepoBase::CLIENT_TYPE_RESELLER.'"';
		} elseif( $params['typeID'] === IEdepoBase::CLIENT_TYPE_CLIENT ) {
			$w .= ' AND (c.typeID="' . IEdepoBase::CLIENT_TYPE_CLIENT . '" OR c.typeID="' . IEdepoBase::CLIENT_TYPE_ENT_CLIENT . '")';
		} elseif( $params['typeID'] === IEdepoBase::CLIENT_TYPE_ENT_CLIENT ) {
			$w .= ' AND c.typeID="' . IEdepoBase::CLIENT_TYPE_ENT_CLIENT . '"';
		}
		if( $params['searchValue'] ) {
			$w .= ' AND c.name LIKE ' . $this->db->quote( '%' . $params['searchValue'] . '%' );
		}
		if( $resellerID ) {
			$w .= ' AND c.resellerID=' . $params['resellerID'];
		}
		$sortDir = ($params['sortBy'] > 0) ? 'DESC' : 'ASC';
		switch( $type ) {
		case 'count':
			return $this->db->col( 'SELECT COUNT(*) FROM Clients c ' . $w );
		case 'rows':
			switch( abs( $params['sortBy'] ) ) {
				case 1:
					$sortBy = "c.ID {$sortDir}";
					break;
				case 2:
					$sortBy = "c.name  {$sortDir}, c.ID ASC";
					break;
				case 3:
					$sortBy = "c.typeID {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 4:
					$sortBy = "c.contactName {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 5:
					$sortBy = "c.startDate {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 6:
					$sortBy = "c.deactivated {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 7:
					$sortBy = "r.name {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 8:
					$sortBy = "c.resellerLevel {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 9:
					$sortBy = "c.resellerClass {$sortDir}, c.name ASC, c.ID ASC";
					break;
			}
			$sortBy = ' ORDER BY ' . $sortBy;
			$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
			$sql = "SELECT c.ID, c.name, c.typeID, c.contactName, c.startDate, c.deactivated, c.resellerLevel, c.resellerClass, r.name as resellerName, c.deleted FROM Clients c
				LEFT JOIN Clients r ON (r.ID=c.resellerID)
				{$w} {$sortBy} {$limit}";
//			\ClickBlocks\Debug::ErrorLog( $sql );
			return $this->db->rows( $sql );
		}
	}

  public function checkUniqueUrl($value)
  {
       if (empty($value)) return true;
       return !($this->db->col('SELECT COUNT(*) FROM Clients WHERE url = ? ', array($value)) > 0);
   }

  public function getResellerURLByDepo($depoID)
  {
      return $this->db->col('SELECT IF(cl.URL IS NULL, r.URL, cl.URL) FROM Depositions d'
		  . ' INNER JOIN Cases ca ON (ca.ID=d.caseID)'
		  . ' INNER JOIN Clients cl ON (cl.ID=ca.clientID)'
		  . ' LEFT JOIN Clients r ON (r.ID=cl.resellerID)'
		  . ' WHERE d.ID=?',array($depoID));
   }

  public function getWidgetReportData($type, array $params)
  {
        // Build WHERE query
        $w = ' WHERE c.deleted is NULL ';

		if ($params['typeID'] === IEdepoBase::CLIENT_TYPE_RESELLER)
		{
			$w .= ' AND (c.typeID="'.IEdepoBase::CLIENT_TYPE_RESELLER.'" AND c.resellerClass IN ("'.IEdepoBase::RESELLERCLASS_SCHEDULING.'","'.IEdepoBase::RESELLERCLASS_RESELLER.'"))';
		} else {
			$w .= ' AND (c.typeID="'.IEdepoBase::CLIENT_TYPE_CLIENT.'" OR c.typeID="'.IEdepoBase::CLIENT_TYPE_ENT_CLIENT.'")';
		}

        if ($params['searchValue'])
        {
            $w .= ' AND c.name LIKE '.$this->db->quote('%'.$params['searchValue'].'%');
        }
        if ($params['resellerID'])
        {
            $w .= ' AND c.resellerID = '.$params['resellerID'];
        }

        if ($params['active'] == '0') {
            $w .= ' AND c.deactivated IS NOT NULL ';
        } else if ($params['active'] == '1')  {
            $w .= ' AND c.deactivated IS NULL ';
        }
		if (trim($params['location']) != false) {
			$w .= ' AND c.location = \'' . $params['location'] . '\'';
		}

        switch ($type)
        {
        case 'count':
            return $this->db->col('SELECT COUNT(*) FROM Clients c ' . $w);
        case 'rows':
            switch (abs($params['sortBy']))
            {
                case 1:
                    $sortBy = 'c.ID';
                    break;
                case 2:
                    $sortBy = 'c.name';
                    break;
                case 3:
                    $sortBy = 'c.city';
                    break;
                case 4:
                    $sortBy = 'c.state';
                    break;
                case 5:
                    $sortBy = 'c.contactName';
                    break;
                case 6:
				default:
                    $sortBy = 'c.deactivated';
                    break;
            }

            if ($params['sortBy'] > 0)
                $sortBy .= ' DESC';
            else $sortBy .= ' ASC';

            $sortBy = ' ORDER BY ' . $sortBy;
            if ($params['pageSize']) $limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
            $sql = 'SELECT c.ID, c.name, c.startDate, c.address1, c.address2, c.city, c.state, c.zip, c.deactivated, c.contactName,'
					. ' c.contactEmail, c.contactPhone, c.courtReporterEmail, c.salesRep'
					. ' FROM Clients c ' . $w . $sortBy . $limit;

           return $this->db->rows($sql);
       }
    }

    public function getTotalClients($resellerID){
        return $this->db->col('SELECT COUNT(*) FROM Clients WHERE deleted IS NULL AND resellerID = ? ', array($resellerID));
    }

	/**
	 * @param bigint $clientID
	 * @return array
	 */
	public function getClientAdmins( $clientID ) {
		$clientID = (int)$clientID;
		$result = $this->db->rows( "SELECT DISTINCT ID as clientAdminID FROM Users WHERE clientID = :clientID AND typeID = 'C'", array( 'clientID' => $clientID ) );
		$clientAdmins = array();
		if( $result && is_array( $result ) && count( $result ) > 0 ) {
			foreach( $result as $row ) {
				$clientAdmins[] = (int)$row['clientAdminID'];
			}
		}
		return $clientAdmins;
	}

    public function getResellerLocations($resellerID){
        return $this->db->rows('SELECT location FROM Clients WHERE deleted IS NULL AND location is NOT NULL AND resellerID = ? ', array($resellerID));
    }

    public function getClientDemoCases( $clientID ){
        return $this->db->rows( 'SELECT * FROM Cases WHERE class = \'Demo\' AND clientID = ?', array( $clientID ) );
    }

	public function getSchedulingResellers( $sponsorID )
	{
		$sponsorID = (int)$sponsorID;
		$sql = 'SELECT r.* FROM Clients r
			LEFT JOIN Clients sr ON (sr.ID=r.ID AND sr.ID=:sponsorID)
			WHERE r.typeID=:typeID
			AND r.resellerClass=:class
			AND r.deactivated IS NULL
			AND r.deleted IS NULL
			ORDER BY -sr.ID DESC, r.resellerLevel ASC, r.name ASC';
		$params = ['sponsorID'=>$sponsorID, 'typeID'=>self::CLIENT_TYPE_RESELLER, 'class'=>self::RESELLERCLASS_SCHEDULING];
//		\ClickBlocks\Debug::ErrorLog( print_r( ['OrchestraClients::getSchedulingResellers', $sql, $params], TRUE ) );
		return $this->db->rows( $sql, $params );
	}

	public function getResellerClients( $resellerID )
	{
		$resellerID = (int)$resellerID;
		return $this->getObjectsByQuery( ['resellerID'=>$resellerID] );
	}

	public function getWidgetCourtClientsData( $type, Array $params ) {
		// Build WHERE query
		$bind = ['typeID'=>IEdepoBase::CLIENT_TYPE_COURTCLIENT];
		$w = ' WHERE c.typeID=:typeID AND c.deleted IS NULL';

		if( $params['searchValue'] ) {
			$bind['search'] = "%{$params['searchValue']}%";
			//\ClickBlocks\Debug::ErrorLog( print_r( $bind, TRUE ) );
			$w .= ' AND c.name LIKE :search';
		}
		$sortDir = ($params['sortBy'] > 0) ? 'DESC' : 'ASC';
		switch( $type ) {
		case 'count':
			return $this->db->col( 'SELECT COUNT(*) FROM Clients c ' . $w, $bind );
		case 'rows':
			switch( abs( $params['sortBy'] ) ) {
				case 1:
					$sortBy = "c.ID {$sortDir}";
					break;
				case 2:
					$sortBy = "c.name {$sortDir}, c.ID ASC";
					break;
				case 3:
					$sortBy = "c.contactName {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 4:
					$sortBy = "c.startDate {$sortDir}, c.name ASC, c.ID ASC";
					break;
				case 5:
					$sortBy = "c.deactivated {$sortDir}, c.name ASC, c.ID ASC";
					break;
			}
			$sortBy = ' ORDER BY ' . $sortBy;
			$limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
			$sql = "SELECT c.ID, c.name, c.contactName, c.startDate, c.deactivated, c.deleted FROM Clients c {$w} {$sortBy} {$limit}";
			//\ClickBlocks\Debug::ErrorLog( $sql );
			return $this->db->rows( $sql, $bind );
		}
	}

	public function getClientByResellerIDDateRange( $resellerID, $startDate, $endDate ) {
		$sql = "SELECT ID, name FROM Clients
				WHERE resellerID=:resellerID
				AND (deactivated IS NULL OR deactivated BETWEEN :startDate AND :endDate OR deactivated > :endDate)
				AND (deleted IS NULL OR deleted BETWEEN :startDate AND :endDate OR deleted > :endDate)
				/*AND (freeTrial = 0 OR (freeTrial = 1 AND LAST_DAY(ADDDATE(created, INTERVAL 30 DAY)) < CURDATE()))*/
				ORDER BY name";
		return $this->db->rows( $sql, ['resellerID'=>$resellerID, 'startDate'=>$startDate, 'endDate'=>$endDate] );
	}

	public function getBillableUserSummaryForReseller( $resellerID, $startDate, $endDate ) {
		$sql = 'SELECT c.ID as clientID,c.name,c.perUserCount,c.perCaseCount,c.price,((c.perUserCount * c.price) + (c.perCaseCount * c.price)) as subtotal
			FROM (
				SELECT cl.ID,cl.name,pmo.price,
				SUM(IF((pcu.ID IS NULL AND (u.deleted IS NULL OR u.deleted BETWEEN :startDate AND :endDate OR u.deleted > :endDate) AND (u.typeID != :clientAdmin OR (u.typeID=:clientAdmin AND u.created < :gfDate))),1,0)) as perUserCount,
				COUNT(pcu.ID) as perCaseCount
				FROM Clients r
				INNER JOIN Clients cl ON (cl.resellerID=r.ID)
				INNER JOIN Users u ON (u.clientID=cl.ID)
				LEFT JOIN PricingModelOptions pmo ON (pmo.clientID=r.ID AND pmo.optionID=:pmoSub)
				LEFT JOIN (
					SELECT pcu_u.ID,pcu_u.clientID
					FROM Users pcu_u
					INNER JOIN Clients pcu_cl ON (pcu_cl.ID=pcu_u.clientID)
					LEFT JOIN Cases pcu_ca ON (pcu_ca.clientID=pcu_cl.ID AND pcu_ca.class=:case)
					LEFT JOIN CaseManagers pcu_cm ON (pcu_cm.userID=pcu_u.ID AND pcu_cm.caseID=pcu_ca.ID)
					LEFT JOIN Depositions pcu_d ON (pcu_d.caseID=pcu_ca.ID)
					LEFT JOIN DepositionAssistants pcu_das ON (pcu_das.userID=pcu_u.ID AND pcu_das.depositionID=pcu_d.ID)
					LEFT JOIN DepositionAttendees pcu_dat ON (pcu_dat.userID=pcu_u.ID AND pcu_dat.depositionID=pcu_d.ID)
					WHERE (pcu_ca.deleted IS NULL OR pcu_ca.deleted BETWEEN :startDate AND :endDate OR pcu_ca.deleted > :endDate)
					AND (pcu_d.deleted IS NULL OR pcu_d.deleted BETWEEN :startDate AND :endDate OR pcu_d.deleted > :endDate)
					AND (pcu_u.deleted IS NULL OR pcu_u.deleted BETWEEN :startDate AND :endDate OR pcu_u.deleted > :endDate)
					AND pcu_u.created > :gfDate
					AND pcu_cl.typeID = :clients
					GROUP BY pcu_ca.ID,pcu_u.ID HAVING SUM(IF(pcu_cm.userID=pcu_u.ID,1,0)) > 0 OR SUM(IF(pcu_d.ownerID=pcu_u.ID,1,0)) > 0 OR SUM(IF(pcu_das.userID=pcu_u.ID,1,0)) > 0 OR SUM(IF(pcu_dat.userID=pcu_u.ID,1,0)) > 0
				) pcu ON (pcu.clientID=cl.ID AND pcu.ID=u.ID)
				WHERE r.ID=:resellerID
				AND (cl.deleted IS NULL OR cl.deleted BETWEEN :startDate AND :endDate OR cl.deleted > :endDate)
				AND (cl.deactivated IS NULL OR cl.deactivated BETWEEN :startDate AND :endDate OR cl.deactivated > :endDate)
				/*AND (cl.freeTrial = 0 OR (cl.freeTrial = 1 AND LAST_DAY(ADDDATE(cl.created, INTERVAL 30 DAY)) < CURDATE()))*/
				GROUP BY cl.ID
			) as c
			ORDER BY c.name,c.ID';
		//\ClickBlocks\Debug::ErrorLog( print_r( ['getBillableUserSummaryForReseller',$sql,$resellerID,$startDate,$endDate,$this->config->userGrandfatherDate] ) );
		return $this->db->rows( $sql, [
			'resellerID' => (int)$resellerID,
			'gfDate' => $this->config->userGrandfatherDate,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'clients' => IEdepoBase::CLIENT_TYPE_CLIENT,
			'clientAdmin' => IEdepoBase::USER_TYPE_CLIENT,
			'pmoSub' => IEdepoBase::PRICING_OPTION_SUBSCRIPTION,
			'case' => IEdepoBase::CASE_CLASS_CASE
		] );
	}
}
