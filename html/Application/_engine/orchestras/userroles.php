<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraUserRoles extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\UserRoles');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\UserRoles';
   }
}

?>