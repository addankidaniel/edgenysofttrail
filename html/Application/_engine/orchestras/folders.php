<?php

namespace ClickBlocks\DB;

use ClickBlocks\API\IEdepoBase,
	ClickBlocks\Debug;

class OrchestraFolders extends OrchestraEdepo
{
  public function __construct()
  {
		parent::__construct('\ClickBlocks\DB\Folders');
	}

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\Folders';
   }

  public function getFoldersByDepo($depoID, $createdBy = null)
  {
        if ($createdBy)
            return $this->db->rows('SELECT * FROM Folders WHERE depositionID = ? AND createdBy = ?', array($depoID, $createdBy));
        else
            return $this->db->rows('SELECT * FROM Folders WHERE depositionID = ?', array($depoID));
    }

	public function setLastModified( $folderID )
	{
		return $this->db->execute( 'UPDATE Folders SET lastModified = NOW() WHERE ID = ?', array( (int)$folderID ) );
	}

	public function getFoldersByName( $depoID, $folderName ) {
		$sql = 'SELECT f.* FROM Folders f
			LEFT JOIN SessionCaseFolders cf ON (cf.folderID=f.ID AND cf.sessionID=:depoID)
			WHERE (f.depositionID=:depoID OR f.ID=cf.folderID) AND name=:folderName';
		return $this->db->rows( $sql, ['depoID'=>(int)$depoID, 'folderName'=>$folderName] );
	}

	public function getCaseFolderByName( $caseID, $folderName ) {
		$sql = 'SELECT f.* FROM Folders f WHERE f.caseID=:caseID AND name=:folderName';
		return $this->db->row( $sql, ['caseID'=>(int)$caseID, 'folderName'=>$folderName] );
	}

   protected function getDepoCreatedBy($depoID)
   {
      return $this->db->col('SELECT createdBy FROM Depositions WHERE ID=?',array($depoID));
   }

	/**
	 * @param bigint $depoID
	 * @param bigint $createdBy
	 * @param string $name
	 * @return array
	 */
	public function getExhibitFolder( $depoID, $createdBy=NULL, $name=NULL )
	{
		$depoID = (int)$depoID;
//		$createdBy = (int)$createdBy;
//		if( !$name ) {
//			$name = $this->config->logic['exhibitFolderName'];
//		}
//		if( !$createdBy ) {
//			$createdBy = $this->getDepoCreatedBy( $depoID );
//		}
		//ED-1793 -- Support for WitnessPrep
		$sql = 'SELECT * FROM Folders WHERE depositionID=:depoID AND class="Exhibit"';
		return $this->db->row( $sql, ['depoID'=>$depoID] );
	}

	public function getExhibitFolders( $depoID ) {
		$depoID = (int)$depoID;
		$sql = 'SELECT * FROM Folders WHERE depositionID=:depoID AND class="Exhibit"';
		return $this->db->rows( $sql, ['depoID'=>$depoID] );
	}

	public function getCourtesyCopyFolder( $depoID )
	{
		return $this->db->row( "SELECT * FROM Folders WHERE depositionID=:depoID AND class='CourtesyCopy'", array( 'depoID'=>$depoID ) );
	}

	/**
	 *
	 * @param bigint $depoID
	 * @param bigint $createdBy
	 * @param string $name
	 * @return ClickBlocks\DB\Folders
	 */
	public function getExhibitFolderObject( $depoID, $createdBy=NULL, $name=NULL )
	{
		$depoID = (int)$depoID;
		$createdBy = (int)$createdBy;
//		if( !$name ) {
//			$name = $this->config->logic['exhibitFolderName'];
//		}
//		if (!$createdBy) {
//			$createdBy = $this->getDepoCreatedBy( $depoID );
//		}
		return $this->getObjectByQuery( ['depositionID'=>$depoID, 'class'=>self::FOLDERS_EXHIBIT] );
	}

  public function getExhibitFoldersObjects($depoID, $createdBy = null)
  {
      if (!$createdBy) {
         $createdBy = $this->getDepoCreatedBy($depoID);
      }
      return $this->getObjectsByQuery(array('depositionID'=>$depoID, 'class'=>'Exhibit', 'createdBy'=>$createdBy));
	}

  public function getPersonalFolder($depoID, $userID)
  {
    return $this->db->row("SELECT * FROM Folders WHERE depositionID = ? AND createdBy = ? AND class='Personal'",array($depoID, $userID));
	}

  /**
    *
    * @param type $depoID
    * @param type $userID
    * @return \ClickBlocks\DB\Folders
    */
  public function getPersonalFolderObject($depoID, $userID)
  {
    return $this->getObjectByQuery(array('depositionID'=>$depoID, 'createdBy'=>$userID, 'class'=>'Personal'));
  }

  public function getPersonalFolderForUserInDeposition( $depoID, $userID )
  {
//	  $sql = 'SELECT depositionID FROM Folders WHERE depositionID IN (SELECT ID FROM Depositions WHERE ID = :depoID OR parentID = :depoID) AND createdBy = :userID LIMIT 1';
//	  $depositionID = $this->db->col( $sql, array( 'depoID' => $depoID, 'userID' => $userID ) );
//	  return $this->getObjectByQuery( array( 'depositionID' => $depositionID, 'createdBy' => $userID, 'isPrivate' => '1' ) );
	  return $this->getObjectByQuery( ['depositionID'=>$depoID, 'createdBy'=>$userID, 'class'=>'Personal'] );
  }

	/**
	 *
	 * @param type $depoID
	 * @param type $userID
	 * @return \ClickBlocks\DB\Folders
	*/
	public function createPersonalFolder( $depoID, $userID )
	{
		$myPersonalFolder = $this->getPersonalFolderObject( $depoID, $userID );
		if( !$myPersonalFolder->ID ) {
			$myPersonalFolder->depositionID = $depoID;
			$myPersonalFolder->createdBy = $userID;
			$myPersonalFolder->class = IEdepoBase::FOLDERS_PERSONAL;
			$myPersonalFolder->name = $this->config->logic['personalFolderName'];
			$myPersonalFolder->insert();
		}
		return $myPersonalFolder;
	}

	public function getWitnessAnnotationsFolder( $depoID, $userID )
	{
		return $this->getObjectByQuery( ['depositionID'=>$depoID, 'createdBy'=>$userID, 'class'=>'WitnessAnnotations'] );
	}

  public function checkUserFolderID($depoID, $userID, $folderID)
  {
    return $this->db->rows('SELECT ID FROM Folders WHERE depositionID = ? AND createdBy = ? AND ID = ?',array($depoID, $userID, $folderID));
	}

	/**
	 *
	 * @param int $userID
	 * @param int $folderID
	 * @return bool
	 */
	public function checkPermissionToFolder( $userID, $folderID ) {
		$folder = new Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			return FALSE;
		}
		if( $folder->isCaseFolder() ) {
			return $this->hasPermissionToCaseFolder( $userID, $folderID );
		}
		$sql = "SELECT f.ID FROM Folders f
			INNER JOIN Depositions d ON (d.ID=f.depositionID)
			INNER JOIN Cases c ON (c.ID=d.caseID)
			INNER JOIN Clients cl ON (cl.ID=c.clientID)
			LEFT JOIN Users u ON (u.clientID=c.clientID AND u.ID=:userID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND (da.userID=u.ID OR da.ID=:userID))
			WHERE f.ID=:folderID
			AND d.deleted IS NULL
			AND c.deleted IS NULL
			AND (
				((u.typeID='C' OR u.clientAdmin=1) AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='Trusted'))
				OR (((cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID) OR d.class='Demo' OR d.class='WPDemo' OR d.class='Demo Trial') AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='Trusted' OR f.class='WitnessAnnotations'))
				OR (f.createdBy=u.ID AND (f.class='Folder' OR f.class='Personal'))
				OR (da.userID=u.ID AND (f.class='Exhibit' OR f.class='Transcript'))
				OR (da.role='WM' AND (f.class='Exhibit' OR f.class='Trusted' OR (f.class='WitnessAnnotations' AND f.createdBy=d.ownerID)))
				OR (da.role='W' AND (f.class='Exhibit'))
				OR (da.role='G' AND (f.class='CourtesyCopy'))
			)";
//		Debug::ErrorLog( print_r( [$sql, ['userID'=>$userID, 'folderID'=>$folderID]], TRUE ) );
		return (int)$this->db->col( $sql, ['userID'=>$userID, 'folderID'=>$folderID] );
	}

	/**
	 * @param int $userID
	 * @param int $folderID
	 * @return bool
	 */
	protected function hasPermissionToCaseFolder( $userID, $folderID ) {
		$sql = "SELECT f.ID FROM Folders f
			INNER JOIN SessionCaseFolders cf ON (cf.folderID=f.ID)
			INNER JOIN Depositions d ON (d.ID=cf.sessionID)
			INNER JOIN Cases c ON (c.ID=d.caseID)
			INNER JOIN Clients cl ON (cl.ID=c.clientID)
			LEFT JOIN Users u ON (u.clientID=c.clientID AND u.ID=:userID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND (da.userID=u.ID OR da.ID=:userID))
			WHERE f.ID=:folderID
			AND d.deleted IS NULL
			AND c.deleted IS NULL
			AND (
				((u.typeID='C' OR u.clientAdmin=1) AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='Trusted'))
				OR (((cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID) OR d.class='Demo' OR d.class='WPDemo' OR d.class='Demo Trial') AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='Trusted' OR f.class='WitnessAnnotations'))
				OR (da.role='WM' AND (f.class='Exhibit' OR f.class='Trusted' OR (f.class='WitnessAnnotations' AND f.createdBy=d.ownerID)))
			)";
//		Debug::ErrorLog( print_r( [$sql, ['userID'=>$userID, 'folderID'=>$folderID]], TRUE ) );
		return (int)$this->db->col( $sql, ['userID'=>(int)$userID, 'folderID'=>(int)$folderID] );
	}

	public function checkPermissionToCaseFolder( $userID, $folderID ) {
		$sql = "SELECT f.ID FROM Folders f
			INNER JOIN Cases c ON (c.ID=f.caseID)
			INNER JOIN Clients cl ON (cl.ID=c.clientID)
			INNER JOIN Users u ON (u.clientID=c.clientID AND u.ID=:userID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			LEFT JOIN Depositions d ON (d.caseID=c.ID)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			WHERE f.ID=:folderID
			AND c.deleted IS NULL
			AND (
				((u.typeID='C' OR u.clientAdmin=1 OR c.class='Demo') AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='Trusted'))
				OR ((cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID) AND (f.class='Exhibit' OR f.class='Transcript' OR f.class='Trusted'))
			)
			GROUP BY f.ID";
//		Debug::ErrorLog( print_r( [$sql, ['userID'=>$userID, 'folderID'=>$folderID]], TRUE ) );
		return (int)$this->db->col( $sql, ['userID'=>(int)$userID, 'folderID'=>(int)$folderID] );
	}

	public function getFolderIDbyFileID($fileID)
	{
		$sql = "SELECT folderID FROM Files WHERE ID = :fileID";
		return $this->db->col( $sql, ['fileID'=>$fileID] );
	}

	/**
	 * @param int $depoID
	 * @param int $createdBy
	 * @return array
	 */
	public function getFolderNameMapByDepo( $depoID, $createdBy ) {
		$sql = 'SELECT f.name,f.ID FROM Folders f
			LEFT JOIN SessionCaseFolders cf ON (cf.folderID=f.ID AND cf.sessionID=:depoID)
			WHERE (f.depositionID=:depoID OR f.ID=cf.folderID) AND createdBy=:userID';
//		Debug::ErrorLog( print_r( ['getFolderNameMapByDepo', $sql, ['depoID' => (int)$depoID, 'userID' => (int)$createdBy]], TRUE ) );
		return $this->db->couples( $sql, ['depoID' => (int)$depoID, 'userID' => (int)$createdBy] );
	}

	public function getCaseFolderNameMapByCase( $caseID ) {
		$sql = 'SELECT name,ID FROM Folders WHERE caseID=:caseID';
		return $this->db->couples( $sql, ['caseID'=>(int)$caseID] );
	}

	public function getCaseFolderIDs( $caseID ) {
		$sql = 'SELECT ID FROM Folders WHERE caseID=:caseID';
		return $this->db->cols( $sql, [ 'caseID' => (int)$caseID ] );
	}

	public function checkUniqueFolder( $depoID, $folderName ) {
		$sql = 'SELECT COUNT(f.ID) FROM Folders f
			LEFT JOIN SessionCaseFolders cf ON (cf.folderID=f.ID AND cf.sessionID=:depoID)
			WHERE (f.depositionID=:depoID OR cf.sessionID IS NOT NULL) AND f.name=:name';
		return !($this->db->col( $sql, ['depoID'=>(int)$depoID,'name'=>$folderName] ) > 0);
	}

	public function checkUniqueCaseFolder( $caseID, $folderName ) {
		return !($this->db->col( 'SELECT COUNT(*) FROM Folders WHERE caseID=:caseID AND name=:name', ['caseID'=>(int)$caseID,'name'=>$folderName] ) > 0);
	}

	public function getFoldersByUserID($depoID, $userID=NULL, $isTrusted=FALSE, $withNumFiles=FALSE, $userType=null )
	{
		return $this->getDepositionFoldersForUser( $depoID, $userID );
//		$ord = new OrchestraDepositions;
//		if (!$isTrusted && $userID) $isTrusted = $ord->isTrustedUser($userID, $depoID);
//		$createdBy = $this->getDepoCreatedBy( $depoID );
//		//$ownerID = $this->getDepoOwnerID( $depoID );
//		$ownerWhere = "f.class='Exhibit'";
//		$userWhere = '0';
//		$whereCourtesyCopy = '';
//		$bv = array( 'depoID' => $depoID, 'createdBy' => $createdBy );
//		if ($userID) {
//			$userWhere = 'f.createdBy=:userID';
//			$bv['userID'] = $userID;
//			if ($isTrusted) {
//				$ownerWhere .= " OR (f.class != 'Personal' AND f.class != 'WitnessAnnotations')";
//				$whereCourtesyCopy = " AND f.class != 'CourtesyCopy'";
//			}
//		} else {
//			//guest vs witness
//			//Debug::ErrorLog( print_r( $userType, true ) );
//			if( !$userType || !in_array( $userType, ['G','W'], TRUE ) ) {
//				//throw new \LogicException( __FUNCTION__ . ": Invalid userType ({$userType}) on Line: " . __LINE__ );
//			} elseif( $userType === 'G' ) {
//				$userWhere = " f.class = 'CourtesyCopy'";
//				$ownerWhere = 0;
//			}
//		}
//		$sel = 'SELECT f.* ';
//		$from = ' FROM Folders f ';
//		$where = 'WHERE f.depositionID=:depoID AND (' . $userWhere . ' OR (f.createdBy=:createdBy AND (' . $ownerWhere . '))) ' . $whereCourtesyCopy . ' ';
//		if ($withNumFiles)
//		{
//			$sel .= ',COUNT(fi.ID) AS numFiles ';
//			$from .= ' LEFT JOIN Files fi ON (fi.folderID=f.ID) ';
//			$where .= ' GROUP BY f.ID';
//		}
//		//Debug::ErrorLog( $sel.$from.$where );
//		//Debug::ErrorLog(print_r( $bv, true ) );
//		return $this->db->rows($sel.$from.$where, $bv);
	}
	
	public function getDepositionFoldersForUser( $depositionID, $userID=NULL, $allCaseFolders=FALSE ) {
//		Debug::ErrorLog( "getDepositionFoldersForUser; depositionID:{$depositionID}, userID:{$userID}" );
		$folders = [];
		$role = self::DEPOSITIONATTENDEE_ROLE_GUEST;
		$orcUsers = new OrchestraUsers();
		$isTrustedUser = FALSE;
		$clientAdminID = (int)$orcUsers->getClientAdminIDForUser( $userID );
		if( $userID ) {
			$orcDepo = new OrchestraDepositions();
			$isTrustedUser = $orcDepo->isTrustedUser( $userID, $depositionID );
			$orchDA = new OrchestraDepositionAttendees();
			$attendee = $orchDA->getAttendeeForDepoID( $depositionID, $userID );
			if( !$attendee ) {
				if( $isTrustedUser ) {
					$role = self::DEPOSITIONATTENDEE_ROLE_MEMBER;
				} else {
					return $folders;
				}
			} else {
				$role = $attendee['role'];
			}
		}
		$sql = NULL;
		$params = [];
		switch( $role ):
			case self::DEPOSITIONATTENDEE_ROLE_MEMBER:
				$params = ['depoID'=>(int)$depositionID, 'userID'=>(int)$userID];
				if( $isTrustedUser ) {
					$params['csUserID'] = $clientAdminID;
					$params['courtClient'] = self::CLIENT_TYPE_COURTCLIENT;
					$allCF = ($allCaseFolders ? '' : ' AND (f.caseID IS NULL OR (f.caseID IS NOT NULL AND cf.sessionID IS NOT NULL))');
					$sql = 'SELECT f.*, COUNT(fi.ID) AS numFiles, IFNULL(cs.sortPos,f.ID) as sortPos, cf.sessionID as linked FROM Folders f
						INNER JOIN Depositions d ON (d.ID=:depoID)
						INNER JOIN Users u ON (u.ID=:userID)
						INNER JOIN Clients cl ON (cl.ID=u.clientID)
						LEFT JOIN SessionCaseFolders cf ON (cf.folderID=f.ID AND cf.sessionID=d.ID)
						LEFT JOIN Files fi ON (fi.folderID=f.ID)
						LEFT JOIN UserCustomSortFolders cs ON (cs.folderID=f.ID AND cs.userID=:csUserID)
						WHERE (f.depositionID=d.ID OR f.caseID=d.caseID)' . $allCF . '
						AND ((f.class="' . self::FOLDERS_EXHIBIT . '" OR f.class="' . self::FOLDERS_TRANSCRIPT . '" OR f.class="' . self::FOLDERS_TRUSTED . '")
							OR (f.createdBy=:userID AND (f.class="' . self::FOLDERS_FOLDER . '" OR f.class="' . self::FOLDERS_PERSONAL . '" OR f.class="' . self::FOLDERS_WITNESSANNOTATIONS . '")))
						AND (cl.typeID != :courtClient OR f.depositionID IS NOT NULL)
						GROUP BY f.ID';
				} else {
					$sql = 'SELECT f.*, COUNT(fi.ID) AS numFiles, IFNULL(cs.sortPos,f.ID) as sortPos FROM Folders f
						LEFT JOIN Files fi ON (fi.folderID=f.ID)
						LEFT JOIN UserCustomSortFolders cs ON (cs.folderID=f.ID AND cs.userID=:userID)
						WHERE f.depositionID=:depoID
						AND ((f.class="' . self::FOLDERS_EXHIBIT . '" OR f.class="' . self::FOLDERS_TRANSCRIPT . '")
							OR (f.createdBy=:userID AND (f.class="' . self::FOLDERS_FOLDER . '" OR f.class="' . self::FOLDERS_PERSONAL . '" OR f.class="' . self::FOLDERS_WITNESSANNOTATIONS . '")))
						GROUP BY f.ID';
				}
				break;
			case self::DEPOSITIONATTENDEE_ROLE_GUEST:
				$sql = 'SELECT f.*, COUNT(fi.ID) AS numFiles, f.ID as sortPos FROM Folders f
					LEFT JOIN Files fi ON (fi.folderID=f.ID)
					WHERE f.depositionID=:depoID
					AND f.class="' . self::FOLDERS_COURTESYCOPY . '"
					GROUP BY f.ID';
				$params = ['depoID'=>(int)$depositionID];
				break;
			case self::DEPOSITIONATTENDEE_ROLE_WITNESS:
				$sql = 'SELECT f.*, COUNT(fi.ID) AS numFiles, f.ID as sortPos FROM Folders f
					LEFT JOIN Files fi ON (fi.folderID=f.ID)
					WHERE f.depositionID=:depoID
					AND f.class="' . self::FOLDERS_EXHIBIT . '"
					GROUP BY f.ID';
				$params = ['depoID'=>(int)$depositionID];
				break;
			case self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER:
				$sql = 'SELECT f.*, COUNT(fi.ID) AS numFiles, f.ID as sortPos FROM Folders f
					LEFT JOIN SessionCaseFolders cf ON (cf.folderID=f.ID AND cf.sessionID=:depoID)
					LEFT JOIN Depositions d ON (f.depositionID=d.ID)
					LEFT JOIN Files fi ON (fi.folderID=f.ID)
					WHERE (f.depositionID=:depoID OR cf.sessionID=:depoID)
					AND (f.class="' . self::FOLDERS_EXHIBIT . '" OR f.class="' . self::FOLDERS_TRUSTED . '" OR (d.ownerID=f.createdBy AND f.class="' . self::FOLDERS_WITNESSANNOTATIONS . '"))
					GROUP BY f.ID';
				$params = ['depoID'=>(int)$depositionID];
				break;
		endswitch;
		if ( !$sql ) {
			return $folders;
		}
		//Debug::ErrorLog( print_r( [$sql, $params], TRUE ) );
		return $this->db->rows( $sql, $params );
	}

	public function getFiles_API( $folderID, $sort=NULL, $pageSize=NULL, $page=NULL )
	{
		$folderID = (int)$folderID;
		$sql = 'SELECT f.ID, f.name, f.created, f.createdBy, f.isExhibit, f.isPrivate, su.ID as suID, su.firstName, su.lastName, su.email
			FROM Files AS f
			LEFT JOIN Users AS su ON (su.ID=IFNULL(f.sourceUserID,f.createdBy))
			WHERE folderID=:folderID';
		$bv = ['folderID'=>$folderID];
		if( $sort ) {
			if( is_array( $sort ) ) {
				$dir = ($sort['dir'] == 'DESC') ? 'DESC' : 'ASC';
				$sort = $sort['by'];
			} else {
				$dir = 'ASC';
			}
			if( $sort ) {
				$sql .= ' ORDER BY f.' . $sort . ' ' . $dir . ',f.ID ';
			}
		}
		if( $pageSize ) {
			$sql .= ' LIMIT ' . ((int)$pageSize * (int)$page) . ',' . (int)$pageSize;
		}
		$rows = $this->db->rows($sql, $bv);
		foreach( $rows as &$file ) {
			$file['byUser'] = ['ID'=>$file['suID'], 'firstName'=>$file['firstName'], 'lastName'=>$file['lastName'], 'email'=>$file['email']];
			unset( $file['suID'], $file['firstName'], $file['lastName'], $file['email'] );
		}
		return $rows;
	}

	public function getFilesForUser( $folderID, $userID=NULL, $sort=NULL, $pageSize=NULL, $page=NULL )
	{
		$folderID = (int)$folderID;
		$userID = (int)$userID;
		$sql = 'SELECT f.ID, f.name, f.created, f.createdBy, f.isExhibit, f.isPrivate, su.ID as suID, su.firstName, su.lastName, su.email, IFNULL(cs.sortPos,f.ID) as sortPos
			FROM Files AS f
			LEFT JOIN Users AS su ON (su.ID=IFNULL(f.sourceUserID,f.createdBy))
			LEFT JOIN UserCustomSortFiles cs ON (cs.fileID=f.ID AND cs.userID=:userID)
			WHERE folderID=:folderID';
		$bv = ['folderID'=>$folderID, 'userID'=>$userID];
		if( $sort ) {
			if( is_array( $sort ) ) {
				$dir = ($sort['dir'] == 'DESC') ? 'DESC' : 'ASC';
				$sort = $sort['by'];
			} else {
				$dir = 'ASC';
			}
			if( $sort ) {
				$sql .= ' ORDER BY f.' . $sort . ' ' . $dir . ',f.ID ';
			}
		}
		if( $pageSize ) {
			$sql .= ' LIMIT ' . ((int)$pageSize * (int)$page) . ',' . (int)$pageSize;
		}
		$rows = $this->db->rows($sql, $bv);
		foreach( $rows as &$file ) {
			$file['byUser'] = ['ID'=>$file['suID'], 'firstName'=>$file['firstName'], 'lastName'=>$file['lastName'], 'email'=>$file['email']];
			unset( $file['suID'], $file['firstName'], $file['lastName'], $file['email'] );
		}
		return $rows;
	}

	public function getFolderFilesForAttendee( $userID, $folderID )
	{
		$userID = (int)$userID;
		$folderID = (int)$folderID;
		if( !$userID || !$folderID ) {
			Debug::ErrorLog( "getFolderFilesForAttendee; userID: {$userID}, folderID: {$folderID} -- invalid parameters" );
			return FALSE;
		}
		if( !$this->checkPermissionToFolder( $userID, $folderID ) ) {
			Debug::ErrorLog( "getFolderFilesForAttendee; userID: {$userID}, folderID: {$folderID} -- permission denied" );
			return FALSE;
		}
		$sql = 'SELECT f.ID, f.folderID, f.name, f.created, f.createdBy, f.isExhibit, f.isPrivate, u.ID as userID, u.firstName, u.lastName, u.email, IFNULL(cs.sortPos,f.ID) as sortPos
			FROM Files AS f
			LEFT JOIN Users AS u ON (u.ID=IFNULL(f.sourceUserID,f.createdBy))
			LEFT JOIN UserCustomSortFiles cs ON (cs.fileID=f.ID AND cs.userID=:userID)
			WHERE folderID=:folderID';
//		Debug::ErrorLog( print_r( [$sql,$userID,$folderID], TRUE ) );
		$rows = $this->db->rows( $sql, ['userID'=>$userID, 'folderID'=>$folderID] );
		if( is_array( $rows ) ) {
			foreach( $rows as &$file ) {
				$file['byUser'] = ['ID'=>$file['userID'], 'firstName'=>$file['firstName'], 'lastName'=>$file['lastName'], 'email'=>$file['email']];
				unset( $file['userID'], $file['firstName'], $file['lastName'], $file['email'] );
			}
		}
		return $rows;
	}

	public function getFoldersForUser( $depoID, $userID=NULL, $withFiles=FALSE, $userType=NULL )
	{
		$folders = $this->getDepositionFoldersForUser( $depoID, $userID );
		foreach( $folders as &$folder ) {
			$folder = array_intersect_key( $folder, ['ID'=>1,'depositionID'=>1,'name'=>1,'class'=>1,'created'=>1,'createdBy'=>1,'numFiles'=>1,'lastModified'=>1, 'sortPos'=>1] );
			if( $withFiles ) {
			   $folder['files'] = $this->getFiles_API( $folder['ID'] );
			}
		}
		return $folders;
	}

   public function getFoldersWithFilesForUser( $depoID, $userID )
	{
		return $this->getFoldersForUser( $depoID, $userID, true );
	}

  public function getAdditionalInfoForPath($createdBy, $depoID)
  {
    return $this->db->row('SELECT u.clientID,d.caseID FROM Users u INNER JOIN Depositions d ON (u.ID=? AND d.ID=?)', array($createdBy, $depoID));
  }

  public function isExist($depoID, $folderID)
  {
  	return $this->db->col('SELECT ID FROM Folders WHERE depositionID = ? AND ID= ? ', array($depoID, $folderID)) !== false;
  }

	public function getTranscriptFolder( $depoID, $createdBy=null, $name=null )
	{
		if (!$name)
		{
			$name = $this->config->logic['transcriptFolderName'];
		}
		if (!$createdBy)
		{
			$createdBy = $this->getDepoCreatedBy( $depoID );
		}
		return $this->db->row( 'SELECT * FROM Folders WHERE depositionID=? AND createdBy=? AND class=? AND name=?', [$depoID,$createdBy,Folders::CLASS_TRANSCRIPT,$name] );
	}

	/**
	 * @return \ClickBlocks\DB\Folders
	 */
	public function getTranscriptFolderObject($depoID, $createdBy = null, $name = null)
	{
		if (!$name)
		{
			$name = $this->config->logic['transcriptFolderName'];
		}
		if (!$createdBy)
		{
			$createdBy = $this->getDepoCreatedBy($depoID);
		}
		return $this->getObjectByQuery( ['depositionID'=>$depoID, 'name'=>$name, 'class'=>Folders::CLASS_TRANSCRIPT, 'createdBy'=>$createdBy] );
	}

	public function getTranscriptFoldersObjects($depoID, $createdBy = null)
	{
		if (!$createdBy)
		{
			$createdBy = $this->getDepoCreatedBy($depoID);
		}
		return $this->getObjectsByQuery( ['depositionID'=>$depoID, 'class'=>Folders::CLASS_TRANSCRIPT, 'createdBy'=>$createdBy] );
	}

	public function getSortPosition( $folderID, $userID )
	{
		$folderID = (int)$folderID;
		$sql = "SELECT IFNULL(sortPos,{$folderID}) as sortPos FROM UserCustomSortFolders WHERE folderID=:folderID AND userID=:userID";
		return $this->db->col( $sql, ['folderID'=>$folderID, 'userID'=>$userID] );
	}

	public function getCaseExhibitsFolderID( $sessionID ) {
		$sql = 'SELECT f.ID FROM Depositions d
			INNER JOIN Folders f ON (f.caseID=d.caseID AND f.class="' . self::FOLDERS_EXHIBIT . '")
			WHERE d.ID=:sessionID';
		return (int)$this->db->col( $sql, ['sessionID'=>(int)$sessionID] );
	}

	public function getCaseTranscriptsFolderID( $sessionID ) {
		$sql = 'SELECT f.ID FROM Depositions d
			INNER JOIN Folders f ON (f.caseID=d.caseID AND f.class="' . self::FOLDERS_TRANSCRIPT . '")
			WHERE d.ID=:sessionID';
		return (int)$this->db->col( $sql, ['sessionID'=>(int)$sessionID] );
	}

	public function getFoldersForFile( $fileID ) {
		$sql = '(SELECT fl.* FROM Files fi
			INNER JOIN Folders fl ON (fl.ID=fi.folderID)
			WHERE fi.ID=:fileID)
			UNION
			(SELECT cf.* FROM Files fi
			INNER JOIN Folders fl ON (fl.ID=fi.folderID AND (fl.class=:exhibit OR fl.class=:transcript))
			INNER JOIN Depositions d ON (d.ID=fl.depositionID)
			INNER JOIN Cases c ON (c.ID=d.caseID)
			INNER JOIN Folders cf ON (cf.caseID=c.ID AND cf.class=fl.class)
			WHERE fi.ID=:fileID)';
		return $this->db->rows( $sql, ['fileID' => (int)$fileID, 'exhibit' => self::FOLDERS_EXHIBIT, 'transcript' => self::FOLDERS_TRANSCRIPT] );
	}

	/**
	 * @param int $userID
	 * @param int $fileID
	 * @return boolean
	 */
	public function checkPermissionToFile( $userID, $fileID ) {
		$user = new Users( $userID );
		if( !$user || !$user->ID || $user->ID != $userID ) {
			Debug::ErrorLog( "Folders::checkPermissionToFile; user not found by ID: {$userID}" );
			return FALSE;
		}
		$file = new Files( $fileID );
		if( !$file || !$file->ID || $file->ID != $fileID ) {
			Debug::ErrorLog( "Folders::checkPermissionToFile; file not found by ID: {$fileID}" );
			return FALSE;
		}
		$owner = new Users( $file->createdBy );
		if( !$owner || !$owner->ID || $owner->ID != $file->createdBy ) {
			Debug::ErrorLog( "Folders::checkPermissionToFile; owner not found by ID: {$file->createdBy}" );
			return FALSE;
		}
		if( $owner->clientID != $user->clientID ) {
			Debug::ErrorLog( "Folders::checkPermissionToFile; file owner client mismatch: {$user->clientID}/{$owner->clientID}" );
			return FALSE;
		}
		$folders = $this->getFoldersForFile( $fileID );
		if( !$folders || !is_array( $folders ) ) {
			Debug::ErrorLog( "Folders::checkPermissionToFile; no folders for fileID: {$fileID}" );
			return FALSE;
		}
		foreach( $folders as $fInfo ) {
			$folder = new Folders();
			$folder->assign( $fInfo );
			$hasAccess = (int)($folder->ID == $this->checkPermissionToFolder( $userID, $folder->ID ));
			$bHasAccess = $hasAccess ? 'TRUE' : 'FALSE';
			Debug::ErrorLog( "Folders::checkPermissionToFile; hasAccess to {$fileID} in Folder: {$folder->ID}, {$bHasAccess}" );
			if( $hasAccess ) {
				return $hasAccess;
			}
		}
	}

	public function isDemo( $folderID ) {
		$sql = 'SELECT IF((cf.class=:demoCase OR c.class=:demoCase OR d.class=:demoDepo OR d.class=:demoWP OR d.class=:demoTrial),1,0) as isDemo
			FROM Folders f
			LEFT JOIN Cases cf ON (cf.ID=f.caseID)
			LEFT JOIN Depositions d ON (d.ID=f.depositionID)
			LEFT JOIN Cases c ON (c.ID=d.caseID)
			WHERE f.ID=:folderID';
		$bind = [
			'folderID' => (int)$folderID,
			'demoCase' => IEdepoBase::CASE_CLASS_DEMO,
			'demoDepo' => IEdepoBase::DEPOSITION_CLASS_DEMO,
			'demoWP' => IEdepoBase::DEPOSITION_CLASS_WPDEMO,
			'demoTrial' => IEdepoBase::DEPOSITION_CLASS_DEMOTRIAL
		];
		//Debug::ErrorLog( print_r( ['Folders::isDemo', $sql, $bind], TRUE ) );
		return (bool)$this->db->col( $sql, $bind );
	}
}
