<?php

namespace ClickBlocks\DB;

use ClickBlocks\MVC\Edepo,
	ClickBlocks\Debug;

class OrchestraCases extends OrchestraEdepo {

	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\Cases' );
	}

	public static function getBLLClassName() {
		return '\ClickBlocks\DB\Cases';
	}

	public function getWidgetCaseData( $type, array $params ) {
		$bind = [
			'userID' => (int)$params['userID'],
			'demo'=>Edepo::CASE_CLASS_DEMO,
			'clientAdmin'=>Edepo::USER_TYPE_CLIENT
		];
		$from = 'FROM Cases c
			INNER JOIN Users u ON (u.ID=:userID AND u.clientID=c.clientID)
			INNER JOIN Clients cl ON (cl.ID=u.clientID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			LEFT JOIN Depositions d ON (d.caseID=c.ID AND d.deleted IS NULL)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			LEFT JOIN DepositionAttendees dat ON (dat.depositionID=d.ID AND dat.userID=u.ID)
			WHERE c.clientID=u.clientID
			AND c.deleted IS NULL AND d.deleted IS NULL
			AND ((u.typeID=:clientAdmin OR u.clientAdmin=1 OR c.class=:demo) OR cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID OR dat.userID=u.ID)
			GROUP BY c.ID';
		switch( $type ) {
			case 'count':
				$select = 'SELECT COUNT(c.ID)';
				$sql = "{$select} {$from}";
				return $this->db->col( $sql, $bind );
			case 'rows':
				$bind['courtClient'] = Edepo::CLIENT_TYPE_COURTCLIENT;
				$select = 'SELECT c.ID,c.number,c.name,c.created,COUNT(d.ID) as depositions,
					SUM(IF((((cl.typeID=:courtClient AND (u.typeID=:clientAdmin OR u.clientAdmin)) OR c.class=:demo) OR (cm.userID=u.ID AND d.ID) OR d.ownerID=u.ID OR das.userID=u.ID OR dat.userID=u.ID),1,0)) as sessionCount,
					SUM(CASE WHEN d.statusID = \'I\' THEN 1 ELSE 0 END) as activeSessionCount,
					IF((u.typeID=:clientAdmin OR u.clientAdmin=1),1,0) as isAdmin,
					IF(cm.userID=u.ID,1,0) as isManager,
					IF((SUM(IF((((cl.typeID=:courtClient AND (u.typeID=:clientAdmin OR u.clientAdmin)) OR c.class=:demo) OR cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID OR dat.userID=u.ID),1,0)) >= 1),1,0) as canView';
				$sql = "{$select} {$from}";
				//Debug::ErrorLog( print_r( ['OrchestraCases::getWidgetCaseData', $sql, $bind], TRUE ) );
				return $this->db->rows( $sql, $bind );
		}
	}

   //Custom function to sort the rows using natural order.
   private function caseSort($row1, $row2)
   {
	   $number1 = $row1['number'];
	   $number2 = $row2['number'];

	   if ($this->reverse) {
		   return strnatcmp($number2, $number1);
	   }
	   return strnatcmp($number1, $number2);
   }

   //For case numbers, we want to do a natural sort on the value, so we need to have a custom call to get the data in the correct order.
//   public function caseDataByNumber($params, $w)
//   {
//		$sql = 'SELECT c.ID, c.number FROM Cases c ' . $w;
//		$rows = $this->db->rows($sql);
//		// If there are no rows, then don't do any extra filtering.
//		if (count($rows) === 0) {
//			return $w;
//		}
//
//		// Sort using natual order.
//		$this->reverse = ($params['sortBy'] > 0) ? true : false;
//		usort($rows, array($this, 'caseSort'));
//
//		// Now pull out the id's for the page we want.
//		$ids = array();
//		foreach ($rows as $row) {
//			$ids[] = $row['ID'];
//		}
//		$idList = implode(',', $ids);
//		// Limit the result to the case ids in the paged list and return the results in the same order.
//		$w .= ' AND c.ID IN (' . $idList . ') ORDER BY FIELD(c.ID, ' . $idList . ')';
//
//		return $w;
//   }

  public function getWidgetReportsCasesData($type, array $params)
  {
//SELECT c.*, cl.name, CONCAT(cmu.firstName, cmu.lastName) as CaseManager, 1 as Active FROM Cases AS c INNER JOIN Clients cl ON c.clientID=cl.ID LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID) LEFT JOIN Users cmu ON (cmu.ID=cm.userID) GROUP BY c.ID
      $select = 'c.*, (SELECT CONCAT(u.firstName," ",u.lastName) FROM CaseManagers cm INNER JOIN Users u ON (u.ID=cm.userID) WHERE cm.caseID=c.ID LIMIT 1) AS CaseManager, 1 as Active';
      $where = array('c.clientID='.(int)$params['clientID']);
      if (strlen($params['active'])) $where[] = (int)$params['active'];
      $from = 'Cases c ';
      $orderMap = array(
          1=>'c.number',
          2=>'c.name',
          3=>'CaseManager',
          4=>'Active',
      );
      return $this->_getWidgetData($type, $params, $select, $from, $where, '', $orderMap);
   }

	public function checkCaseAdmin( $caseID, $userID ) {
		$sql = 'SELECT COUNT(c.ID) FROM Cases c
				INNER JOIN Users u ON (u.ID=:userID)
				INNER JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
				WHERE c.ID=:caseID AND c.clientID=u.clientID AND cm.caseID=c.ID AND cm.userID=u.ID';
		return ($this->db->col( $sql, ['caseID'=>$caseID, 'userID'=>$userID] ) > 0);
	}

  public function getCasesByUser($userID)
  {
       return $this->db->rows('SELECT DISTINCT c.* FROM Cases c LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=:userID) WHERE c.deleted IS NULL AND (c.createdBy = :userID OR cm.caseID IS NOT NULL OR (c.ID IN (SELECT DISTINCT d.caseID FROM Depositions d LEFT JOIN DepositionAssistants da ON da.depositionID = d.ID WHERE d.deleted IS NULL AND (d.ownerID = :userID OR da.userID = :userID))))',array(':userID'=>$userID));
   }

	public function getCaseAdmin($userID, $clientID)
	{
		$userID = (int)$userID;
		$clientID = (int)$clientID;
		$sql = "SELECT c.ID FROM Cases c
			INNER JOIN Users u ON (u.ID=:userID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			WHERE c.clientID=u.clientID
			AND ((c.createdBy=u.ID OR cm.userID=u.ID)
			OR (u.typeID=:clientAdmin OR u.clientAdmin=1))";
		return $this->db->rows( $sql, ['userID'=>$userID, 'clientAdmin'=>Edepo::USER_TYPE_CLIENT] );
	}

  public function getCaseContainDepoAssistant($userID)
  {
        return $this->db->rows('SELECT d.caseID FROM Depositions d INNER JOIN DepositionAssistants da ON da.depositionID = d.ID WHERE d.deleted IS NULL AND da.userID = ?', array($userID));
    }

  public function getCaseContainDepoAttendee($userID)
  {
        return $this->db->rows('SELECT d.caseID FROM Depositions d INNER JOIN DepositionAttendees da ON da.depositionID = d.ID WHERE d.deleted IS NULL AND da.userID = ?', array($userID));
    }

	public function getCaseContainDepoOwner( $userID )
	{
		$userID = (int)$userID;
		$clientID = (int)$clientID;
		$sql = "SELECT d.caseID FROM Depositions d
				INNER JOIN Cases c ON (c.ID=d.caseID)
				WHERE d.deleted IS NULL AND d.ownerID=:userID";
		return $this->db->rows( $sql, ['userID'=>$userID] );
	}

  public function getDemoCasesForClient( $clientID )
  {
    $userID = (int)$userID;
    $clientID = (int)$clientID;
    $sql = "SELECT ID FROM Cases WHERE deleted IS NULL AND class='Demo' AND clientID=:clientID";
    return $this->db->rows( $sql, ['clientID'=>$clientID] );
  }

	public function getAPICasesByUser( $userID, $clientID=NULL )
	{
		$sql = 'SELECT c.*, IFNULL(cs.sortPos,c.ID) AS sortPos FROM Cases c
			INNER JOIN Users u ON (u.ID=:userID AND u.clientID=c.clientID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			LEFT JOIN UserCustomSortCases cs ON (cs.userID=u.ID AND cs.caseID=c.ID)
			WHERE c.deleted IS NULL
			AND (c.createdBy=u.ID OR u.typeID="C" OR cm.userID=u.ID OR c.class="Demo")';
		return $this->db->rows( $sql, ['userID'=>$userID] );
	}

  public function getCasesByIDs(array $caseIDs)
  {
     if (count($caseIDs) == 0) return array();
     foreach ($caseIDs as &$ID) $ID = (int)$ID;
     return $this->db->rows('SELECT * FROM Cases WHERE ID IN ('.  implode(', ', $caseIDs) .') ORDER BY name');
   }

  public function checkCaseID($ID)
  {
		return $this->db->col('SELECT ID FROM Cases WHERE ID = ?', array($ID));
	}

	public function checkSessionOwner( $caseID, $userID ) {
		$sql = 'SELECT COUNT(*) FROM Depositions d WHERE d.caseID=:caseID AND d.deleted IS NULL AND d.ownerID=:userID';
		return (bool)$this->db->col( $sql, ['userID'=>$userID, 'caseID'=>$caseID] );
	}

	public function checkDepoAssist( $caseID, $userID ) {
		$sql = 'SELECT COUNT(*) FROM Depositions d
			LEFT JOIN DepositionAssistants da ON (da.depositionID = d.ID AND da.userID=:userID)
			WHERE d.caseID=:caseID
			AND d.deleted IS NULL
			AND (d.ownerID=:userID OR da.userID IS NOT NULL)';
		return (bool)$this->db->col( $sql, ['userID'=>$userID, 'caseID'=>$caseID] );
	}

	public function checkDepoAttendee( $caseID, $userID )
	{
		$sql = 'SELECT COUNT(*) FROM Depositions d
			LEFT JOIN DepositionAttendees da ON (da.depositionID=d.ID AND da.userID=:userID)
			WHERE d.caseID=:caseID
			AND d.deleted IS NULL
			AND da.userID IS NOT NULL';
		return (bool)$this->db->col( $sql, ['userID'=>(int)$userID, 'caseID'=>(int)$caseID] );
	}

	public function getSortPosition( $caseID, $userID )
	{
		$caseID = (int)$caseID;
		$sql = "SELECT IFNULL(sortPos,{$caseID}) as sortPos FROM UserCustomSortCases WHERE caseID=:caseID AND userID=:userID";
		return $this->db->col( $sql, ['caseID'=>$caseID, 'userID'=>$userID] );
	}

	public function getDepositionOwners( $caseID )
	{
		$caseID = (int)$caseID;
		$sql = 'SELECT d.ownerID FROM Depositions d WHERE d.deleted IS NULL AND d.caseID=:caseID';
		$ownerIDs = $this->db->cols( $sql, ['caseID'=>$caseID] );
		return array_unique( $ownerIDs );
	}

	public function getDepositionAssistants( $caseID )
	{
		$caseID = (int)$caseID;
		$sql = 'SELECT da.userID FROM DepositionAssistants da
			INNER JOIN Depositions d ON (d.ID=da.depositionID AND d.caseID=:caseID)
			WHERE d.deleted IS NULL
			AND d.caseID=:caseID';
		$assistantIDs = $this->db->cols( $sql, ['caseID'=>$this->ID] );
		return array_unique( $assistantIDs );
	}

	public function getDepositionAttendees( $caseID )
	{
		$caseID = (int)$caseID;
		$sql = 'SELECT da.userID FROM DepositionAttendees da
			INNER JOIN Depositions d ON (d.ID=da.depositionID AND d.caseID=:caseID)
			WHERE d.deleted IS NULL
			AND d.caseID=:caseID';
		$attendeeIDs = $this->db->cols( $sql, ['caseID'=>$this->ID] );
		return array_unique( $attendeeIDs );
	}

	public function getCaseFolders( $caseID, $userID ) {
		$sql = 'SELECT f.*, IFNULL(cs.sortPos,f.ID) as sortPos FROM Folders f
				LEFT JOIN UserCustomSortFolders cs ON (cs.folderID=f.ID AND cs.userID=:userID)
				WHERE f.caseID=:caseID';
		$rows = $this->db->rows( $sql, ['caseID'=>(int)$caseID, 'userID'=>(int)$userID] );
		if( is_array( $rows ) && $rows ) {
			foreach( $rows as &$row ) {
				$row['canManage'] = !in_array( $row['class'], [Edepo::FOLDERS_EXHIBIT, Edepo::FOLDERS_TRANSCRIPT] );
			}
		}
		return $rows;
	}

	public function getClientCaseIdBySourceId( $sourceCaseID, $clientID ) {
		$sql = 'SELECT ID FROM Cases WHERE deleted IS NULL AND sourceID=:sourceID AND clientID=:clientID';
		return $this->db->col( $sql, ['sourceID'=>(int)$sourceCaseID, 'clientID'=>(int)$clientID] );
	}

	public function getAllFoldersInCaseForUser( $caseID, $userID, $onlyExhibits=FALSE ) {
		$bind = [
			'userID' => (int)$userID,
			'caseID' => (int)$caseID,
			'demo' => Edepo::CASE_CLASS_DEMO,
			'clientAdmin' => Edepo::USER_TYPE_CLIENT,
			'trialBinder' => Edepo::DEPOSITION_CLASS_TRIALBINDER,
			'exhibit' => Edepo::FOLDERS_EXHIBIT,
			'transcript' => Edepo::FOLDERS_TRANSCRIPT,
			'trusted' => Edepo::FOLDERS_TRUSTED,
			'folder' => Edepo::FOLDERS_FOLDER,
			'personal' => Edepo::FOLDERS_PERSONAL,
			'witnessAnnotations' => Edepo::FOLDERS_WITNESSANNOTATIONS
		];
		$sql = 'SELECT DISTINCT f.ID
			FROM Cases c
			INNER JOIN Users u ON (u.ID=:userID AND u.clientID=c.clientID)
			INNER JOIN Clients cl ON (cl.ID=u.clientID)
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID AND cm.userID=u.ID)
			LEFT JOIN Depositions d ON (d.caseID=c.ID AND d.deleted IS NULL)
			LEFT JOIN DepositionAssistants das ON (das.depositionID=d.ID AND das.userID=u.ID)
			LEFT JOIN DepositionAttendees dat ON (dat.depositionID=d.ID AND dat.userID=u.ID)
			INNER JOIN Folders f ON (f.caseID=c.ID OR f.depositionID=d.ID)
			WHERE c.ID=:caseID
			AND c.clientID=u.clientID
			AND c.deleted IS NULL AND d.deleted IS NULL
			AND ((u.typeID=:clientAdmin OR u.clientAdmin=1 OR c.class=:demo) OR cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID OR dat.userID=u.ID)
			AND ((d.class IS NULL OR d.class != :trialBinder) OR (d.class = :trialBinder AND (cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID)))
			AND (f.caseID=:caseID OR d.caseID=:caseID)
			AND (((f.class=:exhibit OR f.class=:transcript) AND ((u.typeID=@clientAdmin OR u.clientAdmin=1 OR c.class=:demo) OR cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID OR dat.userID=u.ID))
				OR (f.class=:trusted AND ((u.typeID=:clientAdmin OR u.clientAdmin=1 OR c.class=:demo) OR cm.userID=u.ID OR d.ownerID=u.ID OR das.userID=u.ID))
				OR ((f.class=:folder OR f.class=:personal OR f.class=:witnessAnnotations) AND f.createdBy=u.ID))';

		if( $onlyExhibits ) {
			$bind[ 'wp' ] = Edepo::DEPOSITION_CLASS_WITNESSPREP;
			$bind[ 'wpdemo' ] = Edepo::DEPOSITION_CLASS_WPDEMO;
			$sql .= ' AND (f.class=:exhibit OR f.class=:transcript) AND (d.class != :wp AND d.class != :wpdemo)';
		}
		//Debug::ErrorLog( print_r( ['OrchestraCases::getAllFoldersInCaseForUser', $sql, $bind], TRUE ) );
		return $this->db->cols( $sql, $bind );
	}

}
