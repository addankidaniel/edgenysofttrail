<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache,
	ClickBlocks\API\IEdepoBase;

class OrchestraDepositionAttendees extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\DepositionAttendees');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\DepositionAttendees';
   }

	/**
	 *
	 * @param integer $depoID
	 * @param integer $userID
	 * @param bool $lookLinkedDepos true - search also in apropriate deposition for this user
	 * @return \ClickBlocks\DB\DepositionAttendees
	 */
	public function getForUser( $depoID, $userID, $lookLinkedDepos=FALSE ) {
		$bind = ['depoID'=>(int)$depoID, 'userID'=>(int)$userID];
		$daSQL = 'SELECT da.* FROM DepositionAttendees da
				INNER JOIN Depositions d ON (d.ID=da.depositionID)
				INNER JOIN Cases c ON (c.ID=d.caseID)
				WHERE c.deleted IS NULL AND d.deleted IS NULL AND da.depositionID=:depoID AND da.userID=:userID';
		$row = $this->db->row( $daSQL, $bind );
		if( !$row['ID'] && $lookLinkedDepos ) {
			$sql = 'SELECT da.* FROM DepositionAttendees da
			   INNER JOIN Depositions d ON (d.ID=da.depositionID AND d.parentID=:depoID)
			   INNER JOIN Cases c ON (c.ID=d.caseID)
			   INNER JOIN Users u ON (u.ID=:userID AND c.clientID=u.clientID)
			   WHERE c.deleted IS NULL AND d.deleted IS NULL';
			$row = $this->db->row( $sql, $bind );
		}
		$att = new DepositionAttendees( $row['ID'] );
		return $att;
	}

   public function checkUserAttendedDepo($depoID, $userID)
   {
     return (bool)$this->db->col('SELECT userID FROM DepositionAttendees WHERE userID=:userID AND depositionID=:depoID',
            array('userID'=>$userID, 'depoID'=>$depoID));
   }

	public function checkUserIsDepositionAttendee( $depoID, $userID )
	{
		$sql = 'SELECT da.depositionID FROM DepositionAttendees da
			INNER JOIN Depositions d ON (d.ID = da.depositionID AND (d.ID=:depoID or d.parentID=:depoID))
			WHERE userID=:userID';
		return (int)$this->db->col( $sql, ['depoID'=>$depoID, 'userID'=>$userID] );
	}

	/**
	  *
	  * @param type $depoID
	  * @param type $userID
	  * @return \ClickBlocks\DB\DepositionAttendees
	  */
	public function getForGuest( $depoID, $email )
	{
		return $this->getObjectByQuery( ['depositionID'=>$depoID, 'email'=>$email] );
	}

  public function getAttendeesByDepo($dpID)
  {
     $IDs = $this->db->cols('SELECT d.ID FROM Depositions d WHERE d.parentID=?',array($dpID)); // child depos
     foreach ($IDs as $k=>$v) $IDs[$k] = (int)$v;
     $IDs[] = (int)$dpID; // + this depo
     $IDs = '('.implode(', ', $IDs).')';
     return $this->db->rows('SELECT DISTINCT IFNULL(da.name,CONCAT(u.firstName,\' \',u.lastName)) AS `name`, IFNULL(da.email,u.email) AS email, da.ID, da.depositionID, u.clientID, da.userID, da.role FROM DepositionAttendees da LEFT JOIN Users u ON (u.ID = da.userID) WHERE da.depositionID IN '.$IDs);
  }

	public function getNonWitnessAttendessForDepo( $depoID )
	{
		$IDs = $this->db->cols( 'SELECT d.ID FROM Depositions d WHERE d.ID=:depoID OR d.parentID=:depoID', array( 'depoID' => (int)$depoID ) ); // child depositions
		foreach ($IDs as $k=>$v) $IDs[$k] = (int)$v;
		//$IDs[] = (int)$depoID; // + this depo
		//$IDs = '('.implode(', ', $IDs).')';
		return $this->db->rows( 'SELECT DISTINCT
			IFNULL(da.name,CONCAT(u.firstName,\' \',u.lastName)) AS `name`,
			IFNULL(da.email,u.email) AS email,
			da.ID, da.depositionID, u.clientID, da.userID, da.role
			FROM DepositionAttendees da
			LEFT JOIN Users u ON (u.ID = da.userID)
			WHERE da.role IN (\'M\',\'G\') AND da.depositionID IN (' . implode( ',', $IDs ) . ')');
	}

	/**
	 * @param bigint $depoID
	 * @return array
	 */
	public function getNonMemberAttendees( $depoID )
	{
		$depoID = (int)$depoID;
		$sql = 'SELECT da.* FROM DepositionAttendees da WHERE da.depositionID=:depoID AND da.role IN ("G","W")';
		return $this->db->rows( $sql, ['depoID'=>$depoID] );
	}

	/**
	 *
	 * @param bigint $depositionID
	 * @return array
	 */
	public function getMemberAttendees( $depositionID )
	{
		$depositionID = (int)$depositionID;
		return $this->db->rows( 'SELECT u.* FROM DepositionAttendees da
			INNER JOIN Users u ON (u.ID=da.userID)
			WHERE da.depositionID=:ID AND da.userID IS NOT NULL', ['ID'=>$depositionID] );
	}

	/**
	 * @param bigint $depositionID
	 * @param array $roles
	 * @return array
	 */
	public function getAttendeesByRoles( $depositionID, array $roles )
	{
		$depositionID = (int)$depositionID;
		if( !is_array( $roles ) ) {
			$roles = [$roles];
		}
		$sql = 'SELECT da.* FROM DepositionAttendees da WHERE da.depositionID=:ID AND da.role IN ("' . implode( '","', $roles ) . '")';
		return $this->db->rows( $sql, ['ID'=>$depositionID] );
	}

  public function getAttendeesWithFlags($depoID)
  {
     return $this->db->rows('select da.ID as AttendeeID,da.userID,da.name,da.email,u.email,u.clientID,das.userID as isAssistant,cm.userID as isManager,(u.ID=d.createdBy) AS isOwner from DepositionAttendees da
         left join Users u on u.ID=da.userID
         left join DepositionAssistants das on da.userID=das.userID AND das.depositionID=da.depositionID
         inner join Depositions d on d.ID=da.depositionID
         left join CaseManagers cm on cm.userID=da.userID AND cm.caseID=d.caseID
         where da.depositionID=?',array($depoID));
  }

  public function getWidgetAttendeesData($type, array $params)
  {
        $IDs = $this->db->cols('SELECT d.ID FROM Depositions d WHERE d.parentID=?',array($params['depositionID'])); // child depos
        foreach ($IDs as $k=>$v) $IDs[$k] = (int)$v;
        $IDs[] = (int)$params['depositionID']; // + this depo
        array_unique($IDs);
        $IDs = '('.implode(', ', $IDs).')';
        $whereClause = ' FROM DepositionAttendees da  WHERE da.role IN (\'M\',\'G\') AND da.depositionID IN '. $IDs;
        if ($params['searchValue'] != '') {
            $whereClause .= ' AND (da.name like ' . $this->db->quote('%' . $params['searchValue'] . '%') . ' OR da.email like ' . $this->db->quote('%' . $params['searchValue'] . '%') . ')';
        }

        switch ($type) {
            case 'count':
                return $this->db->col('SELECT COUNT(*) ' . $whereClause);
            case 'rows':
                switch (abs($params['sortBy'])) {
                    case 1:
                        $sortBy = 'da.name';
                        break;
                    case 2:
                        $sortBy = 'da.email';
                        break;
                    }
                if ($params['sortBy'] > 0)
                    $sortBy .= ' DESC';
                else
                    $sortBy .= ' ASC';

                if ($params['pageSize']) $limit = ' LIMIT ' . ($params['pageSize'] * $params['pos']) . ', ' . $params['pageSize'];
                else $limit = '';

                return $this->db->rows('SELECT CASE WHEN da.name IS NOT NULL THEN da.name ELSE (SELECT CONCAT(u.firstName,\' \',u.lastName) FROM Users u WHERE u.ID = da.userID) END AS `name`, CASE WHEN da.email IS NOT NULL THEN da.email ELSE (SELECT u.email FROM Users u WHERE u.ID = da.userID) END AS email, CASE WHEN da.userID IS NULL THEN \'Guest\' ELSE \'Member\' END AS type ' . $whereClause . ' ORDER BY ' . $sortBy . $limit);
        }
    }

	public function getAttendeeForDepoID( $depositionID, $userID )
	{
		$sql = 'SELECT da.* FROM DepositionAttendees da WHERE da.depositionID=:depoID AND (da.ID=:userID OR da.userID=:userID)';
		return $this->db->row( $sql, ['depoID'=>$depositionID, 'userID'=>$userID] );
	}

	public function checkAttendeeIsDepositionAttendee( $depoID, $attendeeID )
	{
		$depoID = (int)$depoID;
		$attendeeID = (int)$attendeeID;
//		\ClickBlocks\Debug::ErrorLog( "checkAttendeeIsDepositionAttendee: depoID: {$depoID}, attendeeID: {$attendeeID}" );
		$sql = 'SELECT da.depositionID FROM DepositionAttendees da
			INNER JOIN Depositions d ON (d.ID = da.depositionID AND (d.ID=:depoID or d.parentID=:depoID))
			WHERE da.userID=:userID OR da.ID=:userID';
		return (bool)$this->db->col( $sql, ['userID'=>$attendeeID, 'depoID'=>$depoID] );
	}

	/**
	 * @param integer $depoID
	 * @param integer $attendeeID
	 * @return boolean|\ClickBlocks\DB\DepositionAttendees
	 */
	public function getAttendeeForDeposition( $depoID, $attendeeID )
	{
		$depoID = (int)$depoID;
		$attendeeID = (int)$attendeeID;
//		\ClickBlocks\Debug::ErrorLog( "getAttendeeForDeposition: depoID: {$depoID}, attendeeID: {$attendeeID}" );
		if( !$this->checkAttendeeIsDepositionAttendee( $depoID, $attendeeID ) ) {
			return FALSE;
		}
		$row = $this->getAttendeeForDepoID( $depoID, $attendeeID );
		$attendee = new \ClickBlocks\DB\DepositionAttendees();
		$attendee->assign( $row );
		return $attendee;
	}

	public function pruneUnauthorizedWitnesses( $depoID )
	{
		$depoID = (int)$depoID;
		$role = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS;
		$sql = "DELETE FROM DepositionAttendees WHERE depositionID=:depoID AND role='{$role}'";
		$this->db->execute( $sql, ['depoID'=>$depoID] );
	}

	public function findFirstUnauthorizedWitness( $depoID )
	{
		$depoID = (int)$depoID;
		$role = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS;
		$sql = "SELECT * FROM DepositionAttendees WHERE depositionID=:depoID AND role='{$role}' LIMIT 1";
		return $this->db->row( $sql, ['depoID'=>$depoID] );
	}

	public function getWitnessAttendees( $depoID )
	{
		$depoID = (int)$depoID;
		$wRole = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_WITNESS;
		$twRole = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS;
		$wmRole = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER;
		$sql = "SELECT * FROM DepositionAttendees WHERE depositionID=:depoID AND (role='{$wRole}' OR role='{$twRole}' OR role='{$wmRole}')";
		return $this->db->rows( $sql, ['depoID'=>$depoID] );
	}

	public function getWitnessAttendeesWithRole( $depoID, $role ) {
		if( !in_array( $role, [IEdepoBase::DEPOSITIONATTENDEE_ROLE_WITNESS, IEdepoBase::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS, IEdepoBase::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER] ) ) {
			return FALSE;
		}
		$sql = "SELECT * FROM DepositionAttendees WHERE depositionID=:depoID AND role=:role";
		return $this->db->rows( $sql, ['depoID' => (int)$depoID,'role'=>$role] );
	}

	public function getWitnessMember( $depoID )
	{
		$depoID = (int)$depoID;
		$wmRole = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER;
		$sql = "SELECT * FROM DepositionAttendees WHERE depositionID=:depoID AND role='{$wmRole}'";
		return $this->db->row( $sql, ['depoID'=>$depoID] );
	}
}
