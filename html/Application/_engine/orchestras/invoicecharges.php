<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraInvoiceCharges extends OrchestraEdepo
{
  public function __construct()
  {
        parent::__construct('\ClickBlocks\DB\InvoiceCharges');
    }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\InvoiceCharges';
   }

  public function getWidgetInvoiceChargesData($type, array $params)
  {
        if ($params['ID']) {
            $select = 'SELECT i.ID, i.created, lko.name, ROUND(ic.price/ic.quantity,2) as UnitPrice, SUM(ic.quantity) as quantity, SUM(ic.price) as total ';
            $where = array('i.ID=' . (int) $params['ID']);
            $from = 'Invoices i LEFT JOIN InvoiceCharges ic ON ic.clientID = i.clientID AND ic.created BETWEEN CONCAT(i.startDate," 00:00:00") AND CONCAT(i.endDate," 23:59:59") ';
            $from .= 'INNER JOIN LookupPricingOptions lko ON ic.optionID = lko.ID';
            $groupBy = 'GROUP BY i.ID, i.created, ic.clientID, ic.optionID';
            if (!$params['isSummary']) $groupBy .= ', (ic.price/ic.quantity)';
        } else {
			$params['pageSize'] = -1;
            //$numDocs = 'IF(ic.depositionID IS NOT NULL AND ic.optionID IN (10,11,12,13,14,15),(SELECT COUNT(fs.ID) FROM FileShares fs LEFT JOIN Files f ON f.ID=fs.fileID WHERE fs.depositionID=ic.depositionID AND fs.createdBy != fs.userID AND f.isExhibit=1),NULL) as numDocs, ';
			//$numDocs = 'IF(ic.depositionID IS NOT NULL AND ic.optionID IN (10,11,12,13,14,15),SUM(ic.quantity),NULL) as numDocs, ';
            $select = "ic.created, lko.name, d.depositionOf, CONCAT(u.lastName,', ',u.firstName) as depoUser, ROUND(ic.price/ic.quantity,2) as UnitPrice, SUM(ic.quantity) as quantity, SUM(ic.price) as total, ic.depositionID, ic.optionID, ic.userID ";
            $from = 'InvoiceCharges ic ';
			$from .= 'INNER JOIN LookupPricingOptions lko ON ic.optionID = lko.ID ';
			$from .= 'LEFT JOIN Depositions d ON ic.depositionID = d.ID ';
			$from .= 'LEFT JOIN Users u ON ic.userID = u.ID ';
            $where = array('ic.clientID = ' . (int) $params['clientID']);
            $where[] = 'date_format(ic.created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
            $groupBy = 'GROUP BY ic.clientID, ic.depositionID, ic.userID, ic.optionID';
            if (!$params['isSummary']) $groupBy .= ', (ic.price/ic.quantity)';
        }
        $orderMap = array(1 => 'ic.created, ic.depositionID, ic.userID, ic.optionID');
		//error_log( print_r($params, true) );
        return $this->_getWidgetData($type, $params, $select, $from, $where, $groupBy, $orderMap);
    }

    public function getClientsByReseller($params){
		$options = [
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3
		];
        $sql = 'SELECT childClientID, c.name, c.location FROM InvoiceCharges ic INNER JOIN Clients c ON c.ID = ic.childClientID WHERE childClientID IS NOT NULL AND ic.clientID = ? ';
		$sql .= ' AND ic.optionID IN (' . implode(",", $options) . ')';
        $sql .= ' AND date_format(ic.created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY childClientID';
        return $this->db->rows($sql, array($params['resellerID']));
    }

    public function getChargesByClient($params) {
        $sql = 'SELECT optionID, ROUND(price/quantity, 2) as UnitPrice, lko.name, SUM(price) as total, SUM(quantity) as quantity ';
        $sql .= ' FROM InvoiceCharges INNER JOIN LookupPricingOptions lko ON lko.ID = optionID';
        $sql .= ' WHERE clientID = ? AND childClientID = ?';
        $sql .= ' AND date_format(created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY optionID, UnitPrice ORDER BY lko.name,UnitPrice';
        return $this->db->rows($sql, array($params['resellerID'], $params['clientID']));
    }

    public function getChargesByClientNotCase($params) {
        $sql = 'SELECT optionID, ROUND(price/quantity, 2) as price, lko.name, SUM(price) as total, SUM(quantity) as quantity, DATE_FORMAT(created, "%m/%d/%Y") as created';
        $sql .= ' FROM InvoiceCharges INNER JOIN LookupPricingOptions lko ON lko.ID = optionID';
        $sql .= ' WHERE caseID IS NULL AND clientID = ? ';
        $sql .= ' AND date_format(created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY optionID';
        return $this->db->rows($sql, array($params['clientID']));
    }

    public function getCasesByClient($params) {
        $sql = 'SELECT clientID, caseID, SUM(price) as total ';
        $sql .= ' FROM InvoiceCharges ';
        $sql .= ' WHERE caseID IS NOT NULL AND clientID = ? ';
        $sql .= ' AND date_format(created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY clientID, caseID';
        return $this->db->rows($sql, array($params['clientID']));
    }

    public function getCaseChargesNotDepo($params) {
        $sql = 'SELECT ic.optionID, ROUND(price/quantity, 2) as price, lko.name, SUM(price) as total, SUM(quantity) as quantity, c.name as casename, DATE_FORMAT(ic.created, "%m/%d/%Y") as created, CONCAT(u.firstName, " ", u.lastName) as userName ';
        $sql .= ' FROM InvoiceCharges ic INNER JOIN LookupPricingOptions lko ON lko.ID = ic.optionID';
		$sql .= ' INNER JOIN Cases c ON c.ID = ic.caseID';
		$sql .= ' INNER JOIN Users u ON u.ID = c.createdBy';
        $sql .= ' WHERE depositionID IS NULL AND ic.clientID = ? AND ic.caseID = ? ';
        $sql .= ' AND date_format(ic.created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY ic.clientID, ic.caseID, ic.optionID';
        return $this->db->rows($sql, array($params['clientID'], $params['caseID']));
    }

    public function getDepoByCase($params) {
        $sql = 'SELECT clientID, caseID, depositionID, SUM(price) as total ';
        $sql .= ' FROM InvoiceCharges ';
        $sql .= ' WHERE depositionID IS NOT NULL AND clientID = ? AND caseID = ? ';
        $sql .= ' AND date_format(created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY clientID, caseID, depositionID';
        return $this->db->rows($sql, array($params['clientID'], $params['caseID']));
    }

    public function getDepoCharges($params) {
        $sql = 'SELECT ic.optionID, ROUND(price/quantity, 2) as price, lko.name, SUM(price) as total, SUM(quantity) as quantity, c.name as casename, DATE_FORMAT(ic.created, "%m/%d/%Y") as created, d.depositionOf as depositionOf, CONCAT(u.firstName, " ", u.lastName) as userName, ic.userID ';
        $sql .= ' FROM InvoiceCharges ic INNER JOIN LookupPricingOptions lko ON lko.ID = ic.optionID';
		$sql .= ' INNER JOIN Cases c ON c.ID = ic.caseID';
		$sql .= ' INNER JOIN Depositions d ON d.ID = ic.depositionID';
		$sql .= ' INNER JOIN Users u ON u.ID = d.createdBy';
        $sql .= ' WHERE depositionID IS NOT NULL AND ic.clientID = ? AND ic.caseID = ? AND ic.depositionID = ?';
        $sql .= ' AND date_format(ic.created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY ic.clientID, ic.caseID, ic.depositionID, ic.optionID, price ORDER BY lko.name,price';
        return $this->db->rows($sql, array($params['clientID'], $params['caseID'], $params['depositionID']));
    }

    public function getChargesByClientOrderedByDate($params)
	{
        $sql = 'SELECT ic.optionID, ic.userID, d.ID as depositionID, DATE_FORMAT(ic.created, "%m/%d/%Y") as created, lko.name, CONCAT(u.firstName, " ", u.lastName) as userName, c.name as casename, d.depositionOf as depositionOf, d.class as depositionClass';
        $sql .= ' FROM InvoiceCharges ic INNER JOIN LookupPricingOptions lko ON lko.ID = ic.optionID';
		$sql .= ' LEFT JOIN Cases c ON c.ID = ic.caseID';
		$sql .= ' LEFT JOIN Depositions d ON d.ID = ic.depositionID';
		$sql .= ' LEFT JOIN Users u ON u.ID = ic.userID';
        $sql .= ' WHERE ic.clientID = ?';

		if (isset($params['childClientID']))
		{
			$sql .= ' AND (ic.childClientID = ' . $params['childClientID'] . ' OR ic.childClientID IS NULL)';
		}
		$options = [
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3
		];
		$sql .= ' AND ic.optionID IN (' . implode(",", $options) . ')';
        $sql .= ' AND date_format(ic.created,\'%Y%m%d\') BETWEEN'. $this->db->quote($params['startDate']). 'AND'. $this->db->quote($params['endDate']);
        $sql .= ' GROUP BY ic.clientID, ic.caseID, ic.depositionID, ic.optionID, ic.userID ORDER BY ic.created, ic.ID';
//		\ClickBlocks\Debug::ErrorLog( print_r( ['OrchestraInvoiceCharges::getChargesByClientOrderedByDate', $sql, $params], TRUE ) );
		return $this->db->rows( $sql, [$params['resellerID']] );
    }

    public function getWidgetDepositionChargesData($type, array $params)
    {
      if ($params['startDate'] && $params['endDate']) $dateFilter = 'created BETWEEN '.$this->db->quote($params['startDate'].' 00:00:00').' AND '.$this->db->quote($params['endDate'].' 23:59:59');
      else $dateFilter = '';
      /*if ($type == 'count') {
        $w = array('caseID'=>(int)$params['caseID']);
        if ($dateFilter) $w[] = $dateFilter;
        return $this->col('DISTINCT COUNT(depositionID)', 'InvoiceCharges', $w);
      }*/
      $w = array('d.caseID='.(int)$params['caseID']);
      //SELECT d.*, (SELECT SUM(price) FROM InvoiceCharges WHERE ) AS SetupFee, () AS HostingFee, () AS CountBundles, () AS CountDocuments FROM Depositions d HAVING SetupFee>0 OR HostingFee>0 OR CountBundles>0 OR CountDocuments>0
      $fromIC = 'FROM InvoiceCharges WHERE depositionID=d.ID AND childClientID IS NULL'.($dateFilter ? ' AND '.$dateFilter : '');
      return $this->_getWidgetData($type, $params,
              'd.*, DATE_FORMAT(d.created, "%m/%d/%Y") as Date,
                (SELECT SUM(price) '.$fromIC.' AND optionID=6) as SetupFee,
                (SELECT SUM(price) '.$fromIC.' AND optionID=7) as HostingFee,
                (SELECT SUM(quantity) '.$fromIC.' AND optionID=9) as CountBundles,
                (SELECT SUM(quantity) '.$fromIC.' AND optionID=8) as CountDocuments',
              'Depositions d',
              $w,
              'HAVING SetupFee>0 OR HostingFee>0 OR CountDocuments>0'
              );
    }

    /**
     * Charges sum for Client charges
     * @param type $clientID
     * @param type $caseID
     * @param type $depositionID
     * @param type $optionID
     * @param type $startDate
     * @param type $endDate
     * @return float
     */
   public function getSum($clientID, $caseID, $depositionID = null, $optionID = array(), $startDate = null, $endDate = null)
   {
      $w = array('childClientID IS NULL');
      $bv = array();
      foreach (array('clientID','caseID','depositionID') as $field) {
        if (${$field}) {
          $w[] = $field.'=:'.$field;
          $bv[$field] = ${$field};
        }
      }
      if ($optionID) {
        $w[] = 'optionID '.$this->joinIn((array)$optionID);
      }
      if ($startDate) $w[] = 'created >= '.$this->db->quote($startDate.' 00:00:00');
      if ($endDate) $w[] = 'created <= '.$this->db->quote($endDate.' 23:59:59');
      if (!$w) return 0;
      $w = implode(' AND ', $w);
      return (float)$this->db->col('SELECT SUM(price) FROM InvoiceCharges WHERE '.$w, $bv);
   }

   public function getCaseChargesByPeriod($clientID, $caseID, $startDate, $endDate)
   {
      return (float)$this->db->col('SELECT SUM(price) FROM InvoiceCharges WHERE clientID=:clientID AND caseID=:caseID AND  AND created BETWEEN :startDate AND :endDate', array(':clientID'=>$clientID,':caseID'=>$caseID,':startDate'=>$startDate,':endDate'=>$endDate));
   }

   //ED-135; Child Case/Deposition should not be charged one-time setup fees
   public function removeCaseDepositionSetupCharges( $caseID, $depoID )
   {
	   $caseID = (int)$caseID;
	   $depoID = (int)$depoID;
	   $deposition = $this->db->row( 'SELECT d.ID, d.caseID, d.parentID, u.clientID FROM Depositions d LEFT JOIN Users u ON u.ID = d.createdBy WHERE d.ID=:depoID AND d.caseID=:caseID AND d.parentID IS NOT NULL AND d.deleted IS NULL', array( 'caseID'=>$caseID, 'depoID'=>$depoID ) );
	   if( $deposition && is_array( $deposition ) && isset( $deposition['ID'] ) && isset( $deposition['parentID'] ) && (int)$deposition['ID'] && (int)$deposition['parentID'] )
	   {
		   $caseID = (int)$deposition['caseID'];
		   $depoID = (int)$deposition['ID'];
		   $clientID = (int)$deposition['clientID'];
		   $sql = 'DELETE FROM InvoiceCharges WHERE clientID=:clientID AND optionID IN (4,6) AND caseID=:caseID AND (depositionID IS NULL OR depositionID=:depoID)';
		   $this->db->execute( $sql, array( 'clientID'=>$clientID, 'caseID'=>$caseID, 'depoID'=>$depoID ) );
	   }
   }

}

?>