<?php

namespace ClickBlocks\DB;

use ClickBlocks\MVC\Edepo;

class OrchestraCaseManagers extends OrchestraEdepo {

	public function __construct() {
		parent::__construct( '\ClickBlocks\DB\CaseManagers' );
	}

	public static function getBLLClassName() {
		return '\ClickBlocks\DB\CaseManagers';
	}

	public function deleteByCase( $caseID ) {
		$this->db->execute( 'DELETE FROM CaseManagers WHERE caseID=:caseID', ['caseID'=>(int)$caseID] );
	}

	public function getByCase( $caseID ) {
		return $this->db->rows( 'SELECT userID FROM CaseManagers WHERE caseID=:caseID', ['caseID'=>(int)$caseID] );
	}

	public function getByUser( $userID ) {
		$sql = 'SELECT c.ID as caseID FROM Cases c
			LEFT JOIN CaseManagers cm ON (cm.caseID=c.ID)
			INNER JOIN Users u ON (u.ID=:userID)
			WHERE c.deleted IS NULL AND c.clientID=u.clientID AND (cm.userID=:userID OR c.class=:demo)';
		return $this->db->rows( $sql, ['userID'=>(int)$userID, 'demo'=>Edepo::CASE_CLASS_DEMO] );
	}

	public function checkCaseManager( $caseID, $userID ) {
		$sql = 'SELECT COUNT(*) FROM CaseManagers WHERE caseID=:caseID AND userID=:userID';
		return (bool)$this->db->col( $sql, ['caseID'=>(int)$caseID, 'userID'=>(int)$userID] );
	}

}
