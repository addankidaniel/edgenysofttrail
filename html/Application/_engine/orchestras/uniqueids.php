<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

class OrchestraUniqueIDs extends OrchestraEdepo
{
	public function __construct()
	{
		parent::__construct('\ClickBlocks\DB\UniqueIDs');
	}

	public static function getBLLClassName()
	{
		return '\ClickBlocks\DB\UniqueIDs';
	}
}
