<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\API,
    ClickBlocks\Cache;

class OrchestraPricingModelOptions extends OrchestraEdepo
{
  public function __construct()
  {
      parent::__construct('\ClickBlocks\DB\PricingModelOptions');
   }

  public static function getBLLClassName()
  {
      return '\ClickBlocks\DB\PricingModelOptions';
   }

  public function deleteByClient($clientID)
  {
      $this->db->execute('DELETE FROM PricingModelOptions WHERE clientID = ?', array($clientID));
   }

  public function getByClient($clientID)
  {
      return $this->db->rows('SELECT optionID,price,typeID FROM PricingModelOptions WHERE clientID = ?', array($clientID));
   }

	public function getByClientAndOption($clientID, $optionID)
	{
		$clientID = (int)$clientID;
		$optionID = (int)$optionID;
		$sql = 'SELECT p.*, o.name FROM PricingModelOptions p
			INNER JOIN LookupPricingOptions o ON (o.ID=p.optionID)
			WHERE p.clientID=:clientID AND p.optionID=:optionID';
		$params = ['clientID'=>$clientID, 'optionID'=>$optionID];
//		\ClickBlocks\Debug::ErrorLog( print_r( [$sql, $params], TRUE ) );
		return $this->db->row( $sql, $params );
	}

	/**
	 * @param bigint $clientID
	 * @param int $optionID
	 */
	public function getPriceByOption( $clientID, $optionID )
	{
		$clientID = (int)$clientID;
		$optionID = (int)$optionID;
		if( !$clientID || !$optionID ) {
			return FALSE;
		}
		$sql = 'SELECT price FROM PricingModelOptions WHERE clientID=:clientID AND optionID=:optionID';
		$params = ['clientID'=>$clientID, 'optionID'=>$optionID];
//		\ClickBlocks\Debug::ErrorLog( print_r( [$sql, $params], TRUE ) );
		return $this->db->col( $sql, $params );
	}
}