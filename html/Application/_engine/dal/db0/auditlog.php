<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $description
 * @property varchar $actionID
 * @property bigint $relatedTo
 * @property varchar $entityTypeID
 * @property bigint $createdBy
 * @property timestamp $created
 */
class DALAuditLog extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'AuditLog');
  }
}

?>