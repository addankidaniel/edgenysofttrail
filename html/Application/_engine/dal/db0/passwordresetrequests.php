<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $hash
 * @property varchar $salt
 * @property bigint $userID
 * @property timestamp $created
 * @property timestamp $finished
 */
class DALPasswordResetRequests extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'PasswordResetRequests');
  }
}

?>