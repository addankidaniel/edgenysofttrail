<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $exhibitFileID
 * @property bigint $sourceFileID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property datetime $introducedDate
 * @property varchar $introducedBy
 * @property varchar $exhibitFilename
 */
class DALExhibitHistory extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'ExhibitHistory' );
	}
}
