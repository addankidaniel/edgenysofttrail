<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $caseID
 * @property bigint $createdBy
 * @property bigint $ownerID
 * @property bigint $speakerID
 * @property char $statusID
 * @property varchar $uKey
 * @property varchar $password
 * @property datetime $openDateTime
 * @property varchar $depositionOf
 * @property varchar $volume
 * @property varchar $title
 * @property varchar $location
 * @property varchar $oppositionNotes
 * @property text $notes
 * @property timestamp $created
 * @property timestamp $deleted
 * @property timestamp $started
 * @property timestamp $finished
 * @property varchar $courtReporterEmail
 * @property varchar $courtReporterPassword
 * @property bigint $parentID
 * @property string $exhibitTitle
 * @property string $exhibitSubTitle
 * @property string $exhibitDate
 * @property decimal $exhibitXOrigin
 * @property decimal $exhibitYOrigin
 * @property int $attendeeLimit
 * @property bigint $enterpriseResellerID
 * @property enum $class
 * @property enum $liveTranscript
 * @property varchar $jobNumber
 */
class DALDepositions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Depositions');
  }

	public function __get($f)
	{
		switch( $f )
		{
			case 'password':
			case 'courtReporterPassword':
				return '';
			case 'passwordHash':
				$f = 'password';
			case 'courtReporterPasswordHash':
				$f = 'courtReporterPassword';
		}
		return parent::__get($f);
	}

	public function __set($f, $v)
	{
		if ($f == 'password')
		{
			if ($v == '')
			{
				return false;
			}
			$v = self::hash( $v );
		}
		/** To be introduced after API 1.5 has been deprecated
		if ($f == 'courtReporterPassword')
		{
			throw new \LogicException( 'Court Reporter Password cannot be set directly. Use Depositions::setCourtReporterPassword()' );
		}
		 */
		// for compatibility with API 1.5
		if ($f == 'courtReporterPassword')
		{
			if ($v == '')
			{
				return false;
			}
			$v = self::hashCourtReporter( hash( 'sha1', $v ) );
		}

		if ($f == 'reporterPassword')
		{
			if ($v == '')
			{
				return false;
			}
			$f = 'courtReporterPassword';
			$v = self::hashCourtReporter( $v );
		}
		return parent::__set($f, $v);
	}

	public function comparePassword( $password )
	{
		$da = new \ClickBlocks\DB\DepositionsAuth( $this->ID );
		if( $da && $da->ID ) {	//AES256
			return $da->comparePasscode( $password );
		} elseif( mb_strlen( $this->passwordHash ) === 32 ) {	//MD5
			$valid = (OrchestraDepositionsAuth::getPasswordHash( $password ) === $this->passwordHash);
		} else {	//SHA512
			$valid = (self::hash( $password ) === $this->passwordHash);
		}
		if( $valid ) {
			$da->ID = $this->ID;
			$da->spice = \ClickBlocks\DB\DepositionsAuth::generateSpice();
			$da->code = $password;
			$da->insert();
			if( $da->ID ) {
				$this->password = $password;
				$this->update();
			}
		}
		return $valid;
	}

	/**
	 * setCourtReporterPassword
	 *
	 * Generate a new password for the court reporter. Store the sha1 hash of the password, to match sha1 client-side encoding.
	 * Note that the password will be automatically salted and sha512 hashed on the backend.
	 *
	 * @return string The plain-text password
	 */
	public function setCourtReporterPassword()
	{
		$pass = substr( md5( microtime( true ).'123' ), 0, 8 );
		$this->reporterPassword = hash( 'sha1', $pass );
		$this->save();
		return $pass;
	}

	public static function hash( $f )
	{
		return hash( 'sha512', $f.(Core\Register::getInstance()->config->saltDeposition) );
	}

	public static function hashCourtReporter( $f )
	{
		return hash( 'sha512', $f.(Core\Register::getInstance()->config->saltCourtReporter) );
	}
}
