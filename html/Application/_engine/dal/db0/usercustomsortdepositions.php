<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property bigint $depositionID
 * @property bigint $sortPos
 */
class DALUserCustomSortDepositions extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'UserCustomSortDepositions' );
	}
}
