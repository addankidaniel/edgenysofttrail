<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property enum $vendor
 * @property string $name
 * @property text $URI
 */
class DALDatasources extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'Datasources' );
	}


	public function save()
	{
		return parent::save();
	}

	public function insert()
	{
		return parent::insert();
	}

	public function update()
	{
		return parent::update();
	}
}

?>