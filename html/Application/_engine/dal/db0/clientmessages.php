<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $subject
 * @property longtext $messageBody
 * @property timestamp $createdOn
 */
class DALClientMessages extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'ClientMessages' );
	}

	public function replace()
	{
		throw new Core\FileOperationException( 'replace() on ClientMessages is not allowed' );
	}
}

?>