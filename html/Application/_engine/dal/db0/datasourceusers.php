<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property bigint $datasourceID
 * @property tinyblob $username
 * @property tinyblob $password
 */
class DALDatasourceUsers extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'DatasourceUsers' );
	}
}
