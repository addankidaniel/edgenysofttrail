<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property bigint $caseID
 * @property bigint $sortPos
 */
class DALUserCustomSortCases extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'UserCustomSortCases' );
	}
}
