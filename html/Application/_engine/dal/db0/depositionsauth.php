<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property char $code
 * @property char $spice
 */
class DALDepositionsAuth extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'DepositionsAuth' );
	}

	public function __get( $f )
	{
		switch( $f ) {
			case 'code':
				return '';
			case 'decryptedCode':
				return self::decrypt( parent::__get( 'code' ), $this->spice, $this->config->saltDeposition );
		}
		return parent::__get( $f );
	}

	public function __set( $f, $v )
	{
		if( $f == 'code' ) {
			if( $v == '' ) {
				return FALSE;
			} else {
				$v = self::encrypt( $v, $this->spice, $this->config->saltDeposition );
			}
		}
		return parent::__set( $f, $v );
	}

	public function getPasscode() {
		return $this->__get( 'decryptedCode' );
	}

	private static function encrypt( $v, $pwd, $salt )
	{
		return \ClickBlocks\Utils::encrypt( $v, $pwd, $salt );
	}

	private static function decrypt( $v, $pwd, $salt )
	{
		return \ClickBlocks\Utils::decrypt( $v, $pwd, $salt );
	}

	public function comparePasscode( $code ) {
		return ($this->decryptedCode === $code);
	}
}