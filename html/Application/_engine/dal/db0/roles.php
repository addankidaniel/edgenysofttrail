<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $ID
 * @property varchar $name
 * @property timestamp $created
 */
class DALRoles extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Roles');
  }
}

?>