<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property char $typeID
 * @property bigint $clientID
 * @property varchar $firstName
 * @property varchar $lastName
 * @property varchar $email
 * @property varchar $username
 * @property varchar $phone
 * @property varchar $address1
 * @property varchar $address2
 * @property varchar $city
 * @property char $state
 * @property varchar $region
 * @property varchar $ZIP
 * @property char $countryCode
 * @property timestamp $created
 * @property timestamp $lastLogin
 * @property timestamp $deleted
 * @property tinyint $termsOfService
 * @property tinyint $clientAdmin
 * @property char $phpSessionID
 * @property varchar $affiliateCode
 * @property tinyint $activated
 */
class DALUsers extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'Users' );
	}

	public function __get( $f ) {
		if( $f == 'userID' ) {
			$f = 'ID';
		}
		return parent::__get( $f );
	}

	public function __set( $f, $v ) {
		if( $f == 'userID' ) {
			$f = 'ID';
		}
		return parent::__set( $f, $v );
	}

	public function beforeInsert() {
		$uniqueID = new UniqueIDs();
		$uniqueID->insert();
		$this->ID = $uniqueID->ID;
	}

	public function insert() {
		self::beforeInsert();
		parent::insert();
	}
}
