<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property timestamp $created
 * @property bigint $clientID
 * @property date $startDate
 * @property date $endDate
 */
class DALInvoices extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Invoices');
  }
}

?>