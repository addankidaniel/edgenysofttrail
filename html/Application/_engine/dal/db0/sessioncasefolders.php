<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\SessionCaseFolders;

/**
 * @property bigint $folderID
 * @property bigint $sessionID
 */
class DALSessionCaseFolders extends \ClickBlocks\DB\DALTable {

	public function __construct() {
		parent::__construct( SessionCaseFolders::DB, SessionCaseFolders::BASENAME );
	}
}
