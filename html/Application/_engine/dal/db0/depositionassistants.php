<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $depositionID
 * @property bigint $userID
 */
class DALDepositionAssistants extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'DepositionAssistants');
  }
}

?>