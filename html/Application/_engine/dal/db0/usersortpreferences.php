<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property enum $sortObject
 * @property enum $sortBy
 * @property enum $sortOrder
 */
class DALUserSortPreferences extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'UserSortPreferences' );
	}
}
