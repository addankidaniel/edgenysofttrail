<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property int $roleID
 * @property bigint $permissionID
 * @property timestamp $created
 */
class DALRolePermissions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'RolePermissions');
  }
}

?>