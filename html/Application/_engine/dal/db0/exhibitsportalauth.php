<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\ExhibitsPortalAuth;

/**
 * @property int $sessionID
 * @property string $email
 * @property string $word
 * @property string $spice
 */
class DALExhibitsPortalAuth extends DALTable {

	public function __construct() {
		parent::__construct( ExhibitsPortalAuth::DB, ExhibitsPortalAuth::BASENAME );
	}
}
