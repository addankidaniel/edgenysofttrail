<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property bigint $createdBy
 * @property varchar $name
 * @property varchar $number
 * @property timestamp $created
 * @property timestamp $deleted
 * @property varchar $clientFirstName
 * @property varchar $clientLastName
 * @property varchar $clientAddress
 * @property varchar $clientAddress2
 * @property varchar $clientCity
 * @property char $clientState
 * @property varchar $clientRegion
 * @property varchar $clientZIP
 * @property char $clientCountryCode
 * @property varchar $clientEmail
 * @property varchar $clientPhone
 * @property enum $class
 * @property bigint $sourceID
 */
class DALCases extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'Cases');
  }
}

?>