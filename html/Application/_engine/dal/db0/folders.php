<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\Folders,
	LogicException,
	ClickBlocks\Core\FileOperationException;

/**
 * @property bigint $ID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property bigint $createdBy
 * @property varchar $name
 * @property timestamp $created
 * @property timestamp $lastModified
 * @property enum $class
 */
class DALFolders extends DALTable {

	public function __construct() {
		parent::__construct( Folders::DB, Folders::BASENAME );
	}

	public function replace() {
		throw new FileOperationException( 'replace() on Folder is not allowed' );
	}

	protected function beforeSave() {
		if( $this->caseID && $this->depositionID ) {
			throw new LogicException( 'Error: ' . Folders::BASENAME . '  must not have both caseID and depositionID set.' );
		}
		if( !$this->caseID && !$this->depositionID ) {
			throw new LogicException( 'Error: ' . Folders::BASENAME . '  must have either caseID or depositionID.' );
		}
	}

	public function save() {
		$this->beforeSave();
		return parent::save();
	}

	public function insert() {
		$this->beforeSave();
		return parent::insert();
	}

	public function update() {
		$this->beforeSave();
		return parent::update();
	}

}
