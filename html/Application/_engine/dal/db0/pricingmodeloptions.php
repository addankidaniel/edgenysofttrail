<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $clientID
 * @property tinyint $optionID
 * @property decimal $price
 * @property char $typeID
 */
class DALPricingModelOptions extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'PricingModelOptions');
  }
}

?>