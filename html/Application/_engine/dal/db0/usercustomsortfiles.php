<?php

namespace ClickBlocks\DB;

use ClickBlocks\DB\UserCustomSortFiles;

/**
 * @property bigint $userID
 * @property bigint $fileID
 * @property bigint $sortPos
 */
class DALUserCustomSortFiles extends DALTable {

	public function __construct() {
		parent::__construct( UserCustomSortFiles::DB, UserCustomSortFiles::BASENAME );
	}

}
