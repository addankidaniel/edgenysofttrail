<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property enum $prefKey
 * @property string $value
 */
class DALUserPreferences extends DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'UserPreferences' );
	}
}
