<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $typeID
 * @property varchar $type
 */
class DALLookupClientTypes extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'LookupClientTypes');
  }
}

?>