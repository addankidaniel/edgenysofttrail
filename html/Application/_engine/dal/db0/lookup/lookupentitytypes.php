<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property varchar $typeID
 * @property varchar $type
 */
class DALLookupEntityTypes extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'LookupEntityTypes');
  }
}

?>