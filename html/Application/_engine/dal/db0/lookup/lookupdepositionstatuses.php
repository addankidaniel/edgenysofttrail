<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $statusID
 * @property varchar $status
 */
class DALLookupDepositionStatuses extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'LookupDepositionStatuses');
    }
}

?>