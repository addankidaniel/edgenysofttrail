<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $depositionID
 * @property bigint $createdBy
 * @property timestamp $created
 * @property bigint $fileID
 * @property bigint $copyFileID
 * @property bigint $userID
 * @property bigint $attendeeID
 */
class DALFileShares extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'FileShares');
  }
}

?>