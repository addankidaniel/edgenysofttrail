<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $resellerID
 * @property bigint $clientID
 * @property tinyint $optionID
 * @property char $pricingTerm
 * @property timestamp $created
 * @property bigint $userID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property decimal $price
 * @property int $quantity
 * @property navigation $clients
 * @property navigation $users
 * @property navigation $cases
 * @property navigation $depositions
 */
class DALPlatformLog extends \ClickBlocks\DB\DALTable
{
	public function __construct()
	{
		parent::__construct( 'db0', 'PlatformLog' );
	}
}
