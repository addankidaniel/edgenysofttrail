<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $depositionID
 * @property bigint $userID
 * @property varchar $name
 * @property varchar $email
 * @property enum $role
 * @property timestamp $banned
 */
class DALDepositionAttendees extends DALTable
{
  public function __construct()
  {
    parent::__construct('db0', 'DepositionAttendees');
  }

	protected function checkBeforeSave()
	{
		$wa = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS;
		$wm = \ClickBlocks\API\IEdepoBase::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER;
		if( !$this->userID && !($this->name && $this->email) && !($this->role == $wa || $this->role == $wm) ) {
			throw new \LogicException('Error: DepositionAttendees must have all neccessary fields: userID or name & email.');
		}
	}

  public function save()
  {
    $this->checkBeforeSave();
    return parent::save();
  }

	public function beforeInsert()
	{
		$uniqueID = new UniqueIDs();
		$uniqueID->insert();
		$this->ID = $uniqueID->ID;
	}

	public function insert()
	{
		$this->checkBeforeSave();
		$this->beforeInsert();
		return parent::insert();
	}

  public function update()
  {
    $this->checkBeforeSave();
    return parent::update();
  }
}

?>