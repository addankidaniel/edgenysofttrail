<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core,
	ClickBlocks\DB,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\Debug;

class NodeJS {

	public $host = null;
	public $port = null;
	public $socket = null;
	public $key = '';
	protected $logCallback;
	protected $cfg;

	function __construct(/*$host = null, $port = null, $socket = null, $key = null*/ $useSSL = TRUE)
	{
		if ($useSSL)
		{
			$this->cfg = Core\Register::getInstance()->config->nodejs_ssl;
		}
		else
		{
			$this->cfg = Core\Register::getInstance()->config->nodejs;

		}
		$fields = array('host', 'port', 'socket', 'key');
		foreach($fields as $var)
		{
			if ($var!='key' && !$this->cfg[$var]) throw new \LogicException('nodejs.'.$var.' was not set in config.ini');
			$this->{$var} = $this->cfg[$var];
		}
		//(${$var}!==NULL) ? ${$var} : $this->cfg[$var];
	}

    public function setLogCallback($callback)
    {
       $this->logCallback = $callback;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    private function setupCheckURL($curl, $url){
  		$options = array(CURLOPT_URL => $url,
						CURLOPT_RETURNTRANSFER => true);

		curl_setopt_array($curl, $options);
    }

    public function getURL(){

        if ($this->key === '')
        {
            $url = $this->host.':'.$this->port.'/socket.io/1/?t='.time();
            if ($curl = curl_init()) {
                $this->setupCheckURL($curl, $url);

                $token = explode(':', curl_exec($curl) );
                $this->key = $token[0];

                curl_close($curl);
            }
        }

        return $this->host.':'.$this->port.'/socket.io/1/'.$this->socket.'/'.$this->key.'?t='.time();
    }

    public function sendData($data)
    {
       $url = $this->getURL ();
        if ($curl = curl_init()) {
			$this->setupCheckURL($curl, $url);

            $options = array(CURLOPT_HTTPHEADER => 	array ("Content-Type:text/plain;charset=UTF-8"),
                        CURLOPT_POST      	=>	1,
						CURLOPT_POSTFIELDS 	=>	$data);
            curl_setopt_array($curl, $options);


curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);


            curl_exec($curl);
            if (curl_error($ch))
            {
              echo curl_error($ch) . "(" . curl_errno($ch) . ")";
              curl_close($curl);
              return false;
            }
            curl_close($curl);
            $this->logCall($url, $data);
            return true;
        }
        return false;
    }

    public function sendJsonData(array $data)
    {
       return $this->sendData(json_encode($data));
    }

    public function sendCommand($name, $key, array $params = array())
    {
       return $this->sendData(json_encode(array('name'=>$name, 'key' => $key) + $params));
    }

	public function sendPostCommand( $name, $pathCommand=null, array $params=array() )
	{
		if (!$pathCommand) $pathCommand = $name;
		//$msg = array( 'name'=>$name, 'args'=>$params );
		$msg = array( 'name'=>$name, 'args'=>$params,'logId' =>  @json_decode($_REQUEST['data'])->logId);
		//Debug::ErrorLog( json_encode( $pathCommand ) );
		//Debug::ErrorLog( json_encode( $msg ) );
		return $this->sendPostData( json_encode( $msg ), $pathCommand );
	}

    /**
     * Call after successfully sent data to nodeJS socket
     * @param type $url
     * @param type $data
     */
    protected function logCall($url, $data)
    {
       if (is_callable($this->logCallback)) call_user_func($this->logCallback,'Sent to '.$url.'; data: '.$data);
    }

    public function sendPostData($data, $pathCommand = 'sharefiles')
    {

      $ch = curl_init();
	  $url = "{$this->host}:{$this->port}/{$pathCommand}";

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_FAILONERROR, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 3);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	  //curl_setopt($ch, CURLOPT_CAINFO, '/etc/pki/tls/certs/edepoze.crt');

      $res = curl_exec($ch);
      if (curl_error($ch))
      {
        echo curl_error($ch) . "(" . curl_errno($ch) . ")";
        return false;
      }
      curl_close($ch);
      $this->logCall($url, $data);
      return true;
    }

	/**
	 * @param int $folderID
	 * @param int $byUserID
	 * @param int $sessionID
	 * @param int $skipUserID
	 */
	public function notifyDepositionUpload( $folderID, $byUserID, $sessionID=NULL ) {
		$folderID = (int)$folderID;
		Debug::ErrorLog( "notifyDepositionUpload; folderID:{$folderID}, userID: {$byUserID}, sessionID: {$sessionID}" );
		$folder = new DB\Folders( $folderID );
		if( !$folder || !$folderID || $folder->ID != $folderID ) {
			return;
		}
//		Debug::ErrorLog( print_r( ['notifyDepositionUpload; folder:', $folder->getValues()], TRUE ) );

		if( $folder->isCaseFolder() && $sessionID ) {
			$deposition = new DB\Depositions( $sessionID );
			if( !$deposition || !$deposition->ID || $deposition->ID != $sessionID ) {
				return;
			}
		} else {
			$deposition = new DB\Depositions( $folder->depositionID );
			//Debug::ErrorLog( print_r( $deposition->ID, true ) );
			//Debug::ErrorLog( print_r( $deposition->parentID, true ) );
			if( !$deposition || !$deposition->ID || $deposition->ID != $folder->depositionID ) {
				return;
			}
		}
		if( in_array( $folder->class, [IEdepoBase::FOLDERS_TRUSTED, IEdepoBase::FOLDERS_EXHIBIT, IEdepoBase::FOLDERS_TRANSCRIPT] ) ) {
			$orchDepo = new DB\OrchestraDepositions();
			$notifyUsers = array_keys( $orchDepo->getAllTrustedUsersAsMap( $deposition->ID ) );
		} else {
			$notifyUsers = array( $folder->createdBy );
		}
		if( $deposition->isWitnessPrep() && !$deposition->parentID ) {
			if( $folder->class == IEdepoBase::FOLDERS_TRUSTED || ($folder->class == IEdepoBase::FOLDERS_WITNESSANNOTATIONS && $folder->createdBy == $deposition->ownerID) ) {
				//look for any trusted witness
				$orchDepoAtt = new DB\OrchestraDepositionAttendees();
				$wm = $orchDepoAtt->getWitnessMember( $deposition->ID );
				if( $wm && isset( $wm['ID'] ) ) {
					$notifyUsers[] = $wm['ID'];
				}
			}
		}
		if( $notifyUsers ) {
//			Debug::ErrorLog( print_r( ['notifyDepositionUpload; notifyUsers:', $notifyUsers], true ) );
			$byUserID = (int)$byUserID;
			$params = [
				'depositionID' => $deposition->ID,
				'depositionParentID' => $deposition->parentID,
				'notifyUsers' => $notifyUsers,
				'folderID' => $folder->ID,
				'folderName' => $folder->name,
				'byUserID' => $byUserID
			];
			$this->sendPostCommand( 'deposition_uploaded_files', NULL, $params );
		}
	}

	/**
	 * @param int $sessionID
	 * @param string $sortObject
	 * @param int $csUserID
	 * @param int $byUserID
	 * @param int $folderID
	 */
	public function notifySessionChangedCustomSort( $sessionID, $sortObject, $csUserID, $byUserID=0, $folderID=NULL ) {
//		Debug::ErrorLog( print_r( ['notifySessionChangedCustomSort', $sessionID, $csUserID, $byUserID, $sortObject, $folderID], TRUE ) );
		if( !in_array( $sortObject, [DB\UserSortPreferences::SORTOBJECT_FOLDERS, DB\UserSortPreferences::SORTOBJECT_FILES] ) ) {
			Debug::ErrorLog( "NodeJS::notifySessionChangedCustomSort; unsupported sort object: {$sortObject}" );
			return;
		}
		if( $sortObject === DB\UserSortPreferences::SORTOBJECT_FILES && !$folderID ) {
			Debug::ErrorLog( 'NodeJS::notifySessionChangedCustomSort; missing required folderID for files custom sort change' );
			return;
		}
		$folder = NULL;
		if( $sortObject === DB\UserSortPreferences::SORTOBJECT_FILES ) {
			$folder = new DB\Folders( $folderID );
			if( !$folder || !$folder->ID || $folder->ID !== $folderID ) {
				Debug::ErrorLog( "NodeJS::notifySessionChangedCustomSort; folder not found by ID: {$folderID}" );
				return;
			}
		}
		$orcDepo = new DB\OrchestraDepositions();
		$isTrustedUser = $orcDepo->isTrustedUser( $csUserID, $sessionID );
		$notifyUsers = [];
		// if trusted user -- notify all trusted users
		if( $isTrustedUser && (!$folder || ($folder && in_array( $folder->class, [IEdepoBase::FOLDERS_EXHIBIT,IEdepoBase::FOLDERS_TRANSCRIPT,IEdepoBase::FOLDERS_TRUSTED] ))) ) {
			$trustedUsers = $orcDepo->getAllTrustedUsersAsMap( $sessionID );
			if( is_array( $trustedUsers ) && $trustedUsers ) {
				foreach( array_keys( $trustedUsers ) as $trustedUserID ) {
					$notifyUsers[$trustedUserID] = $trustedUserID;
				}
			}
		} else {
			$notifyUsers[$csUserID] = $csUserID;
		}
		if( isset( $notifyUsers[$byUserID] ) ) {
			unset( $notifyUsers[$byUserID] );	// do not notify the user who made the sort change
		}
		if( $notifyUsers ) {
			$sessionParentID = $orcDepo->getSessionParentID( $sessionID );
//			Debug::ErrorLog( print_r( ['notifyUsers'=>$notifyUsers], TRUE ) );
			$this->sendPostCommand( 'didChangeCustomSort', NULL, [
				'sessionID' => (int)$sessionID,
				'sessionParentID' => (int)$sessionParentID,
				'sortObject' => $sortObject,
				'folderID' => (int)$folderID,
				'notifyUsers' => array_values( $notifyUsers )
			] );
		}
	}

	/**
	 *
	 * @param bigint $caseID
	 * @param bigint $depoID
	 */
	public function notifyCasesUpdated( $caseID, $depoID=NULL )
	{
		Debug::ErrorLog( "notifyCasesUpdated; start. -- caseID: {$caseID}" );
		$case = foo( new DB\ServiceCases() )->getByID( $caseID );
		if( !$case || !$case->ID ) {
			return;
		}

		$notifiedUsers = [];	//users that have already been sent a notification
		$depositions = [];
		$notifyChildDepos = FALSE;
		$orchDepos = new DB\OrchestraDepositions();
		$orchDepoAtt = new DB\OrchestraDepositionAttendees();

		//client admin
		Debug::ErrorLog( 'notifyCasesUpdated; Client Admin ---' );
		$clientAdmin = foo( new DB\OrchestraUsers() )->getClientAdminID( $case->clientID );
		if( !isset( $notifiedUsers[$clientAdmin] ) ) {
			$this->notifyUserCasesUpdated( $clientAdmin );
			$notifiedUsers[$clientAdmin] = TRUE;
		}

		//case managers
		Debug::ErrorLog( 'notifyCasesUpdated; Case Managers ---' );
		$caseManagers = foo( new DB\OrchestraCaseManagers() )->getByCase( $case->ID );
		if( $caseManagers && is_array( $caseManagers ) ) {
			foreach( $caseManagers as $caseMgr ) {
				$caseMgrID = $caseMgr['userID'];
				if( isset( $notifiedUsers[$caseMgrID] ) ) {
					continue;
				}
				$this->notifyUserCasesUpdated( $caseMgrID );
				$notifiedUsers[$caseMgrID] = TRUE;
			}
		}

		//depositions --
		Debug::ErrorLog( 'notifyCasesUpdated; Trusted Users ---' );
		if( $depoID ) {
			$deposition = foo( new DB\ServiceDepositions )->getByID( $depoID );
			if( $deposition && $deposition->ID ) {
				$depositions[] = $deposition->getValues();
				$notifyChildDepos = TRUE;
			}
		} else {
			$depositions = $orchDepos->getAllByCase( $caseID );
			$notifyChildDepos = FALSE;
		}

		//child depositions
		if( $notifyChildDepos && $depositions && is_array( $depositions ) ) {
			foreach( $depositions as $deposition ) {
				$childDepos = $orchDepos->getChildDepositionIDs( $deposition['ID'] );
				if( $childDepos && is_array( $childDepos ) ) {
					foreach( $childDepos as $childDepoID ) {
						$depositions[] = ['ID'=>$childDepoID];
					}
				}
			}
		}

		if( $depositions && is_array( $depositions ) ) {
			foreach( $depositions as $mainDeposition ) {
				//depositions -- trusted users
				Debug::ErrorLog( 'notifyCasesUpdated; Deposition ' . $mainDeposition['ID'] . ' Trusted Users ---' );
				$mainTrustedUsers = $orchDepos->getAllTrustedUsersAsMap( $mainDeposition['ID'] );
				if( $mainTrustedUsers && is_array( $mainTrustedUsers ) ) {
					foreach( array_keys( $mainTrustedUsers ) as $mTrustedUserID ) {
						if( isset( $notifiedUsers[$mTrustedUserID] ) ) {
							continue;
						}
						$this->notifyUserCasesUpdated( $mTrustedUserID );
						$notifiedUsers[$mTrustedUserID] = TRUE;
					}
				}
				//depositions -- attendees
				Debug::ErrorLog( 'notifyCasesUpdated; Deposition ' . $mainDeposition['ID'] . ' Attendees ---' );
				$attendees = $orchDepoAtt->getAttendeesByDepo( $mainDeposition );
				if( $attendees && is_array( $attendees ) ) {
					foreach( $attendees as $attendee ) {
						$attendeeID = $attendee['ID'];
						if( isset( $notifiedUsers[$attendeeID] ) ) {
							continue;
						}
						$this->notifyUserCasesUpdated( $attendeeID );
						$notifiedUsers[$attendeeID] = TRUE;
					}
				}
			}
		}

		Debug::ErrorLog( "notifyCasesUpdated; complete.\n\n" );
	}

	public function notifyUserCasesUpdated( $userIDs )
	{
		Debug::ErrorLog( "notifyUserCasesUpdated for userIDs: " . print_r( $userIDs, TRUE ) );
		$notifyUsers = (is_array( $userIDs )) ? $userIDs : [$userIDs];
		$this->sendPostCommand( 'cases_updated', NULL, ['notifyUsers'=>$notifyUsers] );
	}

	public function notifyUsersRemovedFile( array $folderIDs, $dstFolderID=NULL )
	{
		Debug::ErrorLog( "notifyUsersRemovedFile for folder(s): " . implode( ',', $folderIDs ) );
		$users = [];
		$orcDepo = new DB\OrchestraDepositions();
		$orcFolders = new DB\OrchestraSessionCaseFolders();

		$notifyTrustedUsers = function( $depoID, $folderID ) use ( $orcDepo, &$users ) {
			$trustedUsers = $orcDepo->getAllTrustedUsersAsMap( $depoID );
			if( is_array( $trustedUsers ) && $trustedUsers ) {
				foreach( $trustedUsers as $tUserID => $tEmail ) {
					if( !is_array( $users[$tUserID]['folderIDs'] ) ) {
						$users[$tUserID]['folderIDs'] = [];
					}
					if( !in_array( $folderID, $users[$tUserID]['folderIDs'] ) ) {
						$users[$tUserID]['folderIDs'][] = $folderID;
					}
				}
			}
		};
		$sessionIDs = [];
		$dstSessionIDs = [];
		foreach( $folderIDs as $folderID ) {
			$folder = new DB\Folders( $folderID );
			if( $folder->ID != $folderID ) {
				Debug::ErrorLog( "notifyUsersRemovedFile; folderID: {$folderID} not found" );
				continue;
			}
			if( $folder->isCaseFolder() ) {
				$sessionIDs = $orcFolders->getSessionsForFolder( $folderID );
				if( $dstFolderID ) {
					$dstSessionIDs = $orcFolders->getSessionsForFolder( $dstFolderID );
				}
			} else {
				$deposition = new DB\Depositions( $folder->depositionID );
				if( $deposition->ID != $folder->depositionID ) {
					Debug::ErrorLog( "notifyUsersRemovedFile; depositionID: {$folder->depositionID} not found for folderID: {$folderID}" );
					continue;
				}
				$sessionIDs[] = $deposition->ID;
			}

			// folder shared with trusted users
			if( in_array( $folder->class, [IEdepoBase::FOLDERS_EXHIBIT, IEdepoBase::FOLDERS_TRANSCRIPT, IEdepoBase::FOLDERS_TRUSTED] ) ) {
				if( $sessionIDs && is_array( $sessionIDs ) ) {
					foreach( $sessionIDs as $sessionID ) {
						if( in_array( $sessionID, $dstSessionIDs ) ) {
							continue;
						}
						$notifyTrustedUsers( $sessionID, $folderID );
					}
				}
				continue;
			}

			// Personal folder(s) and/or Folder
			if( in_array( $folder->class, [IEdepoBase::FOLDERS_PERSONAL, IEdepoBase::FOLDERS_WITNESSANNOTATIONS, IEdepoBase::FOLDERS_FOLDER] ) ) {
				$users[$folder->createdBy]['folderIDs'][] = $folderID;
				continue;
			}

		}

//		Debug::ErrorLog( print_r( $users, TRUE ) );
		if( $users ) {
			$this->sendPostCommand( 'deposition_deleted_file', NULL, ['users'=>$users] );
		}
	}

	public function notifyUsersRemovedFolder( $folderID, $sessionIDs ) {
		Debug::ErrorLog( "notifyUsersRemovedFolder; folderID: {$folderID}, sessionID: " . print_r( $sessionIDs, TRUE ) );
		$users = [];
		$folder = new \ClickBlocks\DB\Folders( $folderID );
		if( $folder->ID != $folderID ) {
			Debug::ErrorLog( "notifyUsersRemovedFolder; folderID: {$folderID}, not found" );
			return;
		}
		if( !$folder->isCaseFolder() && empty( $sessionIDs ) ) {
			Debug::ErrorLog( "notifyUsersRemovedFolder; missing sessionID for session folder" );
			return;
		}
		foreach( $sessionIDs as $sessionID ) {
			$deposition = new \ClickBlocks\DB\Depositions( $sessionID );
			if( !$deposition || !$deposition->ID || $deposition->ID != $sessionID ) {
				Debug::ErrorLog( "notifyUsersRemovedFolder; sessionID: {$sessionID}, not found" );
				continue;
			}
			// folder shared with trusted users
			if( in_array( $folder->class, [IEdepoBase::FOLDERS_EXHIBIT, IEdepoBase::FOLDERS_TRANSCRIPT, IEdepoBase::FOLDERS_TRUSTED] ) ) {
				$orcDepo = new \ClickBlocks\DB\OrchestraDepositions();
				$trustedUsers = $orcDepo->getAllTrustedUsersAsMap( $deposition->ID );
				if( is_array( $trustedUsers ) && $trustedUsers ) {
					foreach( $trustedUsers as $tUserID => $tEmail ) {
						$users[$tUserID]['folderIDs'][] = $folder->ID;
					}
				}
				$orcDepoAtt = new DB\OrchestraDepositionAttendees();
				$tWitness = $orcDepoAtt->getWitnessMember( $deposition->ID );
				if( $tWitness ) {
					$users[$tWitness['ID']]['folderIDs'][] = $folder->ID;
				}
			} elseif( in_array( $folder->class, [IEdepoBase::FOLDERS_PERSONAL, IEdepoBase::FOLDERS_WITNESSANNOTATIONS, IEdepoBase::FOLDERS_FOLDER] ) ) {
				$users[$folder->createdBy]['folderIDs'][] = $folder->ID;
			}
		}

//		Debug::ErrorLog( print_r( $users, TRUE ) );
		if( !empty( $users ) ) {
			$this->sendPostCommand( 'deposition_deleted_folder', NULL, ['users'=>$users] );
		}
	}

	public function authorizeWitness( $depositionID, $witnessID )
	{
		$depositionID = (int)$depositionID;
		$witnessID = (int)$witnessID;
		$this->sendPostCommand( 'authorize_witness', NULL, ['depositionID'=>$depositionID, 'witnessID'=>$witnessID] );
	}

	public function notifyFileModified( $fileID, $byUserID, $byName="" ) {
		Debug::ErrorLog( "notifyFileModified: {$fileID}" );
		$file = new DB\Files( $fileID );
		if( !$file || !$file->ID || $file->ID != $fileID ) {
			Debug::ErrorLog( "NodeJS::notifyFileModified; file not found by ID: {$fileID}" );
			return;
		}
		if( !$byUserID || !$byName ) {
			Debug::ErrorLog( 'NodeJS::notifyFileModified; missing byUser' );
			return;
		}
		$folderID = (int)$file->folderID;
		$folder = new DB\Folders( $file->folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			Debug::ErrorLog( "NodeJS::notifyFileModified; folder not found by ID: {$folderID}" );
			return;
		}
		$users = [];
		$orcDepo = new DB\OrchestraDepositions();
		$orcFolders = new DB\OrchestraSessionCaseFolders();

		$notifyTrustedUsers = function( $sessionID ) use ( $orcDepo, &$users, $fileID ) {
			$trustedUsers = $orcDepo->getAllTrustedUsersAsMap( $sessionID );
			if( is_array( $trustedUsers ) && $trustedUsers ) {
				foreach( $trustedUsers as $tUserID => $tEmail ) {
					$users[$tUserID] = (int)$fileID;
				}
			}
		};

		$sessionIDs = [];
		if( $folder->isCaseFolder() ) {
			$sessionIDs = $orcFolders->getSessionsForFolder( $folderID );
		} else {
			$deposition = new DB\Depositions( $folder->depositionID );
			if( $deposition->ID != $folder->depositionID ) {
				Debug::ErrorLog( "notifyFileModified; depositionID: {$folder->depositionID} not found for folderID: {$folderID}" );
				return;
			}
			$sessionIDs[] = $deposition->ID;
		}

		// folder shared with trusted users
		if( in_array( $folder->class, [IEdepoBase::FOLDERS_EXHIBIT, IEdepoBase::FOLDERS_TRANSCRIPT, IEdepoBase::FOLDERS_TRUSTED] ) ) {
			if( $sessionIDs && is_array( $sessionIDs ) ) {
				foreach( $sessionIDs as $sessionID ) {
					$notifyTrustedUsers( $sessionID );
				}
			}
		}

		// Personal folder(s) and/or Folder
		if( in_array( $folder->class, [IEdepoBase::FOLDERS_PERSONAL, IEdepoBase::FOLDERS_WITNESSANNOTATIONS, IEdepoBase::FOLDERS_FOLDER] ) ) {
			$users[$folder->createdBy] = (int)$fileID;
		}

		// do not notify the user that made the change, thats just silly
		if( isset( $users[$byUserID] ) ) {
			unset( $users[$byUserID] );
		}

//		Debug::ErrorLog( print_r( $users, TRUE ) );
		if( $users ) {
			$this->sendPostCommand( 'file_modified', NULL, ['users' => $users, 'byUserID' => (int)$byUserID, 'byUser' => trim( $byName )] );
		}
	}
}
