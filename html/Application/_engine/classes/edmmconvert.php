<?php

namespace ClickBlocks;

use ClickBlocks\Core;

class EDMMConvert
{
	private static $ffmpeg_cfg;

	/**
	 * @param string $inPath
	 * @param string $outPath
	 * @param string $cmdArgs
	 * @return string
	 */
	private static function prepareCommand( $inPath, $outPath, $cmdArgs )
	{
		if( !self::$ffmpeg_cfg ) {
			self::$ffmpeg_cfg = \ClickBlocks\Core\Register::getInstance()->config->ffmpeg;
		}
		if( !$cmdArgs ) {
			\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . " -- Missing command arguments" );
			return;
		}
		setlocale( LC_CTYPE, 'en_US.UTF-8' );
		$cdPath = dirname( $inPath );
//		$cmd = "cd {$cdPath}; ";
		$cmd .= self::$ffmpeg_cfg['bin'];
		if( self::$ffmpeg_cfg['extra_args'] ) {
			$exrta_args = self::$ffmpeg_cfg['extra_args'];
			$cmd .= " {$exrta_args}";
		}
		$inPathArg = escapeshellarg( $inPath );
		$outPathArg = escapeshellarg( $outPath );
		$cmd .= " -i {$inPathArg} {$cmdArgs} {$outPathArg}";
		return $cmd;
	}

	/**
	 * @param string $format mp4|vpx|mp3
	 * @param type $inPath	path to input
	 * @param type $outPath	path to output
	 * @return boolean
	 */
	public static function convertTo( $format, $inPath, $outPath )
	{
		\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . ": {$format}" );
		if( !self::$ffmpeg_cfg ) {
			self::$ffmpeg_cfg = \ClickBlocks\Core\Register::getInstance()->config->ffmpeg;
		}
		if( !is_readable( $inPath ) ) {
			return FALSE;
		}
		$options = NULL;
		switch( strtolower( $format ) ):
			case 'mp4':
				$options = self::$ffmpeg_cfg['encode_mp4_args'];
				break;
			case 'vpx':
				$options = self::$ffmpeg_cfg['encode_vpx_args'];
				break;
			case 'mp3':
				$options = self::$ffmpeg_cfg['encode_mp3_args'];
				break;
			default:
				return;
		endswitch;
		if( !$options ) {
			return;
		}
		$cmd = escapeshellcmd( self::prepareCommand( $inPath, $outPath, $options ) );
		if( !$cmd ) {
			return;
		}
		$output = [];
		$retval = -1;
		$lastLine = exec( $cmd, $output, $retval );
		\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . " -- cmd: {$cmd}" );
		\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . " -- lastLine: {$lastLine}");
		\ClickBlocks\Debug::ErrorLog( __CLASS__ . '::' . __METHOD__ . " -- return value: {$retval}" );
		\ClickBlocks\Debug::ErrorLog( print_r( $output, TRUE ) );
		return ($retval === 0);
	}

	public static function toMP4( $inPath, $outPath )
	{
		return self::convertTo( 'mp4', $inPath, $outPath );
	}

	public static function toVPX( $inPath, $outPath )
	{
		return self::convertTo( 'vpx', $inPath, $outPath );
	}

	public static function toMP3( $inPath, $outPath )
	{
		return self::convertTo( 'mp3', $inPath, $outPath );
	}
}
