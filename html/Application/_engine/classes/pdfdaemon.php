<?php

namespace ClickBlocks;

use ClickBlocks\Core,
	ClickBlocks\Core\Register,
	ClickBlocks\Debug;

class PDFDaemon extends \ClickBlocks\Singleton {

	protected static $config;
	protected static $errNo;
	protected static $errMsg;

	public static $verbose=FALSE;

	const SCALE_THUMB = 'thumb';
	const SCALE_ACTUAL = 'actual';
	const SCALE_ZOOM4 = 'zoom4';

	static $scales = [self::SCALE_THUMB, self::SCALE_ACTUAL, self::SCALE_ZOOM4];

	protected static function init() {
		self::$config = (object)Register::getInstance()->config->pdfdaemon;
	}

	/**
	 * Send command to daemon
	 * @param string $command
	 * @return boolean|string
	 */
	public static function request( $command ) {
		self::getInstance();
		$socket = stream_socket_client( self::$config->path, self::$errNo, self::$errMsg, self::$config->timeout );
		if( !$socket || !is_resource( $socket ) ) {
			Debug::ErrorLog( '[PDFDaemon] Unable to open socket!' );
			return FALSE;
		}
		stream_set_timeout( $socket, self::$config->timeout );

		if( self::$verbose ) {
			Debug::ErrorLog( "[PDFDaemon] Sending Message '$command' ..." );
		}
		if( !fwrite( $socket, $command ) ) {
			Debug::ErrorLog( '[PDFDaemon] Error writing to socket!' );
			fclose( $socket );
			return FALSE;
		}
		$resMsg = stream_get_contents( $socket );
		fclose( $socket );
		if( !$resMsg ) {
			Debug::ErrorLog( '[PDFDaemon] Error while reading from socket!' );
			return FALSE;
		}
		if( self::$verbose ) {
			Debug::ErrorLog( "[PDFDaemon] response: {$resMsg}" );
		}
		return json_decode( $resMsg );
	}

	/**
	 * Annotate command
	 * @param string $inPath
	 * @param string $outPath
	 * @param string $annotationsPath
	 * @param array $pFlags
	 * @return boolean|string
	 */
	public static function annotate( $inPath, $outPath, $annotationsPath=NULL, $pFlags=[] ) {
		self::getInstance();
		if( self::$verbose ) {
			Debug::ErrorLog( print_r( ['inPath' => $inPath, 'outPath' => $outPath, 'annotationsPath' => $annotationsPath, 'pFlags' => $pFlags], TRUE ) );
		}
		$flags = [];
		foreach( ['wm', 'flat', 'clean'] as $flag ) {
			if( in_array( $flag, $pFlags ) ) {
				$flags[] = $flag;
			}
		}
		if( !file_exists( $inPath ) ) {
			Debug::ErrorLog( "[PDFDaemon] annotate; unable to read source: {$inPath}" );
			return;
		}
		if( !file_exists( $annotationsPath ) ) {
			if( !$flags ) {
				Debug::ErrorLog( "[PDFDaemon] annotate; unable to read annotations: {$annotationsPath}" );
				return;
			}
		}
		if( !is_writable( $outPath ) && !is_writable( dirname( $outPath ) ) ) {
			Debug::ErrorLog( "[PDFDaemon] annotate; unable to write outout: {$outPath}" );
			return;
		}

		// {"cmd":"annotate","params":{"inPath":"/path/in.pdf","outPath":"/path/out.pdf","annotationsPath":"/path/annotations.json","flags":["wm","flat","clean"]}}
		$result = self::request( json_encode( [
			'cmd' => 'annotate',
			'params' => [
				'inPath' => $inPath,
				'outPath' => $outPath,
				'annotationsPath' => $annotationsPath,
				'flags' => $flags
				]
			], JSON_UNESCAPED_SLASHES ) );
		return $result->success;
	}

	/**
	 * Watermark command
	 * @param string $inPath
	 * @param string $outPath
	 * @return boolean|string
	 */
	public static function watermark( $inPath, $outPath ) {
		return self::annotate( $inPath, $outPath, NULL, ['wm'] );
	}

	/**
	 * Flatten command
	 * @param string $inPath
	 * @param string $outPath
	 * @return boolean|string
	 */
	public static function flatten( $inPath, $outPath ) {
		return self::annotate( $inPath, $outPath, NULL, ['flat'] );
	}

	/**
	 * Clean command
	 * @param string $inPath
	 * @param string $outPath
	 * @return boolean|string
	 */
	public static function clean( $inPath, $outPath ) {
		return self::annotate( $inPath, $outPath, NULL, ['clean'] );
	}

	public static function cache( $inPath, $page=0, $scale=self::SCALE_ZOOM4 ) {
		self::getInstance();
		Debug::ErrorLog( "PDFDaemon::cache; file: {$inPath}" );
		if( !file_exists( $inPath ) ) {
			Debug::ErrorLog( "PDFDaemon::cache; invalid file: {$inPath}" );
			return;
		}
		if( mime_content_type( $inPath ) !== 'application/pdf' ) {
			return;
		}
		$pageIndex = (int)$page;
		if( !($pageIndex >= 0) ) {
			$pageIndex = 0;
		}
		$fileHash = hash_file( 'md5', $inPath );
		$cmd = [
			'cmd' => 'cache',
			'params' => [
				'inPath' => $inPath,
				'hash' => $fileHash,
				'page' => (int)$pageIndex,
				'scale' => (in_array( $scale, self::$scales )) ? $scale : self::SCALE_ZOOM4,
				'skipSeed' => TRUE
			]
		];
		$cmdMsg = str_replace( "'", "'\''", json_encode( $cmd, JSON_UNESCAPED_SLASHES ) );
		$cli = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'scripts' ) . DIRECTORY_SEPARATOR . 'pdfdaemon-cli.php';
		Debug::ErrorLog( "PDFDaemon::cache; cmd: php {$cli} '{$cmdMsg}'" );
		shell_exec( "php {$cli} '{$cmdMsg}' &> /dev/null &" );
	}

	/**
	 * getText command
	 * @param string $inPath
	 * @return boolean|string
	 */
	public static function getText( $inPath ) {
		self::getInstance();
		if( self::$verbose ) {
			Debug::ErrorLog( print_r( ['inPath' => $inPath], TRUE ) );
		}
		if( !file_exists( $inPath ) ) {
			Debug::ErrorLog( "[PDFDaemon] getText; unable to read source: {$inPath}" );
			return;
		}

		// {"cmd":"text","params":{"inPath":"/path/in.pdf"}}
		$result = self::request(
			json_encode( [
				'cmd' => 'text',
				'params' => [
					'inPath' => $inPath
					]
				],
				JSON_UNESCAPED_SLASHES
			)
		);
		return $result->result;
	}
}
