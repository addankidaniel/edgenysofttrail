<?php

namespace ClickBlocks;

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\PDFDaemon,
	ClickBlocks\Debug,
	Exception,
	finfo,
	Imagick;

// defined( 'PDF_FILES_PATH' ) or define( 'PDF_FILES_PATH', './files/' );
defined( 'TMP_FILES_PATH' ) or define( 'TMP_FILES_PATH', $_SERVER['DOCUMENT_ROOT'].Core\Register::getInstance()->config->dirs['temp'].'/' );

defined( 'CONVERT_TEMPLATE_TXT' ) or define( 'CONVERT_TEMPLATE_TXT', dirname( __FILE__ ).'/templates/txt-conversion.html' );
defined( 'CONVERT_TEMPLATE_IMG' ) or define( 'CONVERT_TEMPLATE_IMG', dirname( __FILE__ ).'/templates/img-conversion.html' );

class EDPDFConvert
{
	const UNIQUE_FILENAME_ATTEMPTS = 64;
	const TEMPLATE_TITLE_PLACEHOLDER = '%DOCUMENT_TITLE%';
	const TEMPLATE_TXT_CONTENT = '%CONTENT_GOES_HERE%';
	const TEMPLATE_IMG_MIMETYPE = '%IMAGE_MIME_TYPE%';
	const TEMPLATE_IMG_IMGDATA = '%IMAGE_CONTENT_BASE64%';

	private $workingFile;
	// protected $config;
	
	private $config;

	function __construct( $filename=null )
	{
		$this->workingFile = null;
		$this->config = Core\Register::getInstance()->config;

		if ($filename)
		{
			$this->setFilename( $filename );
		}
	}

	function __destruct()
	{

	}

	public function reset()
	{
		$this->workingFile = null;
	}

	public function setFilename( $filename )
	{
		if (!file_exists( $filename ))
		{
			throw new Exception( 'Could not find "'.$filename.'"' );
		}

		$this->workingFile = [
			'fullname' => $filename,
			'name' => basename( $filename ),
			'path' => realpath( dirname( $filename ) ) . DIRECTORY_SEPARATOR,
			'mimetype' => mime_content_type( $filename )
		];
	}

	public function convert( $newfilename='', $overwrite=false )
	{
		if (!$this->workingFile || !isset( $this->workingFile['fullname'] ))
		{
			throw new Exception( 'Cannot convert undefined file.' );
		}

		$outPath = pathinfo( $newfilename, PATHINFO_DIRNAME );
		if( !$outPath || $outPath === '.' ) {
			$outPath = realpath( $this->workingFile['path'] );
		}
		$pdfFilename = $outPath . DIRECTORY_SEPARATOR . $this->prepareFilename( $newfilename, $overwrite );

		\ClickBlocks\Debug::ErrorLog( stripos( $this->workingFile['mimetype'], 'text/' ) );
		if( stripos( $this->workingFile['mimetype'], 'text/' ) === 0 ) {
			$this->workingFile['mimetype'] = 'text/plain';
		}

		switch( strtolower( $this->workingFile['mimetype'] ) ) {
			case 'image/png':
			case 'image/gif':
			case 'image/jpeg':
			case 'image/pjpeg':
				$this->convertImgToPDF( $pdfFilename );
				break;
			case 'image/ecopy':
			case 'image/tif':
			case 'image/tiff':
				$this->convertTiffToPDF( $pdfFilename );
				break;
			case 'text/plain':
				$this->convertTxtToPDF( $pdfFilename );
				break;
			case 'application/msword':
				$this->convertDocToPDF( $pdfFilename );
				break;
			case 'application/doc':
				$this->convertDocToPDF( $pdfFilename );
				break;
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				$this->convertDocToPDF( $pdfFilename );
				break;
			default:
				throw new Exception( 'Cannot convert '.$this->workingFile['name'].'; file type "'.$this->workingFile['mimetype'].'" is not supported.' );
		}

		return $pdfFilename;
	}

	protected function convertImageToPDF( $newPdfFilename )
	{
		$image = new Imagick( $this->workingFile['fullname'] );
		$image->setImageFormat( 'pdf' );
		$image->writeImage( $newPdfFilename );

		return true;
	}

	protected function trueConvertImageToPDF( $newPdfFilename )
	{
		$command = escapeshellcmd( Core\Register::getInstance()->config->imagick['path_convert'] )
			. ' '.escapeshellarg( $this->workingFile['fullname'] )
			. ' '.escapeshellarg( $newPdfFilename );
		exec( $command );

		return true;
	}

	protected function convertTiffToPDF( $newPdfFilename )
	{
		// rip TIFF into series of temp images
		$tmpImgKey = 'tmp_'.microtime( true ).'_';
		$tmpImgIncrement = '%03d';
		$command = escapeshellcmd( Core\Register::getInstance()->config->imagick['path_convert'] )
			. ' '.escapeshellarg( $this->workingFile['fullname'] )
			. ' '.escapeshellarg( TMP_FILES_PATH.$tmpImgKey.$tmpImgIncrement.'.png' );
		exec( $command );

		$removeFiles = [];

		// add each temp image into PDF file
		$concatCommand = escapeshellcmd( Core\Register::getInstance()->config->imagick['path_convert'] );
		foreach (scandir( TMP_FILES_PATH ) as $filename)
		{
			if (strpos( $filename, $tmpImgKey ) !== false)
			{
				$removeFiles[] = $filename;
				$concatCommand .= ' '.escapeshellarg( TMP_FILES_PATH.$filename );
			}
		}
		$concatCommand .= ' '.escapeshellarg( $newPdfFilename );
		exec( $concatCommand );

		// remove temp image files
		foreach ($removeFiles as $tmpfile)
		{
			unlink( TMP_FILES_PATH.$tmpfile );
		}

		return true;
	}

	protected function convertImgToPDF( $newPdfFilename )
	{
		// read in the conversion template
		if (($conversionTemplate = file_get_contents( CONVERT_TEMPLATE_IMG )) === false)
		{
			throw new Exception( 'Could not access conversion template "'.CONVERT_TEMPLATE_IMG.'"' );
		}

		$tmpFilePath = $this->workingFile['fullname'];
		if( $this->workingFile['mimetype'] == 'image/jpeg' || $this->workingFile['mimetype'] == 'image/pjpeg' ) {
			try {
				$exif = exif_read_data( $tmpFilePath );
			} catch( \Exception $e ) {}
			if( $exif && isset( $exif['Orientation'] ) ) {
				$rotate = 0;
				switch( $exif['Orientation'] ) {
					case 3:
						$rotate = 180;
						break;
					case 6:
						$rotate = -90;
						break;
					case 8:
						$rotate = 90;
						break;
				}
				if( $rotate != 0 ) {
					$image = NULL;
					switch( $this->workingFile['mimetype'] ) {
						case 'image/png':
							$image = imagecreatefrompng( $this->workingFile['fullname'] );
							break;
						case 'image/gif':
							$image = imagecreatefromgif( $this->workingFile['fullname'] );
							break;
						case 'image/jpeg':
						case 'image/pjpeg':
							$image = imagecreatefromjpeg( $this->workingFile['fullname'] );
							break;
					}
					if( $image ) {
						\ClickBlocks\Debug::ErrorLog( "Rotating uploaded image to corrected orientation: {$rotate} degrees." );
						$image = imagerotate( $image, $rotate, 0 );
						imagepng( $image, $this->workingFile['fullname'] );
						$this->workingFile['mimetype'] = 'image/png';
					}
				}
			}
		}

		$udate = microtime( true );
		$uniqueTime = date( 'YmdHis', floor( $udate ) ).(floor( fmod( $udate, 1 )*10000 ));

		// simple string replacements
		$conversionTemplate = str_replace( self::TEMPLATE_TITLE_PLACEHOLDER, '', $conversionTemplate );
		$conversionTemplate = str_replace( self::TEMPLATE_IMG_MIMETYPE, $this->workingFile['mimetype'], $conversionTemplate );

		$tmpltHeadPos = strpos( $conversionTemplate, self::TEMPLATE_IMG_IMGDATA );
		$tmpltTailPos = $tmpltHeadPos+strlen( self::TEMPLATE_IMG_IMGDATA );

		$tmpFilename = TMP_FILES_PATH.'tmp_wkhtml_'.$uniqueTime.'.html';

		$fwh = fopen( $tmpFilename, 'w' );
		fwrite( $fwh, substr( $conversionTemplate, 0, $tmpltHeadPos ) );

		$frh = fopen( $this->workingFile['fullname'], 'r' );
		$base64_filter = stream_filter_append( $frh, 'convert.base64-encode' );
		$bytesCopied = stream_copy_to_stream( $frh, $fwh );
		fclose( $frh );

		fwrite( $fwh, substr( $conversionTemplate, $tmpltTailPos, strlen( $conversionTemplate )-$tmpltTailPos ) );

		fclose( $fwh );

		list( $imgWidth, $imgHeight ) = getimagesize( $this->workingFile['fullname'] );
		$orientation = ($imgHeight >= $imgWidth) ? 'Portrait' : 'Landscape';
		$zoom = 1.0;
		$minZoom = 0.30;
		$widthFactor = 618.9999;	//based on 8.5x11 Letter with 3mm margins
		$heightFactor = 806.304;	//based on 8.5x11 Letter with 3mm margins
		
		$wZoom = $widthFactor / $imgWidth;
		$hZoom = $heightFactor / $imgHeight;
		\ClickBlocks\Debug::ErrorLog( "wZoom: {$wZoom}" );
		\ClickBlocks\Debug::ErrorLog( "hZoom: {$hZoom}" );
		$purposedZoom = min( [$wZoom, $hZoom] );
		if( $purposedZoom < $minZoom ) {
			$zoom = FALSE;
		} else {
			$zoom = $purposedZoom;
		}

		$pdf = new Utils\WKHTMLToPDF( $tmpFilename, $newPdfFilename );
		$pdf->setPageSize( 'Letter' );
		$pdf->setOrientation( $orientation );
		$pdf->setMargins( 3, 3, 3, 3 );	//millimeters
		if( $zoom ) {
			\ClickBlocks\Debug::ErrorLog( "using zoom: {$wZoom}" );
			$pdf->setZoom( $zoom );
		}
		$pdf->addParam( '--disable-smart-shrinking', TRUE );
		$pdf->addParam( '--no-background', TRUE );
		$pdf->render();

		unlink( $tmpFilename );
		
		if( !$this->pdfCleanup( $newPdfFilename ) ) {
			throw new Exception( 'PDF cleanup failed' );
		}

		return true;
	}

	protected function convertTxtToPDF( $newPdfFilename )
	{
		// read in the conversion template
		if (($conversionTemplate = file_get_contents( CONVERT_TEMPLATE_TXT )) === false)
		{
			throw new Exception( 'Could not access conversion template "'.CONVERT_TEMPLATE_TXT.'"' );
		}

		// simple string replacements
		$conversionTemplate = str_replace( self::TEMPLATE_TITLE_PLACEHOLDER, '', $conversionTemplate );

		$tmpltHeadPos = strpos( $conversionTemplate, self::TEMPLATE_TXT_CONTENT );
		$tmpltTailPos = $tmpltHeadPos+strlen( self::TEMPLATE_TXT_CONTENT );

		$udate = microtime( true );
		$tmpFilename = TMP_FILES_PATH.'tmp_wkhtml_'.date( 'YmdHis', floor( $udate ) ).(floor( fmod( $udate, 1 )*10000 )).'.html';

		$fwh = fopen( $tmpFilename, 'w' );

		fwrite( $fwh, substr( $conversionTemplate, 0, $tmpltHeadPos ) );

		// capture the text file to be converted and place into template in memory-friendly way
		$frh = fopen( $this->workingFile['fullname'], 'r' );
		$bytesCopied = stream_copy_to_stream( $frh, $fwh );
		fclose( $frh );

		fwrite( $fwh, substr( $conversionTemplate, $tmpltTailPos, strlen( $conversionTemplate )-$tmpltTailPos ) );

		fclose( $fwh );

		$pdf = new Utils\WKHTMLToPDF( $tmpFilename, $newPdfFilename );
        $pdf->setPageSize( 'Letter' );
		$pdf->setOrientation( 'Portrait' );
		$pdf->addParam( '--no-background', true );
		$pdf->render();

		unlink( $tmpFilename );
		
		if( !$this->pdfCleanup( $newPdfFilename ) ) {
			throw new Exception( 'PDF cleanup failed' );
		}

		return true;
	}

/*----------------------------------------DOC to PDF START--------------------------------*/


	protected function convertDocToPDF( $newPdfFilename )
	{
		// read in the conversion template
		if (($conversionTemplate = file_get_contents( CONVERT_TEMPLATE_TXT )) === false)
		{
			throw new Exception( 'Could not access conversion template "'.CONVERT_TEMPLATE_TXT.'"' );
		}

		// simple string replacements
		$conversionTemplate = str_replace( self::TEMPLATE_TITLE_PLACEHOLDER, '', $conversionTemplate );

		$tmpltHeadPos = strpos( $conversionTemplate, self::TEMPLATE_TXT_CONTENT );
		$tmpltTailPos = $tmpltHeadPos+strlen( self::TEMPLATE_TXT_CONTENT );

		$udate = microtime( true );
		$tmpFilename = TMP_FILES_PATH.'tmp_wkhtml_'.date( 'YmdHis', floor( $udate ) ).(floor( fmod( $udate, 1 )*10000 )).'.html';

		$fwh = fopen( $tmpFilename, 'w' );

		fwrite( $fwh, substr( $conversionTemplate, 0, $tmpltHeadPos ) );

		// capture the text file to be converted and place into template in memory-friendly way
		$frh = fopen( $this->workingFile['fullname'], 'r' );
		$bytesCopied = stream_copy_to_stream( $frh, $fwh );
		fclose( $frh );

		fwrite( $fwh, substr( $conversionTemplate, $tmpltTailPos, strlen( $conversionTemplate )-$tmpltTailPos ) );

		fclose( $fwh );

		$pdf = new Utils\WKHTMLToPDF( $tmpFilename, $newPdfFilename );
        $pdf->setPageSize( 'Letter' );
		$pdf->setOrientation( 'Portrait' );
		$pdf->addParam( '--no-background', true );
		$pdf->render();

		$file = $request->file('file');

        $name = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/file');

        $file->move($destinationPath, $name);

        $input['file'] = $name;

		$pdfname = $tmpFilename;

        $FilePath = public_path()."/file/".$name;

        $FilePathPdf = public_path()."/pdf_file/".$pdfname;



		$extension = $file->getClientOriginalExtension();

/*  for excel sheet  to pdf */

        if($extension == 'xls' || $extension == 'xlsx'){

            

            $phpWord_xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($FilePath);

            $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($phpWord_xls, 'Dompdf');

            $objWriter->save($FilePathPdf);

            $file = public_path()."/pdf_file/".$pdfname;

        }

 

/*  for doc or docx to pdf  */

        elseif ($extension == 'doc') {

            

            \PhpOffice\PhpWord\Settings::setPdfRendererPath(base_path() .'/vendor/dompdf/dompdf');

            \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

 

            $phpWord = \PhpOffice\PhpWord\IOFactory::load($FilePath, 'MsDoc');

            $pdfWriter = \PhpOffice\PhpWord\IOFactory::createWriter( $phpWord, 'PDF' );

            $pdfWriter->save($FilePathPdf);

            $file = public_path()."/pdf_file/".$pdfname;

          

        }

        else{

            \PhpOffice\PhpWord\Settings::setPdfRendererPath(base_path() .'/vendor/dompdf/dompdf');

            \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

 

            $phpWord = \PhpOffice\PhpWord\IOFactory::load($FilePath);

            $pdfWriter = \PhpOffice\PhpWord\IOFactory::createWriter( $phpWord, 'PDF' );

            $pdfWriter->save($FilePathPdf);

            $file = public_path()."/pdf_file/".$pdfname;

 

        }

		unlink( $tmpFilename );
		
		if( !$this->pdfCleanup( $newPdfFilename ) ) {
			throw new Exception( 'PDF cleanup failed' );
		}

		return true;
	}

	/*----------------------------------------DOC to PDF END ---------------------------------*/

	protected function prepareFilename( $filename=NULL, $overwrite=FALSE )
	{
		if( !$this->workingFile || !isset( $this->workingFile['fullname'] ) || strlen( $this->workingFile['fullname'] ) < 1 ) {
			throw new Exception( 'Filename has not been set.' );
		}

		// if name is not provided manually use the workingFile name
		if( !$filename ) {
			if( !$this->workingFile || !isset( $this->workingFile['name'] ) || strlen( $this->workingFile['name'] ) < 1 ) {
				throw new Exception( 'Filename has not been defined' );
			}
			$filename = $this->workingFile['name'];
		}

		// trim any file extension off of the filename
		$filename = pathinfo( $filename, PATHINFO_FILENAME );

		$considerate_filename = $filename . '.pdf';

		if( $overwrite != FALSE ) {
			return $considerate_filename;
		}

		$copy_count = 0;
		while( file_exists( $this->workingFile['path'] . $considerate_filename ) ) {
			if( $copy_count > self::UNIQUE_FILENAME_ATTEMPTS ) {
				throw new Exception( 'Attempts to identify valid filename exhausted.' );
			}
			$considerate_filename = $filename . '_' . (++$copy_count) . '.pdf';
		}

		return $considerate_filename;
	}
	
	// wkhtmltopdf writes out pdf files that pdfjs does not like, so we run the pdfs through fpdf_util to clean them up.
	public function pdfCleanup( $filename ) {
		$tmpCleanup = TMP_FILES_PATH . Utils::createUUID() . '.pdf';

		if( PDFDaemon::clean( $filename, $tmpCleanup ) && file_exists( $tmpCleanup ) ) {
			Debug::ErrorLog( 'EDPDFConvert::pdfCleanup -- replacing' );
			copy( $tmpCleanup, $filename );
			unlink( $tmpCleanup );
			Debug::ErrorLog( 'EDPDFConvert::pdfCleanup -- finished' );
			return TRUE;
		}

		Debug::ErrorLog( 'EDPDFConvert::pdfCleanup -- error' );
		return FALSE;
	}
}



 

        