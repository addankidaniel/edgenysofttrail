<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core,
    ClickBlocks\Utils,
	ClickBlocks\DB;

define( 'MAIL_CONFIDENTIAL_FOOTER', "This communication constitutes an electronic communication within the meaning of the Electronic Communications Privacy Act, 18 USC 2510, and  its disclosure is strictly limited to the recipient intended by the sender of this message. This communication may contain confidential and privileged material for the sole use of the intended recipient and receipt by anyone other than the intended recipient does not constitute a loss of the confidential or privileged nature of the communication. Any review or distribution by others is strictly prohibited. If you are not the intended recipient please contact the sender by return electronic mail and delete all copies of this communication." );

class EmailGenerator
{
	protected $mailer;

	private static function getTemplate($file) {
		$tpl = new Core\Template();
		$tpl->setTemplate('email',$file);
		return $tpl;
	}

	protected static function getBodyGeneric($name, $text) {
		$tpl = self::getTemplate(Core\IO::dir('backend').'/emails/general.html');
		$tpl->name = $name;
		$tpl->text = $text;
		return $tpl->render();
	}

	protected static function getBodyCourtReporterInvite( $params )
	{
		$tpl = self::getTemplate(Core\IO::dir( 'backend' ) . '/emails/courtReporterInvite.html' );
		$tpl->params = $params;
		return $tpl->render();
	}

	protected static function getBodyWitnessInvite( $params )
	{
		$tpl = self::getTemplate( Core\IO::dir( 'backend' ) . '/emails/witnessInvite.html' );
		$tpl->params = $params;
		return $tpl->render();
	}

	protected static function getBodyRequestLiveTranscript( $params )
	{
		$tpl = self::getTemplate( Core\IO::dir( 'backend' ) . '/emails/requestLiveTranscript.html' );
		$tpl->params = $params;
		return $tpl->render();
	}

	protected static function getBodyAdminNotification( $content )
	{
		$tpl = self::getTemplate(Core\IO::dir( 'backend' ) . '/emails/adminNotification.html' );
		$tpl->notificationBody = $content;
		return $tpl->render();
	}

	protected static function getBodyEnterpriseAgreement( $params )
	{
		$tpl = self::getTemplate( Core\IO::dir( 'backend' ) . '/emails/enterpriseAgreement.html' );
		$tpl->params = $params;
		return $tpl->render();
	}

	protected static function getBodyNewEnterprise( $params )
	{
		$tpl = self::getTemplate( Core\IO::dir( 'backend' ) . '/emails/newEnterpriseAccount.html' );
		$tpl->params = $params;
		return $tpl->render();
	}

	public static function getSiteURL()
	{
		// ED-1542 Set http_host based on if we are running apache or via CLI
		if( isset( $_SERVER['HTTP_HOST'] ) ) {
			$host = $_SERVER['HTTP_HOST'];
		} else {
			$host = Core\Register::getInstance()->config['http_host'];
		}
		return 'https://' . $host;
	}

	public static function getWebAppSiteURL()
	{
		$host = Core\Register::getInstance()->config['webapp_host'];
		return 'https://' . $host;
	}

	/**
    * this mailer will only be used for next mailing operation
    * @param \PHPMailer $mailer
    */
	public function setMailer(\PHPMailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
    * @return \PHPMailer
    */
	protected function getMailer()
	{
		if ($this->mailer === NULL) {
			return new Mailer();
		} else {
			$mailer = $this->mailer;
			$this->mailer = null;
			return $mailer;
		}
	}

	/**
    * Send password recovery email
    * @param array $userInfo
    * @param string $url
    * @return \ClickBlocks\Utils\Mailer
    */
	public static function sendPasswordRecovery(array $userInfo, $url) {
		$mailer = new Mailer(); //$this->getMailer();
		$mailer->Subject = "eDepoze - Password recovery";
		$userName = $userInfo['firstName'] . " " . $userInfo['lastName'];
		$email = $userInfo['email'];
		$mailer->AddAddress($email, $userName);
		$body = self::getBodyGeneric($userName,
			'Password change request has been issued. To change password, proceed to the following link:<br/> '.$url);
		$mailer->Body = $body;
		$mailer->Send();
		return $mailer;
	}

	/**
    * Send court reporter invite email
    * @param int $depositionID
    * @return \ClickBlocks\Utils\Mailer
    */
	public static function sendCourtReporterInvite( $depositionID, $email, $password ) {
		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		$depoCase = $deposition->cases[0];
		$mailer = new Mailer();
		$mailer->Subject = "{$deposition->depositionOf} Exhibits";
		$mailer->AddAddress( $email );
		$mailer->Body = self::getBodyCourtReporterInvite( [
			'url' => self::getSiteURL(). '/courts/exhibits/login',
			'host' => self::getSiteURL(),
			'email' => $email,
			'depositionOf' => $deposition->depositionOf,
			'depositionID' => $deposition->ID,
			'reporterPassword' => $password,
			'clientName' => $depoCase->clients[0]->name,
			'caseName' => $depoCase->name,
			'depositionOwner' => $deposition->owner[0]->firstName . ' ' . $deposition->owner[0]->lastName
		] );
		$sent = $mailer->Send();
		return $sent;
	}

	/**
    * Send witness invite email
    * @param string $witnessEmail
    * @return \ClickBlocks\Utils\Mailer
    */
	public static function sendWitnessInvite( \ClickBlocks\DB\Depositions $deposition, \ClickBlocks\DB\Users $fromUser, $witnessEmail )
	{
		if( !$deposition || !$deposition->ID || !$fromUser || !$fromUser->ID || !$witnessEmail ) {
			return FALSE;
		}
		$owner = $deposition->owner[0];
		$case = $deposition->cases[0];
		$client = $case->clients[0];
		$dAuth = new \ClickBlocks\DB\DepositionsAuth( $deposition->ID );
		if( !$dAuth || !$dAuth->ID || $dAuth->ID != $deposition->ID ) {
			return FALSE;
		}
		$mailer = new Mailer();
		$mailer->SetFrom( $fromUser->email, "{$fromUser->firstName} {$fromUser->lastName}" );
		$mailer->ClearReplyTos();
		$mailer->AddReplyTo( $fromUser->email );
		$mailer->Subject = "eDepoze - Witness Invitation (#{$deposition->ID})";
		$mailer->AddAddress( $witnessEmail );
		$mailer->Body = self::getBodyWitnessInvite( [
			'url' => self::getWebAppSiteURL() . '/witness',
			'host' => self::getSiteURL(),
			'email' => $witnessEmail,
			'witness' => $deposition->depositionOf,
			'sessionID' => $deposition->ID,
			'sessionPasscode' => $dAuth->getPasscode(),
			'clientName' => $client->name,
			'caseName' => $case->name,
			'owner' => "{$owner->firstName} {$owner->lastName}"
		] );
		$mailer->Send();
		return $mailer;
	}

	/**
     * Send Request Live Transcript
	 * @param \ClickBlocks\DB\Depositions $deposition
	 * @param \ClickBlocks\DB\Users $fromUser
     * @param string $reporterEmail
     * @return \ClickBlocks\Utils\Mailer
     */
	public static function sendRequestLiveTranscript( \ClickBlocks\DB\Depositions $deposition, \ClickBlocks\DB\Users $fromUser, $reporterEmail )
	{
		if( !$deposition || !$deposition->ID || !$fromUser || !$fromUser->ID || !$reporterEmail ) {
			return FALSE;
		}
		$owner = $deposition->owner[0];
		$case = $deposition->cases[0];
		$client = $case->clients[0];
		$dAuth = new \ClickBlocks\DB\DepositionsAuth( $deposition->ID );
		if( !$dAuth || !$dAuth->ID || $dAuth->ID != $deposition->ID ) {
			return FALSE;
		}
		$mailer = new Mailer();
		$mailer->SetFrom( $fromUser->email, "{$fromUser->firstName} {$fromUser->lastName}" );
		$mailer->ClearReplyTos();
		$mailer->AddReplyTo( $fromUser->email );
		$mailer->Subject = "eDepoze - Request Live Transcript (#{$deposition->ID})";
		$mailer->AddAddress( $reporterEmail );
		$mailer->Body = self::getBodyRequestLiveTranscript( [
			'reporterEmail' => $reporterEmail,
			'host' => self::getSiteURL(),
			'email' => $reporterEmail,
			'witness' => $deposition->depositionOf,
			'sessionID' => $deposition->ID,
			'clientName' => $client->name,
			'caseName' => $case->name,
			'owner' => "{$owner->firstName} {$owner->lastName}",
			'scheduled' => date( 'm/d/Y', strtotime( $deposition->openDateTime ) )
		] );
		$mailer->Send();
		return $mailer;
	}

	public static function sendAdminNotification( array $params )
	{
		$mailer = new Mailer();
		$mailer->SetFrom( 'info@edepoze.com', 'eDepoze Administrator' );
		$mailer->Subject = $params['subject'];
		$mailer->AddAddress( $params['sendToAddress'], $params['sendToName'] );
		$mailer->Body = self::getBodyAdminNotification( $params['content'] );
		$mailer->Send();
		return $mailer;
	}

	protected static function sendSimpleAttachments($subject, $text, array $emails, $attachment) {
		$arrFail = array();
		$mailer = new Mailer();
		$mailer->Subject = $subject;
		foreach ((array)$attachment as $k=>$file) $mailer->AddAttachment($file, is_numeric($k)?'':$k);

		foreach($emails as $email=>$name)
		{
			$m = clone $mailer;
			$m->AddAddress($email, $name);
			$body = self::getBodyGeneric($name, $text);
			$m->Body = $body;
			$m->Send();
			if ($m->IsError())
				array_push($arrFail, $email);
		}
		return $arrFail;
	}

	public static function sendOfficialTranscript(array $emails, $attachment) {
		return self::sendSimpleAttachments(
			"eDepoze - Transcript",
			'You are receiving the transcript in an electronic pdf format. This transcript is considered official document and can be validated.',
			$emails, $attachment);
	}

	public static function sendExhibits(array $emails, $attachment) {
		return self::sendSimpleAttachments(
			"eDepoze - Exhibits",
			'You are receiving the exhibits archive in zip format.',
			$emails, $attachment);
	}

	public static function sendTranscriptAndExhibits(array $emails, $attachment) {
		return self::sendSimpleAttachments(
			"eDepoze - Transcript & Exhibits",
			'You are receiving the transcript in an electronic pdf format (is considered official document and can be validated) and exhibits archive in zip format.',
			$emails, $attachment);
	}

	public static function sendEnterpriseResellerRequest_dumb( $userID, $agreementID )
	{
		$user = (new DB\ServiceUsers())->getByID( $userID );
		$agreement = (new DB\ServiceEnterpriseAgreements())->getByID( $agreementID );

		$client = (new DB\ServiceClients())->getByID( $agreement->enterpriseClientID );
		$reseller = (new DB\ServiceClients())->getByID( $agreement->enterpriseResellerID );

		$agreementsUrl = self::getSiteURL() . $reseller->URL . '/agreements';

		$mailer = new Mailer();
		$mailer->SetFrom( $user->email, "{$user->firstName} {$user->lastName}" );
		$mailer->Subject = 'The Client "'.$client->name.'" wants to partner with you';
		$mailer->AddAddress( $reseller->contactEmail, $reseller->contactName );
		$mailer->Body = "The Client <b>{$client->name}</b> would like to be able to schedule depositions through <b>{$reseller->name}</b> using eDepoze."
			. " Please review their request on your <a href=\"$agreementsUrl\">Non-Sponsored Agreements</a> page of the eDepoze Cloud Manager."
			. '<br /><br />------------------------'.'<br /><br />'.MAIL_CONFIDENTIAL_FOOTER.'<br />This message was sent by an automated service, please do not reply.';
		$mailer->Send();

		return $mailer;
	}

	public static function sendEnterpriseResellerRequest( \ClickBlocks\DB\Users $fromUser, \ClickBlocks\DB\EnterpriseAgreements $agreement )
	{
		if( !$fromUser || !$fromUser->ID || !$agreement || !$agreement->ID ) {
			return;
		}
		$client = new \ClickBlocks\DB\Clients( $agreement->enterpriseClientID );
		if( !$client || !$client->ID || $client->ID != $agreement->enterpriseClientID ) {
			return;
		}
		$reseller = new \ClickBlocks\DB\Clients( $agreement->enterpriseResellerID );
		if( !$reseller || !$reseller->ID || $reseller->ID != $agreement->enterpriseResellerID ) {
			return;
		}
		$agreementsUrl = self::getSiteURL() . "/{$reseller->URL}/agreements";
		$mailer = new Mailer();
		$mailer->SetFrom( $fromUser->email, "{$fromUser->firstName} {$fromUser->lastName}" );
		$mailer->ClearReplyTos();
		$mailer->AddReplyTo( $fromUser->email );
		$mailer->Subject = 'The Client "' . $client->name . '" wants to partner with you';
		$mailer->AddAddress( $reseller->contactEmail, $reseller->contactName );
		$mailer->Body = self::getBodyEnterpriseAgreement( [
			'url' => $agreementsUrl,
			'host' => self::getSiteURL(),
			'contact' => $reseller->contactName,
			'clientName' => $client->name,
			'resellerName' => $reseller->name
		] );
		$mailer->Send();
		return $mailer;
	}

	public static function sendEnterpriseAdminNotification(\ClickBlocks\DB\Clients $client, \ClickBlocks\DB\Users $fromUser, \ClickBlocks\DB\OrchestraUsers $OU)
	{
		if( !$client || !$client->ID || !$fromUser || !$fromUser->ID ) {
			return;
		}

		$superAdminIds = $OU->getUserIdsByType(\ClickBlocks\MVC\Edepo::USER_TYPE_SUPERADMIN);
		if(is_array($superAdminIds)) {
			foreach($superAdminIds as $id) {
				$toUser = new DB\Users($id);
				if( !$toUser || !$toUser->ID ) {
					continue;
				}
				//$email = $toUser->email;
				$mailer = new Mailer();
				$mailer->SetFrom( $fromUser->email, "{$fromUser->firstName} {$fromUser->lastName}" );
				$mailer->ClearReplyTos();
				$mailer->AddReplyTo( $fromUser->email );
				$mailer->Subject = 'New Enterprise "' . $client->name . '" needs to be activated.';
				$mailer->AddAddress( $toUser->email, "{$toUser->firstName} {$toUser->lastName}" );
				$mailer->Body = self::getBodyNewEnterprise( [
					'host' => self::getSiteURL(),
					'contact' => "{$toUser->firstName} {$toUser->lastName}",
					'clientName' => $client->name,
					'userName' => "{$fromUser->firstName} {$fromUser->lastName}"
				] );
				$mailer->Send();
			}
		}
	}

	/**
     * Send Account Activation Email
     * @param Array $user
     * @return \ClickBlocks\Utils\Mailer
     */
	public static function sendAccountActivationEmail( \ClickBlocks\DB\Users $user ) {
		if( !$user->ID || !$user->email ) {
			return;
		}
		$cfg = Core\Register::getInstance()->config;
		$mailer = new Mailer();
		$mailer->SetFrom( $cfg->email['fromEmail'], $cfg->email['fromName'] );
		$mailer->Subject = 'eDepoze - Account Confirmation';
		$mailer->AddAddress( $user->email );
		$host = self::getSiteURL();
		$b64email = rawurlencode( base64_encode( $user->email ) );
		$params = [
			'host' => $host,
			'url' => "{$host}{$cfg->signupPath}/confirm?a={$b64email}",
			'email' => $user->email,
			'fullName' => "{$user->firstName} {$user->lastName}",
			'username' => $user->username
		];
		$mailer->Body = self::getBodyAccountActivation( $params );
		$mailer->Send();
		return $mailer;
	}

	protected static function getBodyAccountActivation( $params ) {
		$tpl = self::getTemplate( Core\IO::dir( 'backend' ) . '/emails/accountActivation.html' );
		$tpl->params = $params;
		return $tpl->render();
	}
}
