<?php

namespace ClickBlocks;

class FFProbe {

	private $cfg;
	private $metadata;
	private $filename;

	/**
	 * FFProbe
	 * @param string $filename
	 * @throws \Exception
	 */
	public function __construct( $filename ) {
		if( !file_exists( $filename ) ) {
			throw new \Exception( "File does not exist: {$filename}" );
		}
		$this->cfg = \ClickBlocks\Core\Register::getInstance()->config->ffprobe;
		$this->filename = $filename;
		$this->probe();
	}

	/**
	 * execute ffprobe on file
	 * @throws \Exception
	 */
	private function probe() {
		// Avoid escapeshellarg() issues with UTF-8 filenames
		setlocale( LC_CTYPE, 'en_US.UTF-8' );

		// Run the ffprobe, save the JSON output then decode
		$cmd = sprintf( '%s %s %s', $this->cfg['bin'], $this->cfg['args'], escapeshellarg( $this->filename ) );
		$output = shell_exec( $cmd );
//		Debug::ErrorLog( $output );
		$this->metadata = json_decode( $output );

		if( !$this->metadata || !isset( $this->metadata->format ) ) {
			throw new \Exception( 'Unsupported file type' );
		}
	}

	/**
	 * H.264 video with AAC (low complexity) audio
	 * @return boolean
	 */
	public function videoIsH264withAAC() {
		if( !$this->metadata ) {
			return FALSE;
		}
		//check MP4 video
		$formatName = explode( ',', $this->metadata->format->format_name );
		if( !$formatName || !is_array( $formatName ) || !in_array( 'mp4', $formatName ) ) {
			return FALSE;
		}
		//check streams for H.264 and AAC
		if( !$this->metadata->streams || !is_array( $this->metadata->streams ) ) {
			return FALSE;
		}
		$isH264 = FALSE;
		$isAAC = FALSE;
		foreach( $this->metadata->streams as $stream ) {
			if( $stream->codec_type == 'video' ) {
				$isH264 = ($stream->codec_name == 'h264' && $stream->profile == 'Constrained Baseline');
				if( !$isH264 ) {
					return FALSE;
				}
			} elseif( $stream->codec_type == 'audio' ) {
				$isAAC = ($stream->codec_name == 'aac');
				if( !$isAAC ) {
					return FALSE;
				}
			}
		}
		return ($isH264 && $isAAC);
	}

	/**
	 * MP3 audio
	 * @return boolean
	 */
	public function audioIsMP3() {
		if( $this->metadata->format->format_name != 'mp3' ) {
			return FALSE;
		}
		$isMP3 = FALSE;
		foreach( $this->metadata->streams as $stream ) {
			if( $stream->codec_type == 'audio' ) {
				return ($stream->codec_name == 'mp3');
			}
		}
		return $isMP3;
	}

	/**
	 * has video streams
	 * @return boolean
	 */
	public function isVideo() {
		if( !$this->metadata ) {
			return FALSE;
		}
		if( $this->metadata->format->format_name == 'mp3' ) {
			return FALSE;
		}
		foreach( $this->metadata->streams as $stream ) {
			if( $stream->codec_type == 'video' ) {
				return TRUE;
			}
		}
	}

}
