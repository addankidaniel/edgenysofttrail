<?php

namespace ClickBlocks;

/**
 * Courtroom Connect Transcripts
 */
class CCTranscripts {

	/**
	 * Convert PTF transcript to PDF
	 * @param string $ptf Path to PTF
	 * @param string $pdf Output to PDF
	 * @return boolean
	 */
	public static function convertPTF( $ptf, $pdf )
	{
		if( !file_exists( $ptf ) ) {
			\ClickBlocks\Debug::ErrorLog( "CCTranscripts::convertPTF; Missing file: {$ptf}" );
			return FALSE;
		}
		$fhPTF = fopen( $ptf, 'r' );
		if( !$fhPTF ) {
			\ClickBlocks\Debug::ErrorLog( "CCTranscripts::convertPTF; Unable to open ptf: {$ptf}" );
			return FALSE;
		}
		$txt = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.html';
		$fhTXT = fopen( $txt, 'w+' );
		if( !$fhTXT ) {
			\ClickBlocks\Debug::ErrorLog( "CCTranscripts::convertPTF; Unable to open txt: {$txt}" );
			return FALSE;
		}
		flock( $fhTXT, LOCK_EX );
		$section = NULL;
		$lineNum = 1;
		$pageNum = 1;
		while( ($buffer = fgets( $fhPTF, 4096 )) !== FALSE ) {
			if( !strpos( $buffer, '=' ) ) {
				\ClickBlocks\Debug::ErrorLog( "CCTranscripts::convertPTF; invalid format: {$buffer}" );
				continue;
			}
			$cmp = explode( '=', $buffer );
			if( isset( $cmp[0] ) && $cmp[0] === 'begin' ) {
				$section = trim( $cmp[1] );
				if( $section === 'Text' ) {
					fwrite( $fhTXT, '<html><body style="font-family:calibri;">' );
				}
			} elseif( isset( $cmp[0] ) && $cmp[0] === 'end' ) {
				if( $section === 'Text' ) {
					fwrite( $fhTXT, '</body></html>' );
				}
				$section = NULL;
			} else if( $section === 'Text' ) {
				if( stripos( $buffer, 'fmt=pb' ) === 0 ) {
//					fwrite( $fhTXT, '</pre><pre>' );
					$lineNum = 1;
					++$pageNum;
					continue;
				}
				$text = rtrim( $cmp[1] );
				if( $lineNum === 1 ) {
					$text = self::styleTextLine( "{$pageNum}:{$lineNum}", $text );
				} else {
					$text = self::styleTextLine( $lineNum, $text );
				}
				fwrite( $fhTXT, $text );
				++$lineNum;
			}
		}
		fflush( $fhTXT );
		flock( $fhTXT, LOCK_UN );
		fclose( $fhTXT );
		$wkhtmltopdf = new \ClickBlocks\Utils\WKHTMLToPDF( $txt, $pdf );
		$wkhtmltopdf->setPageSize( 'Letter' );
		$wkhtmltopdf->setOrientation( 'Portrait' );
		$wkhtmltopdf->addParam( '--no-background', TRUE );
		$okToGo = $wkhtmltopdf->render();
		if( file_exists( $ptf ) ) {
			unlink( $ptf );
		}
		if( file_exists( $txt ) ) {
			unlink( $txt );
		}
		return $okToGo;
	}

	private static function styleTextLine( $lineNum, $text )
	{
		return <<<EOL
<table border="0" width="100%" style="border:0;border-collapse:collapse;">
<tr>
	<td width="60" style="font-size:12px;border-right:1px solid #999;text-align:right;padding-right:8px;color:#bbb;">{$lineNum}</td>
	<td style="font-size:13px;padding-left:8px;white-space:pre;color:#000;">{$text}</td>
</tr>
</table>
EOL;
	}
}
