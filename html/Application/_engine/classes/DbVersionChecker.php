<?php

namespace ClickBlocks\Utils;
use ClickBlocks\DB;

class DbVersionChecker
{
    public static function validate($scriptDir)
    {
        return new self($scriptDir);
    }
    
    public function __construct($scriptDir)
    {
        $this->validateDbVersion($scriptDir);
    }

    private function validateDbVersion($scriptDir)
    {
       
        $dbVersion = $this->getDbVersion();
        $latestVersion = $this->getLatestVersion($scriptDir);
        if ($dbVersion < $latestVersion) {
            die('Your database schema is out of date. Current version is '. $latestVersion . ', but your version is ' . $dbVersion . '.');
        }
    }
    
    /**
     *
     * @param object $dbObject: is a Orchestra::getDB() object
     * @return integer 
     */
    private function getDbVersion()
    {
        return (int) foo(new DB\OrchestraDbVersion())->getDbVersion();
    }

    private function getLatestVersion($scriptDir)
    {
        $scriptDir = realpath($scriptDir);
        $filenames = array();
        foreach (scandir($scriptDir) as $filename) {
            $filenames[] = $filename;
        }

        $latestFilename = max($filenames);
        $versions = preg_split('/_/', $latestFilename, 2);
        $latestVersion = (int)reset($versions);

        return $latestVersion;
    }
}