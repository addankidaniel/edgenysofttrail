<?php

namespace ClickBlocks;

use ClickBlocks\Core,
	ClickBlocks\Core\Register,
	Elasticsearch\ClientBuilder,
	ClickBlocks\PDFDaemon,
	ClickBlocks\Debug,
	ClickBlocks\DB\OrchestraFolders,
	ClickBlocks\DB\OrchestraFiles;

// require_once( 'Elasticsearch/vendor/autoload.php' );

class Elasticsearch extends \ClickBlocks\Singleton {

	protected static $config;
	protected static $client;
	protected static $mappings = [
		'PDF' => [
			'_source' => [
				'enabled' => TRUE
			],
			'properties' => [
				'fileID' => [
					'type' => 'integer',
					'index' => 'not_analyzed'
				],
				'folderID' => [
					'type' => 'integer',
					'index' => 'not_analyzed'
				],
				'caseID' => [
					'type' => 'integer',
					'index' => 'not_analyzed'
				],
				'sessionID' => [
					'type' => 'integer',
					'index' => 'not_analyzed'
				],
				'fileText' => [
					'type' => 'string'
				],
				'fileName' => [
					'type' => 'string',
					'analyzer' => 'simple',
					'search_analyzer' => 'simple'
				],
				'fileName_std' => [
					'type' => 'string',
					'analyzer' => 'standard',
					'search_analyzer' => 'standard'
				]
			]
		]
	];

	protected static function init() {

		self::$config = (object)Register::getInstance()->config->elasticsearch;
		self::$config->disabled = true;
		if( self::$config->disabled ) {
			return;
		}

		$url = self::$config->host . ':' . self::$config->port;

		// Set the client
		self::$client = ClientBuilder::create()->setHosts( [ $url ] )->build();
	}

	/**
	 * Create an Elasticsearch index
	 * @param string $indexName
	 * @return array
	 */
	public static function createIndex( $indexName ) {
		self::getInstance();
		self::$config = (object)Register::getInstance()->config->elasticsearch;
		if( self::$config->disabled ) {
			return;
		}

		$params = [
			'index' => $indexName,
			'body' => [
				'settings' => [
					'number_of_shards' => self::$config->shards,
					'number_of_replicas' => self::$config->replicas
				],
				'mappings' => self::$mappings
			]
		];

		Debug::ErrorLog( "[Elasticsearch] Creating index '{$indexName}'" );
		$response = self::$client->indices()->create( $params );
		return $response;
	}

	/**
	 * Check if an Elasticsearch index exists
	 * @param string $indexName
	 * @return bool
	 */
	public static function indexExists( $indexName ) {
		self::getInstance();
		if( self::$config->disabled ) {
			return;
		}
		$params = [ 'index' => "{$indexName}" ];
		$exists = self::$client->indices()->exists( $params );
		return $exists;
	}


	/**
	 * Update an Elasticsearch index details
	 * @param string $indexName
	 * @param int $shards
	 * @param int $replicas
	 * @return array
	 */
	public static function updateIndex( $indexName, $shards, $replicas ) {
		self::getInstance();
		if( self::$config->disabled ) {
			return;
		}

		$params = [
			'index' => $indexName,
			'body' => [
				'settings' => [
					'number_of_shards' => $shards,
					'number_of_replicas' => $replicas
				]
			]
		];

		$response = self::$client->indices()->putSettings( $params );
		return $response;
	}


	/**
	 * Delete an Elasticsearch index
	 * @param string $indexName
	 * @return array
	 */
	public static function deleteIndex( $indexName ) {
		self::getInstance();
		self::$config = (object)Register::getInstance()->config->elasticsearch;
		if( self::$config->disabled ) {
			return;
		}

		if( !self::indexExists( $indexName ) ) {
			Debug::ErrorLog( "[Elasticsearch::deleteIndex] index: '{$indexName}' does not exist" );
			return;
		}

		$params = [ 'index' => $indexName ];

		Debug::ErrorLog( "[Elasticsearch::deleteIndex] Deleting index: '{$indexName}'" );
		$response = self::$client->indices()->delete( $params );
		Debug::ErrorLog( '[Elasticsearch::deleteIndex] Deleted: ' . json_encode( $response, JSON_UNESCAPED_SLASHES ) );
		return $response;
	}

	/**
	 * Index a file in Elasticsearch
	 * @param int $fileID
	 * @param string $type
	 * @return array
	 */
	public static function indexDocument( $fileID, $type='PDF' ) {
		self::getInstance();
		if( self::$config->disabled ) {
			return;
		}

		$file = new DB\Files( $fileID );
		$filePath = $file->getFullName();

		if( !file_exists( $filePath ) ) {
			Debug::ErrorLog( "[Elasticsearch] Unable to index fileID: {$fileID}, file does not exist." );
			return FALSE;
		}
		$folder = new DB\Folders( $file->folderID );

		if( $folder->caseID ) {
			$caseID = $folder->caseID;
			$sessionID = null;
		} else {
			$session = new DB\Depositions( $folder->depositionID );
			$caseID = $session->caseID;
			$sessionID = $session->ID;
		}

		$case = new DB\Cases( $caseID );
		$fileText = PDFDaemon::getText( $filePath );
		if( $fileText === NULL ) {
			Debug::ErrorLog( "[Elasticsearch] fileID: {$fileID}, indexed '{$file->name}', but no text extracted." );
		}

		$orcFolders = new OrchestraFolders();
		$allFolders = $orcFolders->getFoldersForFile( $fileID );
		$fileFolders = [];

		if( $allFolders ) {
			foreach( $allFolders as $fileFolder ) {
				$fileFolders[] = (int)$fileFolder[ 'ID' ];
			}
		} else {
			$fileFolders[] = (int)$folder->ID;
		}

		$indexName = self::$config->index_prefix . 'client:' . $case->clientID;
		if( !self::indexExists( $indexName ) ) {
			self::createIndex( $indexName );
		}

		// Index a document
		$index_params = [
			'index' => $indexName,
			'type'  => $type,
			'id'    => $fileID,
			'body'  => [
				'fileID' => $fileID,
				'fileName' => $file->name,
				'fileName_std' => $file->name,
				'folderID' => $fileFolders,
				'caseID' => $caseID,
				'sessionID' => $sessionID,
				'fileText' => $fileText
			]
		];

		$response = self::$client->index( $index_params );
		if( $response['created'] > 0 || $response['_version'] > 0 ) {
			Debug::ErrorLog( "[Elasticsearch::indexDocument] Success for fileID: {$fileID}" );
		} else {
			Debug::ErrorLog( "[Elasticsearch::indexDocument] Indexing error for fileID: {$fileID}" );
			throw new \Exception( "[Elasticsearch::indexDocument] Indexing error for fileID: {$fileID}" );
		}
		return $response;
	}

	/**
	 * Search Elasticsearch for given terms
	 * @param string $indexName
	 * @param string $terms
	 * @param string $type
	 * @param array $options [ 'searchContent' = boolean, 'includeFilename' = boolean, 'folderID' = integer, 'caseID' = integer ]
	 * @return array
	 */
	public static function search( $indexName, $terms, $options, $type='PDF' ) {
		self::getInstance();
		if( self::$config->disabled ) {
			return;
		}

		$indexName = self::$config->index_prefix . $indexName;
		if( !self::indexExists( $indexName ) ) {
			Debug::ErrorLog( "[Elasticsearch::search] Index '{$indexName}' was not found." );
			return FALSE;
		}
		Debug::ErrorLog( "[Elasticsearch::search] Searching index '{$indexName}' for '{$terms}'" );

		// 10000 being a large enough number that it should give us what we want?
		// This can really slow things down if we get this high! Might consider
		// reducing it if we have issues.
		$quantity = 10000;
		$startNumber = 0; // Zero means skip no results, start at 1.

		$fieldMatch = [];
		$returnFields = [ 'fileID', 'folderID', 'caseID', 'sessionID' ];
		$filterMatch = [];

		// Let's get the actual search phrases and terms
		$phrases = [];
		preg_match_all( '/(?!AND|OR|NOT)(?<!")\b\w+\b|(?<=")\b[^"]+/', $terms, $phrases, PREG_PATTERN_ORDER );

		if ( $options[ 'searchContent' ] || ( !$options[ 'includeFilename' ] && !$options[ 'searchContent' ] ) ) {
			$fieldMatch[] = [ 'query_string' => [ 'fields' => [ 'fileText' ], 'query' => "{$terms}" ] ];
		}
		if ( $options[ 'includeFilename' ] ) {
			if( count( $phrases[ 0 ] ) > 1 ) {
				$fieldMatch[] = [ 'query_string' => [ 'fields' => [ 'fileName^5' ], 'query' => "{$terms}" ] ];
			} else {
				$fileName = str_replace( '*', '', $terms );
				$fieldMatch[] = [ 'query_string' => [ 'fields' => [ 'fileName_std^5' ], 'query' => "*{$fileName}*" ] ];
			}
		}
		if ( $options[ 'startNumber' ] && $options[ 'startNumber' ] >= 0 ) {
			$startNumber = (int)$options[ 'startNumber' ];
		}
		if ( $options[ 'quantity' ] && $options[ 'quantity' ] > 0 ) {
			$quantity = (int)$options[ 'quantity' ];
		}
		if ( $options[ 'highlight' ] ) {
			$returnFields[] = 'fileText';
		}
		if ( $options[ 'folderID' ] ) {
			if( !is_array( $options[ 'folderID' ] ) ) {
				$options[ 'folderID' ] = [ $options[ 'folderID' ] ];
			}
			foreach( $options[ 'folderID' ] as $folderID ) {
				$filterMatch[] = [ 'match' => [ 'folderID' => $folderID ] ];
			}
		} else if ( $options[ 'caseID' ] ) {
			$filterMatch[] = [ 'match' => [ 'caseID' => $options[ 'caseID' ] ] ];
		} else if ( $options[ 'fileID' ] ) {
			$filterMatch[] = [ 'match' => [ 'fileID' => $options[ 'fileID' ] ] ];
		}

		// Search a document
		$params = [
			'index' => $indexName,
			'type' => $type,
			'body' => [
				'fields' => $returnFields,
				'size' => $quantity,
				'from' => $startNumber,
				'query' => [
					'bool' => [
						'must' => [
							'bool' => [
								'should' => $fieldMatch,
								'minimum_should_match' => 1
							]
						],
						'should' => $filterMatch,
						'minimum_should_match' => 1
					]
				]
			]
		];

		Debug::ErrorLog( '[Elasticsearch::search] params: ' . json_encode( $params, JSON_UNESCAPED_SLASHES ) );

		try {
			$results = self::$client->search( $params );
		} catch( \Exception $err ) {
			Debug::ErrorLog( '[Elasticsearch::search] caught: ' . $err->getMessage() );
			throw new \Exception( $err->getMessage() );
		}

		$logResults = $results;
		unset( $logResults['hits']['hits'] );
		Debug::ErrorLog( '[Elasticsearch::search] results: ' . json_encode( $logResults, JSON_UNESCAPED_SLASHES ) );
		unset( $logResults );

		$fileIDs = [];
		foreach( $results[ 'hits' ][ 'hits' ] as $hit ) {
			$fileIDs[] = (int)$hit[ 'fields' ][ 'fileID' ][ 0 ];
		}

		$orcFiles = new OrchestraFiles();
		$rows = $orcFiles->getContentSearchResults( $fileIDs );
		Debug::ErrorLog( '[Elasticsearch::search] rows: ' . count( $rows ) );
		if( count( $rows ) !== $results['hits']['total'] ) {
			Debug::ErrorLog( "[Elasticsearch::search] results vs rows count mistmatch! (Index: {$indexName})" );
		}

		foreach( $results[ 'hits' ][ 'hits' ] as &$match ) {
			$fileID = (int)$match[ 'fields' ][ 'fileID' ][ 0 ];
			$row = $rows[ $fileID ];
			$match[ 'fileSize' ] = $row[ 'hrFileSize' ];
			$match[ 'fileID' ] = $fileID;
			$match[ 'fileName' ] = $row[ 'name' ];
			$match[ 'folderName' ] = $row[ 'folderName' ];
			$match[ 'sessionName' ] = $row[ 'sessionName' ];

			if ( $options[ 'highlight' ] ) {
				// Parse out matched terms
				$highlight = [];
				$text = strtolower( $match[ 'fileName' ] . ' ' . $match[ 'fields' ][ 'fileText' ][ 0 ] );
				foreach( $phrases[ 0 ] as $checkPhrase ) {
					if( strstr( $text, strtolower( $checkPhrase ) ) ) {
						$highlight[ strtolower( $checkPhrase ) ] = TRUE;
					}
				}
				$match[ 'highlight' ] = array_keys( $highlight );
			}
			//unset( $results[ 'hits' ][ 'hits' ][ $idx ][ 'fields' ][ 'fileText' ] );
			unset( $match[ 'fields' ] );
		}

		$results[ 'hits' ][ 'totalReturned' ] = count( $results[ 'hits' ][ 'hits' ] );
		$results[ 'hits' ][ 'searchedTerms' ] = $phrases[ 0 ];
		$logResults = $results;
		unset( $logResults[ 'hits' ][ 'hits' ] );
		Debug::ErrorLog( '[Elasticsearch::search] return: ' . json_encode( $logResults, JSON_UNESCAPED_SLASHES ) );
		return $results[ 'hits' ];
	}

	/**
	 * Delete an Elasticsearch document by ID
	 * @param int $fileID
	 * @param string $type
	 * @return array
	 */
	public static function deleteDocument( $fileID, $type='PDF' ) {
		Debug::ErrorLog( "[Elasticsearch::deleteDocument] Deleting fileID: {$fileID}" );
		self::getInstance();
		if( self::$config->disabled ) {
			return;
		}

		$file = new DB\Files( $fileID );
		$folder = new DB\Folders( $file->folderID );
		$session = new DB\Depositions( $folder->depositionID );
		$case = new DB\Cases( $session->caseID );
		$indexName = self::$config->index_prefix . 'client:' . $case->clientID;

		if( !self::indexExists( $indexName ) ) {
			Debug::ErrorLog( "[Elasticsearch::deleteDocument] Unable to delete document. Index '{$indexName}' was not found." );
			return FALSE;
		}

		$params = [
			'index' => $indexName,
			'type' => $type,
			'id' => $fileID
		];

		try {
			Debug::ErrorLog( '[Elasticsearch::deleteDocument] params: ' . json_encode( $params, JSON_UNESCAPED_SLASHES ) );
			$response = self::$client->delete( $params );
			Debug::ErrorLog( '[Elasticsearch::deleteDocument] response: ' . json_encode( $response, JSON_UNESCAPED_SLASHES ) );
		} catch( \Exception $e ) {
			Debug::ErrorLog( [ '[Elasticsearch::deleteDocument] Exception:', [ 'message' => $e->getMessage(), 'code' => $e->getCode(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ] );
		}

		if( $response[ 'found' ] == 1 ) {
			Debug::ErrorLog( "[Elasticsearch::deleteDocument] Success for fileID: {$fileID}" );
		} else {
			Debug::ErrorLog( "[Elasticsearch::deleteDocument] Error for fileID: {$fileID}" );
		}
		return $response;
	}

	/**
	 * Delete multiple Elasticsearch documents by their IDs
	 * @param array $fileIDs
	 * @param int $clientID
	 * @param string $type
	 * @return array
	 */
	public static function deleteMultipleDocuments( array $fileIDs, $clientID, $type='PDF' ) {
		self::getInstance();
		if( self::$config->disabled ) {
			return;
		}

		$indexName = self::$config->index_prefix . 'client:' . $clientID;
		if( !self::indexExists( $indexName ) ) {
			Debug::ErrorLog( "[Elasticsearch::deleteMultipleDocuments] Unable to delete documents. Index '{$indexName}' was not found." );
			return FALSE;
		}

		//$ids = implode( ' OR ', $fileIDs );
		//Debug::ErrorLog( "[Elasticsearch::deleteMultipleDocuments] Deleting document IDs: {$ids}" );

		$params = [
			'index' => $indexName,
			'type' => $type,
			'body' => [
				'query' => [
					'bool' => [
						'should' => []
					]
				]
			]
		];
		foreach( $fileIDs as $fileID ) {
			$params[ 'body' ][ 'query' ][ 'bool' ][ 'should' ][] =  [ 'match' => [ 'fileID' => $fileID ] ];
		}

		try {
			Debug::ErrorLog( '[Elasticsearch::deleteMultipleDocuments] params: ' . json_encode( $params, JSON_UNESCAPED_SLASHES ) );
			$response = self::$client->deleteByQuery( $params );
			Debug::ErrorLog( '[Elasticsearch::deleteMultipleDocuments] response: ' . json_encode( $response, JSON_UNESCAPED_SLASHES ) );
		} catch( \Exception $e ) {
			Debug::ErrorLog( [ '[Elasticsearch::deleteMultipleDocuments] Exception:', [ 'message' => $e->getMessage(), 'code' => $e->getCode(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ] );
		}

		if( is_array( $response[ '_indices' ][ $indexName ] ) ) {
			Debug::ErrorLog( '[Elasticsearch::deleteMultipleDocuments] Success' );
		} else {
			Debug::ErrorLog( '[Elasticsearch::deleteMultipleDocuments] Failure: ' . print_r( $response, TRUE ) );
		}
		return $response;
	}

}
