<?php

namespace ClickBlocks;

class Debug {

	public static function ErrorLog( $message ) {
		if( !is_string( $message ) ) {
			$message = print_r( $message, TRUE );
		}
		$mt = substr( str_pad( microtime( TRUE ), 15, '0' ), 11 );
		$now = date( 'Y-m-d H:i:s' );
		error_log( "{$now}.{$mt}: {$message}\n", 3, "{$_SERVER['DOCUMENT_ROOT']}/../logs/php-error.log" );
	}

	public static function InfoLog( $file, $line, $desc, $info='' ) {
		if( !\ClickBlocks\MVC\MVC::isFirstRequest() ) return;
		$file = str_replace( $_SERVER['DOCUMENT_ROOT'], '', $file );
		static::ErrorLog( "{$_SERVER['REMOTE_ADDR']} " . session_id() . " {$file}:{$line} -- {$desc} " . print_r( (gettype( $info ) === 'string') ? $info : json_encode( $info ), TRUE ) );
	}

}