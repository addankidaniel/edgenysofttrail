Elasticsearch uses the service available on our AWS instances.
See the config.ini [elasticsearch] section for details.
If you want to host your own elasticsearch service, change the config
settings in your .local.ini file to point to your instance.

To setup your own elasticsearch in CentOS, see the INSTALLATION section in this
readme file.

---------------------------- GENERAL USAGE INFO --------------------------------

For most applications you can just use the Elasticsearch class, found in
elasticsearch.php in the main classes folder.

Or, notice the 'autoload.php' file. Include this in your PHP in order to load
the \Elasticsearch functions. See the sample 'indexer.php' that is included in
the directory with this README file.

There is also a search API. View the code to see what it is capable of.

Here is a sample request to the API:

https://mdaines.lexcity.com/API/2.2.0/search/search?data={"terms":"phillip AND enron","clientID":2,"sKey":"abc123xyz987"}

The API returns json results similar to this (showing just the first 2 hits for
brevity, comments are NOT part of the valid json):

{
	success: true,
	result: {
		search_result: {
			total: 1696, // Total documents matching the query
			max_score: 0.050592452, // Highest rank of any match
			hits: [ // Array of all matched documents returned for this search
				{
					_index: "mdaines:client2", // The Elasticsearch index we are searching in
					_type: "PDF", // Document type. You should not need to worry about this
					_id: "16713", // Internal ID of the document. This should always match our file ID
					_score: 0.050592452, // The rank for this particular document
					fields: { // All the data you should need to identify who/what/where the file is.
						fileName: [
							"EN000611.pdf"
						],
						sessionID: [
							"57"
						],
						folderID: [
							"1219"
						],
						fileID: [
							"16713"
						],
						caseID: [
							"47"
						]
					},
					highlight: [ // Array of all the terms in your query that matched.
						"phillip",
						"enron"
					]
				},
				{
					_index: "mdaines:client2",
					_type: "PDF",
					_id: "17457",
					_score: 0.050592452,
					fields: {
						fileName: [
							"EN001355.pdf"
						],
						sessionID: [
							"57"
						],
						folderID: [
							"1219"
						],
						fileID: [
							"17457"
						],
						caseID: [
							"47"
						]
					},
					highlight: [
						"phillip",
						"enron"
					]
				}
			]
		}
	},
	echo: ""
}

Configuration for Elasticsearch is in config.ini, similar to the following:

[elasticsearch]
host = "https://search-develop-kvepchzr2pf3kvlezq6srvb7oa.us-east-1.es.amazonaws.com"
port = "443"
index_prefix = "production:"
shards = 2
replicas = 0

You can set the index_prefix to whatever you want in your local.ini, preferably
with the ":" included as shown (e.g. I use "mdaines:". This can help to
differentiate your search indexes from everyone elses.

See the following for a list of indexes on AWS (special access required):
http://search-develop-kvepchzr2pf3kvlezq6srvb7oa.us-east-1.es.amazonaws.com/_cat/indices?v

------------------------------- INSTALLATION -----------------------------------

First you need to install the elasticsearch binary. This can be done from repos
by adding elasticsearch and then using 'yum install'.

On your dev VM, create the file /etc/yum.repos.d/elasticsearch.repo with the
following content:

[elasticsearch-2.x]
name=Elasticsearch repository for 2.x packages
baseurl=http://packages.elastic.co/elasticsearch/2.x/centos
gpgcheck=1
gpgkey=http://packages.elastic.co/GPG-KEY-elasticsearch
enabled=1

	[root@mdaines ~]# vim /etc/yum.repos.d/elasticsearch.repo

Now import the GPG key:

	[root@mdaines ~]# rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch

Now install elasticsearch:

	[root@mdaines ~]# yum install elasticsearch
	Loaded plugins: fastestmirror, versionlock
	Setting up Install Process
	Loading mirror speeds from cached hostfile
	epel/metalink                                                                                                 |  11 kB     00:00     
	 * base: repos.redrockhost.com
	 * epel: linux.mirrors.es.net
	 * extras: mirror.netdepot.com
	 * remi: mirrors.mediatemple.net
	 * remi-php56: mirrors.mediatemple.net
	 * remi-safe: mirrors.mediatemple.net
	 * rpmforge: mirror.chpc.utah.edu
	 * updates: mirror.netdepot.com
	base                                                                                                          | 3.7 kB     00:00     
	elasticsearch-2.x                                                                                             | 2.9 kB     00:00     
	elasticsearch-2.x/primary_db                                                                                  | 3.8 kB     00:00     
	epel                                                                                                          | 4.3 kB     00:00     
	epel/primary_db                                                                                               | 5.7 MB     00:05     
	extras                                                                                                        | 3.4 kB     00:00     
	extras/primary_db                                                                                             |  34 kB     00:00     
	passenger/signature                                                                                           |  836 B     00:00     
	passenger/signature                                                                                           | 1.0 kB     00:00 ... 
	passenger-source/signature                                                                                    |  836 B     00:00     
	passenger-source/signature                                                                                    | 1.0 kB     00:00 ... 
	remi                                                                                                          | 2.9 kB     00:00     
	remi/primary_db                                                                                               | 1.3 MB     00:04     
	remi-php56                                                                                                    | 2.9 kB     00:00     
	remi-php56/primary_db                                                                                         | 198 kB     00:00     
	remi-safe                                                                                                     | 2.9 kB     00:00     
	remi-safe/primary_db                                                                                          | 234 kB     00:00     
	rpm-eprints-org-xapian                                                                                        | 2.9 kB     00:00     
	rpmforge                                                                                                      | 1.9 kB     00:00     
	updates                                                                                                       | 3.4 kB     00:00     
	updates/primary_db                                                                                            | 3.3 MB     00:04     
	Resolving Dependencies
	--> Running transaction check
	---> Package elasticsearch.noarch 0:2.1.1-1 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	=====================================================================================================================================
	 Package                          Arch                      Version                       Repository                            Size
	=====================================================================================================================================
	Installing:
	 elasticsearch                    noarch                    2.1.1-1                       elasticsearch-2.x                     28 M

	Transaction Summary
	=====================================================================================================================================
	Install       1 Package(s)

	Total download size: 28 M
	Installed size: 28 M
	Is this ok [y/N]: y
	Downloading Packages:
	elasticsearch-2.1.1.rpm                                                                                       |  28 MB     00:26     
	Running rpm_check_debug
	Running Transaction Test
	Transaction Test Succeeded
	Running Transaction
	Creating elasticsearch group... OK
	Creating elasticsearch user... OK
		Installing : elasticsearch-2.1.1-1.noarch                                                                                      1/1 
	### NOT starting on installation, please execute the following statements to configure elasticsearch service to start automatically using chkconfig
	 sudo chkconfig --add elasticsearch
	### You can start elasticsearch service by executing
	 sudo service elasticsearch start
		Verifying  : elasticsearch-2.1.1-1.noarch                                                                                      1/1 

	Installed:
		elasticsearch.noarch 0:2.1.1-1                                                                                                     

	Complete!
	[root@mdaines ~]# 

Note that elasticsearch is installed as a service, and can be stopped, started, and restarted using the common service command:

	service elasticsearch [start,stop,restart]

The configuration for elasticsearch is found in a YAML file here:

	/etc/elasticsearch/elasticsearch.yml

This is where the location of the physical indices are defined, as well as any
clustering and network options are set. See the following for details on
configuration:

	https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-configuration.html

Next, the PHP extension needs to be installed. This must be done with composer,
and should be done on a machine that is NOT your dev VM. Once the composer
install is done, include the generated files in the git repository for inclusion
on dev, staging, and production.

Contents of 'composer.json':

{
		"require": {
				"elasticsearch/elasticsearch": "~2.0@beta"
		}
}

With this composer.json in place, run 'composer install'. Since my workstation
is already Linux it was simple to run this from my workstation in the SSHFS
mounted location on my VM where I actually want it to be, so I didn't need
to copy the files to my VM after the fact:

	[matthew@matthew ~/mounts/mdaines.edepoze.com/mdaines.edepoze/elasticsearch] 2016-01-18 12:17:31$ composer install
	Loading composer repositories with package information
	Installing dependencies (including require-dev)
		- Installing react/promise (v2.2.1)
			Downloading: 100%         

		- Installing guzzlehttp/streams (3.0.0)
			Downloading: 100%         

		- Installing guzzlehttp/ringphp (1.1.0)
			Downloading: 100%         

		- Installing psr/log (1.0.0)
			Downloading: 100%         

		- Installing elasticsearch/elasticsearch (v2.1.4)
			Downloading: 100%         

	guzzlehttp/ringphp suggests installing ext-curl (Guzzle will use specific adapters if cURL is present)
	elasticsearch/elasticsearch suggests installing ext-curl (*)
	elasticsearch/elasticsearch suggests installing monolog/monolog (Allows for client-level logging and tracing)
	Writing lock file
	Generating autoload files
	[matthew@matthew ~/mounts/mdaines.edepoze.com/mdaines.edepoze/elasticsearch] 2016-01-18 12:18:30$ cd vendor/
	[matthew@matthew ~/mounts/mdaines.edepoze.com/mdaines.edepoze/elasticsearch/vendor] 2016-01-18 12:18:33$ ls
	autoload.php  composer  elasticsearch  guzzlehttp  psr  react
