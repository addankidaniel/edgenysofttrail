<?php

namespace ClickBlocks;

use ClickBlocks\Debug;

class ImageUtils {

	/**
	 * Renders exhibit stamp information to an image to be used as stamp annotation
	 * @param string $savePath
	 * @param string $exhibitTitle
	 * @param string $exhibitSubTitle
	 * @param string $exhibitDate
	 * @param integer $orientation 0-3
	 * @param string $branding
	 * @return array
	 */
	public static function drawExhibitStamp( $savePath, $exhibitTitle, $exhibitSubTitle, $exhibitDate, $orientation=0, $branding=NULL ) {
		//drawn large and resampled down
		$stampMinWidth = 900;
		$stampHeight = 590;

		$titleFontSize = 86; //points, not pixels
		$dateFontSize = 80; //points, not pixels
		$radius = 70;
		$lineWidth = 19;
		$cornerWidth = $lineWidth * 2;
		$titlePadLR = 96;
		$titlePadTop = 64;
		$subTitlePadTop = 17;
		$datePadTop = 48;
		$seperatorHeight = 8;
		$seperatorMargin = 108;
		$seperatorPadTop = 66;
		$fontPath = Core\IO::url( 'msttcore-fonts' ) . '/arial.ttf';
		$linePad = 15;
		$cropPad = 10;

		if (!file_exists($fontPath)){
			//Fix for ubuntu path
			$fontPath = '/usr/share/fonts/truetype/msttcorefonts/arial.ttf';
		}
/*
$f = fopen('/media/c/Appserv/www/!crowdvac/edepoze/a.log','a');
fwrite($f,$fontPath);
fclose($f);
//return ['a' => $fontPath];
*/

		$exhibitTitleStr = (mb_strlen( trim( $exhibitTitle ) )) ? $exhibitTitle : str_replace( ' ', '8', $exhibitTitle );
		$exhibitSubTitleStr = (mb_strlen( trim( $exhibitSubTitle ) )) ? $exhibitSubTitle : str_replace( ' ', '8', $exhibitSubTitle );

		$titleBox = imagettfbbox( $titleFontSize, 0, $fontPath, $exhibitTitleStr );
		$subTitleBox = imagettfbbox( $titleFontSize, 0, $fontPath, $exhibitSubTitleStr );
		$dateBox = imagettfbbox( $dateFontSize, 0, $fontPath, $exhibitDate );
		if( !$titleBox || !$subTitleBox || !$dateBox ) {
			return;
		}

		$titleW = abs( $titleBox[4] - $titleBox[0] ) + $linePad;
		$titleH = abs( $titleBox[5] );
		$subTitleW = abs( $subTitleBox[4] - $titleBox[0] ) + $linePad;
		$subTitleH = abs( $subTitleBox[5] );
		$dateW = abs( $dateBox[4] - $dateBox[0] ) + $linePad;
		$dateH = abs( $dateBox[5] );
		$width = $stampWidth = max( max( $titleW, $subTitleW, $dateW ) + ($lineWidth * 2) + $titlePadLR, $stampMinWidth );
		$width += ($cropPad * 2);

		$height = $width;
		$hOffset = (($height - $stampHeight - $cropPad) / 2);
		// Set up square image for Foxit library stamp annotation
		$im = imagecreatetruecolor( $width, $height ); // crop src
		$im2 = imagecreatetruecolor( $stampWidth, $stampWidth ); // crop dst

		$abort = function() use ($im, $im2) {
			Debug::ErrorLog( 'ImageUtils::drawExhibitStamp; Abort!' );
			imagedestroy( $im );
			imagedestroy( $im2 );
			return;
		};

		if( function_exists( 'imageantialias' ) ) {
			imageantialias( $im, TRUE );
			imageantialias( $im2, TRUE );
		}
		$black = imagecolorallocate( $im, 0, 0, 0 );
		$transparent = imagecolorallocatealpha( $im, 254, 254, 254, 127 ); //white (green screen)
		$transparent2 = imagecolorallocatealpha( $im2, 254, 254, 254, 127 ); //white (green screen)

		//transparent background
		if( !imagefill( $im, 0, 0, $transparent ) ) {
			return $abort();
		}
		if( !imagefill( $im2, 0, 0, $transparent2 ) ) {
			return $abort();
		}
		if( imagecolortransparent( $im, $transparent ) === -1 ) {
			return $abort();
		}
		if( imagecolortransparent( $im2, $transparent2 ) === -1 ) {
			return $abort();
		}

		self::gradient_fill_vertical( $im, $stampWidth, $stampHeight, '#ffffff', '#e3d5bb' );	//default gradient background, branding will override (TODO: branding)

		//draw corners
		for( $i=0; $i<=$cornerWidth; ++$i ) {
			//top left corner
			$cx = $radius + 1 + $cropPad;
			$cy = $cx + $hOffset;
			$cw = $radius * 2 - $i;
			if( !imagearc( $im, $cx, $cy, $cw, $cw, 180, 270, $black ) ) {
				return $abort();
			}

			//top right corner
			$cx = $stampWidth - $radius - 2 + $cropPad;
			if( !imagearc( $im, $cx, $cy, $cw, $cw, 270, 0, $black ) ) {
				return $abort();
			}

			//bottom right corner
			$cy = $stampHeight - $radius - 2 + $hOffset;
			if( !imagearc( $im, $cx, $cy, $cw, $cw, 0, 90, $black ) ) {
				return $abort();
			}

			//bottom left corner
			$cx = $radius + 1 + $cropPad;
			if( !imagearc( $im, $cx, $cy, $cw, $cw, 90, 180, $black ) ) {
				return $abort();
			}
		}

		//draw borders
		//left border
		$x1 = 1 + $cropPad;
		$y1 = $radius + 2 + $hOffset + $cropPad;
		$x2 = $lineWidth + 1 + $cropPad;
		$y2 = $stampHeight - $radius - 3 + $hOffset;
		if( !imagefilledrectangle( $im, $x1, $y1, $x2, $y2, $black ) ) {
			return $abort();
		}

		//right border
		$x1 = $stampWidth - $lineWidth - 2 + $cropPad;
		$x2 = $x1 + $lineWidth;
		if( !imagefilledrectangle( $im, $x1, $y1, $x2, $y2, $black ) ) {
			return $abort();
		}

		//top border
		$x1 = $radius + 2 + $cropPad;
		$y1 = 1 + $hOffset + $cropPad;
		$x2 = $stampWidth - $radius - 3 + $cropPad;
		$y2 = $lineWidth + 1 + $hOffset + $cropPad;
		if( !imagefilledrectangle( $im, $x1, $y1, $x2, $y2, $black ) ) {
			return $abort();
		}

		//bottom border
		$y1 = $stampHeight - $lineWidth - 2 + $hOffset;
		$y2 = $y1 + $lineWidth;
		if( !imagefilledrectangle( $im, $x1, $y1, $x2, $y2, $black ) ) {
			return $abort();
		}

		// remove background on corners
		if( !imagefilltoborder( $im, 0, 0, $black, $transparent ) ) {
			return $abort();
		}

		//text elements
		//title
		$x = (($stampWidth - $titleW) / 2) + $cropPad;
		$y = $lineWidth + $titleH + $titlePadTop + $hOffset + $cropPad;
		if( !imagettftext( $im, $titleFontSize, 0, $x, $y, -$black, $fontPath, $exhibitTitle ) ) {
			return $abort();
		}
		//subtitle
		$x = (($stampWidth - $subTitleW) / 2) + $cropPad;
		$y = $y + $subTitleH + $titlePadTop + $subTitlePadTop;
		if( !imagettftext( $im, $titleFontSize, 0, $x, $y, -$black, $fontPath, $exhibitSubTitle ) ) {
			return $abort();
		}
		//date
		$x = (($stampWidth - $dateW) / 2) + $cropPad;
		$y = $y + $seperatorPadTop + $seperatorHeight + $datePadTop + $dateH;
		if( !imagettftext( $im, $dateFontSize, 0, $x, $y, -$black, $fontPath, $exhibitDate ) ) {
			return $abort();
		}

		//seperator bar
		$x1 = $seperatorMargin + $cropPad;
		$y1 = $lineWidth + $titleH + $subTitleH + ($titlePadTop * 2) + $subTitlePadTop + $seperatorPadTop + $hOffset + $cropPad;
		$x2 = $x1 + ($stampWidth - ($seperatorMargin * 2));//$stampWidth - $x1 + $cropPad;
		$y2 = $y1 + $seperatorHeight;
		if( !imagefilledrectangle( $im, $x1, $y1, $x2, $y2, $black ) ) {
			return $abort();
		}

		if( $orientation > 0 ) {
			$im3 = imagerotate( $im, $orientation * -90, 0 );
			imagedestroy( $im );
			$im = $im3;
			unset( $im3 );
		}

		//crop src to dst
		imagecopy( $im2, $im, 0, 0, 10, 10, $stampWidth, $stampWidth );
		$im = $im2;
		unset( $im2 );

		$width -= ($cropPad * 2);
		$height -= ($cropPad * 2);

		$rsW = round( $width / 2 );
		$rsH = round( $height / 2 );
		$realImage = imagecreatetruecolor( $rsW, $rsH );
		imagealphablending( $realImage, FALSE );
		imagesavealpha( $realImage, TRUE );
		imagecopyresampled( $realImage, $im, 0, 0, 0, 0, $rsW, $rsH, $width, $height );
		imagedestroy( $im );

		if( !imagepng( $realImage, $savePath, 0 ) ) {
			imagedestroy( $realImage );
			return $abort();
		}
		chmod( $savePath, 0664 );

		imagedestroy( $realImage );

		//exec( "convert -units PixelsPerInch \"{$savePath}\" -resample 300 \"{$savePath}\"" );	//resample stamp to 300dpi
		if( $orientation % 2 == 1 ) {
			$_temp = $stampWidth;
			$stampWidth = $stampHeight;
			$stampHeight = $_temp;
		}

		return ['imageWidth'=>$rsW, 'imageHeight'=>$rsH, 'stampWidth'=>round( $stampWidth / 2 ), 'stampHeight'=>round( $stampHeight / 2 )];
	}

	/**
	 *
	 * @param resource $image
	 * @param int $width
	 * @param int $height
	 * @param string $startColorHex
	 * @param string $endColorHex
	 * @return boolean
	 */
	public static function gradient_fill_vertical( &$image, $width, $height, $startColorHex, $endColorHex )
	{
		$imgH = imagesy( $image );
		$imgW = imagesx( $image );
		$startRGB = self::hex2rgb( $startColorHex );
		$endRGB = self::hex2rgb( $endColorHex );

		for( $i=0; $i<$imgH; ++$i ) {
			$lastRGB = new \stdClass();
			if( isset( $r ) && isset( $g ) && isset( $b ) ) {
				$lastRGB->r = $r;
				$lastRGB->g = $g;
				$lastRGB->b = $b;
			}
            $r = ($endRGB->r - $startRGB->r != 0) ? intval( $startRGB->r + ($endRGB->r - $startRGB->r) * ($i / $imgH) ) : $startRGB->r;
            $g = ($endRGB->g - $startRGB->g != 0) ? intval( $startRGB->g + ($endRGB->g - $startRGB->g) * ($i / $imgH) ) : $startRGB->g;
            $b = ($endRGB->b - $startRGB->b != 0) ? intval( $startRGB->b + ($endRGB->b - $startRGB->b) * ($i / $imgH) ) : $startRGB->b;
			if( "{$lastRGB->r},{$lastRGB->g},{$lastRGB->b}" != "{$r},{$g},{$b}" ) {
				$fill = imagecolorallocate( $image, $r, $g, $b );
			}
			if( !imagefilledrectangle( $image, 0, $i, $imgW, $i, $fill ) ) {
				return FALSE;
			}
		}

		return TRUE;

	}

	/**
	 *
	 * @param string $color
	 * @return \stdClass
	 */
	public static function hex2rgb( $color )
	{
		$color = str_replace( '#', '', $color );
		$s = strlen( $color ) / 3;
		$rgb = new \stdClass();
		$rgb->r = hexdec( str_repeat( substr( $color, 0, $s ), 2/$s ) );
		$rgb->g = hexdec( str_repeat( substr( $color, $s, $s ), 2/$s ) );
		$rgb->b = hexdec( str_repeat( substr( $color, 2*$s, $s ), 2/$s ) );
		return $rgb;
	}

}