<?php

namespace ClickBlocks;

class Utils {

	public static function readableFilesize( $bytes, $decimals=2 )
	{
		$sz = 'BKMGTP';
		$factor = floor( ( strlen( $bytes ) - 1 ) / 3 );
		return sprintf( "%.{$decimals}f", $bytes / pow( 1024, $factor ) ) . @$sz[$factor];
	}

	public static function shorthandSizeToBytes( $val )
	{
		$val = trim( $val );
		$last = strtolower( $val[strlen( $val ) - 1] );
		switch( $last ) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}

	public static function buildURL( Array $urlComponents )
	{
		$scheme = isset( $urlComponents['scheme'] ) ? $urlComponents['scheme'] . '://' : '';
		$host = isset( $urlComponents['host'] ) ? $urlComponents['host'] : '';
		$port = isset( $urlComponents['port'] ) ? ':' . $urlComponents['port'] : '';
		$user = isset( $urlComponents['user'] ) ? $urlComponents['user'] : '';
		$pass = isset( $urlComponents['pass'] ) ? ':' . $urlComponents['pass']  : '';
		$pass = ($user || $pass) ? "{$pass}@" : '';
		$path = isset( $urlComponents['path'] ) ? $urlComponents['path'] : '';
		$query = isset( $urlComponents['query'] ) ? '?' . $urlComponents['query'] : '';
		$fragment = isset( $urlComponents['fragment'] ) ? '#' . $urlComponents['fragment'] : '';
		return trim( "{$scheme}{$user}{$pass}{$host}{$port}{$path}{$query}{$fragment}" );
	}

	public static function createUUID()
	{
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,
			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}

	public static function encrypt( $decrypted, $password, $salt='!F3@pk%9Q&mf*Xe~1K^b' )
	{
		// Build a 256-bit $key which is a SHA256 hash of $salt and $password.
		$key = hash( 'SHA256', "{$salt}{$password}", TRUE );
		// Build $iv and $iv_base64.  We use a block size of 128 bits (AES compliant) and CBC mode.  (Note: ECB mode is inadequate as IV is not used.)
		$iv = mcrypt_create_iv( mcrypt_get_iv_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC ), MCRYPT_RAND );
		if( strlen( $iv_base64 = rtrim( base64_encode( $iv ), '=' ) ) != 22 ) return false;
		// Encrypt $decrypted and an MD5 of $decrypted using $key.  MD5 is fine to use here because it's just to verify successful decryption.
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $key, $decrypted . md5( $decrypted ), MCRYPT_MODE_CBC, $iv ) );
		// We're done!
		return "{$iv_base64}{$encrypted}";
	}

	public static function decrypt( $encrypted, $password, $salt='!F3@pk%9Q&mf*Xe~1K^b' )
	{
		// Build a 256-bit $key which is a SHA256 hash of $salt and $password.
		$key = hash( 'SHA256', "{$salt}{$password}", TRUE );
		// Retrieve $iv which is the first 22 characters plus ==, base64_decoded.
		$iv = base64_decode( substr( $encrypted, 0, 22 ) . '==' );
		// Remove $iv from $encrypted.
		$encrypted = substr( $encrypted, 22 );
		if( !$encrypted ) return false;
		// Decrypt the data.  rtrim won't corrupt the data because the last 32 characters are the md5 hash; thus any \0 character has to be padding.
		$decrypted = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $key, base64_decode( $encrypted ), MCRYPT_MODE_CBC, $iv ), "\0\4" );
		// Retrieve $hash which is the last 32 characters of $decrypted.
		$hash = substr( $decrypted, -32 );
		// Remove the last 32 characters from $decrypted.
		$decrypted = substr( $decrypted, 0, -32 );
		// Integrity check.  If this fails, either the data is corrupted, or the password/salt was incorrect.
		if( md5( $decrypted ) != $hash ) return false;
		// Yay!
		return $decrypted;
	}

	public static function flattenPDF( $inPath, $outPath )
	{
		return \ClickBlocks\PDFTools::flatten( $inPath, $outPath );
	}

	public static function annotatePDF( $inPath, $metadataPath, $outPath )
	{
		return \ClickBlocks\PDFTools::annotate( $inPath, $metadataPath, $outPath );
	}

	public static function watermarkPDF( $inPath, $outPath )
	{
		return \ClickBlocks\PDFTools::watermark( $inPath, $outPath );
	}

	//Format the phone number into a consistant format.
	public static function formatPhone($ph)
	{
            //Remove anything that is not a number
            $phoneNumber = preg_replace('/[^0-9]/', '', $ph);
            if (mb_strlen($phoneNumber) === 10)
            {
                $phoneNumber = mb_substr($phoneNumber, 0, 3) . '-' . mb_substr($phoneNumber, 3, 3) . '-' . mb_substr($phoneNumber, 6, 4);
                return $phoneNumber;
            } else {
                return $ph;
            }
	}

	public static function copyFileChunked( $src, $dst, $chunkSize=1048576 )
	{
		$chunkBytes = (int)$chunkSize;
		if( !$chunkBytes || $chunkBytes <= 0 || $chunkBytes >= 104857600 ) {
			\ClickBlocks\Debug::ErrorLog( "Utils::copyFileChunked; Invalid chunk size: {$chunkSize}" );
			return FALSE;
		}
		$bytesWritten = 0;
		$fhSrc = fopen( $src, 'rb' );
		$fhDst = fopen( $dst, 'w' );
		if( !$fhSrc ) {
			\ClickBlocks\Debug::ErrorLog( "Utils::copyFileChunked; Unable to open source: {$fhSrc}" );
			return FALSE;
		}
		if( !$fhDst ) {
			\ClickBlocks\Debug::ErrorLog( "Utils::copyFileChunked; Unable to open destination: {$fhDst}" );
			return FALSE;
		}
		if( !flock( $fhDst, LOCK_EX ) ) {
			\ClickBlocks\Debug::ErrorLog( "Utils::copyFileChunked; Unable to file lock: {$fhDst}" );
			return FALSE;
		}
		while( !feof( $fhSrc ) ) {
			$buffer = fread( $fhSrc, $chunkBytes );
			if( $buffer === FALSE ) {
				\ClickBlocks\Debug::ErrorLog( "Utils::copyFileChunked; Error while reading source: {$fhSrc}" );
				break;
			}
			$bytes = fwrite( $fhDst, $buffer );
			if( $bytes === FALSE ) {
				\ClickBlocks\Debug::ErrorLog( "Utils::copyFileChunked; Error while writing to file: {$fhDst}" );
				break;
			}
			$bytesWritten += $bytes;
		}
		flock( $fhDst, LOCK_UN );
		fclose( $fhSrc );
		fclose( $fhDst );
		return $bytesWritten;
	}
}
