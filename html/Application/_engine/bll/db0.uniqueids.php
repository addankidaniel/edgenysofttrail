<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 */
class UniqueIDs extends BLLTable
{
	public function __construct($ID = null)
	{
		parent::__construct();
		$this->addDAL(new DALUniqueIDs(), __CLASS__);
		if ($ID) $this->assignByID($ID);
	}
}
