<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $subject
 * @property longtext $messageBody
 * @property timestamp $createdOn
 */
class ClientMessages extends BLLTable {
	public function __construct($ID = null) {
		parent::__construct();
		$this->addDAL( new DALClientMessages(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}
}

?>