<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property bigint $caseID
 * @property bigint $sortPos
 */
class UserCustomSortCases extends BLLTable {
	public function __construct( $userID=NULL, $caseID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserCustomSortCases(), __CLASS__ );
		if( $userID && $caseID ) {
			$this->assignByID( ['userID'=>$userID, 'caseID'=>$caseID] );
		}
	}
}
