<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\Cache,
	ClickBlocks\API;

/**
 * @property bigint $ID
 * @property bigint $resellerID
 * @property bigint $clientID
 * @property tinyint $optionID
 * @property char $pricingTerm
 * @property timestamp $created
 * @property bigint $userID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property decimal $price
 * @property int $quantity
 * @property navigation $clients
 * @property navigation $users
 * @property navigation $cases
 * @property navigation $depositions
 */
class PlatformLog extends BLLTable
{
	public function __construct( $ID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALPlatformLog(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

	protected function _initclients()
	{
		$this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'clients' );
	}

	protected function _initusers()
	{
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'users' );
	}

	protected function _initcases()
	{
		$this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'cases' );
	}

	protected function _initdepositions()
	{
		$this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'depositions' );
	}
}
