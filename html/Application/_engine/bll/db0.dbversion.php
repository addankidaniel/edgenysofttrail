<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $version
 */
class DbVersion extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALDbVersion(), __CLASS__);
  }
}

?>