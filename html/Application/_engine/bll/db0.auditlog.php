<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property varchar $description
 * @property varchar $actionID
 * @property bigint $relatedTo
 * @property varchar $entityTypeID
 * @property bigint $createdBy
 * @property timestamp $created
 * @property navigation $lookupAuditActions
 * @property navigation $lookupEntityTypes
 * @property navigation $users
 */
class AuditLog extends BLLTable {
	public function __construct() {
		parent::__construct();
		$this->addDAL(new DALAuditLog(), __CLASS__);
	}

	protected function _initlookupAuditActions() {
		$this->navigators['lookupAuditActions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'lookupAuditActions');
	}

	protected function _initlookupEntityTypes() {
		$this->navigators['lookupEntityTypes'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'lookupEntityTypes');
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
	}
}

?>