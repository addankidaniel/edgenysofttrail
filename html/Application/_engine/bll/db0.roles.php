<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $ID
 * @property varchar $name
 * @property timestamp $created
 * @property navigation $rolePermissions
 * @property navigation $userRoles
 */
class Roles extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALRoles(), __CLASS__);
  }

  protected function _initrolePermissions()
  {
    $this->navigators['rolePermissions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'rolePermissions');
  }

  protected function _inituserRoles()
  {
    $this->navigators['userRoles'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'userRoles');
  }
}

?>