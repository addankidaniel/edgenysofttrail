<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $userID
 * @property enum $prefKey
 * @property string $value
 */
class UserPreferences extends BLLTable {
	const PREFKEY_DEPOSITIONSFILTER = 'DepositionsFilter';

	private static $prefKeys = [
		self::PREFKEY_DEPOSITIONSFILTER
	];

	public static function prefKeys() {
		return self::$prefKeys;
	}

	public function __construct( $userID=NULL, $prefKey=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserPreferences(), __CLASS__ );
		if( $userID && $prefKey ) {
			$this->assignByID( ['userID'=>$userID, 'prefKey'=>$prefKey] );
		}
	}

	/**
	 * Save User Preference(s)
	 * @param enum $prefKey
	 * @param string $value
	 * @return DB\UserPreferences
	 */
	public function setPreference( $prefKey, $value ) {
		$this->prefKey = $prefKey;
		$this->value = $value;
		$this->save();
		return $this;
	}
}
