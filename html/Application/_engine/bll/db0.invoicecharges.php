<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property bigint $childClientID
 * @property bigint $invoiceID
 * @property bigint $caseID
 * @property bigint $depositionID
 * @property bigint $userID
 * @property decimal $price
 * @property tinyint $optionID
 * @property int $quantity
 * @property timestamp $created
 * @property navigation $clients
 * @property navigation $invoices
 * @property navigation $lookupPricingOptions
 * @property navigation $cases
 * @property navigation $depositions
 * @property navigation $users
 */
class InvoiceCharges extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALInvoiceCharges(), __CLASS__);
  }

  protected function _initclients()
  {
    $this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'clients');
  }

  protected function _initinvoices()
  {
    $this->navigators['invoices'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'invoices');
  }

  protected function _initlookupPricingOptions()
  {
    $this->navigators['lookupPricingOptions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'lookupPricingOptions');
  }

  protected function _initcases()
  {
    $this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'cases');
  }

  protected function _initdepositions()
  {
    $this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositions');
  }

  protected function _initusers()
  {
	  $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }
}

?>