<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $depositionID
 * @property bigint $fileID
 * @property navigation $depositions
 * @property navigation $files
 */
class Presentations extends BLLTable
{
	const BLL_CLASSNAME = '\ClickBlocks\DB\Presentations';

	public function __construct( $depositionID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALPresentations(), __CLASS__ );
		if( $depositionID ) {
			$this->assignByID( $depositionID );
		}
	}

	protected function _initdepositions()
	{
		$this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'depositions' );
	}

	protected function _initfiles()
	{
		$this->navigators['files'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'files' );
	}
}
