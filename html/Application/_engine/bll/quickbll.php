<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\DB\ORM;

class QuickBLL implements IBLLTable
{
  protected $ar;
  protected $pk;
  
  public function __construct(array $ar = array(), $pk = null) {
    foreach ($ar as $k=>$v) if (is_array($v)) $ar[$k] = new QuickBLL($v);  // mock navigation properties
    $this->ar = $ar;
    if ($pk) $this->pk = $pk;
    else $this->pk = reset($ar);
  }
  
  public function __clone() {
     throw new \Exception("No Cloning!");
  }

    public function __get($k) {
    return @$this->ar[$k];
  }
  
  public function __set($k, $v) {
    if (is_array($v)) $v = new QuickBLL($v);
    $this->ar[$k] = $v;
  }
  
  public function isKeyFilled() {
     if (is_array($this->pk)) {
        $flag = true;
        foreach ($this->pk as $pk) if (!$this->ar[$pk]) $flag = false;
        return $flag;
     }
     return $this->ar[$this->pk];
  }

  public function setValues($ar)
  {
     foreach ($ar as $k=>$v) $this->{$k} = $v;
    //$this->ar = array_merge($this->ar, $ar);
  }
  
  public function getValues()
  {
    $ar = $this->ar;
    foreach ($ar as $k=>$v) if (is_object($v)) unset($ar[$k]);
    ksort($ar);
    return $ar;
  }
  
  public function save() {
    throw new \Exception('Stub ::insert method!');
  }
  
  public function insert() {
    throw new \Exception('Stub ::insert method!');
  }
  
  public function delete() {
    throw new \Exception('Stub ::delete method!');
  }
  
  public function update() {
    throw new \Exception('Stub ::update method!');
  }
  
  public function replace() {
    throw new \Exception('Stub ::replace method!');
  }
}

class QuickDAL extends QuickBLL implements IDALTable {}

class QuickNavigation extends QuickBLL {}

/*class QuickColumnFamily extends QuickBLL implements ORM\IColumnFamily
{
  private $key;
  private $property = null;
  private $isSuper = false;
  //private $deleteCallback;
  
  public function __construct($key = null, array $ar = array(), $super = false) {
    parent::__construct($ar);
    $this->key = $key;
    if ($property)
      $this->property = $property;
    else
      $this->isSuper = (bool)$super;
  }
  
  public function __set($k, $v) {
    if ($this->isSuper) {
      if ($this->property) {
        $this->ar[$this->property][$k] = $v;
      } else {
        if (is_array($v)) {
          $this->ar[$k] = $v;
        } else
          throw new \Exception('Cannot assign scalar to super column family!');
      }
    } else {
      parent::__set($k, $v);
    }
  }
  
  public function __get($k) {
    if ($this->isSuper) 
      if ($this->property) {
        $val = @$this->ar[$this->property][$k];
        $this->property = null;
        return $val;
      } else {
        $this->property = $k;
        return $this;
      }
    return @$this->ar[$k];
  }
  
  public function setValues($ar)
  {
    if ($this->isSuper) {
      if ($this->property) {
        $this->ar[$this->property] = array_merge($this->ar[$this->property], $ar);
      } else {
        parent::setValues ($ar);
      }
    } else
      parent::setValues ($ar);
  }
  
  public function getValues()
  {
    if ($this->isSuper) {
      if ($this->property) {
        return $this->ar[$this->property];
      } else {
        return parent::getValues();
      }
    } else
      return parent::getValues();
  }
  
  public function isAllowedKey($k) {
    return true;
  }
  
  public function isKeyFilled($k) {
    return (bool)$this->key;
  }

  public function getKey() {
    return $this->key;
  }
  
  public function setKey($k) {
    $this->key = $k;
  }
  
  public function delete() {
    throw new \Exception('Stub ::delete method!');
  }
  
  public function update() {
    throw new \Exception('Stub ::update method!');
  }
  
  public function replace() {
    throw new \Exception('Stub ::replace method!');
  }
}*/


?>
