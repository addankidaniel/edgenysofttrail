<?php

namespace ClickBlocks\DB;

/**
 * @property int $ID
 * @property string $code
 * @property string $spice
 */
class DepositionsAuth extends BLLTable {

	public function __construct( $ID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALDepositionsAuth(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

	public static function generateSpice() {
		return parent::generateSpice();
	}
}
