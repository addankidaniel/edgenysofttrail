<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property int $roleID
 * @property bigint $permissionID
 * @property timestamp $created
 * @property navigation $permissions
 * @property navigation $roles
 */
class RolePermissions extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALRolePermissions(), __CLASS__);
  }

  protected function _initpermissions()
  {
    $this->navigators['permissions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'permissions');
  }

  protected function _initroles()
  {
    $this->navigators['roles'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'roles');
  }
}

?>