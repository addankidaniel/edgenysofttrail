<?php                

namespace ClickBlocks\UnitTest\DB\Files;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\UnitTest\DB\BLLObjectTestCase,
    ClickBlocks\UnitTest;

/**
 * @group all
 */ 
class allTest extends BLLObjectTestCase
{
   protected static $fileName = '/some/file/name.ext';
   protected static $fileDestName = '/some/file/dest/name/name.ext';
   protected static $fileDestPath = '/some/file/dest/name';
   protected static $fileRow = array(
       'ID' => '1',
       'folderID' => '2',
       'createdBy' => '3',
       'created' => '2012-05-12 12:12:12',
       'name' => 'name.ext',
   );
   
   protected function getLogicTableName() {
      return 'Files';
   }
   
   public function setUp()
   {
     parent::setUp();
     //$this->getMockClass('ClickBlocks\DB\Files', array());
   }
   
   public static function setUpBeforeClass() {
      parent::setUpBeforeClass();
   }

   public function getFileMock(array $methods = array())
   {
      return self::getMockWrapper($this->getThisMock(null, $methods));
   }
   
   public function getFS()
   {
     return self::getMockWrapper($this->fs);
   }

   public function getFileMockPurpose($purpose = '')
   {
      switch ($purpose) {
         case 'create':
            $file = $this->getFileMock(array('getFullName','handleNameCollision'));
            $file->setSourceFile(self::$fileName);
            $file->folderID = 5;
            $file->createdBy = 4;
            $file->once()->getFullName(false)->result(self::$fileDestName);
            break;
         case 'delete':
            $file = $this->getFileMock(array('getFullName'));
            $file->assign(self::$fileRow);
            $file->once()->getFullName(false)->result(self::$fileDestName);
            break;
         default:
            throw new \Exception('Unknown porpose: '.$purpose);
      }
      return $file;
   }
   
   public function expectFileException($code = NULL, $message = '')
   {
      $this->setExpectedException('\ClickBlocks\Core\FileOperationException', $message, $code);
   }
   
   public function test_test()
   {
      //$file = new DB\Files();
      //die(get_class($file));
   }

   public function test_create_file()
   {
      $file = $this->getFileMockPurpose('create');
      $fs = $this->getFS();
      $fs->expects(1)->is_dir(self::$fileDestPath)->result(true);
      $fs->expects(1)->rename(self::$fileName, self::$fileDestName)->result(true);
      $file->once()->handleNameCollision();
      /*$this->fs->expects(self::once())->method('is_dir')
          ->with(self::equalTo($fileDestPath))
          ->will(self::returnValue(true));*/
      /*$this->fs->expects(self::onConsecutiveCalls())->method('__call')
          ->with(self::equalTo('rename'), self::equalTo(array($fileName, $fileDestName)))
          ->will(self::returnValue(true));*/
      $file->getDB()->expects(self::once())->method('execute');
      $file->insert();
   }
   
   public function test_create_file_path_not_exist()
   {
      $this->expectFileException(null, 'not exist');
      $file = $this->getFileMockPurpose('create');
      $file->once()->handleNameCollision();
      $fs = $this->getFS();
      $fs->expects(1)->is_dir(self::$fileDestPath)->result(false);
      $fs->expects(0)->rename();
      $file->getDB()->expects(self::never())->method('execute');
      $file->insert();
   }
   
   public function test_create_file_cannot_rename()
   {
      $this->expectFileException(null, 'rename');
      $file = $this->getFileMockPurpose('create');
      $file->once()->handleNameCollision();
      $fs = $this->getFS();
      $fs->expects(1)->is_dir(self::$fileDestPath)->result(true);
      $fs->expects(1)->rename(self::$fileName, self::$fileDestName)->result(false);
      $file->getDB()->expects(self::never())->method('execute');
      $file->insert();
   }
   
   public function test_create_file_cannot_rename_2()
   {
      $this->expectFileException(null, 'rename');
      $file = $this->getFileMockPurpose('create');
      $file->once()->handleNameCollision();
      $fs = $this->getFS();
      $fs->expects(1)->is_dir(self::$fileDestPath)->result(true);
      $fs->expects(1)->rename(self::$fileName, self::$fileDestName)->resultException(new \ErrorException('rename error'));
      $file->getDB()->expects(self::never())->method('execute');
      $file->insert();
   }
   
   public function test_delete_file()
   {
      $file = $this->getFileMockPurpose('delete');
      $fs = $this->getFS();
      $fs->once()->is_file(self::$fileDestName)->result(true);
      $fs->once()->unlink(self::$fileDestName)->result(true);
      self::getMockWrapper($file->getDB())->once()->delete('Files',array('ID'=>self::$fileRow['ID']))->result(1);
      $file->delete();
   }
}

?>