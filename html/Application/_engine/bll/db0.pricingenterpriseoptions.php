<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $clientID
 * @property bigint $resellerID
 * @property tinyint $optionID
 * @property decimal $price
 * @property char $typeID
 * @property navigation $clients
 * @property navigation $lookupPricingOptions
 * @property navigation $lookupPricingTerms
 */
class PricingEnterpriseOptions extends BLLTable
{
	public function __construct( $clientID=NULL, $resellerID=NULL, $optionID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALPricingEnterpriseOptions(), __CLASS__ );
		if( $clientID && $resellerID && $optionID ) {
			$this->assignByID( ['clientID'=>$clientID, 'resellerID'=>$resellerID, 'optionID'=>$optionID] );
		}
	}

	protected function _initlookupPricingTerms()
	{
		$this->navigators['lookupPricingTerms'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'lookupPricingTerms' );
	}

	protected function _initclients()
	{
		$this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'clients' );
	}

	protected function _initlookupPricingOptions()
	{
		$this->navigators['lookupPricingOptions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'lookupPricingOptions' );
	}
}

?>