<?php

namespace ClickBlocks\DB;

use ClickBlocks\Utils\NodeJS,
	ClickBlocks\Debug;

/**
 * @property int $userID
 * @property int $folderID
 * @property int $sortPos
 */
class UserCustomSortFolders extends BLLTable {

	const DB = 'db0';
	const BASENAME = 'UserCustomSortFolders';
	const NSCLASS = __CLASS__; // \ClickBlocks\DB\UserCustomSortFolders

	public function __construct( $userID=NULL, $folderID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserCustomSortFolders(), __CLASS__ );
		if( $userID && $folderID ) {
			$this->assignByID( ['userID'=>$userID, 'folderID'=>$folderID] );
		}
	}

	/**
	 * save Folders custom sort
	 * @param int $csUserID
	 * @param array $folderIDs
	 * @param int $byUserID
	 * @param int $sessionID
	 */
	public static function setCustomSort( $csUserID, array $folderIDs, $byUserID=0 ) {
		if( !$csUserID || !$folderIDs ) {
			return;
		}
		foreach( $folderIDs as $folderID => $sortPos ) {
			$folderID = (int)$folderID;
			$sortPos = (int)$sortPos;
			if( !$sortPos || $sortPos <= 0 ) {
				continue;
			}
			$cs = new UserCustomSortFolders( $csUserID, $folderID );
			if( $cs->folderID != $folderID ) {
				$cs->userID = $csUserID;
				$cs->folderID = $folderID;
			}
			$cs->sortPos = $sortPos;
			$cs->save();
		}
		$keys = array_keys( $folderIDs );
		$firstID = array_shift( $keys );
		$folder = new Folders( $firstID );
		if( !$folder || !$folder->ID || $folder->ID != $firstID ) {
			Debug::ErrorLog( "UserCustomSortFolders::setCustomSort; folder not found by ID: {$firstID}" );
			return;
		}
		self::notifyUsers( $folder, (int)$csUserID, (int)$byUserID );
	}

	/**
	 * @param \ClickBlocks\DB\Folders $folder
	 * @param int $csUserID
	 * @param int $byUserID
	 */
	protected static function notifyUsers( \ClickBlocks\DB\Folders $folder, $csUserID, $byUserID=0 ) {
		if( !$folder || !$folder->ID ) {
			Debug::ErrorLog( 'UserCustomSortFolders::notifyUsers; invalid folder' );
			return;
		}
		$sessionIDs = [];
		if( $folder->isCaseFolder() ) {
			$orcDepo = new OrchestraDepositions();
			$sessions = $orcDepo->getAllByCase( $folder->caseID );
			if( is_array( $sessions ) && $sessions ) {
				foreach( $sessions as $session ) {
					$sessionIDs[] = $session['ID'];
				}
			}
		} else {
			$sessionIDs[] = $folder->depositionID;
		}
		if( $sessionIDs ) {
			$nodeJS = new NodeJS( TRUE );
			foreach( $sessionIDs as $sessionID ) {
				$nodeJS->notifySessionChangedCustomSort( (int)$sessionID, UserSortPreferences::SORTOBJECT_FOLDERS, (int)$csUserID, (int)$byUserID );
			}
		}
	}

}
