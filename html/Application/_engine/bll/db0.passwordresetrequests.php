<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\API,
    ClickBlocks\Cache,
	ClickBlocks\Utils;

/**
 * @property char $hash
 * @property varchar $salt
 * @property bigint $userID
 * @property timestamp $created
 * @property timestamp $finished
 * @property navigation $users
 */
class PasswordResetRequests extends BLLTable
{
  public function __construct($ID = null)
  {
    parent::__construct();
    $this->addDAL(new DALPasswordResetRequests(), __CLASS__);
    if ($ID) $this->assignByID($ID);
  }

	public function insert()
	{
		// remove any existing reset requests for the user
		if (!$this->userID)
		{
			throw new \Exception( 'Column "userID" cannot be null' );
		}
		(new OrchestraPasswordResetRequests())->finishExistingRequests( $this->userID );
		return parent::insert();
	}

  protected function _initusers()
  {
    $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }

	public static function sendToUser( $userID )
	{
		$user = (new ServiceUsers())->getByID( $userID );

		$recovery_salt = self::generateNewSalt();
		$recovery_password = self::generateNewPassword( $user->ID );

		if ($recovery_password === false)
		{
			$this->except( 'Error generating temporary password', 1006 );
		}

		$request = new PasswordResetRequests();
		$request->userID = $user->ID;
		$request->salt = $recovery_salt;
		$request->hash = hash( 'sha512', ($recovery_password.$recovery_salt) );
		$request->insert();

		$client = (new ServiceClients())->getByID( $user->clientID );
		if (!$client->ID)
		{
			$domain = 'admin';
		} else {
			$domain = ($client->typeID === API\IEdepoBase::CLIENT_TYPE_ENT_CLIENT) ? $client->URL : $client->getReseller()->URL;
		}
		$url = 'http://'.(new Utils\URI())->source['host'].'/'.$domain.'/password-change?hash='.$recovery_password;
		$mailer = Utils\EmailGenerator::sendPasswordRecovery($user->getValues(), $url);
		if ($mailer->IsError())
		{
			$request->delete();
			$this->except ('Error sending email', 1002);
		}
	}

	private static function generateNewSalt()
	{
		$randmax = mt_getrandmax();
		return hash( 'sha1', dechex( mt_rand( $randmax/2, $randmax ) ).microtime() );
	}

	private static function generateNewPassword( $userID )
	{
		$user = new Users( $userID );
		return (!$user->ID) ? false : hash( 'sha1', implode( '&', [
			$user->email,
			$user->username,
			date( 'r' ),
		] ) );
	}
}

?>