<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property bigint $datasourceID
 * @property tinyblob $username
 * @property tinyblob $password
 */
class DatasourceUsers extends BLLTable
{
	public function __construct($ID = null)
	{
		parent::__construct();
		$this->addDAL( new DALDatasourceUsers(), __CLASS__ );
		if( $ID ) $this->assignByID( $ID );
	}
}
