<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
	ClickBlocks\API,
	ClickBlocks\Cache,
	ClickBlocks\API\IEdepoBase,
	ClickBlocks\MVC\Edepo;

/**
 * @property bigint $ID
 * @property bigint $caseID
 * @property bigint $createdBy
 * @property bigint $ownerID
 * @property bigint $speakerID
 * @property char $statusID
 * @property varchar $uKey
 * @property varchar $password
 * @property datetime $openDateTime
 * @property varchar $depositionOf
 * @property varchar $volume
 * @property varchar $title
 * @property varchar $location
 * @property varchar $oppositionNotes
 * @property text $notes
 * @property timestamp $created
 * @property timestamp $deleted
 * @property timestamp $started
 * @property timestamp $finished
 * @property varchar $courtReporterEmail
 * @property varchar $courtReporterPassword
 * @property bigint $parentID
 * @property string $exhibitTitle
 * @property string $exhibitSubTitle
 * @property string $exhibitDate
 * @property decimal $exhibitXOrigin
 * @property decimal $exhibitYOrigin
 * @property int $attendeeLimit
 * @property bigint $enterpriseResellerID
 * @property enum $class
 * @property enum $liveTranscript
 * @property varchar $jobNumber
 * @property navigation $cases
 * @property navigation $lookupDepositionStatuses
 * @property navigation $creator
 * @property navigation $owner
 * @property navigation $depositionAssistants
 * @property navigation $depositionAttendees
 * @property navigation $folders
 * @property navigation $fileShares
 * @property navigation $parentDepositions1
 * @property navigation $childrenDepositions1
 * @property navigation $invoiceCharges
 * @property navigation $speaker
 * @property navigation $enterpriseReseller
 */
class Depositions extends BLLTable
{
	private $ownerDidChange = FALSE;

	/**
	 * @var \ClickBlocks\DB\DB $db
	 */
	private $db;

	public function __construct( $id=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALDepositions(), __CLASS__ );
		if( $id ) {
			$this->assignByID( $id );
		}
	}

	public function getCopy()
	{
		$vals = $this->getValues();
		$vals['ID'] = $vals['createdBy'] = $vals['created'] = $vals['courtReporterEmail'] = $vals['courtReporterPassword'] = $vals['parentID'] = '';
		$copy = new Depositions;
		$copy->setValues($vals);
		$copy->uKey = time() . mt_rand( 1000000, 9999999 ); // this is temporary
		return $copy;
	}

	protected function _initcreator()
	{
		$this->navigators['creator'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'creator');
	}

	protected function _initowner()
	{
		$this->navigators['owner'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'owner');
	}

	protected function _initcases()
	{
		$this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'cases');
	}

	protected function _initdepositionAssistants()
	{
		$this->navigators['depositionAssistants'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionAssistants');
	}

	protected function _initdepositionAttendees()
	{
		$this->navigators['depositionAttendees'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositionAttendees');
	}

	protected function _initfolders()
	{
		$this->navigators['folders'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'folders');
	}

	protected function _initlookupDepositionStatuses()
	{
		$this->navigators['lookupDepositionStatuses'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'lookupDepositionStatuses');
	}

	protected function _initinvoiceCharges()
	{
		$this->navigators['invoiceCharges'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'invoiceCharges');
	}

	protected function _initenterpriseReseller()
	{
		$this->navigators['enterpriseReseller'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'enterpriseReseller');
	}

	public function getOwnerID()
	{
		return $this->ownerID;
	}

	public function resetOwner()
	{
		$this->ownerID = $this->createdBy;
		$this->update();
	}

	public function getCase()
	{
		return $this->cases[0];
	}

	public function getParentDeposition()
	{
		return $this->parentDepositions1[0];
	}

	/**
	 * @return \ClickBlocks\DB\Clients
	 */
	public function getClient()
	{
		return $this->cases[0]->clients[0];
	}

	/**
	 * @return array of depositions
	 */
	public function getChildDepositions()
	{
		$depos = array();
		foreach ($this->childrenDepositions1 as $depo) $depos[] = $depo;
		return $depos;
	}

	// ED-1481 Overload the parent so we can check ownership change
	public function setValues( $ar, $isRawData=FALSE )
	{
		if( $ar['ownerID'] != $this->ownerID ) {
			$this->ownerDidChange = TRUE;
		}
		parent::setValues( $ar, $isRawData );
	}

	public function getValues( $isRawData=FALSE, $userID=NULL )
	{
		$values = parent::getValues( $isRawData, $userID );
		if( $userID ) {
			$orchDepo = new \ClickBlocks\DB\OrchestraDepositions();
			$values['sortPos'] = $orchDepo->getSortPosition( $this->ID, $userID );
		}
		return $values;
	}

	public function __set($field, $value)
	{
		//if ($field == 'caseID') $this->ownerID = null;
		return parent::__set($field, $value);
	}

	public function start()
	{
		if( $this->statusID != IEdepoBase::DEPOSITION_STATUS_NEW ) {
			throw new \LogicException( "Deposition with status '{$this->statusID}' cannot be started." );
		}
		$this->started = date( 'Y-m-d H:i:s' );
		$this->statusID = IEdepoBase::DEPOSITION_STATUS_IN_PROCESS;
		$this->update();
		if( !$this->isDemo() && !$this->isWitnessPrep() ) {
			$this->addTranscriptsFolder();
		}
	}

	public function finish()
	{
		if( $this->statusID != 'I' ) {
			throw new \LogicException( 'Deposition with status '.$this->statusID.' cannot be finished.' );
		}
		$this->finished = date( 'Y-m-d H:i:s' );
		$this->statusID = 'F';
		$this->update();

		// Bill and add transcripts if not a demo (or witness prep)
		if( $this->class == 'Deposition' ) {
			$this->finishChildDepositions();
			$this->chargePerDepositionFees();
//			$this->importRoughTranscript();
		} elseif( $this->isDemo() ) {
			$this->resetDemoSession( $this->createdBy );
			return;
		}  elseif( $this->class == IEdepoBase::DEPOSITION_CLASS_WITNESSPREP ) {
			$this->finishChildDepositions();
		}
		$node = new \ClickBlocks\Utils\NodeJS( TRUE );
		$node->sendPostCommand( 'deposition_end', NULL, ['date'=>$this->finished, 'depositionID'=>$this->ID] );
		$case = $this->cases[0];
		$node->notifyCasesUpdated( $case->ID, $this->ID );
	}

	public function addTranscriptsFolder() {
		if( $this->isDemo() || $this->isWitnessPrep() ) {
			return;	// ED-3083; no transcripts folder for WitnessPrep(s)
		}
		return; // never create transcripts folders
		$folder = (new OrchestraFolders())->getTranscriptFolderObject( $this->ID, NULL, $this->config->logic['transcriptFolderName'] );
		if( !$folder->ID ) {
			$folder = ServiceFolders::createFolder( $this->config->logic['transcriptFolderName'], $this->createdBy, $this->ID, IEdepoBase::FOLDERS_TRANSCRIPT );
		}
		return $folder;
	}

	public function importRoughTranscript()
	{
		return; //ED-2620; do not save transcripts
		$redis = new \ClickBlocks\Cache\CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
		$rKey = 'transcript:' . $this->ID;
		$txInfo = $redis->getJSON( $rKey );
		if( !$txInfo || !$txInfo->guid || $txInfo->isDemo || $txInfo->guid === '5ebeed36-cf78-424a-be18-a69f8e394999' ) {
			return;
		}
		$txAPI = $this->config->courtroom_connect['transcript_api'];
		$remoteURL = "https://{$txAPI['host']}{$txAPI['basePath']}{$txInfo->guid}{$txAPI['ptfSuffix']}";
		$ptfFile = $_SERVER['DOCUMENT_ROOT'] . \ClickBlocks\Core\IO::url( 'temp' ) . "/{$txInfo->guid}{$txAPI['ptfSuffix']}";
		//		\ClickBlocks\Debug::ErrorLog( "Depositions::importRoughTranscript; Saving '{$remoteURL}' as '{$ptfFile}'" );
		$bytes = \ClickBlocks\Utils::copyFileChunked( $remoteURL, $ptfFile );
		if( $bytes === FALSE ) {
			\ClickBlocks\Debug::ErrorLog( 'Depositions::importRoughTranscript; Failed to copy transcript' );
			return FALSE;
		}
		$pdfSuffix = str_replace( '.ptf', '.pdf', $txAPI['ptfSuffix'] );
		$pdfFile = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . "/{$txInfo->guid}{$pdfSuffix}";
		$okToGo = \ClickBlocks\CCTranscripts::convertPTF( $ptfFile, $pdfFile );
		if( $okToGo !== TRUE || !file_exists( $pdfFile ) ) {
			\ClickBlocks\Debug::ErrorLog( 'Depositions::importRoughTranscript; Failed to convert transcript' );
			return FALSE;
		}
		$orcFolders = new OrchestraFolders();
		$folder = $orcFolders->getTranscriptFolderObject( $this->ID, NULL, $this->config->logic['transcriptFolderName'] );
		if( !$folder || !$folder->ID || $folder->depositionID != $this->ID ) {
			\ClickBlocks\Debug::ErrorLog( 'Depositions::importRoughTranscript; Missing Transcripts folder!' );
			return FALSE;
		}
		$now = date( 'Y-m-d H:i:s' );
		$transcript = new \ClickBlocks\DB\Files();
		$transcript->setSourceFile( $pdfFile );
		$transcript->folderID = $folder->ID;
		$transcript->createdBy = $this->createdBy;
		$transcript->sourceUserID = NULL;
		$transcript->name = 'RoughTranscript.pdf';
		$transcript->created = $now;
		$transcript->isExhibit = 1;
		$transcript->isPrivate = 0;
		$transcript->isTranscript = 1;
		$transcript->sourceID = NULL;
		$transcript->isUpload = 0;
		$transcript->insert();
		$folder->lastModified = $now;
		$folder->update();
	}

	public function finishChildDepositions()
	{
		foo(new OrchestraDepositions)->finishByParentID($this->ID);
	}

	public function setAsDeleted()
	{
		$this->deleted = date( 'Y-m-d H:i:s' );
		$this->update();
		$this->finishChildDepositions();
		foreach( $this->folders as $folder ) {
			if( in_array( $folder->class, [ Edepo::FOLDERS_EXHIBIT, Edepo::FOLDERS_TRANSCRIPT ] ) ) {
				$folder->deleteFromElasticSearch();
				continue;
			}
			$folder->delete();
		}
	}

	public function save() {
		//deprecated per ED-23
		$this->checkAttendeeLimit();
		$insert = ($this->isInstantiated() == false);
		if( $insert ) {
			$this->beforeInsert();
		} else {
			$this->beforeUpdate();
		}
		$r = parent::save();

		// ED-1481 Soft kick anyone currently in the deposition
		if( $this->ownerDidChange ) {
			if( $this->isDemo() ) {
				$node = new \ClickBlocks\Utils\NodeJS( TRUE );
				$node->sendPostCommand( 'set_demo_deposition_owner', NULL, ['depositionID'=>$this->ID, 'ownerID'=>$this->ownerID] );
			}
			$this->ownerDidChange = FALSE;
		}
		if( $this->isWitnessPrep() ) {
			$this->createWitnessAnnotationsFolder();
		}

		if( $insert ) {
			$this->afterInsert();
		}
		return $r;
	}

	public function insert() {
		//deprecated per ED-23
		$this->checkAttendeeLimit();
		$this->beforeInsert();
		$r = parent::insert();
		$this->afterInsert();
		return $r;
	}

	/**
	 * Returns number of attendees to this deposition + to all linked depositions, if any
	 * @return int
	 */
	public function getAttendeeCount() {
		$sql = 'SELECT COUNT(*) FROM DepositionAttendees da INNER JOIN Depositions d ON d.ID=da.depositionID WHERE (da.depositionID=:id OR d.parentID=:id) AND da.banned IS NULL';
		return (int)$this->getDAL()->getDB()->col( $sql, array( 'id' => $this->ID ) );
	}

	/**
	 * @deprecated per ED-23
	 */
	public function checkAttendeeLimit() {
		$this->attendeeLimit = 0;
		//if (!$this->attendeeLimit) $this->attendeeLimit = $this->cases->clients->includedAttendees;
	}

	public function delete() {
		foreach( $this->folders as $folder ) {
			if( in_array( $folder->class, [ Edepo::FOLDERS_EXHIBIT, Edepo::FOLDERS_TRANSCRIPT ] ) ) {
				$folder->deleteFromElasticSearch();
				continue;
			}
			$folder->delete();
		}
		$this->finishChildDepositions();
		parent::delete();
	}

	protected function _initfileShares() {
		$this->navigators['fileShares'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'fileShares');
	}

	protected function _initparentDepositions1() {
		$this->navigators['parentDepositions1'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'parentDepositions1');
	}

	protected function _initchildrenDepositions1() {
		$this->navigators['childrenDepositions1'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'childrenDepositions1');
	}

	public function getClientAdmin() {
		return (new OrchestraUsers())->getClientAdminID( $this->cases[0]->clientID );
	}

	public function beforeInsert() {
		if( !$this->exhibitTitle ) {
			$this->exhibitTitle = '';
		}
		if( !$this->exhibitSubTitle ) {
			$this->exhibitSubTitle = '';
		}
		if( !$this->exhibitDate ) {
			$this->exhibitDate = '';
		}
		if( !$this->exhibitXOrigin ) {
			$this->exhibitXOrigin = 0;
		}
		if( !$this->exhibitYOrigin ) {
			$this->exhibitYOrigin = 0;
		}
		if( !$this->liveTranscript ) {
			$this->liveTranscript = 'N';
		}
		$this->createdBy = $this->getClientAdmin();
		if( !$this->parentID ) {
			$this->speakerID = $this->ownerID;
		}
	}

	public function beforeUpdate() {
		$this->createdBy = $this->getClientAdmin();
	}

	public function afterInsert() {
		// Charge client for all new non-demo Depositions
		if( $this->class == IEdepoBase::DEPOSITION_CLASS_DEPOSITION ) {
			ServiceInvoiceCharges::charge( $this->cases->clientID, IEdepoBase::PRICING_OPTION_DEPOSITION_SETUP, 1, $this->caseID, $this->ID );
			$client = $this->getClient();
			ServicePlatformLog::addLog( $client->resellerID, IEdepoBase::PRICING_OPTION_DEPOSITION_SETUP, 1, $client->ID, $this->createdBy, $this->caseID, $this->ID );
		}
		$orcFolders = new OrchestraFolders();
		// Automatically create Exhibit folder for creator
		$oeFolder = $orcFolders->getExhibitFolder($this->ID, $this->createdBy);
		if( !$oeFolder ) {
			$oeFolder = new Folders();
			$oeFolder->name = Folders::getExhibitFolderName( $this->class );
			$oeFolder->class = IEdepoBase::FOLDERS_EXHIBIT;
			$oeFolder->createdBy = $this->createdBy;
			$oeFolder->depositionID = $this->ID;
			$oeFolder->insert();
		}
		if( !$this->parentID && !in_array( $this->class, Edepo::trialClasses() ) ) {
			$ccFolder = $orcFolders->getCourtesyCopyFolder( $this->ID );
			if( !$ccFolder ) {
				//\ClickBlocks\Debug::ErrorLog( 'afterInsert -- creating Courtesy Copy folder' );
				$ccFolder = new Folders();
				$ccFolder->name = $this->config->logic['courtesyCopyFolderName'];
				$ccFolder->class = IEdepoBase::FOLDERS_COURTESYCOPY;
				$ccFolder->createdBy = $this->createdBy;
				$ccFolder->depositionID = $this->ID;
				$ccFolder->insert();
			}
			if( $this->isDemo() ) {
				$this->_copyDemoFiles();
			}
		}
		if( !$this->isDemo() && !$this->isWitnessPrep() ) {
			$this->addTranscriptsFolder();
		}

		// automatically start Trial Binder sessions
		if( $this->class == IEdepoBase::DEPOSITION_CLASS_TRIALBINDER && $this->statusID == IEdepoBase::DEPOSITION_STATUS_NEW ) {
			$this->start();
		}
	}

	protected function _copyDemoFiles() {
		if( $this->parentID ) {
			return;	//do not create demo folder(s) for child depositions
		}
		// Add back canned Potential Exhibits folder to the demo deposition
		$demoFolder = new \ClickBlocks\DB\Folders();
		$demoFolder->createDemoPotentialExhibitsFolder( $this->ID, $this->createdBy );
		$demoFile = new \ClickBlocks\DB\Files();
		$demoPath = Core\IO::dir( 'demos' );
		$demoFile->addDemoFiles( $demoPath, $demoFolder, $this->createdBy );

		$demoMiscFolder = new \ClickBlocks\DB\Folders();
		$demoMiscFolder->createDemoMiscFolder( $this->ID, $this->createdBy );
		$demoFile = new \ClickBlocks\DB\Files();
		$demoPath = Core\IO::dir( 'demosMisc' );
		$demoFile->addDemoFiles( $demoPath, $demoMiscFolder, $this->createdBy );

		$demoNotesFolder = new \ClickBlocks\DB\Folders();
		$demoNotesFolder->createDemoNotesFolder( $this->ID, $this->createdBy );
		$demoFile = new \ClickBlocks\DB\Files();
		$demoPath = Core\IO::dir( 'demosNotes' );
		$demoFile->addDemoFiles( $demoPath, $demoNotesFolder, $this->createdBy );
	}

	protected function _initspeaker()
	{
		$this->navigators['speaker'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'speaker');
	}

	protected function _getExhibitsIntroducedPricingTier($numIntroduced)
	{
		$numIntroduced = (int)$numIntroduced;
		if ($numIntroduced > 0) {
			if ($numIntroduced < 16) {
				return IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1;
			} elseif ($numIntroduced < 101) {
				return IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2;
			} elseif ($numIntroduced > 100) {
				return IEdepoBase::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3;
			}
		}
		return false;
	}

	protected function _getExhibitsReceivedPricingTier($numReceived)
	{
		$numReceived = (int)$numReceived;
		if ($numReceived > 0) {
			if ($numReceived < 16) {
				return IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1;
			} elseif ($numReceived < 31) {
				return IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2;
			} elseif ($numReceived > 30) {
				return IEdepoBase::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3;
			}
		}
		return false;
	}

	protected function _getDepositionClientAttendees()
	{
		$sql = 'SELECT DISTINCT userID FROM DepositionAttendees WHERE depositionID = :id AND userID IS NOT NULL AND banned IS NULL';
		return $this->getDAL()->getDB()->rows( $sql, array( 'id' => $this->ID ) );
	}

	protected function getMemberAttendees()
	{
		if( !$this->db ) {
			$this->db = $this->getDAL()->getDB();
		}
		$sql = 'SELECT da.ID, da.depositionID, da.userID FROM Depositions d
			INNER JOIN DepositionAttendees da ON (da.depositionID=d.ID)
			WHERE (d.ID=:ID OR d.parentID=:ID)
			AND da.userID IS NOT NULL
			AND da.banned IS NULL';
		//		\ClickBlocks\Debug::ErrorLog( print_r( [$sql, $this->ID], TRUE ) );
		return $this->db->rows( $sql, ['ID'=>$this->ID] );
	}

	public function getExhibitsIntroducedCount( $userID )
	{
		if( !$this->db ) {
			$this->db = $this->getDAL()->getDB();
		}
		$sql = 'SELECT COUNT(fi.ID) FROM Folders fl
			INNER JOIN Files fi ON (fi.folderID = fl.ID)
			WHERE depositionID = :depoID
			AND (fl.class = "Exhibit" OR fi.isExhibit)
			AND fi.sourceUserID = :userID';
		return (int)$this->db->col( $sql, ['depoID'=>(int)$this->ID, 'userID'=>(int)$userID] );
	}

	public function getExhibitsReceivedCount( $userID )
	{
		if( !$this->db ) {
			$this->db = $this->getDAL()->getDB();
		}
		$sql = 'SELECT COUNT(fi.ID) FROM Folders fl
			INNER JOIN Files fi ON (fi.folderID = fl.ID)
			WHERE depositionID = :depoID
			AND (fl.class = "Exhibit" OR fi.isExhibit)
			AND fi.sourceUserID != :userID';
		return (int)$this->db->col( $sql, ['depoID'=>(int)$this->ID, 'userID'=>(int)$userID] );
	}

	public function getFileCount($depoID=false)
	{
		$depoID = $depoID ? $depoID : $this->ID;
		$sql = 'SELECT COUNT(fi.ID) FROM Folders fl INNER JOIN Files fi ON fi.folderID = fl.ID WHERE depositionID = :depoID';
		return (int)$this->getDAL()->getDB()->col( $sql, array( 'depoID' => $depoID ) );
	}

	public function getDemoCannedFileCount(){
		$files = glob( Core\IO::dir('demos') . "/*.pdf" );
		if( $files ){
			return count( $files );
		}
		return 0;
	}

	protected function chargePerDepositionFees()
	{
		if( (int)$this->parentID ) {
			throw new \LogicException("Session is a child to session: {$this->parentID}");
		}
		$creator = new Users( $this->createdBy );
		if( !$creator || !$creator->ID || $creator->ID != $this->createdBy ) {
			throw new \LogicException( "Unable to determine session creator: {$this->createdBy}" );
		}
		$client = new Clients( $creator->clientID );
		if( !$client || !$client->ID || $client->ID != $creator->clientID ) {
			throw new \LogicException( "Unable to determine session client: {$creator->clientID}" );
		}
		$resellerID = ($this->enterpriseResellerID) ? $this->enterpriseResellerID : $client->resellerID;
		$reseller = new Clients( $resellerID );
		if( !$reseller || !$reseller->ID || $reseller->ID != $resellerID ) {
			throw new \LogicException("Unable to determine session reseller: {$client->resellerID}");
		}
		$attendees = $this->getMemberAttendees();
		if( !is_array( $attendees ) ) {
			\ClickBlocks\Debug::ErrorLog( "Deposition::chargePerDepositionFees -- no attendees for ID: {$this->ID}" );
			return;
		}
		$invoiceQty = 1;
		$orchPMO = new \ClickBlocks\DB\OrchestraPricingModelOptions();
		foreach( $attendees as $member ) {
			$memberID = (int)$member['userID'];
			$attendee = new Users( $memberID );
			if( !$attendee || !$attendee->ID || $attendee->ID != $memberID ) {
				\ClickBlocks\Debug::ErrorLog( __FILE__ . ' Line: ' . __LINE__ . " chargePerDepositionFees() -- Unable to determine attendee: {$memberID}" );
				continue;
			}
			$client = $attendee->getClient();
			if ($client->getReseller()->resellerClass === IEdepoBase::RESELLERCLASS_DEMO) {
				continue;
			}
			$numIntroduced = (int)$this->getExhibitsIntroducedCount( $attendee->ID );
			$numReceived = (int)$this->getExhibitsReceivedCount( $attendee->ID );
			$tierIntroduced = $this->_getExhibitsIntroducedPricingTier( $numIntroduced );
			if( $tierIntroduced !== FALSE ) {
				$priceModel = $orchPMO->getByClientAndOption( $reseller->ID, $tierIntroduced );
				ServiceInvoiceCharges::addResellerCharges( $reseller->ID, $attendee->clientID, $tierIntroduced, $invoiceQty, $priceModel['price'], $this->caseID, $this->ID, $attendee->ID );
				//				ServicePlatformLog::addLog( $reseller->ID, $tierIntroduced, $invoiceQty, $client->ID, $attendee->ID, $this->caseID, $this->ID );
			}
			$tierReceived = $this->_getExhibitsReceivedPricingTier( $numReceived );
			if( $tierReceived !== FALSE ) {
				$priceModel = $orchPMO->getByClientAndOption( $reseller->ID, $tierReceived );
				ServiceInvoiceCharges::addResellerCharges( $reseller->ID, $attendee->clientID, $tierReceived, $invoiceQty, $priceModel['price'], $this->caseID, $this->ID, $attendee->ID );
				//				ServicePlatformLog::addLog( $reseller->ID, $tierReceived, $invoiceQty, $client->ID, $attendee->ID, $this->caseID, $this->ID );
			}
		}
	}

	/**
	 * ED-1258 Create a demo deposition for a case. This is so new clients have a deposition
	 * they can utilize for training purposes.
	 * @param $case object		DB\Cases object where the deposition will reside.
	 * @param int $userID		ID of the user who will initially own the case. This will be overwritten
	 *							by the first client user to access this deposition.
	 **/
	public function createDemoDeposition( $case, $userID )
	{
		if( !$case->ID || !$userID )
		{
			return false;
		}

		$this->caseID = $case->ID;
		$this->createdBy = $userID;
		$this->ownerID = $case->createdBy;
		$this->statusID = 'N';
		$this->uKey = time();
		// Set default demo password to NULL
		$this->password = NULL;
		$this->openDateTime = date('Y-m-d H:i:s');
		$this->depositionOf = 'Jon Demo (DEMO)';
		$this->volume = '1';
		$this->title = 'Demo Witness';
		$this->location = 'Demo Location';
		$this->created = date('Y-m-d H:i:s');
		$this->courtReporterEmail = $case->clientEmail;
		$this->exhibitTitle = '';
		$this->exhibitSubTitle = '';
		$this->exhibitXOrigin = 0;
		$this->exhibitYOrigin = 0;
		$this->attendeeLimit = 0;
		$this->class = 'Demo';

		$this->save();

		// Set uKey
		$this->uKey = $this->ID;
		$this->save();

		// Add DepositionsAuth for the demo deposition
		$da = new DepositionsAuth();
		$da->ID = $this->ID;
		$da->spice = DepositionsAuth::generateSpice();
		$da->code = 'Demo321';
		$da->insert();
	}

	/**
	 * Check if the user is the owner of a demo deposition.
	 * If not and no owner is yet set, set as owner.
	 * @param int $userId 	ID of user we want to check ownership for.
	 * @return boolean		True if owner, false if not.
	 **/
	public function setDemoOwner( $userID )
	{
		// If this is a demo, see if we need to set the owner.
		if( $this->isDemo() ) {
			$oldSpeakerID = $this->speakerID;

			$this->ownerID = $userID;
			$this->speakerID = $userID;
			$this->save();

			// Remove new owner as assistant
			$srvAssist = new \ClickBlocks\DB\ServiceDepositionAssistants();
			$srvAssist->unsetDA( $this->ID, $userID );

			$orcDepo = new \ClickBlocks\DB\OrchestraDepositions();
			$orcDepo->setSpeakerIDForChildDepositions( $this->ID, $this->speakerID );

			$node = new \ClickBlocks\Utils\NodeJS( TRUE );
			$node->sendPostCommand( 'set_demo_deposition_owner', NULL, ['depositionID'=>$this->ID, 'ownerID'=>$userID] );
			$args = [
				'depositionID'=>$this->ID,
				'ownerID'=>$this->ownerID,
				'oldSpeakerID'=>$oldSpeakerID,
				'newSpeakerID'=>$this->speakerID,
				'exhibitTitle'=>$this->exhibitTitle,
				'exhibitSubTitle'=>$this->exhibitSubTitle,
				'exhibitXOrigin'=>$this->exhibitXOrigin,
				'exhibitYOrigin'=>$this->exhibitYOrigin
				];
			$node->sendPostCommand( 'deposition_setspeaker', NULL, $args );
			return TRUE;
		} else if( $this->ownerID == $userID ) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Reset a demo deposition to a pristine condition
	 * @param int $userID	ID of user who will have inital ownership.
	 **/
	public function resetDemoSession( $userID ) {
		if( !$this->isDemo() ) {
			throw new \LogicException( 'Session is not a demo session.' );
		}

		$isChildDepo = ($this->parentID);

		$notifyList = [];

		$nodeJS = new \ClickBlocks\Utils\NodeJS( TRUE );
		foreach( $this->depositionAttendees as $attendee ) {
			//ED-1494; If attendee is logged in (at cases)
			if( $attendee->userID ) {
				$notifyList[] = $attendee->userID;
			}
			$attendee->delete();
		}

		$parentStartTime = strtotime( $this->started );
		$now = date( 'Y-m-d H:i:s' );

		foreach( $this->childrenDepositions1 as $childDeposition ) {
			if( !empty( $childDeposition->depositionAttendees ) ) {
				foreach( $childDeposition->depositionAttendees as $attendee ) {
					//ED-1494; If attendee is logged in (at cases)
					if( $attendee->userID ) {
						$notifyList[] = $attendee->userID;
					}
					$attendee->delete();
				}
			}

			// ED-1434
			$childCase = ( new \ClickBlocks\DB\ServiceCases() )->getByID( $childDeposition->caseID );
			if( $childCase->created ) {

				$childDepositionTime = strtotime( $childDeposition->created );
				$childCaseTime = strtotime( $childCase->created );

				// Use abs() since we could possibly have child deposition times older than their case times.
				if( $childDepositionTime > $parentStartTime && ( abs( $childCaseTime - $childDepositionTime ) <= 2 ) && count($childCase->depositions) < 2 ) {
					$childCase->delete();
				} else if( $childDepositionTime > $parentStartTime && ( abs( $childCaseTime - $childDepositionTime ) <= 2 ) ) {
					// This is a very special case. Someone added additional deposition(s) to the 2nd client's
					// new case while the parent deposition is in progress. Do NOT delete the new child case,
					// only the actual child deposition.
					$childDeposition->delete();
				} else if( $childDepositionTime > $parentStartTime && ( abs( $childCaseTime - $childDepositionTime ) >= 2 ) ) {
					$childDeposition->delete();
				} else {
					// Do NOT delete the child deposition OR case. Just finish the child deposition
					$childDeposition->parentID = NULL;
					$childDeposition->statusID = 'F';
					$childDeposition->finished = $now;
					$childDeposition->update();
				}
			}
		}

		// Kick 'em all out
		$node = new \ClickBlocks\Utils\NodeJS( TRUE );
		if( $isChildDepo ) {
			$node->sendPostCommand('reset_deposition', NULL, ['depositionID'=>$this->parentID, 'notifyUsers'=>$notifyList] );
		} else {
			$node->sendPostCommand('reset_deposition', NULL, ['depositionID'=>$this->ID] );
		}

		// ED-1475 Always clean up folders on reset.
		foreach( $this->folders as $folder ) {
			$folder->delete();
		}

		// Remove all assistants
		$orcAssist = new \ClickBlocks\DB\OrchestraDepositionAssistants();
		$orcAssist->deleteByDeposition( $this->ID );

		// unlink any case folders
		(new OrchestraSessionCaseFolders())->unlinkCaseFolders( $this->ID );

		$this->statusID = 'N';
		$this->created = $now;
		$this->ownerID = $userID;
		$this->createdBy = $userID;
		$this->speakerID = $userID;
		$this->parentID = NULL;
		$this->started = NULL;
		$this->deleted = NULL;
		$this->finished = NULL;
		$this->courtReporterEmail = NULL;
		$this->courtReporterPassword = NULL;
		$this->openDateTime = $now;
		$this->exhibitTitle = '';
		$this->exhibitSubTitle = '';
		$this->exhibitDate = '';
		$this->exhibitXOrigin = 0;
		$this->exhibitYOrigin = 0;

		$this->save();
		$this->afterInsert();

		if( $this->class == IEdepoBase::DEPOSITION_CLASS_WPDEMO ) {
			$this->createWitnessAnnotationsFolder();
		}

		//Let everyone one who attend the deposition that it's status has changed.
		$nodeJS->notifyUserCasesUpdated( $notifyList );
	}

	public function checkPermission( $userID )
	{
		$userID = (int)$userID;
		$depositionID = (int)$this->ID;
		$case = $this->cases[0];
		if( !$userID || !$depositionID || !$case ) {
			return FALSE;
		}
		$clientID = (int)$case->clientID;
		$user = new \ClickBlocks\DB\Users( $userID );
		if( !$user || !$user->ID || $user->ID != $userID || $user->clientID != $clientID ) {
			return FALSE;
		}

		//client admin
		if( $user->typeID === IEdepoBase::USER_TYPE_CLIENT ) {
			return TRUE;
		}
		//demo case
		if( $case->class === IEdepoBase::CASE_CLASS_DEMO ) {
			return TRUE;
		}
		//case managers
		$orchCaseMgrs = new \ClickBlocks\DB\OrchestraCaseManagers();
		if( $orchCaseMgrs->checkCaseManager( $this->caseID, $userID ) ) {
			return TRUE;
		}
		//deposition owner
		if( intval( $this->ownerID ) === $userID ) {
			return TRUE;
		}
		//deposition assistant
		$orchDepoAssist = new \ClickBlocks\DB\OrchestraDepositionAssistants();
		if( $orchDepoAssist->checkDepositionAssistant( $depositionID, $userID ) ) {
			return TRUE;
		}
		//deposition attendee
		$orchDepoAtt = new \ClickBlocks\DB\OrchestraDepositionAttendees();
		$attendee = $orchDepoAtt->getAttendeeForDepoID( $depositionID, $userID );
		if( $attendee && intval( $attendee['userID'] ) === $userID ) {
			return TRUE;
		}
		return FALSE;
	}

	protected function createWitnessAnnotationsFolder() {
		$orcFolders = new OrchestraFolders();
		$folder = $orcFolders->getWitnessAnnotationsFolder( $this->ID, $this->ownerID );
		if( !$folder || !$folder->ID ) {
			//\ClickBlocks\Debug::ErrorLog( "Creating WitnessAnnotations folder for: {$this->ID}, {$this->ownerID}" );
			$waFolder = new Folders();
			$waFolder->name = $this->config->logic['witnessAnnotationsFolderName'];
			$waFolder->class = IEdepoBase::FOLDERS_WITNESSANNOTATIONS;
			$waFolder->createdBy = $this->ownerID;
			$waFolder->depositionID = $this->ID;
			$waFolder->insert();
		}
	}

	/**
	 * @return string
	 */
	public function friendlyClass() {
		switch( $this->class ) {
			case IEdepoBase::DEPOSITION_CLASS_DEMO:
				return 'Demo Deposition';
			case IEdepoBase::DEPOSITION_CLASS_WITNESSPREP:
				return 'Witness Prep';
			case IEdepoBase::DEPOSITION_CLASS_WPDEMO:
				return 'Demo Witness Prep';
			case IEdepoBase::DEPOSITION_CLASS_DEPOSITION:
			case IEdepoBase::DEPOSITION_CLASS_TRIAL:
			case IEdepoBase::DEPOSITION_CLASS_DEMOTRIAL:
			case IEdepoBase::DEPOSITION_CLASS_TRIALBINDER:
			case IEdepoBase::DEPOSITION_CLASS_ARBITRATION:
			case IEdepoBase::DEPOSITION_CLASS_HEARING:
			case IEdepoBase::DEPOSITION_CLASS_MEDIATION:
			default:
				return $this->class;
		}
	}

	public function isDemo() {
		return ($this->class == IEdepoBase::DEPOSITION_CLASS_DEMO || $this->class == IEdepoBase::DEPOSITION_CLASS_WPDEMO || $this->class == IEdepoBase::DEPOSITION_CLASS_DEMOTRIAL);
	}

	public function isWitnessPrep() {
		return ($this->class == IEdepoBase::DEPOSITION_CLASS_WITNESSPREP || $this->class == IEdepoBase::DEPOSITION_CLASS_WPDEMO);
	}

	public function isTrial() {
		return in_array( $this->class, Edepo::trialClasses() );
	}
}
