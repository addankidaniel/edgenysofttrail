<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $depositionID
 * @property bigint $userID
 * @property navigation $depositions
 * @property navigation $users
 */
class DepositionAssistants extends BLLTable
{
	public function __construct( $depositionID=NULL, $userID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALDepositionAssistants(), __CLASS__ );
		if( $depositionID && $userID ) {
			$this->assignByID( ['depositionID'=>$depositionID, 'userID'=>$userID] );
		}
	}

	protected function _initusers()
	{
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'users' );
	}

	protected function _initdepositions()
	{
		$this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'depositions' );
	}
}
