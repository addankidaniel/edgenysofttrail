<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property string $name
 * @property string $email
 * @property bigint $clientMessageID
 * @property timestamp $createdOn
 * @property timestamp $sentOn
 */
class ClientNotifications extends BLLTable {
	public function __construct($ID = null) {
		parent::__construct();
		$this->addDAL( new DALClientNotifications(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
	}

	protected function _initclientmessages() {
		$this->navigators['clientmessages'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'clientmessages');
	}
}

?>