<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\API,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $enterpriseResellerID
 * @property bigint $enterpriseClientID
 * @property tinyint $isInactive
 * @property tinyint $isDisabled
 * @property timestamp $created
 * @property timestamp $accepted
 * @property timestamp $deleted
 */
class EnterpriseAgreements extends BLLTable
{
	public function __construct()
	{
		parent::__construct();
		$this->addDAL(new DALEnterpriseAgreements(), __CLASS__);
	}

	public function setAsDeleted()
	{
		$this->deleted = 'NOW()';
		$this->update();
	}
}

?>
