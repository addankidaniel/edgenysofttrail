<?php

namespace ClickBlocks\DB;

/**
 * @property bigint $folderID
 * @property bigint $sessionID
 * @property navigation $folders
 * @property navigation $sessions
 */
class SessionCaseFolders extends BLLTable {

	const DB = 'db0';
	const BASENAME = 'SessionCaseFolders';
	const NSCLASS = __CLASS__;

	public function __construct( $folderID=NULL, $sessionID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALSessionCaseFolders(), __CLASS__ );
		if( $folderID && $sessionID ) {
			$this->assignByID( ['folderID'=>$folderID, 'sessionID'=>$sessionID] );
		}
	}

	protected function _initfolders() {
		$this->addNavigator( 'folders', __CLASS__ );
	}

	protected function _initsessions() {
		$this->addNavigator( 'sessions', __CLASS__ );
	}
}
