<?php

namespace ClickBlocks\DB;

use ClickBlocks\Utils\NodeJS,
	ClickBlocks\Debug;

/**
 * @property int $userID
 * @property int $fileID
 * @property int $sortPos
 */
class UserCustomSortFiles extends BLLTable {

	const DB = 'db0';
	const BASENAME = 'UserCustomSortFiles';
	const NSCLASS = __CLASS__; // \ClickBlocks\DB\UserCustomSortFiles

	public function __construct( $userID=NULL, $fileID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserCustomSortFiles(), __CLASS__ );
		if( $userID && $fileID ) {
			$this->assignByID( ['userID'=>$userID, 'fileID'=>$fileID] );
		}
	}

	/**
	 * save Files custom sort
	 * @param int $csUserID
	 * @param array $fileIDs
	 * @param int $byUserID
	 */
	public static function setCustomSort( $csUserID, array $fileIDs, $byUserID=0 ) {
		if( !$csUserID || !$fileIDs ) {
			return;
		}
		foreach( $fileIDs as $fileID => $sortPos ) {
			$fileID = (int)$fileID;
			$sortPos = (int)$sortPos;
			if( !$sortPos || $sortPos <= 0 ) {
				continue;
			}
			$cs = new UserCustomSortFiles( $csUserID, $fileID );
			if( $cs->fileID != $fileID ) {
				$cs->userID = $csUserID;
				$cs->fileID = $fileID;
			}
			$cs->sortPos = $sortPos;
			$cs->save();
		}
		$now = date( 'Y-m-d H:i:s' );
		$keys = array_keys( $fileIDs );
		$firstID = array_shift( $keys );
		$orcFolders = new OrchestraFolders();
		$orcDepo = new OrchestraDepositions();
		$folders = $orcFolders->getFoldersForFile( $firstID );
		if( !$folders || !is_array( $folders ) ) {
			Debug::ErrorLog( "UserCustomSortFiles::setCustomSort; no folders found for fileID: {$firstID}" );
			return;
		}
		foreach( $folders as $folderInfo ) {
			$folder = new Folders();
			$folder->assign( $folderInfo );
			$folder->lastModified = $now;
			$folder->update();
			$sessionIDs = [];
			if( $folder->isCaseFolder() ) {
				$sessions = $orcDepo->getAllByCase( $folder->caseID );
				if( is_array( $sessions ) && $sessions ) {
					foreach( $sessions as $session ) {
						$sessionIDs[] = $session['ID'];
					}
				}
			} else {
				$sessionIDs[] = $folder->depositionID;
			}
			if( $sessionIDs ) {
				$nodeJS = new NodeJS( TRUE );
				foreach( $sessionIDs as $sessionID ) {
					$nodeJS->notifySessionChangedCustomSort( (int)$sessionID, UserSortPreferences::SORTOBJECT_FILES, (int)$csUserID, (int)$byUserID, $folder->ID );
				}
			}
		}
	}

}
