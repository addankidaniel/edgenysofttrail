<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $depositionID
 * @property bigint $userID
 * @property varchar $name
 * @property varchar $email
 * @property enum $role
 * @property timestamp $banned
 * @property navigation $depositions
 * @property navigation $users
 * @property navigation $aPISessions
 * @property navigation $fileShares
 */
class DepositionAttendees extends BLLTable
{
	public function __construct( $ID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALDepositionAttendees(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

	protected function _inituniqueids()
	{
		$this->navigators['uniqueids'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'uniqueids');
	}

  protected function _initdepositions()
  {
    $this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositions');
  }

  protected function _initusers()
  {
    $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }

  protected function _initaPISessions()
  {
    $this->navigators['aPISessions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'aPISessions');
  }

  protected function _initfileShares()
  {
    $this->navigators['fileShares'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'fileShares');
  }

	public function onFirstAttendedDepo( $depo=null )
	{
		// When logged in first time (attendee created), clone Exhibit folder from admin user
		if( !$depo ) $depo = $this->depositions[0];
		$orF = new OrchestraFolders();
		// Create Personal Folder on attendance
		$myPersonalFolder = $orF->createPersonalFolder( $depo->ID, $this->userID );
	}
}

?>