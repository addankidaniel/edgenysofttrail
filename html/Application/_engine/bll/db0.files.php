<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
  ClickBlocks\Utils,
  ClickBlocks\PDFDaemon,
  ClickBlocks\Elasticsearch,
  ClickBlocks\Debug;

/**
 * @property bigint $ID
 * @property bigint $folderID
 * @property bigint $createdBy
 * @property bigint $sourceUserID
 * @property varchar $name
 * @property timestamp $created
 * @property tinyint $isExhibit
 * @property tinyint $isPrivate
 * @property tinyint $isTranscript
 * @property bigint $sourceID
 * @property tinyint $isUpload
 * @property string $mimeType
 * @property integer $filesize
 * @property string $hash
 * @property navigation $folders
 * @property navigation $users
 * @property navigation $fileShares
 * @property navigation $exhibitHistory
 */
class Files extends BLLTable
{
   /**
    * @var \ClickBlocks\Utils\FileSystem
    */
   protected $fs = null;
   protected $oldName = null;
   protected $oldFolderID = null;
   protected $oldFullName = null;
   protected $overwrite = false;
   private $sourceFile = array();

  public function __construct($ID = null)
  {
     parent::__construct();
     $this->addDAL(new DALFiles(), __CLASS__);
     if ($ID) $this->assignByID($ID);
     $this->fs = new Utils\FileSystem();
   }

  public function attachMock($mock)
  {
      if ($mock instanceof Utils\FileSystem) $this->fs = $mock;
   }

  public function mockNavigators()
  {
      foreach ($this->navigationFields as $field=>$v) $this->fields[$field] = new QuickNavigation();
   }

  public function init()
  {
      parent::init();
   }

  public function getFS()
  {
      return $this->fs;
   }

  protected function _initusers()
  {
    $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }

  protected function _initfolders()
  {
    $this->navigators['folders'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'folders');
  }

	protected function _initexhibithistory()
	{
		$this->navigators['exhibitHistory'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'exhibitHistory' );
	}

  public function __set($field, $value)
  {
      return parent::__set ($field, $value);
   }

  public function assign(array $data = null)
  {
      $this->oldName = $data['name'];
      $this->oldFolderID = $data['folderID'];
      return parent::assign($data);
   }

  protected function getShortClassName()
  {
      return 'Files';
   }

  /**
    * sets source file to be used; use this when creating new file:<br/>
    * $file = new DB\Files();<br/>
    * $file->setSourceFile($tempFileFullName);<br/>
    * $file->name = 'correctFileName.pdf';<br/>
    * $file->insert();
    * @param string $fullName
    * @param bool $copy true - copy source file, false - move (rename) source file to destination
    */
  public function setSourceFile($fullName, $copy = false)
  {
      $this->sourceFile = array('name'=>$fullName, 'copy'=>$copy);
      if (!$this->name) $this->name = pathinfo($fullName, PATHINFO_BASENAME);
   }

	public function delete( $fromElasticSearch=TRUE ) {
		//\ClickBlocks\Debug::ErrorLog( 'db0.files.php -- delete()' );
		$fileID = $this->ID;
		try {
			$file = $this->getFullName();
			if( $this->fs->is_file( $file) ) {
				$this->fs->unlink( $file );
			}
			if( $fromElasticSearch ) {
				$es = Elasticsearch::getInstance();
				$es->deleteDocument( $fileID );
			}
		} catch( \Exception $e ) {
			Debug::ErrorLog( [ '[Files::delete] Exception:', [ 'message' => $e->getMessage(), 'code' => $e->getCode(), 'file' => $e->getFile(), 'line' => $e->getLine() ] ] );
			throw new Core\FileOperationException( 'Delete File failed: '. $e->getMessage(), NULL, $e );
		}
		$r = parent::delete();
		//$this->setFolderLastModified();
		return $r;
	}

	public function save() {
		//\ClickBlocks\Debug::ErrorLog( 'db0.files.php -- save()' );
		$insert = ($this->isInstantiated() == FALSE);
		if( $insert ) {
			$this->beforeInsert();
		} else {
			$this->beforeUpdate(); // check only when updating record
		}
		$r = parent::save();
		if( $insert ) {
			$this->afterInsert();
		} else {
			$this->afterUpdate();
		}
		return $r;
	}

	protected function beforeInsert() {
		try {
			if( !$this->sourceFile['name'] ) {
				throw new \Exception('Source file is not set! use $file->setSourceFile()');
			}
			foreach( ['folderID','name','createdBy'] as $field ) {
				if( !$this->$field ) {
					throw new \Exception( "Not all required fields are set, cannot determine file destination: {$field}" );
				}
			}
			$this->handleNameCollision();
			$fullName = $this->getFullName();
			if( !$this->fs->is_dir( pathinfo( $fullName, PATHINFO_DIRNAME ) ) ) {
				throw new \Exception( 'Destination "'.pathinfo( $fullName, PATHINFO_DIRNAME ) .'" file path does not exist!' );
			}
			$action = $this->sourceFile['copy'] ? 'copy' : 'rename2';
			if( !$this->fs->$action( $this->sourceFile['name'], $fullName ) ) {
				throw new \Exception( "{$action} failed." );
			}
			chmod( $fullName, 0664 );
			$this->setMetadata( FALSE );
		} catch( \Exception $e ) {
			throw new Core\FileOperationException( 'Create File Failed: '.$e->getMessage(), NULL, $e );
		}
	}

	public function insert() {
		//\ClickBlocks\Debug::ErrorLog( 'db0.files.php -- insert()' );
		if( $this->isInstantiated() == FALSE ) {
			$this->beforeInsert();
		}
		$r = parent::insert();
		$this->afterInsert();
		return $r;
	}

	protected function afterInsert() {
		Elasticsearch::indexDocument( $this->ID );
		// if demo -- defer seeding cache until after watermark
		if( !$this->isDemo() ) {
			PDFDaemon::cache( $this->getFullName(), 0, PDFDaemon::SCALE_ZOOM4 );
		}
	}

	protected function beforeUpdate() {
		try {
			if( $this->name !== $this->oldName || $this->oldFolderID !== $this->folderID ) {
				$this->handleNameCollision();
				$curFileName = $this->getFullName();
				$oldFullName = $this->oldFullName ?: $this->getFullName( TRUE );
				if( $curFileName !== $oldFullName ) {
				   rename2( $oldFullName, $curFileName );
				}
			}
			$this->setMetadata( FALSE );
		} catch( \Exception $e ) {
			$this->name = $this->oldName;
			$this->folderID = $this->oldFolderID;
			throw new Core\FileOperationException( null, null, $e );
		}
		$this->oldName = $this->name;
		$this->oldFolderID = $this->folderID;
	}

	public function update() {
		//\ClickBlocks\Debug::ErrorLog( 'db0.files.php -- update()' );
		$this->beforeUpdate();
		$r = parent::update();
		$this->afterUpdate();
		return $r;
	}

	protected function afterUpdate() {
		Elasticsearch::indexDocument( $this->ID );
		PDFDaemon::cache( $this->getFullName(), 0, PDFDaemon::SCALE_ZOOM4 );
	}

	public function getFullName( $oldName=FALSE ) {
		$name = $oldName ? $this->oldName : $this->name;
		$folderID = $oldName ? $this->oldFolderID : $this->folderID;
		$folder = new Folders( $folderID );
		return $folder->getFullPath() . DIRECTORY_SEPARATOR . $name;
	}

  public function handleNameCollision()
  {
      $siblings = foo(new OrchestraFiles)->getFileNameMapByFolder($this->folderID);
      $name = $this->name;
      if (isset($siblings[$name])) {
         if ($this->overwrite) {
		   $file = new Files( $siblings[$name] );
           $file->delete(); // overwrite file
		   $this->ID = $file->ID;
         } else {
            $i = 1;
            while($i<9999) {
              $pos = strrpos($name, '.');
              $tryName = substr($name, 0, $pos).'_'.$i.substr($name, $pos);
              if (!isset($siblings[$tryName])) break;
              $i++;
            }
            $this->name = $tryName;
         }
      }
   }

  public function setOverwrite($mode = true)
  {
     $this->overwrite = (bool)$mode;
   }

  /**
    * Returns a copy of file; you can change folderID, createdBy, name of this file and then you MUST insert() it
    * @return Files a copy of file
    */
  public function copy()
  {
      $copy = new Files();
      $vals = $this->getValues();
      unset($vals['ID']);
      $copy->setValues($vals);
      $copy->created = date( 'Y-m-d H:i:s' );
      $copy->setSourceFile($this->getFullName(), true);
	  $copy->isUpload = 0;
      return $copy;
   }

  protected function _initfileShares()
  {
    $this->navigators['fileShares'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'fileShares');
  }

	protected function setFolderLastModified()
	{
		//\ClickBlocks\Debug::ErrorLog( 'db0.files.php -- setFolderLastModified' );
		foo(new OrchestraFolders())->setLastModified( $this->folderID );
	}

	public function checkUserAccess( $userID )
	{
		$folder = $this->folders;
		$depo = $folder->depositions;
		$orchDepoAtt = new \ClickBlocks\DB\OrchestraDepositionAttendees();
		$attended = $orchDepoAtt->checkUserAttendedDepo( $depo->ID, $userID );
		$orchDepos = new \ClickBlocks\DB\OrchestraDepositions();
		$hasAccess = (($attended && ($this->createdBy == $userID || $folder->class == 'Exhibit' )) || ($this->createdBy == $depo->createdBy && $orchDepos->isTrustedUser( $userID, $depo->ID )));
		return $hasAccess;
	}

	public function checkGuestAccess( DepositionAttendees $attendee )
	{
		$depo = $this->folders->depositions;
		$attended = ($attendee->depositionID == $depo->ID);
		$hasAccess = ($attended && $this->folders->class == 'CourtesyCopy');
		return $hasAccess;
	}

	public function addDemoFiles( $demoPath, $folder, $userID ) {
		$demoFiles = scandir( $demoPath );
		foreach( $demoFiles as $file ) {
			if( pathinfo( $file, PATHINFO_EXTENSION ) == 'pdf' ) {
				$copy = new Files();
				$copy->folderID = $folder->ID;
				$copy->createdBy = $userID;
				$copy->sourceUserID = $userID;
				$copy->name = $file;
				$copy->created = date( 'Y-m-d H:i:s' );
				$copy->setSourceFile( "{$demoPath}/{$file}", TRUE );
				$copy->insert();
			}
		}
	}

	public function getValues( $isRawData=FALSE, $userID=NULL )
	{
		$values = parent::getValues( $isRawData, $userID );
		if( $userID ) {
			$orchFiles = new \ClickBlocks\DB\OrchestraFiles();
			$values['sortPos'] = $orchFiles->getSortPosition( $this->ID, $userID );
		}
		return $values;
	}

	/**
	 * File belongs to a folder in a Demo session or Demo case
	 * @return boolean
	 */
	public function isDemo() {
		$orcFolders = new OrchestraFolders();
		return $orcFolders->isDemo( $this->folderID );
	}

	public function setMetadata( $update=TRUE ) {
		//clearstatcache();
		$filePath = $this->getFullName();
		if( is_readable( $filePath ) ) {
			$this->mimeType = mime_content_type( $filePath );
			$this->filesize = filesize( $filePath );
			$this->hash = hash_file( 'MD5', $filePath );
			if( $this->ID && $update ) {
				$this->update();
			}
		} else {
			Debug::ErrorLog( print_r( ['Files::setMetadata', $this->getValues()], TRUE ) );
			$this->mimeType = '';
			$this->filesize = 0;
			$this->hash = '';
		}
	}
}
