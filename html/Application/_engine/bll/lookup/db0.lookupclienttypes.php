<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $typeID
 * @property varchar $type
 * @property navigation $clients
 */
class LookupClientTypes extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupClientTypes(), __CLASS__);
  }

  protected function _initclients()
  {
    $this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'clients');
  }
}

?>