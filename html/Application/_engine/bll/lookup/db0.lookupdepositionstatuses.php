<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $statusID
 * @property varchar $status
 * @property navigation $depositions
 */
class LookupDepositionStatuses extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupDepositionStatuses(), __CLASS__);
    }

  protected function _initdepositions()
  {
    $this->navigators['depositions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'depositions');
  }
}

?>