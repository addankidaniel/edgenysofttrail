<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $typeID
 * @property varchar $type
 * @property navigation $cases
 */
class LookupCaseTypes extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupCaseTypes(), __CLASS__);
  }

  protected function _initcases()
  {
    $this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'cases');
  }
}

?>