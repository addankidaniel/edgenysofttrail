<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property varchar $typeID
 * @property varchar $type
 * @property navigation $auditLog
 */
class LookupEntityTypes extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupEntityTypes(), __CLASS__);
  }

  protected function _initauditLog()
  {
    $this->navigators['auditLog'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'auditLog');
  }
}

?>