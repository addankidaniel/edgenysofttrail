<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property char $typeID
 * @property varchar $type
 * @property navigation $users
 */
class LookupUserTypes extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupUserTypes(), __CLASS__);
  }

  protected function _initusers()
  {
    $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }
}

?>