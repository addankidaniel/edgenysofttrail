<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property int $ID
 * @property varchar $name
 * @property tinyint $isEnterprise
 * @property navigation $pricingModelOptions
 * @property navigation $invoiceCharges
 */
class LookupPricingOptions extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupPricingOptions(), __CLASS__);
  }

  protected function _initpricingModelOptions()
  {
    $this->navigators['pricingModelOptions'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'pricingModelOptions');
  }

  protected function _initinvoiceCharges()
  {
    $this->navigators['invoiceCharges'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'invoiceCharges');
  }
}

?>