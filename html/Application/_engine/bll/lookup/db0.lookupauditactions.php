<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property varchar $actionID
 * @property varchar $action
 * @property navigation $auditLog
 */
class LookupAuditActions extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALLookupAuditActions(), __CLASS__);
  }

  protected function _initauditLog()
  {
    $this->navigators['auditLog'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'auditLog');
  }
}

?>