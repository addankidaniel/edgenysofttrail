<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $clientID
 * @property enum $vendor
 * @property string $name
 * @property text $URI
 * @property navigation $clients
 */
class Datasources extends BLLTable
{
	public function __construct($ID = null)
	{
		parent::__construct();
		$this->addDAL( new DALDatasources(), __CLASS__ );
		if( $ID ) $this->assignByID( $ID );
	}

	protected function _initclients()
	{
		$this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'clients' );
	}
}

?>