<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property int $roleID
 * @property timestamp $created
 * @property navigation $roles
 * @property navigation $users
 */
class UserRoles extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALUserRoles(), __CLASS__);
  }

  protected function _initusers()
  {
    $this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
  }

  protected function _initroles()
  {
    $this->navigators['roles'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'roles');
  }
}

?>