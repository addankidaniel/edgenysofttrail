<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property timestamp $created
 * @property bigint $clientID
 * @property date $startDate
 * @property date $endDate
 * @property navigation $clients
 * @property navigation $invoiceCharges
 */
class Invoices extends BLLTable
{
  public function __construct()
  {
    parent::__construct();
    $this->addDAL(new DALInvoices(), __CLASS__);
  }

  protected function _initclients()
  {
    $this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'clients');
  }

  protected function _initinvoiceCharges()
  {
    $this->navigators['invoiceCharges'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'invoiceCharges');
  }
}

?>