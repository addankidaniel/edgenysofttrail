<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $ID
 * @property bigint $userID
 * @property char $word
 * @property char $spice
 * @property timestamp $updated
 * @property timestamp $expired
 * @property tinyint $lockCount
 * @property timestamp $softLock
 * @property timestamp $hardLock
 * @property navigation $users
 */

# defined( 'ED_MIN_PASSWORD_LENGTH' ) || define( 'ED_MIN_PASSWORD_LENGTH', 8 );
# defined( 'ED_PASSWORD_PATTTERNMATCH' ) || define( 'ED_PASSWORD_PATTTERNMATCH',  );

class UserAuth extends BLLTable
{
	const ED_MIN_PASSWORD_LENGTH = 8;
	const ED_PASSWORD_PATTTERNMATCH = 3;

	public function __construct( $ID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALUserAuth(), __CLASS__ );
		if( $ID ) {
			$orcUA = new OrchestraUserAuth();
			$this->assignByID( $orcUA->getIDByUserID( $ID ) );
		}
	}

	protected function _initusers()
	{
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty($this, __CLASS__, 'users');
	}

	public static function generateSpice() {
		return parent::generateSpice();
	}

	public static function updatePassword( $userID, $pwd, $reset=false ) {
		$auth = new UserAuth( $userID );
		if( !$auth->spice || strlen( $auth->spice ) < 1 || $reset ) {
			$auth->spice = self::generateSpice();
		}
		$auth->word = hash( 'SHA512', $pwd.$auth->spice );
		$auth->updated = date( 'Y-m-d h:i:s' );
		if( $auth->ID ) {
			$auth->update();
		} else {
			$auth->userID = $userID;
			$auth->insert();
		}
	}

	public static function unlockAccount( $userID )
	{
		$auth = (new ServiceUserAuth())->getByUserID( $userID );

		if (!$auth->ID || !$auth->hardLock)
		{
			return;
		}

		$auth->hardLock = null;
		$auth->update();
	}

	public static function validatePassword( $passwd )
	{
		if (strlen( $passwd ) < self::ED_MIN_PASSWORD_LENGTH)
		{
			return false;
		}

		$patterns = [
			"/[a-z]/",
			"/[A-Z]/",
			"/[0-9]/",
			"/[!@#$%&*_\-+=|<>(){}[\],.:;]/",
		];
		$pattern_matches = 0;
		foreach ($patterns as $pattern)
		{
			$pattern_matches += (preg_match( $pattern, $passwd ) ? 1 : 0);
		}
		return ($pattern_matches >= self::ED_PASSWORD_PATTTERNMATCH);
	}
}
