<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\API,
    ClickBlocks\Cache,
	ClickBlocks\MVC\Edepo,
	ClickBlocks\Debug;

/**
 * @property bigint $ID
 * @property char $typeID
 * @property bigint $resellerID
 * @property varchar $name
 * @property date $startDate
 * @property timestamp $created
 * @property timestamp $deactivated
 * @property timestamp $deleted
 * @property varchar $contactName
 * @property varchar $contactPhone
 * @property varchar $contactEmail
 * @property text $description
 * @property varchar $logo
 * @property varchar $bannerColor
 * @property varchar $URL
 * @property varchar $address1
 * @property varchar $address2
 * @property varchar $city
 * @property char $state
 * @property varchar $region
 * @property varchar $ZIP
 * @property char $countryCode
 * @property int $includedAttendees
 * @property int $attendeeBundleSize
 * @property varchar $location
 * @property varchar $courtReporterEmail
 * @property varchar $liveTranscriptsEmail
 * @property varchar $salesRep
 * @property tinyint $casesDemoDefault
 * @property enum $resellerLevel
 * @property enum $resellerClass
 * @property tinyint $freeTrial
 * @property navigation $parentClients1
 * @property navigation $lookupClientTypes
 * @property navigation $cases
 * @property navigation $childrenClients1
 * @property navigation $pricingModelOptions
 * @property navigation $users
 * @property navigation $invoiceCharges
 * @property navigation $invoices
 */
class Clients extends BLLTable {
	protected $oldData = null;

	public function __construct( $ID=NULL ) {
		parent::__construct();
		$this->addDAL( new DALClients(), __CLASS__ );
		if( $ID ) {
			$this->assignByID( $ID );
		}
	}

	protected function _initparentClients1() {
		$this->navigators['parentClients1'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'parentClients1' );
	}

	protected function _initlookupClientTypes() {
		$this->navigators['lookupClientTypes'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'lookupClientTypes' );
	}

	protected function _initcases() {
		$this->navigators['cases'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'cases' );
	}

	protected function _initchildrenClients1() {
		$this->navigators['childrenClients1'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'childrenClients1' );
	}

	protected function _initpricingModelOptions() {
		$this->navigators['pricingModelOptions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'pricingModelOptions' );
	}

	protected function _initusers() {
		$this->navigators['users'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'users' );
	}

	public function assign(array $data = null) {
		$this->oldData = $data;
		return parent::assign( $data );
	}

	public function getClientsCount() {
		if( $this->typeID != 'R' ) {
			return 0;
		}
		return (int)$this->getDAL()->getDB()->col( 'SELECT COUNT(*) FROM Clients WHERE resellerID=?', array( $this->ID ) );
	}

	public function setAsDeleted() {
		$this->deleted = date( 'Y-m-d H:i:s' );
		$this->update();
		foreach( $this->cases as $case ) {
			$case->delete();
		}
		$es = \ClickBlocks\Elasticsearch::getInstance();
		$es->deleteIndex( "{$this->config->elasticsearch[ 'index_prefix' ]}client:{$this->ID}" );
		$this->deleteUsers();
	}

	protected function deleteUsers() {
		$orcUsers = new OrchestraUsers();
		$users = $orcUsers->getUsersBLLByClient( $this->ID );
		foreach( $users as $user ) {
			$user->delete();
		}
	}

	/**
     * @return \ClickBlocks\DB\Clients
     */
	public function getReseller() {
		if (in_array( $this->typeID, [API\IEdepoBase::CLIENT_TYPE_RESELLER] )) {
			return $this;
		}
		$res = new Clients;
		$res->assignByID( $this->resellerID );
		return $res;
	}

	public function getBannerColor( $i = 0 ) {
		$cl = explode( ',', $this->bannerColor );
		return $cl[$i];
	}

	public function getBannerTextColor() {
		return $this->getBannerColor(1);
	}

	public function setBannerColor( $value, $i = 0 ) {
		$cl = explode( ',', $this->bannerColor );
		$cl[$i] = $value;
		$this->bannerColor = implode( ',', $cl );
	}

	public function setBannerTextColor( $value ) {
		return $this->setBannerColor( $value, 1 );
	}

	public function chargeUponCreation() {
		if( in_array( $this->typeID, [API\IEdepoBase::CLIENT_TYPE_CLIENT, API\IEdepoBase::CLIENT_TYPE_ENT_CLIENT] ) ) {
			ServiceInvoiceCharges::charge( $this->ID, API\IEdepoBase::PRICING_OPTION_BASE_LICENSING, 1 );
			ServiceInvoiceCharges::charge( $this->ID, API\IEdepoBase::PRICING_OPTION_SETUP, 1 );
			ServicePlatformLog::addLog( $this->resellerID, API\IEdepoBase::PRICING_OPTION_BASE_LICENSING, 1, $this->ID );
			ServicePlatformLog::addLog( $this->resellerID, API\IEdepoBase::PRICING_OPTION_SETUP, 1, $this->ID );
		}
	}

	public function delete() {
		// DELETE all clients of reseller
		if( in_array( $this->typeID, [ Edepo::CLIENT_TYPE_RESELLER ] ) ) {
			foreach( $this->childrenClients1 as $client ) {
				$client->delete();
			}
		}
		// DELETE all users of client
		$this->deleteUsers();
		// Delete Empty directories for Client
		$dir = Folders::getClientDirPath( $this->ID );
		if( is_dir( $dir ) ) {
			Core\IO::deleteDirectories( $dir );
		}
		parent::delete();
	}

	public function getClientAdmin() {
		return foo( new OrchestraUsers )->getObjectByQuery( array( 'clientID'=>$this->ID, 'typeID'=>API\IEdepoBase::USER_TYPE_CLIENT ) );
	}

	protected function _initinvoiceCharges() {
		$this->navigators['invoiceCharges'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'invoiceCharges' );
	}

	protected function _initinvoices() {
		$this->navigators['invoices'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'invoices' );
	}

	public function save() {
		$update = $this->isInstantiated();
		if( !$update ) {
			$this->beforeInsert();
		}
		parent::save();
		if( $update ) {
			$this->afterUpdate ();
		}
	}

	public function beforeInsert() {
		$this->freeTrial = ($this->freeTrial ? 1 : 0); // normalize (can't be null)
	}

	public function insert() {
		$this->beforeInsert();
		$r = parent::insert();
		return $r;
	}

	public function update() {
		// ED-792: allow enterprise client's sponsoring reseller to be unset
		if( $this->typeID === API\IEdepoBase::CLIENT_TYPE_ENT_CLIENT && (int)$this->resellerID === 0 ) {
			$this->resellerID = null;
		}
		parent::update();
		$this->afterUpdate();
	}

	public function afterUpdate() {
	 /* deprecated per ED-36 / ED-23
     if ($this->typeID == 'R' && ($this->includedAttendees != $this->oldData['includedAttendees'] || $this->attendeeBundleSize != $this->oldData['attendeeBundleSize'])) {
        $this->getDAL()->getDB()->update('Clients',array('includedAttendees'=>$this->includedAttendees,'attendeeBundleSize'=>$this->attendeeBundleSize),array('resellerID'=>$this->ID));
     }
	  */
		if( $this->deactivated || $this->deleted ) {
			switch( $this->typeID ) {
				case API\IEdepoBase::CLIENT_TYPE_RESELLER:
					foreach( $this->childrenClients1 as $client ) {
						$this->expireUserAPISessions( $client->users );
					}
					break;
				case API\IEdepoBase::CLIENT_TYPE_CLIENT:
					$this->expireUserAPISessions( $this->users );
					break;
			}
		}
	}

	protected function expireUserAPISessions( $users ) {
		$orcAPI = new OrchestraAPISessions();
		$nodeJS = new \ClickBlocks\Utils\NodeJS();
		foreach( $users as $user ) {
			if( $orcAPI->userHasSession( $user->ID ) ) {
				Debug::ErrorLog( "Clients::expireUserAPISessions; user has API Session: {$user->ID}" );
				$orcAPI->deleteByUser( $user->ID );
				$nodeJS = new \ClickBlocks\Utils\NodeJS();
				$nodeJS->sendPostCommand( 'attendee_kick', null, ['userID' => $user->ID] );
			}
		}
	}

	public function hasDemoCase() {
		return (int)$this->getDAL()->getDB()->col( 'SELECT ID FROM Cases WHERE clientID=? AND class=\'Demo\'', array($this->ID ) );
	}

	public function propigateLiveTranscriptsEmail() {
		//\ClickBlocks\Debug::ErrorLog( "propigateLiveTranscriptsEmail -- resellerID: {$this->ID}" );
		if( !$this->ID || !$this->liveTranscriptsEmail || $this->typeID !== \ClickBlocks\MVC\Edepo::CLIENT_TYPE_RESELLER ) {
			return;
		}
		$orchC = new OrchestraClients();
		$clients = $orchC->getResellerClients( $this->ID );
		foreach( $clients as $c ) {
			if( !$c->liveTranscriptsEmail ) {
				//\ClickBlocks\Debug::ErrorLog( "propigateLiveTranscriptsEmail -- resellerID: {$this->ID}, clientID: {$c->ID}, email: {$this->liveTranscriptsEmail}" );
				$c->liveTranscriptsEmail = $this->liveTranscriptsEmail;
				$c->update();
			}
		}
	}
}
