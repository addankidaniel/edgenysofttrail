<?php

namespace ClickBlocks\DB;

use ClickBlocks\Core,
    ClickBlocks\Cache;

/**
 * @property bigint $clientID
 * @property tinyint $optionID
 * @property decimal $price
 * @property char $typeID
 * @property navigation $clients
 * @property navigation $lookupPricingOptions
 * @property navigation $lookupPricingTerms
 */
class PricingModelOptions extends BLLTable
{
	public function __construct( $clientID=NULL, $optionID=NULL )
	{
		parent::__construct();
		$this->addDAL( new DALPricingModelOptions(), __CLASS__ );
		if( $clientID && $optionID ) {
			$this->assignByID( ['clientID'=>$clientID, 'optionID'=>$optionID] );
		}
	}

	protected function _initlookupPricingTerms()
	{
		$this->navigators['lookupPricingTerms'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'lookupPricingTerms' );
	}

	protected function _initclients()
	{
		$this->navigators['clients'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'clients' );
	}

	protected function _initlookupPricingOptions()
	{
		$this->navigators['lookupPricingOptions'] = new \ClickBlocks\DB\NavigationProperty( $this, __CLASS__, 'lookupPricingOptions' );
	}
}