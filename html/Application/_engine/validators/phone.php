<?php

namespace ClickBlocks\Web\UI\POM;

class ValidatorPhone extends ValidatorRegularExpression {

	const PHONE_REG_EXP = '/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/';

	public function __construct( $id, $message=NULL ) {

		parent::__construct( $id, $message );
		$this->properties['expression'] = self::PHONE_REG_EXP;
	}
}
