<?php

namespace ClickBlocks\Web\UI\POM;

use ClickBlocks\Core;

class ValidatorDate extends ValidatorCustom
{
   public function __construct($id, $message = null)
   {
      parent::__construct($id, $message);
      //$this->properties['serverFunction'] = 'ClickBlocks\Web\UI\POM\ValidatorDate@'.$this->uniqueID.'->validateDate';
      $this->properties['clientFunction'] = 'validateDate';
      $this->properties['client'] = true;
   }
   
   public function validateDate($ctrls, $mode = 'AND')
   {
      $flag = true;
      foreach ($ctrls as $ctrl) {
         $val = $this->page->getByUniqueID($ctrls[i])->value;
         if (!preg_match('/^(\d\d)\/(\d\d)\/(\d\d\d\d)$/', $val, $m) || !checkdate($m[1], $m[2], $m[3])) $flag = false;
      }
      return $flag;
   }

   public function JS() {
      parent::JS();
      $this->js->add(new \ClickBlocks\Web\UI\Helpers\Script('datetime',null,Core\IO::url('common-js').'/datetime.js'), 'link');
      return $this;
   }
}

?>
