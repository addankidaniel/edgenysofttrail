<?php

namespace ClickBlocks\API\v2_2_0\Logic;

use ClickBlocks\API\v2_2_0,
	ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB,
	ClickBlocks\Elasticsearch;


class Search extends Edepo
{
	public function api_search( $p )
	{
		if( $this->config->elasticsearch[ 'search_disabled' ] ) {
			$this->except( "Content search is not currently available while the indices are being rebuilt", 1000 );
		}

		$this->validateParams( [
			'terms' => [ self::TYPE_STRING, 'req'=> TRUE ],
			'sessionID' => [ self::TYPE_INTEGER, 'req'=> TRUE ],
			'options' => [ self::TYPE_ARRAY, 'req'=> FALSE ]
		] );

		$sessionID = (int)$p[ 'sessionID' ];
		if( $this->user ) {
			$userID = $this->user->ID;
			$clientID = $this->user->clientID;
		} else {
			$userID = $this->attendee->ID;
		}
		$isTrustedUser = $this->getOrchestraDepositions()->isTrustedUser( $userID, $sessionID );
		$sessionClientID = $this->getOrchestraDepositions()->getSessionClientID( $sessionID );
		$isAttendee = $this->getOrchestraDepositionAttendees()->checkAttendeeIsDepositionAttendee( $sessionID, $userID );
		if( !$isTrustedUser && !$isAttendee ) {
			$this->except( 'Permission denied', 1001 );
		}

		if( !$p[ 'options' ][ 'folderID' ] ) {
			$p[ 'options' ][ 'folderID' ] = [];
			$folders = ( new DB\OrchestraFolders() )->getDepositionFoldersForUser( $sessionID, $userID, FALSE );
			foreach( $folders as $searchFolder ){
				$p[ 'options' ][ 'folderID' ][] = $searchFolder['ID'];
			}
		}
		$p[ 'options' ][ 'sessionID' ] = $p[ 'sessionID' ];

		// Perform the search
		try {
			$result = Elasticsearch::search( "client:{$sessionClientID}", $p[ 'terms' ], $p[ 'options' ] );
		} catch( \Exception $err ) {
			$this->except( $err->getMessage(), 1001 );
		}
		$this->return['search_result'] = $result;
	}
}
