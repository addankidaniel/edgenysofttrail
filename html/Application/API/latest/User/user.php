<?php

namespace ClickBlocks\API\v2_2_0\Logic;
use ClickBlocks\API\v2_2_0,
    ClickBlocks\Core,
    ClickBlocks\DB,
	ClickBlocks\DB\UserSortPreferences,
	ClickBlocks\MVC,
	ClickBlocks\Web\UI\POM,
	ClickBlocks\MVC\Backend\PageSignup,
	ClickBlocks\Debug;

/**
 * User controller should handle all actions related to Users, authorization, etc.
 *
 * @author Kolosovsky Vladislav
 * @property ClickBlocks\DB\ServiceUsers $svc Description
 */
class User extends Edepo {

	const ATTENDEE_ROLE_GUEST = 'G';
	const ATTENDEE_ROLE_WITNESS = 'W';

    public function api_logout()
    {
      if ($this->isUserMember())
        $this->deleteSession($this->user->ID, $this->user->typeID);
      else
        $this->deleteSession($this->attendee->ID, self::USER_TYPE_GUEST);
    }

    protected function getRelatedEntity() {
       return 'Users';
    }

    protected function getNoAuthMethods() {
       return array_merge(parent::getNoAuthMethods(), array('login','loginGuest','guestLogin','witnessLogin','recoverPassword','checkVersion','signup'));
    }

	public function api_recoverPassword($p)
	{
		$this->validateParams(array(
			'username' => array('type'=>self::TYPE_STRING, 'req'=>true),
		));

		// $user = $this->getOrchestraUsers()->getUserByUsername($p['username']);
		$userID = (new DB\OrchestraUsers())->getUserIDByUsername( $p['username'] );
		if (!$userID)
		{
			$this->except('User not found', 1);
		}

		DB\PasswordResetRequests::sendToUser( $userID );

		$user = new \ClickBlocks\DB\Users( $userID );
		return array('email'=>$user->email);
	}

    public function api_renewSession() {
       if ($this->isUserGuest()) $sKey = $this->startSession($this->attendee->ID, self::USER_TYPE_GUEST);
       else $sKey = $this->startSession($this->user->ID, $this->user->typeID);
       $this->return = array('sKey'=>$sKey, 'leaseTime'=> $this->sessionLifetime);
    }

    protected function checkDepositionKey( $p ) {
       $depoID = (int)$this->getOrchestraDepositions()->checkDepositionUKey( $p['depositionKey'], TRUE );
       if (!$depoID) $this->except ('incorrect depositionKey', 1001);
       return $depoID;
    }

	public function api_login( $p )
	{
		$this->validateParams( [
			'username' => [self::TYPE_STRING, 'req'=>TRUE],
			'password' => [self::TYPE_STRING, 'req'=>TRUE],
			'depositionID' => [self::TYPE_STRING, 'req'=>FALSE],
			'depositionPasscode' => [self::TYPE_STRING, 'req'=>FALSE]
		] );

		$userInfo = $this->getOrchestraUsers()->secureLoginAPI( $p['username'], $p['password'] );

		if( empty( $userInfo ) ) {
			$this->except( 'Unauthorized', 401 );
		}
		if( isset( $userInfo['unfederated'] ) ) {
			$this->except( 'Password Reset', 418 );
		}
		if( isset( $userInfo['accountLocked'] ) ) {
			$this->except( 'Too Many Requests', 429 );
		}
		if( isset( $userInfo['accountBanned'] ) ) {
			$this->except( 'Forbidden', 403 );
		}
		if( !$userInfo['ID'] ) {
			$this->except( 'Authorization Failed', 401 );
		}
		$this->return = ['needToLinkDepo' => FALSE];
		if( $p['depositionID'] ) {
			if( !$p['depositionPasscode'] ) {
				$this->except( 'Passcode is required to attend!', 201 );
			}
			$depoID = $inDepoID = (int)$p['depositionID'];
			$depo = new \ClickBlocks\DB\Depositions( $depoID );
			if( !$depo || !$depo->ID || $depo->ID != $depoID ) {
				$this->except( 'Incorrect Session ID or Passcode', 1001 );
			}
			if( $depo->comparePassword( $p['depositionPasscode'] ) == FALSE ) {
				$this->except( 'Incorrect Session ID or Passcode', 1001 );
			}
			$depoCase = $depo->cases[0];
			if( $depoCase->deleted ) {
				$this->except( 'Incorrect Session ID or Passcode', 1001 );
			}
			$depoType = $depo->friendlyClass();
			// Check if this is a demo, and if I need to become the owner.
			if( $depo->isDemo() && $depo->started == NULL && $depoCase->clientID == $userInfo['clientID'] ) {
				$redis = new \ClickBlocks\Cache\CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
				$isOwned = $redis->get( 'demo:'.$depo->ID );
				if( !$isOwned ) {
					if( !$depo->setDemoOwner( $userInfo['ID'] ) ) {
						$this->except( "Cannot login, {$depoType} not started", 1002 );
					}
				} else {
					$this->except( "Cannot login, {$depoType} not started", 1002 );
				}
			} elseif( $depo->started == NULL && !$this->getOrchestraDepositions()->isTrustedUser($userInfo['ID'], $depo->ID) ) {
				$this->except( "Cannot login, {$depoType} not started", 1002 );
			}
			if( $p['depositionID'] && $depo->finished ) {
				$this->except( "Cannot login, {$depoType} is finished", 1003 );
			}
			if( $depo->class == self::DEPOSITION_CLASS_TRIALBINDER && $depoCase->clientID != $userInfo['clientID'] ) {
				$this->except( "Cannot join a '{$depo->class}' session", 1005 );
			} elseif( $depo->class == self::DEPOSITION_CLASS_TRIALBINDER && $depoCase->clientID == $userInfo['clientID'] ) {
				if( !$this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $depo->ID ) ) {
					$this->except( "Cannot join a '{$depo->class}' session", 1006 );
				}
			}

			$createAttendee = TRUE;
			// FOR Associated/Linked depo: Cannot directly login to depo of another client, RESOLVE:
			if( $depoCase->clientID != $userInfo['clientID'] ) {
				if( $depo->parentID && $depo->getParentDeposition()->cases->clientID == $userInfo['clientID'] ) {
					// this deposition is linked to some deposition of my client, login silently to my deposition
					$depoID = (int)$depo->parentID;
					$depo = $depo->getParentDeposition();
				} else {
					$targetDepo = $this->getOrchestraDepositions()->getChildDepositionForClient( $depoID, $userInfo['clientID'] );
					if ($targetDepo['ID']) // this client already has child depo for this depo, so login silently to linked depo instead
					{
						$depoID = (int)$targetDepo['ID'];
						$depo = $this->getService('Depositions')->getByID($targetDepo['ID']);
						// If this user is not Case manager, not Depo Assistant, set him automatically as  assistant
						$amICM = (bool)$this->getService( 'CaseManagers' )->getByID( ['caseID'=>$depo->caseID, 'userID'=>$userInfo['ID']] )->userID;
						if( !$amICM ) {
							$this->getService( 'DepositionAssistants' )->setDA( $depoID, $userInfo['ID'] );
						}
					} else {
						// return to display popup
						$createAttendee = false;
						$this->return['needToLinkDepo'] = TRUE;
						$this->return['deposition'] = ['ID'=>$depo->ID, 'class'=>$depo->class, 'depositionOf'=>$depo->depositionOf, 'caseID'=>$depo->caseID];
					}
				}
			}

			if( $userInfo['clientTypeID'] == self::CLIENT_TYPE_COURTCLIENT && $this->return['needToLinkDepo'] ) {
				$depoCtrl = new Deposition();
				$depoCtrl->user = new DB\Users( $userInfo['ID'] );
				$depo = $depoCtrl->linkSession( ['sourceDepositionID' => $inDepoID] );
				$depoID = $depo->ID;
				$this->return['needToLinkDepo'] = FALSE;
				$createAttendee = TRUE;
			}

			if( $createAttendee ) {
				$this->userAttendToDeposition( $userInfo['ID'], $depo );
				$this->return['depositionID'] = (int)$depoID;
				$this->return['deposition'] = BLLFormat::getValues( $depo, $userInfo['ID'] );
				$this->return['folders'] = BLLFormat::formatRows( $this->getOrchestraFolders()->getFoldersForUser( $depoID, $userInfo['ID'] ), 'folders' );
				$this->return['deposition']['exhibitHistory'] = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $depo->ID );
				$this->return['deposition']['liveTranscript'] = (!$depo->isWitnessPrep()) ? 'Y' : 'N';
			}
		}
		$user = array_intersect_key( $userInfo, ['ID'=>1,'firstName'=>1,'lastName'=>1,'email'=>1,'username'=>1,'clientID'=>1,'typeID'=>1, 'clientTypeID'=>1] );
		$db_user = new \ClickBlocks\DB\Users( $user['ID'] );
		$db_user->lastLogin = date( 'Y-m-d H:i:s' );
		$db_user->update();
		$user['username'] = mb_strtolower( $user['username'] );
		$user['sortPreferences'] = $this->getOrchestraUserSortPreferences()->getSortPreferencesForUserID( $userInfo['ID'] );
		$user['userPreferences'] = $this->getOrchestraUserPreferences()->getPreferencesForUserID( $userInfo['ID'] );
		$this->return += ['sKey' => $this->startSession( $userInfo['ID'], $userInfo['typeID'] ), 'leaseTime'=>$this->sessionLifetime, 'user'=>$user];
		$this->tutorialVideos();
	}

	/**
	 * @deprecated since version 1.6.3
	 */
	public function api_loginGuest( $p )
	{
		return $this->api_guestLogin( $p );
	}

	public function api_guestLogin( $p )
	{
		$this->validateParams( [
			'email' => [self::TYPE_EMAIL, 'req'=>TRUE],
			'name' => [self::TYPE_STRING, 'req'=>TRUE],
			'depositionID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'depositionPasscode' => [self::TYPE_STRING, 'req'=>TRUE]
		] );
		$depoID = (int)$p['depositionID'];
		$depo = new \ClickBlocks\DB\Depositions( $depoID );
		if( !$depo || !$depo->ID || $depo->ID != $depoID ) {
			$this->except( 'Incorrect Session ID or Passcode', 1001 );
		}
		if( $depo->comparePassword( $p['depositionPasscode'] ) == FALSE ) {
			$this->except( 'Incorrect Session ID or Passcode', 1001 );
		}
		if( in_array( $depo->class, MVC\Edepo::trialClasses() ) ) {
			$this->except( "Guests are not permitted to attend {$depo->class} sessions", 1004 );
		}
		$case = new DB\Cases( $depo->caseID );
		if( $case->deleted ) {
			$this->except( 'Incorrect Session ID or Passcode', 1001 );
		}
		$depoType = $depo->friendlyClass();
		$attendee = $this->getOrchestraDepositionAttendees()->getForGuest( $depoID, $p['email'] );
		if( $attendee->banned ) {
			$this->except( "You do not have access to this {$depoType}.", self::ERR_BANNED_IN_DEPO );
		}
		if( $depo->started == NULL ) {
			$this->except( "Cannot login, {$depoType} not started", 1002 );
		}
		if ( $depo->finished ) {
			$gracePeriod = strtotime( $depo->finished );
			if( $attendee->role === self::ATTENDEE_ROLE_GUEST ) {
				// only guests (who attended) may login to a finished deposition for 24 hours
				$gracePeriod += (60 * 60 * 24);	// +24 Hours
			}
			if( time() > $gracePeriod ) {
				$this->except( "Cannot login, {$depoType} is finished", 1003 );
			}
		}
		if( !$attendee->ID ) {
			$attendee->name = $p['name'];
			$attendee->email = $p['email'];
			$attendee->depositionID = $depoID;
			$attendee->role = self::DEPOSITIONATTENDEE_ROLE_GUEST;
			$attendee->insert();
		} else {
			$attendee->name = $p['name'];
			$attendee->role = self::ATTENDEE_ROLE_GUEST;
			$attendee->update();
		}
		if( !in_array( $depo->class, MVC\Edepo::trialClasses() ) ) {
			//create Courtesy Copy if it doesn't exist
			$ccFolder = $this->getOrchestraFolders()->getCourtesyCopyFolder( $depoID );
			if( !$ccFolder ) {
				\ClickBlocks\Debug::ErrorLog( 'loginGuest -- creating Courtesy Copy folder' );
				$ccFolder = new \ClickBlocks\DB\Folders();
				$ccFolder->name = $this->config->logic['courtesyCopyFolderName'];
				$ccFolder->class = self::FOLDERS_COURTESYCOPY;
				$ccFolder->createdBy = $depo->createdBy;
				$ccFolder->depositionID = $depoID;
				$ccFolder->insert();
			}
		}
		$this->return = [
			'depositionID' => $depoID,
			'guestID' => $attendee->ID,
			'folders' => BLLFormat::formatRows( $this->getOrchestraFolders()->getFoldersForUser( $depoID, NULL, FALSE, self::ATTENDEE_ROLE_GUEST ), 'folders' ),
			'deposition' => BLLFormat::getValues( $depo ),
			'sKey' => $this->startSession( $attendee->ID, self::USER_TYPE_GUEST ),
			'leaseTime' => $this->sessionLifetime,
			'tutorialVideoURL' => $this->config->videos['guestURL']
		];
		$this->tutorialVideos();
	}

	public function api_witnessLogin( $p )
	{
		$this->validateParams( [
			'depositionID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'depositionPasscode' => [self::TYPE_STRING, 'req'=>FALSE],
			'skipPasscode' => [self::TYPE_BOOLEAN, 'req'=>FALSE],
			'witnessID' => [self::TYPE_INTEGER, 'req'=>FALSE]
		] );
		$depositionID = (int)$p['depositionID'];
		$skipPasscode = (bool)$p['skipPasscode'];
		$witnessID = isset( $p['witnessID'] ) ? $p['witnessID'] : 0;
		$witnessRole = self::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS;
		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID || $deposition->parentID ) {
			$this->except( 'Invalid session ID', 1001 );
		}
		$attendee = new DB\DepositionAttendees( $witnessID );
		if( $attendee->ID && $attendee->depositionID != $deposition->ID ) {
			// witness ID doesn't match session ID, create new attendee
			$attendee = new DB\DepositionAttendees();
		}
		$depoType = $deposition->friendlyClass();
		if( $deposition->statusID == self::DEPOSITION_STATUS_FINISHED ) {
			$this->except( "Cannot login, {$depoType} is finished", 1004 );
		}
		$reqPasscode = ($deposition->isWitnessPrep() || $deposition->class == self::DEPOSITION_CLASS_TRIALBINDER);
		if( $deposition->isWitnessPrep() && !$skipPasscode  && (!isset( $p['depositionPasscode'] ) || !$p['depositionPasscode']) ) {
			$this->except( 'Session passcode or skip passcode parameter is required', 1003 );
		}
		if( $deposition->class == self::DEPOSITION_CLASS_TRIALBINDER && !$skipPasscode  && (!isset( $p['depositionPasscode'] ) || !$p['depositionPasscode']) ) {
			$this->except( 'Session passcode or skip passcode parameter is required', 1005 );
		}
		if( $reqPasscode && !$skipPasscode && (isset( $p['depositionPasscode'] ) && $p['depositionPasscode']) ) {
			if( $deposition->comparePassword( $p['depositionPasscode'] ) == FALSE ) {
				$this->except( 'Incorrect Session ID or Passcode' );
			} else {
				$witnessRole = self::DEPOSITIONATTENDEE_ROLE_WITNESS;
				if( $deposition->isWitnessPrep() ) {
					$witnessRole = self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER;
					$wm = $this->getOrchestraDepositionAttendees()->getWitnessMember( $depositionID );
					if( $wm && isset( $wm['ID'] ) && $wm['ID'] ) {
						$attendee->assignByID( $wm['ID'] );
					}
					//kick any other witnesses -- only for witness prep
					$witnessAttendees = $this->getOrchestraDepositionAttendees()->getWitnessAttendees( $depositionID );
					if( $witnessAttendees && is_array( $witnessAttendees ) ) {
						$args = ['depositionID'=>$depositionID, 'ban'=>FALSE, 'guestID'=>0];
						foreach( $witnessAttendees as $witness ) {
							if( $attendee->ID == $witness['ID'] ) {
								continue;
							}
							\ClickBlocks\Debug::ErrorLog( "Kicking (other) Witness from {$deposition->class}: " . print_r( $witness, TRUE ) );
							$args['guestID'] = $witness['ID'];
							$this->getNodeJS()->sendPostCommand( 'attendee_kick', null, $args );
							if( $attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER ) {
								continue;	//do not delete witness member
							}
							$att = new \ClickBlocks\DB\DepositionAttendees( $witness['ID'] );
							if( !$att || !$att->ID || $att->ID != $witness['ID'] ) {
								continue;
							}
							$att->delete();
						}
					}
				} elseif( $deposition->class == self::DEPOSITION_CLASS_TRIALBINDER ) {
					$witnessAttendees = $this->getOrchestraDepositionAttendees()->getWitnessAttendeesWithRole( $depositionID, self::DEPOSITIONATTENDEE_ROLE_WITNESS );
					if( is_array( $witnessAttendees ) && $witnessAttendees ) {
						foreach( $witnessAttendees as $witAttendee ) {
							if( $witAttendee['role'] == self::DEPOSITIONATTENDEE_ROLE_WITNESS && $witAttendee['depositionID'] == $depositionID ) {
								$attendee->assign( $witAttendee );
								break;
							}
						}
					} else {
						$attendee->email = 'witness@edepoze.com';
					}
				}
			}
		}
		$attendee->role = $witnessRole;
		if( !$deposition->started ) {
			$this->except( "Cannot login, {$depoType} not started", 1002 );
		}

		if( !$attendee->ID ) {
			$attendee->depositionID = $deposition->ID;
			$attendee->name = trim( str_replace( '(DEMO)', '', $deposition->depositionOf ) );
			$attendee->role = $witnessRole;
		}
		Debug::ErrorLog( print_r( $attendee->getValues(), TRUE ) );
		$attendee->save();
		$this->return = [
			'depositionID' => (int)$deposition->ID,
			'sKey' => $this->startSession( $attendee->ID, self::USER_TYPE_GUEST ),
			'witnessID' => (int)$attendee->ID,
			'userType' => $witnessRole,
			'name' => $attendee->name,
			'tutorialVideoURL' => $this->config->videos['witnessURL']
		];
		$this->tutorialVideos();
	}

    public function api_test($p)
    {
      if ($this->isUserMember())
        return array('member'=>$this->user->getValues());
      elseif ($this->isUserGuest())
        return array('guest'=>$this->attendee->getValues());
      else return 'No User';
    }

	public function api_checkVersion( $p )
	{
		$this->validateParams( array( 'iPadBuild'=> array( self::TYPE_STRING, 'req'=>TRUE ) ) );

		$versions = new \ClickBlocks\Core\Config();
		$versions->init( $versions->root . '/Application/_config/versions.ini' );

		if( !isset( $versions['device']['appStoreVersion'] ) )
			throw new \LogicException( 'Unable to determine application version.' );

		$liveVersionComponents = explode( '.', $versions['device']['appStoreVersion'] );
		$testVersionComponents = explode( '.', $p['iPadBuild'] );

		$updateAvailable = 'ok';

		$majorLive = (isset( $liveVersionComponents[0] )) ? (int)$liveVersionComponents[0] : 0;
		$majorTest = (isset( $testVersionComponents[0] )) ? (int)$testVersionComponents[0] : 0;
		$minorLive = (isset( $liveVersionComponents[1] )) ? (int)$liveVersionComponents[1] : 0;
		$minorTest = (isset( $testVersionComponents[1] )) ? (int)$testVersionComponents[1] : 0;
		$patchLive = (isset( $liveVersionComponents[2] )) ? (int)$liveVersionComponents[2] : 0;
		$patchTest = (isset( $testVersionComponents[2] )) ? (int)$testVersionComponents[2] : 0;

		if( $majorTest < $majorLive )
		{
			$updateAvailable = 'update';
		} elseif( $majorTest === $majorLive ) {
			if( $minorTest < $minorLive )
			{
				$updateAvailable = 'update';
			} elseif( $minorTest === $minorLive ) {
				if( $patchTest < $patchLive )
				{
					$updateAvailable = 'update';
				}
			}
		}

		$this->return = array( 'versionCheck' => $updateAvailable, 'appStoreVersion' => $versions['device']['appStoreVersion'] );
	}

	public function api_checkSession( $p )
	{
		$this->return = ['okayToGo'=>TRUE];
	}

	public function api_consoleLog( $p )
	{
		if( is_array( $p['messages'] ) ) {
			$logPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'logs' ) . '/' . v2_2_0\codeName . '.log';
			$fh = fopen( $logPath, 'a' );
			if( !$fh ) $this->except( 'Unable to write to log' );
			$sKey = substr( $p['sKey'], 0, 6 ) . '~' . substr( $p['sKey'], -6, 6 );
			foreach( $p['messages'] as $msg ) {
				$message = ['now' => date( 'r' ), 'userID' => $this->user->ID, 'sKey' => $sKey, 'message' => $msg];
				$jsonMsg = json_encode( $message, JSON_UNESCAPED_SLASHES );
				if( $jsonMsg ) fwrite( $fh, "{$jsonMsg}\n" );
			}
			fclose( $fh );
		}
		$this->return = [];
	}

	public function api_setSortPreference( $p ) {
		if( !$this->isUserMember() ) {
			$this->except( 'Access denied, member-only method', 1001 );
		}
		$this->validateParams( [
			'sortObject' => [self::TYPE_ENUM, 'req'=>TRUE, 'options'=>UserSortPreferences::sortObjects()],
			'sortBy' => [self::TYPE_ENUM, 'req'=>TRUE, 'options'=>UserSortPreferences::sortBys()],
			'sortOrder' => [self::TYPE_ENUM, 'req'=>TRUE, 'options'=>UserSortPreferences::sortOrders()],
		] );

		$sortPref = new UserSortPreferences( $this->user->ID, $p['sortObject'] );
		if( !$sortPref || !$sortPref->userID || !$sortPref->sortObject || $sortPref->userID != $this->user->ID || $sortPref->sortObject != $p['sortObject'] ) {
			$sortPref->userID = $this->user->ID;
			$sortPref->sortObject = $p['sortObject'];
		}
		$sortPref->setSortPreference( $p['sortBy'], $p['sortOrder'] );
		$this->return = ['sortPreferences' => $this->getOrchestraUserSortPreferences()->getSortPreferencesForUserID( $this->user->ID )];
	}

	public function api_setCustomSort( $p ) {
		if( !$this->isUserMember() ) {
			$this->except( 'Access denied, member-only method', 1001 );
		}
		$this->validateParams( [
			'sortObject' => [self::TYPE_ENUM, 'req'=>TRUE, 'options'=>UserSortPreferences::sortObjects()],
			'list' => [self::TYPE_ARRAY, TRUE],
			'sessionID' => [self::TYPE_INTEGER, 'req'=>FALSE]	//required for Folders and/or Files
		] );

		$sortObject = $p['sortObject'];
		$list = $p['list'];
		$isTrustedUser = FALSE;
		$csUserID = $this->user->ID;

		if( in_array( $sortObject, [UserSortPreferences::SORTOBJECT_FOLDERS, UserSortPreferences::SORTOBJECT_FILES] ) ) {
			$this->validateParams( ['sessionID' => [self::TYPE_INTEGER, 'req'=>TRUE]] );
			$sessionID = (int)$p['sessionID'];
			$isTrustedUser = $this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $sessionID );
			$csUserID = $isTrustedUser ? $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID ) : $this->user->ID;
		}

		if( !is_array( $list ) || !$list ) {
			$this->except( 'Invalid list', 1001 );
		}
		$objectIDs = [];
		foreach( $list as $item ) {
			foreach( $item as $objectID => $sortPos ) {
				$objectIDs[$objectID] = $sortPos;
			}
		}
		unset( $objectID );

		switch( mb_strtolower( $sortObject ) ):
			case 'cases':
				foreach( $objectIDs as $objectID => $sortPos ) {
					$objectID = (int)$objectID;
					$sortPos = (int)$sortPos;
					$cs = new DB\UserCustomSortCases( $this->user->ID, $objectID );
					if( $cs->caseID != $objectID ) {
						$cs->userID = $this->user->ID;
						$cs->caseID = $objectID;
					}
					$cs->sortPos = $sortPos;
					$cs->save();
				}
				break;
			case 'depositions':
				foreach( $objectIDs as $objectID => $sortPos ) {
					$objectID = (int)$objectID;
					$sortPos = (int)$sortPos;
					$cs = new DB\UserCustomSortDepositions( $this->user->ID, $objectID );
					if( $cs->depositionID != $objectID ) {
						$cs->userID = $this->user->ID;
						$cs->depositionID = $objectID;
					}
					$cs->sortPos = $sortPos;
					$cs->save();
				}
				break;
			case 'folders':
				DB\UserCustomSortFolders::setCustomSort( $csUserID, $objectIDs, $this->user->ID );
				break;
			case 'files':
				DB\UserCustomSortFiles::setCustomSort( $csUserID, $objectIDs, $this->user->ID );
				break;
		endswitch;
	}

	public function api_setUserPreference( $p )
	{
		$this->validateParams( [
			'prefKey' => [self::TYPE_ENUM, 'req'=>TRUE, 'options'=>\ClickBlocks\DB\UserPreferences::prefKeys()],
			'value' => [self::TYPE_STRING, 'req'=>FALSE]
		] );

		if( !$this->user || !$this->user->ID ) {
			$this->except( 'Invalid User', 1001 );
		}

		$userPref = new \ClickBlocks\DB\UserPreferences( $this->user->ID, $p['prefKey'] );
		if( !$userPref || !$userPref->userID || !$userPref->prefKey || $userPref->userID != $this->user->ID || $userPref->prefKey != $p['prefKey'] ) {
			$userPref->userID = $this->user->ID;
			$userPref->prefKey = $p['prefKey'];
		}
		$userPref->setPreference( $p['prefKey'], $p['value'] );
		$this->return = ['userPreferences' => $this->getOrchestraUserPreferences()->getPreferencesForUserID( $this->user->ID )];
	}

	public function api_objectExists( $p )
	{
		$this->validateParams( [
			['type' => [self::TYPE_ENUM, 'req'=>TRUE, 'options'=>['Case','Deposition','Folder','File']]],
			['objectID' => [self::TYPE_INTEGER, 'req'=>TRUE]]
		] );

		$objectType = $p['type'];
		$objectID = (int)$p['objectID'];
		$exists = FALSE;

		switch( mb_strtolower( $objectType ) ):
			case 'file':
				$exists = $this->filePermissions( $objectID );
				break;
			case 'folder':
				$exists = $this->folderPermissions( $objectID );
				break;
			case 'deposition':
				$exists = $this->depositionPermissions( $objectID );
				break;
			case 'case':
				$exists = $this->casePermissions( $objectID );
				break;
			default:
				$this->except( "Unhandled object type: {$objectType}", 1000 );
		endswitch;

		$this->return = ['exists'=>$exists];
	}

	protected function filePermissions( $fileID )
	{
		$fileID = (int)$fileID;
		$file = new \ClickBlocks\DB\Files( $fileID );
		if( !$file || !$file->ID || $file->ID != $fileID ) {
			return FALSE;
		}
		//permissions are granted at the folder
		return $this->folderPermissions( $file->folderID );
	}

	protected function folderPermissions( $folderID )
	{
		$folderID = (int)$folderID;
		$folder = new \ClickBlocks\DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			return FALSE;
		}
		$returnID = $this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $folder->ID );
		return ($returnID && intval( $folder->ID ) === intval( $returnID ));
	}

	protected function depositionPermissions( $depositionID )
	{
		$depositionID = (int)$depositionID;
		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
			return FALSE;
		}
		return $deposition->checkPermission( $this->user->ID );
	}

	protected function casePermissions( $caseID )
	{
		$caseID = (int)$caseID;
		$case = new \ClickBlocks\DB\Cases( $caseID );
		if( !$case || !$case->ID || $case->ID != $caseID ) {
			return FALSE;
		}
		return $case->checkPermission( $this->user->ID );
	}

	private function tutorialVideos() {
		$tutorialVideos = [];
		foreach( $this->config->videos as $item ) {
			$video = json_decode( $item, FALSE, JSON_UNESCAPED_SLASHES );
			if( $video && is_object( $video ) ) {
				$video->url = "https://{$this->config->http_host}{$video->url}";
				$tutorialVideos[] = $video;
			}
		}
		$this->return['tutorialVideos'] = $tutorialVideos;
	}

	/**
	 * User Sign Up
	 * @param Array $p
	 */
	public function api_signup( $p ) {
		$genErr = 'Unable to sign up at this time';
		if( !isset( $this->config->signup['resellerID'] ) || !$this->config->signup['resellerID'] ) {
			$this->except( $genErr, 1001 );
		}
		array_walk( $p, 'trim' ); // trim all values
		$countries = MVC\Edepo::getCountries( TRUE );
		array_shift( $countries ); // remove first empty
		$this->validateParams( [
			'fullName' => [self::TYPE_STRING, 'req' => TRUE],
			'firm' => [self::TYPE_STRING, 'req' => FALSE],
			'address1' => [self::TYPE_STRING, 'req' => TRUE],
			'address2' => [self::TYPE_STRING, 'req' => FALSE],
			'city' => [self::TYPE_STRING, 'req' => TRUE],
			'countrycode' => [self::TYPE_ENUM, 'req' => TRUE, 'options' => $countries],
			'phone' => [self::TYPE_STRING, 'req' => TRUE],
			'email' => [self::TYPE_EMAIL, 'req' => TRUE]
		] );

		$email = mb_strtolower( trim( $p['email'] ) );
		if( !$this->getOrchestraUsers()->checkUniqueUsername( $email ) ) {
			$this->except( 'An account with this email already exists', 1002 );
		}
		if( !preg_match( POM\ValidatorPhone::PHONE_REG_EXP, $p['phone'] ) ) {
			$this->except( 'Invalid phone number', 1003 );
		}
		switch( $p['countrycode'] ) {
			case 'CA':
				$provinces = MVC\Edepo::getCanadianProvinces( TRUE );
				array_shift( $provinces ); // remove first empty
				$this->validateParams( [
					'province' => [self::TYPE_ENUM, 'req' => TRUE, 'options' => $provinces],
					'postalcode' => [self::TYPE_STRING, 'req' => TRUE]
				] );
				break;
			case 'OT':
				$this->validateParams( [
					'region' => [self::TYPE_STRING, 'req' => TRUE],
					'postalcode' => [self::TYPE_STRING, 'req' => TRUE]
				] );
				break;
			case 'US':
			default:
				$states = MVC\Edepo::getUSAStates( TRUE );
				array_shift( $states ); // remove first empty
				$this->validateParams( [
					'state' => [self::TYPE_ENUM, 'req' => TRUE, 'options' => $states],
					'zipcode' => [self::TYPE_INTEGER, 'req' => TRUE]
				] );
				break;
		}

		$reseller = new DB\Clients( $this->config->signup['resellerID'] );
		if( !$reseller || !$reseller->ID || $reseller->ID != $this->config->signup['resellerID'] || $reseller->typeID !== self::CLIENT_TYPE_RESELLER ) {
			$this->except( $genErr, 1004 );
			return;
		}

		// done with validation -- okay to go
		$now = date( 'Y-m-d H:i:s' );
		$name = preg_split( '/\s+(?=\S*+$)/', $p['fullName'] );
		$phone = \ClickBlocks\Utils::formatPhone( $p['phone'] );

		// client
		$client = new DB\Clients();
		$client->typeID = self::CLIENT_TYPE_CLIENT;
		$client->resellerID = $reseller->ID;
		$client->name = ($p['firm'] ? $p['firm'] : $p['fullName']);
		$client->startDate = $now;
		$client->created = $now;
		$client->contactName = $p['fullName'];
		$client->contactPhone = $phone;
		$client->contactEmail = $email;
		$client->address1 = $p['address1'];
		$client->address2 = ($p['address2'] ? $p['address2'] : NULL);
		$client->city = $p['city'];
		$client->state = ($p['state'] ? $p['state'] : ($p['province'] ? $p['province'] : ''));
		$client->region = ($p['region'] ? $p['region'] : NULL);
		$client->ZIP = ($p['zipcode'] ? $p['zipcode'] : ($p['postalcode'] ? $p['postalcode'] : ''));
		$client->countryCode = ($p['countrycode'] ? $p['countrycode'] : 'US');
		$client->casesDemoDefault = 1;
		$client->insert();
		if( !$client->ID ) {
			$this->except( $genErr, 1005 );
		}

		$cAdmin = new DB\Users();
		$cAdmin->typeID = self::USER_TYPE_CLIENT;
		$cAdmin->clientID = $client->ID;
		$cAdmin->firstName = trim( $name[0] );
		$cAdmin->lastName = (isset( $name[1] ) ? trim( $name[1] ) : '');
		$cAdmin->email = $email;
		$cAdmin->username = $email;
		$cAdmin->phone = $phone;
		$cAdmin->address1 = $client->address1;
		$cAdmin->address2 = $client->address2;
		$cAdmin->city = $client->city;
		$cAdmin->state = $client->state;
		$cAdmin->region = $client->region;
		$cAdmin->ZIP = $client->ZIP;
		$cAdmin->countryCode = $client->countryCode;
		$cAdmin->created = $now;
		$cAdmin->termsOfService = 0;
		$cAdmin->clientAdmin = 1;
		$cAdmin->affiliateCode = ($p['affiliatecode'] ? $p['affiliatecode'] : NULL);
		$cAdmin->insert();
		if( !$cAdmin->ID ) {
			$this->except( $genErr, 1006 );
		}
		$cAdmin->activated = 0; // deactivate until email validation
		$cAdmin->update();

		$demoCase = new DB\Cases();
		$demoCase->createDemoCase( $client->ID, $cAdmin->ID );
		$demoCase->insert();

		$demoDepo = new DB\Depositions();
        $demoDepo->createDemoDeposition( $demoCase, $cAdmin->ID );

		// set random password -- to be changed/reset on activation
		$pw = DB\UserAuth::generateSpice();
		DB\UserAuth::updatePassword( $cAdmin->ID, $pw );

		PageSignup::sendActivationEmail( $email );

		$countryList = MVC\Edepo::getCountries( FALSE );
		$msg = "The following info was submitted for an account:\r\n\r\n";
		$msg .= "Full Name: {$cAdmin->firstName} {$cAdmin->lastName}\r\n";
		$msg .= ($p['firm'] ? "Firm: {$p['firm']}\r\n" : '');
		$msg .= "Email: {$email}\r\n";
		$msg .= "Address: {$cAdmin->address1}\r\n";
		$msg .= ($cAdmin->address2 ? "Address: {$cAdmin->address2}\r\n" : '');
		$msg .= "City: {$cAdmin->city}\r\n";
		$msg .= ($cAdmin->countryCode == 'US' ? "State: {$cAdmin->state}\r\n" : '');
		$msg .= ($cAdmin->countryCode == 'CA' ? "Province: {$cAdmin->state}\r\n" : '');
		$msg .= ($cAdmin->countryCode == 'OT' ? "Region: {$cAdmin->region}\r\n" : '');
		$msg .= ($cAdmin->countryCode == 'US' ? "Zipcode: {$cAdmin->ZIP}\r\n" : '');
		$msg .= (in_array( $cAdmin->countryCode, ['CA','OT'] )  ? "Postal Code: {$cAdmin->ZIP}\r\n" : '');
		$msg .= "Country: {$countryList[$cAdmin->countryCode]}\r\n";
		mail( $this->config->signup['notify'], "Account signup submission: {$cAdmin->firstName} {$cAdmin->lastName}", $msg, "From: {$this->config->email['fromName']} <{$this->config->email['fromEmail']}>" );
	}
}
