<?php

namespace ClickBlocks\APITest;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Exceptions,
    ClickBlocks\Utils\DT,
    ClickBlocks\Core;

if(!defined('PHPUnit_MAIN_METHOD')){
    define('PHPUnit_MAIN_METHOD', 'DepositionTests::main');
}
require_once(__DIR__ . '/../../../connect_phpunit.php');


class DepositionTests{
    public static function main() {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
    public static function suite(){
        $ts= new \PHPUnit_Framework_TestSuite('Deposition Classes');
        $ts->addTestSuite('ClickBlocks\APITest\Deposition\getFilesTest');
        return $ts;
    }
}
if(PHPUnit_MAIN_METHOD == 'AppTests::main')
    DepositionTests::main();

?>