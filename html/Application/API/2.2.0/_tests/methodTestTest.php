<?php

namespace ClickBlocks\APITest\General;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\APITest\MethodTest,
    ClickBlocks\Exceptions,
    ClickBlocks\APITest;

/**
 * @group methodTest 
 */ 
class methodTestTest extends MethodTest
{
  public static function setUpBeforeClass() {}
  
  public function test_assertArrayContainsArray_1()
  {
    $this->assertArrayContainsArray(array('one'=>1, 'two'=>2), array('one'=>1,'two'=>2,'three'=>3,'four'=>4));
  }
  
  public function test_assertArrayContainsArray_2()
  {
    $this->setExpectedException('\PHPUnit_Framework_ExpectationFailedException');
    $this->assertArrayContainsArray(array('one'=>1, 'five'=>5), array('one'=>1,'two'=>2,'three'=>3,'four'=>4));
  }
  
  public function test_assertArrayContainsArray_3()
  {
    $this->setExpectedException('\PHPUnit_Framework_ExpectationFailedException');
    $this->assertArrayContainsArray(array('one'=>1, 'two'=>3), array('one'=>1,'two'=>2,'three'=>3,'four'=>4));
  }
}

?>
