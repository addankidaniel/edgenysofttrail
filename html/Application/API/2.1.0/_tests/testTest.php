<?php

namespace ClickBlocks\APITest\General;

use ClickBlocks\APITest,
    ClickBlocks\DB;

class SomeOtherClass
{
   public static function staticMethod($param1, $param2)
   {
      // to something
      throw new \LogicException('Method was not mocked!');
   }
}

class someClass
{
  private function privateMethod() {
    return '1';
  }
  protected function protectedMethod() {
    return '1';
  }
  public function publicMethod() {
    return '1'.$this->protectedMethod().$this->privateMethod();
  }
  public static function staticMethod() {
    return '1';
  }
  
  public function callStaticOnOther() {
     $res = SomeOtherClass::staticMethod(123, 'abc');
     if ($res != 'woot') throw new \LogicException('Unexpected value!');
  }
}

/**
 * @group  testTest
 */
class testTest extends APITest\BaseTest
{
  
  public function testSome()
  {
    $mock = $this->getMock('\ClickBlocks\APITest\General\someClass',array('protectedMethod'));
    $mock->expects(self::any())->method('protectedMethod')->will(self::returnValue('0'));
    $s = $mock->publicMethod();
    $this->assertEquals('101', $s);
    $s = $mock::staticMethod();
    $this->assertEquals('1', $s);
    /*$mockclass = $this->getMockClass('\ClickBlocks\APITest\General\SomeOtherClass', array('staticMethod'));
    $mockclass::staticExpects(self::once())->method('staticMethod')
        ->with(self::equalTo(123), self::equalTo('abc'))
        ->will(self::returnValue(123));
    $mock->callStaticOnOther();*/
  }
  
  public static function callback($method, $pr)
  {
     echo $method;
     self::assertEquals('somefile.txt', $pr[0]);
     return 'content';
  }

  public function test_file_system()
  {
     $fs = \Phake::mock('\ClickBlocks\Utils\FileSystem');
     \Phake::when($fs)->file_get_contents(\Phake::anyParameters())->thenGetReturnByLambda(__CLASS__.'::callback');
     /*$fs = $this->getMock('\ClickBlocks\Utils\FileSystem', array('file_get_contents'));
     $fs->expects(self::once())->method('file_get_contents')
         ->with(self::equalTo('somefile.txt'))
         ->will(self::returnValue('content'));*/
     $res = $fs->file_get_contents('somefile.txt');
     \Phake::verify($fs)->file_get_contents('somefile.txt');
     self::assertSame('content', $res);
  }
  
  public function test_servicebllmatcher()
  {
     
     $svc = \Phake::mock('\ClickBlocks\DB\ServiceUsers');
     $id = 12;
     \Phake::when($svc)->getByID($id)->thenReturn(new DB\QuickBLL);
     
     //code under test
     $bll = $svc->getByID($id);
     $bll->A = 1;
     $bll->B = 2;
     $bll->C = 3;
     $svc->save($bll);
     
     \Phake::verify($svc)->save(new APITest\ServiceBLLMatcher(array('A'=>1,'B'=>2)));
  }
}

?>
