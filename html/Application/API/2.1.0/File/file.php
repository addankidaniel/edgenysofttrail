<?php

namespace ClickBlocks\API\v2_1_0\Logic;

use ClickBlocks\Core,
	ClickBlocks\Utils,
	ClickBlocks\DB,
	ClickBlocks\PDFDaemon,
	ClickBlocks\PDFTools,
	ClickBlocks\EDVideos,
	ClickBlocks\EDPDFConvert,
	ClickBlocks\EDMMConvert,
	ClickBlocks\FFProbe,
	ClickBlocks\Cache\CacheRedis,
	ClickBlocks\Debug;

define( 'MAX_ATTACHMENT_SIZE', (15*1024*1024) );	// limit the size of file attachments for a single email; arbitrary value, may need to fine tune
define( 'DROP_FILE_AFTER_EMBED', true );
define( 'FORCE_SINGLE_FILE', false );

class File extends Edepo {

	public function api_download($p) {
		$this->validateParams( ['fileID' => ['type'=>self::TYPE_NUMBER, 'req'=>FALSE]] );
		$fileID = (int)$p['fileID'];
		$file = new DB\Files( $fileID );
		if( !$file || !$file->ID || $file->ID != $fileID ) {
			$this->except( 'File not found', 1001 );
		}
		if( $this->isUserMember() ) {
			// check access for Member
			$hasAccess = $this->getOrchestraFolders()->checkPermissionToFile( $this->user->ID, $file->ID );
			if( !$hasAccess ) {
				$this->except( 'Access denied!', 1002 );
			}
		} else {
			$okayToGo = FALSE;
			$folder = $file->folders[0];
			switch( $this->attendee->role ) {
				case self::DEPOSITIONATTENDEE_ROLE_GUEST:
					$okayToGo = ($folder->class == self::FOLDERS_COURTESYCOPY && $this->getOrchestraDepositionAttendees()->checkAttendeeIsDepositionAttendee( $folder->depositionID, $this->attendee->ID ));
					break;
				case self::DEPOSITIONATTENDEE_ROLE_WITNESS:
					$okayToGo = ($folder->class == self::FOLDERS_EXHIBIT && $this->getOrchestraDepositionAttendees()->checkAttendeeIsDepositionAttendee( $folder->depositionID, $this->attendee->ID ));
					break;
				case self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER:
					$okayToGo = $this->getOrchestraFolders()->checkPermissionToFolder( $this->attendee->ID, $folder->ID );
					break;
			}
			if( !$okayToGo ) {
				$this->except( 'Access denied', 1003 );
			}
		}
		if( isset( $p['embed'] ) && $p['embed'] ) {
			$this->embedDownload( $file->getFullName() );
		} else {
			$this->flushFile( $file->getFullName() );
		}
	}

	// upload file to user dir
	public function api_upload( $p ) {
		if( !$this->isUserMember() && ($this->attendee && $this->attendee->role != self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER) ) {
		   $this->except( 'Only member user access allowed!' );
		}
		$this->validateParams( [
			'depositionID' => ['type'=>self::TYPE_NUMBER, 'req'=>TRUE],
			'folderID' => ['type'=>self::TYPE_NUMBER, 'req'=>FALSE],
			'overwrite' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE],
			'file' => ['type'=>self::TYPE_FILE, 'req'=>TRUE],
			'sourceFileID' => ['type'=>self::TYPE_NUMBER, 'req'=>FALSE]
		] );
		//Debug::ErrorLog( "upload fileID: {$p['sourceFileID']}" );
		$depositionID = (int)$p['depositionID'];
		$folderID = (int)$p['folderID'];
		$attID = ($this->isUserMember()) ? $this->user->ID : $this->attendee->ID;
		$depo = new DB\Depositions( $depositionID );
		if( !$depo || !$depo->ID || $depo->ID != $depositionID ) {
			$this->except( "Session not found by ID: {$depositionID}", 1001 );
		}
		if( $folderID ) {
			$folder = new DB\Folders( $folderID );
			if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
				$this->except( "Folder not found by ID: {$folderID}", 1002 );
			}
			if( !$this->getOrchestraFolders()->checkPermissionToFolder( $attID, $folder->ID ) ) {
				$this->except( 'Access denied to folder!', 1003 );
			}
			if( $this->isUserGuest() && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER && ($folder->class != self::FOLDERS_WITNESSANNOTATIONS || $folder->createdBy != $depo->ownerID) ) {
				$this->except( 'Access denied to folder!', 1004 );
			}
		}
		if( !$folderID || $folder->class == self::FOLDERS_EXHIBIT ) {
			$folder = $this->getOrchestraFolders()->getPersonalFolderObject( $depo->ID, $attID );
			if( !$folder->ID ) {
				$this->except( "Folder not found", 1005 );
			}
		}
		$info = $_FILES['file'];
		if( !is_array( $info['tmp_name'] ) ) {
			foreach( $info as $k=>$v ) {
				$info[$k] = [$v];
			}
		}
		$this->return['files'] = [];

		//source fileID -- for Courtesy Copy
		if( !isset( $p['sourceFileID'] ) ) {
			$p['sourceFileID'] = 0;
		}
		$orchFiles = $this->getOrchestraFiles();
		$sourceFileID = $orchFiles->getSourceFileID( $p['sourceFileID'] );
		$now = date( 'Y-m-d H:i:s' );

		$fileCreatedBy = ($depo->isWitnessPrep() && $this->attendee && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER) ? $depo->createdBy : $this->user->ID;
		$byUserID = ($depo->isWitnessPrep() && $this->attendee && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER) ? $this->attendee->ID : $this->user->ID;
		$byName = ($depo->isWitnessPrep() && $this->attendee && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER) ? $depo->depositionOf : "{$this->user->firstName} {$this->user->lastName}";

		foreach( $info['tmp_name'] as $k => $v ) {
			if( $info['error'][$k] ) {
				$this->except( "File upload error: {$info['error'][$k]}" );
			}
			if( $p['overwrite'] ) {
				$this->validateParams( ['fileID'=>['type'=>self::TYPE_NUMBER, 'req'=>TRUE]] );
				$fileID = (int)$p['fileID'];
				$file = new DB\Files( $fileID );
				if( !$file || !$file->ID || $file->ID != $fileID ) {
					$this->except( "File not found by ID: {$fileID}", 1006 );
				}
				copy( $info['tmp_name'][$k], $file->getFullName() );
				unlink( $info['tmp_name'][$k] );
				$file->created = $now;
				$file->sourceID = $sourceFileID;
				$file->save();
				$this->getNodeJS()->notifyFileModified( $file->ID, $byUserID, $byName );
			} else {
				$file = new DB\Files();
				$file->name = $info['name'][$k];
				$file->folderID = $folder->ID;
				$file->created = $now;
				$file->createdBy = $fileCreatedBy;
				$file->sourceID = $sourceFileID;
				$file->isExhibit = 0;
				$file->isPrivate = 0;
				$file->isTranscript = 0;
				$file->isUpload = 0;
				$file->sourceUserID = $fileCreatedBy;
				$file->setOverwrite( $p['overwrite'] );
				$file->setSourceFile( $info['tmp_name'][$k], true );
				$file->insert();
			}
			$folder->lastModified = $now;
			$folder->update();
			$this->return['files'][] = [
				'ID' => $file->ID,
				'folderID' => $folder->ID,
				'folder' => $folder->getValues(),
				'file' => $file->getValues()
			];
		}
		$nodeJS = $this->getNodeJS();
		$nodeJS->notifyDepositionUpload( $folder->ID, $this->user->ID );
	}

	// upload witness annotations to deposition leader witness annotations folder
	public function api_upload_witness( $p )
	{
		$this->validateParams( [
				'depositionID' => ['type'=>self::TYPE_NUMBER, 'req'=>TRUE],
				'file' => ['type'=>self::TYPE_FILE, 'req'=>TRUE],
				'sourceFileID' => ['type'=>self::TYPE_NUMBER, 'req'=>FALSE]
		] );

		if( !$this->isUserGuest() ) {
			$this->except( 'Only for guest users!' );
		}

		$depositionID = (int)$p['depositionID'];

		$mainDeposition = new DB\Depositions( $depositionID );
		if ( !$mainDeposition->ID ) {
			$this->except( "Session not found by ID: {$depositionID}", 404 );
		}

		$speakerID = ($mainDeposition->speakerID) ? $mainDeposition->speakerID : $mainDeposition->ownerID;
		if( $mainDeposition->isWitnessPrep() && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER ) {
			$speakerID = $mainDeposition->ownerID;
		}

		$isChildDepo = !($this->getOrchestraDepositionAttendees()->checkUserAttendedDepo( $depositionID, $speakerID ));
		if( $isChildDepo ) {
			$depositionID = $this->getOrchestraDepositions()->getUserChildDeposition( $speakerID, $depositionID );
			$childDeposition = new DB\Depositions( $depositionID );
		}

		$folder = $this->getWitnessAnnotationsFolder( $depositionID, $speakerID );

		$uploads = $_FILES['file'];
		if ( !is_array( $uploads['tmp_name'] ) ) {
		   foreach ($uploads as $k => $v) {
			   $uploads[$k] = array( $v );
		   }
		}

		$this->return['files'] = array();

		//source fileID -- for Courtesy Copy
		if( !isset( $p['sourceFileID'] ) ) {
			$p['sourceFileID'] = 0;
		}
		$orchFiles = $this->getOrchestraFiles();
		$sourceFileID = $orchFiles->getSourceFileID( $p['sourceFileID'] );

		foreach( $uploads['tmp_name'] as $k => $v ) {
			if( $uploads['error'][$k] != 0 ) {
				$this->except( 'Incorrect file upload, error: ' . $uploads['error'][$k] );
			}
			$file = $this->getBLL('Files');

			$fileParts = pathinfo( $uploads['name'][$k] );
			$file->name = "{$fileParts['filename']}_WA.{$fileParts['extension']}";
			$file->folderID = $folder->ID;
			$file->createdBy = $speakerID;
			$file->sourceUserID = $speakerID;
			$file->sourceID = $sourceFileID;
			$file->isPrivate = ($folder->class == 'Personal' || $folder->class == 'WitnessAnnotations');
			$file->setSourceFile( $uploads['tmp_name'][$k], true );
			$file->insert();
			$this->return['folderID'] = $folder->ID; // for backward compatibility
			$this->return['fileID'] = $file->ID;     // for backward compatibility
			$this->return['files'][] = ['ID'=>$file->ID, 'folderID'=>$folder->ID];
			// Debug::ErrorLog( "File: {$file->ID} Name: '{$file->name}'" );
		}

		$nodeJS = $this->getNodeJS();
		$nodeJS->sendPostCommand( 'witness_uploaded', null, [
			'depositionID' => $mainDeposition->ID,
			'targetDepositionID' => ($isChildDepo && $childDeposition->ID) ? $childDeposition->ID : $mainDeposition->ID,
			'notifyUsers' => [$speakerID],
			'folderID' => $folder->ID,
			'folderName' => $folder->name
		] );
		if( $mainDeposition->isWitnessPrep() && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER ) {
			$nodeJS->notifyDepositionUpload( $folder->ID, $this->attendee->ID );
		}
	}

	/**
	 *
	 * @param bigint $depositionID
	 * @param bigint $speakerID
	 * @return \ClickBlocks\DB\Folders
	 */
	protected function getWitnessAnnotationsFolder( $depositionID, $speakerID )
	{
//		Debug::ErrorLog( "depositionID: {$depositionID} SpeakerID: {$speakerID}" );
		$isChildDepo = !($this->getOrchestraDepositionAttendees()->checkUserAttendedDepo( $depositionID, $speakerID ));
//		Debug::ErrorLog( "isChildDepo: {$isChildDepo}" );
		if( $isChildDepo ) {
			$depositionID = $this->getOrchestraDepositions()->getUserChildDeposition( $speakerID, $depositionID );
		}
//		Debug::ErrorLog( "depositionID: {$depositionID} SpeakerID: {$speakerID}" );

		$folder = $this->getOrchestraFolders()->getWitnessAnnotationsFolder( $depositionID, $speakerID );
		if (!$folder || !$folder->ID)
		{
			$folder = new DB\Folders();
			$folder->name = $this->config->logic['witnessAnnotationsFolderName'];
			$folder->class = 'WitnessAnnotations';
			$folder->createdBy = $speakerID;
			$folder->depositionID = $depositionID;
			$folder->created = date( 'Y-m-d H:i:s' );
			$folder->lastModified = $folder->created;
//			$folder->isPrivate = 1;
			$folder->insert();
		}
		if( !$folder->ID ) {
			$this->except( "Unable to get Folder for UserID: {$speakerID}", 404 );
		}
		//Debug::ErrorLog( "Deposition: {$deposition->ID} SpeakerID: {$speakerID} Folder: {$folder->ID} '{$folder->name}'" );
		return $folder;
	}

	/**
	 * Future proofing the web application
	 */
	public function api_uploadWitness( $p )
	{
		$this->api_upload_witness( $p );
	}

	public function api_saveFolder( $p ) {
		$this->validateParams( ['folderID'] );
		$folderID = (int)$p['folderID'];
		$userID = $this->isUserMember() ? $this->user->ID : $this->attendee->ID;
		if( !$this->getOrchestraFolders()->checkPermissionToFolder( $userID, $folderID ) ) {
			$this->except( 'Folder not found.', 404 );
		}
		$Folder = new DB\Folders( $folderID );
		$fileName = $Folder->name .'_' . time() . '.zip';
		$filePath = Core\IO::dir( 'temp' ) . '/' . $fileName;
		$zip = new \ZipArchive();
		$zip->open( $filePath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );
		foreach( $Folder->files as $file ) {
			if( !$file->ID ) {
				continue;
			}
			$zip->addFile( $file->getFullName(), $file->name );
		}
		$zip->close();
		$this->embedDownload( $filePath, DROP_FILE_AFTER_EMBED );
	}

	public function api_offlineUpload( $p )
	{
		if( !$this->isUserMember() ) $this->except( 'Only member user access allowed!', 403 );
		$this->validateParams( [
			'depositionID' => ['type' => self::TYPE_NUMBER, 'req' => TRUE],
			'metadata' => ['type'=> self::TYPE_ARRAY, 'req' => TRUE],
			'file' => ['type' => self::TYPE_FILE, 'req' => TRUE]
		] );

		$this->return['errors'] = [];
		if( !is_array( $p['metadata'] ) ) {
			$p['metadata'] = [$p['metadata']];
		}

		$depoID = (int)$p['depositionID'];
		//check user permissions to given deposition (creator, owner, case manager, deposition assistant, attendee)
		$deposition = $this->getOrchestraDepositions()->getDepositionForUserByID( $this->user->ID, $depoID );
		if( !$deposition ) {
			//check for child deposition
			$depoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $p['depositionID'] );
		}
		if( !$depoID ) $this->except( "Deposition not found by ID: {$p['depositionID']}", 404 );

		foreach( $p['metadata'] as $idx => $fileInfo ) {
			//Debug::ErrorLog( print_r( $fileInfo, TRUE ) );
			$folder = FALSE;
			$folderID = $this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $fileInfo['folderID'] );
			if( $folderID ) {
				$folder = $this->getService( 'Folders' )->getByID( $folderID );
				//Debug::ErrorLog( print_r( $folder->getValues(), TRUE ) );
			}
			if( !$folder || !$folder->ID || $depoID !== (int)$folder->depositionID ) {
				$this->return['errors'][] = ['metadata'=>$fileInfo, 'file'=>"{$idx}", 'errno'=>501];
				continue;
			}
			if( $folder->class == 'Exhibit' || $folder->class == 'CourtesyCopy' ) {
				$this->except( 'Access denied to protected folder', 403 );
			}

			$uploadInfo = FALSE;
			$f =& $_FILES['file'];
			foreach( $f['name'] as $i => $value ) {
				if( (int)$value === (int)$idx ) {
					$uploadInfo = ['name'=>$f['name'][$i], 'type'=>$f['type'][$i], 'tmp_name'=>$f['tmp_name'][$i], 'error'=>$f['error'][$i], 'size'=>$f['size'][$i]];
					break;
				}
			}
			if( $uploadInfo && $uploadInfo['error'] !== UPLOAD_ERR_OK ) {
				$this->return['errors'][] = ['metadata'=>$fileInfo, 'file'=>$uploadInfo['name'], 'errno'=>$uploadInfo['error']];
				continue;
			}
			//Debug::ErrorLog( print_r( $uploadInfo, TRUE ) );

			$fileID = (int)$fileInfo['fileID'];
			if( $fileID > 0 ) {
				//overwrite
				$file = $this->getService( 'Files' )->getByID( $fileID );
				if( (int)$file->folderID !== (int)$folderID ) {
					//Debug::ErrorLog( print_r( $file->getValues(), TRUE ) );
					$this->return['errors'][] = ['metadata'=>$fileInfo, 'file'=>$uploadInfo['name'], 'errno'=>501];
					continue;
				}
				if( $file && $file->ID ) {
					copy( $uploadInfo['tmp_name'], $file->getFullName() );
					unlink( $uploadInfo['tmp_name'] );
					if( $fileInfo['fileName'] !== $file->name ) {
						$dir = dirname( $file->getFullName() );
						rename2( $file->getFullName(), "{$dir}/{$fileInfo['fileName']}" );
						$file->name = $fileInfo['fileName'];
					}
					$file->created = $fileInfo['created'];
					$sourceFileID = (int)$fileInfo['sourceFileID'];
					$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $sourceFileID );
					if( $sourceFileID !== $file->ID ) $file->sourceID = $sourceFileID;
					//Debug::ErrorLog( 'Saving file: ' . print_r( $file->getValues(), TRUE ) );
					$file->save();
					$this->getNodeJS()->notifyFileModified( $file->ID, $this->user->ID, "{$this->user->firstName} {$this->user->lastName}" );
					$this->return['files'][] = ['ID'=>$file->ID, 'folderID'=>$file->folderID, 'fileName'=>$file->name, 'metadata'=>$fileInfo];
				} else {
					//create
					$fileID = -1;
				}
			}
			if( $fileID < 0 ) {
				//create
				$file = $this->getBLL( 'Files' );
				$file->folderID = $folderID;
				$file->createdBy = $folder->createdBy;
				$file->name = $fileInfo['fileName'];
				$file->created = $fileInfo['created'];
				if( $sourceFileID !== $file->ID ) $file->sourceID = $sourceFileID;
				$file->setSourceFile( $uploadInfo['tmp_name'], TRUE );
				//Debug::ErrorLog( 'Creating file: ' . print_r( $file->getValues(), TRUE ) );
				$file->insert();
				$this->return['files'][] = ['ID'=>$file->ID, 'folderID'=>$file->folderID, 'fileName'=>$file->name, 'metadata'=>$fileInfo];
			}
		}
	}

	public function api_sendFolder( $p )
	{
		$this->validateParams( [
			'folderID' => ['type'=>self::TYPE_SCALAR, 'req'=>true],
			'selectedFileIDs' => ['type'=>self::TYPE_ARRAY, 'req'=>FALSE],
			'sendTo' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendCc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendBcc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendSubject' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendContents' => ['type'=>self::TYPE_STRING, 'req'=>false],
		] );

		// initialize the phpMailer helper
		$mailer = $this->prepareMailer( $p );

		$filesToAttach = [];

		// Numbers to calculate approximate file size after base64 encoding
		$base64Headers = 814;
		$base64EncodedSizeIncrease = 1.37;

		// for the folderID, get a list of files and add as attachments to the headers
		$Folder = $this->getService( 'Folders' )->getByID( $p['folderID'] );
		$pathToFolder = $Folder->getFullPath();
		foreach( $this->getOrchestraFolders()->getFilesForUser( $Folder->ID, $this->user->ID ) as $file ) {
			$File = new DB\Files( $file['ID'] );
			if( !$File || !$File->ID || $File->ID != $file['ID'] ) {
				continue;
			}
			if( isset( $p['selectedFileIDs'] ) && is_array( $p['selectedFileIDs'] ) && $p['selectedFileIDs'] ) {
				if( !in_array( $File->ID, $p['selectedFileIDs'] ) ) {
					continue;
				}
			}
			$fullPath = $pathToFolder.DIRECTORY_SEPARATOR.$File->name;
			$fileSize = filesize( $fullPath );
			$filesToAttach[] = (object)[
				'path' => $fullPath,
				'size' => ( $fileSize * $base64EncodedSizeIncrease ) + $base64Headers,
			];
		}

		try {
			// limit number of attachments by file size, if greater send multiple emails until all attachments have been sent.
			$packet_count = 0;
			while (($toAttach = $this->prepareAttachment( $filesToAttach, ($p['sendSubject'].'_'.(++$packet_count)) )) !== null)
			{
				$mailer->addAttachment( $toAttach );
				$sendStatus = $this->mailSend( $mailer );

				unlink( $toAttach );
				$mailer->ClearAttachments();

				if (!$sendStatus)
				{
					throw new Exception( 'An error occured attempting to send folder' );
				}
			}
			$this->return['status'] = true;
		} catch (Exception $e) {
			$this->return['status'] = false;
			$this->return['error'] = $e->getMessage();
		}

		unset( $mailer );
	}

	public function api_sendFile( $p )
	{
		$this->validateParams( [
			'fileID' => ['type'=>self::TYPE_SCALAR, 'req'=>true],
			'sendTo' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendCc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendBcc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendSubject' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendContents' => ['type'=>self::TYPE_STRING, 'req'=>false],
		] );

		// initialize the phpMailer helper
		$mailer = $this->prepareMailer( $p );

		// Numbers to calculate approximate file size after base64 encoding
		$base64Headers = 814;
		$base64EncodedSizeIncrease = 1.37;

		// add file as attachment to the headers
		$File = $this->getService( 'Files' )->getByID( $p['fileID'] );
		$filename = $File->getFullName();
		$fileSize = filesize( $filename );
		$approxEncodedFileSize = ($fileSize * $base64EncodedSizeIncrease) + $base64Headers;
		if ($approxEncodedFileSize > $this->config->email['maxSize']) {
			$this->except( "Email is too large", 501 );
		}
		$mailer->addAttachment( $filename, substr( $filename, strrpos( $filename, DIRECTORY_SEPARATOR )+1 ) );

		$mailStatus = $this->mailSend( $mailer );

		$this->return['status'] = $mailStatus;
		if (!$mailStatus)
		{
			$this->return['error'] = 'An error occurred attempting to send file.';
		}

		unset( $mailer );
	}

	public function api_annotate( $p )
	{
		$this->validateParams( [
			'depositionID' => ['type' => self::TYPE_SCALAR, 'req' => TRUE],
			'sourceFileID' => ['type' => self::TYPE_SCALAR, 'req' => TRUE],
			'annotations' => ['type' => self::TYPE_STRING, 'req' => TRUE],
			'overwrite' => ['type' => self::TYPE_BOOLEAN, 'req' => FALSE],
			'iswitness' => ['type' => self::TYPE_BOOLEAN, 'req' => FALSE]
		] );

		$depositionID = (int)$p['depositionID'];
		$sourceFileID = (int)$p['sourceFileID'];
		$annotations = json_encode( json_decode( $p['annotations'] ), JSON_UNESCAPED_SLASHES );
		$overwrite = (bool)$p['overwrite'];
		$isWitness = (bool)$p['iswitness'];

		Debug::ErrorLog( "API Annotate: depoID: {$depositionID}" );

		$deposition = new DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
			$this->except( "Session not found: {$depositionID}", 1001 );
		}

		//source file
		$inSrcFile = new DB\Files( $sourceFileID );
		if( !$inSrcFile || !$inSrcFile->ID || $inSrcFile->ID != $sourceFileID ) {
			$this->except( "Source file not found: {$sourceFileID}", 1002 );
		}

		if( $isWitness ) {
			$this->annotateFileSaveTemp( $annotations, $inSrcFile->ID, $deposition->ID );
		} else {
			$attID = ($this->isUserGuest()) ? $this->attendee->ID : $this->user->ID;
			if( !$this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $depositionID ) ) {
				$depoAtt = $this->getOrchestraDepositionAttendees()->getAttendeeForDeposition( $depositionID, $attID );
				if( !$depoAtt || !$depoAtt->ID ) {
					$this->except( 'Unauthorized', 1003 );
				}
			}
			if( !$this->getOrchestraFolders()->checkPermissionToFile( $attID, $inSrcFile->ID ) ) {
				$this->except( 'Unauthorized', 1004 );
			}

			$folderID = 0;
			if( $overwrite ) {
				$this->validateParams( ['fileID' => ['type' => self::TYPE_SCALAR, 'req' => TRUE]] );
				$fileID = (int)$p['fileID'];
				$this->annotateFileOverwrite( $annotations, $inSrcFile->ID, $fileID );
				$folderID = (int)$inSrcFile->folderID;
			} else {
				$this->validateParams( [
					'fileName' => ['type' => self::TYPE_STRING, 'req' => TRUE],
					'folderID' => ['type' => self::TYPE_SCALAR, 'req' => TRUE]
				] );
				$folderID = (int)$p['folderID'];
				$this->annotateFileSave( $annotations, $inSrcFile->ID, $p['fileName'], $folderID );
			}
			if( $folderID ) {
				$nodeJS = $this->getNodeJS();
				$nodeJS->notifyDepositionUpload( $folderID, $this->user->ID );
			}
		}
	}

	public function embedDownload( $file, $purgeAfterEmbed=0, $filename=NULL )
	{
		$contentType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $file );
		if( !$filename ) {
			$filename = substr( $file, strrpos( $file, '/' )+1 );
		}
		$this->formalOutput = false;
		ob_end_clean();
		ob_start();
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private', FALSE );
		header( 'Content-Disposition: attachment; filename="'.$filename.'"' );
		header( 'Content-Encoding: binary' );
		header( 'Content-Transfer-Encoding: binary' );
		header( 'Content-Type: ' . $contentType );
		header( 'Expires: 0' );
		header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
//		if( $contentType == 'application/pdf' ) {
//			header( 'Content-Length: ' . (int)filesize( $file ) );
//			readfile( $file );
//		} else {
			EDVideos::rangeDownload( $file );
//		}
		ob_end_flush();
		if( $purgeAfterEmbed === DROP_FILE_AFTER_EMBED ) {
			unlink( $file );
		}
		exit;
	}

	protected function annotateFileOverwrite( $annotations, $sourceFileID, $overwriteFileID )
	{
		$inFile = new DB\Files( $sourceFileID );

		$fileSourceID = $this->getOrchestraFiles()->getSourceFileID( $inFile->ID );

		$outFile = new DB\Files( $overwriteFileID );
		if( !$outFile->ID ) {
			$this->except( "File not found: $overwriteFileID", 404 );
		}

		$folder = new DB\Folders( $outFile->folderID );
		$attID = ($this->isUserMember()) ? $this->user->ID : $this->attendee->ID;
		$fileAccessToFolder = (int)$this->getOrchestraFolders()->checkPermissionToFolder( $attID, $outFile->folderID );
		if( !$folder->ID || !$fileAccessToFolder || $fileAccessToFolder !== (int)$outFile->folderID ) {
			$this->except( 'Unauthorized', 403 );
		}

		$inPath = $inFile->getFullName();
		$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.json';
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';

		$jsonString = json_encode( json_decode( $annotations ), JSON_UNESCAPED_SLASHES );
		if( !$jsonString ) {
			$this->except( 'Invalid annotations metadata', 501 );
		}
		$metaSuccess = file_put_contents( $metadataPath, $jsonString, 0 );
		if( !$metaSuccess ) {
			$this->except( 'Unable to write annotations metadata', 501 );
		}

		if( finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $inPath ) === 'application/pdf' ) {
			if( !PDFDaemon::annotate( $inPath, $outPath, $metadataPath ) ) {
				Debug::ErrorLog( "api_annotate (overwrite); failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
				if( file_exists( $metadataPath ) ) {
					unlink( $metadataPath );
				}
				$this->except( 'Unable to perform annotations', 501 );
			}

			Debug::ErrorLog( "api_annotate (overwrite); success" );
		} else {
			//fake it for multimedia
			copy( $inPath, $outPath );
		}

		if( file_exists( $metadataPath ) ) {
			unlink( $metadataPath );
		}
		if( copy( $outPath, $outFile->getFullName() ) ) {
			unlink( $outPath );
		}
		$now = date( 'Y-m-d H:i:s' );
		$outFile->created = $now;
		$outFile->sourceID = $fileSourceID;
		$outFile->update();
		$this->return['fileID'] = $outFile->ID;
		$this->getNodeJS()->notifyFileModified( $outFile->ID, $this->user->ID, "{$this->user->firstName} {$this->user->lastName}" );

		$folder->lastModified = $now;
		$folder->update();
	}

	protected function annotateFileSave( $annotations, $sourceFileID, $fileName, $folderID )
	{
		$inFile = new DB\Files( $sourceFileID );

		$fileSourceID = $this->getOrchestraFiles()->getSourceFileID( $inFile->ID );

		//permissions to destination folder
		$folder = new DB\Folders( $folderID );
		$attID = ($this->isUserMember()) ? $this->user->ID : $this->attendee->ID;
		$fileAccessToFolder = (int)$this->getOrchestraFolders()->checkPermissionToFolder( $attID, $folderID );
		if( !$folder->ID || !$fileAccessToFolder || $fileAccessToFolder !== (int)$folderID ) {
			$this->except( 'Unauthorized', 403 );
		}

		$inPath = $inFile->getFullName();
		$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.json';
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';

		$jsonString = json_encode( json_decode( $annotations ), JSON_UNESCAPED_SLASHES );
		if( !$jsonString ) {
			$this->except( 'Invalid annotations metadata', 501 );
		}
		$metaSuccess = file_put_contents( $metadataPath, $jsonString, 0 );
		if( !$metaSuccess ) {
			$this->except( 'Unable to write annotations metadata', 501 );
		}

		if( finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $inPath ) === 'application/pdf' ) {
			if( !PDFDaemon::annotate( $inPath, $outPath, $metadataPath ) ) {
				Debug::ErrorLog( "PDFDaemon::annotate (save); failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
				if( file_exists( $metadataPath ) ) {
					unlink( $metadataPath );
				}
				$this->except( 'Unable to perform annotations', 501 );
			}
			Debug::ErrorLog( "PDFDaemon::annotate (save); success" );
		} else {
			//fake it for multimedia
			copy( $inPath, $outPath );
		}

		if( file_exists( $metadataPath ) ) {
			unlink( $metadataPath );
		}
		$now = date( 'Y-m-d H:i:s' );
		$createdBy = ($this->isUserMember()) ? $this->user->ID : $folder->createdBy;
		$newFile = new DB\Files();
		$newFile->folderID = $folder->ID;
		$newFile->createdBy = $createdBy;
		$newFile->name = $fileName;
		$newFile->created = $now;
		$newFile->sourceID = $fileSourceID;
		$newFile->setSourceFile( $outPath, FALSE );
		$newFile->insert();
		$this->return['fileID'] = $newFile->ID;

		$folder->lastModified = $now;
		$folder->update();
	}

	protected function annotateFileSaveTemp( $annotations, $sourceFileID, $depositionID )
	{
		$originalFile = new DB\Files( $sourceFileID );
		$originalFileName = $originalFile->getFullName();

		// file sourceID - for Courtesy copy
		$fileSourceID = $this->getOrchestraFiles()->getSourceFileID( $originalFile->ID );

		$originalFilePath = $originalFileName;
		$tempFilePath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';

		// prepare a temp file to store the annotations prior to flattening
		$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.json';
		$jsonString = json_encode( json_decode( $annotations ), JSON_UNESCAPED_SLASHES );
		if( !$jsonString ) {
			$this->except( 'Invalid annotations metadata', 501 );
		}
		if (!file_put_contents( $metadataPath, $jsonString, 0 ))
		{
			$this->except( 'Unable to write annotations metadata', 501 );
		}

		// get the Witness Annotations folder for the Deposition Leader
		$deposition = $this->getService( 'Depositions' )->getByID( $depositionID );
		$speakerID = ($deposition->speakerID) ? $deposition->speakerID : $deposition->ownerID;
		$folder = $this->getWitnessAnnotationsFolder( $deposition->ID, $speakerID );

		$result = PDFDaemon::annotate( $originalFilePath, $tempFilePath, $metadataPath );

		// destroy the temp annotations file
		if (file_exists( $metadataPath )) {
			unlink( $metadataPath );
		}

		if (!$result) {
			Debug::ErrorLog( "api_annotate (temp); failure" );
			if (file_exists( $tempFilePath ))
			{
				unlink( $tempFilePath );
			}
			$this->except( 'Unable to perform annotations', 501 );
		}
		Debug::ErrorLog( "api_annotate (temp); success" );

		// insert the file into the Witness Annotations folder
		$fileParts = pathinfo( $originalFileName );

		$file = $this->getBLL('Files');
		$file->name = "{$fileParts['filename']}_WA.{$fileParts['extension']}";
		$file->folderID = $folder->ID;
		$file->createdBy = $speakerID;
		$file->sourceID = $fileSourceID;
		$file->setSourceFile( $tempFilePath, true );
		$file->insert();

		$this->return['folderID'] = $folder->ID;
		$this->return['fileID'] = $file->ID;

		$isChildDepo = !($this->getOrchestraDepositionAttendees()->checkUserAttendedDepo( $depositionID, $speakerID ));
		if( $isChildDepo ) {
			$depositionID = $this->getOrchestraDepositions()->getUserChildDeposition( $speakerID, $depositionID );
			$childDeposition = new DB\Depositions( $depositionID );
		}

		$nodeJS = $this->getNodeJS();
		$nodeJS->sendPostCommand( 'witness_uploaded', null, [
			'depositionID' => $deposition->ID,
			'targetDepositionID' => ($isChildDepo && $childDeposition->ID) ? $childDeposition->ID : $depositionID,
			'notifyUsers' => [$speakerID],
			'folderID' => $folder->ID,
			'folderName' => $folder->name
		] );
		if( $deposition->isWitnessPrep() && $this->attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER ) {
			$nodeJS->notifyDepositionUpload( $folder->ID, $this->attendee->ID );
		}
	}

	protected function mailSend( $phpmail )
	{
		$sent = $phpmail->Send();
		if (!$sent) {
			$this->except( "Email failed to send.", 501 );
		}
		return true;
	}

	protected function prepareMailer( $p )
	{
		$mailer = new Utils\Mailer();

		// set subject, content, and sender
		$mailer->Subject = $p['sendSubject'];
		$mailer->Body = (isset( $p['sendContents'] ) && strlen( $p['sendContents'] ) > 0 ? $p['sendContents'] : " ");
		if (isset( $this->attendee ))
		{
			$mailer->SetFrom( $this->attendee->email, $this->attendee->name );
			$mailer->ClearReplyTos();
			$mailer->AddReplyTo( $this->attendee->email );
		} else
		if (isset( $this->user ))
		{
			$mailer->SetFrom( $this->user->email, "{$this->user->firstName} {$this->user->lastName}" );
			$mailer->ClearReplyTos();
			$mailer->AddReplyTo( $this->user->email );
		}

		// set addresses, separate addresses by newline, comma or semi-colon
		foreach (preg_split( "/[\n\r;,]+/", $p['sendTo'] ) as $to)
		{
			$mailer->AddAddress( $to );
		}

		if ($p['sendCc'] && strlen( $p['sendCc'] ) > 0)
		{
			foreach (preg_split( "/[\n\r;,]+/", $p['sendCc'] ) as $cc)
			{
				$mailer->AddCC( $cc );
			}
		}

		if ($p['sendBcc'] && strlen( $p['sendBcc'] ) > 0)
		{
			foreach (preg_split( "/[\n\r;,]+/", $p['sendBcc'] ) as $bcc)
			{
				$mailer->AddBCC( $bcc );
			}
		}

		return $mailer;
	}

	/**
	 * Given a list of files with properties path and size, prepare a "packet" of files with size less than MAX_ATTACHMENT_SIZE.
	 * Archive the files with standard zip and return the path to the zip file.
	 *
	 * prepareAttachment() is destructive to input array
	 *
	 * @param array $files
	 * @param string $archiveName
	 * @return string Path to archive of file "packet"
	 */
	protected function prepareAttachment( &$files, $archiveName='', $splitFiles=true  )
	{
		if (!is_array( $files ) || count( $files ) < 1)
		{
			return null;
		}

		if (strlen( $archiveName ) < 1)
		{
			$archiveName = 'archive_'.date( 'YmdHis' );
		}

		// test opening the zip archive before we tear anything up
		$archivePath = CORE\IO::dir( 'temp' ).DIRECTORY_SEPARATOR.$archiveName.'.zip';
		if(file_exists($archivePath) ) {
			unlink( $archivePath );
		}
		$archive = new \ZipArchive();
		if (($result = $archive->open( $archivePath, \ZipArchive::CREATE )) !== true)
		{
			throw new Exception( $result );
		}

		$accrual = 0;

		do {
			if ($splitFiles !== FORCE_SINGLE_FILE && $accrual > 0 && $files[0]->size+$accrual > MAX_ATTACHMENT_SIZE) { // $this->config->email['maxSize']
				break;
			}
			$file = array_shift( $files );
			if ($archive->addFile( $file->path, substr( $file->path, strrpos( $file->path, DIRECTORY_SEPARATOR )+1 ) ) === false) {
				$this->api_log( "unknown error occurred adding {$file->path} to archive $archiveName", true );
			}
			$accrual += $file->size;
		} while(count( $files ) > 0);

		// save the archive and close for writing
		$archive->close();
		unset( $archive );
		return $archivePath;
	}

	public function api_sendFileWithAnnotations( $p )
	{
		$this->validateParams( [
			'sendTo' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendCc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendBcc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendSubject' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendContents' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'depositionID' => ['type'=>self::TYPE_SCALAR, 'req'=>true],
			'sourceFileID' => ['type'=>self::TYPE_SCALAR, 'req'=>true],
			'annotations' => ['type'=>self::TYPE_STRING, 'req'=>true]
		] );

		//attendee must be Guest
		//if( !$this->attendee || $this->attendee->role != 'G' ) {
		//	$this->except( 'Method only available to Guests', 501 );
		//}

		$deposition = new DB\Depositions( $p['depositionID'] );
		if( !$deposition->ID ) {
			$this->except( "Session not found by ID: {$p['depositionID']}", 404 );
		} elseif( $this->isUserGuest() && $this->attendee->depositionID != $deposition->ID ) {
			$this->except( "Invalid session ID: {$p['depositionID']}", 501 );
		}
		$attendeeID = ($this->isUserMember() ? $this->user->ID : $this->attendee->ID);
		$this->getOrchestraDepositionAttendees()->checkAttendeeIsDepositionAttendee( $p['depositionID'], $attendeeID );

		//source file
		$inSrcFile = new DB\Files( $p['sourceFileID'] );
		if( !$inSrcFile->ID ) {
			$this->except( "File not found: {$p['sourceFileID']}", 404 );
		}
		$hasAccess = $this->isUserGuest() ? $inSrcFile->checkGuestAccess( $this->attendee ) : true;
		if( !$hasAccess ) {
			$this->except( "File not found: {$p['sourceFileID']}", 501 );
		}

		$inPath = $inSrcFile->getFullName();
		$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.json';
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';

		$jsonString = json_encode( json_decode( $p['annotations'] ), JSON_UNESCAPED_SLASHES );
		if( !$jsonString ) {
			$this->except( 'Invalid annotations metadata', 501 );
		}
		$metaSuccess = file_put_contents( $metadataPath, $jsonString, 0 );
		if( !$metaSuccess ) {
			$this->except( 'Unable to write annotations metadata', 501 );
		}

		if( !PDFDaemon::annotate( $inPath, $outPath, $metadataPath ) ) {
			Debug::ErrorLog( 'api_sendFileWithAnnotations (annotate); failure' );
			if( file_exists( $outPath ) ) {
				unlink( $outPath );
			}
			if( file_exists( $metadataPath ) ) {
				unlink( $metadataPath );
			}
			$this->except( 'Unable to perform annotations', 501 );
		}

		Debug::ErrorLog( "api_sendFileWithAnnotations (annotate); success" );
		if( file_exists( $metadataPath ) ) {
			unlink( $metadataPath );
		}

		// initialize the phpMailer helper
		$mailer = $this->prepareMailer( $p );
		$mailer->addAttachment( $outPath, $inSrcFile->name );
		$mailStatus = $this->mailSend( $mailer );

		$this->return['status'] = $mailStatus;
		if( !$mailStatus ) {
			$this->return['error'] = 'An error occurred attempting to send the file.';
		}
		unset( $mailer );
		if( file_exists( $outPath ) ) {
			unlink( $outPath );
		}
	}

	public function api_abortTempFile( $p )
	{
		$this->validateParams( [
			'depositionID' => ['type' => self::TYPE_SCALAR, 'req' => TRUE],
			'fileID' => ['type' => self::TYPE_SCALAR, 'req' => TRUE]
		] );

		$depositionID = (int)$p['depositionID'];
		$fileID = (int)$p['fileID'];

		$isAttendee = $this->getOrchestraDepositionAttendees()->checkUserIsDepositionAttendee( $depositionID, $this->user->ID );
		if( !$isAttendee ) {
			$this->except( 'Access denied (User is not attendee)', 501 );
		}

		$tmpFile = new DB\Files( $fileID );
		if( intval( $tmpFile->ID ) === $fileID ) {
			$folder = $tmpFile->folders[0];
			if( intval( $folder->depositionID ) === $depositionID && $folder->class == 'Personal' && $folder->createdBy === $this->user->ID && strpos( $tmpFile->name, 'tmp_' ) === 0 ) {
				Debug::ErrorLog( 'api_abortTempFile -- deleting temp file: ' . $tmpFile->name );
				$tmpFile->delete();
			} else {
				$this->except( 'File not found', 502 );
			}
		} else {
			$this->except( 'File not found', 503 );
		}
	}

	public function api_getPresentationSource( $p )
	{
		$this->validateParams( [
			'depositionID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'embed' => [self::TYPE_BOOLEAN, 'req'=>FALSE]
		] );

		$depositionID = (int)$p['depositionID'];
		$embed = (bool)$p['embed'];

		$attendeeID = ($this->attendee && $this->attendee->ID) ? $this->attendee->ID : $this->user->ID;

		if( !$this->getOrchestraDepositionAttendees()->checkAttendeeIsDepositionAttendee( $depositionID, $attendeeID ) ) {
			$this->except( 'Access denied (User is not attendee)', 1001 );
		}

		$redis = new CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
		$pKey = 'presentation:' . $depositionID;
		$pInfo = $redis->getJSON( $pKey );
		if( !$pInfo ) {
			$this->except( 'Unable to get presentation info', 1002 );
		}

		$file = new DB\Files( $pInfo->fileID );
		if( !$file || !$file->ID || $file->ID != $pInfo->fileID ) {
			$this->except( 'Invalid File ID', 1003 );
		}

		//ED-1458; Flatten Presentation Source
//		$fileHash = md5_file( $file->getFullName() );
//		$hash = sha1( "{$depositionID}::{$file->ID}::{$fileHash}" );
		$inPath = realpath( $file->getFullName() );
		$pFilePath =  $inPath;
//		$hashPath = realpath( $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) ) . DIRECTORY_SEPARATOR . $hash;
//		$tmpPath = $hashPath . '.tmp';
//		$pFilePath = $hashPath . '.pdf';
//		$lockPath = $hashPath . '.lock';
//		if( !file_exists( $pFilePath ) && !file_exists( $lockPath ) ) {
//			touch( $lockPath );
//			$fp = fopen( $lockPath, 'r+' );
//			//attempt exclusive lock
//			if( flock( $fp, LOCK_EX | LOCK_NB ) ) {
//				if( PDFTools::flatten( $inPath, $tmpPath ) ) {
//					Debug::ErrorLog( "File::getPresentationSource; Flatten Source -- success" );
//					rename2( $tmpPath, $pFilePath );
//				} else {
//					Debug::ErrorLog( "File::getPresentationSource; Flatten Source -- failure" );
//					$pFilePath = $inPath;	//attempt to continue -- existing annotations may cause havok
//				}
//				flock( $fp, LOCK_UN );
//			}
//		} else {
//			for( $i=0; $i<50; ++$i ) {
//				usleep( 400000 );
//				if( file_exists( $pFilePath ) ) {
//					break;
//				}
//			}
//		}
		if( !file_exists( $pFilePath ) ) {
			$this->except( "Unable to get source", 1005 );
		}

		if( $embed ) {
			$this->embedDownload( $pFilePath, FALSE, $file->name );
		} else {
			$this->flushFile( $pFilePath, $file->name );
		}
	}

	public function api_addFile( $p )
	{
		$this->validateParams( [
			'depositionID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'folderID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'file' => [self::TYPE_FILE, 'req'=>TRUE],
			'fileName' => [self::TYPE_STRING, 'req'=>TRUE]
		] );

		$depositionID = (int)$p['depositionID'];
		$folderID = (int)$p['folderID'];
		$fileName = $p['fileName'];
		$file = $_FILES['file'];
		$now = date( 'Y-m-d H:i:s' );

		//access to deposition
		$attendedDepoID = $this->getOrchestraDepositionAttendees()->checkUserIsDepositionAttendee( $depositionID, $this->user->ID );
		if( $attendedDepoID != $depositionID ) {
			$this->except( 'Deposition not found', 501 );
		}
		$deposition = new DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
			$this->except( 'Deposition not found', 502 );
		}

		//access to folder
		$accessFolderID = $this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $folderID );
		if( $accessFolderID != $folderID ) {
			$this->except( 'Folder not found', 503 );
		}
		$folder = new DB\Folders( $folderID );
		if( !$folder || !$folder->ID || $folder->ID != $folderID ) {
			$this->except( 'Folder not found', 504 );
		}
		if( !$folder->isCaseFolder() && $folder->depositionID != $deposition->ID ) {
			$this->except( 'Folder not found for deposition', 505 );
		}

		if( $deposition->isDemo() ) {
			if( $this->config->demoDocLimit && ($deposition->getFileCount() - $deposition->getDemoCannedFileCount()) >= $this->config->demoDocLimit ) {
				$this->except( 'Documents for Demo sessions are limited to ' . $this->config->demoDocLimit . '.', 512 );
			}
		}

		//verify file
		$contentType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $file['tmp_name'] );
		$documentTypes = trim( preg_replace( '/\s+/', '', $this->config->valid_files['document_types'] ) );
		$multimediaTypes = trim( preg_replace( '/\s+/', '', $this->config->valid_files['multimedia_types'] ) );
		$uploadTypes = array_merge( explode( ',', $documentTypes ), explode( ',', $multimediaTypes ) );
		if( !in_array( $contentType, $uploadTypes ) ) {
			$this->except( "Unsupported content-type '{$contentType}' for file '{$fileName}'", 506 );
		}
		if( $file['error'] != UPLOAD_ERR_OK ) {
			switch( $file['error'] ):
				case UPLOAD_ERR_NO_FILE:
					$this->except( 'Error: no file sent', 507 );
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					$this->except( 'Error: file size exceeded', 508 );
				case UPLOAD_ERR_PARTIAL:
					$this->except( 'Error: file partially uploaded', 509 );
				default:
					$this->except( "Upload error: {$file['error']}", 510 );
			endswitch;
		}

		$newFile = new DB\Files();
		$newFile->folderID = $folderID;
		$newFile->name = $fileName;
		$newFile->createdBy = $folder->createdBy;
		$newFile->sourceUserID = $this->user->ID;
		$newFile->created = $now;
		$newFile->isExhibit = 0;
		$newFile->isPrivate = ($folder->class == self::FOLDERS_PERSONAL || $folder->class == self::FOLDERS_WITNESSANNOTATIONS) ? 1 : 0;
		$newFile->isTranscript = 0;
		$newFile->isUpload = 1;

		if( stripos( $contentType, 'text/' ) === 0 ) {
			$contentType = 'text/plain';
		}

		Debug::ErrorLog( "contentType: '{$contentType}'" );
		switch( strtolower( $contentType ) ):
			case 'application/pdf':
				$newFile->setSourceFile( $file['tmp_name'], TRUE );
				$newFile->save();
				break;
			case 'audio/wav':
			case 'application/wav':
			case 'audio/x-wav':
			case 'audio/mp2':
			case 'audio/mp3':
			case 'audio/mp4':
			case 'audio/mpeg':
			case 'audio/x-mpeg-2':
			case 'audio/aac':
			case 'audio/vnd.dlna.adts':
			case 'audio/x-aac':
			case 'audio/x-hx-aac-adts':
			case 'audio/x-hx-aac-adif':
			case 'audio/mp4a-latm':
			case 'audio/x-ms-wma':
			case 'video/mpg':
			case 'video/mpeg':
			case 'video/mp2':
			case 'video/mp4':
			case 'application/mp4':
			case 'video/mpeg4':
			case 'video/wmv':
			case 'video/x-ms-wmv':
			case 'video/avi':
			case 'video/msvideo':
			case 'video/x-msvideo':
			case 'video/x-ms-asf':
			case 'video/quicktime':
			case 'video/mov':
			case 'video/x-mov':
			case 'quicktime/mov':
				$filePath = $this->convertMultimedia( $file['tmp_name'] );
				Debug::ErrorLog( "convertMultimedia; completed." );
				if( $filePath && file_exists( $filePath ) ) {
					$fileName = pathinfo( $fileName, PATHINFO_FILENAME );
					$newFile->name = $fileName . '.' . pathinfo( $filePath, PATHINFO_EXTENSION );
					$newFile->setSourceFile( $filePath, TRUE );
					$newFile->save();
					unlink( $filePath );
				}
//				$this->except( 'Needs to be converted to a supported format' );
				break;
			case 'text/plain':
			case 'image/png':
			case 'image/gif':
			case 'image/jpeg':
			case 'image/pjpeg':
			case 'image/ecopy':
			case 'image/tif':
			case 'image/tiff':
				$filePath = $this->convertUploadToPDF( $file['tmp_name'], $contentType );
				Debug::ErrorLog( "convertUploadToPDF; completed." );
				if( $filePath && file_exists( $filePath ) ) {
					$fileName = pathinfo( $fileName, PATHINFO_FILENAME );
					$newFile->name = $fileName . '.' . pathinfo( $filePath, PATHINFO_EXTENSION );
					$newFile->setSourceFile( $filePath, TRUE );
					$newFile->save();
					unlink( $filePath );
				}
				break;
		endswitch;

		if( !$newFile->ID ) {
			$this->except( "Unable to add file -- mime type: {$contentType}", 511 );
		}

		DB\ServiceInvoiceCharges::charge( $this->user->clientID, self::PRICING_OPTION_UPLOAD_PER_DOC, 1, $deposition->caseID, $deposition->ID );

		$fMimeType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $newFile->getFullName() );
		if( $fMimeType == 'application/pdf' && $deposition->class === self::DEPOSITION_CLASS_DEMO ) {
			//Watermark Demo files
			$inPath = $newFile->getFullName();
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';
			//Debug::ErrorLog( "File/addFile; Watermark --\nin: {$inPath}\nout: {$outPath}\n" );
			if( PDFTools::watermark( $inPath, $outPath ) ) {
				//Debug::ErrorLog( "File/addFile; Watermark -- success" );
				rename2( $outPath, $inPath );
			} else {
				//Debug::ErrorLog( "File/addFile; Watermark -- failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
			}
		}

		$folder->lastModified = $now;
		$folder->save();

		$this->getNodeJS()->notifyDepositionUpload( $folder->ID, $this->user->ID );

		$this->return = ['file'=>$newFile->getValues( NULL, $this->user->ID )];
	}

	protected function convertUploadToPDF( $inPath )
	{
		if( !$inPath || !file_exists( $inPath ) ) {
			Debug::ErrorLog( "convertUploadToPDF; file not found: '{$inPath}'" );
			return FALSE;
		}
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID() . '.pdf';
		Debug::ErrorLog( "convertUploadToPDF; {$inPath} to {$outPath}" );
		$converter = new EDPDFConvert( $inPath, TRUE );
		return $converter->convert( $outPath, TRUE );
	}

	protected function convertMultimedia( $inPath )
	{
		Debug::ErrorLog( "convertMultimedia; inPath: '{$inPath}'" );
		if( !$inPath || !file_exists( $inPath ) ) {
			Debug::ErrorLog( "convertMultimedia; file not found: '{$inPath}'" );
			return FALSE;
		}
		$mmConvert = new EDMMConvert();
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . Utils::createUUID();
		$ffprobe = new FFProbe( $inPath );
		if( $ffprobe->isVideo() ) {
			$outPath .= '.mp4';
			if( !$ffprobe->videoIsH264withAAC() ) {
				if( $mmConvert->toMP4( $inPath, $outPath ) ) {
					return $outPath;
				}
				Debug::ErrorLog( "convertMultimedia; (1) failed to convert: '{$inPath}'" );
				return FALSE;
			} else {
				copy( $inPath, $outPath );
				unlink( $inPath );
				return $outPath;
			}
		} else {
			$outPath .= '.mp3';
			if( !$ffprobe->audioIsMP3() ) {
				if( $mmConvert->toMP3( $inPath, $outPath ) ) {
					return $outPath;
				}
				Debug::ErrorLog( "convertMultimedia; (2) failed to convert: '{$inPath}'" );
				return FALSE;
			} else {
				copy( $inPath, $outPath );
				unlink( $inPath );
				return $outPath;
			}
		}
		Debug::ErrorLog( "convertMultimedia; (3) failed to convert: '{$inPath}'" );
	}
}
