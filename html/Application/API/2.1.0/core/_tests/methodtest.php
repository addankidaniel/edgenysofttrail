<?php

namespace ClickBlocks\UnitTest\API;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\UnitTest,
    ClickBlocks\Exceptions,
    ClickBlocks\API,
    ClickBlocks\Core;

abstract class MethodTest extends TestCase
{
  protected $params;
  protected $return;
  
  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $orc;
  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $svc;
  
  private $mockOrchestras;
  private $mockServices;
  private $mockController;
  private $currentUser;
  private $invoked = false;
  
  public static function setUpBeforeClass()
  {
    parent::setUpBeforeClass();
  }
   
  public static function tearDownAfterClass()
  {
    parent::tearDownAfterClass();
  }
   
  public function setUp() 
  {
    parent::setUp();
    $this->invoked = false;
    $this->currentUser = null;
    $this->mockController = null;
    $this->mockOrchestras = array();
    $this->mockServices = array();
  }
  
  public function tearDown()
  {
    parent::tearDown();
  }
  
  /**
   * @param string $origClass
   * @param array $stubMethods array of methods to stub, only used for Controller! 
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function setUpMock($origClass, array $stubMethods = null)
  {    
    $cls = explode('\\',$origClass); $cls=end($cls);
    if (is_subclass_of($origClass,'\ClickBlocks\DB\Orchestra')) {
      return $this->mockOrchestras[substr($cls, 9)] = $this->getFullMock($origClass);
    } elseif (is_subclass_of($origClass,'\ClickBlocks\DB\Service')) {
      return $this->mockServices[substr($cls, 7)] = $this->getFullMock($origClass);
    } elseif (is_subclass_of($origClass,'\ClickBlocks\API\ApiControllerBase')) {
      if (!$stubMethods)
        throw new \Exception('Trying to mock controller with empty method list (would result in all methods stubbed - not expected behaviour');
      return $this->mockController = $this->getMock($origClass, $stubMethods );
    } else
      throw new \Exception("Type of $origClass is unknown");
  }
  
  protected function setUpMockController(array $stubMethods = null)
  {
     return $this->setUpMock($this->getControllerName(), $stubMethods);
  }
  
  protected function setUpMockService($shortName, array $stubMethods = null)
  {
     return $this->setUpMock('\ClickBlocks\DB\Service'.$shortName, $stubMethods);
  }
  
  protected function setUpMockOrchestra($shortName, array $stubMethods = null)
  {
     return $this->setUpMock('\ClickBlocks\DB\Orchestra'.$shortName, $stubMethods);
  }
  
  protected function getMethodName() { }
  protected function getControllerName() { }

  /**
   * @param mixed controller parameters 
   */
  protected function invokeThis($params = null)
  {
    if ($this->invoked)
      throw new \Exception(self::$methodName.' cannot be invoked more than once in single test!');
    $this->invoked = true;
    $cls = explode('\\', get_class($this)); $cls = end($cls);
    $method = $this->getMethodName();
    if (!$method && substr($cls, -4)=='Test') $method = substr($cls, 0, -4);
    if (!$method) throw new \Exception('methodName is not set!');
    $method = 'api_'.strtolower($method);
    if ($params)
      $this->params = $params;
    
    $this->controller = $controller = ($this->mockController ?: new $this->controllerName());
    if ($params['sKey']) {
      if (!is_object($this->currentUser))
        throw new \Exception('Current user data is not defined! Use setCurrentUser()');
      $_SERVER['HTTPS'] = 'on'; // like we're on HTTPS
    }
    //$controller->setUnitTestMode_();
    $controller->attachMock_((array)$this->mockOrchestras, (array)$this->mockServices, @$this->currentUser);
    $this->return = $this->impersonateApi($controller, $method, $params);
    return $this->return;
  }

  /**
   *
   * @param array $userData 
   */
  protected function setCurrentUser(array $userData)
  {
    if (!$userData['ID']) {
      throw new \PHPUnit_Framework_Exception('userID or key required');
    }
    
    $this->currentUser = $this->getMock('ClickBlocks\DB\QuickBLL', array('update', 'insert', 'delete', 'save', 'replace'), array( $userData, 'userID' ));
    
    return $this->currentUser;
  }

    /**
   * @param string $short
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function getOrchestra($short)
  {
    if (!isset($this->mockOrchestras[$short]))
      throw new \Exception('Mock '.$short.' not found! Use setUpApiMock() to set up');
    return $this->mockOrchestras[$short];
  }
  
  /**
   * @param string $short
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function getService($short)
  {
    if (!isset($this->mockServices[$short]))
      throw new \Exception('Mock '.$short.' not found! Use setUpApiMock() to set up');
    return $this->mockServices[$short];
  }
  
  /**
   * @return \PHPUnit_Framework_MockObject_MockObject
   */
  protected function getController()
  {
    if (!isset($this->mockController))
      throw new \Exception('Mock controller not found! Use setUpApiMock() to set up');
    return $this->mockController;
  }

}

?>