<?php

namespace ClickBlocks\API\Logic;

use ClickBlocks\API,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;


class File extends Edepo
{
	public function api_download($p) {
     $this->requireParams(array('fileID'));
     $file = $this->getService('Files')->getByID($p['fileID']);
     if (!$file->ID)
       $this->except('File not found', 1002);
     if ($this->isUserMember()) { // check access for Member
        $folder = $file->folders[0];
       $depoID = $folder->depositionID;
       $attended = $this->getOrchestraDepositionAttendees()->checkUserAttendedDepo($depoID, $this->user->ID);
       $hasAccess = (
              ($attended && ($file->createdBy==$this->user->ID || $folder->isExhibit))
           || ($file->createdBy==$folder->depositions->createdBy && $this->getOrchestraDepositions()->isTrustedUser($this->user->ID, $depoID))
         );
       if (!$hasAccess) $this->except('Access denied', 403);
     } else { // check access for Guest
       if (!$file->folders->isExhibit && !$this->getOrchestra('FileShares')->checkAttendeeAccessToFile($this->attendee->ID, $file->ID))
         $this->except('Access denied', 403);
     }
     $this->flushFile($file->getFullName());
  }
  
  // upload file to user dir
  public function api_upload($p)
  {
     if (!$this->isUserMember()) $this->except ('Only member user access allowed!');
     $this->validateParams(array(
         'depositionID' => array('type'=>self::TYPE_NUMBER, 'req'=>true),
         'folderID' => array('type'=>self::TYPE_NUMBER, 'req'=>false),
         'overwrite' => array('type'=>self::TYPE_BOOLEAN, 'req'=>false),
         'file' => array('type'=>self::TYPE_FILE, 'req'=>true),
     ));
     $depo = $this->getService('Depositions')->getByID($p['depositionID']);
     if (!$depo->ID) $this->except('Deposition not found by ID: '.$p['depositionID'], 404);
     if ($p['folderID']) {
       $folder = $this->getService('Folders')->getByID($p['folderID']);
       if (!$folder->ID) $this->except ('Folder not found by ID: '.$p['folderID'], 404);
       if ($folder->createdBy != $this->user->ID) $this->except ('Access denied to folder!', 403);
     }
     if (!$p['folderID'] || $folder->isExhibit) {
       $folder = $this->getOrchestraFolders()->getPersonalFolderObject($depo->ID, $this->user->ID);
       if (!$folder->ID) { // first time saving to personal folder - create it
          $folder->depositionID = $depo->ID;
          $folder->createdBy = $this->user->ID;
          $folder->name = $this->config->logic['personalFolderName'];
          $folder->isPrivate = 1;
          $folder->insert();
       }
     }
     $info = $_FILES['file'];
     if (!is_array($info['tmp_name'])) {
        foreach ($info as $k=>$v) $info[$k] = array($v);
     }
     $this->return['files'] = array();
     foreach ($info['tmp_name'] as $k=>$v) 
     {
         if ($info['error'][$k] != 0) $this->except ('Incorrect file upload, error: '.$info['error'][$k]);
         $file = $this->getBLL('Files');
         $file->name = $info['name'][$k];
         $file->folderID = $folder->ID;
         $file->createdBy = $this->user->ID;
         $file->setOverwrite($p['overwrite']);
         $file->setSourceFile($info['tmp_name'][$k], true);
         $file->insert();
         $this->return['folderID'] = $folder->ID; // for backward compatibility
         $this->return['fileID'] = $file->ID;     // for backward compatibility
         $this->return['files'][] = array('ID'=>$file->ID, 'folderID'=>$folder->ID);
     }
  }

}

?>
