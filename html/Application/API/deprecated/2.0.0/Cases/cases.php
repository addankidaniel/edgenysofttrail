<?php

namespace ClickBlocks\API\v2_0_0\Logic;

use ClickBlocks\API\v2_0_0,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;


class Cases extends Edepo
{
	public function api_getList( $p )
	{
		$this->validateParams( ['onlyScheduled' => [self::TYPE_BOOLEAN, 'req'=>FALSE]] );
		// cases directly owned or managed, or demos
		$cases = $this->getOrchestraCases()->getAPICasesByUser( $this->user->ID, $this->user->clientID );
		// all accessible depositions list (in all cases)
		$depos = $this->getOrchestraDepositions()->getAPIUserDepositions( $this->user->ID, NULL, $p['onlyScheduled'] );
		$caseIDs = [];
		foreach( $depos as $idx => $depo ) {
			// Remove the password hashes. We don't want these visible!
			unset( $depos[$idx]['password'], $depos[$idx]['courtReporterPassword'] );

			$caseIDs[$depo['caseID']] = TRUE;
		}
		foreach( $cases as $case ) {
			unset( $caseIDs[$case['ID']] );
		}
		// load additional cases if neccesary
		if( count( $caseIDs ) ) {
			$cases = array_merge( $cases, $this->getOrchestraCases()->getCasesByIDs( array_keys( $caseIDs ) ) );
//			usort( $cases, function( $a, $b ){ return strcmp( $a['name'], $b['name'] ); } );
		}
		foreach( $cases as $k => $case ) {
			$cases[$k]['depositions'] = [];
			foreach( $depos as $depo ) {
				if( $depo['caseID'] == $case['ID'] ) {
					$cases[$k]['depositions'][] = $depo;
				}
			}
		}
		$this->return['cases'] = $cases;
	}

	private function getDepositionsForUser( $scheduled=TRUE )
	{
		$scheduled = (bool)$scheduled;
		$caseIDs = [];
		$cases = [];
		$caseDepositions = [];
		$userDepositions = $this->getOrchestraDepositions()->getAPIDepositions( $this->user->ID, $scheduled );
		foreach( $userDepositions as $deposition ) {
			$caseID = (int)$deposition['caseID'];
			$caseIDs[] = $caseID;
			$caseDepositions[$caseID][] = $deposition;
		}
		$userCases = $this->getOrchestraCases()->getCasesByIDs( $caseIDs );
		if( $userCases && is_array( $userCases ) ) {
			foreach( $userCases as $case ) {
				$caseID = (int)$case['ID'];
				$case['depositions'] = $caseDepositions[$caseID];
				$cases[] = $case;
			}
		}
		return $cases;
	}

	public function api_getScheduledList( $p=NULL )
	{
		$this->return['cases'] = $this->getDepositionsForUser( TRUE );
	}

	public function api_getFinishedList( $p=NULL )
	{
		$this->return['cases'] = $this->getDepositionsForUser( FALSE );
	}
}

?>
