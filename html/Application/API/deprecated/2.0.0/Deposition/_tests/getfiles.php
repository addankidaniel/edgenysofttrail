<?php                

namespace ClickBlocks\APITest\Deposition;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\APITest\MethodTest,
    ClickBlocks\APITest;


class getFilesTest extends MethodTest
{
   protected function getControllerName() {
      return '\Clickblocks\API\Logic\Deposition';
   }
   
   public function setUp()
   {
     parent::setUp();
     $this->setUpMockController(array('api_getFiles'));
   }

   public static function impersonateApi()
   {
   }

   public function test_success()
   {
     $param = array('depostionID' => "text");
     $row = array(
		  'folders' => array(
     		 0 => array(
	     		 'ID' => '2',
				 'depositionID' => '33',
				 'createdBy' => '4',
				 'name' => 'ewrerer',
				 'created'=>'2012-09-26 15:04:42',
				 'isPrivate' => '0',
	     		 'files' => array(
	     		 	0 => array(
	     		 		'ID' => '1',
						    'folderID' => '2',
						    'createdBy' => '4',
						    'name' => 'sdfdfd',
						    'created' => '2012-09-26 15:15:11',
						    'isExhibit' => '0',
						    'isPrivate' => '0'
	     		 	),
	     		 	1 => array(
	     		 		        'ID' => '2',
							    'folderID' => '2',
							    'createdBy' => '4',
							    'name' => 'sdfdfs',
							    'created' => '2012-09-26 15:47:03',
							    'isExhibit' => '0',
							    'isPrivate' => '0'
	     		 	)	 
	     		 )
     		 )
     		 
     	  )
	  );
   	 
	 $this->setCurrentUser(array('ID'=>4, 'typeID'=>'C'));
     $this->getController()->expects(self::once())->method('api_getFiles')->with($param)->will(self::returnValue($row)); 
     $info = $this->invokeThis($param);
     $this->assertEquals($row, $info);
   }

}

?>