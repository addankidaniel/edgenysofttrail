<?php

namespace ClickBlocks\API\v2_0_0\Logic;

use ClickBlocks\API\v2_0_0,
ClickBlocks\Core,
ClickBlocks\Utils,
ClickBlocks\DB;


class Deposition extends Edepo
{
	const ERR_HAS_ACTIVE_DEPO = 1101;
	private $errors = array(
		'ERR_UNKNOWN_DEPOSITION' => 'Deposition ID doesn\'t exist.',
		'ERR_EXIST_FOLDER_FILE' => 'Folder ID and File ID parameters can\'t be exist in same request.',
		'ERR_EXIST_EXHIBIT_USERIDS' => 'isExhibit, UserIDs or GuestIDs parameters can\'t be exist in same request.',
		'ERR_NOT_EXIST_FOLDER_ID' => 'Folder ID doesn\'t exist.',
		'ERR_NOT_EXIST_FILE_ID' => 'File ID doesn\'t exist.',
		'ERR_NOT_EXIST_USER' => 'User ID doesn\'t exist.',
		'ERR_NOT_EXIST_CASE' => 'Case ID doesn\'t exist.',
		'ERR_MISSING_FILE' => 'Missing File ID or Folder ID parameter',
		'ERR_MISSING_USER' => 'Missing isExhibit or userIDs or guestIDs parameter',
		'ERR_NOT_SHARE_EXHIBIT' => 'Exhibit folder can\'t be shared.',
	);

	protected function createFolder( $folderName, $userID, $depoID, $class=self::FOLDERS_FOLDER )
	{
		$folder = new \ClickBlocks\DB\Folders();
		$folder->name = $folderName;
		$folder->createdBy = $userID;
		$folder->depositionID = $depoID;
		$folder->insert();
		return $folder->ID;
	}

  protected function buildShareFilesReturn($shareObjects)
  {
      $shares = array();
      foreach ($shareObjects as $share)
      {
          if ($share->attendeeID) {
            $tmp = array('ID' => $share->ID, 'fileID' => $share->fileID, 'guestID' => $share->attendeeID);
          } else {
            $tmp = array('ID' => $share->ID, 'fileID' => ($share->copyFileID ?: $share->fileID), 'userID' => $share->userID);
          }
          $shares[] = $tmp;
      }
      return $shares;
  }

	protected function buildShareFilesReturnNodeJS( $shareObjects, $deposition, $folderName, $fileName='', $isExhibit=0 )
	{
		$receivers = [];
		$folder = NULL;
		$foldersForIDs = [];
		foreach( $shareObjects as $share ) {
			if ($share->copyFileID && $isExhibit) continue;
			//\ClickBlocks\Debug::ErrorLog( "shareID: {$share->ID}" );
			$receiverID = ($share->attendeeID) ? 'G'.$share->attendeeID : $share->userID;
			$fileID = ($share->copyFileID) ? $share->copyFileID : $share->fileID;

			//\ClickBlocks\Debug::ErrorLog( "receiverID: {$receiverID}" );
			//\ClickBlocks\Debug::ErrorLog( "fileID: {$fileID}  copyFileID: {$share->copyFileID}" );

			/*
			if ($share->attendeeID)
			{
				$f = $this->getService('Files')->getByID($share->fileID);
				$receivers['G'.$share->attendeeID]['files'][] = array('ID' => $share->fileID, 'isExhibit'=>(bool)$isExhibit, 'name' => $fileName ?: $f->name, 'shareID' => $share->ID, 'created' => $f->created);
			}
			else
			{
				$f = $this->getService('Files')->getByID($share->copyFileID ?: $share->fileID);
				$receivers[$share->userID]['files'][] = array('ID' => $f->ID, 'isExhibit'=>(bool)$isExhibit, 'name' => $f->name, 'shareID' => $share->ID, 'depositionID' => $folder->depositionID, 'created' => $f->created);
			}
			*/
			$f = $this->getService( 'Files' )->getByID( $fileID );

			// determine the mime-type of the file
			$finfo = finfo_open( FILEINFO_MIME_TYPE );
			$mimetype = finfo_file( $finfo, $f->getFullName() );
			finfo_close( $finfo );

			$fData = [
				'ID' => $f->ID,
				'isExhibit' => (bool)$isExhibit,
				'name' => $f->name,
				'shareID' => $share->ID,
				'created' => $f->created,
				'mimetype' => $mimetype,
			];
			if( (bool)$isExhibit ) {
				$fData['exhibitHistory'] = $this->getOrchestraExhibitHistory()->exhibitHistoryForExhibit( $f->ID );
			}
			$receivers[$receiverID]['files'][] = $fData;

			if( !$foldersForIDs[$receiverID] ) {
				$folder = new \ClickBlocks\DB\Folders( $f->folderID );
				$foldersForIDs[$receiverID] = $folder->getValues( NULL, $this->user->ID );
				//\ClickBlocks\Debug::ErrorLog( print_r( $foldersForIDs[$receiverID], true ) );
			}
		}
		// call to Nodejs
		$args = array();
		$args['sharedBy'] = $this->user->ID;
		$args['depositionID'] = $deposition->ID;
		$args['receivers'] = array();
		foreach ($receivers as $k => $receiver) {
			if ($k[0] == 'G') {
				$receiver['guestID'] = (int)substr($k, 1);
			} else {
				$receiver['userID'] = (int)$k;
			}
			$receiver['folder'] = $foldersForIDs[$k];
			$args['receivers'][] = $receiver;
		}
		//\ClickBlocks\Debug::ErrorLog( print_r( $args, true ) );
		return $args;
	}

	protected function copyFolder( \ClickBlocks\DB\Folders $origFolder, $name, $userID, $depoID, $class=self::FOLDERS_FOLDER )
	{
		$isExhibit = ($class == self::FOLDERS_EXHIBIT);
		$copy = $origFolder->copy();
		$copy->createdBy = $userID;
		$copy->depositionID = $depoID;
		$copy->class = $class;
		if( $name ) {
			$copy->name = $name;
		}
		$copy->insert();
		$idsMap = [];
		foreach( $origFolder->files as $file ) {
			$idsMap[$file->ID] = $this->copyFile( $file, $copy->ID, $userID, NULL, $isExhibit );
		}
		return $idsMap;
	}

	protected function copyFile( $origFile, $folderID, $userID, $fileName='', $isExhibit=0, $isPrivate=0, $sourceID=NULL )
	{
		if( is_object( $origFile ) && $origFile instanceof DB\Files ) {
			$file = $origFile->copy();
		} else {
			$file = new DB\Files;
			$file->setSourceFile( $origFile );
		}
		$file->folderID = (int)$folderID;
		$file->createdBy = (int)$userID;
		$file->sourceUserID = $this->user->ID;
		$file->isExhibit = $isExhibit;
		$file->isPrivate = $isPrivate;
		if ($fileName) $file->name = $fileName;
		$file->sourceID = $sourceID;
		$file->isUpload = 0;
		$file->insert();
		return $file->ID;
	}

   protected function checkDepositionKey($p)
   {
      $depoID = $this->getOrchestraDepositions()->checkDepositionUKey($p['depositionKey'], true);
      if (!$depoID) $this->except ('incorrect depositionKey', 1001);
      return $depoID;
   }

	public function api_get( $p )
	{
		$this->validateParams( [
			'depositionID' => ['type'=>self::TYPE_INTEGER, 'req'=>FALSE],
			'depositionKey' => ['type'=>self::TYPE_STRING, 'req'=>FALSE],
			'includeFiles' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE],
		] );

		if( !$p['depositionID'] && !$p['depositionKey'] ) {
			$this->except( 'DepositionID is required' );
		}

		$depoID = ($p['depositionID'] ?: $this->checkDepositionKey( $p ));
//		$depo = $this->getService( 'Depositions' )->getByID( $depoID );
		$depo = new \ClickBlocks\DB\Depositions( $depoID );
		if( !$depo || !$depo->ID || $depo->ID != $depoID ) {
			$this->except( 'Session not found', 404 );
		}

		// determine that user has permission to get deposition details
		$isCaseManager = (new DB\OrchestraCaseManagers())->checkCaseManager( $depo->caseID, $this->user->ID );
		$isClientAdmin = ($this->user->ID == $depo->cases->clientID && $this->user->typeID == self::USER_TYPE_CLIENT );
		$depoOrchestra = new DB\OrchestraDepositions();
		$isDepositionAssistant = (count( $depoOrchestra->getTrustedDepositions( $depo->caseID, $this->user->ID ) ) > 0);
		$isDepositionAttendee = (count( $depoOrchestra->getByAttendee( $depo->caseID, $this->user->ID ) ) > 0);

		if( !$isCaseManager && !$isClientAdmin && !$isDepositionAssistant && !$isDepositionAttendee ) {
			$this->except( 'Access Denied', 401 );
		}

		$depoValues = BLLFormat::getValues( $depo, $this->user->ID );

		if( !$depoValues['speakerID'] ) {
			$depoValues['speakerID'] = ($depo->parentID ? $this->getOrchestraDepositions()->getOwnerID( $depo->ID ) : $depo->ownerID);
		}

		$depoValues['exhibitHistory'] = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $depo->ID );

		if( $p['includeFiles'] ) {
			$this->api_getFolders( ['depositionID'=>$depo->ID] );
			$depoValues['folders'] = $this->return['folders'];
			unset( $this->return['folders'] );
		}

		$this->return['deposition'] = $depoValues;
		$this->return['deposition']['liveTranscript'] = (!$depo->isWitnessPrep()) ? 'Y' : 'N';
	}

	public function api_getFolders( $p )
	{
		$this->validateParams( array( 'depositionID' => array('type'=>self::TYPE_STRING, 'req'=>true ) ) );
		$userType = ($this->user ? 'M' : $this->attendee->role);
		$userID = ($this->user ? $this->user->ID : $this->attendee->ID);
		$this->return['folders'] = BLLFormat::formatRows( $this->getOrchestraFolders()->getFoldersForUser( $p['depositionID'], $userID, FALSE, $userType ), 'folders' );
	}

	public function api_getFiles($p)
	{
		$this->validateParams(array(
			'folderID' => array('type'=>self::TYPE_STRING, 'req'=>true),
			'sortBy'   => array('type'=>self::TYPE_ENUM, 'req'=>false, 'options'=>['created','name','ID']),
			'sortDir'  => array('type'=>self::TYPE_ENUM, 'req'=>false, 'options'=>['ASC','DESC']),
			'pageSize' => array('type'=>self::TYPE_INTEGER, 'req'=>false),
			'page'     => array('type'=>self::TYPE_INTEGER, 'req'=>false),
		));

		$userID = ($this->isUserMember()) ? $this->user->ID : $this->attendee->ID;
		$folderID = (int)$p['folderID'];
//		$files = $this->getOrchestraFolders()->getFilesForUser( $folderID, $this->user->ID, ['by'=>$p['sortBy'],'dir'=>$p['sortDir']], $p['pageSize'], $p['page'] );
		$files = $this->getOrchestraFolders()->getFolderFilesForAttendee( $userID, $folderID );
		if( !is_array( $files ) ) {
			$this->except( 'Invalid folderID', 1001 );
		}

		//TODO: performance issue, very expensive loop
		// capture the mime-type for each file and add to the response packet
		$finfo = new \finfo( FILEINFO_MIME_TYPE );
		foreach( $files as &$file ) {
			$File = new \ClickBlocks\DB\Files( $file['ID'] );
			$file['mimeType'] = $finfo->file( $File->getFullName() );
		}
		unset( $finfo );

		$this->return['files'] = $files;
	}

	/**
	 * @deprecated	As of 03-26-2014.
	*/
	public function api_getList($p)
	{
		$this->except('This API method is deprecated. Please update as needed.', 501);
		/*    $this->validateParams(array(
		              'caseID' => array('type'=>self::TYPE_INTEGER, 'req'=>true)
		    ));
		    $case = $this->getService('Cases')->getByID($p['caseID']);
		    if (!$case->ID) $this->except($this->errors['ERR_NOT_EXIST_CASE'], 104);
		    $this->return['depositions'] = $this->getOrchestraDepositions()->getAllByCase($p['caseID']);
		    foreach ($this->return['depositions'] as &$depo) $depo['ownerID'] = $case->createdBy;
		*/
	}

	/**
	 * @deprecated since version 1.7.0
	 */
	public function api_shareFiles( $p )
	{
		//TODO: deprecate
		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );
		$this->validateParams(array(
			'depositionID' => array('type' => self::TYPE_SCALAR, 'req'=>true),
			'fileID' => array('type' => self::TYPE_SCALAR, 'req'=>false),
			'folderID' => array('type' => self::TYPE_SCALAR, 'req'=>false),
			'sourceFileID' => array('type' => self::TYPE_SCALAR, 'req'=>false),
			'name' => array('type' => self::TYPE_STRING, 'req'=>false),
			'file' => array('type' => self::TYPE_FILE, 'req'=>false),
			'userIDs' => array('type' => self::TYPE_ARRAY, 'req'=>false),
			'guestIDs' => array('type' => self::TYPE_ARRAY, 'req'=>false),
			'isExhibit' => array('type' => self::TYPE_BOOLEAN, 'req'=>false),
			'exhibitTitle' => array( 'type'=>self::TYPE_STRING, 'req'=>FALSE ),
			'exhibitSubTitle' => array( 'type'=>self::TYPE_STRING, 'req'=>FALSE ),
			'exhibitXOrigin' => array( 'type'=>self::TYPE_NUMBER, 'req'=>FALSE ),
			'exhibitYOrigin' => array( 'type'=>self::TYPE_NUMBER, 'req'=>FALSE ),
			'overwrite' => array( 'type'=>self::TYPE_BOOLEAN, 'req'=>FALSE ),
			'tempSourceID' => array( 'type'=>self::TYPE_SCALAR, 'req'=>FALSE)
		));

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		$isTempFile = false;
		//WebApp -- introduce exhibit
		if( (int)$p['isExhibit'] && !isset( $_FILES['file'] ) ) {
			$this->prepareExhibitFile( $p );
			if ( isset( $p['tempSourceID'] ) && (int)$p['tempSourceID'] > 0) {
				$isTempFile = true;
			}
		}

		// ED-42; Flatten Exhibits
		if( $p['isExhibit'] && isset( $_FILES['file'] ) &&  $_FILES['file']['tmp_name'] && $_FILES['file']['error'] == UPLOAD_ERR_OK ) {
			$inPath = $_FILES['file']['tmp_name'];
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID();
			//\ClickBlocks\Debug::ErrorLog( "Deposition::api_shareFiles; Flatten Exhibit --\nin: {$inPath}\nout: {$outPath}\n" );
			if( \ClickBlocks\PDFTools::flatten( $inPath, $outPath ) ) {
				\ClickBlocks\Debug::ErrorLog( "Deposition::api_shareFiles; Flatten Exhibit -- success" );
				rename( $outPath, $inPath );
			} else {
				\ClickBlocks\Debug::ErrorLog( "Deposition::api_shareFiles; Flatten Exhibit -- failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
			}
		}

		if( $p['fileID'] && $p['isExhibit'] && isset( $p['overwrite'] ) && $p['overwrite'] ) {
			//no need to re-establish file shares etc, just handle the overwrite
			$this->overwriteExhibit( $p );
			return;
		}

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		$guestIDs = $targetUsers = $filesInfo = $shareObjects = $copiedFileIDs = $attendees = array();
		$origFile = $origFolder = null;
		$folderName = $fileName = '';

		if ($p['isExhibit']==false && count($p['userIDs'])==0 && count($p['guestIDs'])==0) $this->except($this->errors['ERR_MISSING_USER'], 1016);

		// this is deposition I logged in
		$deposition = $this->getService('Depositions')->getByID($p['depositionID']);
		if( !$deposition->ID )
			$this->except('Deposition not found', 1000);

		// this is parent (source) deposition of my deposition. by default it is equal to $deposition
		$mainDeposition = ($deposition->parentID) ? $deposition->parentDepositions1[0] : $deposition;
		$childDepoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $mainDeposition->ID );
		//\ClickBlocks\Debug::ErrorLog( "childDepoID: {$childDepoID}" );
		$childDeposition = ($childDepoID) ? $this->getService( 'Depositions' )->getByID( $childDepoID ) : FALSE;
		//\ClickBlocks\Debug::ErrorLog( print_r( $childDeposition->getValues(), TRUE ) );
		if( $childDeposition && $childDeposition->ID ) {
			$deposition = $childDeposition;
		}

		// Only Owner OR speaker of main (source) deposition can share!
		if ($mainDeposition->ownerID != $this->user->ID && (!$mainDeposition->speakerID || $mainDeposition->speakerID != $this->user->ID))
			$this->except('Access denied', 403);

		if( (int)$p['isExhibit'] ) {
			//courtesy copy folder -- create if not exists
			$ccFolder = $this->getOrchestraFolders()->getCourtesyCopyFolder( $mainDeposition->ID );
			if( !$ccFolder ) {
				\ClickBlocks\Debug::ErrorLog( 'shareFiles -- creating "' . $this->config->logic['courtesyCopyFolderName'] . '" folder' );
				$ccFolder = new DB\Folders();
				$ccFolder->name = $this->config->logic['courtesyCopyFolderName'];
//				$ccFolder->isCourtesyCopy = 1;
				$ccFolder->class = 'CourtesyCopy';
				$ccFolder->createdBy = $mainDeposition->createdBy;
				$ccFolder->depositionID = $mainDeposition->ID;
				$ccFolder->insert();
			}
		}

		if(((int)$p['isExhibit'] && (isset($p['userIDs']) || isset($p['guestIDs']))) || ((int)$p['isExhibit'] && ((isset($p['userIDs']) && count($p['userIDs']) < 1 ) || (isset($p['guestIDs']) && count($p['guestIDs']) < 1))))
			$this->except($this->errors['ERR_EXIST_EXHIBIT_USERIDS'], 1011);
		if(isset($p['folderID']) && isset($p['fileID']))
			$this->except($this->errors['ERR_EXIST_FOLDER_FILE'], 1010);
		if(empty($p['folderID']) && empty($p['fileID']))
			$this->except($this->errors['ERR_MISSING_FILE'], 1025);

		if (isset($p['fileID'])) {
			$origFile = $this->getService('Files')->getByID($p['fileID']);
			if( !$origFile->ID )
				$this->except( $this->errors['ERR_NOT_EXIST_FILE_ID'], 1013 );
			if( $origFile->folders->depositionID != $deposition->ID )
				$this->except( $this->errors['ERR_NOT_EXIST_FILE_ID'], 1013 );
			if( !in_array( $origFile->createdBy, array( $deposition->createdBy, $this->user->ID ) ) )
				$this->except( 'No access to given file', 1000 );
			$fileName = $origFile->name;
			$filesInfo[] = array('ID' => $origFile->ID, 'name' => $file->name);
			$folderName = $this->config->logic['personalFolderName'];
		}

		if (isset($p['folderID'])) {
			$origFolder = $this->getService('Folders')->getByID($p['folderID']);
			if( !$origFolder->ID || $origFolder->depositionID != $deposition->ID )
				$this->except($this->errors['ERR_NOT_EXIST_FOLDER_ID'], 1012);
			if( !in_array( $origFolder->createdBy, array( $deposition->createdBy, $this->user->ID ) ) )
				$this->except( 'No access to given file', 1000 );
			//if (($origFolder->isExhibit && $origFolder->name == $this->config->logic['exhibitFolderName']) || $origFolder->isPrivate)
			if ($origFolder->class == 'Exhibit' && $origFolder->name == $this->config->logic['exhibitFolderName'])
				$this->except('Cannot share Exhibit folders.', 1017);
			$folderName = $origFolder->name;
			$filesInfo = $this->getOrchestraFiles()->getFilesByFolder($p['folderID']);
			if (count($filesInfo) < 1) $this->except('Empty folder can\'t be shared.', 1026);
		}

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		// Validate name parameter
		$p['name'] = trim( $p['name'] );
		if( $p['name'] ) {
			if ($p['folderID']) {
				// check folder name not include special character
				if( !preg_match( '=^[^/?*;:{}\\\\]+$=', $p['name'] ) )
					$this->except('Folder Name is invalid', 1029);
				$folderName = $p['name'];
			} else {
				// check file name not include special character and has a dot
				if( !preg_match( '=^[^/?*;:{}\\\\]+\.[^/?*;:{}\\\\]+$=', $p['name'] ) )
					$this->except('File Name is invalid', 1021);
				$fileName = $p['name'];
			}
		}
		// this is trusted users from Local Depo
		$trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap($deposition->ID);

		// Don't share Folder to trusted users
		$needShareToUser = function($userID) use (&$trustedUsers, $p)
		{
			return (!isset($trustedUsers[$userID]) || ($p['fileID']));
		};

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );
		$sourceFileID = ( isset( $p['sourceFileID'] ) && $p['sourceFileID'] ? $p['sourceFileID'] : $p['fileID'] );
		$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $sourceFileID );
		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );

		$depoAttendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo( $mainDeposition->ID );
		$depoAttendeesByID = [];
		foreach( $depoAttendees as $attendee ) {
			$depoAttendeesByID[$attendee['ID']] = $attendee;
		}
		unset( $depoAttendees );

		if( count( $p['userIDs'] ) > 0 ) {
			$depoMap = $this->getOrchestraDepositions()->getClient2DepoMapForLinkedTo($mainDeposition->ID);
			foreach($p['userIDs'] as $id) {
				if( isset( $depoAttendeesByID[$id] ) && $depoAttendeesByID[$id]['role'] !== 'M' ) continue;	//only share to member attendees
				if ($needShareToUser($id) == false) continue; // Don't share to specific users in some cases
				$user = $this->getService('Users')->getByID($id);
				if (!$user->ID) $this->except('UserID '.$id.' not exist!', 1018);
				if (!isset($depoMap[$user->clientID]))
					$this->except('Cannot share to user '.$id.' (clientID='.$user->clientID.'), linked deposition for this Client not found.', 1040);
				$targetUsers[$id] = array('userID'=>$user->ID, 'depositionID'=>$depoMap[$user->clientID]);
			}
		}

		if (count($p['guestIDs']) > 0)
		{
			foreach($p['guestIDs'] as $id) {
				$guest = $this->getService('DepositionAttendees')->getByID($id);
				if (!$guest->ID) $this->except('GuestID '.$id.' not exist', 1019);
			}
			$guestIDs = $p['guestIDs'];
		}

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		$addShare = function ($userID, $attendeeID, $fileID, $fileCopyID, $depositionID)
		{
			$share = $this->getBLL('FileShares');
			$share->depositionID = $depositionID;
			$share->fileID = $fileID;
			$share->copyFileID = $fileCopyID;
			$share->createdBy = $this->user->ID;
			$share->userID = $userID;
			$share->attendeeID = $attendeeID;
			$share->insert();
			return $share;
		};

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

      // Share to Users
      if((int)$p['isExhibit'])
      {
		$introducedDate = date( 'Y-m-d H:i:s' );
		$introducedBy = "{$this->user->firstName} {$this->user->lastName}";
		$srcFileID = ((int)$p['sourceFileID']) ? (int)$p['sourceFileID'] : NULL;
		if ($isTempFile) {
			// For temp files that will be deleted, we need to use the tempSourceID for the file.
			$srcFileID = (int)$p['tempSourceID'];
		}

		//source file
		$srcFile = new \ClickBlocks\DB\Files( $srcFileID );
		\ClickBlocks\Debug::ErrorLog( "srcFileID: {$srcFileID} srcFile->ID: {$srcFile->ID}" );

        // get all attendees of main deposition + all child depos
        $attendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo($mainDeposition->ID);

        // always process main deposition, for guests
        $targetDepos = array($mainDeposition->ID => array());
        foreach ($attendees as $att)
        {
           //if ($att['userID'] == $this->user->ID) continue; // don't share to self
           if ($att['userID']) {
              if ($needShareToUser($att['userID']) == false) continue; // Don't share to specific users in some cases
              $targetDepos[$att['depositionID']][] = $att['userID'];
           } else $guestIDs[] = $att['ID'];
        }
        if ($p['folderID'])
        {
            // for share FOLDER: set this folder as isExhibit:
            $origFolder->isExhibit = 1;
			$origFolder->class = 'Exhibit';
            $origFolder->update();
            // share all files from folder to target users of Local Deposition
            foreach ((array)$targetDepos[$deposition->ID] as $userID)
            {
               foreach ($filesInfo as $fileInfo) {
				  //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
                  $shareObjects[] = $addShare($userID, null, $fileInfo['ID'], null, $deposition->ID);
               }
            }
        } else if ($p['fileID'])
        {
           $folderName = $this->config->logic['exhibitFolderName'];
           // Override filesInfo array with file that we will put in exhibit folder
		   // $filesInfo array will only be used for guests, because each target will have his own filesInfo
		   $filesInfo = array();
        }
        // copy shared files to each creator of all depositions within association graph (Source Depo + all Target Depos)
        foreach ($targetDepos as $depoID=>$userIDs)
        {
           if ($p['folderID'] && ($depoID == $deposition->ID)) continue; // don't copy folder to self
           $creatorID = $this->getOrchestraDepositions()->getCreatorID($depoID);
           if ($p['folderID'])
           {
              // when I share folder to all attendees - create this folder in Target Depo creator with all contents copied
              $copiedFileIDs = $this->copyFolder($origFolder, $folderName, $creatorID, $depoID, 1);
           } else {
              // copy file to "Exhibit" folder of Creator
              $ownerFolderID = $this->getOrchestraFolders()->getExhibitFolder($depoID, $creatorID)['ID'];
              if (!$ownerFolderID) $ownerFolderID = $this->createFolder($this->config->logic['exhibitFolderName'], $creatorID, $depoID, 'Exhibit');
              // This will copy original file (OR uploaded file with watermark) to this deposition creator
              if ($_FILES['file']['tmp_name'] && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
                 $_sourceFile = '/tmp/'.uniqid();
                 copy($_FILES['file']['tmp_name'], $_sourceFile);
               } else {
                 $_sourceFile = $origFile;
               }
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- copyFile' );
			$copyFileID = $this->copyFile($_sourceFile, $ownerFolderID, $creatorID, $fileName, 1, 0, $sourceFileID);
			$copiedFileIDs[$p['fileID']] = $copyFileID;
			$this->getOrchestraFolders()->setLastModified( $ownerFolderID );

			//ED-631; Exhibit History
			$targetDepo = new \ClickBlocks\DB\Depositions( $depoID );
			$copyFile = new \ClickBlocks\DB\Files( $copyFileID );
			$exhibitHistory = new \ClickBlocks\DB\ExhibitHistory();
			$exhibitHistory->exhibitFileID = $copyFile->ID;
			$exhibitHistory->caseID = $targetDepo->caseID;
			$exhibitHistory->depositionID = $targetDepo->ID;
			$exhibitHistory->introducedDate = $introducedDate;
			$exhibitHistory->introducedBy = $introducedBy;
			\ClickBlocks\Debug::ErrorLog( "srcFile->ID: {$srcFile->ID}, copyFile->users->clientID: {$copyFile->users->clientID}, user->clientID: {$this->user->clientID}" );
			if( $srcFile->ID && $copyFile->users->clientID == $this->user->clientID ) {
				\ClickBlocks\Debug::ErrorLog( 'setting ExhibitHistory sourceFileID: ' . $srcFile->ID );
				$exhibitHistory->sourceFileID = $srcFile->ID;
//				$this->return['introducedFile'] = $copyFile->getValues();
			}
			\ClickBlocks\Debug::ErrorLog( "user->clientID: {$this->user->clientID}, targetDepo->cases->clientID: {$targetDepo->cases->clientID}" );
			if( $this->user->clientID == $targetDepo->cases->clientID ) {
				\ClickBlocks\Debug::ErrorLog( 'setting introducedFile: ' . $copyFile->ID );
				$this->return['introducedFile'] = $copyFile->getValues();
			}
			$exhibitHistory->insert();
           }
		   //\ClickBlocks\Debug::ErrorLog( print_r( $copiedFileIDs, TRUE ) );
           foreach ($copiedFileIDs as $origFileID=>$copyFileID)
           {
              // Share file to Depo Creator
			  //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
              $shareObjects[] = $addShare($creatorID, null, $origFileID, $copyFileID, $depoID);
              if ($depoID == $mainDeposition->ID) $filesInfo[] = array('ID' => $copyFileID); // this for guests of main deposition
              foreach ($userIDs as $userID) {
                 // Share file to specific user in this Depo
				 //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
                 $shareObjects[] = $addShare($userID, null, $copyFileID, null, $depoID);
              }
           }
        }

			//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

			//exhibit stamp info
			$this->saveExhibitStampMetadata( $p, $mainDeposition );
		}
      else  // share to specific users
      {
		  //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sharing to specific user' );
         foreach ($targetUsers as $userID=>$info)
         {
            if ($p['folderID']) // we are copying folder, and we need to create with same name on destination user
            {
				//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- copyFolder' );
               $copiedFileIDs = $this->copyFolder($origFolder, $p['name'], $userID, $info['depositionID']);
            }
            else  // copying one file to Personal folder
            {
               $folderID = $this->getOrchestraFolders()->getPersonalFolder($info['depositionID'], $userID)['ID'];
               if (!$folderID) $folderID = $this->createFolder($this->config->logic['personalFolderName'], $userID, $info['depositionID'], 'Personal');
               $copiedFileIDs[$origFile->ID] = $this->copyFile($origFile, $folderID, $userID, $fileName, 0, 1, $sourceFileID);
			   $this->getOrchestraFolders()->setLastModified( $folderID );
            }
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- start' );
            foreach($copiedFileIDs as $origFileID => $copyFileID)
            {
			   //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
               $shareObjects[] = $addShare($userID, null, $origFileID, $copyFileID, $info['depositionID']);
            }
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- end' );
         }
      }

		// Add Shares for Guests
		$guestAttendees = [];
		$guestsAttendeeIDs = [];
		$nwAttendees = $this->getOrchestraDepositionAttendees()->getNonWitnessAttendessForDepo( $mainDeposition->ID );
		if( is_array( $nwAttendees ) ) {
			foreach( $nwAttendees as $nwAttendee ) {
				if( $nwAttendee['role'] === 'G' ) $guestsAttendeeIDs[] = $nwAttendee['ID'];
			}
		}

		//\ClickBlocks\Debug::ErrorLog( print_r( $guestsAttendeeIDs, TRUE ) );
		foreach( $guestIDs as $guestID ) {
			if( $p['isExhibit'] ) {
				//guest vs witness -- only witness get "Official Exhibits", guest attendee gets "Courtesy Copy"
				//\ClickBlocks\Debug::ErrorLog( 'GuestID: ' . $guestID );
				if( in_array( $guestID, $guestsAttendeeIDs ) ) {
					\ClickBlocks\Debug::ErrorLog( 'Guest (' . $guestID . ') is Attendee ... ' );
					$guestAttendees[] = $guestID;
					continue;
				}
				//\ClickBlocks\Debug::ErrorLog( 'Guest (' . $guestID . ') is Witness ... ' );
				// add to fileshares table
				foreach( $filesInfo as $fileInfo ) {
				  //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
				  $shareObjects[] = $addShare( null, $guestID, $fileInfo['ID'], null, $mainDeposition->ID );
				}
			} else {
				// add to fileshares table
				foreach( $filesInfo as $fileInfo ) {
				  //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
				  $shareObjects[] = $addShare( null, $guestID, $fileInfo['ID'], null, $mainDeposition->ID );
				}
			}
		}
		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );
		if( $p['isExhibit'] ) {
			$ccShares = $this->shareCourtesyCopy( $mainDeposition->ID, $sourceFileID, $p['name'], $guestAttendees );
			$shareObjects = array_merge( $shareObjects, $ccShares );
		}

		// For the Web App, if this is a temp file, we will delete it after we have introduced it.
		if ($isTempFile) {
			$sourceFile = new \ClickBlocks\DB\Files( $p['sourceFileID'] );
			if ( $sourceFile && $sourceFile->folders->class == 'Personal' && strpos( $sourceFile->name, 'tmp_' ) === 0 ) {
				\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- deleting temp file: ' . $sourceFile->name);
				$sourceFile->delete();
			}
		}
//		$shareObjs = array();
//		foreach( $shareObjects as $fileShare ) {
//			$shareObjs[] = array( 'ID'=>$fileShare->ID, 'fileID'=>($fileShare->copyFileID ?: $fileShare->fileID), 'userID' => $fileShare->userID );
//		}
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- done with shares -- shareObjects: ' . print_r( $shareObjs, TRUE ) );

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		// add result to Nodejs
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- buildShareFilesReturnNodeJS' );
		$args = $this->buildShareFilesReturnNodeJS( $shareObjects, $mainDeposition, $folderName, $fileName, $p['isExhibit'] );
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- args: ' . print_r( $args, true ) );
		$this->return['shares'] = $this->buildShareFilesReturn($shareObjects);
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sendPostCommand' );
		$this->getNodeJS()->sendPostCommand( 'sharefiles', null, $args );
		//$this->return['argsToNodeJS'] = $args;
	}

	public function api_introduceExhibit( $p )
	{
		$this->validateParams( [
			'depositionID'=>['type'=>self::TYPE_SCALAR, 'req'=>TRUE],
			'fileID'=>['type'=>self::TYPE_SCALAR, 'req'=>TRUE],
			'sourceFileID'=>['type'=>self::TYPE_SCALAR, 'req'=>FALSE],
			'name'=>['type'=>self::TYPE_STRING, 'req'=>FALSE],
			'file'=>['type'=>self::TYPE_FILE, 'req'=>FALSE],
			'isExhibit'=>['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE],
			'exhibitTitle'=>['type'=>self::TYPE_STRING, 'req'=>FALSE],
			'exhibitSubTitle'=>['type'=>self::TYPE_STRING, 'req'=>FALSE],
			'exhibitXOrigin'=>['type'=>self::TYPE_NUMBER, 'req'=>FALSE],
			'exhibitYOrigin'=>['type'=>self::TYPE_NUMBER, 'req'=>FALSE],
			'overwrite'=>['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE],
			'skipStamp' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE],
			'tempSourceID'=>['type'=>self::TYPE_SCALAR, 'req'=>FALSE],
			'isMultimedia' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE]
		] );

		$isTempFile = FALSE;
		$depositionID = (int)$p['depositionID'];
		$fileID = (int)$p['fileID'];
		$isMultimedia = (bool)$p['isMultimedia'];
		$now = date( 'Y-m-d H:i:s' );

		if( !$isMultimedia && isset( $_FILES['file'] ) && $_FILES['file']['error'] == UPLOAD_ERR_OK ) {
			$inMimeType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $_FILES['file']['tmp_name'] );
			if( $inMimeType != 'application/pdf' ) {
				\ClickBlocks\Debug::ErrorLog( "api_introduceExhibit; manually setting isMultimedia for: '{$p['name']}' type: '{$inMimeType}'" );
				$isMultimedia = $p['isMultimedia'] = TRUE;
			}
		}

		//WebApp -- introduce exhibit
		if( !$isMultimedia && !isset( $_FILES['file'] ) ) {
			$this->prepareExhibitFile( $p );	//fakes an upload entry to $_FILES for flatten
			if ( isset( $p['tempSourceID'] ) && (int)$p['tempSourceID'] ) {
				$isTempFile = TRUE;
			}
		}

		// ED-42; Flatten Exhibits
		if( !$isMultimedia && isset( $_FILES['file'] ) && $_FILES['file']['tmp_name'] && $_FILES['file']['error'] == UPLOAD_ERR_OK ) {
			$inPath = $_FILES['file']['tmp_name'];
			$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.pdf';
			//\ClickBlocks\Debug::ErrorLog( "Deposition::api_shareFiles; Flatten Exhibit --\nin: {$inPath}\nout: {$outPath}\n" );
			if( \ClickBlocks\PDFTools::flatten( $inPath, $outPath ) ) {
				\ClickBlocks\Debug::ErrorLog( "Deposition::api_introduceExhibit; Flatten Exhibit -- success" );
				rename2( $outPath, $inPath );
			} else {
				\ClickBlocks\Debug::ErrorLog( "Deposition::api_introduceExhibit; Flatten Exhibit -- failure" );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
			}
		}

		if( $p['fileID'] && isset( $p['overwrite'] ) && $p['overwrite'] ) {
			//no need to re-establish file shares etc, just handle the overwrite
			$this->overwriteExhibit( $p );
			return;
		}

		$targetUsers = $filesInfo = $shareObjects = $copiedFileIDs = $attendees = [];
		$origFile = $origFolder = NULL;

		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
			$this->except( 'Deposition not found', 1000 );
		}

		$mainDeposition = ($deposition->parentID) ? $deposition->parentDepositions1[0] : $deposition;
		$childDepoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $mainDeposition->ID );
		$childDeposition = ($childDepoID) ? new \ClickBlocks\DB\Depositions( $childDepoID ) : FALSE;
		if( $childDeposition && $childDeposition->ID ) {
			$deposition = $childDeposition;
		}

		// Only speaker of main deposition can introduce
		if( $mainDeposition->speakerID && $mainDeposition->speakerID != $this->user->ID ) {
			$this->except( 'Access denied', 403 );
		}

		//courtesy copy folder -- create if not exists
		$ccFolder = $this->getOrchestraFolders()->getCourtesyCopyFolder( $mainDeposition->ID );
		if( !$ccFolder ) {
			\ClickBlocks\Debug::ErrorLog( 'introduceExhibit -- creating "' . $this->config->logic['courtesyCopyFolderName'] . '" folder' );
			$ccFolder = new \ClickBlocks\DB\Folders();
			$ccFolder->name = $this->config->logic['courtesyCopyFolderName'];
			$ccFolder->class = self::FOLDERS_COURTESYCOPY;
			$ccFolder->createdBy = $mainDeposition->createdBy;
			$ccFolder->depositionID = $mainDeposition->ID;
			$ccFolder->created = $now;
			$ccFolder->lastModified = $now;
			$ccFolder->insert();
		}

		if( !$fileID ) {
			$this->except( $this->errors['ERR_MISSING_FILE'], 1025 );
		}

		$origFile = new \ClickBlocks\DB\Files( $fileID );
		if( !$origFile || !$origFile->ID || $origFile->ID != $fileID ) {
			$this->except( $this->errors['ERR_NOT_EXIST_FILE_ID'], 1013 );
		}
		if( $origFile->folders[0]->depositionID != $deposition->ID ) {
			$this->except( $this->errors['ERR_NOT_EXIST_FILE_ID'], 1013 );
		}
		if( !$this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $origFile->folderID ) ) {
			$this->except( 'No access to given file', 1000 );
		}
		$fileName = $origFile->name;
//		$filesInfo[] = ['ID'=>$origFile->ID, 'name'=>$file->name];
		$folderName = $this->config->logic['exhibitFolderName'];

		// Validate name parameter
		$p['name'] = trim( $p['name'] );
		if( $p['name'] ) {
			// check file name not include special character and has a dot
			if( !preg_match( '=^[^/?*;:{}\\\\]+\.[^/?*;:{}\\\\]+$=', $p['name'] ) ) {
				$this->except( 'File Name is invalid', 1021 );
			}
			$fileName = $p['name'];
		}

		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $p['sourceFileID'] );
		$sourceFileID = (isset( $p['sourceFileID'] ) && $p['sourceFileID'] ? $p['sourceFileID'] : $p['fileID']);
		$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $sourceFileID );
		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );

//		$depoAttendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo( $mainDeposition->ID );
//		$depoAttendeesByID = [];
//		foreach( $depoAttendees as $attendee ) {
//			$depoAttendeesByID[$attendee['ID']] = $attendee;
//		}
//		unset( $depoAttendees );

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		$addShare = function( $userID, $attendeeID, $fileID, $fileCopyID, $depositionID )
		{
			$share = new \ClickBlocks\DB\FileShares();
			$share->depositionID = $depositionID;
			$share->fileID = $fileID;
			$share->copyFileID = $fileCopyID;
			$share->createdBy = $this->user->ID;	//TODO: scope?
			$share->userID = $userID;
			$share->attendeeID = $attendeeID;
			$share->insert();
			return $share;
		};

		$introducedBy = "{$this->user->firstName} {$this->user->lastName}";
		$srcFileID = ((int)$p['sourceFileID']) ? (int)$p['sourceFileID'] : NULL;
		if( $isTempFile ) {
			// For temp files that will be deleted, we need to use the tempSourceID for the file.
			$srcFileID = (int)$p['tempSourceID'];
		}

		//source file
		$srcFile = new \ClickBlocks\DB\Files( $srcFileID );

		// get all attendees of main deposition + all child depos
		$attendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo( $mainDeposition->ID );

		// always process main deposition, for guests & witness
		$witnesses = [];
		$targetDepos = [$mainDeposition->ID => []];
		foreach( $attendees as $att ) {
			//if ($att['userID'] == $this->user->ID) continue; // don't share to self
			if( $att['userID'] ) {
				$targetDepos[$att['depositionID']][] = $att['userID'];
			} elseif( $att['role'] === self::DEPOSITIONATTENDEE_ROLE_WITNESS || $att['role'] === self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER ) {
				$witnesses[] = $att;
			}
		}

		$folderName = $this->config->logic['exhibitFolderName'];
		// Override filesInfo array with file that we will put in exhibit folder
		// $filesInfo array will only be used for guests, because each target will have his own filesInfo
		$filesInfo = [];

		// copy shared files to each creator of all depositions within association graph (Source Depo + all Target Depos)
		foreach( $targetDepos as $depoID => $userIDs ) {
			$creatorID = $this->getOrchestraDepositions()->getCreatorID( $depoID );

			// copy file to "Exhibit" folder of Creator
			$exhibitFolder = $this->getOrchestraFolders()->getExhibitFolder( $depoID, $creatorID );
			$ownerFolderID = $exhibitFolder['ID'];
			if( !$ownerFolderID ) {
				$ownerFolderID = $this->createFolder( $this->config->logic['exhibitFolderName'], $creatorID, $depoID, self::FOLDERS_EXHIBIT );
			}
			// This will copy original file (OR uploaded file with watermark) to this deposition creator
			if( !$isMultimedia && $_FILES['file']['tmp_name'] && $_FILES['file']['error'] == UPLOAD_ERR_OK ) {
				$_sourceFile = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.pdf';
				copy( $_FILES['file']['tmp_name'], $_sourceFile );
			} else {
				$_sourceFile = $origFile;
			}
			$copyFileID = $this->copyFile( $_sourceFile, $ownerFolderID, $creatorID, $fileName, 1, 0, $sourceFileID );
			$copiedFileIDs[$p['fileID']] = $copyFileID;
			$this->getOrchestraFolders()->setLastModified( $ownerFolderID );

			//ED-631; Exhibit History
			$targetDepo = new \ClickBlocks\DB\Depositions( $depoID );
			$copyFile = new \ClickBlocks\DB\Files( $copyFileID );
			$exhibitHistory = new \ClickBlocks\DB\ExhibitHistory();
			$exhibitHistory->exhibitFileID = $copyFile->ID;
			$exhibitHistory->caseID = $targetDepo->caseID;
			$exhibitHistory->depositionID = $targetDepo->ID;
			$exhibitHistory->introducedDate = $now;
			$exhibitHistory->introducedBy = $introducedBy;
			\ClickBlocks\Debug::ErrorLog( "srcFile->ID: {$srcFile->ID}, copyFile->users->clientID: {$copyFile->users->clientID}, user->clientID: {$this->user->clientID}" );
			if( $srcFile->ID && $copyFile->users->clientID == $this->user->clientID ) {
				\ClickBlocks\Debug::ErrorLog( 'setting ExhibitHistory sourceFileID: ' . $srcFile->ID );
				$exhibitHistory->sourceFileID = $srcFile->ID;
//				$this->return['introducedFile'] = $copyFile->getValues();
			}
			\ClickBlocks\Debug::ErrorLog( "user->clientID: {$this->user->clientID}, targetDepo->cases->clientID: {$targetDepo->cases->clientID}" );
			if( $this->user->clientID == $targetDepo->cases->clientID ) {
				\ClickBlocks\Debug::ErrorLog( 'setting introducedFile: ' . $copyFile->ID );
				$this->return['introducedFile'] = $copyFile->getValues();
			}
			$exhibitHistory->insert();

			//\ClickBlocks\Debug::ErrorLog( print_r( $copiedFileIDs, TRUE ) );
			foreach ($copiedFileIDs as $origFileID=>$copyFileID)
			{
				// Share file to Depo Creator
				//\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
				$shareObjects[] = $addShare( $creatorID, NULL, $origFileID, $copyFileID, $depoID );
				if( $depoID == $mainDeposition->ID ) {
					$filesInfo[] = ['ID' => $copyFileID]; // this for guests of main deposition
				}
				foreach( $userIDs as $userID ) {
					// Share file to specific user in this Depo
					//\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
					$shareObjects[] = $addShare( $userID, NULL, $copyFileID, NULL, $depoID );
				}
			}
		}

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		//exhibit stamp info
		$this->saveExhibitStampMetadata( $p, $mainDeposition );

		// Add Shares for Guests
		$guestAttendees = [];
		$guestsAttendeeIDs = [];
		$nwAttendees = $this->getOrchestraDepositionAttendees()->getNonWitnessAttendessForDepo( $mainDeposition->ID );
		if( is_array( $nwAttendees ) ) {
			foreach( $nwAttendees as $nwAttendee ) {
				if( $nwAttendee['role'] === 'G' ) {
					$guestsAttendeeIDs[] = $nwAttendee['ID'];
				}
			}
		}

		//\ClickBlocks\Debug::ErrorLog( print_r( $witnesses, TRUE ) );
		foreach( $witnesses as $witness ) {
			foreach( $filesInfo as $fileInfo ) {
				//\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
				$shareObjects[] = $addShare( null, $witness['ID'], $fileInfo['ID'], null, $mainDeposition->ID );
			}
		}

		//\ClickBlocks\Debug::ErrorLog( __FUNCTION__ . ' Line: ' . __LINE__ );

		$ccShares = $this->shareCourtesyCopy( $mainDeposition->ID, $sourceFileID, $p['name'], $guestsAttendeeIDs );
		$shareObjects = array_merge( $shareObjects, $ccShares );

		// For the Web App, if this is a temp file, we will delete it after we have introduced it.
		if( $isTempFile ) {
			$sourceFile = new \ClickBlocks\DB\Files( $p['sourceFileID'] );
			if( $sourceFile && $sourceFile->folders[0]->class == 'Personal' && strpos( $sourceFile->name, 'tmp_' ) === 0 ) {
				\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- deleting temp file: ' . $sourceFile->name );
				$sourceFile->delete();
			}
		}

		// add result to Nodejs
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- buildShareFilesReturnNodeJS' );
		$args = $this->buildShareFilesReturnNodeJS( $shareObjects, $mainDeposition, $folderName, $fileName, TRUE );
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- args: ' . print_r( $args, true ) );
//		$this->return['shares'] = $this->buildShareFilesReturn( $shareObjects );
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sendPostCommand' );
		$this->getNodeJS()->sendPostCommand( 'introduced_exhibit', NULL, $args );
		//$this->return['argsToNodeJS'] = $args;
	}

	public function overwriteExhibit( $p )
	{
		$this->validateParams( array(
			'depositionID' => array( 'type'=>self::TYPE_SCALAR, 'req'=>TRUE ),
			'fileID' => array( 'type'=>self::TYPE_SCALAR, 'req'=>TRUE ),
			'sourceFileID' => array( 'type'=>self::TYPE_SCALAR, 'req'=>FALSE ),
			'name' => array( 'type' => self::TYPE_STRING, 'req'=>FALSE ),
			'file' => array( 'type' => self::TYPE_FILE, 'req'=>FALSE ),
			'isExhibit' => array( 'type' => self::TYPE_BOOLEAN, 'req'=>FALSE ),
			'exhibitTitle' => array( 'type'=>self::TYPE_STRING, 'req'=>FALSE ),
			'exhibitSubTitle' => array( 'type'=>self::TYPE_STRING, 'req'=>FALSE ),
			'exhibitXOrigin' => array( 'type'=>self::TYPE_NUMBER, 'req'=>FALSE ),
			'exhibitYOrigin' => array( 'type'=>self::TYPE_NUMBER, 'req'=>FALSE ),
			'overwrite' => array( 'type'=>self::TYPE_BOOLEAN, 'req'=>FALSE ),
			'tempSourceID' => array( 'type'=>self::TYPE_SCALAR, 'req'=>FALSE),
			'isMultimedia' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE]
		));

		$depositionID = (int)$p['depositionID'];
		$fileID = (int)$p['fileID'];
		$isMultimedia = (bool)$p['isMultimedia'];
		$tempSourceID = NULL;
		$isTempFile = FALSE;
		if( isset( $p['tempSourceID'] ) && (int)$p['tempSourceID'] ) {
			$isTempFile = TRUE;
			$tempSourceID = (int)$p['tempSourceID'];
		}

		$inDeposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$inDeposition || !$inDeposition->ID || $inDeposition->ID != $depositionID ) {
			$this->except( "Invalid Deposition ID: {$depositionID}", 1000 );
		}

		$rootDeposition = ($inDeposition->parentID) ? $inDeposition->parentDepositions1[0] : $inDeposition;
		if( !$rootDeposition->ID ) {
			$this->except( "Invalid Deposition ID: {$inDeposition->parentID}", 1001 );
		}

		$childDepoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $rootDeposition->ID );
		$childDeposition = ($childDepoID) ? new \ClickBlocks\DB\Depositions( $childDepoID ) : FALSE;
		if( $childDeposition && $childDeposition->ID ) {
			//if the user attends a child deposition, inDeposition is expected to be the child deposition
			$inDeposition = $childDeposition;
		}

		$inFile = new \ClickBlocks\DB\Files( $fileID );
		if( !$inFile || !$inFile->ID || $inFile->ID != $fileID ) {
			$this->except( "Invalid File ID: {$fileID}", 1002 );
		}

		$rootFile = NULL;

		//working from a child deposition?
		if( $inDeposition->ID !== $rootDeposition->ID ) {
			//child deposition
			$rootFileID = $this->getOrchestraFiles()->getFileIDByName( $rootDeposition->ID, $p['name'] );
			if( $rootFileID ) {
				$rootFile = new \ClickBlocks\DB\Files( $rootFileID );
				if( !$rootFile || !$rootFile->ID || $rootFile->ID != $rootFileID ) {
					$this->except( "Invalid File ID: {$rootFileID}", 1003 );
				}
			} else {
				$this->except( "Unable to find file by name '{$p['name']}' for deposition '{$rootDeposition->ID}'" );
			}
		} else {
			//root deposition
			$rootFile = $inFile;
		}

		if( !is_object( $rootFile ) || !isset( $rootFile->ID ) || !$rootFile->ID ) {
			$this->except( "Invalid File ID: {$fileID}", 1004 );
		}

		$rootFolder = new \ClickBlocks\DB\Folders( $rootFile->folderID );
		if( !$rootFolder || !$rootFolder->ID || $rootFolder->ID != $rootFile->folderID ) {
			$this->except( "Invalid Folder ID: {$rootFile->folderID}", 1005 );
		}

		//\ClickBlocks\Debug::ErrorLog( "fileID: {$fileID}/{$rootFile->ID}, fileName: {$p['name']}/{$rootFile->name}" );
		//\ClickBlocks\Debug::ErrorLog( print_r( $_FILES, TRUE ) );
		
		$exhibitSrcID = (int)$p['sourceFileID'];

		if( !is_array( $_FILES ) || !isset( $_FILES['file'] ) || !is_array( $_FILES['file'] ) || !isset( $_FILES['file']['tmp_name'] ) || !isset( $_FILES['file']['error'] ) || $_FILES['file']['error'] !== UPLOAD_ERR_OK ) {
			if( !$isMultimedia ) {
				$this->except( 'Missing or invalid file for upload', 1006 );
			}
			if( !$exhibitSrcID ) {
				$this->except( 'Missing or invalid source file for introduction', 1007 );
			}
		}

		$bllFile = new \ClickBlocks\DB\Files( $rootFile->ID );
		$rootFileFullPath = realpath( $bllFile->getFullName( FALSE ) );

		$srcUploaded = FALSE;
		$pathInfo = pathinfo( $rootFileFullPath );
		$srcFullPath = NULL;
		if( isset( $_FILES['file']['tmp_name'] ) ) {
			$srcFullPath = realpath( $_FILES['file']['tmp_name'] );
			$srcUploaded = TRUE;
		} else {
			$srcFile = new \ClickBlocks\DB\Files( $exhibitSrcID );
			if( !$srcFile || !$srcFile->ID || $srcFile->ID != $exhibitSrcID ) {
				$this->except( 'Invalid source file for introduction', 1008 );
			}
			$srcFullPath = realpath( $srcFile->getFullName( FALSE ) );
		}
		if( !$srcFullPath || !file_exists( $srcFullPath ) ) {
			$this->except( 'Invalid source file for introduction', 1009 );
		}
		$mimeType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $srcFullPath );
		$exhibitFileName = $pathInfo['filename'];
		$extension = '.';
		if( stripos( $mimeType, 'audio' ) !== FALSE ) {
			$extension .= 'mp3';
		} elseif( stripos( $mimeType, 'video' ) !== FALSE ) {
			$extension .= 'mp4';
		} else {
			$extension .= 'pdf';
		}
		$exhibitFileName .= $extension;
		$exhibitFullPath = realpath( $pathInfo['dirname'] ) . DIRECTORY_SEPARATOR . $exhibitFileName;
//		\ClickBlocks\Debug::ErrorLog( print_r( [$srcFullPath, $exhibitFullPath, $rootFileFullPath, $mimeType, $extension], TRUE ) );

		//root deposition
		$this->saveExhibitStampMetadata( $p, $rootDeposition );
		
		$now = date( 'Y-m-d H:i:s' );
		$rootFile->setSourceFile( $srcFullPath, TRUE );
		$rootFile->name = $exhibitFileName;
		$rootFile->created = $now;
		$rootFile->sourceID = ($isTempFile) ? $tempSourceID : $exhibitSrcID;
		$rootFile->save();

//		\ClickBlocks\Debug::ErrorLog( print_r( [$srcFullPath, $exhibitFullPath, $rootFile->getFullName()], TRUE ) );
		if( !copy( $srcFullPath, $exhibitFullPath ) ) {
			$this->except( 'Unable to copy file', 1010 );
		}

		$rootFolder->lastModified = $now;
		$rootFolder->save();

		if( $rootFileFullPath != $exhibitFullPath && file_exists( $rootFileFullPath ) ) {
//			\ClickBlocks\Debug::ErrorLog( "(parent) Removing: {$rootFileFullPath}" );
			unlink( $rootFileFullPath );
		}

		//ED-631; Exhibit History -- root deposition
		$introducedDate = date( 'Y-m-d H:i:s' );
		$introducedBy = "{$this->user->firstName} {$this->user->lastName}";
		$srcFile = new \ClickBlocks\DB\Files( $rootFile->sourceID );
		if( !$srcFile || !$srcFile->ID || $srcFile->ID != $rootFile->sourceID ) {
			$this->except( "Invalid source for fileID: {$rootFile->sourceID}", 1018 );
		}
		$exhibitHistory = new \ClickBlocks\DB\ExhibitHistory();
		$exhibitHistory->exhibitFileID = $rootFile->ID;
		$exhibitHistory->caseID = $rootDeposition->caseID;
		$exhibitHistory->depositionID = $rootDeposition->ID;
		$exhibitHistory->introducedDate = $introducedDate;
		$exhibitHistory->introducedBy = $introducedBy;
		$hasAccess = $rootFile->checkUserAccess( $this->user->ID );
		$exhibitHistory->sourceFileID = ($srcFile->ID && $hasAccess) ? $srcFile->ID : NULL;
		$exhibitHistory->insert();

		$foldersByDepo = array();
		$filesByDepo = array();

		$foldersByDepo[$rootDeposition->ID] = $rootFolder;
		$filesByDepo[$rootDeposition->ID] = $rootFile;

		//child depositions
		$childDepoIDs = $this->getOrchestraDepositions()->getChildDepositionIDs( $rootDeposition->ID );

		\ClickBlocks\Debug::ErrorLog( 'childDepoIDs: '. print_r( $childDepoIDs, TRUE ) );
		if( $childDepoIDs && is_array( $childDepoIDs ) ) {
			foreach( $childDepoIDs as $cDepoID ) {
				//\ClickBlocks\Debug::ErrorLog( 'childDepoID: '. print_r( $cDepoID, TRUE ) );
				$childFile = NULL;
				$childFileID = $this->getOrchestraFiles()->getFileIDByName( $cDepoID, $pathInfo['basename'] );
				if( $childFileID ) {
					//\ClickBlocks\Debug::ErrorLog( 'childFileID: '. print_r( $childFileID, TRUE ) );
					$childFile = new \ClickBlocks\DB\Files( $childFileID );
				} else {
					$this->except( "Unable to find file by name '{$pathInfo['basename']}' for deposition '{$cDepoID}'" );
				}
				if( !$childFile || !$childFile->ID || $childFile->ID != $childFileID ) {
					$this->except( "Invalid File ID '{$childFileID}'", 1011 );
				}
				$childFileFullPath = realpath( $childFile->getFullName( FALSE ) );
				$cPathInfo = pathinfo( $childFileFullPath );
				$childExhibitPath = realpath( $cPathInfo['dirname'] ) . DIRECTORY_SEPARATOR . $exhibitFileName;
				$childFile->setSourceFile( $exhibitFullPath, TRUE );
				$childFile->name = $exhibitFileName;
				$childFile->created = $now;
				$childFile->save();
//				\ClickBlocks\Debug::ErrorLog( "(child) copy file: {$exhibitFullPath} -> {$childExhibitPath}" );
				if( !copy( $exhibitFullPath, $childExhibitPath ) ) {
					$this->except( "Error copying file: {$exhibitFullPath} to {$childExhibitPath}", 1012 );
				}
				if( stripos( $childFileFullPath, $exhibitFileName ) === FALSE && file_exists( $childFileFullPath ) ) {
//					\ClickBlocks\Debug::ErrorLog( "(child) Removing: {$childFileFullPath}" );
					unlink( $childFileFullPath );
				}
				$childFolder = new \ClickBlocks\DB\Folders( $childFile->folderID );
				if( !$childFolder || !$childFolder->ID || $childFolder->ID != $childFile->folderID ) {
					$this->except( "Invalid Folder ID '{$childFile->folderID}'", 1013 );
				}
				$childFolder->lastModified = $now;
				$childFolder->save();
				
				$foldersByDepo[$cDepoID] = $childFolder;
				$filesByDepo[$cDepoID] = $childFile;

				//ED-631; Exhibit History -- child deposition
				$childDepo = new \ClickBlocks\DB\Depositions( $cDepoID );
				$exhibitHistory = new \ClickBlocks\DB\ExhibitHistory();
				$exhibitHistory->exhibitFileID = $childFile->ID;
				$exhibitHistory->caseID = $childDepo->caseID;
				$exhibitHistory->depositionID = $childDepo->ID;
				$exhibitHistory->introducedDate = $introducedDate;
				$exhibitHistory->introducedBy = $introducedBy;
				$hasAccess = $childFile->checkUserAccess( $this->user->ID );
				$exhibitHistory->sourceFileID = ($srcFile->ID && $hasAccess) ? $srcFile->ID : NULL;
				$exhibitHistory->insert();
			}
		}

		//Courtesy Copy
		$ccSourceFileID = ( isset( $p['sourceFileID'] ) && $p['sourceFileID'] ) ? $p['sourceFileID'] : $p['fileID'];
		$ccSourceFileID = $this->getOrchestraFiles()->getSourceFileID( $ccSourceFileID );
		$ccFileInfo = pathinfo( $rootFileFullPath );
		$ccFileName = "{$ccFileInfo['filename']}_Courtesy.{$ccFileInfo['extension']}";
		$sourceFile = new \ClickBlocks\DB\Files( $ccSourceFileID );
		if( !$sourceFile || !$sourceFile->ID || $sourceFile->ID != $ccSourceFileID ) {
			$this->except( "Invalid source file ID '{$ccSourceFileID}'", 1014 );
		}
		$ccFileID = $this->getOrchestraFiles()->getFileIDByName( $rootDeposition->ID, $ccFileName );
		//\ClickBlocks\Debug::ErrorLog( "courtesy copy fileID: {$ccFileID}" );
		if( !$ccFileID ) {
			$this->except( "Unable to find file by name '{$ccFileName}' for deposition '{$rootDeposition->ID}'" );
		}
		$ccFile = new \ClickBlocks\DB\Files( $ccFileID );
		if( !$ccFile || !$ccFile->ID || $ccFile->ID != $ccFileID ) {
			$this->except( "Invalid File ID '{$ccFileID}'", 1015 );
		}
		//update info to new exhibit/source
		$eFileInfo = pathinfo( $exhibitFullPath );
		$ccFileName = "{$eFileInfo['filename']}_Courtesy.{$eFileInfo['extension']}";
		//\ClickBlocks\Debug::ErrorLog( "courtesy copy file: {$ccFile->ID}: {$ccFile->name}" );
		$ccFileFullPath = realpath( $ccFile->getFullName( FALSE ) );
		$sourceFileFullPath = realpath( $sourceFile->getFullName( FALSE ) );
		$ccPathInfo = pathinfo( $ccFileFullPath );
		$ccNewPath = realpath( $ccPathInfo['dirname'] ) . DIRECTORY_SEPARATOR . $ccFileName;
		$ccFile->setSourceFile( $sourceFileFullPath, TRUE );
		$ccFile->name = $ccFileName;
		$ccFile->created = $now;
		$ccFile->save();
//		\ClickBlocks\Debug::ErrorLog( "(cc) copy file: {$sourceFileFullPath} -> {$ccNewPath}" );
		if( !copy( $sourceFileFullPath, $ccNewPath ) ) {
			$this->except( "Error copying file: {$sourceFileFullPath} to {$ccNewPath}", 1016 );
		}
		if( stripos( $ccFileFullPath, $ccFileName ) === FALSE && file_exists( $ccFileFullPath ) ) {
//			\ClickBlocks\Debug::ErrorLog( "(cc) Removing: {$ccFileFullPath}" );
			unlink( $ccFileFullPath );
		}
		$ccFolder = new \ClickBlocks\DB\Folders( $ccFile->folderID );
		if( !$ccFolder || !$ccFolder->ID || $ccFolder->ID != $ccFile->folderID ) {
			$this->except( "Invalid Folder ID '{$ccFile->folderID}'", 1017 );
		}
		$ccFolder->lastModified = $now;
		$ccFolder->save();

		$attendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo( $rootDeposition->ID );
		//\ClickBlocks\Debug::ErrorLog( print_r( $attendees, TRUE ) );

		//FileShares
		$receivers = array();
		$shares = array();
		if( $attendees && is_array( $attendees ) ) {
			// Prepare fileinfo pointer to extract mimetypes from files
			$finfo = finfo_open( FILEINFO_MIME_TYPE );

			foreach( $attendees as $attendee ) {
				$attDepoID = $attendee['depositionID'];
				$UIDCol = NULL;
				$attUID = NULL;
				$isGuest = FALSE;
				if( isset( $attendee['userID'] ) ) {
					$UIDCol = 'userID';
					$attUID = $attendee['userID'];
				} elseif( isset( $attendee['ID'] ) ) {
					$UIDCol = 'guestID';
					$attUID = $attendee['ID'];
					$isGuest = TRUE;
				}
				$childFolder = $foldersByDepo[$attDepoID];
				$childFile = $filesByDepo[$attDepoID];
				$recFiles = [];
				$recFolder = [];
				$exhibitHistory = $this->getOrchestraExhibitHistory()->exhibitHistoryForExhibit( $childFile->ID );
				//\ClickBlocks\Debug::ErrorLog( "userType: {$attendee['role']}" );
				if( !in_array( $attendee['role'], ['G'] ) ) {
					//\ClickBlocks\Debug::ErrorLog( "fileShares for 'Official Exhibits'" );
					$shareID = $this->getOrchestra( 'FileShares' )->getFileShareIDForFile( $childFile->ID, $attUID, TRUE, $isGuest );
					//\ClickBlocks\Debug::ErrorLog( "M/W shareID: {$shareID} childFile->ID: {$childFile->ID} attUID: {$attUID}, TRUE, isGuest: {$isGuest}" );
					if( $shareID ) {
						$shares[$shareID] = array( 'ID'=>$shareID, 'fileID'=>$childFile->ID, $UIDCol=>$attUID );
					}
					$shareID = $this->getOrchestra( 'FileShares' )->getFileShareIDForFile( $childFile->ID, $attUID, FALSE, $isGuest );
					//\ClickBlocks\Debug::ErrorLog( "M/W shareID: {$shareID} childFile->ID: {$childFile->ID} attUID: {$attUID}, FALSE, isGuest: {$isGuest}" );
					if( $shareID ) {
						$shares[$shareID] = array( 'ID'=>$shareID, 'fileID'=>$childFile->ID, $UIDCol=>$attUID );
					}

					$recFiles[] = [
						'ID' => $childFile->ID,
						'isExhibit' => TRUE,
						'name' => $childFile->name,
						'shareID' => $shareID,
						'created' => $now,
						'mimetype' => finfo_file( $finfo, $childFile->getFullName() ),
						'exhibitHistory' => $exhibitHistory
					];
					$recFolder = array( 'ID'=>$childFolder->ID, 'folderName'=>$childFolder->name, 'depositionID'=>$attDepoID, 'created'=>$now, 'lastModified'=>$now );
					//\ClickBlocks\Debug::ErrorLog( print_r( $recFiles, TRUE ) );
					//\ClickBlocks\Debug::ErrorLog( print_r( $recFolder, TRUE ) );
				} else {
					//\ClickBlocks\Debug::ErrorLog( "fileShares for 'Courtesy Copy'" );
					$shareID = $this->getOrchestra( 'FileShares' )->getFileShareIDForFile( $ccFile->ID, $attUID, TRUE, $isGuest );
					//\ClickBlocks\Debug::ErrorLog( "G shareID: {$shareID} ccFile->ID: {$ccFile->ID} attUID: {$attUID}, TRUE, isGuest: {$isGuest}" );
					if( $shareID ) $shares[$shareID] = array( 'ID'=>$shareID, 'fileID'=>$ccFile->ID, $UIDCol=>$attUID );
					$shareID = $this->getOrchestra( 'FileShares' )->getFileShareIDForFile( $ccFile->ID, $attUID, FALSE, $isGuest );
					//\ClickBlocks\Debug::ErrorLog( "G shareID: {$shareID} ccFile->ID: {$ccFile->ID} attUID: {$attUID}, FALSE, isGuest: {$isGuest}" );
					if( $shareID ) $shares[$shareID] = array( 'ID'=>$shareID, 'fileID'=>$ccFile->ID, $UIDCol=>$attUID );
					$recFiles[] = [
						'ID' => $ccFile->ID,
						'isExhibit' => TRUE,
						'name' => $ccFile->name,
						'shareID' => $shareID,
						'created' => $now,
						'mimetype' => finfo_file( $finfo, $ccFile->getFullName() ),
						'exhibitHistory' => $exhibitHistory
					];
					$recFolder = array( 'ID'=>$ccFolder->ID, 'folderName'=>$ccFolder->name, 'depositionID'=>$attDepoID, 'created'=>$now, 'lastModified'=>$now );
				}
				$receivers[] = array( 'files'=>$recFiles, $UIDCol=>(int)$attUID, 'folder'=>$recFolder );
				if( $attUID == $this->user->ID ) {
					$this->return['introducedFile'] = $childFile->getValues();
				}
			}

			finfo_close( $finfo );
		}
		//sort by shareID
		ksort( $shares );
		foreach( $shares as $share ) {
			$this->return['shares'][] = $share;
		}

		$args = array( 'sharedBy'=>$this->user->ID, 'depositionID'=>$rootDeposition->ID, 'receivers'=>$receivers );
		//$this->return['argsToNodeJS'] = $args;
		$this->getNodeJS()->sendPostCommand( 'introduced_exhibit', NULL, $args );

		// For the Web App, if this is a temp file, we will delete it after we have introduced it.
		if( $isTempFile ) {
			$sourceFile = new \ClickBlocks\DB\Files( $p['sourceFileID'] );
			if ( $sourceFile && $sourceFile->folders->class == 'Personal' && strpos( $sourceFile->name, 'tmp_' ) === 0 ) {
				\ClickBlocks\Debug::ErrorLog( 'api_shareFiles (overwriteExhibit) -- deleting temp file: ' . $sourceFile->name );
				$sourceFile->delete();
			}
		}
	}

	public function saveExhibitStampMetadata( $p, \ClickBlocks\DB\Depositions $rootDeposition )
	{
		$this->validateParams( array(
			'exhibitTitle' => array( 'type'=>self::TYPE_STRING, 'req'=>FALSE ),
			'exhibitSubTitle' => array( 'type'=>self::TYPE_STRING, 'req'=>FALSE ),
			'exhibitXOrigin' => array( 'type'=>self::TYPE_NUMBER, 'req'=>FALSE ),
			'exhibitYOrigin' => array( 'type'=>self::TYPE_NUMBER, 'req'=>FALSE )
		));

		$split = function( &$exhibitText, &$number ) {
			if( mb_ereg( '[0-9]+$', $exhibitText, $match ) ) {
				$exhibitText = mb_substr( $exhibitText, 0, -mb_strlen( $match[0] ) );
				$number = (int)( $match[0] );
			} else {
				$number = 0;
			}
		};

		$mb_equal = function( $first, $second ) {
			$lenFirst = mb_strlen( $first );
			$lenSecond = mb_strlen( $second );
			// Do a zero length test first because mb_strpos does not like checking with empty strings.
			if( $lenFirst === 0 && $lenSecond === 0) {
				return true;
			}
			if( $lenFirst === $lenSecond && mb_strpos( $first, $second ) === 0 ) {
				return true;
			}
			return false;
		};

		$newTitleText = $p['exhibitTitle'];
		$oldTitleText = $rootDeposition->exhibitTitle;
		$newSubTitleText = $p['exhibitSubTitle'];
		$oldSubTitleText = $rootDeposition->exhibitSubTitle;
		$newInt = 0;
		$oldInt = 0;

		// Split out the number from the sub-title or from the title.
		if( mb_strlen( $newSubTitleText ) ) {
			$split( $newSubTitleText, $newInt );
			$split( $oldSubTitleText, $oldInt );
		} else {
			$split( $newTitleText, $newInt );
			$split( $oldTitleText, $oldInt );
		}

		//If the titles are the same and the new number less than or equal to the old number, we don't update the meta data because the user did an overwrite and we
		//leave the counter at it's old value.
		if( $mb_equal( $oldTitleText, $newTitleText ) && $mb_equal( $oldSubTitleText, $newSubTitleText ) && $newInt <= $oldInt ) {
			\ClickBlocks\Debug::ErrorLog( 'The exhibit stamp meta data number is lower than the last number, so do not save the meta data.  Old: ' . $oldInt . ' New: ' . $newInt );
			return;
		}
		$rootDeposition->exhibitTitle = $p['exhibitTitle'];
		$rootDeposition->exhibitSubTitle = $p['exhibitSubTitle'];
//		$rootDeposition->exhibitXOrigin = number_format( $p['exhibitXOrigin'], 8 );
//		$rootDeposition->exhibitYOrigin = number_format( $p['exhibitYOrigin'], 8 );
		$rootDeposition->save();

		$childDepositions = $this->getOrchestraDepositions()->getChildDepositionIDs( $rootDeposition->ID );
		if( is_array( $childDepositions ) ) {
			foreach( $childDepositions as $childID ) {
				$childDeposition = $this->getService( 'Depositions' )->getByID( $childID );
				$childDeposition->exhibitTitle = $rootDeposition->exhibitTitle;
				$childDeposition->exhibitSubTitle = $rootDeposition->exhibitSubTitle;
//				$childDeposition->exhibitXOrigin = $rootDeposition->exhibitXOrigin;
//				$childDeposition->exhibitYOrigin = $rootDeposition->exhibitYOrigin;
				$childDeposition->save();
			}
		}
	}

	/**
	 *
	 * @param bigint $depositionID
	 * @param bigint $fileID
	 * @param string $filename
	 */
	public function shareCourtesyCopy( $depositionID, $fileID, $fileName, $guests, $overwrite=FALSE )
	{
		$depositionID = (int)$depositionID;
		$fileID = (int)$fileID;
		$shares = [];
		//\ClickBlocks\Debug::ErrorLog( 'shareCourtesyCopy ---' );
		//\ClickBlocks\Debug::ErrorLog( "depositionID: {$depositionID} fileID: {$fileID}" );

		$deposition = $this->getService( 'Depositions' )->getByID( $depositionID );
		$mainDeposition = ($deposition->parentID ? $deposition->parentDepositions1[0] : $deposition);
		//\ClickBlocks\Debug::ErrorLog( "mainDepositionID: {$mainDeposition->ID}" );

		//courtesy copy folder -- create if not exists
		$ccFolderInfo = $this->getOrchestraFolders()->getCourtesyCopyFolder( $mainDeposition->ID );
		$ccFolder = new \ClickBlocks\DB\Folders();
		if( !$ccFolderInfo ) {
			\ClickBlocks\Debug::ErrorLog( 'shareCourtesyCopy -- creating "' . $this->config->logic['courtesyCopyFolderName'] . '" folder' );
			$ccFolder->name = $this->config->logic['courtesyCopyFolderName'];
//			$ccFolder->isCourtesyCopy = 1;
			$ccFolder->class = 'CourtesyCopy';
			$ccFolder->createdBy = $mainDeposition->createdBy;
			$ccFolder->depositionID = $mainDeposition->ID;
			$ccFolder->insert();
		} else {
			$ccFolder->assignByID( $ccFolderInfo['ID'] );
		}
		$ccFolder->lastModified = date( 'Y-m-d H:i:s' );
		$ccFolder->update();
		//\ClickBlocks\Debug::ErrorLog( "ccFolderID: {$ccFolderID}" );

		//find source file -- copy to "Courtesy Copy"
		$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $fileID );
		//\ClickBlocks\Debug::ErrorLog( 'sourceFileID: ' . print_r( $sourceFileID, TRUE ) );
		$sourceFile = $this->getService( 'Files' )->getByID( $sourceFileID );
		$fileInfo = pathinfo( $fileName );
		$ccFileName = "{$fileInfo['filename']}_Courtesy.{$fileInfo['extension']}";
		//$overwriteFileID = $this->getOrchestraFiles()->getFileIDByName( $mainDeposition->ID, $ccFileName );
		$copyFileID = $this->copyFile( $sourceFile, $ccFolder->ID, $mainDeposition->createdBy, $ccFileName, 0, 0, $sourceFileID );
		//\ClickBlocks\Debug::ErrorLog( 'copyFileID: ' . print_r( $copyFileID, TRUE ) );

		//\ClickBlocks\Debug::ErrorLog( 'Guests: ' . print_r( $guests, TRUE ) );
		if( is_array( $guests ) ) {
			foreach( $guests as $guest ) {
				\ClickBlocks\Debug::ErrorLog( 'Guest ID: ' . $guest );
				$fileShare = $this->getBLL( 'FileShares' );
				$fileShare->depositionID = $mainDeposition->ID;
				$fileShare->fileID = $copyFileID;
				$fileShare->copyFileID = NULL;
				$fileShare->createdBy = $mainDeposition->createdBy;
				$fileShare->userID = NULL;
				$fileShare->attendeeID = $guest;
				$fileShare->insert();
				$shares[] = $fileShare;
			}
		}

		//\ClickBlocks\Debug::ErrorLog( 'Shares: ' . count( $shares ) );
		//foreach( $shares as $share ) {
		//	\ClickBlocks\Debug::ErrorLog( 'Share: ' . print_r( $share->getValues(), TRUE ) );
		//}
		return $shares;
	}

	protected function prepareExhibitFile( $p )
	{
		$this->validateParams( [
			'depositionID' => ['type' => self::TYPE_SCALAR, 'req'=>TRUE],
			'fileID' => ['type' => self::TYPE_SCALAR, 'req'=>FALSE],	//only required for overwrite
			'sourceFileID' => ['type' => self::TYPE_SCALAR, 'req'=>TRUE],
			'name' => ['type' => self::TYPE_STRING, 'req'=>TRUE],
			'isExhibit' => ['type' => self::TYPE_BOOLEAN, 'req'=>TRUE],	//should never not be true, thus passes validation as required
			'exhibitTitle' => ['type'=>self::TYPE_STRING, 'req'=>FALSE],
			'exhibitSubTitle' => ['type'=>self::TYPE_STRING, 'req'=>FALSE], // Can be an empty string.
			'exhibitXOrigin' => ['type'=>self::TYPE_NUMBER, 'req'=>FALSE],	// The position can be 0.0
			'exhibitYOrigin' => ['type'=>self::TYPE_NUMBER, 'req'=>FALSE],	// The position can be 0.0
			'exhibitDate' => ['type'=>self::TYPE_STRING, 'req'=>TRUE],
			'overwrite' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE],	//cannot require a boolean, validation fails when its false
			'skipStamp' => ['type'=>self::TYPE_BOOLEAN, 'req'=>FALSE]	//cannot require a boolean, validation fails when its false
		] );

		$skipStamp = (bool)$p['skipStamp'];
		$depoID = (int)$p['depositionID'];
		$sourceFileID = (int)$p['sourceFileID'];

		//resolve deposition
		$deposition = new \ClickBlocks\DB\Depositions( $depoID );
		if( !$deposition->ID ) {
			$this->except( "Unable to find deposition by ID: {$depoID}", 1100 );
		}
		$depoID = ($deposition->parentID) ? (int)$deposition->parentID : (int)$deposition->ID;	//force depoID to root deposition
		$attendedDepoID = $this->getOrchestraDepositionAttendees()->checkUserIsDepositionAttendee( $depoID, $this->user->ID );
		if( !$attendedDepoID ) {
			$this->except( 'Unauthorized access', 1101 );
		}
		$isTrustedUser = $this->getOrchestraDepositions()->isTrustedUser( $this->user->ID, $attendedDepoID );

		//resolve source file
		$sourceFile = new \ClickBlocks\DB\Files( $sourceFileID );
		if( !$sourceFile->ID ) {
			$this->except( "Unable to find file by ID: {$sourceFileID}", 1102 );
		}
		$srcFileMimeType = finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $sourceFile->getFullName() );
		if( $srcFileMimeType != 'application/pdf' ) {
			\ClickBlocks\Debug::ErrorLog( "prepareExhibitFile; SourceFile: '{$sourceFile->name}' type: '{$srcFileMimeType}' cannot be prepared." );
			return;
		}
		$fileDepoID = (int)$sourceFile->folders->depositionID;
		if( $fileDepoID !== $attendedDepoID ) {
			$this->except( "Invalid file ID: {$sourceFileID}", 1103 );
		}
		if( !$isTrustedUser && $sourceFile->createdBy !== $this->user->ID ) {
			$this->except( "Invalid file ID: {$sourceFileID}", 1104 );
		}

		$inPath = $sourceFile->getFullName();
		$outPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.pdf';

		if( !$skipStamp ) {
			$stampPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.png';
			$imgInfo = \ClickBlocks\ImageUtils::drawExhibitStamp( $stampPath, $p['exhibitTitle'], $p['exhibitSubTitle'], $p['exhibitDate'] );
			if( !$imgInfo ) {
				$this->except( 'Error generating exhibit stamp', 1105 );
			}
			$metadata = json_encode( [
				'drawType' => 'stamp',
				'exhibitXOrigin' => (float)$p['exhibitXOrigin'],
				'exhibitYOrigin' => (float)$p['exhibitYOrigin'],
				'stampWidth' => (float)$imgInfo['width'],
				'stampHeight' => (float)$imgInfo['height'],
				'filename' => $stampPath
			], JSON_UNESCAPED_SLASHES );
			$metadataPath = $_SERVER['DOCUMENT_ROOT'] . Core\IO::url( 'temp' ) . '/' . \ClickBlocks\Utils::createUUID() . '.json';
			if( !file_put_contents( $metadataPath, $metadata ) ) {
				$this->except( 'Unable to write metadata for exhibit stamp', 1106 );
			}

			if( \ClickBlocks\PDFTools::annotate( $inPath, $metadataPath, $outPath ) ) {
				\ClickBlocks\Debug::ErrorLog( 'prepareExhibitFile -- fpdf_annotate: success' );
				if( file_exists( $metadataPath ) ) {
					unlink( $metadataPath );
				}
				if( file_exists( $stampPath ) ) {
					unlink( $stampPath );
				}
			} else {
				\ClickBlocks\Debug::ErrorLog( 'prepareExhibitFile -- fpdf_annotate: failure' );
				if( file_exists( $outPath ) ) {
					unlink( $outPath );
				}
				if( file_exists( $metadataPath ) ) {
					unlink( $metadataPath );
				}
				if( file_exists( $stampPath ) ) {
					unlink( $stampPath );
				}
				$this->except( 'Unable to perform annotations', 1107 );
			}
		} else {
			//skip stamp
			$result = copy( $inPath, $outPath );
			if( !$result ) {
				$this->except( 'Unable to copy file', 1108 );
			}
		}

		//fake the upload
		$_FILES['file'] = [
			'tmp_name' => $outPath,
			'name' => $p['name'],
			'error' => UPLOAD_ERR_OK
		];

	}

	public function api_shareFolder( $p )
	{
		$this->validateParams( [
			'depositionID'=>['type'=>self::TYPE_SCALAR, 'req'=>TRUE],
			'folderID'=>['type'=>self::TYPE_SCALAR, 'req'=>TRUE],
			'name'=>['type'=>self::TYPE_STRING, 'req'=>TRUE],
			'userIDs'=>['type'=>self::TYPE_ARRAY, 'req'=>FALSE]
		] );

		$depositionID = (int)$p['depositionID'];
		$folderID = (int)$p['folderID'];

		$targetUsers = $filesInfo = $shareObjects = $copiedFileIDs = $attendees = [];
		$origFile = $origFolder = NULL;
		$folderName = $fileName = '';

		if( !count( $p['userIDs'] ) ) {
			$this->except( $this->errors['ERR_MISSING_USER'], 1016 );
		}

		// this is deposition I logged in
		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID !=$depositionID ) {
			$this->except( 'Deposition not found', 1000 );
		}

		// this is parent (source) deposition of my deposition. by default it is equal to $deposition
		$mainDeposition = ($deposition->parentID) ? $deposition->parentDepositions1[0] : $deposition;
		$childDepoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $mainDeposition->ID );
		$childDeposition = ($childDepoID) ? new \ClickBlocks\DB\Depositions( $childDepoID ) : FALSE;
		if( $childDeposition && $childDeposition->ID ) {
			$deposition = $childDeposition;
		}

		// Only speaker of main deposition can share
		if( $mainDeposition->speakerID != $this->user->ID ) {
			$this->except( 'Access denied', 403 );
		}

		if( !$folderID ) {
			$this->except( $this->errors['ERR_MISSING_FILE'], 1025 );
		}


		$origFolder = new \ClickBlocks\DB\Folders( $folderID );
		if( !$origFolder->ID || $origFolder->depositionID != $deposition->ID ) {
			$this->except( $this->errors['ERR_NOT_EXIST_FOLDER_ID'], 1012 );
		}
		if( !$this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $folderID ) ) {
			$this->except( 'No access to given file', 1000 );
		}
		if( $origFolder->class == self::FOLDERS_EXHIBIT ) {
			$this->except( 'Cannot share Exhibit folders', 1017 );
		}
		$folderName = $origFolder->name;
		$filesInfo = $this->getOrchestraFiles()->getFilesByFolder( $folderID );
		if( !count( $filesInfo ) ) {
			$this->except( 'Cannot share empty folder', 1026 );
		}

		// Validate name parameter
		$p['name'] = trim( $p['name'] );
		if( $p['name'] ) {
			// check folder name not include special character
			if( !preg_match( '=^[^/?*;:{}\\\\]+$=', $p['name'] ) ) {
				$this->except( 'Folder Name is invalid', 1029 );
			}
			$folderName = $p['name'];
		}
		// this is trusted users from Local Depo
		$trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap( $deposition->ID );

		// Don't share Folder to trusted users
		$needShareToUser = function( $userID ) use ( &$trustedUsers )
		{
			return (!isset( $trustedUsers[$userID] ));
		};

		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );
		$sourceFileID = ( isset( $p['sourceFileID'] ) && $p['sourceFileID'] ? $p['sourceFileID'] : $p['fileID'] );
		$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $sourceFileID );
		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );

		$depoAttendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo( $mainDeposition->ID );
		$depoAttendeesByID = [];
		foreach( $depoAttendees as $attendee ) {
			$depoAttendeesByID[$attendee['ID']] = $attendee;
		}
		unset( $depoAttendees );

		$result = (object)['shared'=>0,'skipped'=>0,'failed'=>0];

		if( count( $p['userIDs'] ) ) {
			$depoMap = $this->getOrchestraDepositions()->getClient2DepoMapForLinkedTo( $mainDeposition->ID );
			foreach( $p['userIDs'] as $id ) {
				if( isset( $depoAttendeesByID[$id] ) && $depoAttendeesByID[$id]['role'] !== 'M' ) {
					++$result->skipped;
					\ClickBlocks\Debug::ErrorLog( "Skipping share to userID: {$id} -- user is non-member" );
					continue;	//only share to member attendees
				}
				$user = new \ClickBlocks\DB\Users( $id );
				if( !$user || !$user->ID || $user->ID != $id ) {
					++$result->failed;
					\ClickBlocks\Debug::ErrorLog( "Failed to share to userID: {$id} -- user doesn't exist" );
					continue;
				}
				if( $needShareToUser( $id ) == FALSE && ($origFolder->class === self::FOLDERS_TRUSTED && $user->clientID == $this->user->clientID) ) {
					++$result->skipped;
					\ClickBlocks\Debug::ErrorLog( "Skipping share to userID: {$id} -- no need to share folder->class:{$origFolder->class}" );
					continue; // Don't share to specific users in some cases
				}
				if( !isset( $depoMap[$user->clientID] ) ) {
					++$result->failed;
					\ClickBlocks\Debug::ErrorLog( "Cannot share to userID: {$id} -- no linked deposition for this Client not found" );
					continue;
				}
				$targetUsers[$id] = ['userID'=>$user->ID, 'depositionID'=>$depoMap[$user->clientID]];
			}
		}

		$addShare = function( $userID, $attendeeID, $fileID, $fileCopyID, $depositionID )
		{
			$share = new \ClickBlocks\DB\FileShares();
			$share->depositionID = $depositionID;
			$share->fileID = $fileID;
			$share->copyFileID = $fileCopyID;
			$share->createdBy = $this->user->ID;
			$share->userID = $userID;
			$share->attendeeID = $attendeeID;
			$share->insert();
			return $share;
		};


		// Share to Users
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sharing to specific user' );
		$orchDepo = $this->getOrchestraDepositions();
		foreach( $targetUsers as $userID=>$info ) {
			//we are copying folder, and we need to create with same name on destination user
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- copyFolder' );
			$class = ($orchDepo->isTrustedUser( $userID, $info['depositionID'] )) ? self::FOLDERS_TRUSTED : self::FOLDERS_FOLDER;
			if( $origFolder->class == self::FOLDERS_PERSONAL || $origFolder->class == self::FOLDERS_WITNESSANNOTATIONS ) {
				$class = self::FOLDERS_FOLDER;
			}
			$copiedFileIDs = $this->copyFolder( $origFolder, $p['name'], $userID, $info['depositionID'], $class );
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- start' );
			foreach( $copiedFileIDs as $origFileID => $copyFileID ) {
			   //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
			   $shareObjects[] = $addShare( $userID, NULL, $origFileID, $copyFileID, $info['depositionID'] );
			}
			++$result->shared;
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- end' );
		}

		// add result to Nodejs
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- buildShareFilesReturnNodeJS' );
		$args = $this->buildShareFilesReturnNodeJS( $shareObjects, $mainDeposition, $folderName, $fileName, FALSE );
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- args: ' . print_r( $args, true ) );
		$this->return['shares'] = $this->buildShareFilesReturn( $shareObjects );
		$this->return['result'] = $result;
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sendPostCommand' );
		$this->getNodeJS()->sendPostCommand( 'shared_folder', NULL, $args );
		//$this->return['argsToNodeJS'] = $args;
	}

	public function api_shareFile( $p )
	{
		$this->validateParams( [
			'depositionID'=>['type'=>self::TYPE_SCALAR, 'req'=>TRUE],
			'fileID'=>['type'=>self::TYPE_SCALAR, 'req'=>TRUE],
			'name'=>['type'=>self::TYPE_STRING, 'req'=>TRUE],
			'userIDs'=>['type'=>self::TYPE_ARRAY, 'req'=>FALSE]
		] );

		$depositionID = (int)$p['depositionID'];
		$fileID = (int)$p['fileID'];

		$targetUsers = $filesInfo = $shareObjects = $copiedFileIDs = $attendees = [];
		$origFile = $origFolder = NULL;
		$folderName = $fileName = '';

		if( !count( $p['userIDs'] ) ) {
			$this->except( $this->errors['ERR_MISSING_USER'], 1016 );
		}

		// this is deposition I logged in
		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID !=$depositionID ) {
			$this->except( 'Deposition not found', 1000 );
		}

		// this is parent (source) deposition of my deposition. by default it is equal to $deposition
		$mainDeposition = ($deposition->parentID) ? $deposition->parentDepositions1[0] : $deposition;
		$childDepoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $mainDeposition->ID );
		$childDeposition = ($childDepoID) ? new \ClickBlocks\DB\Depositions( $childDepoID ) : FALSE;
		if( $childDeposition && $childDeposition->ID ) {
			$deposition = $childDeposition;
		}

		// Only speaker of main deposition can share
		if( $mainDeposition->speakerID != $this->user->ID ) {
			$this->except( 'Access denied', 403 );
		}

		if( !$fileID ) {
			$this->except( $this->errors['ERR_MISSING_FILE'], 1025 );
		}


		$origFile = new \ClickBlocks\DB\Files( $fileID );
		if( !$origFile || !$origFile->ID || $origFile->ID != $p['fileID'] ) {
			$this->except( $this->errors['ERR_NOT_EXIST_FILE_ID'], 1013 );
		}
		if( $origFile->folders[0]->depositionID != $deposition->ID ) {
			$this->except( $this->errors['ERR_NOT_EXIST_FILE_ID'], 1013 );
		}
		if( !$this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $origFile->folderID ) ) {
			$this->except( 'No access to given file', 1000 );
		}
		$fileName = $origFile->name;
		$filesInfo[] = ['ID'=>$origFile->ID, 'name'=>$file->name];
		$folderName = $this->config->logic['personalFolderName'];

		// Validate name parameter
		$p['name'] = trim( $p['name'] );
		if( $p['name'] ) {
			// check file name not include special character and has a dot
			if( !preg_match( '=^[^/?*;:{}\\\\]+\.[^/?*;:{}\\\\]+$=', $p['name'] ) ) {
				$this->except( 'File Name is invalid', 1021 );
			}
			$fileName = $p['name'];
		}

		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );
		$sourceFileID = ( isset( $p['sourceFileID'] ) && $p['sourceFileID'] ? $p['sourceFileID'] : $p['fileID'] );
		$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $sourceFileID );
		\ClickBlocks\Debug::ErrorLog( __LINE__ . ' -- sourceFileID: ' . $sourceFileID );

		$depoAttendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo( $mainDeposition->ID );
		$depoAttendeesByID = [];
		foreach( $depoAttendees as $attendee ) {
			$depoAttendeesByID[$attendee['ID']] = $attendee;
		}
		unset( $depoAttendees );

		$result = (object)['shared'=>0,'skipped'=>0,'failed'=>0];

		if( count( $p['userIDs'] ) ) {
			$depoMap = $this->getOrchestraDepositions()->getClient2DepoMapForLinkedTo( $mainDeposition->ID );
			foreach( $p['userIDs'] as $id ) {
				if( isset( $depoAttendeesByID[$id] ) && $depoAttendeesByID[$id]['role'] !== 'M' ) {
					++$result->skipped;
					\ClickBlocks\Debug::ErrorLog( "Skipping share to userID: {$id} -- user is non-member" );
					continue;	//only share to member attendees
				}
				$user = new \ClickBlocks\DB\Users( $id );
				if( !$user || !$user->ID || $user->ID != $id ) {
					++$result->failed;
					\ClickBlocks\Debug::ErrorLog( "Failed to share to userID: {$id} -- user doesn't exist" );
					continue;
				}
				if( !isset( $depoMap[$user->clientID] ) ) {
					++$result->failed;
					\ClickBlocks\Debug::ErrorLog( "Cannot share to userID: {$id} -- no linked deposition for this Client not found" );
					continue;
				}
				$targetUsers[$id] = ['userID'=>$user->ID, 'depositionID'=>$depoMap[$user->clientID]];
			}
		}

		$addShare = function( $userID, $attendeeID, $fileID, $fileCopyID, $depositionID )
		{
			$share = new \ClickBlocks\DB\FileShares();
			$share->depositionID = $depositionID;
			$share->fileID = $fileID;
			$share->copyFileID = $fileCopyID;
			$share->createdBy = $this->user->ID;
			$share->userID = $userID;
			$share->attendeeID = $attendeeID;
			$share->insert();
			return $share;
		};


		// Share to Users
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sharing to specific user' );
		foreach( $targetUsers as $userID=>$info ) {
			// copying one file to Personal folder
			$folderID = $this->getOrchestraFolders()->getPersonalFolder( $info['depositionID'], $userID )['ID'];
			if( !$folderID ) {
				$folderID = $this->createFolder( $this->config->logic['personalFolderName'], $userID, $info['depositionID'], self::FOLDERS_PERSONAL );
			}
			$copiedFileIDs[$origFile->ID] = $this->copyFile( $origFile, $folderID, $userID, $fileName, 0, 1, $sourceFileID );
			$this->getOrchestraFolders()->setLastModified( $folderID );
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- start' );
			foreach( $copiedFileIDs as $origFileID => $copyFileID ) {
			   //\ClickBlocks\Debug::ErrorLog( 'addShare: ' . __LINE__ );
			   $shareObjects[] = $addShare( $userID, NULL, $origFileID, $copyFileID, $info['depositionID'] );
			}
			++$result->shared;
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- end' );
		}

		// add result to Nodejs
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- buildShareFilesReturnNodeJS' );
		$args = $this->buildShareFilesReturnNodeJS( $shareObjects, $mainDeposition, $folderName, $fileName, FALSE );
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- args: ' . print_r( $args, true ) );
		$this->return['shares'] = $this->buildShareFilesReturn( $shareObjects );
		$this->return['result'] = $result;
		//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sendPostCommand' );
		$this->getNodeJS()->sendPostCommand( 'shared_file', NULL, $args );
		//$this->return['argsToNodeJS'] = $args;
	}

	public function api_getAttendees($p)
  {
     $this->requireParams(array('depositionID'));
     return $this->getOrchestraDepositionAttendees()->getAttendeesWithFlags($p['depositionID']);
  }

  public function api_abortShare($p)
  {
	  //TODO: deprecate
     $this->validateParams(array(
              'depositionID' => array('type' => self::TYPE_NUMBER, 'req'=>true),
              'shareIDs' => array('type' => self::TYPE_ARRAY, 'req'=>true),
              'folderID' => array('type' => self::TYPE_NUMBER, 'req'=>false),
              'isExhibit'=> array('type' => self::TYPE_BOOLEAN, 'req'=>false),
     ));
     $depo = $this->getService('Depositions')->getByID($p['depositionID']);
     if (!$depo->ID) $this->except ('Deposition not found', 404);
     $svc = $this->getService('FileShares');
     $svF = $this->getService('Files');
     $recievers = array();
     $affectedFolders = array();
     $shares = array();
     // First gather information from shares before delete
     foreach ($p['shareIDs'] as $shareID)
     {
        $share = $svc->getByID($shareID);
        if (!$share->ID)  continue;
        // if ($share->depositionID != $p['depositionID']) continue; ???
        if ($share->attendeeID) {
           $recievers['G'.$share->attendeeID]['files'][] = array('ID'=>$share->fileID, 'shareID'=>$shareID); // guest should stop downloading original file
        } else {
           $recievers[$share->userID]['files'][] = array('ID'=>($share->copyFileID ?: $share->fileID), 'shareID'=>$shareID); // user will stop downloading file copy
        }
        $shares[] = $share;
     }
     // Perform abort
     foreach ($shares as $share) {
        // Delete copied file and share object from server
        if ($share->copyFileID) {
           $copyFile = $svF->getByID($share->copyFileID);
           $affectedFolders[$copyFile->folderID] = true;
           $copyFile->delete(); // when File is deleted, Share is also deleted by foreign key
        } else {
           $share->delete();
        }
     }
     $svFo = $this->getService('Folders');
     // Delete empty folders
     foreach ($affectedFolders as $folderID=>$v) {
        $folder = $svFo->getByID($folderID);
        if ($folder->class == 'Personal') continue; // Don't delete empty Personal folders
        if ($folder->class == 'Exhibit' && $folder->name == $this->config->logic['exhibitFolderName']) continue; // never delete main exhibit folder
        if ($folder->files->count() == 0) $folder->delete();
     }
     // Unshare folder as exhibit
     if ($p['folderID'] && $p['isExhibit']) {
        $folder = $svFo->getByID($p['folderID']);
        if ($folder->ID) {
           $folder->isExhibit = 0;
		   $folder->class = 'Folder';
           $folder->update();
        } else echo 'Folder '.$p['folderID'].' was not found!!';
     }
     $args = array('recievers'=>array());
     foreach ($recievers as $k=>$reciever) {
        if ($k[0]=='G') $reciever['guestID'] = (int)substr($k,1);
        else $reciever['userID'] = (int)$k;
        $args['recievers'][] = $reciever;
     }
     $args['depositionID'] = $depo->ID;
     $args['abortedBy'] = $this->user->ID;
     // Send command to NodeJS to stop downloading this files
     $this->getNodeJS()->sendPostCommand('abortshare', null, $args);
  }

	public function api_start($p)
	{
		$this->validateParams( ['depositionID'=>[self::TYPE_NUMBER, 'req'=>TRUE]] );
		$depositionID = (int)$p['depositionID'];
		$depo = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$depo || !$depo->ID || $depo->ID != $depositionID ) {
			$this->except( 'Session not found', 1001 );
		}
		if( $depo->ownerID != $this->user->ID ) {
			$this->except( 'Access denied', 1002 );
		}
		$depo->speakerID = $this->user->ID;
		$depo->start();
		$da = new \ClickBlocks\DB\DepositionsAuth( $depo->ID );
		if( !$da || !$da->ID || $da->ID != $depo->ID ) {
			\ClickBlocks\Debug::ErrorLog( "DepositionAuth missing for ID: {$depo->ID}" );
			$this->return['key'] = '********';
		} else {
			$this->return['key'] = $da->getPasscode();
		}
		$nodeJS = $this->getNodeJS();
		$nodeJS->sendPostCommand( 'deposition_start', NULL, ['date'=>date( 'Y-m-d H:i:s' ), 'depositionID'=>$depo->ID] );
		$nodeJS->notifyCasesUpdated( $depo->caseID, $depo->ID );
	}

	public function api_finish( $p )
	{
		$this->validateParams( ['depositionID'=>[self::TYPE_NUMBER, 'req'=>TRUE]] );
		$depositionID = (int)$p['depositionID'];
		$depo = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$depo || !$depo->ID || $depo->ID != $depositionID ) {
			$this->except( 'Session not found', 1003 );
		}
		if( $depo->ownerID != $this->user->ID ) {
			$this->except( 'Access denied', 403 );
		}
		if( $depo->parentID ) {
			$this->except( 'Cannot finish associated session. Owner of source session must do this.', 1004 );
		}

		if( $depo->courtReporterEmail && $depo->class == self::DEPOSITION_CLASS_DEPOSITION ) {
			// send notification if email is set
			Utils\EmailGenerator::sendCourtReporterInvite( $depo->ID );
		}
		$depo->finish();

		$nodejs = $this->getNodeJS();
		$nodejs->notifyCasesUpdated( $depo->caseID, $depo->ID );
	}

	public function api_link($p)
	{
		$this->validateParams( [
			'sourceDepositionID'  => [self::TYPE_NUMBER, 'req'=>true],
			'targetCaseID'        => [self::TYPE_NUMBER, 'req'=>false],
			'targetDepositionID'  => [self::TYPE_NUMBER, 'req'=>false],
			] );
		$sourceDepo = new \ClickBlocks\DB\Depositions( $p['sourceDepositionID'] );
		if( $sourceDepo->ID != $p['sourceDepositionID'] ) {
			$this->except( 'Source deposition not found', 1011 );
		}
		if( $sourceDepo->cases->clientID == $this->user->clientID ) {
			$this->except( 'There is no point to link deposition of your own team (client)', 1001 );
		}
		if( $sourceDepo->started == NULL ) {
			$this->except( 'Cannot link/attend, source Deposition is not started yet', 1003 );
		}
		if( $p['targetDepositionID'] ) {
			$targetDepo = new \ClickBlocks\DB\Depositions( $p['targetDepositionID'] );
			if( $targetDepo->ID != $p['targetDepositionID'] ) {
				$this->except( 'Deposition not found', 1009 );
			}
			$targetCase = new \ClickBlocks\DB\Cases( $targetDepo->caseID );
			if( $targetCase->ID != $targetDepo->caseID ) {
				$this->except( 'Case not found for deposition', 1010 );
			}
			if( $this->getOrchestraDepositions()->isTrustedUser($this->user->ID, $targetDepo->ID) == false)
				$this->except( 'Access denied for this deposition', 403 );
			if ($targetDepo->started || $targetDepo->statusID != self::DEPOSITION_STATUS_NEW ) {
				$this->except( 'Cannot link when target deposition was started', 1005 );
			}
			if( $targetDepo->parentID ) {
				$this->except( 'Target Deposition is already linked to Depo #' . $targetDepo->parentID, 1004 );
			}

			//ED-1434; Child Depo class must match parent class
			if( $sourceDepo->isDemo() && $targetDepo->class != $sourceDepo->class ) {
				$this->except( 'Session must be a demo session', 1006 );
			}
			if( !$sourceDepo->isDemo() && $targetDepo->class != $sourceDepo->class ) {
				$this->except( 'Session must not be a demo session', 1007 );
			}
			if( $sourceDepo->isDemo() ) {
				$sourceDepoStarted = strtotime( $sourceDepo->started );
				$targetDepoCreated = strtotime( $targetDepo->created );
				if( $targetDepoCreated > $sourceDepoStarted ) {
					// Update the target deposition created so it is OLDER than the source.
					// This way it will not get deleted on the parent demo deposition reset.
					$targetDepo->created = date( 'Y-m-d H:i:s', $sourceDepoStarted - 60 );
					$targetDepo->update();
				}
			}
		} else {
			$myAdminID = $this->getOrchestraUsers()->getClientAdminID( $this->user->clientID );
			if( $p['targetCaseID'] ) {
				$targetCase = new \ClickBlocks\DB\Cases( $p['targetCaseID'] );
				if( $targetCase->ID != $p['targetCaseID'] ) {
					$this->except( 'Case not found for deposition', 1010 );
				}
				if( !$sourceDepo->isDemo() && $targetCase->class == 'Demo' ) {
					$this->except( 'Invalid Case ID', 1008 );
				}
			} else {
				// CLONE case
				$targetCase = $sourceDepo->getCase()->getCopy();
				$targetCase->clientID = $this->user->clientID;
				$targetCase->createdBy = $myAdminID;
				$targetCase->created = date( 'Y-m-d H:i:s' );
				$targetCase->insert();
				$this->getService( 'CaseManagers' )->setCM( $targetCase->ID, $this->user->ID );
				$this->return['clonedCaseID'] = $targetCase->ID;
			}
			// CLONE deposition
			$targetDepo = $sourceDepo->getCopy();
			$targetDepo->caseID = $targetCase->ID;
			$targetDepo->createdBy = $targetDepo->ownerID = $this->user->ID;
			$targetDepo->created = date( 'Y-m-d H:i:s' );
			$targetDepo->insert();
			$this->return['clonedDepositionID'] = $targetDepo->ID;
			$targetDepo->uKey = $targetDepo->ID;
		}

		$targetDepo->parentID = $sourceDepo->ID;
		$targetDepo->speakerID = $sourceDepo->speakerID ? $sourceDepo->speakerID : $sourceDepo->ownerID;
		$targetDepo->started = $sourceDepo->started;
		$targetDepo->statusID = self::DEPOSITION_STATUS_IN_PROCESS;
		$targetDepo->update();

		// Copy exhibit folder to target depo
		$exhibits = $this->getOrchestraFolders()->getExhibitFoldersObjects( $sourceDepo->ID );
		foreach( $exhibits as $exhibit ) {
			if( $exhibit->class == self::FOLDERS_EXHIBIT ) {
				$targetExhibit = $this->getOrchestraFolders()->getExhibitFolderObject( $targetDepo->ID ); // "Exhibit" folder on target
			} else {
				$targetExhibit = null;
			}
			if( $targetExhibit && $targetExhibit->ID ) {
				$targetExhibit->setFilesToCopy( $exhibit )->copyFiles();
			} else {
				$exhibitCopy = $exhibit->copy( true );
				$exhibitCopy->depositionID = $targetDepo->ID;
				$exhibitCopy->createdBy = $targetDepo->getOwnerID();
				$exhibitCopy->insert();
			}
		}

	  //ED-135; Child Case/Depositions should not be charged one-time setup fees
	  $this->getOrchestra( 'InvoiceCharges' )->removeCaseDepositionSetupCharges( $targetCase->ID, $targetDepo->ID );

		//ED-631; Exhibit History
		$history = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $sourceDepo->ID );
		if( $history && is_array( $history ) ) {
			foreach( $history as $log ) {
				if( !$targetExhibit ) continue;
				$exhibitHistory = new \ClickBlocks\DB\ExhibitHistory();
				$exhibitHistory->exhibitFileID = $targetExhibit->exhibitHistoryMap[$log['exhibitFileID']];
				$exhibitHistory->sourceFileID = NULL;
				$exhibitHistory->caseID = $targetDepo->caseID;
				$exhibitHistory->depositionID = $targetDepo->ID;
				$exhibitHistory->introducedDate = $log['introducedDate'];
				$exhibitHistory->introducedBy = $log['introducedBy'];
				$exhibitHistory->insert();
			}
		}

		// try to attend
		$this->userAttendToDeposition( $this->user->ID, $targetDepo );

		$this->return['sourceDeposition'] = $sourceDepo->getValues();
		$this->return['targetDeposition'] = $targetDepo->getValues();
		$this->return['sourceDeposition']['folders'] = BLLFormat::formatRows( $this->getOrchestraFolders()->getFoldersForUser( $sourceDepo->ID, $this->user->ID ), 'folders' );
		$this->return['targetDeposition']['folders'] = BLLFormat::formatRows( $this->getOrchestraFolders()->getFoldersForUser( $targetDepo->ID, $this->user->ID ), 'folders' );
		$this->return['targetDeposition']['exhibitHistory'] = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $targetDepo->ID );
		$this->return['sourceDeposition']['liveTranscript'] = (!$targetDepo->isWitnessPrep()) ? 'Y' : 'N';
		$this->return['targetDeposition']['liveTranscript'] = (!$targetDepo->isWitnessPrep()) ? 'Y' : 'N';

		// Push info to NodeJS
		$this->getNodeJS()->sendPostCommand( 'deposition_link', NULL, ['clientID'=>$this->user->clientID, 'targetDepositionID'=>$targetDepo->ID] );
	}

	public function api_attend( $p )
	{
		$this->validateParams( ['depositionID'=>[self::TYPE_NUMBER, 'req'=>TRUE]] );
		$depositionID = (int)$p['depositionID'];
		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
			$this->except( 'Session not found', 1003 );
		}
		$depoType = $deposition->friendlyClass();

		// Check if this is a demo, and if I need to become the owner.
		if( $deposition->isDemo() && $deposition->statusID !== self::DEPOSITION_STATUS_FINISHED ) {
			$redis = new \ClickBlocks\Cache\CacheRedis( $this->config->cache['host'], $this->config->cache['port'] );
			$isOwned = $redis->get( 'demo:' . $deposition->ID );
			if( !$isOwned && $deposition->statusID == self::DEPOSITION_STATUS_NEW ) {
				if( !$deposition->setDemoOwner( $this->user->ID ) ) {
					$this->except( "Cannot attend, {$depoType} not started yet", 1002 );
				}
				$redis->set( 'demo:' . $deposition->ID, (int)$this->user->ID, (60 * 60 * 24) );
			} elseif( $deposition->statusID == self::DEPOSITION_STATUS_NEW && $deposition->getOwnerID() != $this->user->ID ) {
				$this->except( "Cannot attend, {$depoType} not started yet", 1002 );
			}
		} elseif( $deposition->started == NULL && $deposition->getOwnerID() != $this->user->ID ) {
			$this->except( "Cannot attend, {$depoType} not started yet", 1002 );
		}

		// ED-185 - Do not attempt to "attend" a finished Deposition
		if( $deposition->statusID !== self::DEPOSITION_STATUS_FINISHED ) {
			$this->userAttendToDeposition( $this->user->ID, $deposition );
		} else {
			$this->except( "Cannot attend, {$depoType} is finished.", 1003 );
		}

		$p['includeFiles'] = TRUE;
		$this->api_get( $p );
	}

  /**
   *
   * @deprecated per ED-23
   */
  public function api_buyAttendeeBundle($p)
   {
	  /*
      $this->validateParams(array(
         'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
      ));
      $depo = $this->getService('Depositions')->getByID($p['depositionID']);
      if (!$depo->ID) $this->except('Deposition not found', 1003);
      if ($depo->getOwnerID() != $this->user->ID) $this->except('Access denied', 403);
      $bundleSize = (int)$this->user->clients->attendeeBundleSize;
      if (!$bundleSize) $this->except ('This client cannot buy attendee bundle, because bundle size is 0.', 1004);
      DB\ServiceInvoiceCharges::charge($this->user->clientID, self::PRICING_OPTION_ADDITIONAL_BUNDLE, 1, $depo->caseID, $depo->ID);
      $this->return['newLimit'] = $depo->attendeeLimit = (int)$depo->attendeeLimit + $bundleSize;
      $depo->update();
      $this->getNodeJS()->sendPostCommand('attendee_limit_extended', null, array('depositionID' => $depo->ID, 'extended'=>true, 'newLimit'=>$depo->attendeeLimit));
	  */
   }

   public function api_kickAttendee($p)
   {
      $this->validateParams(array(
          'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
          'userID'  => array(self::TYPE_NUMBER, 'req'=>false),
          'guestID'  => array(self::TYPE_NUMBER, 'req'=>false),
          'ban'  => array(self::TYPE_BOOLEAN, 'req'=>false),
      ));
      $depo = $this->getService('Depositions')->getByID($p['depositionID']);
      if (!$depo->ID) $this->except('Invalid depositionID', 404);
      if ($depo->ownerID != $this->user->ID) $this->except('Access denied!', 403);
      if (!$p['userID'] && !$p['guestID']) $this->except ('UserID or GuestID required!', 201);
      if ($p['userID'] && $p['guestID']) $this->except ('Both UserID AND GuestID are not allowed!', 204);
      $args = array('depositionID' => $p['depositionID'], 'ban' => (bool)$p['ban']);
      if ($p['userID']) {
         $att = $this->getOrchestraDepositionAttendees()->getForUser($p['depositionID'], $p['userID'], true);
         $args['userID'] = $p['userID'];
      } else {
         $att = $this->getService('DepositionAttendees')->getByID($p['guestID']);
         if ($att->depositionID != $depo->ID && (!$att->depositions->parentID || $att->depositions->parentID != $depo->ID)) $this->except ('Attendee not found!', 404);
         $args['guestID'] = $p['guestID'];
      }
      //$this->return['wtf'] = $att->getValues();
      if (!$att->ID) $this->except ('Attendee not found!', 404);
      if ($p['ban']) {
         $att->banned = new DB\SQLNOWValue;
         $att->update();
      } else {
         $att->delete();
      }
      $this->getNodeJS()->sendPostCommand('attendee_kick', null, $args);
   }

	public function api_setSpeaker( $p )
	{
		$this->validateParams(
			array(
				'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
				'userID'  => array(self::TYPE_NUMBER, 'req'=>false),
			)
		);
		$depo = $this->getService('Depositions')->getByID($p['depositionID']);
		if (!$depo->ID) $this->except('Invalid depositionID', 404);
		if ($depo->parentID) $this->except('Cannot set speaker for an associated deposition. Owner of source deposition must do this.', 1004);
		if ($depo->ownerID != $this->user->ID) {
			if ($p['userID']) $this->except('Access denied! (only owner can assign speaker)', 403);
			else if ($depo->speakerID != $this->user->ID) $this->except('Access denied! (only owner and speaker can remove speaker)', 403);
		}
		if ($p['userID']) {
			$user = $this->getService('Users')->getByID($p['userID']);
			if (!$user->ID) $this->except ('User not found', 404);
			//if ($this->getOrchestraDepositions()->isTrustedUser($user->ID, $depo->ID) == false) $this->except('Cannot set non-trusted user as speaker.');
		}
		$oldSpeaker = $depo->speakerID;
		$depo->speakerID = ($p['userID']) ?: NULL;
		$newSpeakerID = ($depo->speakerID) ? $depo->speakerID : $depo->ownerID;
		$depo->speakerID = $newSpeakerID;
		$depo->update();
		$this->getOrchestraDepositions()->setSpeakerIDForChildDepositions( $depo->ID, $depo->speakerID );
		$args = array(
			'depositionID'=>$depo->ID,
			'ownerID'=>$depo->ownerID,
			'oldSpeakerID'=>$oldSpeaker,
			'newSpeakerID'=>$depo->speakerID,
			'exhibitTitle'=>$depo->exhibitTitle,
			'exhibitSubTitle'=>$depo->exhibitSubTitle,
			'exhibitXOrigin'=>$depo->exhibitXOrigin,
			'exhibitYOrigin'=>$depo->exhibitYOrigin
		);
		$this->getNodeJS()->sendPostCommand( 'deposition_setspeaker', null, $args );
	}

	public function api_key( $p )
	{
		$this->validateParams( ['depoID' => [self::TYPE_NUMBER, 'req'=>TRUE]] );
		$depo = new \ClickBlocks\DB\Depositions( $p['depoID'] );
		if( $depo->ID && $depo->ownerID == $this->user->ID ) {
			$da = new \ClickBlocks\DB\DepositionsAuth( $depo->ID );
			if( $da->ID ) {
				$this->return['key'] = $da->getPasscode();
			} else {
				$this->return['key'] = '********';
			}
			return;
		}
		$this->except( 'Invalid request', 501 );
	}

	public function api_join( $p )
    {
		$this->validateParams( [
			'depositionID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'depositionPasscode' => [self::TYPE_STRING, 'req'=>TRUE]
		] );

		$depositionID = (int)$p['depositionID'];
		$depositionPasscode = $p['depositionPasscode'];

		$this->return = ['needToLinkDepo' => FALSE];

		if( !$depositionID ) {
			$this->except( 'Users must specify Deposition ID' );
		} else {
			$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
			if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID ) {
				$this->except( 'Incorrect Deposition Key or Passcode' );
			}
			if( !$deposition->comparePassword( $depositionPasscode ) ) {
				$this->except( 'Incorrect Deposition Key or Passcode', 1004 );
			}
			if( $deposition->started == NULL && $deposition->ownerID != $this->user->ID ) {
				$this->except( 'Cannot login, deposition not yet started', 1002 );
			}
			if( $deposition->finished ) {
				$this->except( 'Cannot login, deposition is finished', 1003 );
			}

			$createAttendee = TRUE;
			// For Associated/Linked depo : Cannot directly login to depo of another client
			if( $deposition->getCase()->clientID != $this->user->clientID ) {
				$depositionParent = $deposition->parentDepositions1[0];
				if( $depositionParent && $depositionParent->getCase()->clientID == $this->user->clientID ) {
					$deposition = $depositionParent;
				} else {
					$targetDeposition = $this->getOrchestraDepositions()->getChildDepositionForClient( $deposition->ID, $this->user->clientID );
					if( $targetDeposition && $targetDeposition['ID'] ) {
						// found existing linked depo
						$deposition = new \ClickBlocks\DB\Depositions( $targetDeposition['ID'] );
						// If this user is not Case Manager and not Depo Assistant, automatically set as Assistant
						$caseManager = new \ClickBlocks\DB\CaseManagers( $deposition->caseID, $this->user->ID );
						if( !$caseManager || !$caseManager->userID || !$caseManager->caseID || $caseManager->caseID != $deposition->caseID || $caseManager->userID != $this->user->ID ) {
							$depoAssist = new \ClickBlocks\DB\DepositionAssistants( $deposition->ID, $this->user->ID );
							if( !$depoAssist->depositionID || !$depoAssist->userID ) {
								$depoAssist->depositionID = $deposition->ID;
								$depoAssist->userID = $this->user->ID;
								$depoAssist->save();
							}
						}
					} else {
						$createAttendee = FALSE;
						$this->return['needToLinkDepo'] = TRUE;
						$this->return['deposition'] = ['ID'=>$deposition->ID, 'class'=>$deposition->class, 'depositionOf'=>$deposition->depositionOf, 'caseID'=>$deposition->caseID];
					}
				}
			}

			if( $createAttendee ) {
				$this->userAttendToDeposition( $this->user->ID, $deposition );
				$this->return['depositionID'] = $deposition->ID;
				$this->return['deposition'] = $deposition->getValues();
				$this->return['folders'] = $this->getOrchestraFolders()->getFoldersForUser( $deposition->ID, $this->user->ID );
				$this->return['deposition']['exhibitHistory'] = $this->getOrchestraExhibitHistory()->exhibitHistoryForDeposition( $deposition->ID );
				$this->return['deposition']['liveTranscript'] = (!$deposition->isWitnessPrep()) ? 'Y' : 'N';
			}
		}
    }

	public function api_authorizeWitness( $p )
	{
		$this->validateParams( [
			'depositionID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'witnessID' => [self::TYPE_INTEGER, 'req'=>TRUE],
			'authorize' => [self::TYPE_BOOLEAN, 'req'=>FALSE]
		] );

		$depositionID = (int)$p['depositionID'];
		$witnessID = (int)$p['witnessID'];
		$authorize = (bool)$p['authorize'];

		$deposition = new \ClickBlocks\DB\Depositions( $depositionID );
		if( !$deposition || !$deposition->ID || $deposition->ID != $depositionID || $deposition->parentID || $deposition->ownerID != $this->user->ID ) {
			$this->except( 'Invalid deposition ID' );
		}

		$attendee = new \ClickBlocks\DB\DepositionAttendees( $witnessID );
		if( !$attendee || !$attendee->ID || $attendee->ID != $witnessID || $attendee->depositionID != $depositionID || $attendee->role != self::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS ) {
			$this->except( 'Invalid witness ID' );
		}

		if( !$authorize ) {
			$this->getOrchestraDepositionAttendees()->pruneUnauthorizedWitnesses( $depositionID );
			return;
		}

		$nodeJS = $this->getNodeJS();
		$nodeJS->authorizeWitness( $depositionID, $witnessID );

		//remove any other witnesses
		$witnessAttendees = $this->getOrchestraDepositionAttendees()->getWitnessAttendees( $depositionID );
		if( $witnessAttendees && is_array( $witnessAttendees ) ) {
			$args = ['depositionID'=>$depositionID, 'ban'=>FALSE, 'guestID'=>0];
			foreach( $witnessAttendees as $witness ) {
				if( $witness['ID'] == $attendee->ID ) {
					continue;	//do not remove the witness being authorized
				}
				if( $attendee->role == self::DEPOSITIONATTENDEE_ROLE_TEMPWITNESS ) {
					\ClickBlocks\Debug::ErrorLog( "Kicking (other) Witness from {$deposition->class}: " . print_r( $witness, TRUE ) );
					$args['guestID'] = $witness['ID'];
					$this->getNodeJS()->sendPostCommand( 'attendee_kick', null, $args );
				}
				if( $attendee->role == self::DEPOSITIONATTENDEE_ROLE_WITNESSMEMBER ) {
					continue;	//do not delete witness member
				}
				$att = new \ClickBlocks\DB\DepositionAttendees( $witness['ID'] );
				if( !$att || !$att->ID || $att->ID != $witness['ID'] ) {
					continue;
				}
				$att->delete();
			}
		}

		//Okay to go
		$attendee->role = self::DEPOSITIONATTENDEE_ROLE_WITNESS;
		$attendee->email = 'witness@edepoze.com';
		$attendee->update();
	}
}
