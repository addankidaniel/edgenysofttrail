<?php

namespace ClickBlocks\UnitTest;

use ClickBlocks\DB;

class ServiceBLLMatcher implements \Phake_Matchers_IArgumentMatcher
{
   /**
    *
    * @var array
    */
   private $values;
   //private $mustContainAll;
   
   public function __construct(array $values/*, $matchAll = false*/) {
      $this->values = $values;
      //$this->mustContainAll = (bool)$mustContainAll;
   }

   public function matches(&$argument)
   {
     return ($argument instanceof DB\IBLLTable
         && (//($this->mustContainAll && $argument->getValues() == $this->values) || 
         (array_intersect_key($argument->getValues(), $this->values) == $this->values)));
   }

   public function __toString()
   {
      $vals = array();
      foreach ($this->values as $k=>$v) $vals[] = $k.':'.$v;
      return '<BLLObject: '.implode(',',$vals).'>';
   }
}

?>
