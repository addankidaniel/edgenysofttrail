<?php

namespace ClickBlocks\API\v2_0_0;
const apiVersion = '2.0.0';
const codeName = 'yosemite';

require_once(__DIR__ . '/../../connect.php');

/**
 * lowercased array of allowed controllers
 */
$ents = array('edepo','door','user','chat','deposition','clients','cases','file');

$api = new API();
$api->setEntities($ents)->execute();
