<?php

namespace ClickBlocks\API\v1_3_2\Logic;
use ClickBlocks\API\v1_3_2,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;

/**
 * User controller should handle all actions related to Users, authorization, etc.
 *
 * @author Kolosovsky Vladislav
 * @property ClickBlocks\DB\ServiceUsers $svc Description
 */
class User extends Edepo
{
    public function api_logout()
    {
      if ($this->isUserMember())
        $this->deleteSession($this->user->ID, $this->user->typeID);
      else
        $this->deleteSession($this->attendee->ID, self::USER_TYPE_GUEST);
    }

    protected function getRelatedEntity() {
       return 'Users';
    }

    protected function getNoAuthMethods() {
       return array_merge(parent::getNoAuthMethods(), array('login','loginGuest','recoverPassword','checkVersion'));
    }

    public function api_recoverPassword($p)
    {
       $this->validateParams(array(
           'username' => array('type'=>self::TYPE_STRING, 'req'=>true),
       ));

       $user = $this->getOrchestraUsers()->getUserByUsername($p['username']);
       if (!$user->ID) $this->except('User not found', 1);
       $request = new DB\PasswordResetRequests();

       $request->hash = md5(microtime().$this->config->secretSalt.$user->password);
       $request->userID = $user->ID;
       $request->insert();

       $uri = new Utils\URI();
       $client = $user->clients[0];
       $url = ($client->ID) ? $client->getReseller()->URL : 'admin';
       $url = 'http://' . $uri->source['host'].'/'.$url.'/password-change?hash='.$request->hash;

       $mailer = Utils\EmailGenerator::sendPasswordRecovery($user->getValues(), $url);

       if ($mailer->IsError()) {
          $request->delete();
          $this->except ('Error sending email', 1002);
       }
       return array('email'=>$user->email);
     }

    /*public function _register($p) {
      if (!self::validateEmail($p['email']))
        $this->except('Incorrect email', 000);
      $this->except('WRONG!', 5);
    }*/

    public function api_renewSession() {
       if ($this->isUserGuest()) $sKey = $this->startSession($this->attendee->ID, self::USER_TYPE_GUEST);
       else $sKey = $this->startSession($this->user->ID, $this->user->typeID);
       $this->return = array('sKey'=>$sKey, 'leaseTime'=> $this->sessionLifetime);
    }

    protected function checkDepositionKey($p) {
       $depoID = $this->getOrchestraDepositions()->checkDepositionUKey($p['depositionKey'], true);
       if (!$depoID) $this->except ('incorrect depositionKey', 1001);
       return $depoID;
    }

    public function api_login($p) {
       $this->validateParams(array(
           'username' => array(self::TYPE_STRING, 'req'=>true),
           'password' => array(self::TYPE_STRING, 'req'=>true),
           'depositionKey' => array(self::TYPE_STRING, 'req'=>false),
           'depositionPassword' => array(self::TYPE_STRING, 'req'=>false),
       ));
       $userInfo = $this->getOrchestraUsers()->loginAPI($p['username'], $p['password']);
       if (!$userInfo['ID']) $this->except ('Authorization Failed', 401);
       if ($userInfo['deactivated'] || $userInfo['ClientDeactivated']) $this->except ('User is deactivated.', 1010);
       $this->return = array('needToLinkDepo' => false);
       if ($p['depositionKey']) {
          if (!$p['depositionPassword']) $this->except('Password is required to attend!', 201);
          $depoID = $this->checkDepositionKey($p);
          $depo = $this->getService('Depositions')->getByID($depoID);
          if ($depo->comparePassword($p['depositionPassword']) == false) $this->except('Incorrect Deposition Password!');
          $ownerID = $depo->getOwnerID();
          if ($depo->started == NULL && $ownerID != $userInfo['ID'])
             $this->except('Cannot login, deposition not started yet', 1002);
		  if ($p['depositionKey'] && $depo->finished )
			$this->except('Cannot login, deposition is finished', 1003);

          $createAttendee = true;
          // FOR Associated/Linked depo: Cannot directly login to depo of another client, RESOLVE:
          if ($depo->cases->clientID != $userInfo['clientID'])
          {
             if ($depo->parentID && $depo->getParentDeposition()->cases->clientID == $userInfo['clientID'])
             {
               // this deposition is linked to some deposition of my client, login silently to my deposition
               $depoID = $depo->parentID;
               $depo = $depo->getParentDeposition();
             } else
             {
                $targetDepo = $this->getOrchestraDepositions()->getChildDepositionForClient($depoID, $userInfo['clientID']);
                if ($targetDepo['ID']) // this client already has child depo for this depo, so login silently to linked depo instead
                {
                   $depoID = $targetDepo['ID'];
                   $depo = $this->getService('Depositions')->getByID($targetDepo['ID']);
                   // If this user is not Case manager, not Depo Assistant, set him automatically as  assistant
                   $amICM = (bool)$this->getService('CaseManagers')->getByID(array('caseID'=>$depo->caseID, 'userID'=>$userInfo['ID']))->userID;
                   if (!$amICM) $this->getService('DepositionAssistants')->setDA($depoID, $userInfo['ID']);
                } else {
                   // return to display popup
                   $createAttendee = false;
                   $this->return['needToLinkDepo'] = true;
                   $this->return['deposition'] = array('ID'=>$depo->ID, 'title'=>$depo->title, 'depositionOf'=>$depo->depositionOf);
                }
             }
          }

          if ($depo->parentID) $this->return['sourceDeposition'] = BLLFormat::getValues($depo->getParentDeposition());

          if ($createAttendee)
          {
            $this->userAttendToDeposition($userInfo['ID'], $depo);
            $this->return += array(
                'depositionID'=> $depoID,
                'deposition'=> BLLFormat::getValues($depo),
                'folders' => BLLFormat::formatRows($this->getOrchestraFolders()->getFoldersForUser($depoID, $userInfo['ID']), 'folders'));
          }
       }
	   $userInfo['username'] = mb_convert_case( $userInfo['username'], MB_CASE_LOWER );	//force usernames to lowercase (fixes offline logins using lowercase)
       $user = array_intersect_key($userInfo, array('ID'=>1,'firstName'=>1,'lastName'=>1,'email'=>1,'username'=>1,'clientID'=>1,'typeID'=>1));
       $this->return += array('sKey' => $this->startSession($userInfo['ID'], $userInfo['typeID']), 'leaseTime'=>$this->sessionLifetime, 'user'=>$user);
    }

    public function api_loginGuest($p)
    {
       $this->validateParams(array(
           'email' => array(self::TYPE_EMAIL, 'req'=>true),
           'name' => array(self::TYPE_STRING, 'req'=>true),
           'depositionKey' => array(self::TYPE_STRING, 'req'=>true),
           'depositionPassword' => array(self::TYPE_STRING, 'req'=>true),
       ));
       $depoID = $this->checkDepositionKey($p);
       $depo = $this->getService('Depositions')->getByID($depoID);
       if ($depo->comparePassword($p['depositionPassword']) == false) $this->except('Incorrect Deposition Password!');
       $attendance = $this->getOrchestraDepositionAttendees()->getForGuest($depoID, $p['email']);
       if ($depo->started == NULL)
         $this->except('Cannot login, deposition not started yet', 1002);
       if ($depo->finished)
         $this->except('Cannot login, deposition is finished', 1003);
       if (!$attendance->ID) {
          $mainDepo = ($depo->parentID ? $depo->getParentDeposition() : $depo);
		  //deprecated per ED-36 / ED-23
          //$this->validateAttendeeLimit($mainDepo);
          //if ($depo->finished) $this->except('Cannot attend, deposition is finished.', 1003);
          $attendance->name = $p['name'];
          $attendance->email = $p['email'];
          $attendance->depositionID = $depoID;
          $this->getService('DepositionAttendees')->insert($attendance);
       }
       if ($attendance->banned) $this->except('This user was blocked from attending this deposition!', self::ERR_BANNED_IN_DEPO);
       $this->return = array(
           'depositionID'=>$depoID,
           'guestID' => $attendance->ID,
           'folders' => BLLFormat::formatRows($this->getOrchestraFolders()->getFoldersForUser($depoID, null), 'folders'),
           'deposition' => BLLFormat::getValues($depo),
           'sKey' => $this->startSession($attendance->ID, self::USER_TYPE_GUEST),
           'leaseTime'=>$this->sessionLifetime);
    }

    public function api_test($p)
    {
      if ($this->isUserMember())
        return array('member'=>$this->user->getValues());
      elseif ($this->isUserGuest())
        return array('guest'=>$this->attendee->getValues());
      else return 'No User';
    }

	public function api_checkVersion( $p )
	{
		$this->validateParams( array( 'iPadBuild'=> array( self::TYPE_STRING, 'req'=>TRUE ) ) );

		$versions = new \ClickBlocks\Core\Config();
		$versions->init( $versions->root . '/Application/_config/versions.ini' );

		if( !isset( $versions['device']['appStoreVersion'] ) )
			throw new \LogicException( 'Unable to determine application version.' );

		$liveVersionComponents = explode( '.', $versions['device']['appStoreVersion'] );
		$testVersionComponents = explode( '.', $p['iPadBuild'] );

		$updateAvailable = 'ok';

		$majorLive = (isset( $liveVersionComponents[0] )) ? (int)$liveVersionComponents[0] : 0;
		$majorTest = (isset( $testVersionComponents[0] )) ? (int)$testVersionComponents[0] : 0;
		$minorLive = (isset( $liveVersionComponents[1] )) ? (int)$liveVersionComponents[1] : 0;
		$minorTest = (isset( $testVersionComponents[1] )) ? (int)$testVersionComponents[1] : 0;
		$patchLive = (isset( $liveVersionComponents[2] )) ? (int)$liveVersionComponents[2] : 0;
		$patchTest = (isset( $testVersionComponents[2] )) ? (int)$testVersionComponents[2] : 0;

		if( $majorTest < $majorLive )
		{
			$updateAvailable = 'update';
		} elseif( $majorTest === $majorLive ) {
			if( $minorTest < $minorLive )
			{
				$updateAvailable = 'update';
			} elseif( $minorTest === $minorLive ) {
				if( $patchTest < $patchLive )
				{
					$updateAvailable = 'update';
				}
			}
		}

		$this->return = array( 'versionCheck' => $updateAvailable, 'appStoreVersion' => $versions['device']['appStoreVersion'] );
	}
}

?>
