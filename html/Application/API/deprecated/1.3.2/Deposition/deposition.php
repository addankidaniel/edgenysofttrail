<?php

namespace ClickBlocks\API\v1_3_2\Logic;

use ClickBlocks\API\v1_3_2,
ClickBlocks\Core,
ClickBlocks\Utils,
ClickBlocks\DB;


class Deposition extends Edepo
{
  const ERR_HAS_ACTIVE_DEPO = 1101;
  private $errors = array(
      	'ERR_UNKNOWN_DEPOSITION' => 'Deposition ID doesn\'t exist.',
  		  'ERR_EXIST_FOLDER_FILE' => 'Folder ID and File ID parameters can\'t be exist in same request.',
        'ERR_EXIST_EXHIBIT_USERIDS' => 'isExhibit, UserIDs or GuestIDs parameters can\'t be exist in same request.',
        'ERR_NOT_EXIST_FOLDER_ID' => 'Folder ID doesn\'t exist.',
        'ERR_NOT_EXIST_FILE_ID' => 'File ID doesn\'t exist.',
        'ERR_NOT_EXIST_USER' => 'User ID doesn\'t exist.',
        'ERR_NOT_EXIST_CASE' => 'Case ID doesn\'t exist.',
        'ERR_MISSING_FILE' => 'Missing File ID or Folder ID parameter',
  			'ERR_MISSING_USER' => 'Missing isExhibit or userIDs or guestIDs parameter',
        'ERR_NOT_SHARE_EXHIBIT' => 'Exhibit folder can\'t be shared.',
  );

  protected function createFolder($folder_name, $userID, $depoID, $isExhibit = 0, $isPrivate = 0)
  {
  	$folder = new DB\Folders;
    $folder->name = $folder_name;
    $folder->isExhibit = $isExhibit;
    $folder->isPrivate = $isPrivate;
    $folder->createdBy = $userID;
    $folder->depositionID = $depoID;
    $folder->insert();
    return $folder->ID;
  }

  protected function buildShareFilesReturn($shareObjects)
  {
      $shares = array();
      foreach ($shareObjects as $share)
      {
          if ($share->attendeeID) {
            $tmp = array('ID' => $share->ID, 'fileID' => $share->fileID, 'guestID' => $share->attendeeID);
          } else {
            $tmp = array('ID' => $share->ID, 'fileID' => ($share->copyFileID ?: $share->fileID), 'userID' => $share->userID);
          }
          $shares[] = $tmp;
      }
      return $shares;
  }

  protected function buildShareFilesReturnNodeJS($shareObjects, $deposition, $folderName, $fileName = '', $isExhibit = 0)
  {
    $receivers = array();
	$folder = NULL;
	$foldersForIDs = array();
    foreach ($shareObjects as $share)
    {
		if ($share->copyFileID && $isExhibit) continue;
		//\ClickBlocks\Debug::ErrorLog( "shareID: {$share->ID}" );
		$receiverID = ($share->attendeeID) ? 'G'.$share->attendeeID : $share->userID;
		$fileID = ($share->copyFileID) ? $share->copyFileID : $share->fileID;

		//\ClickBlocks\Debug::ErrorLog( "receiverID: {$receiverID}" );
		//\ClickBlocks\Debug::ErrorLog( "fileID: {$fileID}  copyFileID: {$share->copyFileID}" );

		/*
        if ($share->attendeeID)
		{
            $f = $this->getService('Files')->getByID($share->fileID);
            $receivers['G'.$share->attendeeID]['files'][] = array('ID' => $share->fileID, 'isExhibit'=>(bool)$isExhibit, 'name' => $fileName ?: $f->name, 'shareID' => $share->ID, 'created' => $f->created);
        }
		else
		{
            $f = $this->getService('Files')->getByID($share->copyFileID ?: $share->fileID);
            $receivers[$share->userID]['files'][] = array('ID' => $f->ID, 'isExhibit'=>(bool)$isExhibit, 'name' => $f->name, 'shareID' => $share->ID, 'depositionID' => $folder->depositionID, 'created' => $f->created);
        }
		*/
		$f = $this->getService( 'Files' )->getByID( $fileID );
		$receivers[$receiverID]['files'][] = array( 'ID' => $f->ID, 'isExhibit' => (bool)$isExhibit, 'name' => $f->name, 'shareID' => $share->ID, 'created' => $f->created );

		if (!$foldersForIDs[$receiverID])
		{
			$folder = $this->getService('Folders')->getByID( $f->folderID );
			$foldersForIDs[$receiverID] = array('folderID' => $f->folderID, 'folderName'=> $folder->name, 'folderID' => $folder->ID, 'depositionID' => $folder->depositionID, 'created' => $folder->created, 'lastModified' => $folder->lastModified);
			//\ClickBlocks\Debug::ErrorLog( print_r( $foldersForIDs[$receiverID], true ) );
		}
    }
    // call to Nodejs
    $args = array();
    $args['sharedBy'] = $this->user->ID;
    $args['depositionID'] = $deposition->ID;
    $args['receivers'] = array();
    foreach ($receivers as $k => $receiver) {
        if ($k[0] == 'G') {
			$receiver['guestID'] = (int)substr($k, 1);
		} else {
			$receiver['userID'] = (int)$k;
		}
		$receiver['folder'] = $foldersForIDs[$k];
        $args['receivers'][] = $receiver;
    }
	//\ClickBlocks\Debug::ErrorLog( print_r( $args, true ) );
    return $args;
  }

   protected function copyFolder(DB\Folders $origFolder, $name, $userID, $depoID, $isExhibit = 0)
   {
      $copy = $origFolder->copy();
      $copy->createdBy = $userID;
      if ($name) $copy->name = $name;
      $copy->depositionID = $depoID;
      $copy->isExhibit = $isExhibit;
      $copy->insert();
      $idsMap = array();
      foreach ($origFolder->files as $file) $idsMap[$file->ID] = $this->copyFile($file, $copy->ID, $userID, null, $isExhibit);
      return $idsMap;
   }

   protected function copyFile($origFile, $folderID, $userID, $fileName = '', $isExhibit = 0, $isPrivate = 0)
   {
      if (is_object($origFile) && $origFile instanceof DB\Files) {
        $file = $origFile->copy();
      } else {
        $file = new DB\Files;
        $file->setSourceFile($origFile);
      }
      $file->folderID = (int)$folderID;
      $file->createdBy = (int)$userID;
      $file->sourceUserID = $this->user->ID;
      $file->isExhibit = $isExhibit;
      $file->isPrivate = $isPrivate;
      if ($fileName) $file->name = $fileName;
      $file->insert();
      return $file->ID;
   }

   protected function checkDepositionKey($p)
   {
      $depoID = $this->getOrchestraDepositions()->checkDepositionUKey($p['depositionKey'], true);
      if (!$depoID) $this->except ('incorrect depositionKey', 1001);
      return $depoID;
   }

   public function api_get($p)
   {
      $this->validateParams(array(
              'depositionID' => array('type'=>self::TYPE_INTEGER, 'req'=>false),
              'depositionKey' => array('type'=>self::TYPE_STRING, 'req'=>false),
              'includeFiles' => array('type'=>self::TYPE_BOOLEAN, 'req'=>false),
      ));
      if (!$p['depositionID'] && !$p['depositionKey']) $this->except('DepositionID is required');
      $depoID = ($p['depositionID'] ?: $this->checkDepositionKey($p));
      $depo = $this->getService('Depositions')->getByID($depoID);
      if (!$depo->ID) $this->except('Deposition not found', 404);
      $this->return['deposition'] = BLLFormat::getValues($depo);
      if ($p['includeFiles']) {
		 $this->api_getFolders(array('depositionID'=>$depo->ID));
         $this->return['deposition']['folders'] = $this->return['folders'];
         unset($this->return['folders']);
      }
   }

	public function api_getFolders($p)
	{
	  $this->validateParams( array( 'depositionID' => array('type'=>self::TYPE_STRING, 'req'=>true ) ) );
	  $this->return['folders'] = BLLFormat::formatRows($this->getOrchestraFolders()->getFoldersForUser($p['depositionID'], $this->user->ID), 'folders');
	}

	public function api_getFiles($p)
	{
		$this->validateParams(array(
			'folderID' => array('type'=>self::TYPE_STRING, 'req'=>true),
			'sortBy'   => array('type'=>self::TYPE_ENUM, 'req'=>false, 'options'=>['created','name','ID']),
			'sortDir'  => array('type'=>self::TYPE_ENUM, 'req'=>false, 'options'=>['ASC','DESC']),
			'pageSize' => array('type'=>self::TYPE_INTEGER, 'req'=>false),
			'page'     => array('type'=>self::TYPE_INTEGER, 'req'=>false),
		));
		$this->return['files'] = $this->getOrchestraFolders()->getFiles_API($p['folderID'], ['by'=>$p['sortBy'],'dir'=>$p['sortDir']], $p['pageSize'], $p['page']);
		$folder = $this->getService('Folders')->getByID( $p['folderID'] );
		//$this->return['folder'] = array( 'folderID' => (int)$folder->ID, 'folderName' => $folder->name, 'created' => $folder->created, 'lastModified' => $folder->lastModified );
	}

  public function api_getList($p)
  {
    $this->validateParams(array(
              'caseID' => array('type'=>self::TYPE_INTEGER, 'req'=>true)
    ));
    $case = $this->getService('Cases')->getByID($p['caseID']);
    if (!$case->ID) $this->except($this->errors['ERR_NOT_EXIST_CASE'], 104);
    $this->return['depositions'] = $this->getOrchestraDepositions()->getAllByCase($p['caseID']);
    foreach ($this->return['depositions'] as &$depo) $depo['ownerID'] = $case->createdBy;
  }

   public function api_shareFiles($p)
   {
      $this->validateParams(array(
                'depositionID' => array('type' => self::TYPE_SCALAR, 'req'=>true),
                'fileID' => array('type' => self::TYPE_SCALAR, 'req'=>false),
                'folderID' => array('type' => self::TYPE_SCALAR, 'req'=>false),
                'name' => array('type' => self::TYPE_STRING, 'req'=>false),
                'file' => array('type' => self::TYPE_FILE, 'req'=>false),
                'userIDs' => array('type' => self::TYPE_ARRAY, 'req'=>false),
                'guestIDs' => array('type' => self::TYPE_ARRAY, 'req'=>false),
                'isExhibit' => array('type' => self::TYPE_BOOLEAN, 'req'=>false),
      ));

	  //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- start' );

      $guestIDs = $targetUsers = $filesInfo = $shareObjects = $copiedFileIDs = $attendees = array();
      $origFile = $origFolder = null;
      $folderName = $fileName = '';

      if ($p['isExhibit']==false && count($p['userIDs'])==0 && count($p['guestIDs'])==0) $this->except($this->errors['ERR_MISSING_USER'], 1016);

      // this is deposition I logged in
      $deposition = $this->getService('Depositions')->getByID($p['depositionID']);
      if (!$deposition->ID)
          $this->except('Deposition not found', 1000);

      // this is parent (source) deposition of my deposition. by default it is equal to $deposition
      $mainDeposition = ($deposition->parentID) ? $deposition->parentDepositions1[0] : $deposition;

      // Only Owner OR speaker of main (source) deposition can share!
      if ($mainDeposition->ownerID != $this->user->ID && (!$mainDeposition->speakerID || $mainDeposition->speakerID != $this->user->ID))
          $this->except('Access denied', 403);

      if(((int)$p['isExhibit'] && (isset($p['userIDs']) || isset($p['guestIDs']))) || ((int)$p['isExhibit'] && ((isset($p['userIDs']) && count($p['userIDs']) < 1 ) || (isset($p['guestIDs']) && count($p['guestIDs']) < 1))))
        $this->except($this->errors['ERR_EXIST_EXHIBIT_USERIDS'], 1011);
      if(isset($p['folderID']) && isset($p['fileID']))
          $this->except($this->errors['ERR_EXIST_FOLDER_FILE'], 1010);
      if(empty($p['folderID']) && empty($p['fileID']))
          $this->except($this->errors['ERR_MISSING_FILE'], 1025);

      if (isset($p['fileID']))
      {
          $origFile = $this->getService('Files')->getByID($p['fileID']);
          if (!$origFile->ID || $origFile->folders->depositionID != $p['depositionID'])
              $this->except($this->errors['ERR_NOT_EXIST_FILE_ID'], 1013);
          if (!in_array($origFile->createdBy, array($deposition->createdBy, $this->user->ID)))
              $this->except('No access to given file', 1000);
          $fileName = $origFile->name;
          $filesInfo[] = array('ID' => $origFile->ID, 'name' => $file->name);
          $folderName = $this->config->logic['personalFolderName'];
      }

      if (isset($p['folderID']))
      {
          $origFolder = $this->getService('Folders')->getByID($p['folderID']);
          if (!$origFolder->ID || $origFolder->depositionID != $p['depositionID'])
              $this->except($this->errors['ERR_NOT_EXIST_FOLDER_ID'], 1012);
          if (!in_array($origFolder->createdBy, array($deposition->createdBy, $this->user->ID)))
              $this->except('No access to given file', 1000);
          //if (($origFolder->isExhibit && $origFolder->name == $this->config->logic['exhibitFolderName']) || $origFolder->isPrivate)
		  if ($origFolder->isExhibit && $origFolder->name == $this->config->logic['exhibitFolderName'])
              $this->except('Cannot share Exhibit folders.', 1017);
          $folderName = $origFolder->name;
          $filesInfo = $this->getOrchestraFiles()->getFilesByFolder($p['folderID']);
          if (count($filesInfo) < 1) $this->except('Empty folder can\'t be shared.', 1026);
      }

      // Validate name parameter
      $p['name'] = trim($p['name']);
      if ($p['name'])
      {
         if ($p['folderID'])
         {
            // check folder name not include special character
            if (!preg_match('=^[^/?*;:{}\\\\]+$=', $p['name'])) $this->except('Folder Name is invalid', 1029);
            $folderName = $p['name'];
         }
         else
         {
            // check file name not include special character and has a dot
            if (!preg_match('=^[^/?*;:{}\\\\]+\.[^/?*;:{}\\\\]+$=', $p['name'])) $this->except('File Name is invalid', 1021);
            $fileName = $p['name'];
         }
      }
      // this is trusted users from Local Depo
      $trustedUsers = $this->getOrchestraDepositions()->getAllTrustedUsersAsMap($deposition->ID);

      // Don't share Folder to trusted users
      $needShareToUser = function($userID) use (&$trustedUsers, $p) {
         return (!isset($trustedUsers[$userID]) || ($p['fileID']));
      };
      if (count($p['userIDs']) > 0)
      {
          $depoMap = $this->getOrchestraDepositions()->getClient2DepoMapForLinkedTo($mainDeposition->ID);
          foreach($p['userIDs'] as $id)
          {
              if ($needShareToUser($id) == false) continue; // Don't share to specific users in some cases
              $user = $this->getService('Users')->getByID($id);
              if (!$user->ID) $this->except('UserID '.$id.' not exist!', 1018);
              if (!isset($depoMap[$user->clientID]))
                 $this->except('Cannot share to user '.$id.' (clientID='.$user->clientID.'), linked deposition for this Client not found.', 1040);
              $targetUsers[$id] = array('userID'=>$user->ID, 'depositionID'=>$depoMap[$user->clientID]);
          }
      }
      if (count($p['guestIDs']) > 0)
      {
         foreach($p['guestIDs'] as $id)
         {
             $guest = $this->getService('DepositionAttendees')->getByID($id);
             if (!$guest->ID) $this->except('GuestID '.$id.' not exist', 1019);
         }
         $guestIDs = $p['guestIDs'];
      }

      $addShare = function ($userID, $attendeeID, $fileID, $fileCopyID, $depositionID)
      {
         $share = $this->getBLL('FileShares');
         $share->depositionID = $depositionID;
         $share->fileID = $fileID;
         $share->copyFileID = $fileCopyID;
         $share->createdBy = $this->user->ID;
         $share->userID = $userID;
         $share->attendeeID = $attendeeID;
         $share->insert();
         return $share;
      };

      // Share to Users
      if((int)$p['isExhibit'])
      {
        // get all attendees of main deposition + all child depos
        $attendees = $this->getOrchestraDepositionAttendees()->getAttendeesByDepo($mainDeposition->ID);
        // always process main deposition, for guests
        $targetDepos = array($mainDeposition->ID => array());
        foreach ($attendees as $att)
        {
           //if ($att['userID'] == $this->user->ID) continue; // don't share to self
           if ($att['userID']) {
              if ($needShareToUser($att['userID']) == false) continue; // Don't share to specific users in some cases
              $targetDepos[$att['depositionID']][] = $att['userID'];
           } else $guestIDs[] = $att['ID'];
        }
        if ($p['folderID'])
        {
            // for share FOLDER: set this folder as isExhibit:
            $origFolder->isExhibit = 1;
            $origFolder->update();
            // share all files from folder to target users of Local Deposition
            foreach ((array)$targetDepos[$deposition->ID] as $userID)
            {
               foreach ($filesInfo as $fileInfo) {
                  $shareObjects[] = $addShare($userID, null, $fileInfo['ID'], null, $deposition->ID);
               }
            }
        } else if ($p['fileID'])
        {
           $folderName = $this->config->logic['exhibitFolderName'];
           // Override filesInfo array with file that we will put in exhibit folder
           // $filesInfo array will only be used for guests, because each target will have his own filesInfo
           $filesInfo = array();
        }
        // copy shared files to each creator of all depositions within association graph (Source Depo + all Target Depos)
        foreach ($targetDepos as $depoID=>$userIDs)
        {
           if ($p['folderID'] && ($depoID == $deposition->ID)) continue; // don't copy folder to self
           $creatorID = $this->getOrchestraDepositions()->getCreatorID($depoID);
           if ($p['folderID'])
           {
              // when I share folder to all attendees - create this folder in Target Depo creator with all contents copied
              $copiedFileIDs = $this->copyFolder($origFolder, $folderName, $creatorID, $depoID, 1);
           } else {
              // copy file to "Exhibit" folder of Creator
              $ownerFolderID = $this->getOrchestraFolders()->getExhibitFolder($depoID, $creatorID)['ID'];
              if (!$ownerFolderID) $ownerFolderID = $this->createFolder($this->config->logic['exhibitFolderName'], $creatorID, $depoID, 1);
              // This will copy original file (OR uploaded file with watermark) to this deposition creator
              if ($_FILES['file']['tmp_name'] && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
                 $_sourceFile = '/tmp/'.uniqid();
                 copy($_FILES['file']['tmp_name'], $_sourceFile);
               } else {
                 $_sourceFile = $origFile;
               }
			   //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- copyFile' );
              $copiedFileIDs[$p['fileID']] = $this->copyFile($_sourceFile, $ownerFolderID, $creatorID, $fileName, 1);
			  $this->getOrchestraFolders()->setLastModified( $ownerFolderID );
           }
           foreach ($copiedFileIDs as $origFileID=>$copyFileID)
           {
              // Share file to Depo Creator
              $shareObjects[] = $addShare($creatorID, null, $origFileID, $copyFileID, $depoID);
              if ($depoID == $mainDeposition->ID) $filesInfo[] = array('ID' => $copyFileID); // this for guests of main deposition
              foreach ($userIDs as $userID) {
                 // Share file to specific user in this Depo
                 $shareObjects[] = $addShare($userID, null, $copyFileID, null, $depoID);
              }
           }
        }
      }
      else  // share to specific users
      {
		  //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sharing to specific user' );
         foreach ($targetUsers as $userID=>$info)
         {
            if ($p['folderID']) // we are copying folder, and we need to create with same name on destination user
            {
				//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- copyFolder' );
               $copiedFileIDs = $this->copyFolder($origFolder, $p['name'], $userID, $info['depositionID']);
            }
            else  // copying one file to Personal folder
            {
               $folderID = $this->getOrchestraFolders()->getPersonalFolder($info['depositionID'], $userID)['ID'];
               if (!$folderID) $folderID = $this->createFolder($this->config->logic['personalFolderName'], $userID, $info['depositionID'], 0, 1);
               $copiedFileIDs[$origFile->ID] = $this->copyFile($origFile, $folderID, $userID, $fileName, 0, 1);
			   $this->getOrchestraFolders()->setLastModified( $folderID );
            }
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- start' );
            foreach($copiedFileIDs as $origFileID => $copyFileID)
            {
               $shareObjects[] = $addShare($userID, null, $origFileID, $copyFileID, $info['depositionID']);
            }
			//\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- addShare -- end' );
         }
      }

      // Add Shares for guests
      foreach ($guestIDs as $guestID)
      {
          // add to share file table
          foreach($filesInfo as $fileInfo)
          {
            $shareObjects[] = $addShare(null, $guestID, $fileInfo['ID'], null, $mainDeposition->ID);
          }
      }

	 //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- done with shares' );

     // add result to Nodejs
	 //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- buildShareFilesReturnNodeJS' );
     $args = $this->buildShareFilesReturnNodeJS($shareObjects, $mainDeposition, $folderName, $fileName, $p['isExhibit']);
	 //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- args: ' . print_r( $args, true ) );
	 //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- buildShareFilesReturn' );
     $this->return['shares'] = $this->buildShareFilesReturn($shareObjects);
	 //\ClickBlocks\Debug::ErrorLog( 'api_shareFiles -- sendPostCommand' );
     $this->getNodeJS()->sendPostCommand( 'sharefiles', null, $args );
     $this->return['argsToNodeJS'] = $args;
  }

  public function api_getAttendees($p)
  {
     $this->requireParams(array('depositionID'));
     return $this->getOrchestraDepositionAttendees()->getAttendeesWithFlags($p['depositionID']);
  }

  public function api_abortShare($p)
  {
     $this->validateParams(array(
              'depositionID' => array('type' => self::TYPE_NUMBER, 'req'=>true),
              'shareIDs' => array('type' => self::TYPE_ARRAY, 'req'=>true),
              'folderID' => array('type' => self::TYPE_NUMBER, 'req'=>false),
              'isExhibit'=> array('type' => self::TYPE_BOOLEAN, 'req'=>false),
     ));
     $depo = $this->getService('Depositions')->getByID($p['depositionID']);
     if (!$depo->ID) $this->except ('Deposition not found', 404);
     $svc = $this->getService('FileShares');
     $svF = $this->getService('Files');
     $recievers = array();
     $affectedFolders = array();
     $shares = array();
     // First gather information from shares before delete
     foreach ($p['shareIDs'] as $shareID)
     {
        $share = $svc->getByID($shareID);
        if (!$share->ID)  continue;
        // if ($share->depositionID != $p['depositionID']) continue; ???
        if ($share->attendeeID) {
           $recievers['G'.$share->attendeeID]['files'][] = array('ID'=>$share->fileID, 'shareID'=>$shareID); // guest should stop downloading original file
        } else {
           $recievers[$share->userID]['files'][] = array('ID'=>($share->copyFileID ?: $share->fileID), 'shareID'=>$shareID); // user will stop downloading file copy
        }
        $shares[] = $share;
     }
     // Perform abort
     foreach ($shares as $share) {
        // Delete copied file and share object from server
        if ($share->copyFileID) {
           $copyFile = $svF->getByID($share->copyFileID);
           $affectedFolders[$copyFile->folderID] = true;
           $copyFile->delete(); // when File is deleted, Share is also deleted by foreign key
        } else {
           $share->delete();
        }
     }
     $svFo = $this->getService('Folders');
     // Delete empty folders
     foreach ($affectedFolders as $folderID=>$v) {
        $folder = $svFo->getByID($folderID);
        if ($folder->isPrivate) continue; // Don't delete empty Personal folders
        if ($folder->isExhibit && $folder->name == $this->config->logic['exhibitFolderName']) continue; // never delete main exhibit folder
        if ($folder->files->count() == 0) $folder->delete();
     }
     // Unshare folder as exhibit
     if ($p['folderID'] && $p['isExhibit']) {
        $folder = $svFo->getByID($p['folderID']);
        if ($folder->ID) {
           $folder->isExhibit = 0;
           $folder->update();
        } else echo 'Folder '.$p['folderID'].' was not found!!';
     }
     $args = array('recievers'=>array());
     foreach ($recievers as $k=>$reciever) {
        if ($k[0]=='G') $reciever['guestID'] = (int)substr($k,1);
        else $reciever['userID'] = (int)$k;
        $args['recievers'][] = $reciever;
     }
     $args['depositionID'] = $depo->ID;
     $args['abortedBy'] = $this->user->ID;
     // Send command to NodeJS to stop downloading this files
     $this->getNodeJS()->sendPostCommand('abortshare', null, $args);
  }

	public function api_start($p) {
		$this->requireParams(array('depositionID'));
		$depo = $this->getService('Depositions')->getByID($p['depositionID']);
		if (!$depo->ID) $this->except('Deposition not found', 1003);
		if ($depo->getOwnerID() != $this->user->ID) $this->except('Access denied', 403);
		$active = $this->getOrchestraDepositions()->getActiveByOwner($this->user->ID);
		if (count($active)) $this->except ('Has active depo', self::ERR_HAS_ACTIVE_DEPO, array('otherDeposition'=>array_intersect_key($active[0], array('ID'=>1,'depositionOf'=>1,'title'=>1))));
		$depo->start();
		$this->getNodeJS()->sendPostCommand('deposition_start', null, array('date' => date('Y-m-d H:i:s'), 'depositionID'=>$depo->ID));
	}

  public function api_finish($p) {
    $this->validateParams(array(
        'depositionID'=>array(self::TYPE_NUMBER, 'req'=>true),
        'courtReporterEmail' => array(self::TYPE_EMAIL, 'req'=>false),
      ));
    $depo = $this->getService('Depositions')->getByID($p['depositionID']);
    if (!$depo->ID) $this->except('Deposition not found', 1003);
    if ($depo->getOwnerID() != $this->user->ID) $this->except('Access denied', 403);
    if ($depo->parentID) $this->except('Cannot finish associated deposition. Owner of source deposition must do this.', 1004);
    $oldCREmail = $depo->courtReporterEmail;
    if ($p['courtReporterEmail']) $depo->courtReporterEmail = $p['courtReporterEmail'];
    if ($depo->courtReporterEmail && $depo->courtReporterPassword !== $oldCREmail) { // send notification if email is set
       $pass = substr(md5(microtime(1).'123'), 0, 8);
       $depo->courtReporterPassword = $pass; // proper hash is automatically set
       $url = '/'.$this->getOrchestraClients()->getResellerURLByDepo($depo->ID).'/reporter/login';
	   $info['email'] = $depo->courtReporterEmail;
       $info['password'] = $pass;
       $info['depositionID'] = $depo->uKey;
       $info['depositionOf'] = $depo->depositionOf;
       Utils\EmailGenerator::sendCourtReporterInvite( $info, $url );
    }
    $depo->finish();
    $this->getNodeJS()->sendPostCommand('deposition_end', null, array('date' => date('Y-m-d H:i:s'), 'depositionID'=>$depo->ID));
  }

  /* testing
  public function api_bfinish( $p ) {
	  $this->validateParams( array( 'depositionID' => array( self::TYPE_NUMBER, 'req'=>true ) ) );
	  $this->chargePerDepositionFees( $p['depositionID'] );
  }
  */

  public function api_link($p)
  {
      $this->validateParams(array(
        'sourceDepositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
        'targetCaseID'        => array(self::TYPE_NUMBER, 'req'=>false),
        'targetDepositionID'  => array(self::TYPE_NUMBER, 'req'=>false),
      ));
      $svc = $this->getService('Depositions');
      $sourceDepo = $svc->getByID($p['sourceDepositionID']);
      if (!$sourceDepo->ID) $this->except ('sourceDepositionID invalid', 404);
      if ($sourceDepo->cases->clientID == $this->user->clientID)
         $this->except ('There is no point to link deposition of your own team (client)', 1001);
      if ($sourceDepo->started == NULL)
         $this->except ('Cannot link/attend, source Deposition is not started yet', 1003);

      if ($p['targetDepositionID']) {
         $targetDepo = $svc->getByID($p['targetDepositionID']);
         if (!$targetDepo->ID) $this->except('targetDepositionID invalid', 404);
         if ($this->getOrchestraDepositions()->isTrustedUser($this->user->ID, $targetDepo->ID) == false)
            $this->except('Access denied for this deposition', 403);
         if ($targetDepo->started)
            $this->except ('Cannot link when target deposition was started', 1005);
         if ($targetDepo->parentID)
            $this->except ('Target Deposition is already linked to Depo #'.$targetDepo->parentID, 1004);
      } else {
         $myAdminID = $this->getOrchestraUsers()->getClientAdminID($this->user->clientID);
         if ($p['targetCaseID']) {
            $targetCase = $this->getService('Cases')->getByID($p['targetCaseID']);
            if (!$targetCase->ID) $this->except('targetCaseID invalid', 404);
         } else {
            // CLONE case
            $targetCase = $sourceDepo->getCase()->getCopy();
            $targetCase->clientID = $this->user->clientID;
            $targetCase->createdBy = $myAdminID;
            $targetCase->insert();
            $this->getService('CaseManagers')->setCM($targetCase->ID, $this->user->ID);
            $this->return['clonedCaseID'] = $targetCase->ID;
         }
         // CLONE deposition
         $targetDepo = $sourceDepo->getCopy();
         $targetDepo->caseID = $targetCase->ID;
         $targetDepo->createdBy = $targetDepo->ownerID = $this->user->ID;
         $targetDepo->insert();
         $this->return['clonedDepositionID'] = $targetDepo->ID;
         $targetDepo->uKey = $targetDepo->ID;
      }
      $this->return['sourceDeposition'] = BLLFormat::getValues($sourceDepo);
      $this->return['targetDeposition'] = BLLFormat::getValues($targetDepo);
      //$this->return['sourceDeposition']['folders'] = BLLFormat::formatRows($this->getOrchestraFolders()->getFoldersForUser($p['sourceDepositionID'], $this->user->ID), 'folders');

      $targetDepo->parentID = $sourceDepo->ID;
      // Copy exhibit folder to target depo
      $exhibits = $this->getOrchestraFolders()->getExhibitFoldersObjects($sourceDepo->ID);
      foreach ($exhibits as $exhibit) {
         if ($exhibit->name == $this->config->logic['exhibitFolderName']) {
            $targetExhibit = $this->getOrchestraFolders()->getExhibitFolderObject($targetDepo->ID); // "Exhibit" folder on target
         }
         else $targetExhibit = null;
         if ($targetExhibit && $targetExhibit->ID) {
            $targetExhibit->setFilesToCopy($exhibit)->copyFiles();
         } else {
            $exhibitCopy = $exhibit->copy(true);
            $exhibitCopy->depositionID = $targetDepo->ID;
            $exhibitCopy->createdBy = $targetDepo->getOwnerID();
            $exhibitCopy->insert();
         }
      }
      $targetDepo->started = 'NOW()';
      $targetDepo->statusID = self::DEPOSITION_STATUS_IN_PROCESS;
      $targetDepo->update();

	  //ED-135; Child Case/Depositions should not be charged one-time setup fees
	  $this->getOrchestra( 'InvoiceCharges' )->removeCaseDepositionSetupCharges( $targetCase->ID, $targetDepo->ID );

      // try to attend
      $this->userAttendToDeposition($this->user->ID, $targetDepo);

	  $this->return['sourceDeposition']['folders'] = BLLFormat::formatRows($this->getOrchestraFolders()->getFoldersForUser($sourceDepo->ID, $this->user->ID), 'folders');
	  $this->return['targetDeposition']['folders'] = BLLFormat::formatRows($this->getOrchestraFolders()->getFoldersForUser($targetDepo->ID, $this->user->ID), 'folders');

	  // Push info to NodeJS
      $this->getNodeJS()->sendPostCommand('deposition_link', null, array('clientID' => $this->user->clientID, 'targetDepositionID'=>$targetDepo->ID));
  }

  public function api_attend($p)
  {
    $this->validateParams(array(
        'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
      ));
    $depo = $this->getService('Depositions')->getByID($p['depositionID']);
    if (!$depo->ID) $this->except('Deposition not found', 1003);
    if ($depo->started == NULL && $depo->getOwnerID() != $this->user->ID)
       $this->except('Cannot attend, deposition not started yet', 1002);
    $this->userAttendToDeposition($this->user->ID, $depo);
  }

  /**
   *
   * @deprecated per ED-23
   */
  public function api_buyAttendeeBundle($p)
   {
	  /*
      $this->validateParams(array(
         'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
      ));
      $depo = $this->getService('Depositions')->getByID($p['depositionID']);
      if (!$depo->ID) $this->except('Deposition not found', 1003);
      if ($depo->getOwnerID() != $this->user->ID) $this->except('Access denied', 403);
      $bundleSize = (int)$this->user->clients->attendeeBundleSize;
      if (!$bundleSize) $this->except ('This client cannot buy attendee bundle, because bundle size is 0.', 1004);
      DB\ServiceInvoiceCharges::charge($this->user->clientID, self::PRICING_OPTION_ADDITIONAL_BUNDLE, 1, $depo->caseID, $depo->ID);
      $this->return['newLimit'] = $depo->attendeeLimit = (int)$depo->attendeeLimit + $bundleSize;
      $depo->update();
      $this->getNodeJS()->sendPostCommand('attendee_limit_extended', null, array('depositionID' => $depo->ID, 'extended'=>true, 'newLimit'=>$depo->attendeeLimit));
	  */
   }

   public function api_kickAttendee($p)
   {
      $this->validateParams(array(
          'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
          'userID'  => array(self::TYPE_NUMBER, 'req'=>false),
          'guestID'  => array(self::TYPE_NUMBER, 'req'=>false),
          'ban'  => array(self::TYPE_BOOLEAN, 'req'=>false),
      ));
      $depo = $this->getService('Depositions')->getByID($p['depositionID']);
      if (!$depo->ID) $this->except('Invalid depositionID', 404);
      if ($depo->ownerID != $this->user->ID) $this->except('Access denied!', 403);
      if (!$p['userID'] && !$p['guestID']) $this->except ('UserID or GuestID required!', 201);
      if ($p['userID'] && $p['guestID']) $this->except ('Both UserID AND GuestID are not allowed!', 204);
      $args = array('depositionID' => $p['depositionID'], 'ban' => (bool)$p['ban']);
      if ($p['userID']) {
         $att = $this->getOrchestraDepositionAttendees()->getForUser($p['depositionID'], $p['userID'], true);
         $args['userID'] = $p['userID'];
      } else {
         $att = $this->getService('DepositionAttendees')->getByID($p['guestID']);
         if ($att->depositionID != $depo->ID && (!$att->depositions->parentID || $att->depositions->parentID != $depo->ID)) $this->except ('Attendee not found!', 404);
         $args['guestID'] = $p['guestID'];
      }
      //$this->return['wtf'] = $att->getValues();
      if (!$att->ID) $this->except ('Attendee not found!', 404);
      if ($p['ban']) {
         $att->banned = new DB\SQLNOWValue;
         $att->update();
      } else {
         $att->delete();
      }
      $this->getNodeJS()->sendPostCommand('attendee_kick', null, $args);
   }

   public function api_setSpeaker($p)
   {
      $this->validateParams(array(
          'depositionID'  => array(self::TYPE_NUMBER, 'req'=>true),
          'userID'  => array(self::TYPE_NUMBER, 'req'=>false),
      ));
      $depo = $this->getService('Depositions')->getByID($p['depositionID']);
      if (!$depo->ID) $this->except('Invalid depositionID', 404);
      if ($depo->parentID) $this->except('Cannot set speaker for an associated deposition. Owner of source deposition must do this.', 1004);
      if ($depo->ownerID != $this->user->ID) {
         if ($p['userID']) $this->except('Access denied! (only owner can assign speaker)', 403);
         else if ($depo->speakerID != $this->user->ID) $this->except('Access denied! (only owner and speaker can remove speaker)', 403);
      }
      if ($p['userID']) {
         $user = $this->getService('Users')->getByID($p['userID']);
         if (!$user->ID) $this->except ('User not found', 404);
         //if ($this->getOrchestraDepositions()->isTrustedUser($user->ID, $depo->ID) == false) $this->except('Cannot set non-trusted user as speaker.');
      }
      $oldSpeaker = $depo->speakerID;
      $depo->speakerID = ($p['userID']) ?: NULL;
	  $newSpeakerID = ($depo->speakerID) ? $depo->speakerID : $depo->ownerID;
	  $depo->speakerID = $newSpeakerID;
      $depo->update();
	  $this->getOrchestraDepositions()->setSpeakerIDForChildDepositions( $depo->ID, $depo->speakerID );
      $args = array('depositionID'=>$depo->ID, 'ownerID'=>$depo->ownerID, 'oldSpeakerID'=>$oldSpeaker, 'newSpeakerID'=>$depo->speakerID);
      $this->getNodeJS()->sendPostCommand('deposition_setspeaker', null, $args);
   }
}

$this->return['fff'] = array("isFile"=>is_file($_FILES['file']['tmp_name']));
?>
