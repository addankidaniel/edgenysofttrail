<?php                

namespace ClickBlocks\APITest\General;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\API\Logic\O4S,
    ClickBlocks\APITest\BaseTest,
    ClickBlocks\Exceptions;

class TestController extends O4S
{
  public function validateTest($p, $params)
  {
    $this->params = $p;
    $this->validateParams($params);
  }
  
  public function requireTest($p, $params)
  {
    $this->params = $p;
    $this->requireParams($params);
  }
}

/**
 * @group validateParams 
 */
class ValidateParamsTest extends BaseTest
{
  static $cnt;
  public static function setUpBeforeClass()
  {
    self::$cnt = new TestController();
  }
  
  public function testRequireParams_singleParam()
  {
    $this->setExpectedException('\LogicException', '', 201);
    self::$cnt->requireTest(array('something'=>'asd'), 'singleparam');
  }
  
  public function testRequireParams_shouldTreatEmptyAsNotProvided()
  {
    $this->setExpectedException('\LogicException', '', 201);
    self::$cnt->requireTest(array('empty'=>''), 'empty');
  }
  
  public function testRequireParams_severalParams_shouldPass()
  {
    self::$cnt->requireTest(array('1'=>1, 2=>3, '3'=>'3'), array('1','2','3') );
  }
  
  public function testRequireParams_severalParams_shouldFail()
  {
    $this->setExpectedException('\LogicException', "'1', '2', '3', '4', '5'", 201);
    self::$cnt->requireTest(array('1'=>false, 2=>0, '4'=>null, 5=>''), array('1','2','3','4','5') );
  }

  public function testNotProvided()
  {
    $this->setExpectedException('\LogicException','',201);
    self::$cnt->validateTest(array('first'=>'', 'second'=>array()),
            array('first'=>array('req'=>true), 'second'=>array('type'=>O4S::TYPE_SCALAR)));
  }
  
  public function testNotProvidedList()
  {
    $this->setExpectedException('\LogicException',"'first', 'second', 'third'",201);
    self::$cnt->validateTest(array('first'=>'', 'second'=>0),
            array('first'=>array('req'=>true), 'second'=>array('req'=>true), 'third'=>array('req'=>1)));
  }
  
  public function testParameterList()
  {
    $this->setExpectedException('\LogicException',"'first', 'second'",201);
    self::$cnt->validateTest(
            array('first'=>'', 'second'=>0),
            array('first'=>array('req'=>true), array('req'=>true, 'name'=>'second'))
          );
  }
  
  public function testCheckType_Bool_Fail()
  {
    $this->setExpectedException('\LogicException','boolean',202);
    self::$cnt->validateTest(
            array('bool'=>'true'),
            array('bool'=>array('type'=>O4S::TYPE_BOOLEAN/*, 'required'=>true*/))
          );
  }

  public function testCheckType_Int_Fail()
  {
    $this->setExpectedException('\LogicException','integer',202);
    self::$cnt->validateTest(
            array('int'=>'124.2', 'second'=>0),
            array('int'=>O4S::TYPE_INTEGER)
          );
  }
  
  public function testCheckType_Int_Min_Fail()
  {
    $this->setExpectedException('\LogicException','less than',202);
    self::$cnt->validateTest(
            array('int'=>'-14'),
            array('int'=>array('type'=>O4S::TYPE_INTEGER, 'min'=>5))
          );
  }
  
  public function testCheckType_Float_Max_Fail()
  {
    $this->setExpectedException('\LogicException','greater than',202);
    self::$cnt->validateTest(
            array('float'=>1242.324),
            array('float'=>array(O4S::TYPE_FLOAT, 'max'=>'242'))
          );
  }
  
  public function testCheckType_Float_Fail()
  {
    $this->setExpectedException('\LogicException','float',202);
    self::$cnt->validateTest(
            array('int'=>'124n', 'second'=>0),
            array('int'=>O4S::TYPE_FLOAT)
          );
  }
  
  public function testCheckType_Email_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('email'=>'dfasd@asdsd', 'second'=>0),
            array('email'=>array('type'=>O4S::TYPE_EMAIL))
          );
  }
  
  public function testCheckType_Regexp_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('re'=>'12+42=124'),
            array('re'=>array('type'=>O4S::TYPE_REGEXP, 'expression'=>'/\d{2}\+\d{2}\=\d{4}/'))
          );
  }
  
  public function testCheckType_Scalar_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('sc'=>array(123)),
            array('sc'=>array('type'=>O4S::TYPE_SCALAR))
          );
  }
  
  public function testCheckType_Scalar_by_Default_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('scal'=>array('sasd', 121321 , null)),
           array('scal'=>array('type'=>O4S::TYPE_STRING))
          );
  }
  
  public function testCheckType_File_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('file'=>123),
           array('file'=>array('type'=>O4S::TYPE_FILE))
          );
  }
  
  public function testCheckType_Array_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('arr'=>'123'),
           array('arr'=>array('type'=>O4S::TYPE_ARRAY))
          );
  }
  
  public function testCheckType_Enum_Fail()
  {
    $this->setExpectedException('\LogicException','',202);
    self::$cnt->validateTest(
            array('enum'=>'123'),
           array('enum'=>array('type'=>O4S::TYPE_ENUM, 'options'=>array('321','asd',111)))
          );
  }
  
  public function testCheckType_All_Pass()
  {
    self::$cnt->validateTest(
            array(
                're'=>'12+42=1243', 
                'email'=>'what@t.h.e.he.ll',
                'int'=>'1242',
                'num'=>'234234.12',
                'float'=>'4241.12',
                'date'=>'2012-02-29',
                'bool'=>'1',
                'str'=>'asdd',
                'str1'=>123124,
                'str2'=>null,
                'file'=>array('tmp_name'=>'','error'=>4),
                'arr'=>array(1,2,3),
                'emptyarr'=>array(),
                'enum'=>'fsdf',
                ),
            array(
                're'=>array('type'=>O4S::TYPE_REGEXP, 'regexp'=>'/^\d{2}\+\d{2}\=\d{4}$/'),
                'email'=>array('type'=>O4S::TYPE_EMAIL),
                'email2'=>array('type'=>O4S::TYPE_EMAIL),
                'int'=>array('type'=>O4S::TYPE_INTEGER),
                'num'=>array('type'=>O4S::TYPE_NUMBER),
                'bool'=>O4S::TYPE_BOOLEAN,
                'float'=>array('type'=>O4S::TYPE_FLOAT),
                'date'=>array('type'=>O4S::TYPE_DATESTRING),
                'str'=>array('type'=>O4S::TYPE_STRING),
                'str1'=>O4S::TYPE_STRING,
                'str2'=>array('type'=>O4S::TYPE_STRING),
                'file'=>array('type'=>O4S::TYPE_FILE),
                'arr'=>array('type'=>O4S::TYPE_ARRAY),
                array('name'=>'enum', 'type'=>O4S::TYPE_ENUM, 'options'=>array('fsdf','dssd','123')),
                'emptyarr'=>array(),
                )
          );
  }

  public static function tearDownAfterClass()
   {
     //self::removeTestUser();
   }
                            
}

?>