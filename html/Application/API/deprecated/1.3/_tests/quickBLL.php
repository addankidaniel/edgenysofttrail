<?php

namespace ClickBlocks\APITest\General;

use ClickBlocks\Core,
    ClickBlocks\DB;

require_once('PHPUnit/Framework/TestCase.php');

/**
 * @group  QuickBLLTest
 */
class QuickBLLTest extends \PHPUnit_Framework_TestCase
{
  private static $vals = array(
        'this'=>array('one'=>1, 'two'=>2),
        'that'=>array('three'=>3, 'four'=>4),
        );


  /*public function test_quickColumnFamily_supercolumns()
  {
    $cf = new \ClickBlocks\DB\QuickColumnFamily('key', self::$vals, true);
    self::assertEquals(self::$vals, $cf->getValues());
    $subcf = $cf->that;
    self::assertTrue($subcf instanceof \ClickBlocks\DB\QuickColumnFamily);
    self::assertEquals(self::$vals['that'], $subcf->getValues());
    self::assertEquals('key', $subcf->getKey());
  }*/
  
  public function test_quickBLL_navigationProperty()
  {
    $cf = new DB\QuickBLL(array(
        'some'=>'123', 
        'prop'=>'asdasd', 
        'nav' =>array('his' => '31', 'property'=>'dsf'),
      ), true);
    
    self::assertEquals(31, $cf->nav->his);
    self::assertEquals('dsf', $cf->nav->property);
  }
}

?>
