<?php

namespace ClickBlocks\API\v1_3\Logic;
use ClickBlocks\API\v1_3,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;

/**
 * Base controller class for eDepo API
 *
 * @author Vladislav
 */
class Edepo extends \ClickBlocks\API\v1_3\APIControllerExt implements \ClickBlocks\API\IEdepoBase {
  private $noAuthMethods;
  /**
   * @var \ClickBlocks\Core\Register;
   */
  protected $reg;
  protected $sessionLifetime;
  /**
   * Currently authenticated user
   * @var \ClickBlocks\DB\Users
   */
  protected $user;

  /**
   * @var \ClickBlocks\DB\DepositionAttendees
   */
  protected $attendee;

  protected $unitTestMode = false;

  /**
   * @var array of mock objects
   */
  private $mocks;

  /**
   * @var \ClickBlocks\DB\Orchestra
   */
  protected $orc;
  /**
   * @var \ClickBlocks\DB\Service
   */
  protected $svc;

  /**
   * @var array
   */
  protected $configApi;

  /**
   * @var object
   */
  protected $config;

  /**
   * @var type \ClickBlocks\Utils\Timer
   */
  protected $timer;

  /**
   * @var \ClickBlocks\Utils\FileSystem
   */
  protected $fs;
  /**
   * DB entity name, most closely related to current controller; if set, $this->orc and $this->svc is automatically created on init_();
   * @var string
   */
  protected static $relatedEntity = null;

  const SESSION_STORAGE = 'api_auth';

  const USER_TYPE_GUEST = 'guest';

  const TYPE_STRING = 13;
  const TYPE_SCALAR = 9;
  const TYPE_INTEGER = 1;
  const TYPE_FLOAT = 2;
  const TYPE_NUMBER = 3;
  const TYPE_BOOLEAN = 4;
  const TYPE_DATESTRING = 5;
  const TYPE_REGEXP = 6;
  const TYPE_EMAIL = 7;
  const TYPE_FILE = 8;
  const TYPE_ARRAY = 10;
  const TYPE_ENUM = 11;
  const TYPE_MONEY = 12;
  const TYPE_TIMESTAMP = 14;

  const ERR_ATTENDEE_LIMIT = 105;
  const ERR_BANNED_IN_DEPO = 106;

   public function __construct() {
     $this->reg = Core\Register::getInstance();
     $this->config = $this->reg->config;
     $this->configApi = $this->reg->config->api;
     $this->sessionLifetime = (int)$this->reg->config->api['sessionLifetime'] ?: 60;
     $this->timer = new Utils\Timer();
     $this->fs = new Utils\FileSystem();
   }

   protected function getDBAliases_()
   {
      return array('db0');
   }

   public final function init_($method)
   {
      if (!$this->reg) throw new \Exception('parent constructor is not called for '.__CLASS__);
      if (!$this->unitTestMode) try {
         $orm = DB\ORM::getInstance();
         foreach ($this->getDBAliases_() as $dbalias) $orm->getDB($dbalias)->catchException = true;
      } catch (\Exception $e) {
         throw new \LogicException('Database is not available', 500);
      }
      if ($this->reg->config->api['mockHTTPS'] && !$this->unitTestMode) $_SERVER['HTTPS'] = 'on';
      //$this->timerStart('Controller');

      // check authorization
      $flag = true;
      $sKey = @$this->params['sKey'];
      $noAuthMethods = (array)$this->getNoAuthMethods();
      foreach ($noAuthMethods as &$met)
        $met = strtolower($met);
      if (!$sKey || !is_string($sKey)) {
        $flag = false;  // session key not provided
      } elseif (!$this->unitTestMode) {
        $this->requireProtocolHTTPS();
        $orc = $this->getOrchestra('APISessions');
        $orc->deleteExpired($this->sessionLifetime);
        $session = $orc->getBySKey($sKey);
        if ($session->isInstantiated()) {
          if ($session->attendeeID) {
            $this->attendee = $session->depositionAttendees[0];
          } else {
            $this->user = $session->users[0];
          }
        } else {
          $flag = false; // session not found
        }
      }
      if (!$flag && ($sKey || !in_array(substr(strtolower($method), 4), $noAuthMethods)))
        $this->except('Authorization failed', 401);

      $ent = $this->getRelatedEntity();
      if ($ent) {
        $this->orc = new LazyORMEntity($this, self::ORM_ENTITY_ORCHESTRA, $ent); //$this->getOrchestra($ent); //
        $this->svc = new LazyORMEntity($this, self::ORM_ENTITY_SERVICE, $ent); //$this->getService($ent); //
      }
   }

   protected function getRelatedEntity() { }

   /**
    * @return array of method names, that will not require sKey authentication (and as result, will not have user context
    */
   protected function getNoAuthMethods() {
      return array('test','clearLog');
   }

   public function finalize_($method) {
   }

   public function attachMock_(array $mockOrchestras = array(), array $mockServices = array(), $user = null, $fs = null) {
      $this->unitTestMode = true;
      foreach ($mockOrchestras as $k=>$v)
        $this->mocks['orc'][strtolower($k)] = $v;
      foreach ($mockServices as $k=>$v)
        $this->mocks['svc'][strtolower($k)] = $v;
      if ($user)
        $this->user = $user;
      if ($fs)
        $this->fs = $fs;
   }

   //public final function setUnitTestMode_($user) { }

   protected static function validateParam($value, &$info)
   {
      $o = array('field'=>$value);
     return JSONObjectValidator::validateField($o, 'field', $info);
   }

   /**
    * checks type of $this->user
    * @param array/string $types array of user types (or single type)
    * @throws \LogicException if user type not in $types
    */
   protected function restrictCurrentUserType($types, $code = 403)
   {
     if (!in_array($this->user->typeID, (array)$types))
       throw new \LogicException('User type doesn\'t match required: '.implode(' or ', (array)$types), $code);
   }

   protected function requireProtocolHTTPS()
   {
     if (!$_SERVER['HTTPS'] || $_SERVER['HTTPS']=='off')
        $this->except('HTTP protocol not supported for this type of request!', 505);
   }


   protected function updateCurrentUser()
   {
     $this->getService('Users')->update($this->user);
   }

   /**
    * Check presense of parameters
    * @param array $list  Array of parameters
    * @param type $notEmpty
    * @throws API\ParameterRequiredException If not specified
    */
  protected function requireParams(array $list, $notEmpty = true, $code = 201)
  {
    try {
      parent::requireParams((array)$list, $notEmpty);
    } catch (\LogicException $e) {
      throw new API\ParameterRequiredException($e->getMessage(), $code);
    }
  }

   public static function validateObject(&$obj, array $fields, $name)
   {
      return JSONObjectValidator::validateObject($obj, $fields, $name);
   }

  /**
   * Validates set of function parameters; should be called at start of every controller method
   * @param array $fields List of maps with keys:
   *   name - string name of field to validate
   *   type - string type to check against
   *   required - bool whether it must be evaluated as true
   *   expression - regular expression to match against, for regexp type
   * @throws API\ParameterValidationException if value is present but not match validation
   * @throws API\ParameterRequiredException if one or more required parameters are missing
   */
  protected function validateParams(array $fields)
  {
    return JSONObjectValidator::validateObject($this->params, $fields, 'data');
  }

  /**
   *
   * @param array $dbobject object from call to Orchestra or BLL->getValues() with raw string DB data
   * @param array $fields array describing fields and types
   * @param boolean $preserveOtherFields if true, fields the present in $dbobject but omitted in $fields will be preserved
   * @return type
   */
   protected function db2jsonObject(array $dbobject, array $fields, $preserveOtherFields = false)
   {
      $object = array();
      foreach ($fields as $field=>$info) {
         if (!is_array($info)) $info = array('type'=>$info);
         elseif (!$info['type'] && $info[0]) $info['type'] = $info[0];
         $dbfield = $info['bind'] ?: $field;
         $val = $dbobject[$dbfield];
         if ($info['bind']) unset($dbobject[$info['bind']]);
         //if (isset($info['nullValue'])) $this->return['test'] = $info['nullValue'];
         if (isset($info['nullValue']) && $val === (string)$info['nullValue']) $val = NULL;
         if ((bool)$info['type'] && $info['type']!=self::TYPE_STRING && $val === '') $val = NULL;
         if ($val !== NULL) switch ($info['type']) {
            case self::TYPE_INTEGER:
               $val = (int)$val;
               break;
            case self::TYPE_NUMBER: case self::TYPE_FLOAT:
               $val = (float)$val;
               break;
            case self::TYPE_BOOLEAN:
               $val = (bool)$val;
               break;
            case self::TYPE_MONEY:
               $val = (int)((float)$val*100);
               break;
            case self::TYPE_TIMESTAMP:
               if (!is_numeric($val)) $val = Utils\DT::sql2date($val, 'U', strlen($val)<12);
               $val = (int)$val;
               break;
         }
         $object[$field] = $val;
      }
      if ($preserveOtherFields) $object += $dbobject;
      return $object;
   }

  /**
   * @return  string  session ID
   */
  protected function startSession($ID, $userType) {
    if ($this->unitTestMode) return;
    $hash = hash('sha512', microtime().$this->reg->config->api['secretSalt']);
    $this->deleteSession($ID, $userType);
    $session = $this->getBLL('APISessions');
    if ($userType == self::USER_TYPE_GUEST) {
      $session->attendeeID = $ID;
    } else {
      $session->userID = $ID;
    }
    $session->sKey = $hash;
    $session->insert();
    return $hash;
  }

  protected function deleteSession($ID, $userType) {
    if ($this->unitTestMode) return;
    $method = ($userType == self::USER_TYPE_GUEST) ? 'deleteByAttendee' : 'deleteByUser';
    return $this->getOrchestra('APISessions')->$method($ID);
  }

  /**
   * Test method to check if API works
   */
  public function api_test($pr) {
      if ($pr['error'])
        throw new \LogicException($pr['error'], (int)$pr['code']);
      echo 'Something echo ';
      return array('status'=>'All is fine!', 'youSent'=>$pr);
  }

  public function api_clearLog() {
     $log = Core\IO::dir('temp').'/api_log.txt';
     if (is_file($log)) unlink($log);
     else return 'Log already deleted';
  }

  /**
   *
   * @param string $message Debug message
   * @param int $code Error code
   * @param mixed $data Additional error information for Client
   * @throws LogicException
   */
  protected function except($message, $code = 0, $data = null) {
    throw new \ClickBlocks\API\v1_3\MethodException($message, $code, $data);
  }


  /**
   * @return \ClickBlocks\DB\OrchestraClients
   */
  protected function getOrchestraClients() {
     return $this->getOrchestra('Clients');
  }

  /**
   * @return \ClickBlocks\DB\OrchestraDepositions
   */
  protected function getOrchestraDepositions() {
     return $this->getOrchestra('Depositions');
  }
  /**
   * @return \ClickBlocks\DB\OrchestraUsers
   */
  protected function getOrchestraUsers() {
     return $this->getOrchestra('Users');
  }
  /**
   * @return \ClickBlocks\DB\OrchestraFiles
   */
  protected function getOrchestraFiles() {
     return $this->getOrchestra('Files');
  }
  /**
   * @return \ClickBlocks\DB\OrchestraCases
   */
  protected function getOrchestraCases() {
     return $this->getOrchestra('Cases');
  }
  /**
   * @return \ClickBlocks\DB\OrchestraFolders
   */
  protected function getOrchestraFolders() {
     return $this->getOrchestra('Folders');
  }
  /**
   * @return \ClickBlocks\DB\OrchestraDepositionAttendees
   */
  protected function getOrchestraDepositionAttendees() {
     return $this->getOrchestra('DepositionAttendees');
  }

  public function getORMEntity_($shortName, $type) {
    $classPrefix = '\ClickBlocks\DB\\'.($type == self::ORM_ENTITY_ORCHESTRA ? 'Orchestra' : 'Service');
    $shortName = strtolower($shortName);
    if (isset($this->mocks[$type][$shortName]) && is_object($mock = $this->mocks[$type][$shortName])) {
      return $mock;
    } elseif (is_string($shortName)) {
      if ($this->unitTestMode)
        throw new \Exception('Attempt to access physical layer in Controller Unit Test. Add $this->setUpApiMock("'.$classPrefix.$shortName.'") to setUp() method.');
      $name = $classPrefix.$shortName;
      return new $name();
    } else
      throw new API\APIException('ORM entity must be string or object!');
  }

  /**
   * @param string $name entity name (Customers, Merchants, etc.)
   * @return \ClickBlocks\DB\Orchestra
   * @throws \Exception
   */
  protected function getOrchestra($name) {
    return $this->getORMEntity_($name, self::ORM_ENTITY_ORCHESTRA);
  }

  /**
   * @param string $name entity name
   * @return \ClickBlocks\DB\Service
   * @throws \Exception
   */
  protected function getService($name) {
    return $this->getORMEntity_($name, self::ORM_ENTITY_SERVICE);
  }

  /**
   * @return \ClickBlocks\Utils\NodeJS
   */
  protected function getNodeJS() {
    if ($this->unitTestMode) return $this->mocks['nodejs'];
    else {
       $node = new Utils\NodeJS( TRUE );
       if ($this->config->isDebug) {
         $logCallback = function($text) {
            $text = 'Calling NodeJS: '.$text;
            @file_put_contents(Core\IO::dir('temp').'/api_log.txt', $text.PHP_EOL, FILE_APPEND);
         };
         $node->setLogCallback($logCallback);
       }
       return $node;
    }
  }

  /**
   * @param string $name entity name
   * @return \ClickBlocks\DB\BLLTable
   * @throws \Exception
   */
  protected function getBLL($name) {
    if ($this->unitTestMode) {
      return new DB\QuickBLL();
    } else {
      $cls = '\ClickBlocks\DB\\'.$name;
      return new $cls;
    }
  }

  protected function isUserMember() {
    return (bool)$this->user && (bool)$this->user->ID;
  }

  protected function isUserGuest() {
    return !$this->isUserMember() && (bool)$this->attendee && (bool)$this->attendee->name;
  }

  protected function validateAttendeeLimit(DB\Depositions $depo)
  {
	 /* deprecated per ED-23
     if ($depo->getAttendeeCount()>=(int)$depo->attendeeLimit) {
        $this->getNodeJS()->sendPostCommand('notify_attendee_limit', null, array('userID'=>$depo->getOwnerID(), 'depositionID'=>$depo->ID));
        $this->except ('Attendee limit reached for this deposition ('.(int)$depo->attendeeLimit.')!', self::ERR_ATTENDEE_LIMIT);
     }
	 */
  }

  /**
   *
   * @param type $userID
   * @param int|DB\Depositions $depoID
   * @return \ClickBlocks\DB\DepositionAttendees
   */
  public function userAttendToDeposition($userID, DB\Depositions $depo)
  {
     $attendance = foo(new DB\OrchestraDepositionAttendees())->getForUser($depo->ID, $userID);
     if (!$attendance->ID) {
        $mainDepo = ($depo->parentID ? $depo->getParentDeposition() : $depo);
		//deprecated per ED-23
        //$this->validateAttendeeLimit($mainDepo);
        if ($depo->finished) throw new \LogicException('Cannot attend, deposition is finished.', 1003);
        $attendance->userID = $userID;
        $attendance->depositionID = $depo->ID;
        $attendance->insert();
        $attendance->onFirstAttendedDepo($depo);
     }
     if ($attendance->banned) $this->except('This user was blocked from attending this deposition!', self::ERR_BANNED_IN_DEPO);
     return $attendance;
  }
}

?>
