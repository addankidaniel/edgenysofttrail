<?php

namespace ClickBlocks\UnitTest\API;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\Exceptions,
    ClickBlocks\Utils\DT,
    ClickBlocks\API,
    ClickBlocks\Core,
    ClickBlocks\UnitTest;

abstract class TestCase extends UnitTest\TestCase
{
  protected $controller;
  
  public function setUp() 
  {
//    $reg = Core\Register::getInstance();
//    $reg->loader->onPreload = '\Clickblocks\Tests\BaseTest::onPreloadClass';
  }
  
  /**
   * Imitates method execution, as it is done by API
   * @param type $method
   * @param type $params
   * @param type $mock  
   * @return mixed everything returned by controller
   */
  protected function invokeMethod($method = null, $params = null, $mockOrchestra = array(), $mockService = array(), $mockController = null) 
  {
    if (!$method)
      $method = $this->methodName;
    if (!$method)
      throw new \Exception('Method not defined!');
    $method = strtolower($method);
    $this->controller = $controller = ($mockController ?: new $this->controllerName());
    $controller->attachMock_((array)$mockOrchestra, (array)$mockService);
    return $this->impersonateApi($controller, $method, $params);
  }
  
  protected final function impersonateApi(API\IAPIControllerBase $controller, $method, $params) 
  {
    $controller->params = $params;
    $controller->init_($method);
    $return = $controller->$method($params);
    if ($return === NULL)
      $return = $controller->return;
    
    if ($controller) {
      $controller->finalize_($method);
      $controller->afterFlush_();
    }
    return $return;
  }


  public function tearDown()
  {
    $this->controller = null;
  }

 /* protected function invokeMethodHttp($method, array $params = null, array $files = null) 
  {
    $url = Core\Register::getInstance()->config->phpunit['apiurl'];
    if (!$url)
      throw new \Exception("[phpunit] apiurl setting is empty!");
    $entity = explode('\\', $this->controllerName);
    $entity = end($entity);
    $url .= $entity.'/'.$method;
    if ($files)
      $url .= '?data='.urlencode(json_encode($params));
    //echo $url;
    $c = curl_init($url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_HEADER, false);
    if ($files) {
      foreach ($files as $k=>$v) 
        $files[$k] = '@'.Core\IO::dir('testfiles').'/'.$v;
      curl_setopt($c, CURLOPT_POSTFIELDS, $files);
    } else {
      curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($params));
      curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
    }
    $result = curl_exec($c);
    if ($result === FALSE) {
      throw new \Exception('Curl error #'.curl_errno($c).': "'.curl_error($c).'"');
    }
    curl_close($c);
    return json_decode($result, true);
  }*/
  
  public static function onPreloadClass($class) 
  {
    if (isset(self::$convertClass[$class]))
        return self::$convertClass[$class];
    else return $class;
  }
  
  protected function expectApiError($code, $msg = '') 
  {
    $this->setExpectedException('\LogicException', $msg, $code);
  }
  
}

?>