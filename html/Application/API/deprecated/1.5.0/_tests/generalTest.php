<?php

namespace ClickBlocks\APITest;;

use ClickBlocks\DB,
    ClickBlocks\Utils,
    ClickBlocks\Exceptions,
    ClickBlocks\Utils\DT,
    ClickBlocks\Core;

if(!defined('PHPUnit_MAIN_METHOD')){
    define('PHPUnit_MAIN_METHOD', 'generalTests::main');
}
require_once(__DIR__ . '/../../connect_phpunit.php');

class generalTests{
    public static function main() {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }
    public static function suite(){
        $ts= new \PHPUnit_Framework_TestSuite('User Classes');
        $ts->addTestSuite('ClickBlocks\APITest\General\testTest');
        //$ts->addTestSuite('ClickBlocks\APITest\General\validateParamsTest');
        //$ts->addTestSuite('ClickBlocks\APITest\General\methodTestTest');
        //$ts->addTestSuite('ClickBlocks\APITest\General\AuthorizationTest');
        //$ts->addTestSuite('ClickBlocks\APITest\General\QuickBLLTest');
        return $ts;
    }
}
if(PHPUnit_MAIN_METHOD == 'AppTests::main')
    generalTests::main();



?>