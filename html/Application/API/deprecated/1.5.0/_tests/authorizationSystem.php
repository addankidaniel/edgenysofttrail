<?php                

namespace ClickBlocks\APITest\General;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\API\Logic\O4S,
    ClickBlocks\APITest,
    ClickBlocks\APITest\BaseTest,
    ClickBlocks\Exceptions;

class TestController2 extends O4S
{
  protected $noAuthMethods = array('testPublic');
  public $mockService;
  
  protected function getDBAliases_() {
     return array();
  }

  public function testPublic($p)
  {
    
  }
  
  public function testStartSession($id) {
    return $this->startSession($id, 'C');
  }
  
  public function testDeleteSession($id) {
    return $this->deleteSession($id);
  }


  public function testSecure($p)
  {
    return $this->user->userID;
  }
  
  public function getORMEntity_($w, $type)
  {
    if ($type!='svc') throw new \Exception('Only Services in this test!');
    return $this->mockService[$w];
  }
}



/**
 * @group AuthorizationSystem
 */
class AuthorizationTest extends BaseTest
{
  private $cnt;
  private static $sessions;
  private static $user = array(
      'userID' => 123,
      'typeID' => 'C'
  );
  
  private static $user2 = array(
      'userID' => 4,
      'typeID' => 'S'
  );

  private static $userID;
  private static $sKey;
  private static $realCache;
  
  public function __construct() 
  {
    
  }

  public static function getService_()
  {
    //return 
  }


  public static function setUpBeforeClass()
  {
    self::$sessions = array();
    self::$sessions['key1'] = array('id'=>1, 'type'=>'C', 'expire'=> PHP_INT_MAX);
    self::$sessions['key2'] = array('id'=>2, 'type'=>'C', 'expire'=> PHP_INT_MAX);
    self::$sessions['key3'] = array('id'=>4, 'type'=>'S', 'expire'=> PHP_INT_MAX);
    self::$sessions['key_expired'] = array('id'=>3, 'expire'=> time() - 1); // expired
    
  }
  
  public static function setSessions($k, $v, $exp)
  {
    if ($k != O4S::SESSION_STORAGE)
      return self::$realCache->set($k, $v, $exp);
    //self::assertEquals(O4S::SESSION_STORAGE, $k);
    self::assertGreaterThan(0, $exp);
    self::$sessions = $v;
  }
  
  public static function getSessions($k)
  {
    //self::assertEquals(O4S::SESSION_STORAGE, $k);
    if ($k != O4S::SESSION_STORAGE)
      return self::$realCache->get($k);
    return self::$sessions;
  }

  public function setUp() {
    parent::setUp();
    
    $reg = Core\Register::getInstance();
    if (!self::$realCache) { // swap real cache with mock
      self::$realCache = $reg->cache;
    }
    $cache = $this->getMock('\ClickBlocks\Cache\Cache');
    //$cache->expects(self::any())->method('get')->will(self::returnValue(self::$sessions));
    //->with(self::equalTo(O4S::SESSION_STORAGE))->will(self::returnValue(self::$sessions));
    $cache->expects(self::any())->method('get')->will(self::returnCallback(array(__CLASS__, 'getSessions')));
    $cache->expects(self::any())->method('set')->will(self::returnCallback(array(__CLASS__, 'setSessions')));
    $reg->cache = $cache;
    
    $this->cnt = new TestController2();
    $this->svc = $this->getMockBuilder('\ClickBlocks\DB\ServiceUsers')->disableOriginalConstructor()->getMock();
    $this->cnt->mockService = array('Users' => $this->svc);
    unset($_SERVER['HTTPS']);
    $reg->config->api['mockHTTPS'] = false;
    //$this->svc-
  }

  private function invoke_O4S_init($method, $params = array(), $https = false)
  {
    $this->svc->expects(self::any())->method('getByID')->with(self::greaterThan(0))->will(self::returnValue(new DB\QuickBLL(self::$user)));
    if ($https)
      $_SERVER['HTTPS'] = 'on';
    $this->cnt->params = $params;
    $this->cnt->init_($method);
  }
  
  public function test_sKey_over_HTTPS_secure_method()
  {
    $this->assertArrayHasKey('key_expired', self::$sessions);
    $this->invoke_O4S_init('testSecure', array('sKey'=>'key1'), true);
    $result = $this->cnt->testSecure(array());
    $this->assertEquals(123, $result);
    $this->assertArrayNotHasKey('key_expired', self::$sessions);
  }

  public function test_sKey_over_HTTP_public_method()
  {
    $this->expectApiError(505);
    $this->cnt->params = array('sKey'=>'key1');
    $this->cnt->init_('testPublic');
  }
  
  public function test_sKey_over_HTTP_secure_method()
  {
    $this->expectApiError(505);
    $this->cnt->params = array('sKey'=>'key1');
    $this->cnt->init_('testSecure');
  }
  
  public function test_sKey_over_HTTPS_public_method()
  {
    $_SERVER['HTTPS'] = 'on';
    $this->svc->expects(self::once())->method('getByID')->with(self::equalTo(2))->will(self::returnValue(new DB\QuickBLL(self::$user)));
    $this->cnt->params = array('sKey'=>'key2');
    $this->cnt->init_('testPublic');
  }
  
  /*public function test_sKey_for_Salesperson()
  {
    $_SERVER['HTTPS'] = 'on';
    $this->svc->expects(self::never())->method('getByID');
    $svc2 = $this->getMockBuilder('\ClickBlocks\DB\ServiceSalespersons')->disableOriginalConstructor()->getMock();
    $svc2->expects(self::once())->method('getByID')->with(self::equalTo(4))->will(self::returnValue(new DB\QuickBLL(self::$user2)));
    $this->cnt->mockService['Salespersons'] = $svc2;
    $this->cnt->params = array('sKey'=>'key3');
    $this->cnt->init_('testPublic');
  }*/
  
  public function test_no_sKey()
  {
    $this->expectApiError(401);
    $_SERVER['HTTPS'] = 'on';
    $this->cnt->params = array('sKey'=>'');
    $this->cnt->init_('testSecure');
  }
  
  public function test_invalid_sKey()
  {
    $this->expectApiError(401);
    $_SERVER['HTTPS'] = 'on';
    $this->cnt->params = array('sKey'=>'key123123');
    $this->cnt->init_('testSecure');
  }
  
  public function test_valid_sKey_no_user()
  {
    $this->expectApiError(0, 'non-existant user');
    $this->svc->expects(self::once())->method('getByID')->with(self::equalTo(1))->will(self::returnValue(null));
    $_SERVER['HTTPS'] = 'on';
    $this->cnt->params = array('sKey'=>'key1');
    $this->cnt->init_('testSecure');
  }
  
  public function test_startSession()
  {
    self::$userID = 555;
    self::$sKey = $this->cnt->testStartSession(self::$userID);
    self::assertTrue(strlen(self::$sKey)==128, self::$sKey);
    self::assertEquals(self::$userID, self::$sessions[self::$sKey]['id']);
  }
  
  /**
   * @depends test_startSession
   */
  public function test_deleteSession()
  {
    $this->cnt->testDeleteSession(self::$userID);
    self::assertArrayNotHasKey(self::$sKey, self::$sessions);
  }

  public static function tearDownAfterClass()
  {
     Core\Register::getInstance()->cache = self::$realCache;
     self::$realCache = null;
  }
                            
}

?>