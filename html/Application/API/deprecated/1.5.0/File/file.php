<?php

namespace ClickBlocks\API\v1_5_0\Logic;

use ClickBlocks\API\v1_5_0,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;

define( 'MAX_ATTACHMENT_SIZE', (15*1024*1024) );	// limit the size of file attachments for a single email; arbitrary value, may need to fine tune
define( 'DROP_FILE_AFTER_EMBED', true );
define( 'FORCE_SINGLE_FILE', false );

class File extends Edepo
{
	public function api_download($p) {
		$this->requireParams(array('fileID'));
		$file = $this->getService('Files')->getByID($p['fileID']);
		if (!$file->ID)
			$this->except('File not found', 1002);
		if ($this->isUserMember()) { // check access for Member
			$folder = $file->folders[0];
			$depoID = $folder->depositionID;
			$attended = $this->getOrchestraDepositionAttendees()->checkUserAttendedDepo($depoID, $this->user->ID);
			$hasAccess = (
				($attended && ($file->createdBy==$this->user->ID || $folder->class == 'Exhibit'))
				|| ($file->createdBy==$folder->depositions->createdBy && $this->getOrchestraDepositions()->isTrustedUser($this->user->ID, $depoID))
			);
			if (!$hasAccess) $this->except('Access denied', 403);
		} else { // check access for Guest
			if (!$file->folders->class == 'Exhibit' && $file->folders->class != 'CourtesyCopy' && !$this->getOrchestra('FileShares')->checkAttendeeAccessToFile($this->attendee->ID, $file->ID))
				$this->except('Access denied', 403);
		}
		if (isset( $p['embed'] ) && $p['embed']) {
			$this->embedDownload( $file->getFullName() );
		} else {
			$this->flushFile( $file->getFullName() );
		}
	}

	// upload file to user dir
	public function api_upload($p)
	{
		if (!$this->isUserMember()) $this->except ('Only member user access allowed!');
		$this->validateParams(array(
			'depositionID' => array('type'=>self::TYPE_NUMBER, 'req'=>true),
			'folderID' => array('type'=>self::TYPE_NUMBER, 'req'=>false),
			'overwrite' => array('type'=>self::TYPE_BOOLEAN, 'req'=>false),
			'file' => array('type'=>self::TYPE_FILE, 'req'=>true),
			'sourceFileID' => array( 'type'=>self::TYPE_NUMBER, 'req'=>false )
		));
		//\ClickBlocks\Debug::ErrorLog( "upload fileID: {$p['sourceFileID']}" );

		$depo = $this->getService('Depositions')->getByID($p['depositionID']);
		if (!$depo->ID) $this->except('Deposition not found by ID: '.$p['depositionID'], 404);
		if ($p['folderID']) {
			$folder = $this->getService('Folders')->getByID($p['folderID']);
			if (!$folder->ID) $this->except ('Folder not found by ID: '.$p['folderID'], 404);
			if ($folder->createdBy != $this->user->ID) $this->except ('Access denied to folder!', 403);
		}
		if (!$p['folderID'] || $folder->class == 'Exhibit') {
			$folder = $this->getOrchestraFolders()->getPersonalFolderObject($depo->ID, $this->user->ID);
			if (!$folder->ID) { // first time saving to personal folder - create it
				$folder->depositionID = $depo->ID;
				$folder->createdBy = $this->user->ID;
				$folder->name = $this->config->logic['personalFolderName'];
				$folder->isPrivate = 1;
				$folder->class = 'Personal';
				$folder->insert();
			}
		}
		$info = $_FILES['file'];
		if (!is_array($info['tmp_name'])) {
			foreach ($info as $k=>$v) $info[$k] = array($v);
		}
		$this->return['files'] = array();

		//source fileID -- for Courtesy Copy
		if( !isset( $p['sourceFileID'] ) ) $p['sourceFileID'] = 0;
		$orchFiles = $this->getOrchestraFiles();
		$sourceFileID = $orchFiles->getSourceFileID( $p['sourceFileID'] );

		foreach( $info['tmp_name'] as $k => $v ) {
			if ($info['error'][$k] != 0) $this->except('Incorrect file upload, error: '.$info['error'][$k]);
			if( $p['overwrite'] ) {
				//$this->validateParams( array( 'fileID' => array( 'type'=>self::TYPE_NUMBER, 'req'=>true ) ));	//ED-655; fixes for offline file upload
				if( isset( $p['fileID'] ) && $p['fileID'] ) {
					$file = foo($this->getService( 'Files' ))->getByID( $p['fileID'] );
					move_uploaded_file( $info['tmp_name'][$k], $file->getFullName() );
					$file->created = 'NOW()';
					$file->sourceID = $sourceFileID;
					$file->save();
				} else {
					//BEGIN v1.5.0 Offline files upload hack
					$fileID = $orchFiles->getFolderFileIDByName( $folder->ID, $info['name'][$k] );
					if( $fileID ) {
						//exists -- overwrite
						$file = foo( $this->getService( 'Files' ) )->getByID( $fileID );
						move_uploaded_file( $info['tmp_name'][$k], $file->getFullName() );
						$file->created = 'NOW()';
						$file->save();
					} else {
						//insert
						$file = $this->getBLL( 'Files' );
						$file->name = $info['name'][$k];
						$file->folderID = $folder->ID;
						$file->createdBy = $this->user->ID;
						$file->sourceID = $sourceFileID;
						$file->setOverwrite( $p['overwrite'] );
						$file->setSourceFile( $info['tmp_name'][$k], TRUE );
						$file->insert();
					}
				}
			} else {
				$file = $this->getBLL('Files');
				$file->name = $info['name'][$k];
				$file->folderID = $folder->ID;
				$file->createdBy = $this->user->ID;
				$file->sourceID = $sourceFileID;
				$file->setOverwrite($p['overwrite']);
				$file->setSourceFile($info['tmp_name'][$k], true);
				$file->insert();
			}
			$this->return['folderID'] = $folder->ID; // for backward compatibility
			$this->return['fileID'] = $file->ID;     // for backward compatibility
			$this->return['files'][] = array( 'ID'=>$file->ID, 'folderID'=>$folder->ID );
		}
	}


	// upload witness annotations to deposition owner personal folder
	public function api_upload_witness( $p )
	{
		if ( !$this->isUserGuest() ) $this->except( 'Only for guest users!' );

		$this->validateParams(
			array(
				'depositionID' => array( 'type' => self::TYPE_NUMBER, 'req'=>true ),
				'file' => array( 'type' => self::TYPE_FILE, 'req'=>true ),
				'sourceFileID' => array( 'type'=>self::TYPE_NUMBER, 'req'=>false )
			)
		);

		$depositionID = (int)$p['depositionID'];


		$deposition = $this->getService( 'Depositions' )->getByID( $depositionID );
		if ( !$deposition->ID ) $this->except( "Deposition not found by ID: {$depositionID}", 404 );

		$speakerID = ($deposition->speakerID) ? $deposition->speakerID : $deposition->ownerID;

		$folder = $this->getOrchestraFolders()->getPersonalFolderForUserInDeposition( $deposition->ID, $speakerID );
		if (!$folder->ID) $this->except( "Unable to get Folder for UserID: {$speakerID}", 404 );

		//\ClickBlocks\Debug::ErrorLog( "Deposition: {$deposition->ID} SpeakerID: {$speakerID} Folder: {$folder->ID} '{$folder->name}'" );

		$uploads = $_FILES['file'];
		if ( !is_array( $uploads['tmp_name'] ) ) {
		   foreach ($uploads as $k => $v) {
			   $uploads[$k] = array( $v );
		   }
		}

		$this->return['files'] = array();

		//source fileID -- for Courtesy Copy
		if( !isset( $p['sourceFileID'] ) ) $p['sourceFileID'] = 0;
		$orchFiles = $this->getOrchestraFiles();
		$sourceFileID = $orchFiles->getSourceFileID( $p['sourceFileID'] );

		foreach( $uploads['tmp_name'] as $k => $v ) {
			if( $uploads['error'][$k] != 0) $this->except( 'Incorrect file upload, error: ' . $uploads['error'][$k] );
			$file = $this->getBLL('Files');

			$fileParts = pathinfo( $uploads['name'][$k] );
			$file->name = "{$fileParts['filename']}_WitnessAnnotations.{$fileParts['extension']}";
			$file->folderID = $folder->ID;
			$file->createdBy = $speakerID;
			$file->sourceID = $sourceFileID;
			$file->setSourceFile( $uploads['tmp_name'][$k], true );
			$file->insert();
			$this->return['folderID'] = $folder->ID; // for backward compatibility
			$this->return['fileID'] = $file->ID;     // for backward compatibility
			$this->return['files'][] = array( 'ID' => $file->ID, 'folderID' => $folder->ID );
			// \ClickBlocks\Debug::ErrorLog( "File: {$file->ID} Name: '{$file->name}'" );
		}

		$this->getNodeJS()->sendPostCommand( 'witness_uploaded', null, array( 'depositionID' => $deposition->ID, 'notifyUsers' => array( $speakerID ), 'folderID' => $folder->ID, 'folderName' => $folder->name ) );
	}

	public function api_offlineUpload( $p )
	{
		if( !$this->isUserMember() ) $this->except( 'Only member user access allowed!', 403 );
		$this->validateParams( [
			'depositionID' => ['type' => self::TYPE_NUMBER, 'req' => TRUE],
			'metadata' => ['type'=> self::TYPE_ARRAY, 'req' => TRUE],
			'file' => ['type' => self::TYPE_FILE, 'req' => TRUE]
		] );

		$this->return['errors'] = [];

		$depoID = (int)$p['depositionID'];
		//check user permissions to given deposition (creator, owner, case manager, deposition assistant, attendee)
		$deposition = $this->getOrchestraDepositions()->getDepositionForUserByID( $this->user->ID, $depoID );
		if( !$deposition ) {
			//check for child deposition
			$depoID = $this->getOrchestraDepositions()->getUserChildDeposition( $this->user->ID, $p['depositionID'] );
		}
		if( !$depoID ) $this->except( "Deposition not found by ID: {$p['depositionID']}", 404 );

		foreach( $p['metadata'] as $idx => $fileInfo ) {
			//\ClickBlocks\Debug::ErrorLog( print_r( $fileInfo, TRUE ) );
			$folder = FALSE;
			$folderID = $this->getOrchestraFolders()->checkPermissionToFolder( $this->user->ID, $fileInfo['folderID'] );
			if( $folderID ) {
				$folder = $this->getService( 'Folders' )->getByID( $folderID );
				//\ClickBlocks\Debug::ErrorLog( print_r( $folder->getValues(), TRUE ) );
			}
			if( !$folder || !$folder->ID || $depoID !== (int)$folder->depositionID ) {
				$this->return['errors'][] = ['metadata'=>$fileInfo, 'file'=>"{$idx}", 'errno'=>501];
				continue;
			}
			if( $folder->class == 'Exhibit' || $folder->class == 'CourtesyCopy' ) {
				$this->except( 'Access denied to protected folder', 403 );
			}

			$uploadInfo = FALSE;
			$f =& $_FILES['file'];
			foreach( $f['name'] as $i => $value ) {
				if( (int)$value === (int)$idx ) {
					$uploadInfo = ['name'=>$f['name'][$i], 'type'=>$f['type'][$i], 'tmp_name'=>$f['tmp_name'][$i], 'error'=>$f['error'][$i], 'size'=>$f['size'][$i]];
					break;
				}
			}
			if( $uploadInfo && $uploadInfo['error'] !== UPLOAD_ERR_OK ) {
				$this->return['errors'][] = ['metadata'=>$fileInfo, 'file'=>$uploadInfo['name'], 'errno'=>$uploadInfo['error']];
				continue;
			}
			//\ClickBlocks\Debug::ErrorLog( print_r( $uploadInfo, TRUE ) );

			$fileID = (int)$fileInfo['fileID'];
			if( $fileID > 0 ) {
				//overwrite
				$file = $this->getService( 'Files' )->getByID( $fileID );
				if( (int)$file->folderID !== (int)$folderID ) {
					//\ClickBlocks\Debug::ErrorLog( print_r( $file->getValues(), TRUE ) );
					$this->return['errors'][] = ['metadata'=>$fileInfo, 'file'=>$uploadInfo['name'], 'errno'=>501];
					continue;
				}
				if( $file && $file->ID ) {
					move_uploaded_file( $uploadInfo['tmp_name'], $file->getFullName() );
					if( $fileInfo['fileName'] !== $file->name ) {
						$dir = dirname( $file->getFullName() );
						rename( $file->getFullName(), "{$dir}/{$fileInfo['fileName']}" );
						$file->name = $fileInfo['fileName'];
					}
					$file->created = $fileInfo['created'];
					$sourceFileID = (int)$fileInfo['sourceFileID'];
					$sourceFileID = $this->getOrchestraFiles()->getSourceFileID( $sourceFileID );
					if( $sourceFileID !== $file->ID ) $file->sourceID = $sourceFileID;
					//\ClickBlocks\Debug::ErrorLog( 'Saving file: ' . print_r( $file->getValues(), TRUE ) );
					$file->save();
					$this->return['files'][] = ['ID'=>$file->ID, 'folderID'=>$file->folderID, 'fileName'=>$file->name, 'metadata'=>$fileInfo];
				} else {
					//create
					$fileID = -1;
				}
			}
			if( $fileID < 0 ) {
				//create
				$file = $this->getBLL( 'Files' );
				$file->folderID = $folderID;
				$file->createdBy = $folder->createdBy;
				$file->name = $fileInfo['fileName'];
				$file->created = $fileInfo['created'];
				if( $sourceFileID !== $file->ID ) $file->sourceID = $sourceFileID;
				$file->setSourceFile( $uploadInfo['tmp_name'], TRUE );
				//\ClickBlocks\Debug::ErrorLog( 'Creating file: ' . print_r( $file->getValues(), TRUE ) );
				$file->insert();
				$this->return['files'][] = ['ID'=>$file->ID, 'folderID'=>$file->folderID, 'fileName'=>$file->name, 'metadata'=>$fileInfo];
			}
		}
	}

	public function api_saveFolder( $p )
	{
		$this->validateParams( ['folderID'] );

		// get a list of files for the requested folder
		$filesToAttach = [];
		$Folder = $this->getService( 'Folders' )->getByID( $p['folderID'] );
		$pathToFolder = $Folder->getFullPath();
		foreach ($this->getOrchestraFolders()->getFiles_API( $Folder->ID ) as $file)
		{
			$File = $this->getService( 'Files' )->getByID( $file['ID'] );
			$fullPath = $pathToFolder.DIRECTORY_SEPARATOR.$File->name;
			$filesToAttach[] = (object)[
				'path' => $fullPath,
				'size' => filesize( $fullPath ),
			];
		}
		$compressedFolder = $this->prepareAttachment( $filesToAttach, $Folder->name, FORCE_SINGLE_FILE );
		$this->embedDownload( $compressedFolder, DROP_FILE_AFTER_EMBED );
	}

	public function api_sendFolder( $p )
	{
		$this->validateParams( [
			'folderID' => ['type'=>self::TYPE_SCALAR, 'req'=>true],
			'sendTo' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendCc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendBcc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendSubject' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendContents' => ['type'=>self::TYPE_STRING, 'req'=>false],
		] );

		// initialize the phpMailer helper
		$mailer = $this->prepareMailer( $p );

		$filesToAttach = [];

		// for the folderID, get a list of files and add as attachments to the headers
		$Folder = $this->getService( 'Folders' )->getByID( $p['folderID'] );
		$pathToFolder = $Folder->getFullPath();
		foreach ($this->getOrchestraFolders()->getFiles_API( $Folder->ID ) as $file)
		{
			$File = $this->getService( 'Files' )->getByID( $file['ID'] );
			$fullPath = $pathToFolder.DIRECTORY_SEPARATOR.$File->name;
			$filesToAttach[] = (object)[
				'path' => $fullPath,
				'size' => filesize( $fullPath ),
			];
		}

		try {
			// limit number of attachments by file size, if greater send multiple emails until all attachments have been sent.
			$packet_count = 0;
			while (($toAttach = $this->prepareAttachment( $filesToAttach, ($p['sendSubject'].'_'.(++$packet_count)) )) !== null)
			{
				$mailer->addAttachment( $toAttach );
				$sendStatus = $this->mailSend( $mailer );

				unlink( $toAttach );
				$mailer->ClearAttachments();

				if (!$sendStatus)
				{
					throw new Exception( 'An error occured attempting to send folder' );
				}
			}
			$this->return['status'] = true;
		} catch (Exception $e) {
			$this->return['status'] = false;
			$this->return['error'] = $e->getMessage();
		}

		unset( $mailer );
	}

	public function api_sendFile( $p )
	{
		$this->validateParams( [
			'fileID' => ['type'=>self::TYPE_SCALAR, 'req'=>true],
			'sendTo' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendCc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendBcc' => ['type'=>self::TYPE_STRING, 'req'=>false],
			'sendSubject' => ['type'=>self::TYPE_STRING, 'req'=>true],
			'sendContents' => ['type'=>self::TYPE_STRING, 'req'=>false],
		] );

		// initialize the phpMailer helper
		$mailer = $this->prepareMailer( $p );

		// for the folderID, get a list of files and add as attachments to the headers
		$File = $this->getService( 'Files' )->getByID( $p['fileID'] );
		$filename = $File->getFullName();
		$mailer->addAttachment( $filename, substr( $filename, strrpos( $filename, DIRECTORY_SEPARATOR )+1 ) );

		$mailStatus = $this->mailSend( $mailer );

		$this->return['status'] = $mailStatus;
		if (!$mailStatus)
		{
			$this->return['error'] = 'An error occurred attempting to send file.';
		}

		unset( $mailer );
	}

	public function embedDownload( $file, $purgeAfterEmbed=0 )
	{
		$finfo = finfo_open( FILEINFO_MIME_TYPE );
		$contentType = finfo_file( $finfo, $file );
		finfo_close( $finfo );
		$this->formalOutput = false;
		ob_end_clean();
		ob_start();
		header( 'Accept-Ranges: bytes' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private', FALSE );
		header( 'Content-Disposition: attachment; filename='.substr( $file, strrpos( $file, '/' )+1 ) );
		header( 'Content-Encoding: binary' );
		header( 'Content-Length: ' . (int)filesize( $file ) );
		header( 'Content-Transfer-Encoding: binary' );
		header( 'Content-Type: ' . $contentType );
		header( 'Expires: 0' );
		header( 'Last-Modified: '.gmdate( 'D, d M Y H:i:s' ).' GMT' );
		readfile( $file );
		ob_end_flush();

		if ($purgeAfterEmbed === DROP_FILE_AFTER_EMBED)
		{
			unlink( $file );
		}

		exit;
	}

	protected function mailSend( $phpmail )
	{
		try {
			$phpmail->Send();
		} catch (Exception $e) {
			$errMsg = (get_class( $e ) === 'phpmailerException') ? $e->errorMessage() : $e->getMessage();
			$this->api_log( "mail() error occurred: $errMsg [{$e->getCode()}]", true );
			return false;
		}
		return true;
	}

	protected function prepareMailer( $p )
	{
		$mailer = new Utils\Mailer();

		// set subject, content, and sender
		$mailer->Subject = $p['sendSubject'];
		$mailer->Body = (isset( $p['sendContents'] ) && strlen( $p['sendContents'] ) > 0 ? $p['sendContents'] : " ");
		if (isset( $this->attendee ))
		{
			$mailer->SetFrom( $this->attendee->email, $this->attendee->name );
			$mailer->ClearReplyTos();
			$mailer->AddReplyTo( $this->attendee->email );
		} else
		if (isset( $this->user ))
		{
			$mailer->SetFrom( $this->user->email, "{$this->user->firstName} {$this->user->lastName}" );
			$mailer->ClearReplyTos();
			$mailer->AddReplyTo( $this->user->email );
		}

		// set addresses, separate addresses by newline, comma or semi-colon
		foreach (preg_split( "/[\n\r;,]+/", $p['sendTo'] ) as $to)
		{
			$mailer->AddAddress( $to );
		}

		if ($p['sendCc'] && strlen( $p['sendCc'] ) > 0)
		{
			foreach (preg_split( "/[\n\r;,]+/", $p['sendCc'] ) as $cc)
			{
				$mailer->AddCC( $cc );
			}
		}

		if ($p['sendBcc'] && strlen( $p['sendBcc'] ) > 0)
		{
			foreach (preg_split( "/[\n\r;,]+/", $p['sendBcc'] ) as $bcc)
			{
				$mailer->AddBCC( $bcc );
			}
		}

		return $mailer;
	}

	/**
	 * Given a list of files with properties path and size, prepare a "packet" of files with size less than MAX_ATTACHMENT_SIZE.
	 * Archive the files with standard zip and return the path to the zip file.
	 *
	 * prepareAttachment() is destructive to input array
	 *
	 * @param array $files
	 * @param string $archiveName
	 * @return string Path to archive of file "packet"
	 */
	protected function prepareAttachment( &$files, $archiveName='', $splitFiles=true  )
	{
		if (!is_array( $files ) || count( $files ) < 1)
		{
			return null;
		}

		if (strlen( $archiveName ) < 1)
		{
			$archiveName = 'archive_'.date( 'YmdHis' );
		}

		// test opening the zip archive before we tear anything up
		$archivePath = CORE\IO::dir( 'temp' ).DIRECTORY_SEPARATOR.$archiveName.'.zip';
		$archive = new \ZipArchive();
		if (($result = $archive->open( $archivePath, \ZipArchive::CREATE )) !== true)
		{
			throw new Exception( $result );
		}

		$accrual = 0;

		do {
			if ($splitFiles !== FORCE_SINGLE_FILE)
			{
				if ($files[0]->size+$accrual > MAX_ATTACHMENT_SIZE)
				{
					break;
				}
			}
			$file = array_shift( $files );
			if ($archive->addFile( $file->path, substr( $file->path, strrpos( $file->path, DIRECTORY_SEPARATOR )+1 ) ) === false)
			{
				$this->api_log( "unknown error occurred adding {$file->path} to archive $archiveName", true );
			}
			$accrual += $file->size;
		} while(count( $files ) > 0);

		// save the archive and close for writing
		$archive->close();
		unset( $archive );
		return $archivePath;
	}
}

?>
