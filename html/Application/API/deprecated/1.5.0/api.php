<?php

namespace ClickBlocks\API\v1_5_0;
const apiVersion = '1.5.0';

require_once(__DIR__ . '/../../connect.php');

/**
 * lowercased array of allowed controllers
 */
$ents = array('edepo','door','user','chat','deposition','clients','cases','file');

$api = new API();
$api->setEntities($ents)->execute();

?>
