<?php

namespace ClickBlocks\API\v1_6_3\Logic;

use ClickBlocks\API\v1_6_3,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;


class Cases extends Edepo
{
	public function api_getList($p) {
		$this->validateParams(array(
			//'isPast' => array(self::TYPE_BOOLEAN),
			'onlyScheduled' => array(self::TYPE_BOOLEAN),
			'date' => array(self::TYPE_DATESTRING)
		));
		// cases directly owned or managed, or demos
		$cases = $this->getOrchestraCases()->getAPICasesByUser($this->user->ID, $this->user->clientID);
		// all accessible depositions list (in all cases)
		$depos = $this->getOrchestraDepositions()->getAPIUserDepositions($this->user->ID, $p['date'], $p['onlyScheduled']);
		$caseIDs = array();
		foreach ($depos as $idx => $depo) {
			// Remove the password hashes. We don't want these visible!
			unset( $depos[$idx]['password'], $depos[$idx]['courtReporterPassword'] );

			$caseIDs[$depo['caseID']] = true;
			if( $depo['parentID'] ) {
				$depos[$idx]['sourceDeposition'] = BLLFormat::getValues( $this->getService( 'Depositions' )->getByID( $depo['ID'] )->getParentDeposition() );
			}
		}
		foreach ($cases as $case) unset($caseIDs[$case['ID']]);
		// load additional cases if neccesary
		if (count($caseIDs) > 0) {
			$cases = array_merge($cases, $this->getOrchestraCases()->getCasesByIDs(array_keys($caseIDs)));
			usort($cases, function($a,$b){ return strcmp($a['name'],$b['name']); });
		}
		foreach ($cases as $k=>$case) {
			$cases[$k]['depositions'] = array();
			foreach ($depos as $depo) if ($depo['caseID'] == $case['ID']) $cases[$k]['depositions'][] = $depo;
		}
		$this->return['cases'] = $cases;
	}

	private function getDepositionsForUser( $scheduled=TRUE )
	{
		$scheduled = (bool)$scheduled;
		$caseIDs = [];
		$cases = [];
		$caseDepositions = [];
		$userDepositions = $this->getOrchestraDepositions()->getAPIDepositions( $this->user->ID, $scheduled );
		foreach( $userDepositions as $deposition ) {
			$caseID = (int)$deposition['caseID'];
			$caseIDs[] = $caseID;
			$caseDepositions[$caseID][] = $deposition;
		}
		$userCases = $this->getOrchestraCases()->getCasesByIDs( $caseIDs );
		if( $userCases && is_array( $userCases ) ) {
			foreach( $userCases as $case ) {
				$caseID = (int)$case['ID'];
				$case['depositions'] = $caseDepositions[$caseID];
				$cases[] = $case;
			}
		}
		return $cases;
	}

	public function api_getScheduledList( $p=NULL )
	{
		$this->return['cases'] = $this->getDepositionsForUser( TRUE );
	}

	public function api_getFinishedList( $p=NULL )
	{
		$this->return['cases'] = $this->getDepositionsForUser( FALSE );
	}
}

?>
