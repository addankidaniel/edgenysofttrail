<?php

namespace ClickBlocks\API\v1_4_0\Logic;

use ClickBlocks\API\v1_4_0,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\DB;


class File extends Edepo
{
	public function api_download($p) {
		$this->requireParams(array('fileID'));
		$file = $this->getService('Files')->getByID($p['fileID']);
		if (!$file->ID)
			$this->except('File not found', 1002);
		if ($this->isUserMember()) { // check access for Member
			$folder = $file->folders[0];
			$depoID = $folder->depositionID;
			$attended = $this->getOrchestraDepositionAttendees()->checkUserAttendedDepo($depoID, $this->user->ID);
			$hasAccess = (
				($attended && ($file->createdBy==$this->user->ID || $folder->isExhibit))
				|| ($file->createdBy==$folder->depositions->createdBy && $this->getOrchestraDepositions()->isTrustedUser($this->user->ID, $depoID))
			);
			if (!$hasAccess) $this->except('Access denied', 403);
		} else { // check access for Guest
			if (!$file->folders->isExhibit && !$this->getOrchestra('FileShares')->checkAttendeeAccessToFile($this->attendee->ID, $file->ID))
				$this->except('Access denied', 403);
		}
		if( isset( $p['embed'] ) && $p['embed'] ) {
			$this->embedDownload( $file->getFullName() );
			return;
		}
		$this->flushFile( $file->getFullName() );
	}
	
	public function embedDownload( $file )
	{
		//$contentType = function_exists( 'mime_content_type' ) ? mime_content_type( $file ) : 'application/octet-stream';
		$finfo = finfo_open( FILEINFO_MIME_TYPE );
		$contentType = finfo_file( $finfo, $file );
		finfo_close( $finfo );
		ob_end_clean();
		ob_start();
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private', FALSE );
		header( 'Content-Encoding: binary' );
		header( 'Content-Type: ' . $contentType );
		header( 'Content-Length: ' . (int)filesize( $file ) );
		readfile( $file );
		ob_end_flush();
		exit;
	}

  // upload file to user dir
  public function api_upload($p)
  {
     if (!$this->isUserMember()) $this->except ('Only member user access allowed!');
     $this->validateParams(array(
         'depositionID' => array('type'=>self::TYPE_NUMBER, 'req'=>true),
         'folderID' => array('type'=>self::TYPE_NUMBER, 'req'=>false),
         'overwrite' => array('type'=>self::TYPE_BOOLEAN, 'req'=>false),
         'file' => array('type'=>self::TYPE_FILE, 'req'=>true),
     ));
     $depo = $this->getService('Depositions')->getByID($p['depositionID']);
     if (!$depo->ID) $this->except('Deposition not found by ID: '.$p['depositionID'], 404);
     if ($p['folderID']) {
       $folder = $this->getService('Folders')->getByID($p['folderID']);
       if (!$folder->ID) $this->except ('Folder not found by ID: '.$p['folderID'], 404);
       if ($folder->createdBy != $this->user->ID) $this->except ('Access denied to folder!', 403);
     }
     if (!$p['folderID'] || $folder->isExhibit) {
       $folder = $this->getOrchestraFolders()->getPersonalFolderObject($depo->ID, $this->user->ID);
       if (!$folder->ID) { // first time saving to personal folder - create it
          $folder->depositionID = $depo->ID;
          $folder->createdBy = $this->user->ID;
          $folder->name = $this->config->logic['personalFolderName'];
          $folder->isPrivate = 1;
          $folder->insert();
       }
     }
     $info = $_FILES['file'];
     if (!is_array($info['tmp_name'])) {
        foreach ($info as $k=>$v) $info[$k] = array($v);
     }
     $this->return['files'] = array();
	foreach ($info['tmp_name'] as $k=>$v)
	{
		if ($info['error'][$k] != 0) $this->except('Incorrect file upload, error: '.$info['error'][$k]);
		if( $p['overwrite'] ) {
			$file = foo($this->getService( 'Files' ))->getByID( $p['fileID'] );
			move_uploaded_file( $info['tmp_name'][$k], $file->getFullName() );
			$file->created = 'NOW()';
			$file->save();
		} else {
			$file = $this->getBLL('Files');
			$file->name = $info['name'][$k];
			$file->folderID = $folder->ID;
			$file->createdBy = $this->user->ID;
			$file->setOverwrite($p['overwrite']);
			$file->setSourceFile($info['tmp_name'][$k], true);
			$file->insert();
		}
		$this->return['folderID'] = $folder->ID; // for backward compatibility
		$this->return['fileID'] = $file->ID;     // for backward compatibility
		$this->return['files'][] = array('ID'=>$file->ID, 'folderID'=>$folder->ID);
	}
  }


	// upload witness annotations to deposition owner personal folder
	public function api_upload_witness( $p )
	{
		if ( !$this->isUserGuest() ) $this->except( 'Only for guest users!' );

		$this->validateParams(
			array(
				'depositionID' => array( 'type' => self::TYPE_NUMBER, 'req'=>true ),
				'speakerID' => array( 'type' => self::TYPE_NUMBER, 'req'=>true ),
				'file' => array( 'type' => self::TYPE_FILE, 'req'=>true )
			)
		);

		$depositionID = (int)$p['depositionID'];
		$speakerID = (int)$p['speakerID'];


		$deposition = $this->getService( 'Depositions' )->getByID( $depositionID );
		if ( !$deposition->ID ) $this->except( "Deposition not found by ID: {$depositionID}", 404 );

		$folder = $this->getOrchestraFolders()->getPersonalFolderForUserInDeposition( $deposition->ID, $speakerID );
		if (!$folder->ID) $this->except( "Unable to get Folder for UserID: {$speakerID}", 404 );

		//\ClickBlocks\Debug::ErrorLog( "Deposition: {$deposition->ID} SpeakerID: {$speakerID} Folder: {$folder->ID} '{$folder->name}'" );

		$uploads = $_FILES['file'];
		if ( !is_array( $uploads['tmp_name'] ) )
		{
		   foreach ($uploads as $k => $v)
		   {
			   $uploads[$k] = array( $v );
		   }
		}

		$this->return['files'] = array();

		foreach ($uploads['tmp_name'] as $k => $v)
		{
			if ($uploads['error'][$k] != 0) $this->except( 'Incorrect file upload, error: ' . $uploads['error'][$k] );
			$file = $this->getBLL('Files');

			$fileParts = pathinfo( $uploads['name'][$k] );
			$file->name = "{$fileParts['filename']}_WitnessAnnotations.{$fileParts['extension']}";
			$file->folderID = $folder->ID;
			$file->createdBy = $speakerID;
			$file->setSourceFile( $uploads['tmp_name'][$k], true );
			$file->insert();
			$this->return['folderID'] = $folder->ID; // for backward compatibility
			$this->return['fileID'] = $file->ID;     // for backward compatibility
			$this->return['files'][] = array( 'ID' => $file->ID, 'folderID' => $folder->ID );
			//\ClickBlocks\Debug::ErrorLog( "File: {$file->ID} Name: '{$file->name}'" );
		}

		$this->getNodeJS()->sendPostCommand( 'witness_uploaded', null, array( 'depositionID' => $deposition->ID, 'notifyUsers' => array( $speakerID ), 'folderID' => $folder->ID, 'folderName' => $folder->name ) );
	}

}

?>
