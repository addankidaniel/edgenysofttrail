<?php

namespace ClickBlocks\API\v1_4_0\Logic;

class LazyORMEntity
{
  private $obj = null;
  private $contr;
  private $name;
  private $type;

  function __construct($contr, $type, $name) {
    $this->contr = $contr;
    $this->type = $type;
    $this->name = $name;
  }

  private function connect() {
    if (!$this->obj) {
       $this->obj = $this->contr->getORMEntity_($this->name, $this->type);
    }
  }

  public function __call($meth, $args) {
    $this->connect();
    return call_user_func_array(array($this->obj, $meth), $args);
  }

  public static function __callStatic($meth, $args) {
    $this->connect();
    return call_user_func_array(array($this->obj, $meth), $args);
  }

  public function __get($k) {
    $this->connect();
    return $this->obj->{$k};
  }

  public function __set($k, $v) {
    $this->connect();
    $this->obj->{$k} = $v;
  }
}
?>
