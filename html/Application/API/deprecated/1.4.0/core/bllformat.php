<?php

namespace ClickBlocks\API\v1_4_0\Logic;

use ClickBlocks\DB;

/**
 * Description of blldecorator
 *
 * @author Killian
 */
class BLLFormat {

   public static function formatRow(array $vals, $class = null) {
      if ($class) switch (strtolower($class)) {
         case 'depositions':
            unset($vals['courtReporterPassword']);
            break;
         case 'users':
            unset($vals['password']);
            break;
         case 'folders':
            if (isset($vals['numFiles'])) $vals['numFiles'] = (int)$vals['numFiles'];
      }
      //foreach ($vals as $k=>$v) if (preg_match('/(ID)|(By)/',$k)) $vals[$k] = (float)$v;
      //if (isset($vals['ID'])) $vals['ID'] = (int)$vals['ID'];
      return $vals;
   }

   public static function formatRows(array $rows, $class = null) {
      foreach ($rows as &$row) $row = self::formatRow($row, $class);
      return $rows;
   }

   public static function getValues($bll) {
      $vals = $bll->getValues();
      foreach (array('Depositions','Users') as $shcl)
         if (is_a($bll, 'ClickBlocks\DB\\'.$shcl)) {
            $class = $shcl;
            break;
         }
      if ($class == 'Depositions') {
         $vals['case'] = $bll->cases[0]->getValues();
         $vals['clientID'] = $bll->cases[0]->clientID;
      }
      $vals = self::formatRow($vals, $class);
      return $vals;
   }
}

?>
