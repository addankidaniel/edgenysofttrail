<?php

namespace ClickBlocks\API\v1_4_0;
use ClickBlocks\DB,
    ClickBlocks\Core,
    ClickBlocks\Utils,
    ClickBlocks\Exceptions,
    ClickBlocks\Utils\DT;

class Example extends APIControllerBase
{
   const SESSION_STORAGE = 'api_auth';

   const TYPE_INT = 'int';
   const TYPE_STRING = 'str';
   const TYPE_BOOL = 'bool';
   const TYPE_ARRAY = 'array';
   const TYPE_NUMBER = 'number';

   /**
    * @var \ClickBlocks\Core\Register
    */
   protected $reg;

   /**
    * @var \ClickBlocks\Core\Config
    */
   protected $config;

   /**
    * @var \ClickBlocks\Utils\Timer
    */
   protected $timer;

   /**
    * @var \ClickBlocks\DB\Users
    */
   protected $user;

   /**
    * This method is executed right before the main method that was called by user
    * @param string $method Name of method that will be called
    * @throws \LogicException
    */
   public function init_($method)
   {
      parent::init_($method);
      try {
         DB\ORM::getInstance()->getDB('db0')->catchException = true;
      } catch (\Exception $e) {
         throw new \LogicException('Database is not available', 500);
      }
      $this->reg = Core\Register::getInstance();
      $this->config = $this->reg->config;
      $this->timer = Utils\Timer::getInstance();
      // check authorization
      $flag = true;
      $sKey = @$this->params['sKey'];
      if (!$sKey) {
         $flag = false;  // session key not provided
      } else {
         $hashes = $this->reg->cache->get(self::SESSION_STORAGE) ? : array();
         // delete expired sessions
         foreach ($hashes as $hash => $info) if (time() > $info['expire']) unset($hashes[$hash]);
         if (isset($hashes[$sKey])) {
            /*$this->user = foo(new DB\ServiceUsers)->getByID($hashes[$sKey]['id']);
            if (!$this->user->userID) {
               unset($hashes[$sKey]);
               throw new \LogicException('Session for non-existent user #' . $hashes[$sKey]['id'], 0);
            }*/
         } else {
            $flag = false; // session not found
         }
         $this->reg->cache->set(self::SESSION_STORAGE, $hashes, $this->getSessionLifeTime());
      }
      if (!$flag && ($sKey || !in_array(strtolower($method), $this->getNoAuthMethods())))
         $this->except('Authorization failed', 401);
      if ($this->config->isDebug) $this->timer->start('Executing '.__CLASS__.'::'.$method.'()');
   }

   protected function getSessionLifeTime()
   {
      return (int)$this->reg->config->api['sessionLifeTime'] ?: 60;
   }

   public function finalize_($method)
   {
      parent::finalize_($method);
      $this->return = array('result'=>(object)(is_scalar($this->return) ? array('status'=>$this->return) : $this->return));
      if ($this->config->isDebug) {
         $this->timer->stop();
         $times = $this->timer->getTimes();
         if (count($times)) $this->return['times'] = $times;
      }
      $this->return['timestamp'] = time();
      $tz = date('Z');
      $this->return['tzOffset'] = (int)$tz;
   }

   protected function createSession($userID)
   {
    $hash = hash('sha512', microtime().$this->reg->config->secretSalt);
    $hashes = $this->reg->cache->get(self::SESSION_STORAGE)?: array();
    foreach ($hashes as $h=>$info) // delete all previous sessions
      if ($info['id']==$userID)
        unset($hashes[$h]);
    $hashes[$hash] = array('id'=>$userID, 'expire'=>time() + $this->getSessionLifeTime() );
    $this->reg->cache->set(self::SESSION_STORAGE,$hashes, $this->getSessionLifeTime());
    return $hash;
  }

  protected function deleteSession($userID)
  {
    $hashes = $this->reg->cache->get(self::SESSION_STORAGE)?: array();
    foreach ($hashes as $h=>$info) // delete all previous sessions
      if ($info['id']==$userID)
      {
        unset($hashes[$h]);
      }
    $this->reg->cache->set(self::SESSION_STORAGE,$hashes, $this->sessionLifetime);
  }

   /**
    * Should return array of method names, for which the sKey will not be required (no user context, or it is optional)
    * @return array Array of method names
    * @access protected
    */
   protected function getNoAuthMethods()
   {
      return array('test');
   }

   /**
    * Checks string for valid email address, according to ClickBlocks ValidatorEmail class
    * @param string $email Email to validate
    * @throws \LogicException with code 202 in case of incorrect email
    */
   private static function validateEmail($email)
   {
      if (!$email) return;
      $regexp = \ClickBlocks\Web\UI\POM\ValidatorEmail::EMAIL_REG_EXP;
      if (!preg_match($regexp, $email)) $this->except('Email format incorrect', 202);
   }

   public function test(array $p) {
      $this->return = array('test'=> "The API is working! Great!", 'par'=>$p);
   }

   protected function requireParams($list, $notEmpty = true)
   {
      return parent::requireParams($list, $notEmpty, 201);
   }

   protected function except($msg, $code = 0)
   {
      throw new \LogicException($msg, $code);
   }

   protected static function exec($cmd, $error = 'executing')
   {
      exec($cmd . ' 2>&1', $out, $err);
      if ($err) {
         throw new \Exception('Error ' . $error . ': "' . implode('; ', $out) . '"; Command: "' . $cmd . '"');
      }
      return implode('', $out);
   }

   protected function db2jsonObject(array $dbobject, array $fields)
   {
      $object = array();
      foreach ($fields as $field=>$info) {
         if (!is_array($info)) $info = array('type'=>$info);
         elseif (!$info['type'] && $info[0]) $info['type'] = $info[0];
         $dbfield = $info['bind'] ?: $field;
         $val = $dbobject[$dbfield];
         //if (isset($info['nullValue'])) $this->return['test'] = $info['nullValue'];
         if (isset($info['nullValue']) && $val === (string)$info['nullValue']) $val = NULL;
         if ($val !== NULL) switch ($info['type']) {
            case self::TYPE_INT:
               $val = (int)$val;
               break;
            case self::TYPE_NUMBER:
               $val = (float)$val;
               break;
            case self::TYPE_BOOL:
               $val = (bool)$val;
               break;
         }
         $object[$field] = $val;
      }
      return $object;
   }

   protected function validateObject($object, array $fields, $objectName)
   {
      try {
         if (!is_array($object)) $this->except('Not object');
         foreach ($fields as $field=>$info) {
            if (!is_array($info)) $info = array('type'=>$info, 'req'=>false);
            elseif (!$info['type'] && $info[0]) $info['type'] = $info[0];
            $val = $object[$field];
            if ($val === NULL && $info['req']) $this->except("Field '{$field}' is required.");
            $isType = true;
            if ($val !== NULL) switch ($info['type']) {
               case self::TYPE_STRING:
                  if (!is_string($val)) $isType = false;
                  break;
               case self::TYPE_INT:
                  if (!is_int($val)) $isType = false;
                  break;
               case self::TYPE_NUMBER:
                  if (!is_numeric($val)) $isType = false;
                  break;
               case self::TYPE_BOOL:
                  if (!is_bool($val)) $isType = false;
                  break;
               case self::TYPE_ARRAY:
                  if (!is_array($val)) $isType = false;
                  break;
            }
            if (!$isType) $this->except("Field '{$field}' must be of type '{$info['type']}'.");
            $object[$field] = $val;
         }
      } catch (\LogicException $e) {
         $this->except('Error in object \''.$objectName.'\': "'.$e->getMessage().'"', 202);
      }
   }
}

?>
