<?php                

namespace ClickBlocks\UnitTest\API\User;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\API,
    ClickBlocks\UnitTest\TestCase,
    ClickBlocks\UnitTest;

/**
 * @group logout 
 */ 
class logoutTest extends UnitTest\API\MethodTest
{
   protected function getControllerName() {
      return '\Clickblocks\API\Logic\User';
   }
   
   public function setUp()
   {
     parent::setUp();
     $this->setUpMockController(array('deleteSession'));
   }

   public function test_no_auth()
   {
     $this->expectApiError(401);
     $this->invokeThis(array('sKey'=>'')); 
   }

   public function test_success()
   {
     //$svc = $this->setUpMock('\ClickBlocks\DB\ServiceUsers');
     $this->setCurrentUser(array('ID'=>1, 'typeID'=>'C'));
     $old_sKey = 'OLD SESSION KEY!';
     $this->getController()->expects(self::once())->method('deleteSession')->with(self::equalTo(1))->will(self::returnValue(null)); // check session delete for user
     //$svc->expects(self::once())->method('update')->will(self::returnCallback(array(__CLASS__, 'save')));
     $this->invokeThis(array('sKey'=>$old_sKey));
   }

}

?>