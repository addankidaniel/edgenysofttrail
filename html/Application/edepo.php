<?php

namespace ClickBlocks\MVC;

use ClickBlocks\Core,
    ClickBlocks\DB,
    ClickBlocks\Web,
    ClickBlocks\Web\UI\Helpers,
    ClickBlocks\Web\UI\POM,
    ClickBlocks\Utils;

/**
 * Base Page class for eDepo Website
 *
 * @author Vladislav
 *
 *
 */
class Edepo extends Page implements \ClickBlocks\API\IEdepoBase
{
  protected $ie = null;
  protected $uri = null;

  /**
   * @var string Site base URL, like http://www.edepo.com
   */
  protected $siteURL;


  /**
   * @var array reference to $_SESSION['__EDEPO__'];
   */
  protected $session;

  /**
   *
   * @var array of mock objects
   */
  protected $mocks;
  private $unitTestMode;

  const SESSION_NS = '__EDEPO__';

	public function __construct( $template=null )
	{
		parent::__construct($template);
		preg_match('/MSIE ([0-9]?)/i', $_SERVER['HTTP_USER_AGENT'], $arr);
		$this->ie = (int)$arr[1];
		$this->uri = $this->reg->uri = new Utils\URI();
		$this->reg->db = DB\ORM::getInstance()->getDB('db0');
		$this->session =& $_SESSION[self::SESSION_NS];
		$this->siteURL = $this->uri->scheme . '://' . $this->uri->getSource();

		Web\XHTMLParser::$controls['POPUP'] = 1;
		Web\XHTMLParser::$controls['CUSTOMDROPDOWNBOX'] = 1;
		Web\XHTMLParser::$controls['CUSTOMCHECKBOX'] = 1;
		Web\XHTMLParser::$controls['CUSTOMRADIOBUTTON'] = 1;
		Web\XHTMLParser::$controls['FILEDROPUPLOAD'] = 1;
		Web\XHTMLParser::$controls['REPORTDATERANGESELECTOR'] = 1;
		Web\XHTMLParser::$controls['AUTOFILLDROPDOWNBOX'] = 1;
		Web\XHTMLParser::$controls['PAGESIZE'] = 1;
	}

  public function init()
  {
    parent::init();
    $this->tpl->config = $this->config;
    //$this->tpl->imagesURL = Core\IO::url('images');
  }

	public function redirect( $uri='/' )
	{
		header( 'Location: '.htmlspecialchars( $uri ) );
		exit;
	}

  public static function getUSAStates($short = true)
   {
       $states = array(
           '' => '',
           'AL' => 'Alabama',
           'AK' => 'Alaska',
           'AZ' => 'Arizona',
           'AR' => 'Arkansas',
           'CA' => 'California',
           'CO' => 'Colorado',
           'CT' => 'Connecticut',
           'DC' => 'D.C.',
           'DE' => 'Delaware',
           'FL' => 'Florida',
           'GA' => 'Georgia',
           'HI' => 'Hawaii',
           'ID' => 'Idaho',
           'IL' => 'Illinois',
           'IN' => 'Indiana',
           'IA' => 'Iowa',
           'KS' => 'Kansas',
           'KY' => 'Kentucky',
           'LA' => 'Louisiana',
           'ME' => 'Maine',
           'MD' => 'Maryland',
           'MA' => 'Massachusetts',
           'MI' => 'Michigan',
           'MN' => 'Minnesota',
           'MS' => 'Mississippi',
           'MO' => 'Missouri',
           'MT' => 'Montana',
           'NE' => 'Nebraska',
           'NV' => 'Nevada',
           'NH' => 'New Hampshire',
           'NJ' => 'New Jersey',
           'NM' => 'New Mexico',
           'NY' => 'New York',
           'NC' => 'North Carolina',
           'ND' => 'North Dakota',
           'OH' => 'Ohio',
           'OK' => 'Oklahoma',
           'OR' => 'Oregon',
           'PA' => 'Pennsylvania',
           'RI' => 'Rhode Island',
           'SC' => 'South Carolina',
           'SD' => 'South Dakota',
           'TN' => 'Tennessee',
           'TX' => 'Texas',
           'UT' => 'Utah',
           'VT' => 'Vermont',
           'VA' => 'Virginia',
           'WA' => 'Washington',
           'WV' => 'West Virginia',
           'WI' => 'Wisconsin',
           'WY' => 'Wyoming');
	   ksort( $states );
       if ($short) {
           $keys = array_keys($states);
           $states = array_combine($keys, $keys);
       }
       return $states;
   }

	public static function getCountries( $short = true )
	{
		$countries = [
			'' => '',
			'US' => 'United States',
			'CA' => 'Canada',
			'OT' => 'Other'
		];
		if( $short ){
			$keys = array_keys( $countries );
			$countries = array_combine( $keys, $keys );
		}
		return $countries;
	}

	public static function getCanadianProvinces( $short = true )
	{
		$canadian_provinces = [
			'' => '',
			'AB' => 'Alberta', 
			'BC' => 'British Columbia', 
			'MB' => 'Manitoba', 
			'NL' => 'Newfoundland and Labrador', 
			'NB' => 'New Brunswick', 
			'NT' => 'Northwest Territories', 
			'NS' => 'Nova Scotia', 
			'NU' => 'Nunavut',
			'ON' => 'Ontario', 
			'PE' => 'Prince Edward Island', 
			'QC' => 'Quebec', 
			'SK' => 'Saskatchewan', 
			'YT' => 'Yukon Territory'
		];
		if( $short ){
			$keys = array_keys( $canadian_provinces );
			$canadian_provinces = array_combine( $keys, $keys );
		}
		ksort( $canadian_provinces );
		return $canadian_provinces;
	}

   public function isUnitTest()
   {
      return (bool)$this->unitTestMode;
   }

   /*public function __call($method, $args)
   {
      foreach (array('getOrchestra','getService','getBLL') as $prefix) {
         if (strpos($method, $prefix)===0) {
            return $this->{$prefix}(substr($method, strlen($prefix)));
         }
      }
      throw new \ErrorException(__CLASS__.' does not have method: '.$method);
   }*/

   public function getORMEntity($shortName, $type) {
    $classPrefix = '\ClickBlocks\DB\\'.($type == self::ORM_ENTITY_ORCHESTRA ? 'Orchestra' : 'Service');
    $shortName = strtolower($shortName);
    if (isset($this->mocks[$type][$shortName]) && is_object($mock = $this->mocks[$type][$shortName])) {
      return $mock;
    } elseif (is_string($shortName)) {
      if ($this->unitTestMode)
        throw new \Exception('Attempt to access physical layer in Page Unit Test. Add $this->setUpPageMock("'.$classPrefix.$shortName.'") to setUp() method.');
      $name = $classPrefix.$shortName;
      return new $name();
    } else
      throw new \LogicException('ORM entity must be string or object!');
  }

  /**
   * @return \ClickBlocks\DB\OrchestraUsers Description
   */
  protected function getOrchestraUsers() { return $this->getOrchestra('Users'); }
  /**
   * @return \ClickBlocks\DB\OrchestraClients Description
   */
  protected function getOrchestraClients() { return $this->getOrchestra('Clients'); }
  /**
   * @return \ClickBlocks\DB\OrchestraFiles Description
   */
  protected function getOrchestraFiles() { return $this->getOrchestra('Files'); }
  /**
   * @return \ClickBlocks\DB\OrchestraDepositions Description
   */
  protected function getOrchestraDepositions() { return $this->getOrchestra('Depositions'); }
  /**
   * @return \ClickBlocks\DB\OrchestraDepositionAssistants Description
   */
  protected function getOrchestraDepositionAssistants() { return $this->getOrchestra('DepositionAssistants'); }
  /**
   * @return \ClickBlocks\DB\OrchestraDepositionAttendees Description
   */
  protected function getOrchestraDepositionAttendees() { return $this->getOrchestra( 'DepositionAttendees' ); }
  /**
   * @return \ClickBlocks\DB\OrchestraCases Description
   */
  protected function getOrchestraCases() { return $this->getOrchestra('Cases'); }
  /**
   * @return \ClickBlocks\DB\OrchestraCaseManagers Description
   */
  protected function getOrchestraCaseManagers() { return $this->getOrchestra( 'CaseManagers' ); }
  /**
   * @return \ClickBlocks\DB\OrchestraFolders Description
   */
  protected function getOrchestraFolders() { return $this->getOrchestra('Folders'); }
  /**
   * @return \ClickBlocks\DB\OrchestraDatasources Description
   */
  protected function getOrchestraDatasources() { return $this->getOrchestra('Datasources'); }
  /**
   * @return \ClickBlocks\DB\OrchestraDatasourceUsers Description
   */
  protected function getOrchestraDatasourceUsers() { return $this->getOrchestra('DatasourceUsers'); }
  /**
   * @return \ClickBlocks\DB\OrchestraExhibitHistory
   */
  protected function getOrchestraExhibitHistory() { return $this->getOrchestra('ExhibitHistory'); }
  /**
   * @return \ClickBlocks\DB\OrchestraEnterpriseAgreements
   */
  protected function getOrchestraEnterpriseAgreements() { return $this->getOrchestra('EnterpriseAgreements'); }
  /**
   * @return \ClickBlocks\DB\OrchestraUserPreferences
   */
  protected function getOrchestraUserPreferences() { return $this->getOrchestra( 'UserPreferences' ); }
  /**
   * @return \ClickBlocks\DB\OrchestraInvoiceCharges
   */
  protected function getOrchestraInvoiceCharges() { return $this->getOrchestra( 'InvoiceCharges' ); }
  /**
   * @return \ClickBlocks\DB\OrchestraPlatformLog
   */
  protected function getOrchestraPlatformLog() { return $this->getOrchestra( 'PlatformLog' ); }
  /**
   * @return \ClickBlocks\DB\OrchestraUserSortPreferences
   */
  protected function getOrchestraUserSortPreferences() { return $this->getOrchestra( 'UserSortPreferences' ); }
  /**
   * @return \ClickBlocks\DB\OrchestraFolders Description
   */
  protected function getOrchestraSessionCaseFolders() { return $this->getOrchestra('SessionCaseFolders'); }

  /**
   * @param string $name entity name (Cases, Users, etc.)
   * @return \ClickBlocks\DB\Orchestra
   * @throws \Exception
   */
  protected function getOrchestra($name) {
    return $this->getORMEntity($name, self::ORM_ENTITY_ORCHESTRA);
  }

  /**
   * @param string $name entity name (Cases, Users, etc.)
   * @return \ClickBlocks\DB\Service
   * @throws \Exception
   */
  protected function getService($name) {
    return $this->getORMEntity($name, self::ORM_ENTITY_SERVICE);
  }

  /**
   * @param string $name entity name (Cases, Users, etc.)
   * @return \ClickBlocks\DB\BLLTable
   * @throws \Exception
   */
  protected function getBLL($name) {
    if ($this->unitTestMode) {
      return new DB\QuickBLL();
    } else {
      $cls = '\ClickBlocks\DB\\'.$name;
      return new $cls;
    }
  }

   /**
    *
    * @param type $mockType self::MOCK_TYPE_**
    * @param string $mockName
    * @param object $mock
    */
   public function attachMock($mockType, $mockName, $mock) {
      $this->unitTestMode = true;
      switch ($mockType):
         case self::MOCK_TYPE_ORCHESTRA:
         case self::MOCK_TYPE_SERVICE:
            $this->mocks[$mockType][strtolower($mockName)] = $mock;
            break;
         case self::MOCK_TYPE_PROPERTY:
            $this->{$mockName} = $mock;
            break;
      endswitch;
   }

	public static function getResellerLevels()
	{
		return [
			 self::RESELLERLEVEL_PLATINUM => 'Platinum',
			 self::RESELLERLEVEL_GOLD => 'Gold',
			 self::RESELLERLEVEL_SILVER => 'Silver',
			 self::RESELLERLEVEL_BRONZE => 'Bronze',
			 self::RESELLERLEVEL_NONE => 'None'
		];
	}

	public static function getResellerClasses()
	{
		return [
			self::RESELLERCLASS_SCHEDULING => 'Scheduling Reseller',
			self::RESELLERCLASS_RESELLER => 'eDiscovery Reseller',
			self::RESELLERCLASS_DEMO => 'Demo Reseller'
		];
	}

	public static function getPricingOptions()
	{
		return [
			self::PRICING_OPTION_BASE_LICENSING,
			self::PRICING_OPTION_SETUP,
			self::PRICING_OPTION_SUBSCRIPTION,
			self::PRICING_OPTION_CASE_SETUP,
			self::PRICING_OPTION_CASE_MONTHLY,
			self::PRICING_OPTION_DEPOSITION_SETUP,
			self::PRICING_OPTION_DEPOSITION_MONTHLY,
			self::PRICING_OPTION_UPLOAD_PER_DOC,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER1,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER2,
			self::PRICING_OPTION_EXHIBIT_RECEIVED_TIER3,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER1,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER2,
			self::PRICING_OPTION_EXHIBIT_INTRODUCED_TIER3,
			self::PRICING_OPTION_WITNESSPREP_ATTENDEE
		];
	}

	public static function getCourtUserTypes() {
		return [
			self::USER_TYPE_JUDGE => 'Judge',
			self::USER_TYPE_REPORTER => 'Reporter / Official',
			self::USER_TYPE_USER => 'User'
		];
	}

	public static function trialClasses() {
		return [
			self::DEPOSITION_CLASS_TRIAL,
			self::DEPOSITION_CLASS_DEMOTRIAL,
			self::DEPOSITION_CLASS_TRIALBINDER,
			self::DEPOSITION_CLASS_MEDIATION,
			self::DEPOSITION_CLASS_ARBITRATION,
			self::DEPOSITION_CLASS_HEARING
		];
	}

	public static function folderClasses() {
		return [
			self::FOLDERS_COURTESYCOPY,
			self::FOLDERS_EXHIBIT,
			self::FOLDERS_FOLDER,
			self::FOLDERS_PERSONAL,
			self::FOLDERS_TRANSCRIPT,
			self::FOLDERS_TRUSTED,
			self::FOLDERS_WITNESSANNOTATIONS
		];
	}

	public static function getReservedFolders( $allowPersonal=FALSE ) {
		$reserved = [];
		$cfg = Core\Register::getInstance()->config;
		$unreservedFolders = [
			'potentialExhibitsFolderName',
			'miscellaneousFilesFolderName',
			'attorneyNotesFolderName'
		];
		if( $allowPersonal === TRUE ) {
			$unreservedFolders[] = 'personalFolderName';
		}
		foreach( $cfg->logic as $key => $folderName ) {
			if( in_array( $key, $unreservedFolders ) ) {
				continue;
			}
			$reserved[$key] = $folderName;
		}
		return $reserved;
	}

	public static function isReservedFolder( $folderName, $allowPersonal=FALSE ) {
		foreach( self::getReservedFolders( $allowPersonal ) as $logicName ) {
            if( strcasecmp( $folderName, $logicName ) === 0 ) {
                return TRUE;
            }
        }
		return FALSE;
	}
}
