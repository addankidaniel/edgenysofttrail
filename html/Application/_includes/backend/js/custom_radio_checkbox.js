
function setupLabel() {
	if (jQuery('.custom_radio input').length) {
		jQuery('.custom_radio').each(function(){ 
			jQuery(this).removeClass('r_on');
		});
		jQuery('.custom_radio input:checked').each(function(){ 
			jQuery(this).parent('label').addClass('r_on');
		});
	};
	if (jQuery('.custom_checkbox input').length) {
		jQuery('.custom_checkbox').each(function(){ 
			jQuery(this).removeClass('r_on');
		});
		jQuery('.custom_checkbox input:checked').each(function(){ 
			jQuery(this).parent('label').addClass('r_on');
		});
	};
};
jQuery(document).ready(function(){
	jQuery('.custom_radio').click(function(){
		setupLabel();
	});
	jQuery('.custom_checkbox').click(function(){
		setupLabel();
	});
	setupLabel(); 
});
