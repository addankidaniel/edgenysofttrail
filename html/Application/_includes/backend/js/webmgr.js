/* global ajax */

/**
 * WebMgr
 */
var WebMgrClass = (function() {

	var _instance;
	var _s = {};

	/**
	 * Index; property of object
	 * @param {Object} o
	 * @param {String} i
	 * @returns {o[i]}
	 */
	var idx = function( o, i )
	{
		if( typeof o === 'undefined' || o === null ) {
			return;
		}
		return o[i];
	};

	function isset( v ) {
		return (typeof v !== 'undefined' && v !== null);
	}

	function WebMgrClass() {
		if( !(this instanceof WebMgrClass) ) {
			return new WebMgrClass();
		}

		var self = this;
		var onReadyQueue = [];
		var preWebMgrReadyState = true;
		var viewerAPI = null;
		var viewOnly = false;

		var WebMgrReady = function() {
			console.log( 'WebMgrClass::WebMgrReady()' );
			preWebMgrReadyState = false;
			while( onReadyQueue.length ) {
				onReadyQueue.shift().call();
			}
		};

		this.onReady = function( func ) {
			console.log( 'WebMgrClass::onReady()');
			if( typeof func === 'function' ) {
				onReadyQueue.push( func );
			}
			if( window.top.document.readyState === 'interactive' || window.top.document.readyState === 'complete' ) {
				WebMgrReady();
			}
		};

		this.viewerOnReady = function() {
			console.log('WebMgr::viewerOnReady()');
			if(isset(contentSearchTerms)) {
				viewerAPI.searchMultiTerms(contentSearchTerms);
			}
		};

		$(window.top.document).ready( function() {
			WebMgrReady();
		});

		this.setS = function( prop, val ) {
			_s[prop] = val;
		};

		this.getS = function( prop ) {
			if( typeof prop !== 'string' ) return;
			return prop.split( '.' ).reduce( idx, _s );
		};

		this.bindViewer = function( viewer ) {
			if( typeof viewer === "object" || viewer === null ) {
				viewerAPI = viewer;
				if( isset( viewerAPI ) && typeof viewerAPI.setActivityCallback === "function" ) {
					viewerAPI.onReady( self.viewerOnReady );
					viewerAPI.onSavable( viewerOnSavableStateChange );
				}
				if( isset( viewerAPI ) ) {
					setTimeout( function() {
						viewerAPI.activateDrawMode();
					}, 10 );
				}
			}
		};

		function viewerOnSavableStateChange( savable ) {
			if( !!savable ) {
				// enable
				$(".viewFile .btnSave").addClass( "active" );
			}
			else {
				$(".viewFile .btnSave").removeClass( "active" );
			}
		};

		this.viewerHasChanges = function() {
			var hasChanges = false;
			if( isset( viewerAPI ) ) {
				hasChanges = !!viewerAPI.hasAnnotationChanges();
			}
			return hasChanges;
		};

		this.saveAnnotations = function( formValues, sourceFileID, overwrite ) {
			if( !isset( viewerAPI ) ) {
				return;
			}
			var pAnnotations = viewerAPI.getAnnotationChanges().annotations;
			var annotations = JSON.stringify( pAnnotations );

			formValues.annotations = annotations;
			formValues.sourceFileID = sourceFileID;

			var data = ajax.doit( "->annotateFile", formValues );

			console.log( "WebMgr::annotate()::post", pAnnotations, formValues );

			if( data ) {
				console.log( "WebApp::annotate()::success", data, typeof data, arguments );

				if( overwrite === "overwrite" ) {
					console.log( "The document was successfully resaved!", overwrite );
				} else {
					console.log( "The document was successfully saved!", overwrite );
				}

				var fileID = sourceFileID;
				if( isset( data ) ) {
					newFileID = parseInt( data.fileID );
					console.log( "WebMgr::annotate() - set new file id:", newFileID );
					fileID = newFileID;
				}
				ajax.doit('->viewFile', newFileID);
				console.log( "WebApp::annotate()::return", fileID );
			} else {
				console.log( "WebMgr::annotate()::fail" );
				// TODO Notify user of failure
			}
		};

		this.discardChanges = function() {
			if( isset( viewerAPI ) ) {
				viewerAPI.discardAnnotationChanges();
				viewerOnSavableStateChange( false );
			}
		};

		this.viewOnlyMode = function() {
			viewOnly = true;
			if( isset( viewerAPI ) ) {
				viewerAPI.activateViewMode();
			}
		};
	}

	return {
		init: function()
		{
			if( !_instance ) {
				_instance = WebMgrClass.apply( null, arguments );
			}
			return _instance;
		},
		getInstance: function()
		{
			if( !_instance ) {
				return this.init.apply( null, arguments );
			}
			return _instance;
		}
	};

}());

window.top.webMgr = WebMgrClass.getInstance();

window.top.webMgr.onReady( function() {
	console.log( 'WebMgr::onReady' );
} );
