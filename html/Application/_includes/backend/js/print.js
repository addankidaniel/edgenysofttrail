/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
   var isPDF = ($('.header').size() == 0);
   if( isPDF ) {
	   return;	//nothing is needed
   }
   var pageHeight = isPDF ? 1150 : 1110;
   var pageNumber = 0;
   var lastBreak = 0;
   var top;
   var lim = 100;
   do {
	  if (++pageNumber > 1) {
		  pageHeight = 1000;
	  }
      var splitted = false;
      $('div.title-box,div.top-info-box,.page-break,.table-total,table.table,table.table tbody tr').each(function() {
		 top = $(this).position().top;
         if (this.className == 'page-break') {
			 lastBreak = top;
		 } else if (top - lastBreak > pageHeight) {
            if (this.tagName == 'TR') {
               // split tables
               var i = $(this).index();
               var table = $(this).parent().parent();
               var table2 = table.clone();
               table.find('tbody tr').slice(i).empty().remove();
               table2.find('tbody tr').slice(0,i).empty().remove();
               table2.insertAfter(table);
               table.after('<div class="page-break"></div>');
               splitted = true;
			   lastBreak = top;
               return false;
            } else {
               $(this).after('<div class="page-break"></div>');
            }
            lastBreak = top;
         }
      });
   } while (splitted && (--lim > 0));

   // if it's a PDF print, no need for this
	if (!isPDF)
	{
		var header = $('.header:eq(0)');
		var footer = $('.footer:eq(0)');
		var prevTop = 0, top;
		var pageHeight = 1240; // this is experimental value
		// process all page breaks
		$('.page-break').each(function(){
		   top = $(this).position().top;
		   footer.clone().css({'margin-top':(pageHeight - top - 70 + prevTop)+'px'}).insertBefore(this);
		   prevTop = header.clone().insertAfter(this).position().top;
		});
		var lastfoot = $('.footer:last');
		top = lastfoot.position().top;
		lastfoot.css({'margin-top':(pageHeight - top - 70 + prevTop)+'px'});

		setTimeout(print, 100);
	}
});