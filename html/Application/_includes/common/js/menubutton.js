/*
 * MenuButton
 */
var MenuButton = function() {

	this.toggle = function(id) {
		var el = $(id);
		var ul = el.siblings('ul');
		var iv = ul.is(':visible');
		if( !iv ) {
			el.children('div').addClass( 'active' );
		} else {
			el.children('div').removeClass( 'active' );
		}
		var tRect = ul.parent()[0].getBoundingClientRect();
		ul.toggle();
		if (ul.outerWidth() > tRect.width) {
			ul.css('border-top-left-radius', '8px');
		}
		if ( ul.parent().hasClass('sessionOptions') ) {
			if (ul.outerWidth() > tRect.width) {
				ul.parent().addClass('wideList');
			} else {
				ul.parent().addClass('narrowList');
			}
		}
	};

	this.hide = function() {
		$('.menubtn>ul').hide();
		$('.menubtn>a>div').removeClass('active');
	};
};

var menubutton = new MenuButton();

$(document).mouseup( function(e){
	var menuBtnID = $(e.target).parent().parent().parent().attr('id');
	var container = $(menuBtnID);
	if( !container.is(e.target) && container.has(e.target).length === 0) menubutton.hide();
} );

