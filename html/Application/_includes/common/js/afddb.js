/*
 * AutoFillDropDownBox
 */

/* global controls */
function AutoFillDDB() {
	console.log( "AutoFillDDB.new;" );
	this.instances = {};

	this.initialize = function( id ) {
		//console.log( "AutoFillDDB.initialize; id:", id );
		var options = [];
		var optionElements = [];
		$( "#list_" + id + " li" ).each( function() {
			var optEl = $( this );
			var option = {"key": optEl.attr( "data-id" ), "value": optEl.text()};
			options.push( option );
			optionElements.push( optEl[ 0 ] );
		} );
		this.instances[ id ] = {
			"options": options,
			"optionElements": optionElements,
			"pos": 0,
			"isFiltered": false,
			"blurTimeout": null
		};
	};

	this.select = function( id, option, key ) {
		//console.log( "AutoFillDDB.select; id:", id, "option:", option, "key:", key );
		controls.$( "val_" + id ).value = option;
		controls.$( "key_" + id ).value = key;
		controls.display( "list_" + id, "none" );
	};

	this.toggle = function( id, e ) {
		//console.log( "AutoFillDDB.toggle; id:", id, "e:", e );
		var instance = this.instances[ id ];
		if( instance.blurTimeout ) {
			clearTimeout( instance.blurTimeout );
		}
		var list_id = "list_" + id;
		var list_id_li_f = "#" + list_id + " li.filtered";
		var displayStyle = controls.getStyle( list_id, "display" );
		if( displayStyle === "none" ) {
			$( list_id_li_f ).removeClass( "filtered" );
			controls.display( list_id, "block" );
			instance.isFiltered = false;
			instance.pos = 0;
		} else {
			if( instance.isFiltered ) {
				$( list_id_li_f ).removeClass( "filtered" );
				instance.isFiltered = false;
			} else {
				controls.display( list_id, "none" );
				instance.pos = 0;
			}
		}
		controls.$( "val_" + id ).focus();
		if( typeof e !== "undefined" || typeof event !== "undefined" ) {
			controls.stopEvent( e || event );
		}
	};

	this.close = function( id, e ) {
		var options = this.instances[ id ].options;
		//console.log( "AutoFillDDB.close; id:", id, "e:", e, options );
		controls.display( "list_" + id, "none" );
		this.instances[ id ].pos = 0;
		if( typeof e !== "undefined" || typeof event !== "undefined" ) {
			controls.stopEvent( e || event );
		}
		var isOption = false;
		for( var i in options ) {
			if( options.hasOwnProperty( i ) ) {
				var option = options[ i ];
				if( option.value.toLowerCase() === controls.$( "val_" + id ).value.toLowerCase() ) {
					controls.$( "key_" + id ).value = option.key;
					controls.$( "val_" + id ).value = option.value; // case-insensitive match, restore "normal" value
					isOption = true;
					break;
				}
			}
		}
		if( !isOption ) {
			controls.$( "key_" + id ).value = 0;
		}
		//console.log( "AutoFillDDB.close; id:", id, controls.$( "key_" + id ).value, controls.$( "val_" + id ).value );
	};

	this.filter = function( id, e ) {
		//console.log( "AutoFillDDB.filter; id:", id, "e:", e );
		if( typeof e !== "undefined" || typeof event !== "undefined" ) {
			controls.stopEvent( e || event );
		}
		this.handleKeyCode( id, e );
	};

	this.handleKeyCode = function( id, e ) {
		//console.log( "AutoFillDDB.handleKeyCode; id:", id, "e:", e );
		var instance = this.instances[ id ];
		var val_id = "val_" + id;
		var list_id = "list_" + id;
		var list_id_li = "#" + list_id + " li";
		if( typeof e.keyCode !== "undefined" ) {
			var pos = instance.pos;
			var max = instance.options.length;
			var wasVisible = (controls.getStyle( list_id, "display" ) !== "none");
			controls.display( list_id, "block" );
			switch( e.keyCode ) {
				case 40:	//down
					if( e.ctrlKey || e.shiftKey ) return;
					pos = Math.min( ++pos, max );
					for( var p=pos; p<=max; ++p ) {
						var li = instance.optionElements[ (p - 1) ];
						if( typeof li === "undefined" ) {
							continue;
						}
						var jli = $( li );
						if( !jli.hasClass( "filtered" ) ) {
							$( list_id_li ).removeClass( "highlight" );
							jli.addClass( "highlight" );
							instance.pos = p;
							var jul = jli.parent();
							var ulHeight = jli.parent().height();
							var liPos = $( li ).position().top;
							if( liPos < 0 || liPos >= ulHeight ) {
								jul.scrollTop( liPos );
							}
							break;
						}
					}
					this.setCursorPos( id );
					if( wasVisible ) {
						return;
					}
					break;
				case 38:	//up
					if( e.ctrlKey || e.shiftKey ) {
						return;
					}
					pos = Math.max( --pos, 0 );
					for( var p=pos; p>=0; --p ) {
						if( p === 0 ) {
							$( list_id_li ).removeClass( "highlight" );
							instance.pos = p;
							break;
						}
						var li = instance.optionElements[ (p - 1) ];
						if( typeof li === "undefined" ) {
							continue;
						}
						var jli = $( li );
						if( !jli.hasClass( "filtered" ) ) {
							$( list_id_li ).removeClass( "highlight" );
							jli.addClass( "highlight" );
							instance.pos = p;
							var jul = jli.parent();
							var ulHeight = jli.parent().height();
							var liPos = $( li ).position().top;
							if( liPos < 0 || liPos >= ulHeight ) {
								jul.scrollTop( liPos );
							}
							break;
						}
					}
					this.setCursorPos( id );
					if( wasVisible ) {
						return;
					}
					break;
				case 13:	//enter
					$( list_id_li ).removeClass( "highlight" );
					if( pos > 0 ) {
						var li = instance.optionElements[ (pos - 1) ];
						controls.$( val_id ).value = $( li ).text();
						controls.display( list_id, "none" );
						instance.pos = 0;
						if( typeof e !== "undefined" || typeof event !== "undefined" ) {
							controls.stopEvent( e || event );
						}
						return;
					}
					break;
				case 27:	//esc
					if( typeof e !== "undefined" || typeof event !== "undefined" ) {
						controls.stopEvent( e || event );
					}
					instance.pos = 0;
					controls.display( list_id, "none" );
					return;
				case 37:	//left
				case 39:	//right
				case 16:	//shift
				case 17:	//ctrl
				case 18:	//alt
				case 19:	//pause/break
				case 20:	//capslock
				case 33:	//page up
				case 34:	//page down
					return;
					break;
				default:
					instance.pos = 0;
					$( list_id_li ).removeClass( "highlight" );
			}
		}
		instance.isFiltered = true;
		var curText = $( "#" + val_id ).val().toLowerCase();
		$( list_id_li ).each( function() {
			var optVal = $( this ).text().toLowerCase();
			if( optVal.indexOf( curText ) === 0 ) {
				$( this ).removeClass( "filtered" );
			} else {
				$( this ).addClass( "filtered" );
			}
		} );
	};

	/*
	 * move cursor position to end of input
	 */
	this.setCursorPos = function( id ) {
		//console.log( "AutoFillDDB.setCursorPos; id:", id );
		var ctrl = controls.$( "val_" + id );
		if( typeof ctrl !== "undefined" && ctrl.hasOwnProperty( "value" ) ) {
			var sameVal = ctrl.value;
			ctrl.value = sameVal; // trick browser to move cursor
		}
	};

	this.refocus = function( id ) {
		//console.log( "AutoFillDDB.refocus; id:", id );
		var instance = this.instances[ id ];
		if( instance.blurTimeout ) {
			clearTimeout( instance.blurTimeout );
		}
		controls.$( "val_" + id ).focus();
	};
};

if( typeof afddb === "undefined" ) {
	var afddb = new AutoFillDDB();
}
