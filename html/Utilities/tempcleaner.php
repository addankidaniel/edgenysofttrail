<?php

namespace ClickBlocks\Utils;

use ClickBlocks\Core;

class TempFolderCleaner {

	protected $config;
	protected $probability;
	protected $deleteOlder;

	public function __construct() {
		$this->config = Core\Register::getInstance()->config;
		$this->probability = isset( $this->config->tempCleanProbability ) ? (int)$this->config->tempCleanProbability : 5;
		$this->deleteOlder = (int)$this->config->tempCleanOlderThan ?: 86400;
	}

	public function cleanWithProbability() {
		if( rand( 0,100 ) <= $this->probability ) {
			$this->cleanDir( Core\IO::dir( 'temp' ) );
		}
	}

	public function cleanDir( $dir ) {
		$h = opendir( $dir );
		while( ($file = readdir( $h )) !== FALSE ) {
			if( $file == '.' || $file == '..' || mb_substr( $file, 0, 1 ) == '.' ) {
				continue;
			}
			$full = $dir . DIRECTORY_SEPARATOR . $file;
			if( is_dir( $full ) ) {
				$this->cleanDir($full);
			} elseif( (time() - filemtime( $full )) >= $this->deleteOlder ) {
				unlink( $full ); // delete older than 24h
			}
		}
	}
}
