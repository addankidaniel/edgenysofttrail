#!/bin/env php
<?php
require_once __DIR__ . '/../Application/connect.php';

use ClickBlocks\Core,
    ClickBlocks\DB;

$version = (int) foo(new DB\OrchestraDbVersion())->getDbVersion();

$version = sprintf("%03d", $version + 1);

$dbScriptDir = realpath(Core\IO::dir('db-scripts'));

$files = array();
foreach (new DirectoryIterator($dbScriptDir) as $fileInfo) {
    if($fileInfo->isDot()) continue;
    $files[$fileInfo->getFilename()] = $fileInfo->getPathname();
}
ksort($files);

$mysqlExec      = $reg->config->mysqlExec;
$dbConfig       = $reg->config->db0;
$dbNamePos      = strpos($dbConfig['dsn'], 'dbname=') + strlen('dbname=');
$endDbNamePos   = strpos($dbConfig['dsn'], ';');
$dbName         = substr($dbConfig['dsn'], $dbNamePos, $endDbNamePos - $dbNamePos);

$username = $dbConfig['dbuser'];
if (!$mysqlExec || !$dbName || !$username) die('Invalid config, please check again');

$password = $dbConfig['dbpass'];
if ($password) {
    $password = "-p{$password} ";
}

foreach ($files as $filename => $pathname) {
    if ($filename > $version) {
            
        $cmd = "{$mysqlExec} -u{$username} -D{$dbName} {$password} --default_character_set utf8 < {$pathname}";
        echo "{$cmd}\n";
        $process = proc_open($cmd, array(
            0 => STDIN,
            1 => STDOUT,
            2 => array('pipe', 'w'),
        ), $pipes);
        echo stream_get_contents($pipes[2]);
        $returnValue = proc_close($process);
        
        if ($returnValue != 0) break;
    }
}
